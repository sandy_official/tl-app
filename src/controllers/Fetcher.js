import { GET_METHOD, POST_METHOD } from "../utils/Constants";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";

export const fetcher = (url, method, body, ref, appToken, token, successCB, failureCB) => {
  let options = {
    method: method == GET_METHOD ? GET_METHOD : POST_METHOD,
    headers: {
      "Content-Type": "application/json",
      deviceid: "62c7c7042511c086",
      requestCode: "TLmappv1",
      TrendlyneAppToken: appToken,
    },
  };

  if (token) options.headers.KEY = token;

  if (body && Object.keys(body).length > 0) options.body = JSON.stringify(body);
  setFirebaseAnalyticsEvent(url, body);
  // console.log("ref", ref);

  fetch(url, options)
    .then((response) => {
      // console.log("url raw response", response);
      return response.json();
    })
    .then((json) => {
      console.log("URL json response", url, options, json);
      let { status, statusDescription } = json.head;
      if (status == 0) successCB(json);
      else {
        if (status == 4) ref && ref.showSubscribe();
        if (status == 100) ref && ref.showLogin();
        failureCB(statusDescription, status);
      }
    })
    .catch((error) => {
      failureCB("failed");
      console.log(error);
    });
};
