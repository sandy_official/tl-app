import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { GET_MY_ALERTS_URL, GET_MY_NOTIFICATIONS_URL } from "../utils/ApiEndPoints";

// export const getMyNewsFeed = (pageNumber, pageCount, qsTime, token, cb) => {
//   getFetchOptions(token, fetchOptions => {
//     const body = {
//       perPageCount: pageCount,
//       pageNumber: pageNumber,
//       qsTime: qsTime ? qsTime : new Date().getTime()
//     };

//     fetchOptions.body = JSON.stringify(body);
//     setFirebaseAnalyticsEvent(MY_NEWS_FEED_URL, body);

//     console.log("news fetch", fetchOptions);

//     fetch(MY_NEWS_FEED_URL, fetchOptions)
//       .then(response => {
//         console.log("news response before parsing", response);
//         return response.json();
//       })
//       .then(json => {
//         let header = json.head;
//         console.log("news response", json);

//         if (header.status == 0)
//           cb({
//             news: json.body.newsList,
//             qsTime: json.body.qsTime,
//             isNextPage: json.head.isNextPage,
//             pageNumber: json.head.thisPageNumber
//           });
//         else cb({}, true);
//       })
//       .catch(error => {
//         console.log(error);
//         cb({}, true);
//       });
//   });
// };

// export const getActiveScreeners = (token, ref, cb) => {
//   getFetchOptions(token, fetchOptions => {
//     setFirebaseAnalyticsEvent(ACTIVE_SCREENERS_URL, {});

//     fetch(ACTIVE_SCREENERS_URL, fetchOptions)
//       .then(response => {
//         return response.json();
//       })
//       .then(json => {
//         let header = json.head;
//         console.log("active screeners response", json);

//         if (header.status == 0) {
//           const screenerHeaders = json.body.alertHeaders;
//           const screenerData = json.body.alertData;
//           cb({
//             screeners: mapTableData(screenerHeaders, screenerData),
//             maxAllowedFreq: json.body.maxAllowedFreq,
//             allowedFreqList: json.body.allowedFreqList
//           });
//         } else {
//           cb({}, true);
//         }
//       })
//       .catch(error => {
//         console.log(error);
//         cb({}, true);
//       });
//   });
// };

// export const getScreenerAlertHistory = (screenId, token, cb) => {
//   getFetchOptions(token, fetchOptions => {
//     const body = { screenPk: screenId };
//     fetchOptions.body = JSON.stringify(body);
//     setFirebaseAnalyticsEvent(GET_SCREENER_ALERTS_HISTORY_URL, body);

//     fetch(GET_SCREENER_ALERTS_HISTORY_URL, fetchOptions)
//       .then(response => {
//         return response.json();
//       })
//       .then(json => {
//         let header = json.head;
//         console.log("active screeners response", json);

//         if (header.status == 0) {
//           const screenerHeaders = json.body.alertHeaders;
//           const screenerData = json.body.alertData;
//           cb({
//             history: mapTableData(screenerHeaders, screenerData)
//             // maxAllowedFreq: json.body.maxAllowedFreq,
//             // allowedFreqList: json.body.allowedFreqList
//           });
//         } else cb({}, true);
//       })
//       .catch(error => {
//         console.log(error);
//         cb({}, true);
//       });
//   });
// };

// export const getStockListCustomParams = (stockPkList, columns, token, cb) => {
//   getFetchOptions(token, fetchOptions => {
//     const body = { stockPkList: stockPkList, columns: columns };
//     fetchOptions.body = JSON.stringify(body);
//     setFirebaseAnalyticsEvent(GET_STOCK_CUSTOM_PARAMS_URL, body);

//     fetch(GET_STOCK_CUSTOM_PARAMS_URL, fetchOptions)
//       .then(response => {
//         return response.json();
//       })
//       .then(json => {
//         let header = json.head;
//         console.log("active screeners response", json);

//         if (header.status == 0) {
//           const stockHeaders = json.body.tableHeaders;
//           const stockData = json.body.tableData;
//           cb(mapTableData(stockHeaders, stockData));
//         } else cb([], true);
//       })
//       .catch(error => {
//         console.log(error);
//         cb([], true);
//       });
//   });
// };

mapTableData = (headers, data) => {
  let dataList = [];

  data.map((dataItem) => {
    let dataObj = {};
    dataItem.map((item, i) => {
      if (headers[i] != undefined) {
        let colName = "unique_name" in headers[i] ? headers[i].unique_name : headers[i].name;
        dataObj[colName] = item;
      }
    });

    dataList.push(dataObj);
  });
  return dataList;
};

export const getStockAlert = (baseURL, appToken, token, alertType, pageNumber, perPageCount, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_MY_ALERTS_URL;
      let requestBody = { alertType: alertType, pageNumber: pageNumber, perPageCount: perPageCount };

      fetchOptions.body = JSON.stringify(requestBody);
      setFirebaseAnalyticsEvent(URL, requestBody);

      fetch(URL, fetchOptions)
        .then((response) => {
          console.log("response===", response);
          return response.json();
        })
        .then((json) => {
          console.log("json", json);

          let header = json.head;
          if (header.status == 0) {
            const tableHeaders = json.body.tableHeaders;
            const tableData = json.body.tableData;
            cb({
              stock: mapTableData(tableHeaders, tableData),
              isNextPage: json.body.isNextPage,
              pageNumber: json.body.pageNumber,
              perPageCount: json.body.perPageCount,
            });
          } else cb(false, "Please try agin");
        })
        .catch((error) => {
          cb(false, "Please try again");
          console.log(error);
        });
    });
  }
};

mapTableData = (headers, data) => {
  let dataList = [];

  data.map((dataItem) => {
    let dataObj = {};
    dataItem.map((item, i) => {
      if (headers[i] != undefined) {
        let colName = "unique_name" in headers[i] ? headers[i].unique_name : headers[i].name;
        dataObj[colName] = item;
      }
    });

    dataList.push(dataObj);
  });
  return dataList;
};

export const getNotificationAlert = (baseURL, appToken, token, notificationType, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_MY_NOTIFICATIONS_URL;
      let requestBody = { notificationType: notificationType };

      fetchOptions.body = JSON.stringify(requestBody);
      setFirebaseAnalyticsEvent(URL, requestBody);
      console.log("Fetch requests==>", URL, "fetchOptions ==>", fetchOptions);
      fetch(URL, fetchOptions)
        .then((response) => {
          console.log(response);
          return response.json();
        })
        .then((json) => {
          console.log("json", json);

          let header = json.head;
          if (header.status == 0)
            cb({
              notifications: json.body.notificationsList,
              isNextPage: json.head.isNextPage,
            });
          else cb({}, true);
        })
        .catch((error) => {
          console.log(error);
          cb({}, true);
        });
    });
  }
};
