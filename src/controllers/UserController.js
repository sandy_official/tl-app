import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import firebase from "react-native-firebase";

import {
  GOOGLE_LOGIN_API_URL,
  FACEBOOK_LOGIN_API_URL,
  EMAIL_LOGIN_API_URL,
  USER_INFO_API_URL,
  USER_DASHBOARD_API_URL,
  LOGOUT_API_URL,
  EMAIL_SIGN_UP_API_URL,
} from "../utils/ApiEndPoints";
import { GoogleSignin } from "react-native-google-signin";
import { LoginManager } from "react-native-fbsdk";

/**
 * Email Login
 * @param {string} baseURL
 * @param {string} email
 * @param {string} password
 * @param {function} cb
 */
export const doLoginApi = (baseURL, appToken, email, password, cb) => {
  if (baseURL) {
    const URL = baseURL + EMAIL_LOGIN_API_URL;
    const body = { email: email, password: password };

    const fetchOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", TrendlyneAppToken: appToken },
      body: JSON.stringify(body),
    };

    setFirebaseAnalyticsEvent(URL, body);

    console.log("fetchOption", fetchOptions);

    fetch(URL, fetchOptions)
      .then((response) => response.json())
      .then((json) => {
        console.log("json", json);

        const header = json.head;
        if (header.status == 0) cb(json.body.key);
        else cb(null);
      })
      .catch((error) => {
        cb(null);
        console.log(error);
      });
  }
};

/**
 * Logout from an APP using token
 * @param {string} baseURL
 * @param {string} token
 * @param {function} cb
 */
export const doLogoutApi = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + LOGOUT_API_URL;
      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          cb(json);
          // let header = json.head;
          // if (header.status == 0) cb(true);
          // else cb("failed");
        })
        .catch((error) => {
          cb(null);
          console.log(error);
        });
    });
  }
};

async function onSignIn(user) {
  let email = "";
  let isSubscriber = "";
  let fullName = "";

  if (user) {
    userInfo = user.userInfo;
    email = userInfo.email;
    isSubscriber = userInfo.isSubscriber + "";
    fullName = userInfo.fullName;
  }
  await Promise.all([
    // firebase.analytics().setUserId(user.email),
    firebase.analytics().setUserProperty("email", email),
    firebase.analytics().setUserProperty("fullName", fullName),
    firebase.analytics().setUserProperty("isSubscriber", isSubscriber),

    firebase.crashlytics().setUserIdentifier(email),
    firebase.crashlytics().setUserName(fullName),
    firebase.crashlytics().setUserEmail(email),
    firebase.crashlytics().setStringValue("Subscriber", isSubscriber),
  ]);
}

/**
 * Get details of logged in user using token
 * @param {string} baseURL
 * @param {string} token
 * @param {function} cb
 */
export const getUserDetails = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + USER_INFO_API_URL;
      fetchOptions.method = "GET";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log("User details", json);
          let header = json.head;
          if (json.body) {
            try {
              onSignIn(json.body);
            } catch (e) {
              console.log(e);
            }
          }
          if (header.status == 0) cb(json.body.userInfo, header.status);
          else cb({}, header.status);
        })
        .catch((error) => {
          cb({}, null);
          console.log(error);
        });
    });
  }
};

/**
 * After Google login, get access token and call this function
 * @param {string} baseURL
 * @param {string} accessToken
 * @param {function} cb
 */
export const doGoogleLoginApi = (baseURL, appToken, accessToken, successCB, failureCB) => {
  if (baseURL) {
    const URL = baseURL + GOOGLE_LOGIN_API_URL;
    const body = { access_token: accessToken };

    const fetchOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", TrendlyneAppToken: appToken },
      body: JSON.stringify(body),
    };

    setFirebaseAnalyticsEvent(URL, {});

    fetch(URL, fetchOptions)
      .then((response) => response.json())
      .then((json) => {
        const header = json.head;
        if (header.status == 0) successCB(json.body.key);
        else failureCB(null);
      })
      .catch((error) => {
        failureCB(null);
        console.log(error);
      });
  }
};

/**
 * After Facebook login, get access token and call this function
 * @param {string} baseURL
 * @param {string} accessToken
 * @param {function} cb
 */
export const doFacebookLoginApi = (baseURL, appToken, accessToken, successCB, failureCB) => {
  if (baseURL) {
    const URL = baseURL + FACEBOOK_LOGIN_API_URL;
    const body = { access_token: accessToken };

    const fetchOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", TrendlyneAppToken: appToken },
      body: JSON.stringify(body),
    };

    setFirebaseAnalyticsEvent(URL, {});
    //  console.log("fetchOption", fetchOptions);

    fetch(URL, fetchOptions)
      .then((response) => response.json())
      .then((json) => {
        //  console.log("json", json);

        const header = json.head;
        if (header.status == 0) successCB(json.body.key);
        else failureCB(null);
      })
      .catch((error) => {
        failureCB(null);
        console.log(error);
      });
  }
};

/**
 * Sign up using an email Id
 * @param {string} baseURL
 * @param {string} email
 * @param {string} password1
 * @param {string} password2
 * @param {function} cb
 */
export const doEmailSignUp = (baseURL, appToken, email, password1, password2, cb) => {
  if (baseURL) {
    const URL = baseURL + EMAIL_SIGN_UP_API_URL;
    const body = { email: email, password1: password1, password2: password2 };

    const fetchOptions = {
      method: "POST",
      headers: { "Content-Type": "application/json", TrendlyneAppToken: appToken },
      body: JSON.stringify(body),
    };

    setFirebaseAnalyticsEvent(URL, {});
    console.log("fetchOption", fetchOptions);

    fetch(URL, fetchOptions)
      .then((response) => response.json())
      .then((json) => {
        console.log("json", json);

        const header = json.head;
        if (header.status == 0) cb(true, json.body.message);
        else cb(false, json.body.message);
      })
      .catch((error) => {
        cb(false, json.body.message);
        console.log(error);
      });
  }
};

/**
 * Get user dashboard detail
 * @param {string} baseURL
 * @param {string} token
 * @param {function} cb
 */
export const getUserDashboard = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + USER_DASHBOARD_API_URL;

      console.log("fetchOption", fetchOptions);

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log("dashboard json", json);

          const header = json.head;
          if (header.status == 0) cb(json.body);
          else cb({});
        })
        .catch((error) => {
          cb({});
          console.log(error);
        });
    });
  }
};

/**
 * Google sign in function
 */
export const doGoogleSignIn = async (baseURL, appToken, googleKey, successCB, failureCB) => {
  try {
    /** Google sign in configuration */
    GoogleSignin.configure({ webClientId: googleKey, offlineAccess: true, iosClientId: googleKey });

    // clear previous session
    let isSignedIn = await GoogleSignin.isSignedIn();
    if (isSignedIn) await GoogleSignin.signOut();

    await GoogleSignin.hasPlayServices();

    const userInfo = await GoogleSignin.signIn();
    console.log("Google sign in", userInfo);

    const { accessToken } = await GoogleSignin.getTokens();
    console.log("Google sign token", accessToken);

    if (accessToken) {
      doGoogleLoginApi(baseURL, appToken, accessToken, successCB, failureCB);
    } else failureCB("");
  } catch (e) {
    failureCB(e);
  }
};

/**
 * Facebook sign in function
 */
export const doFacebookLogin = async (baseURL, appToken, successCB, failureCB) => {
  LoginManager.logOut(); // to clear previous session

  LoginManager.logInWithPermissions(["public_profile", "email"]).then(
    (result) => {
      if (!result.isCancelled) {
        AccessToken.getCurrentAccessToken().then((data) => {
          const { accessToken } = data;

          if (accessToken) {
            doFacebookLoginApi(baseURL, appToken, accessToken, successCB, failureCB);
          } else failureCB("");
        });
      } else failureCB("");
    },
    (error) => {
      failureCB(error);
    }
  );
};
