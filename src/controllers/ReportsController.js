import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { REPORTS_BY_BROKER_URL, REPORTS_BY_TYPE_URL, RESEARCH_REPORT_BROKER_LIST_URL } from "../utils/ApiEndPoints.js";

export const getResearchReportsByType = async (baseURL, appToken, token, rType, pageNumber, perPageCount, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + REPORTS_BY_TYPE_URL;
      const body = { rtype: rType, pageNumber: pageNumber, perPageCount: perPageCount };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          // console.log("reports json", json);

          if (header.status == 0) cb(mapResearchReportData(json.body));
          else cb({ reports: [], charts: {} });
        })
        .catch((error) => {
          // console.log(error);
          cb({ reports: [], charts: {} });
        });
    });
  }
};

export const getResearchReportByBroker = (baseURL, appToken, token, brokerName, pageNumber, perPageCount, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + REPORTS_BY_BROKER_URL;
      const body = { brokerName: brokerName, pageNumber: pageNumber, perPageCount: perPageCount };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      // console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Broker report", json);

          if (header.status == 0) cb(mapResearchReportData(json.body));
          else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};

export const getBrokerSearchList = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + RESEARCH_REPORT_BROKER_LIST_URL;
      fetchOptions.method = "GET";
      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          // console.log(json);

          if (header.status == 0) cb(json.body.brokerListData);
          else cb([]);
        })
        .catch((error) => {
          console.log(error);
          cb([]);
        });
    });
  }
};

mapResearchReportData = (json) => {
  let reportList = [];
  let chartDataList = {};
  let columnList = json.tableHeaders;

  const pageNumber = json.pageNumber ? json.pageNumber : 0;
  const isNextPage = json.isNextPage ? json.isNextPage : false;

  if ("chartData" in json) {
    const chartData = JSON.parse(JSON.stringify(json.chartData));
    if ("mostBuys" in chartData) chartDataList["Most Buys"] = chartData.mostBuys;
    if ("mostSells" in chartData) chartDataList["Most Sells"] = chartData.mostSells;
    if ("mostReports" in chartData) chartDataList["Most Reports"] = chartData.mostReports;
  }

  json.tableData.forEach((superstar) => {
    let report = {};

    superstar.forEach((item, i) => {
      report[columnList[i].unique_name] = item;
    });

    reportList.push(report);
  });

  return {
    reports: reportList,
    charts: chartDataList,
    pageNumber: pageNumber,
    isNextPage: isNextPage,
  };
};
