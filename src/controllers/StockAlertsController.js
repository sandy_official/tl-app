import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { ACTIVE_STOCK_ALERTS_URL, ADD_STOCK_ALERT_URL, DELETE_STOCK_ALERT_URL } from "../utils/ApiEndPoints";

export const setStockAlert = (baseURL, appToken, token, action, stockId, alertType, alertPeriod, threshold, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = action == "add" ? baseURL + ADD_STOCK_ALERT_URL : baseURL + DELETE_STOCK_ALERT_URL;

      const body = {
        stock_id: stockId,
        alertType: alertType,
        alertPeriod: alertPeriod,
        threshold: threshold,
      };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      console.log(fetchOptions);
      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Stock Alerts", json);
          if (header.status == 0) cb(header.statusDescription);
          else cb(header.statusDescription);
        })
        .catch((error) => {
          console.log(error);
          cb("");
        });
    });
  }
};

export const getActiveStockAlerts = (baseURL, appToken, token, stockId, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      // fetchOptions.method = "GET";
      const URL = baseURL + ACTIVE_STOCK_ALERTS_URL;
      const body = { stock_id: stockId };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          // console.log("Active alerts", response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Active alerts", json);
          if (header.status == 0) cb(json.body);
          else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};
