import AsyncStorage from "@react-native-community/async-storage";

const TOKEN_KEY = "csrftoken";
const COOKIE_KEY = "trendlyneCookie";

export const extractToken = response => {
  console.log("Saving token");

  for (const [name, value] of response.headers) {
    if (name === "set-cookie") {
      setCookie(value);

      for (var i = 0; i < value.split(";").length; i++) {
        if (value.split(";")[i].indexOf("csrftoken") !== -1) {
          return value.split(";")[i].split("=")[1];
        }
      }
    }
  }
};

export const setCookie = cookie => {
  console.log("Saving cookies");

  try {
    AsyncStorage.setItem(COOKIE_KEY, cookie);
    console.log("cookie saved");
  } catch (error) {
    console.log(error);
    console.log("Error in saving cookie");
  }
};

export const storeToken = async token => {
  try {
    AsyncStorage.setItem(TOKEN_KEY, token);
    console.log("CSRF token saved");
  } catch (error) {
    console.log(error);
    console.log("Error in saving csrf token");
  }
};

export const getToken = async cb => {
  try {
    await AsyncStorage.getItem(TOKEN_KEY).then(value => cb(value));
  } catch (error) {
    console.log("Error in getting csrf token");
    console.log(error);
    return null;
  }
};

export const getCookie = async cb => {
  try {
    await AsyncStorage.getItem(COOKIE_KEY).then(value => cb(value));
  } catch (error) {
    console.log("Error in getting csrf token");
    console.log(error);
    return null;
  }
};

/**
 * To get object list from Async storage
 * @param {String key}
 * @param {callback function(list of objects)}
 */
export const getObjectListFromAsync = async (uniqueKey, cb) => {
  try {
    await AsyncStorage.getItem(uniqueKey).then(objectList => {
      // console.log("get : " + objectList);
      typeof objectList !== null ? cb(JSON.parse(objectList)) : cb([]);
    });
  } catch (error) {
    console.log("Error in getting object list");
    console.log(error);
    cb([]);
  }
};

/**
 * Save custom object list in Async storage
 * @param {String key}
 * @param {CustomObject object}
 */
export const storeObjectListInAsync = async (uniqueKey, object) => {
  let list = [];

  try {
    await getObjectListFromAsync(uniqueKey, objectList => {
      console.log(objectList);

      if (objectList != null) {
        list = objectList;
        list.splice(0, 0, object);

        if (list.length > 5) list.pop();
      } else {
        list.push(object);
      }

      // console.log(object);
      AsyncStorage.setItem(uniqueKey, JSON.stringify(list));
      // console.log("object saved");
    });
  } catch (error) {
    console.log(error);
    console.log("Failed to save object");
  }
};

/**
 * To get objects from Async storage
 * @param {String key}
 * @param {callback function(list of objects)}
 */
export const getObjectsFromAsync = async (asyncKey, cb) => {
  try {
    await AsyncStorage.getItem(asyncKey).then(objectList => {
      // console.log("get : " + objectList);
      objectList != null ? cb(JSON.parse(objectList)) : cb({});
    });
  } catch (error) {
    console.log("Error in getting objects");
    console.log(error);
    cb({});
  }
};

/**
 * Save custom objects in Async storage
 * @param {String key}
 * @param {CustomObject object}
 */
export const storeObjectsInAsync = async (asyncKey, object) => {
  try {
    await getObjectListFromAsync(asyncKey, objectList => {
      AsyncStorage.setItem(asyncKey, JSON.stringify(object));
      // console.log("object saved : " + objectList);
    });
  } catch (error) {
    console.log(error);
  }
};

/**
 * Clear all entries from Async storage
 */
export const clearAsync = () => {
  try {
    AsyncStorage.clear(error => console.log(error));
  } catch (error) {
    console.log(error);
  }
};

export const storeKeyValue = async (asyncKey, value) => {
  try {
    await AsyncStorage.setItem(asyncKey, value);
    console.log("value saved in async storage");
  } catch (error) {
    console.log(error);
  }
};

export const getKeyValue = async (asyncKey, cb) => {
  try {
    await AsyncStorage.getItem(asyncKey).then(value => cb(value));
  } catch (error) {
    console.log(error);
    return null;
  }
};
