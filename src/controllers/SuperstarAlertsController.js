import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { GET_ACTIVE_SUPERSTAR_ALERTS_URL, SET_SUPERSTAR_ALERTS } from "../utils/ApiEndPoints";

export const setSuperstarAlerts = (baseURL, appToken, token, alertType, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SET_SUPERSTAR_ALERTS;
      const body = { alert: alertType };

      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log(json);
          let header = json.head;
          if (header.status == 0) cb(header.statusDescription);
          else if (header.status == 2 && header.statusDescription == "Not Authorized / Subscribe to add alert")
            cb("subscribe");
          else cb(header.statusDescription);
        })
        .catch((error) => {
          console.log(error);
          cb("");
        });
    });
  }
};

export const getSuperstarAlerts = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_ACTIVE_SUPERSTAR_ALERTS_URL;
      fetchOptions.method = "GET";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log("superstar alerts", json);
          let header = json.head;
          if (header.status == 0) cb(json.body.activeAlert);
          else cb("");
        })
        .catch((error) => {
          // console.log(error);
          cb("");
        });
    });
  }
};
