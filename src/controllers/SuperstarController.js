import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { HOLDING_LOOKUP_URL } from "../utils/ApiEndPoints";

// export const getIndividualData = (segment, cb) => {
//   getFetchOptions(fetchOptions => {
//     fetchOptions.body = JSON.stringify({ segment: segment });

//     fetch(SUPERSTAR_INDIVIDUAL_URL, fetchOptions)
//       .then(response => {
// checkCookies(response);
//         return response.json();
//       })
//       .then(json => {
//         let header = json.head;
//         // console.log(json);

//         if (header.status == 0)
//           cb(mapIndividualData(json.body));
//         else cb([]);
//       })
//       .catch(error => {
//         console.log(error);
//         cb([]);
//       });
//   });
// };

// export const getSuperstarPortfolio = async (baseURL, appToken,  token, superstarId, quarter, searchString, cb) => {
//   if (baseURL) {
//     await getFetchOptions( appToken, token, fetchOptions => {
//       const URL = baseURL + SUPERSTAR_PORTFOLIO_URL;
//       const body = { QuarterString: quarter };

//       if (searchString.length > 0) body.searchString = searchString;
//       else body.superstarId = superstarId;

//       fetchOptions.body = JSON.stringify(body);
//       setFirebaseAnalyticsEvent(URL, body);
//       console.log(fetchOptions);

//       fetch(URL, fetchOptions)
//         .then(response => response.json())
//         .then(json => {
//           let header = json.head;
//           console.log(json);

//           if (header.status == 0) cb(mapSuperstarPortfolioData(json.body));
//           else cb({});
//         })
//         .catch(error => {
//           console.log(error);
//           cb([]);
//         });
//     });
//   }
// };

// export const getSuperstarBulkDeals = async (baseURL, appToken,  token, superstarId, searchString, cb) => {
//   if (baseURL) {
//     await getFetchOptions( appToken, token, fetchOptions => {
//       const URL = baseURL + SUPERSTAR_BB_DEALS_URL;
//       const body = {};

//       if (searchString && searchString.length > 0) body.searchString = searchString;
//       // else if (superstarId == -1) body.searchString = searchString;
//       else body.superstarId = superstarId;

//       fetchOptions.body = JSON.stringify(body);
//       setFirebaseAnalyticsEvent(URL, body);

//       console.log("fetch options", fetchOptions);

//       fetch(URL, fetchOptions)
//         .then(response => response.json())
//         .then(json => {
//           let header = json.head;
//           console.log(json);

//           if (header.status == 0) cb({ stocks: mapStocksData(json.body) });
//           else cb({ stocks: [] });
//         })
//         .catch(error => {
//           console.log(error);
//           cb({ stocks: [] });
//         });
//     });
//   }
// };

// export const getSuperstarInsiderTrades = async (baseURL,  appToken, token, superstarId, searchString, cb) => {
//   await getFetchOptions( appToken, token, fetchOptions => {
//     const URL = baseURL + SUPERSTAR_IT_SAST_DEALS_URL;
//     const body = {};

//     if (searchString && searchString.length > 0) body.searchString = searchString;
//     // else if (superstarId == -1) body.searchString = searchString;
//     else body.superstarId = superstarId;

//     fetchOptions.body = JSON.stringify(body);
//     setFirebaseAnalyticsEvent(URL, body);

//     fetch(URL, fetchOptions)
//       .then(response => response.json())
//       .then(json => {
//         let header = json.head;
//         console.log(json);

//         if (header.status == 0) cb({ stocks: mapStocksData(json.body) });
//         else cb({ stocks: [] });
//       })
//       .catch(error => {
//         console.log(error);
//         cb({ stocks: [] });
//       });
//   });
// };

export const getSuperstarStockHolding = async (baseURL, appToken, token, superstarId, stockId, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + HOLDING_LOOKUP_URL;
      const body = { superstarId: superstarId, stock_id: stockId };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);
      // console.log("stock", stockId);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          // console.log(json);

          if (header.status == 0) cb(mapStocksData(json.body));
          else cb([]);
        })
        .catch((error) => {
          console.log(error);
          cb([]);
        });
    });
  }
};

// export const getSuperstarList = async (baseURL, token, cb) => {
//   if (baseURL) {
//     await getFetchOptions(token, fetchOptions => {
//       const URL = baseURL + GET_SUPERSTAR_LIST;
//       const body = { segment: "all" };

//       fetchOptions.body = JSON.stringify(body);
//       setFirebaseAnalyticsEvent(URL, body);

//       fetch(URL, fetchOptions)
//         .then(response => response.json())
//         .then(json => {
//           let header = json.head;
//           // console.log(json);

//           if (header.status == 0) cb(mapSuperstarList(json.body));
//           else cb({});
//         })
//         .catch(error => {
//           console.log(error);
//           cb({});
//         });
//     });
//   }
// };

mapIndividualData = (json) => {
  let superstarList = [];

  let columnList = json.tableHeaders;
  let stockList = [];

  json.tableData.forEach((superstar) => {
    let superstarObj = {};

    superstar.forEach((item, i) => {
      superstarObj[columnList[i].name] = item;
      // superstarObj[columnList[i].unique_name] = item;
    });

    superstarList.push(superstarObj);
  });
  // console.log(superstarList)
  return superstarList;
};

mapSuperstarPortfolioData = (json) => {
  // let barChartColumns = json.chartData.increase[0]
  let holdingIncrease = [];
  let holdingDecrease = [];

  if (json.chartData) {
    holdingIncrease = json.chartData.increase ? json.chartData.increase : [];
    holdingDecrease = json.chartData.decrease ? json.chartData.decrease : [];
  }
  let netWorthData = json.netWorthData ? json.netWorthData : [];

  holdingIncrease.shift();
  holdingDecrease.shift();
  netWorthData.shift();

  return {
    stocks: mapStocksData(json),
    barChart: { increase: holdingIncrease, decrease: holdingDecrease },
    lineChart: netWorthData,
    dateList: json.dateList,
    quarterString: json.quarterString,
    totalNetWorth: json.totalNetWorth,
  };
};

mapStocksData = (json) => {
  let columnList = [];
  let stockList = [];
  try {
    if (json.tableHeaders) {
      json.tableHeaders.forEach((column, index) => {
        columnList.push(column.unique_name ? column.unique_name : column.name);
      });
    } else {
      json.tableHeaders.forEach((column, index) => {
        columnList.push(column.unique_name ? column.unique_name : column.name);
      });
    }

    json.tableData.forEach((stock) => {
      let stockObj = {};

      stock.forEach((item, i) => {
        stockObj[columnList[i]] = item;
      });

      stockList.push(stockObj);
    });
  } catch (error) {
    console.log("Error");
  }
  // console.log(stockList)
  return stockList;
};

mapSuperstarList = (json) => {
  let columnList = [];
  let superstarList = {};

  json.tableHeaders.forEach((column) => {
    columnList.push(column.name);
  });

  Object.keys(json.tableData).forEach((key) => {
    json.tableData[key].forEach((superstar) => {
      let obj = {};

      superstar.forEach((item, i) => {
        obj[columnList[i]] = item;
      });

      if (!superstarList[key]) superstarList[key] = [];

      superstarList[key].push(obj);
    });
  });

  return superstarList;
};
