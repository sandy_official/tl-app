import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import {
  MY_SCREENERS_URL,
  BOOKMARKED_SCREENER_URL,
  SCREENERS_BY_TAG_URL,
  GET_ACTIVE_SCREENER_ALERTS_URL,
  SCREENER_BOOKMARK_STATUS_URL,
  ADD_SCREENER_BOOKMARK_URL,
  DELETE_SCREENER_BOOKMARK_URL,
  SCREENER_STOCKS_URL,
  SCREENER_WATCHLIST_STOCKS_URL,
  MODIFY_SCREENER_ALERTS_URL,
} from "../utils/ApiEndPoints";

export const getMyScreeners = (baseURL, appToken, token, pageNumber, pageCount, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + MY_SCREENERS_URL;
      const body = { perPageCount: pageCount, pageNumber: pageNumber };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          console.log(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          // console.log("My Screeners", json);

          if (header.status == 0 && header.statusDescription == "Success")
            cb(json.body.data, json.body.isNextPage, header.status);
          else cb([], true, header.status);
        })
        .catch((error) => {
          b([], true, null);
          console.log(error);
        });
    });
  }
};

export const getScreenersByTag = (
  baseURL,
  appToken,
  token,
  searchQuery,
  tag,
  pageNumber,
  pageCount,
  success,
  failure
) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SCREENERS_BY_TAG_URL;
      let body = { searchString: searchQuery, perPageCount: pageCount, pageNumber: pageNumber };

      if (searchQuery) body.searchString = searchQuery;
      else body.tag = tag;

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Screener by tag", json);

          if (header.status == 0) success(json.body.data, json.body.isNextPage);
          else failure(header.status, header.statusDescription);
        })
        .catch((error) => {
          console.log(error);
          failure(-1, "Something went wrong");
        });
    });
  }
};

export const getScreenerData = (
  baseURL,
  appToken,
  token,
  pk,
  groupType,
  groupName,
  perPageCount,
  pageNumber,
  filterId,
  sortBy,
  order,
  isWatchlist,
  success,
  failure
) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = isWatchlist ? baseURL + SCREENER_WATCHLIST_STOCKS_URL : baseURL + SCREENER_STOCKS_URL;
      const body = {
        screenpk: pk,
        groupType: groupType,
        groupName: groupName,
        perPageCount: perPageCount,
        pageNumber: pageNumber,
        filterListId: filterId == 0 ? undefined : filterId,
        sortBy: sortBy ? sortBy : undefined,
        order: order ? order : undefined,
      };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          console.log(json);

          if (header.status == 0) success(mapScreenerData(json.body));
          else {
            console.log("Something went wrong", header.statusDescription);
            failure(header.status, header.statusDescription);
          }
        })
        .catch((error) => {
          console.log(error);
          failure(-1, "Something went wrong");
        });
    });
  }
};

mapIndividualData = (json) => {
  let columnList = json.tableHeaders;
  let superstarList = [];
  let stockList = [];

  // // columns
  // json.tableHeaders.forEach((column, index) => {
  //   columnList.push(parseTableColumns(column));
  // });

  json.tableData.forEach((superstar) => {
    let superstarObj = {};

    superstar.forEach((item, i) => {
      superstarObj[columnList[i].name] = item;
      // superstarObj[columnList[i].unique_name] = item;
    });

    superstarList.push(superstarObj);
  });
  // console.log(superstarList)
  return superstarList;
};

mapScreenerData = (json) => {
  let stockList = [];
  let columnList = json.tableHeaders;

  let screenData = json.screen ? json.screen : {};
  // json.tableColumns.forEach((column, index) => {
  //   columnList.push(parseTableColumns(column));
  // });

  // stock details
  json.tableData.forEach((stockIndex) => {
    let stockObj = [];
    let stockArray = [];

    console.log("columns data", stockIndex.length);
    let dvmValueCrossed = false; // to get data which is after dvm in json object

    // console.log("Mapping screeners", columnList);
    stockIndex.forEach((item, i) => {
      // let column = columnList[i];
      // let column = JSON.parse(JSON.stringify(columnList[i]));
      let column = Object.assign({}, columnList[i]);
      let columnName = column.unique_name ? column.unique_name : column.name;

      if (dvmValueCrossed) {
        column["value"] = item;
        stockArray.push(column);
      }

      if (column.unique_name == "m_color") dvmValueCrossed = true;

      stockObj[columnName] = item;
    });

    stockList.push({ obj: stockObj, array: stockArray });
  });

  // console.log(stockList);

  return {
    isNextPage: json.isNextPage,
    order: json.order,
    description: json.description,
    title: json.title,
    sortBy: json.sortBy,
    pageNumber: json.thisPageNumber,
    pageCount: json.thisPageCount,
    stocks: stockList,
    screen: screenData,
  };
};

// Bookmark api implementation
export const getBookmarkedScreeners = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + BOOKMARKED_SCREENER_URL;
      fetchOptions.method = "GET";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Bookmarked Screeners", json);

          if (header.status == 0) cb(json.body.data, false, header.status);
          else cb({}, true, header.status);
        })
        .catch((error) => {
          cb({}, true, header.status);
          console.log(error);
        });
    });
  }
};

export const getScreenerBookmarkStatus = (baseURL, appToken, token, bookmarkId, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SCREENER_BOOKMARK_STATUS_URL;
      const body = { bookmarkIds: [bookmarkId] };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;

          // console.log("Screener status ", json);

          if (header.status == 0 && header.statusDescription == "Success") {
            const body = json.body;
            const data = [];

            const tableHeaders = body.tableHeaders;
            const tableData = body.tableData;
            const headerItems = [];

            tableHeaders.map((header) => headerItems.push(header.unique_name));

            tableData.map((item) => {
              let newItem = {};
              newItem[headerItems[0]] = item[0];
              newItem[headerItems[1]] = item[1];

              data.push(newItem);
            });
            cb(data);
          } else cb(false);
        })
        .catch((error) => {
          cb(false);
          console.log(error);
        });
    });
  }
};

export const addRemoveScreenerBookmark = (baseURL, appToken, token, action, bookmarkId, cb) => {
  getFetchOptions(appToken, token, (fetchOptions) => {
    const body = {
      bookmarkId: bookmarkId,
    };

    fetchOptions.body = JSON.stringify(body);

    const URL = action == "add" ? baseURL + ADD_SCREENER_BOOKMARK_URL : baseURL + DELETE_SCREENER_BOOKMARK_URL;

    setFirebaseAnalyticsEvent(URL, body);

    // console.log("action", action);
    // console.log("bookmark id", bookmarkId);
    // console.log(fetchOptions);
    // console.log("Bookmark URL ", URL);

    fetch(URL, fetchOptions)
      .then((response) => {
        // checkCookies(response);
        return response.json();
      })
      .then((json) => {
        let header = json.head;

        console.log("Screener add/remove bookmark ", json);

        if (header.status == 0) {
          cb(true);
        } else cb(false);
      })
      .catch((error) => {
        cb(false);
        console.log(error);
      });
  });
};

// Screener Alerts
export const getScreenerAlerts = (baseURL, appToken, token, screenerId, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      // fetchOptions.method = "GET";
      const URL = baseURL + GET_ACTIVE_SCREENER_ALERTS_URL;
      const body = { screenerId: screenerId };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log(json);

          if (header.status == 0) {
            // console.log("Screener status ", json);
            const body = json.body;
            const alertsData = [];

            const tableHeaders = body.alertHeaders;
            const tableData = body.alertData;

            tableData.map((item, i) => {
              const header = tableHeaders[i].unique_name;
              alertsData[header] = item;
            });

            cb({
              alertsData: alertsData,
              maxAllowedFreq: body.maxAllowedFreq,
              allowedFreq: body.allowedFreqList,
            });
          } else cb({}, true);
        })
        .catch((error) => {
          cb({}, true);
          console.log(error);
        });
    });
  }
};

//If frequency is null delete alert
export const modifyScreenerAlert = (baseURL, appToken, token, screenerId, frequency, cb) => {
  getFetchOptions(appToken, token, (fetchOptions) => {
    let URL = baseURL + MODIFY_SCREENER_ALERTS_URL;

    const body = { screenerId: screenerId };
    if (frequency) body.frequency = frequency;
    fetchOptions.body = JSON.stringify(body);
    setFirebaseAnalyticsEvent(URL, body);

    console.log(fetchOptions);

    fetch(URL, fetchOptions)
      .then((response) => {
        // checkCookies(response);
        return response.json();
      })
      .then((json) => {
        let header = json.head;

        console.log("screener alert modify ", json);

        if (header.status == 0) {
          cb(true);
        } else cb(false);
      })
      .catch((error) => {
        cb(false);
        console.log(error);
      });
  });
};
