import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import {
  SEND_TRANSACTION_SUCCESS_RESPONSE_URL,
  START_TRANSACTION_URL,
  GET_SUBSCRIPTION_CUSTOMER_INFO_URL,
  SET_SUBSCRIPTION_CUSTOMER_INFO_URL,
  SUBSCRIPTION_PLAN_URL,
} from "../utils/ApiEndPoints";

export const getSubscriptionPlans = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SUBSCRIPTION_PLAN_URL;
      // if (action)
      //   fetchOptions.body = JSON.stringify({ stock_id: stockId, action: action });
      // else fetchOptions.body = JSON.stringify({ stock_id: stockId });

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("plans", json);

          if (header.status == 0) cb(true, json.body);
          else cb(false, {});
        })
        .catch((error) => {
          cb(false, {});
          console.log(error);
        });
    });
  }
};

export const getCustomerInfo = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_SUBSCRIPTION_CUSTOMER_INFO_URL;
      // fetchOptions.headers.KEY = "ff2e617300efc16e45f0eee7ead397c3e251a420";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          console.log("get customer info", json);

          let header = json.head;
          if (header.status == 0) cb(json.body.customerInfo);
          else cb({});
        })
        .catch((error) => {
          cb({});
          console.log(error);
        });
    });
  }
};

export const setCustomerInfo = (
  baseURL,
  appToken,
  token,
  name,
  contactNumber,
  email,
  state,
  country,
  address,
  pinCode,
  gstNumber,
  cb
) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SET_SUBSCRIPTION_CUSTOMER_INFO_URL;
      // fetchOptions.headers.KEY = "ff2e617300efc16e45f0eee7ead397c3e251a420";
      const body = {
        Name: name,
        "Contact Number": contactNumber,
        Email: email,
        State: state,
        Country: country,
        Address: address,
        "PIN Code": pinCode,
        "GST Number": gstNumber,
      };

      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);

      console.log(fetchOptions);
      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("set customer info", json);

          if (header.status == 0) cb(true);
          else cb(false, "Please Check inputs and try again");
        })
        .catch((error) => {
          cb(false);
          console.log(error);
        });
    });
  }
};

export const startPaymentTransaction = (
  baseURL,
  appToken,
  token,
  planName,
  paymentMethod,
  planVariant,
  discountPercent,
  couponCode,
  cb
) => {
  getFetchOptions(appToken, token, (fetchOptions) => {
    const URL = baseURL + START_TRANSACTION_URL;
    const body = {
      plan_name: planName,
      payment_processor: "razorpay",
      payment_method: paymentMethod,
      plan_variant: planVariant,
      discountedPer: discountPercent,
      coupon_code: couponCode,
    };

    fetchOptions.body = JSON.stringify(body);

    // fetchOptions.headers.KEY = "ff2e617300efc16e45f0eee7ead397c3e251a420";

    setFirebaseAnalyticsEvent(URL, body);

    console.log("Start transaction:", URL, fetchOptions);

    fetch(URL, fetchOptions)
      .then((response) => {
        return response.json();
      })
      .then((json) => {
        let header = json.head;
        console.log("Start transaction response", json);

        if (header.status == 0) cb(true, json.body);
        else cb(false, {});
      })
      .catch((error) => {
        cb(false, {});
        console.log(error);
      });
  });
};

export const sendTransactionSuccessResponse = (
  baseURL,
  appToken,
  token,
  orderId,
  paymentId,
  razorpaySignature,
  subscriptionId,
  cb
) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SEND_TRANSACTION_SUCCESS_RESPONSE_URL;
      const body = {
        razorpay_order_id: orderId,
        razorpay_payment_id: paymentId,
        razorpay_signature: razorpaySignature,
      };

      if (subscriptionId) body.razorpay_subscription_id = subscriptionId;
      // razorpay_signature: razorpaySignature,

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      // fetchOptions.headers.KEY = "ff2e617300efc16e45f0eee7ead397c3e251a420";

      console.log("send success response options", fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          console.log("send success raw response:", response);

          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Send success transaction response", json);

          if (header.status == 0) cb(true, json.body);
          else cb(false, {});
        })
        .catch((error) => {
          cb(false, {});
          console.log(error);
        });
    });
  }
};
