import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { CONTACT_URL } from "../utils/ApiEndPoints";

export const getContactUsForm = (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + CONTACT_URL;
      fetchOptions.method = "GET";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("contact us", json);
          if (header.status == 0) cb(json.body.form);
          else cb({});
        })
        .catch((error) => {
          cb({});
          console.log(error);
        });
    });
  }
};

export const setContactUsForm = (baseURL, appToken, token, name, email, contactNumber, message, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + CONTACT_URL;
      let requestBody = {
        Name: name,
        Email: email,
        Subject: "App Support",
        Message: message,
      };

      if (contactNumber) requestBody.Phone = !isNaN(contactNumber) ? parseInt(contactNumber) : "";

      fetchOptions.body = JSON.stringify(requestBody);
      setFirebaseAnalyticsEvent(URL, requestBody);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          // console.log("json", fetchOptions, json);

          let header = json.head;
          if (header.status == 0) cb(true, header.statusDescription);
          else cb(false, "Please try agin");
        })
        .catch((error) => {
          cb(false, "Please try again");
          console.log(error);
        });
    });
  }
};
