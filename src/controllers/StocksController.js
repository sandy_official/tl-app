import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import {
  SEARCH_API_URL,
  STOCK_BOARD_MEETINGS_URL,
  STOCK_BONUS_SPLIT_URL,
  STOCK_DIVIDEND_URL,
  GET_STOCK_BULK_BLOCK_DEALS_URL,
  GET_STOCK_INSIDER_TRADES_DEALS_URL,
  STOCK_FUNDAMENTAL_URL,
  CORPORATE_ANNOUNCEMENTS_NEWSFEED_URL,
  BROKER_NEWSFEED_URL,
  STOCK_NEWSFEED_URL,
  STOCK_OHLC_CHART_URL,
  RESEARCH_REPORTS_BY_STOCK_URL,
  STOCK_SHAREHOLDING_URL,
  STOCK_TECHNICAL_ANALYSIS_URL,
  STOCK_HOLDING_HISTORY_URL,
  STOCK_OVERVIEW_URL,
  CANDLESTICK_DESCRIPTION_URL,
} from "../utils/ApiEndPoints";

//News api's

export const searchStockByTerm = async (baseURL, appToken, token, query, sType, cb) => {
  if(!sType){
    sType = "stock";
  }
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + SEARCH_API_URL;
      const body = { searchType: sType, term: query };

      fetchOptions.body = JSON.stringify(body);
      // console.log(fetchOptions);

      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          // console.log(json);
          let header = json.head;
          let body = json.body;

          if (header.status == 0) {
            let stockList = [];
            if(["stock", "all"].indexOf(sType) !== -1 && body.stockHeaders){
                let stockColumnList = [];
                body.stockHeaders.map((item) => stockColumnList.push(item));

                body.stockData.forEach((stock) => {
                  let stockObj = {};

                  stock.forEach((item, i) => {
                    stockObj[stockColumnList[i].unique_name] = item;
                  });

                  stockList.push(stockObj);
                });
            } 
            if(["mf", "all"].indexOf(sType) !== -1 && body.mfHeaders) {
                let mfColumnList = [];
                body.mfHeaders.map((item) => mfColumnList.push(item));

                body.mfData.forEach((mf) => {
                   let mfObj = {};

                   mf.forEach((item, i) => {
                       mfObj[mfColumnList[i].unique_name] = item;
                   });
                   stockList.push(mfObj);
                });
            }
            cb(stockList);
          } else cb([]);
        })
        .catch((error) => {
          console.log(error);
          cb([]);
        });
    });
  }
};

getApiEndpointById = (id) => {
  switch (id) {
    case "Dividend":
      return STOCK_DIVIDEND_URL;
    case "Bonus":
    case "Split":
      return STOCK_BONUS_SPLIT_URL;
    case "Board Meetings":
      return STOCK_BOARD_MEETINGS_URL;
  }
};

export const getCorporateActions = async (baseURL, appToken, token, stockId, identifier, action, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + getApiEndpointById(identifier);
      const body = { stock_id: stockId };

      if (action) body.action = action;

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);
      // console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;

          if (header.status == 0) cb(mapCorporateActionData(json.body));
          else cb({});
        })
        .catch((error) => console.log(error));
    });
  }
};
// export const getStockDeals = (stockId, stockCode, cb) => {
export const getStockDeals = async (baseURL, appToken, token, stockId, type, cb) => {
  await getFetchOptions(appToken, token, (fetchOptions) => {
    let URL = type == "sast" ? baseURL + GET_STOCK_INSIDER_TRADES_DEALS_URL : baseURL + GET_STOCK_BULK_BLOCK_DEALS_URL;
    const body = { stock_id: stockId };

    fetchOptions.body = JSON.stringify(body);

    setFirebaseAnalyticsEvent(URL, body);

    fetch(URL, fetchOptions)
      .then((response) => {
        // checkCookies(response);
        return response.json();
      })
      .then((json) => {
        let header = json.head;
        console.log(json);

        if (header.status == 0) cb(mapCorporateActionData(json.body));
        else cb({});
      })
      .catch((error) => {
        console.log(error);
        cb({});
      });
  });
};

mapCorporateActionData = (json) => {
  let stockObj = {};
  let meetingList = [];
  let insights = json.insight ? json.insight : [];

  let stockColumnList = json.stockHeaders;
  let tableColumnList = json.tableHeaders;
  //   let stockList = [];

  json.stockData.forEach((stockValue, i) => {
    stockObj[stockColumnList[i].unique_name] = stockValue;
  });

  json.tableData.forEach((meeting) => {
    let meetingObj = {};

    meeting.forEach((item, i) => {
      meetingObj[tableColumnList[i].unique_name] = item;
    });
    meetingList.push(meetingObj);
  });
  // console.log(superstarList)
  return { stock: stockObj, table: meetingList, insight: insights };
};

export const getStockNews = (baseURL, appToken, token, stockId, pageNumber, pageCount, type, qsTime, pageType, stockCode, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const body = {
        stock_id: stockId,
        perPageCount: pageCount,
        pageNumber: pageNumber,
        qsTime: qsTime ? qsTime : new Date().getTime(),
      };
      if (pageType == "index") {
        body.stockGroupType = pageType
        body.stockGroupId = stockCode
      }
      fetchOptions.body = JSON.stringify(body);

      let URL = "";
      if (type == "News") URL = baseURL + STOCK_NEWSFEED_URL;
      else if (type == "Reports") URL = baseURL + BROKER_NEWSFEED_URL;
      else URL = baseURL + CORPORATE_ANNOUNCEMENTS_NEWSFEED_URL;

      setFirebaseAnalyticsEvent(URL, body);

      console.log("news fetch", fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          console.log("news response before parsing", response);
          return response.json();
        })
        .then((json) => {
          console.log("news response", json);
          let header = json.head;

          if (header.status == 0)
            cb({
              news: json.body.newsList,
              qsTime: json.body.qsTime,
              isNextPage: json.head.isNextPage,
              pageNumber: json.head.thisPageNumber,
            });
          else cb({}, true);
        })
        .catch((error) => {
          console.log(error);
          cb({}, true);
        });
    });
  }
};

export const getStockTechnical = async (baseURL, appToken, token, stockId, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + STOCK_TECHNICAL_ANALYSIS_URL;
      const body = { stock_id: stockId };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log(json);

          if (header.status == 0) cb(mapStockTechnicalData(json.body));
          else cb({});
        })
        .catch((error) => {
          // console.log(error);
          cb({});
        });
    });
  }
};

mapStockTechnicalData = (json) => {
  let movingAveragesJson = json.movingAverages;
  let volumeJson = json.volume;

  let movingAverages = {};
  let movingAverageTableData = mapTableData(movingAveragesJson.tableHeaders, movingAveragesJson.tableData);

  movingAverages.data = movingAverageTableData;
  movingAverages.label = "Moving Averages";
  movingAverages.insight = "insight" in movingAveragesJson ? movingAveragesJson.insight : "";
  movingAverages.insightColor = "insightColor" in movingAveragesJson ? movingAveragesJson.insightColor : "";

  let volume = {};
  let volumeTableData = mapTableData(volumeJson.tableHeaders, volumeJson.tableData);

  volume.data = volumeTableData;
  volume.label = "Volume Analysis";
  volume.insight = "insight" in volumeJson ? volumeJson.insight : "";
  volume.insightColor = "insightColor" in volumeJson ? volumeJson.insightColor : "";

  let stock = {};
  let stockColumns = json.stockHeaders;
  json.stockData.map((item, i) => {
    stock[stockColumns[i].unique_name] = item;
  });

  const stockPriceData = json.stockPriceData ? json.stockPriceData : {};
  Object.assign(stock, stockPriceData);

  let candlestickJson = json.candlesticks;
  let bullish = {};
  let bearish = {};
  let candlestickColumns = candlestickJson.tableHeaders;

  if ("bearishData" in candlestickJson) {
    bearish = candlestickJson.bearishData;
  }

  if ("bullishData" in candlestickJson) {
    bullish = candlestickJson.bullishData;
  }

  let priceAnalysis = json.priceAnalysis ? json.priceAnalysis : {};
  const priceAnalysisOrder = json.priceAnalysisOrder ? json.priceAnalysisOrder : [];

  let priceAnalysisOrdered = {};
  priceAnalysisOrder.map((item) => (priceAnalysisOrdered[item] = priceAnalysis[item]));

  return {
    priceAnalysis: priceAnalysisOrdered, //json.priceAnalysis,
    technical: json.technicals,
    movingAverages: movingAverages,
    volume: volume,
    momentum: json.momentumData,
    stock: stock,
    candlesticks: { bullish: bullish, bearish: bearish },
  };
};

export const getStockShareHolding = async (baseURL, appToken, token, stockId, quarterString, onSuccess, onFailure) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + STOCK_SHAREHOLDING_URL;
      const body = { stock_id: stockId, quarterString: quarterString };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          // console.log(json);

          if (header.status == 0) onSuccess(mapStockShareholdingData(json.body));
          else onFailure(header.status, header.statusDescription);
        })
        .catch((error) => {
          // console.log(error);
          onFailure(-1);
        });
    });
  }
};

mapStockShareholdingData = (json) => {
  let stock = {};
  let stockColumns = json.stockHeaders;
  json.stockData.map((item, i) => {
    stock[stockColumns[i].unique_name] = item;
  });

  const institutionalData = mapTableData(json.InstitutionalData.tableHeaders, json.InstitutionalData.tableData);

  const promoterData = mapTableData(json.PromoterData.tableHeaders, json.PromoterData.tableData);

  const superstarData = mapTableData(json.SuperstarData.tableHeaders, json.SuperstarData.tableData);

  const publicData = mapTableData(json.PublicData.tableHeaders, json.PublicData.tableData);

  let summary = [];
  if ("summaryData" in json && json.summaryData.length > 0) {
    const summaryHeaders = json.summaryData[0];
    json.summaryData.map((item, i) => {
      if (i != 0) {
        let summaryObj = {};
        summaryObj[summaryHeaders[0]] = item[0];
        summaryObj[summaryHeaders[1]] = item[1];
        summaryObj[summaryHeaders[2]] = item[2];
        summary.push(summaryObj);
      }
    });
  }

  let chart = {};
  const chartJson = json.chartData;
  if ("Promoter" in chartJson) chart.Promoter = chartJson.Promoter;

  if ("Institutional" in chartJson) chart.Institutional = chartJson.Institutional;

  return {
    insights: json.insights,
    chart: chart,
    quarterString: json.quarterString,
    summary: summary,
    dateList: json.dateList.reverse(),
    institutional: institutionalData,
    promoter: promoterData,
    stock: stock,
    superstar: superstarData,
    public: publicData,
  };
};

export const getStockShareHoldingHistory = async (baseURL, appToken, token, holdingId, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + STOCK_HOLDING_HISTORY_URL;
      const body = { holdingId: holdingId };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);
      // console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log(json);

          if (header.status == 0) {
            const headers = json.body.tableHeaders;
            const data = json.body.tableData;
            cb(mapTableData(headers, data));
          } else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};

export const getStockReports = async (baseURL, appToken, token, stockId, pageNumber, perPageCount, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + RESEARCH_REPORTS_BY_STOCK_URL;
      const body = { stock_id: stockId.toString(), pageNumber: pageNumber, perPageCount: perPageCount };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);
      console.log("report", fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          console.log("Stock report", response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Stock report", json);

          if (header.status == 0) cb(mapStockReportData(json.body));
          else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};

mapStockReportData = (json) => {
  let table = mapTableData(json.tableHeaders, json.tableData);
  let stock = mapTableData(json.stockHeaders, [json.stockData]);
  let insights = {};

  if (Array.isArray(json.insights)) {
    json.insights.map((item) => {
      insights[item.unique_name] = item;
    });
  }

  console.log("report stock", stock);
  return {
    reports: table,
    stock: stock[0],
    insights: insights,
    isNextPage: json.isNextPage,
    pageNumber: json.pageNumber,
  };
};

// export const getStockOverview = async (baseURL, token, stockId, cb) => {
//   if (baseURL) {
//     await getFetchOptions(token, fetchOptions => {
//       const URL = baseURL + STOCK_OVERVIEW_URL;
//       const body = { stock_id: stockId };
//       fetchOptions.body = JSON.stringify(body);

//       setFirebaseAnalyticsEvent(URL, body);
//       // console.log(fetchOptions);

//       fetch(URL, fetchOptions)
//         .then(response => {
//           return response.json();
//         })
//         .then(json => {
//           let header = json.head;
//           // console.log("Stock overview", json);

//           if (header.status == 0) cb(mapStockOverviewData(json.body));
//           else cb({});
//         })
//         .catch(error => {
//           console.log(error);
//           cb({});
//         });
//     });
//   }
// };

// mapStockOverviewData = json => {
//   let stock = {};
//   let swot = {};
//   const stockHeaders = json.stockHeaders;

//   console.log("Stock API", json.stockData, stockHeaders);

//   json.stockData.map((item, i) => {
//     stock[stockHeaders[i].unique_name] = item;
//   });

//   if (Object.keys(json.SWOTData).length > 0) {
//     const swotHeaders = json.SWOTData.tableHeaders;
//     const swotData = json.SWOTData.tableData;
//     const swotKeys = Object.keys(swotData);

//     swotKeys.map(key => {
//       swot[key] = mapTableData(swotHeaders, swotData[key]);
//     });
//   }
//   let summaryData = [];
//   if ("summaryData" in json && json.summaryData.length > 0) {
//     const summaryHeaders = json.summaryData[0];
//     json.summaryData.map((item, i) => {
//       if (i != 0) {
//         let summaryObj = {};
//         summaryObj[summaryHeaders[0]] = item[0];
//         summaryObj[summaryHeaders[1]] = item[1];
//         summaryObj[summaryHeaders[2]] = item[2];
//         summaryData.push(summaryObj);
//       }
//     });
//   }

//   let i = 0;
//   let fundamental = [];
//   for (i; i < json.fundamentalData.length; i += 3) {
//     temp = json.fundamentalData.slice(i, i + 3);
//     fundamental.push(temp);
//   }

//   // console.log("temp", fundamental);
//   return {
//     stock: stock,
//     shareholding: summaryData,
//     swot: swot,
//     technical: json.technicalData,
//     fundamental: fundamental
//     // fundamental: json.fundamentalData
//   };
// };

export const getStockChartData = async (baseURL, appToken, token, stockId, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + STOCK_OHLC_CHART_URL;
      const body = { stock_id: stockId };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("stock overview chart", json);

          if (header.status == 0) {
            const body = json.body;
            // const chartHeaders = "tableHeaders" in body ? body.tableHeaders : [];
            // const chartData = "tableData" in body ? body.tableData : [];

            const eodHeader = "eodHeaders" in body ? body.eodHeaders : [];
            const eodData = "eodData" in body ? body.eodData : [];
            const eodChart = mapTableData(eodHeader, eodData);

            const liveHeader = "liveHeaders" in body ? body.liveHeaders : [];
            const liveData = "liveData" in body ? body.liveData : [];
            const liveChart = mapTableData(liveHeader, liveData);

            cb({ eod: eodChart, live: liveChart });
          } else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};

export const getFundamentalData = async (baseURL, appToken, token, stockId, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + STOCK_FUNDAMENTAL_URL;
      const body = { stock_id: stockId };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);
      // console.log("Request sent", new Date());

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log(json);

          if (header.status == 0) cb(mapFundamentalData(json.body));
          else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};
mapFundamentalData = (json) => {
  const resultsMetadata = json.resultsMetadata;
  const quarterlyData = json.quarterlyDataDump;
  const annualData = json.annualDataDump;
  const insights = json.parameterInsights;
  const parameterData = json.parameterMetadata;

  var data = {};

  Object.keys(resultsMetadata).map((resultKey) => {
    const typeData = [];
    // console.log("result key", resultKey);

    Object.keys(resultsMetadata[resultKey]).map((resultTypeKey) => {
      console.log("result type key", resultKey, resultTypeKey);

      let optionData = [];
      let resultTypeData = resultsMetadata[resultKey][resultTypeKey];

      if (resultTypeKey == "quarterly") {
        optionData = getQuarterAnnualData(quarterlyData, resultTypeData, insights, parameterData);

        const quarterOrder = json.quarterlyOrder;
        // console.log("quarter order", quarterOrder);

        let orderConsolidated = {};
        let orderStandalone = {};

        quarterOrder.map((quarter) => {
          if (quarter in optionData.consolidated) orderConsolidated[quarter] = optionData.consolidated[quarter];

          if (quarter in optionData.standalone) orderStandalone[quarter] = optionData.standalone[quarter];
        });
        // console.log("after ordering", orderConsolidated);
        optionData = {
          consolidated: orderConsolidated,
          standalone: orderStandalone,
        };
      } else {
        optionData = getQuarterAnnualData(annualData, resultTypeData, insights, parameterData);

        const annualOrder = json.annualOrder;
        // console.log("annual order", annualOrder);

        let orderConsolidated = {};
        let orderStandalone = {};

        annualOrder.map((annual) => {
          if (annual in optionData.consolidated) orderConsolidated[annual] = optionData.consolidated[annual];

          if (annual in optionData.standalone) orderStandalone[annual] = optionData.standalone[annual];
        });
        // console.log("after ordering", orderConsolidated);
        optionData = {
          consolidated: orderConsolidated,
          standalone: orderStandalone,
        };
      }
      typeData[resultTypeKey] = optionData;
    });
    data[resultKey] = typeData;
  });

  data.preferConsolidated = json.preferConsolidated;
  // console.log("Fundamental data", data);
  // console.log("Data Mapping done", new Date());

  return data;
};

getQuarterAnnualData = (data, dataDump, insight, details) => {
  let recordType = {};

  Object.keys(data).map((recordTypeKey) => {
    let quarterList = data[recordTypeKey];
    let recordList = [];
    // console.log("recordTypeKey -----", recordTypeKey);

    Object.keys(quarterList).map((quarterKey) => {
      let quarterData = quarterList[quarterKey];
      let quarterDataList = [];

      // console.log("quarter key *****************", quarterKey);
      // console.log("quarter Data :::::::::::::::::::>", quarterData);

      dataDump.map((item) => {
        let newItem = {};
        newItem.unique_name = item.paramUniqueName;
        newItem.insight = insight[recordTypeKey][item.paramUniqueName];
        newItem.data = details[item.paramUniqueName];
        newItem.value = quarterData[item.paramUniqueName];

        // console.log("item ==================", item);
        let childList = [];
        item.childrenUniqueNames.map((child) => {
          let childItem = {}; //"childrenUniqueNames";
          childItem.unique_name = child;
          childItem.insight = insight[child];
          childItem.data = details[child];
          childItem.value = quarterData[child];

          childList.push(childItem);
        });
        newItem.children = childList;
        quarterDataList.push(newItem);
        // if (quarterData[item.paramUniqueName] == null)
        //   console.log("quarter Data", item, quarterData);
      });
      recordList[quarterKey] = quarterDataList;
    });
    recordType[recordTypeKey] = recordList;
  });
  return recordType;
  // quarterDataObj[quarterTypeKey] = quarterList;
};

export const getTechnicalCandlestickInfo = (baseURL, appToken, token, descriptorId, describeName, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + CANDLESTICK_DESCRIPTION_URL;
      const body = { descriptorId: descriptorId, describeName: describeName };
      fetchOptions.body = JSON.stringify(body);

      setFirebaseAnalyticsEvent(URL, body);
      // console.log(fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;

          if (header.status == 0) cb(json.body.response);
          else cb({});
        })
        .catch((error) => console.log(error));
    });
  }
};

mapTableData = (headers, data) => {
  let dataList = [];

  data.map((dataItem) => {
    let dataObj = {};
    dataItem.map((item, i) => {
      if (headers[i] != undefined) {
        let colName = "unique_name" in headers[i] ? headers[i].unique_name : headers[i].name;
        dataObj[colName] = item;
      }
    });

    dataList.push(dataObj);
  });
  return dataList;
};
