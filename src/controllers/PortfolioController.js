import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import { GET_PORTFOLIO_LIST_URL, ADD_PORTFOLIO_URL, PORTFOLIO_ADD_HOLDING_URL } from "../utils/ApiEndPoints";

export const getPortfolioListApi = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_PORTFOLIO_LIST_URL;

      fetchOptions.method = "GET";
      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          console.log("Portfolio list API %%%%-->", json)
          if (header.status == 0) cb(json.body.portfolioList);
          else cb([]);
        })
        .catch((error) => {
          // console.log(error);
          cb([]);
        });
    });
  }
};

export const addNewPortfolio = async (baseURL, appToken, token, name, description, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + ADD_PORTFOLIO_URL;
      const body = { name: name, description: description };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Add new portfolio", json);

          if (header.status == 0) cb(true, json.body);
          else cb(false, header.statusDescription);
        })
        .catch((error) => {
          console.log(error);
          cb(false);
        });
    });
  }
};

export const addStockToPortfolio = async (
  baseURL,
  appToken,
  token,
  portfolioId,
  stockId,
  transactionDate,
  quantity,
  price,
  cb
) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + PORTFOLIO_ADD_HOLDING_URL;
      const body = {
        portfolioId: portfolioId,
        stock_id: stockId,
        transactionDate: transactionDate,
        qty: quantity,
        price: price,
        notes: "",
      };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      console.log("Add stock portfolio fetch", fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          console.log("Add stock to portfolio response", response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("Add stock to portfolio", json);

          if (header.status == 0) cb(true);
          else cb(false);
        })
        .catch((error) => {
          cb(false);
          console.log(error);
        });
    });
  }
};
