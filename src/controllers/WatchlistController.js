import { getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import {
  GET_ALL_WATCHLIST_URL,
  GET_ACTIVE_WATCHLIST_URL,
  PUSH_TO_WATCHLIST_URL,
  POP_FROM_WATCHLIST_URL,
  CREATE_WATCHLIST_URL,
} from "../utils/ApiEndPoints";

// export const getAllWatchlist = async (stockId, token, cb) => {};

export const getWatchlistApi = async (baseURL, appToken, token, stockId, cb) => {
  await getFetchOptions(appToken, token, (fetchOptions) => {
    if (baseURL) {
      const URL = baseURL + GET_ALL_WATCHLIST_URL;
      const body = { stock_id: stockId };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log("get watchlist by stock id", json);
          let header = json.head;
          let watchlist = json.body.watchlists ? json.body.watchlists : [];

          let activeInWatchlist = json.body.activeInWatchlist ? json.body.activeInWatchlist : [];

          if (header.status == 0) cb(watchlist, activeInWatchlist);
          else cb([], []);
        })
        .catch((error) => {
          // console.log(error);
          cb([], []);
        });
    }
  });
};

export const pushPopStockFromWatchlist = async (baseURL, appToken, token, watchlistId, stockId, action, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const body = { watchlistId: watchlistId, stock_id: stockId };

      const URL = action == "push" ? baseURL + PUSH_TO_WATCHLIST_URL : baseURL + POP_FROM_WATCHLIST_URL;

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      // console.log('$$$$$$$----->',fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // checkCookies(response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          console.log("$$$$$-->", json);

          if (header.status == 0) cb(true, header.status, header.statusDescription);
          else cb(false, header.status, header.statusDescription);
        })
        .catch((error) => console.log("-->", error));
    });
  }
};

export const createNewWatchlist = async (baseURL, appToken, token, title, description, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + CREATE_WATCHLIST_URL;
      const body = { watchlistTitle: title, watchlistDesc: description };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          console.log("watchlist ", json);
          if (header.status == 0) cb(json.body.watchlistId, header.statusDescription, header.status);
          else if (header.status == 2) cb(-1, header.statusDescription, header.status);
          else cb(-1, header.statusDescription, header.status);
        })
        .catch((error) => {
          cb(-1);
          console.log(error);
        });
    });
  }
};

export const getWatchlistActiveStock = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    const URL = baseURL + GET_ACTIVE_WATCHLIST_URL;

    await getFetchOptions(appToken, token, (fetchOptions) => {
      fetchOptions.method = "GET";

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;

          if (header.status == 0) {
            const stockList = json.body.stock_id_List;
            const activeList = {};

            stockList.forEach((stockId) => {
              activeList[stockId] = [];
            });

            cb(activeList);
          } else cb({});
        })
        .catch((error) => {
          console.log(error);
          cb({});
        });
    });
  }
};
