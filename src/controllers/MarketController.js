import { parseScreenData, getFetchOptions } from "../utils/Utils";
import { setFirebaseAnalyticsEvent } from "./FirebaseController";
import {
  GET_MARKET_DATA_URL,
  GET_CUSTOM_PARAMETER_URL,
  GET_STOCK_BULK_BLOCK_DEALS_URL,
  GET_STOCK_INSIDER_TRADES_DEALS_URL,
  TRENDING_NEWS_URL,
} from "../utils/ApiEndPoints";

export const getNiftySensexPoints = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_CUSTOM_PARAMETER_URL;
      const body = {
        stockCodeList: ["NIFTY50", "BSESENSEX"],
        columns: ["currentPrice", "day_change", "day_changeP"],
      };

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          let header = json.head;
          console.log("nifty", json);

          if (header.status == 0) cb(mapNiftySensexData(json.body));
          else cb([]);
        })
        .catch((error) => {
          cb([]);
          console.log(error);
        });
    });
  }
};

export const getStocksCardData = async (baseURL, appToken, token, endPoint, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_MARKET_DATA_URL + endPoint + "/";

      setFirebaseAnalyticsEvent(URL, {});
      console.log("market ", URL, fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => response.json())
        .then((json) => {
          console.log("market ", json);
          console.log("fetchOptions==", fetchOptions);
          let header = json.head;

          if (header.status == 0) cb(mapStocksCardList(json.body));
          else cb({});
        })

        .catch((error) => console.log(error));
    });
  }
};

// export const getStocksList = async (pk, groupType, groupName, perPageCount, pageNumber, token, cb) => {
//   await getFetchOptions(token, fetchOptions => {
//     const URL = baseURL + "screener/";
//     const body = {
//       screenpk: pk,
//       groupType: groupType,
//       groupName: groupName,
//       perPageCount: perPageCount,
//       pageNumber: pageNumber
//     };

//     fetchOptions.body = JSON.stringify(body);
//     setFirebaseAnalyticsEvent(URL, body);

//     fetch(URL, fetchOptions)
//       .then(response => response.json())
//       .then(json => {
//         let header = json.head;
//         // console.log("market list", json);

//         if (header.status == 0) cb(mapStockList(json.body), header.isNextPage);
//         else {
//           cb([]);
//         }
//       })
//       .catch(error => {
//         cb({});
//         console.log(error);
//       });
//   });
// };

export const getMarketBulkDeals = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_STOCK_BULK_BLOCK_DEALS_URL;

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          if (header.status == 0) cb(mapStocksData(json.body));
          else cb([]);
        })
        .catch((error) => {
          cb([]);
          console.log(error);
        });
    });
  }
};

export const getMarketInsiderTradesDeals = async (baseURL, appToken, token, cb) => {
  if (baseURL) {
    await getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + GET_STOCK_INSIDER_TRADES_DEALS_URL;

      setFirebaseAnalyticsEvent(URL, {});

      fetch(URL, fetchOptions)
        .then((response) => {
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          // console.log(json);

          if (header.status == 0) cb(mapStocksData(json.body));
          else cb([]);
        })
        .catch((error) => {
          cb([]);
          console.log(error);
        });
    });
  }
};

export const getTrendingNews = (baseURL, appToken, token, nextPage, nextLevel, qsTime, cb) => {
  if (baseURL) {
    getFetchOptions(appToken, token, (fetchOptions) => {
      const URL = baseURL + TRENDING_NEWS_URL;
      const body = {
        level: nextLevel,
        pageNumber: nextPage,
        qsTime: qsTime ? qsTime : "",
      };

      // console.log("news body", body);

      fetchOptions.body = JSON.stringify(body);
      setFirebaseAnalyticsEvent(URL, body);

      // console.log("news fetch", fetchOptions);

      fetch(URL, fetchOptions)
        .then((response) => {
          // console.log("news response before parsing", response);
          return response.json();
        })
        .then((json) => {
          let header = json.head;
          // console.log("news response", json);

          if (header.status == 0) {
            // console.log("news response", json.body.newsList);

            cb({
              news: json.body.newsList,
              qsTime: json.body.qsTime,
              nextPage: json.body.nextPage,
              nextLevel: json.body.nextLevel,
              isNextPage: header.isNextPage,
            });
          } else cb({}, true);
        })
        .catch((error) => {
          console.log(error);
          cb({}, true);
        });
    });
  }
};

mapNiftySensexData = (json) => {
  let data = json;
  let columnList = data.tableHeaders;
  let stockList = [];

  // stock details
  data.tableData.forEach((stockIndex) => {
    let stockObj = {};

    stockIndex.forEach((item, i) => {
      stockObj[columnList[i].unique_name] = item;
    });
    stockList.push(stockObj);
  });

  return stockList;
};

//get indices Nifty50,BSE200,Nifty500 or get userData Portfolio, WatchList
mapStocksCardList = (json) => {
  let data = [];
  Object.keys(json).forEach((key) => {
    let cardList = []; //list of cards data for individual screener

    let stockIndex = json[key];
    console.log("data==", data);
    stockIndex.forEach((card) => {
      // console.log("headers", card.tableHeaders.length);
      let columnList = Object.assign({}, card.tableHeaders);
      let stockList = []; // stocks in single card

      // stock details
      card.tableData.forEach((stock) => {
        let stockObj = {};
        // console.log("stock", Object.keys(stock).length);
        stock.forEach((item, i) => {
          if (columnList[i] != undefined) {
            let uniqueName = columnList[i].unique_name ? columnList[i].unique_name : columnList[i].name;
            stockObj[uniqueName] = item;
          }
        });

        stockList.push(stockObj);
      });

      cardList.push({
        cardName: card.cardName,
        groupName: card.groupName,
        groupType: card.groupType,
        screen: parseScreenData(card.screen),
        columns: columnList,
        stocks: stockList,
      });
    });

    data.push(cardList);
  });
  // console.log(data);
  return orderDataForList(data);
};

mapStockList = (json) => {
  let columnList = json.tableHeaders;
  let stockList = [];

  json.tableData.forEach((stock) => {
    let stockObj = {};

    stock.forEach((item, i) => {
      if (columnList[i] != undefined) stockObj[columnList[i].unique_name] = item;
    });
    console.log("item==", item);
    stockList.push(stockObj);
  });

  return stockList;
};

orderDataForList = (data) => {
  let newList = [];
  let pkList = [];

  let stockIndexList = data[0];

  stockIndexList.forEach((stockIndex, i) => {
    let screenerList = [];

    data.forEach((_, j) => {
      let stockIndex = data[j];
      let groupName = stockIndex[i].groupName;

      if (groupName === "NIFTY500") screenerList[0] = stockIndex[i];
      else if (groupName === "BSE200") screenerList[1] = stockIndex[i];
      else if (groupName === "NIFTY50") screenerList[2] = stockIndex[i];
      else screenerList.push(stockIndex[i]);
    });
    newList[i] = screenerList;
    pkList.push(stockIndex.screen.pk);
  });
  return { stockList: newList, pkList: pkList };
};

mapStocksData = (json) => {
  let columnList = [];
  let stockList = [];
  try {
    if (json.tableHeaders) {
      json.tableHeaders.forEach((column, index) => {
        columnList.push(column.unique_name ? column.unique_name : column.name);
      });
    } else {
      json.tableHeaders.forEach((column, index) => {
        columnList.push(column.unique_name ? column.unique_name : column.name);
      });
    }

    json.tableData.forEach((stock) => {
      let stockObj = {};

      stock.forEach((item, i) => {
        stockObj[columnList[i]] = item;
      });

      stockList.push(stockObj);
    });
  } catch (error) {
    console.log("Error");
  }
  // console.log(stockList)
  return stockList;
};
