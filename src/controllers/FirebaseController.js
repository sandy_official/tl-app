import firebase from "react-native-firebase";
import { TEST_SERVER_URL } from "../utils/Constants";

export const getFirebaseAppSettings = async (cb) => {
  await getFirebaseRemoteConfigs((data) => {
    let auth = data.AuthKeys ? JSON.parse(data.AuthKeys) : {};
    let other = data.Others ? JSON.parse(data.Others) : {};
    let plan = data.PlanImages ? JSON.parse(data.PlanImages) : {};
    let version = data["V1"] ? JSON.parse(data["V1"]) : {};

    cb({ auth, other, plan, version });
  });
};

getFirebaseRemoteConfigs = async (cb) => {
  await firebase
    .config()
    .fetch(0) // to avoid cache, We can increase because we have static settings 
    .then(() => {
      return firebase.config().activateFetched();
    })
    .then((activated) => {
      if (!activated) console.log("Fetched data not activated");
      return firebase.config().getValues(["AuthKeys", "Others", "V1", "PlanImages"]);
    })
    .then((snapshot) => {
      let data = {};
      Object.keys(snapshot).map((key) => {
        data[key] = snapshot[key].val();
      });

      // console.log("data => ", snapshot, data);
      cb(data);
    })
    .catch((e) => console.log(e));
};

// this is not being used 
// just for referance 
/*
getFirestoreSettings = async (cb) => {
  await firebase
    .firestore()
    .runTransaction(async (transaction) => {
      const authRef = firebase.firestore().collection("markets").doc("AuthKeys");
      const otherRef = firebase.firestore().collection("markets").doc("Others");
      const planRef = firebase.firestore().collection("markets").doc("PlanImages");
      const versionRef = firebase.firestore().collection("markets").doc("V1");
      const settingsRef = firebase.firestore().collection("markets").doc("settings");

      const [authDoc, otherDoc, planDoc, versionDoc, settingsDoc] = await Promise.all([
        transaction.get(authRef),
        transaction.get(otherRef),
        transaction.get(planRef),
        transaction.get(versionRef),
        transaction.get(settingsRef),
      ]);

      let auth = authDoc.exists ? authDoc.data() : {};
      let other = otherDoc.exists ? otherDoc.data() : {};
      let plan = planDoc.exists ? planDoc.data() : {};
      let version = versionDoc.exists ? versionDoc.data() : {};
      // let settings = settingsDoc.exists ? settingsDoc.data() : {};

      //For test apk
      // version.baseUrl = TEST_SERVER_URL;

      cb({ auth, other, plan, version, settings });
    })
    .catch((error) => {
      console.warn("Transaction failed: ", error);
      // setFirebaseAnalyticsEvent("Firebase settings not received", {});
      // cb({});
    });
};
*/

const slugify = (string) => {
  const a = "àáäâãåèéëêìíïîòóöôùúüûñçßÿœæŕśńṕẃǵǹḿǘẍźḧ·/_,:;";
  const b = "aaaaaaeeeeiiiioooouuuuncsyoarsnpwgnmuxzh______";
  const p = new RegExp(a.split("").join("|"), "g");
  return string
    .toString()
    .toLowerCase()
    .replace(/\s+/g, "_") // Replace spaces with -
    .replace(p, (c) => b.charAt(a.indexOf(c))) // Replace special characters in a with b
    .replace(/&/g, "_and_") // Replace & with ‘and’
    .replace(/[^\w-]+/g, "") // Remove all non-word characters such as spaces or tabs
    .replace(/--+/g, "_") // Replace multiple — with single -
    .replace(/^-+/, "") // Trim — from start of text
    .replace(/-+$/, "") // Trim — from end of text
    .replace(/-/g, "_"); // replace - with _
};

export const setFirebaseAnalyticsEvent = async (path, params) => {
  let urltag = path.replace(/^.*\/\/[^\/]+/, "");
  urltag = slugify(urltag).replace(/^_+/, "");
  try {
    if (params instanceof Object) firebase.analytics().logEvent(slugify(urltag), params);
    else firebase.analytics().logEvent(slugify(urltag));
    // console.log("firebase event", path, urltag, slugify(urltag), params);
  } catch (e) {
    console.log(e);
  }
};

export const setFirebaseAnalyticsScreenView = async (currentScreen, params) => {
  const currentScreenName = slugify(currentScreen.replace(/^.*\/\/[^\/]+/, ""));
  try {
    firebase.analytics().setCurrentScreen(currentScreenName);
    if (params) {
      firebase.analytics().logEvent(currentScreenName, params);
    }
  } catch (e) {
    console.log(e);
  }
};
