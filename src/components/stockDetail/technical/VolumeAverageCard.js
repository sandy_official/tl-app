import React from "react";
import { View, Text, Image, StyleSheet, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class VolumeAverageCard extends React.PureComponent {
  renderListColumns = () => {
    const { data } = this.props;
    if (data.label == "Volume Analysis")
      return (
        <View style={styles.row}>
          <Text style={[styles.left, styles.column]}>Period</Text>
          <Text style={[styles.center, styles.column, { flex: 1.4 }]}>BSE + NSE Avg.</Text>
          <Text style={[styles.right, styles.column]}>Delivery %</Text>
        </View>
      );
    else
      return (
        <View style={styles.row}>
          <Text style={[styles.left, styles.column]}>Period</Text>
          <Text style={[styles.center, styles.column]}>Simple</Text>
          <Text style={[styles.right, styles.column]}>Exponential</Text>
        </View>
      );
  };

  renderListItem = ({ item, index }) => {
    const { data } = this.props;

    let col1 = APIDataFormatter("period", item.period);
    let col2 = " - ";
    let col3 = " - ";
    let midColStyle = [styles.right, styles.columnValue];

    if (data.label == "Volume Analysis") {
      midColStyle.push({ flex: 1.4 });
      col2 = APIDataFormatter("cvol", item.cvol);
      col3 = APIDataFormatter("delivery_avg", item.delivery_avg);
    } else {
      col2 = APIDataFormatter("SMA", item.SMA);
      col3 = APIDataFormatter("EMA", item.EMA);
    }

    // const rowStyle =
    //   index % 2 == 0 ? [styles.row, styles.greyBackground] : styles.row;

    return (
      <View style={styles.row}>
        <Text style={[styles.left, styles.columnValue, styles.boldText]}>{col1}</Text>
        <Text style={midColStyle}>{col2}</Text>
        <Text style={[styles.right, styles.columnValue]}>{col3}</Text>
      </View>
    );
  };

  getInsightIcon = (value) => {
    if (value == "positive") return require("../../../assets/icons/arrow-green.png");
    else if (value == "negative") return require("../../../assets/icons/arrow-red.png");
    // else if (value == "neutral")
    else return require("../../../assets/icons/arrow-yellow.png");
    // else return { backgroundColor: colors.steelGrey };
  };

  getInsightStyle = (value) => {
    if (value == "positive") return { color: colors.green };
    else if (value == "negative") return { color: colors.red };
    // else if (value == "neutral")
    else return { color: colors.squash };
  };

  render() {
    const { data } = this.props;
    const dataList = data.data;
    const insight = data.insight ? data.insight : "";
    const showInsight = insight ? true : false;
    const insightTextStyle = [styles.info, this.getInsightStyle(data.insightColor)];

    // console.log("Volume card", data);

    return (
      <View style={styles.container}>
        <Text style={styles.cardTitle}>{data.label}</Text>

        {showInsight && (
          <View style={styles.tradingInfo}>
            <Image source={this.getInsightIcon(data.insightColor)} style={styles.arrowIcon} />
            <Text style={insightTextStyle} numberOfLines={2} ellipsizeMode="tail">
              {insight}
            </Text>
          </View>
        )}

        <FlatList
          contentContainerStyle={styles.list}
          data={dataList}
          // extraData={this.state}
          showsVerticalScrollIndicator={false}
          ListHeaderComponent={this.renderListColumns}
          renderItem={this.renderListItem}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(88),
    // overflow: "hidden",
    //  marginHorizontal:wp(3),
    marginVertical: wp(1),
    paddingVertical: wp(3),
    marginRight: wp(3),
    backgroundColor: colors.white,
    // borderWidth: 1,
    // borderColor: "rgba(0, 0, 0, 0.08)",
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  row: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(3),
  },
  left: {
    flex: 1,
  },
  center: {
    flex: 1,
    alignItems: "flex-end",
    textAlign: "right",
  },
  // list: {
  //   paddingHorizontal: wp(3),
  //    paddingVertical: wp(1),
  // //   // margin: wp(3),
  // //   // backgroundColor: colors.white,
  // //   // borderRadius: 6,
  // //   // shadowColor: "rgba(77, 77, 77, 0.3)",
  // //   // shadowOffset: {
  // //   //   width: 0,
  // //   //   height: 0
  // //   // },
  // //   // shadowRadius: 5,
  // //   // shadowOpacity: 1,
  // //   // elevation: 2
  //  },
  card: {
    //  paddingHorizontal: wp(3),
    paddingVertical: wp(3),
    margin: wp(3),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },

  right: {
    textAlign: "right",
    flex: 1,
    marginLeft: wp(3),
  },
  greyBackground: {
    backgroundColor: "rgba(23, 64, 120,0.05)",
  },
  cardTitle: {
    fontSize: RF(2),
    fontWeight: "500",
    marginLeft: wp(3),
    marginBottom: wp(3),
    color: colors.black,
  },
  tradingInfo: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp(3),
    marginBottom: wp(3),
  },
  column: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginVertical: wp(1),
  },
  columnValue: {
    fontSize: RF(2.2),
    color: colors.black,
  },
  boldText: {
    fontWeight: "300",
  },
  arrowIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginTop: 3,
    marginRight: wp(3),
  },
  info: {
    flex: 1,
    color: colors.green,
    fontSize: RF(1.9),
    fontWeight: "500",
    // marginBottom: wp(3)
  },
  divider: {
    flex: 1,
    height: 1.5,
    marginHorizontal: wp(0),
    opacity: 0.1,
    backgroundColor: colors.greyishBrown,
  },
});
