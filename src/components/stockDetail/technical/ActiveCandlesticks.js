import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, FlatList, ScrollView } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import MomentumItem from "./MomentumItem";
import { ICON_CDN } from "../../../utils/Constants";
import StockCandlestickDetail from "./StockCandlestickDetail";

export default class ActiveCandlesticks extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedItem: [] };
  }

  openBottomSheet = (item) => {
    this.setState({ selectedItem: item }, () => this.refs.detailScreen.show());
  };

  renderScreenerItem = ({ item, index }) => {
    // const containerStyle = index % 2 == 1 ? [styles.screenerRow, styles.greyBackground] : styles.screenerRow;

    return (
      <View style={styles.screenerRow}>
        <Text numberOfLines={2} ellipsizeMode="tail" style={styles.screenerName}>
          {item[0]}
        </Text>

        <TouchableOpacity onPress={() => this.openBottomSheet(item)}>
          <Image source={require("../../../assets/icons/info-blue.png")} style={styles.infoIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderBullishData = () => {
    const { data } = this.props;
    const showBullish = "bullish" in data && data.bullish.length > 0;

    return (
      <View>
        <Text style={[styles.candleStickType, styles.greenText]}>Bullish Pattern</Text>
        {!showBullish && <Text style={styles.noActiveCandlestick}>No active bullish candlesticks</Text>}

        {showBullish && (
          <FlatList
            // contentContainerStyle={styles.list}
            data={data.bullish}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderScreenerItem}
            keyExtractor={(item, index) => index.toString()}
            // ItemSeparatorComponent={() => <View style={styles.divider} />}
            removeClippedSubviews
          />
        )}
      </View>
    );
  };

  renderBearishData = () => {
    const { data } = this.props;
    const showBearish = "bearish" in data && data.bearish.length > 0;

    return (
      <View>
        <Text style={[styles.candleStickType, styles.redText]}>Bearish Pattern</Text>

        {!showBearish && <Text style={styles.noActiveCandlestick}>No active bearish candlesticks</Text>}

        {showBearish && (
          <FlatList
            //  contentContainerStyle={styles.list}
            data={data.bearish}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderScreenerItem}
            // ItemSeparatorComponent={() => <View style={styles.divider} />}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  };

  render() {
    const { selectedItem } = this.state;

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>ACTIVE CANDLESTICKS</Text>

        <View style={styles.card}>
          {this.renderBearishData()}

          <View style={styles.space} />

          {this.renderBullishData()}
        </View>
        <StockCandlestickDetail ref="detailScreen" data={selectedItem} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    marginTop: hp(4),
  },
  heading: {
    marginLeft: wp(3),
    // marginBottom: hp(2),
    color: colors.black,
    fontSize: RF(2.4),
    fontWeight: "300",
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  card: {
    // paddingHorizontal: wp(3),
    paddingVertical: wp(3),
    margin: wp(3),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  candleStickType: {
    fontSize: RF(2.2),
    fontWeight: "500",
    marginHorizontal: wp(3),
    marginBottom: wp(2),
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.green,
  },
  screenerRow: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
    alignItems: "center",
  },
  screenerName: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  infoIcon: {
    width: wp(4.5),
    height: wp(4.5),
    resizeMode: "contain",
    marginHorizontal: wp(3.5),
    marginVertical: wp(1.5),
  },
  space: {
    marginBottom: wp(5),
  },
  greyBackground: {
    backgroundColor: "rgba(23, 64, 120,0.05)",
  },
  noActiveCandlestick: {
    fontSize: RF(2.2),
    color: colors.black,
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
  },
  divider: {
    flex: 1,
    height: 1.5,
    opacity: 0.12,
    backgroundColor: colors.greyishBrown,
  },
  list: {
    // paddingHorizontal: wp(3),
    // paddingVertical: wp(1),
    margin: wp(3),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 2,
    elevation: 2,
  },
});
