import React from "react";
import { View, Text, Image, StyleSheet, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { ICON_CDN } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class PriceChangeCardList extends React.PureComponent {
  listRef = null;
  // constructor(props) {
  //   super(props);
  //   // this.state = { currentPosition: 0 };
  // }

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.listRef && this.listRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 });
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  getBackgroundStyle = (value) => {
    if (value == "positive") return { backgroundColor: colors.green };
    else if (value == "negative") return { backgroundColor: colors.red };
    else return { backgroundColor: colors.squash };
  };

  findLtp = (item) => {
    const { data, stock } = this.props;
    const itemObj = data[item];
    const lowPrice = !isNaN(itemObj.low) ? itemObj.low : 0;
    const highPrice = !isNaN(itemObj.high) ? itemObj.high : 0;
    const currentPrice = !isNaN(stock.currentPrice) ? stock.currentPrice : 0;
    const highLowDiff = highPrice - lowPrice;
    const percent = highLowDiff > 0 ? highLowDiff / 100 : 0;
    const currentLowDiff = currentPrice - lowPrice;
    const ltp = percent > 0 && currentLowDiff > 0 ? currentLowDiff / percent : 0;
    // console.log("Ltp", ltp);
    return ltp;
  };

  renderPriceChangeCard = ({ item, index }) => {
    const { data } = this.props;
    const itemObj = data[item];

    const lowPrice = APIDataFormatter("low", itemObj.low);
    const highPrice = APIDataFormatter("high", itemObj.high);
    const priceChange = APIDataFormatter("change", itemObj.change);
    const priceChangePercent = APIDataFormatter("changePercent", itemObj.changePercent);

    const cardLabel = item.charAt(0).toUpperCase() + item.slice(1);
    const priceChangeStyle = [styles.priceChangeContainer, this.getBackgroundStyle(itemObj.color)];

    const ltpPercent = this.findLtp(item);
    const ltpStyle = [styles.ltpProgress, { width: ltpPercent + "%" }];
    const ltpLabelPercent = ltpPercent <= 85 ? (ltpPercent / 100) * 85 + "%" : "85%";
    const ltpLabelStyle = [styles.ltp, { paddingLeft: ltpLabelPercent }];

    let cardName = itemObj.name ? `Over ${itemObj.name}` : "";

    return (
      <View style={styles.priceChangeCard}>
        <View style={styles.cardTitleRow}>
          <Text style={styles.duration} numberOfLines={1}>
            {cardName}
          </Text>
          <View style={priceChangeStyle}>
            <Text style={styles.priceChange} numberOfLines={1}>
              {priceChange}
              <Text style={styles.priceChangePercent}> {priceChangePercent}</Text>
            </Text>
          </View>
        </View>

        <View style={styles.priceRow}>
          <View style={styles.lowPrice}>
            <Text style={[styles.priceCol, styles.redText]}>Low</Text>
            <Text style={styles.priceCol}>{lowPrice}</Text>
          </View>

          <View style={styles.ltpCol}>
            <Text style={ltpLabelStyle}>LTP</Text>
            <View style={styles.ltpContainer}>
              <View style={ltpStyle} />
            </View>
          </View>
          <View style={styles.highPrice}>
            <Text style={[styles.priceCol, styles.greenText]}>High</Text>
            <Text style={styles.priceCol}>{highPrice}</Text>
          </View>
        </View>
      </View>
    );
  };

  renderCurrentPrice = () => {
    const { stock } = this.props;

    const currentPrice = APIDataFormatter("currentPrice", stock.currentPrice);
    const priceChange = APIDataFormatter("dayChange", stock.dayChange);
    const priceChangePercent = " " + APIDataFormatter("dayChangeP", stock.dayChangeP);

    const stockIcon =
      stock.dayChange > 0
        ? require("../../../assets/icons/arrow-green.png")
        : require("../../../assets/icons/arrow-red.png");

    const stockBackground =
      stock.dayChange > 0
        ? styles.currentPriceContainer
        : [styles.currentPriceContainer, { backgroundColor: colors.red }];

    return (
      <View style={styles.currentPriceRow}>
        <Text style={styles.currentPriceLabel}>Current Price:</Text>
        <Image source={stockIcon} style={styles.currentPriceIcon} />
        <Text style={styles.currentPrice}>{currentPrice}</Text>

        <View style={stockBackground}>
          <Text style={styles.priceChange} numberOfLines={1}>
            {priceChange} <Text style={styles.priceChangePercent}>{priceChangePercent}</Text>
          </Text>
        </View>
      </View>
    );
  };

  render() {
    const { data } = this.props;

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>PRICE CHANGE ANALYSIS</Text>
        {this.renderCurrentPrice()}
        <FlatList
          ref={(ref) => (this.listRef = ref)}
          contentContainerStyle={styles.list}
          data={Object.keys(data)}
          extraData={this.state}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderPriceChangeCard}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
          removeClippedSubviews
          initialNumToRender={2}
          maxToRenderPerBatch={1}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
  },
  heading: {
    marginLeft: wp(3),
    marginBottom: wp(3),
    marginTop: hp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  priceChangeCard: {
    width: wp(75),
    backgroundColor: colors.white,
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
    borderRadius: 5,
    paddingVertical: wp(4),
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 4,
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.green,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  list: {
    paddingHorizontal: wp(1.5),
    // paddingVertical: wp(1)
  },
  cardTitleRow: {
    flexDirection: "row",
    alignItems: "center",
    paddingLeft: wp(3),
    marginBottom: wp(2),
  },
  duration: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  priceChangeContainer: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    backgroundColor: colors.green,
    paddingHorizontal: 15,
    paddingVertical: 7,
  },
  priceChange: {
    fontSize: RF(1.9),
    color: colors.white,
  },
  priceChangePercent: {
    fontSize: RF(2.2),
    color: colors.white,
    fontWeight: "500",
  },
  priceRow: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
  },
  lowPrice: {
    justifyContent: "flex-start",
  },
  priceCol: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.greyishBrown,
    marginTop: 6,
  },
  ltpCol: {
    flex: 1,
    justifyContent: "flex-end",
    marginHorizontal: wp(3),
  },
  ltp: {
    // textAlign: "center",
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.lightNavy,
  },
  ltpContainer: {
    height: wp(1),
    overflow: "hidden",
    borderRadius: 2.5,
    backgroundColor: "rgba(198,198,198,0.5)",
    marginBottom: wp(1.5),
    marginTop: 2,
  },
  ltpProgress: {
    width: "35%",
    borderRadius: 2.5,
    height: wp(1),
    backgroundColor: colors.lightNavy,
  },
  highPrice: {
    alignItems: "flex-end",
  },
  currentPriceRow: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: wp(3),
    marginBottom: wp(3),
  },
  currentPriceLabel: {
    fontSize: RF(2.2),
    color: colors.black,
    marginRight: wp(3),
  },
  currentPriceIcon: {
    width: wp(2.5),
    height: wp(2.5),
    resizeMode: "contain",
  },
  currentPrice: {
    marginLeft: 6,
    fontSize: RF(2.7),
    fontWeight: "300",
    color: colors.black,
  },
  currentPriceContainer: {
    flexDirection: "row",
    borderRadius: 5,
    backgroundColor: colors.green,
    paddingHorizontal: 10,
    paddingVertical: 7,
    marginLeft: wp(3),
  },
});
