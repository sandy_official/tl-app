import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import MomentumItem from "./MomentumItem";
import MomentumInfoBSheet from "./MomentumInfoBSheet";

export default class MomentumList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedMomentum: {} };
  }

  showPopup = momentum => {
    // console.log("Selected Momentum", momentum);
    this.setState({ selectedMomentum: momentum }, () => this.refs.info.RBSheet.open());
  };

  getBackgroundStyle = value => {
    if (value == "positive") return { backgroundColor: colors.green };
    else if (value == "negative") return { backgroundColor: colors.red };
    else if (value == "neutral") return { backgroundColor: colors.squash };
    else return { backgroundColor: colors.steelGrey };
  };

  renderListItem = ({ item, index }) => {
    return <MomentumItem data={item} onSelect={this.showPopup} />;
  };

  render() {
    const { selectedMomentum } = this.state;
    const { momentum, technical } = this.props;

    const mainMomentumBg = momentum.color
      ? [styles.momentScoreContainer, this.getBackgroundStyle(momentum.color)]
      : styles.momentScoreContainer;
    const showMomentum = Object.keys(momentum).length > 0;
    const title = selectedMomentum.name ? selectedMomentum.name : "";
    const description = selectedMomentum.lt ? selectedMomentum.lt : "";

    return (
      <View style={styles.container}>
        <Text style={styles.heading}>MOMENTUM STATISTICS</Text>

        {showMomentum && (
          <View style={mainMomentumBg}>
            <MomentumItem mainItem data={momentum} onSelect={this.showPopup} />
          </View>
        )}

        {/* Four colors green, red, yellow & grey */}
        <FlatList
          contentContainerStyle={styles.list}
          data={technical}
          extraData={this.props}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          removeClippedSubviews
          initialNumToRender={4}
          maxToRenderPerBatch={1}
        />

        <MomentumInfoBSheet ref="info" title={title} info={description} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: hp(4)
    // flex: 1,
  },
  heading: {
    marginLeft: wp(3),
    marginBottom: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
     color: colors.black,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },
  list: {
    // paddingHorizontal: wp(3),
    // paddingVertical: wp(1),
    margin: wp(3),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyishBrown
  },
  momentScoreContainer: {
    backgroundColor: colors.squash,
    marginHorizontal: wp(3),
    marginBottom: wp(1),
    borderRadius: 5
  }
});
