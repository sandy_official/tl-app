import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView, ImageBackground, Linking } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX, getStatusBarHeight } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../Toolbar";
import RBSheet from "react-native-raw-bottom-sheet";
import { getTechnicalCandlestickInfo } from "../../../controllers/StocksController";
import LoadingView from "../../common/LoadingView";
import { connect } from "react-redux";

// var meaning =
//   "The one day Bullish Reversal pattern Dragonfly Doji is a rare candlestick pattern that occurs at the bottom of a downtrend. It is very similar to the Bullish Hammer Pattern, except on a Dragonfly Doji the opening and closing prices are nearly identical with no body.The Bullish Dragonfly Doji is considered to be more reliable than a Bullish Hammer and tends to be a stronger bullish signal. The pattern is considered most reliable after an established bearish trend. ";

class StockCandlestickDetail extends React.PureComponent {
  show() {
    this.RBSheet.open();
    this.getData();
  }

  constructor(props) {
    super(props);
    this.state = {
      dropdownState: [true, false],
      info: {},
      isRefreshing: false,
    };
  }

  getData = () => {
    const { data, userState, baseURL, appToken } = this.props;
    this.setState({ isRefreshing: true });
    getTechnicalCandlestickInfo(baseURL, appToken, userState.token, null, data[1], (res) => {
      console.log("candlestick data", res);
      this.setState({ info: res, isRefreshing: false });
    });
  };

  openVideo = (url) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        console.log("Don't know how to open URI: " + url);
      }
    });
  };

  selectDropdown = (position) => {
    var dropdownState = this.state.dropdownState;
    dropdownState[position] = !dropdownState[position];
    this.setState({ dropdownState: [...dropdownState] });
  };

  renderDropDown = (title, description, position) => {
    const { dropdownState } = this.state;
    const dropdownIcon =
      title == dropdownState[position]
        ? require("../../../assets/icons/arrow-up-blue.png")
        : require("../../../assets/icons/arrow-down-blue.png");

    return (
      <View>
        <TouchableOpacity style={styles.row} onPress={() => this.selectDropdown(position)}>
          <Text style={styles.dropdownTitle}>{title}</Text>
          <Image source={dropdownIcon} style={styles.dropdownIcon} />
        </TouchableOpacity>
        {dropdownState[position] && <Text style={styles.description}>{description}</Text>}

        <View style={styles.divider} />
      </View>
    );
  };

  renderVideoView = () => {
    const { info } = this.state;
    const { data } = this.props;
    const showVideo = info != undefined && info.videourl ? true : false;
    const videoUrl = showVideo ? info.videourl : "";
    // const tempUrl = "https://www.youtube.com/watch?v=Vmd3Q8kr8Io";

    if (showVideo) {
      return (
        <View style={styles.videoRow}>
          <TouchableOpacity onPress={() => this.openVideo(videoUrl)}>
            <ImageBackground
              // source={{ uri: thumbnail }}
              source={require("../../../assets/icons/video-bg.jpg")}
              style={styles.thumbnail}
            >
              <Image source={require("../../../assets/icons/youtube.png")} style={styles.youtubeIcon} />
            </ImageBackground>
          </TouchableOpacity>

          <View style={styles.videoColumn}>
            <Text style={styles.candlestickName}>
              {data[0]}
              {/* Dragonfly Doji Candlestick */}
            </Text>
            <Image source={require("../../../assets/icons/youtube-logo.png")} style={styles.youtubeLogo} />
          </View>
        </View>
      );
    }
  };

  render() {
    const { info, isRefreshing } = this.state;
    const { data } = this.props;

    const title = data[0] ? data[0] : "";

    const showMeaning = info != undefined && info.text ? true : false;
    const meaning = showMeaning ? info.text : "";

    const showSignificance = info != undefined && info.significance ? true : false;
    const significance = showSignificance ? info.significance : "";

    // significance: null
    // text: "The word marubozu means “bald head” in Japanese, and this is reflected in the candlestick’s lack of wicks. The Black Marubozu is a one day bearish pattern. Here the open is equal to the day high and the close is equal to the day low.
    // ↵
    // ↵It is a long black (down, or red on the charts) candle, with little to zero upper or lower shadows. The pattern shows that sellers controlled the trading day from open to close."
    // videourl: null

    return (
      <RBSheet ref={(ref) => (this.RBSheet = ref)} height={hp(100)} closeOnDragDown={true}>
        <View style={styles.statusbar} />
        <Toolbar title={title} {...this.props}>
          <ImageButton source={require("../../../assets/icons/cross-white.png")} onPress={() => this.RBSheet.close()} />
        </Toolbar>

        <ScrollView style={styles.container}>
          {showMeaning && this.renderDropDown("Meaning", meaning, 0)}
          {showSignificance && this.renderDropDown("Significance", significance, 1)}
          {this.renderVideoView()}
        </ScrollView>
        {/* </View> */}

        {isRefreshing && <LoadingView />}
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};
export default connect(mapStateToProps, null, null, { forwardRef: true })(StockCandlestickDetail);

const styles = StyleSheet.create({
  container: {
    // flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingTop: wp(1),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
  },
  dropdownTitle: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black,
    marginRight: wp(4),
    marginVertical: wp(4),
  },
  dropdownIcon: {
    // width: wp(4.5),
    // height: wp(4.5),
    resizeMode: "contain",
    marginRight: wp(1),
  },
  description: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginBottom: wp(4),
    lineHeight: wp(5.5),
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
    // marginVertical: wp(1)
  },
  videoRow: {
    flexDirection: "row",
    marginTop: wp(6),
  },
  thumbnail: {
    width: wp(36),
    height: wp(20),
    alignItems: "center",
    justifyContent: "center",
    resizeMode: "cover",
  },
  youtubeIcon: {
    // width: wp(10),
    // height: wp(10),
    resizeMode: "contain",
  },
  videoColumn: {
    marginStart: wp(3),
  },
  candlestickName: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.greyishBrown,
  },
  youtubeLogo: {
    marginTop: wp(2.2),
  },
  statusbar: {
    // flex: 1,
    height: getStatusBarHeight(),
    backgroundColor: colors.primary_color,
  },
});
