import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import VolumeAverageCard from "./VolumeAverageCard";

export default class VolumeAverageList extends React.PureComponent {
  listRef = null;

  constructor(props) {
    super(props);
    this.state = { currentPosition: 0 };
  }

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState(
        { currentPosition: viewableItems[0].index },
        () => this.listRef && this.listRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  renderPageIndicator = () => {
    return (
      <View style={styles.pageIndicator}>
        <View style={this.state.currentPosition == 0 ? styles.pageIndicatorActive : styles.pageIndicatorInactive} />
        <View style={this.state.currentPosition == 1 ? styles.pageIndicatorActive : styles.pageIndicatorInactive} />
      </View>
    );
  };

  render() {
    const { data } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.pageIndicatorRow}>
          <Text style={styles.heading}>VOLUME & MOVING AVERAGES</Text>
          {this.renderPageIndicator()}
        </View>

        <FlatList
          ref={(ref) => (this.listRef = ref)}
          contentContainerStyle={styles.list}
          data={data}
          extraData={this.props}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          renderItem={({ item, index }) => <VolumeAverageCard data={item} />}
          keyExtractor={(item, index) => index.toString()}
          removeClippedSubviews={false}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    marginTop: hp(3.9),
    // flex: 1,
  },
  heading: {
    flex: 1,
    marginLeft: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  // list: {

  //   paddingHorizontal: wp(3),
  //   paddingVertical: wp(1)
  // },
  list: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(1),
    //   backgroundColor:colors.blueViolet,
    //   margin: wp(3),
    //   backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 2,
    elevation: 2,
  },
  pageIndicator: {
    flexDirection: "row",
    marginRight: wp(3),
    alignItems: "center",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  pageIndicatorRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: wp(3),
  },
  divider: {
    flex: 1,
    height: 1.5,
    opacity: 0.3,
    backgroundColor: colors.greyishBrown,
  },
});
