import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, FlatList, ScrollView } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { ICON_CDN } from "../../../utils/Constants";
import { roundNumber } from "../../../utils/Utils";

export default class MomentumItem extends React.PureComponent {
  getBackgroundStyle = (value) => {
    if (value == "positive") return { backgroundColor: colors.green };
    else if (value == "negative") return { backgroundColor: colors.red };
    else if (value == "neutral") return { backgroundColor: colors.squash };
    else return { backgroundColor: colors.steelGrey };
  };

  getTextStyle = (value) => {
    if (value == "positive") return { color: colors.green };
    else if (value == "negative") return { color: colors.red };
    else if (value == "neutral") return { color: colors.squash };
    else return { color: colors.steelGrey };
  };

  render() {
    const { mainItem, data, onSelect } = this.props;

    const labelStyle = mainItem ? [styles.label, styles.whiteText] : styles.label;

    const descriptionStyle = mainItem ? [styles.description, styles.whiteText] : styles.description;

    const scoreBgStyle = mainItem
      ? [styles.scoreContainer, styles.whiteBackground]
      : [styles.scoreContainer, this.getBackgroundStyle(data.color)];

    const scoreStyle = mainItem ? [styles.score, this.getTextStyle(data.color)] : styles.score;

    const infoIcon = mainItem
      ? require("../../../assets/icons/info-white.png")
      : require("../../../assets/icons/info-blue.png");

    const showInfoIcon = data.st && data.lt ? true : false;

    // const value = data.value;
    const value = data.value != null && !isNaN(data.value) ? roundNumber(data.value) : " - ";

    return (
      <View style={styles.container}>
        <View style={styles.labelContainer}>
          <Text style={labelStyle} numberOfLines={2} ellipsizeMode="tail">
            {data.name}
          </Text>

          {data.lt != "" && (
            <Text style={descriptionStyle} numberOfLines={2} ellipsizeMode="tail">
              {data.st}
            </Text>
          )}
        </View>

        {showInfoIcon && (
          <TouchableOpacity onPress={() => onSelect(data)}>
            <Image source={infoIcon} style={styles.infoIcon} />
          </TouchableOpacity>
        )}

        <View style={scoreBgStyle}>
          <Text style={[scoreStyle]}>{value}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    padding: wp(3),
    alignItems: "center",
  },
  labelContainer: {
    flex: 1,
  },
  label: {
    fontSize: RF(2.2),
    color: colors.black,
    // fontWeight: "500"
  },
  description: {
    fontSize: RF(1.9),
    marginTop: 6,
    color: colors.greyishBrown,
  },
  infoIcon: {
    width: wp(5.5),
    height: wp(5.5),
    resizeMode: "contain",
    margin: wp(2.5),
  },
  scoreContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.7),
    minWidth: wp(20),
    backgroundColor: colors.red,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  score: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
  },
  whiteBackground: {
    backgroundColor: colors.white,
  },
  whiteText: {
    color: colors.white,
  },
  mainItemText: {
    color: colors.squash,
  },
});
