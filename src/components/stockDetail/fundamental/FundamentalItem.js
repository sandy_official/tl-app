import React from "react";
import { View, Image, Text, TouchableOpacity, StyleSheet, FlatList } from "react-native";
import ChartView from "react-native-highcharts";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { numberWithCommas } from "../../../utils/Utils";
import { CHART_OPTIONS } from "../../../utils/ChartConfigs";

export default class FundamentalItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { indicatorPos: 0 };
  }

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) this.setState({ indicatorPos: viewableItems[0].index });
  };

  _viewabilityConfig = {
    itemVisiblePercentThreshold: 50,
  };

  renderPageIndicator = (data) => {
    const { indicatorPos } = this.state;

    if (data.length > 1)
      return (
        <View style={styles.pageIndicator}>
          {data.map((item, index) => {
            const indicatorStyle = indicatorPos == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

            return <View style={indicatorStyle} key={index.toString()} />;
          })}
        </View>
      );
  };

  renderChartList = (item, itemPos, pIndex) => {
    const { getChartConfig, onMessage } = this.props;
    const insight = item.insight && item.insight.longtext ? item.insight.longtext : "";
    const insightIcon =
      item.insight && item.insight.color == "positive"
        ? require("../../../assets/icons/arrow-green.png")
        : require("../../../assets/icons/arrow-red.png");

    const showInsight = insight ? true : false;

    return (
      <View style={styles.chartListContainer}>
        {showInsight && (
          <View style={styles.insightRow}>
            <Image source={insightIcon} style={styles.insightIcon} />
            <Text numberOfLines={2} style={styles.chartInsight}>
              {insight}
            </Text>
          </View>
        )}
        {/* {this.renderPageIndicator([{}])} */}

        {/* For now only one chart required */}

        {/* <FlatList
          contentContainerStyle={styles.chartList}
          horizontal
          showsHorizontalScrollIndicator={false}
          data={[{}]}
          extraData={this.state}
          renderItem={this.renderChart}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
        /> */}

        <View style={styles.chartList}>
          <View style={(styles.card, styles.chartCard)}>
            {/* <Text style={styles.chartTitle}>
              Title of graph sent by sever goes here
            </Text> */}
            {/* <View style={styles.chart} /> */}
            <ChartView
              style={styles.chart}
              config={getChartConfig(false, itemPos, pIndex, item.unique_name + itemPos)}
              options={CHART_OPTIONS}
              onMessage={onMessage}
            />
          </View>
        </View>
      </View>
    );
  };

  renderFundamentalItem = (item, itemPos, pIndex) => {
    const {
      selectedIndicator,
      selectedChart,
      selectedTab,
      getYoy,
      getChartConfig,
      showInfo,
      setChartOption,
      setIndicator,
    } = this.props;
    var Highcharts = "Highcharts";
    // console.log("fundamental item", item);
    // console.log("index", itemPos, item);

    const indicatorContainerStyle = [styles.leftCol, styles.row, styles.alignCenter];
    const showDropdownIcon = item.children ? item.children.length > 0 : false;
    const showInformation = selectedTab == "Ratios" && "detail" in item;
    const showChildren = selectedIndicator == item.unique_name;
    const showChartList = selectedChart == item.unique_name;

    const name = item.data ? item.data.sstr : null;
    const value = item.value ? numberWithCommas(item.value) : "-";
    const yoyChange = getYoy(item.unique_name, itemPos, pIndex);
    const yoyFormatted = yoyChange && !isNaN(yoyChange) ? "(" + yoyChange + "%)" : "";

    const yoyChangeStyle = yoyChange >= 0 ? styles.amountPercent : [styles.amountPercent, styles.redText];
    const indicatorStyle = pIndex == null ? styles.indicator : styles.subIndicator;
    const dropdownIcon = showChildren
      ? require("../../../assets/icons/arrow-up-blue.png")
      : require("../../../assets/icons/arrow-down-blue.png");

    if (name != null) {
      return (
        <View>
          <View style={styles.listItem}>
            <TouchableOpacity
              style={indicatorContainerStyle}
              onPress={() => setIndicator(item.unique_name)}
              disabled={item.children && item.children.length == 0}
            >
              {showDropdownIcon ? (
                <Image source={dropdownIcon} style={styles.dropdownIcon} />
              ) : (
                <View style={styles.dropdownIcon} />
              )}
              <Text style={indicatorStyle}>{name}</Text>
            </TouchableOpacity>

            {showInformation && (
              <TouchableOpacity
                style={{ paddingRight: wp(2), alignItems: "center" }}
                onPress={() => showInfo(item.data)}
              >
                <Image source={require("../../../assets/icons/info-blue.png")} style={styles.infoIcon} />
              </TouchableOpacity>
            )}
            <View style={[styles.centerCol, styles.alignCenter]}>
              <Text style={[styles.amount]} numberOfLines={2}>
                {value}
              </Text>
              <Text style={yoyChangeStyle}>{yoyFormatted}</Text>
            </View>
            <TouchableOpacity
              style={[styles.rightCol, styles.alignCenter]}
              onPress={() => setChartOption(item.unique_name)}
            >
              {/* <View style={styles.chartThumbnail} /> */}
              <ChartView
                style={styles.chartThumbnail}
                config={getChartConfig(true, itemPos, pIndex, item.unique_name + itemPos)}
                options={CHART_OPTIONS}
              />
            </TouchableOpacity>
          </View>
          {showChildren && (
            <View>
              <View style={styles.divider} />
              <FlatList
                showsVerticalScrollIndicator={false}
                data={item.children}
                renderItem={({ item, index }) => this.renderFundamentalItem(item, index, itemPos)}
                keyExtractor={(item, index) => item.unique_name}
                ItemSeparatorComponent={() => <View style={styles.divider} />}
                removeClippedSubviews={false}
              />
            </View>
          )}

          {showChartList && (
            <View>
              <View style={styles.divider} />
              {this.renderChartList(item, itemPos, pIndex)}
            </View>
          )}
        </View>
      );
    } else return null;
  };

  render() {
    const { data, index } = this.props;

    return this.renderFundamentalItem(data, index, null);
  }
}

const styles = StyleSheet.create({
  pageIndicator: {
    flexDirection: "row",
    // marginTop: wp(6)
  },
  pageIndicatorActive: {
    width: wp(1.8),
    height: wp(1.8),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.3),
    height: wp(1.3),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  divider: {
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  chartThumbnail: {
    width: wp(22),
    height: hp(9),
    alignSelf: "flex-end",
    overflow: "hidden",
  },
  chartCard: {
    padding: wp(3),
    // paddingVertical: wp(1),
    // margin: wp(3),
    marginHorizontal: wp(1),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 8,
    shadowOpacity: 2,
    elevation: 2,
  },
  // chartCard: {
  //   width: wp(90),
  //   height: hp(40),
  //   marginHorizontal: wp(1)
  // },
  chart: {
    width: wp(86),
    height: hp(30),
    overflow: "hidden",
  },
  chartList: {
    paddingVertical: wp(3),
    paddingHorizontal: wp(2),
  },
  chartListContainer: {
    backgroundColor: colors.white,
    paddingTop: wp(3),
    paddingBottom: wp(3),
  },
  chartInsight: {
    flex: 1,
    marginHorizontal: wp(2),
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  chartTitle: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(4),
  },
  insightRow: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: wp(3),
    marginBottom: wp(3),
  },
  insightIcon: {
    width: wp(2.5),
    height: wp(2.5),
    resizeMode: "contain",
  },
  lightBlueBg: {
    opacity: 0.08,
    backgroundColor: "rgba(36,83,161,0.03)",
  },
  infoIcon: {
    width: wp(5),
    height: wp(5),
    resizeMode: "contain",
  },
  redText: {
    color: colors.red,
  },

  yoy: {
    marginTop: 5,
    textAlign: "right",
  },
  alignCenter: {
    alignSelf: "center",
    alignItems: "center",
  },
  amount: {
    fontSize: RF(2.2),
    textAlign: "right",
    color: colors.black,
    alignSelf: "flex-end",
  },
  amountPercent: {
    fontSize: RF(1.9),
    textAlign: "right",
    color: colors.green,
    marginTop: 5,
    alignSelf: "flex-end",
  },
  dropdownIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    marginRight: 4,
  },
  listItem: {
    flexDirection: "row",
    padding: wp(3),
    alignItems: "center",
  },
  indicator: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  subIndicator: {
    fontSize: RF(2),
    color: colors.greyishBrown,
  },
  leftCol: {
    flex: 1.2,
    marginRight: wp(3),
    alignSelf: "flex-end",
    // flexWrap: "wrap"
  },
  centerCol: {
    flex: 1,
    alignSelf: "flex-end",
  },
  rightCol: {
    flex: 1,
    marginLeft: wp(2),
    justifyContent: "flex-end",
    alignSelf: "flex-end",
    alignItems: "flex-end",
  },
  row: {
    flexDirection: "row",
  },
});
