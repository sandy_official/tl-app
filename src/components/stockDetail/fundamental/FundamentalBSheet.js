import React from "react";
import { View, StyleSheet, Text, ScrollView, Image, TouchableWithoutFeedback, TouchableOpacity } from "react-native";
import { colors } from "../../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";

const data = {
  results: { title: "Select a duration:", options: ["Quarterly", "Annual"] },
  financial: {
    title: "Select a financial:",
    options: ["Cash Flow", "Balance Sheet"],
  },
  ratios: {
    title: "Select a ratio:",
    options: ["Liquidity", "Profitability", "Valuation", "Cash Flow", "Return"],
  },
};

export class FundamentalBSheet extends React.PureComponent {
  componentDidMount() {
    const { type } = this.props;
    //   this.RBSheet.open();
    this.setState({ selectedOption: data[type].options[0] });
  }

  constructor(props) {
    super(props);
    this.state = { selectedOption: "" };
  }

  closeDialog = () => {
    this.RBSheet.close();
  };

  setSelectedOption = (option) => {
    const { onSelect } = this.props;

    onSelect(option);
    this.setState({ selectedOption: option });
    this.closeDialog();
  };

  renderOption = (option) => {
    const { selectedOption } = this.state;

    const optionStyle = selectedOption === option ? [styles.radioButton, styles.radioButtonActive] : styles.radioButton;
    const radioButtonStyle = selectedOption === option ? styles.radioButtonCircle : styles.hide;
    const optionTextStyle = selectedOption == option ? styles.dropdownOptionActive : styles.dropdownOption;

    return (
      <View key={option}>
        <TouchableWithoutFeedback onPress={() => this.setSelectedOption(option)}>
          <View style={styles.dropdownOptionContainer}>
            <View style={optionStyle}>
              <View style={radioButtonStyle} />
            </View>
            <Text style={[optionTextStyle]}>{option}</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  renderHeader = () => {
    const { type } = this.props;
    const crossIcon = require("../../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={[styles.title]} numberOfLines={2} ellipsizeMode="tail">
          {data[type].title}
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderOptionList = () => {
    const { type } = this.props;
    return data[type].options.map((option) => this.renderOption(option));
  };

  render() {
    const { type } = this.props;
    const cardHeight = type == "ratios" ? hp(46) : hp(30);

    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        closeOnDragDown={false}
        height={cardHeight}
        duration={300}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {this.renderOptionList()}
          <View style={styles.extraSpace} />
        </View>
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2.2),
    color: colors.black,
    paddingBottom: hp(2),
  },
  extraSpace: {
    paddingBottom: ifIphoneX ? hp(2) : 8,
  },
  row: {
    height: hp(6),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginBottom: wp(2.5),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  dropdownOptionContainer: {
    flexDirection: "row",
    paddingVertical: wp(2.5),
    alignItems: "center",
  },
  dropdownOption: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    marginLeft: 11,
  },
  dropdownOptionActive: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "500",
    marginLeft: 11,
  },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  radioButtonActive: {
    borderColor: colors.primary_color,
  },
  radioButtonCircle: {
    width: wp(2.4),
    height: wp(2.4),
    borderRadius: wp(1.2),
    backgroundColor: colors.primary_color,
  },
  hide: {
    display: "none",
  },
});
