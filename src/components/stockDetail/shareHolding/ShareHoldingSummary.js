import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import ProgressBar from "../../custom/ProgressBar";
import { colors } from "../../../styles/CommonStyles";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import Slider from "../../../libs/react-native-slider";
import RF from "react-native-responsive-fontsize";
import { ICON_CDN } from "../../../utils/Constants";
import SuperstarQuarterList from "../../superstar/SuperstarQuarterList";
import { FlatList } from "react-native-gesture-handler";
import { roundNumber } from "../../../utils/Utils";

const SUMMARY_COLORS = {
  Promoter: colors.blueViolet,
  "Mutual Funds": colors.hotMagenta,
  FII: colors.vibrantPurple,
  "Other Institutions": colors.squash,
  Individual: colors.barbiePink,
  other: colors.primary_color,
};

export default class ShareHoldingSummary extends React.PureComponent {
  constructor(props) {
    super(props);
    console.warn("quarter", props);
    this.state = { quarterString: props.quarter, quarterMonth: props.quarterName };
  }

  setQuarterString = (quarter) => {
    if (quarter[1] != this.state.quarterString) {
      this.props.onQuarterChange(quarter);
      // this.refs.toast.show("Quarter Changed", 1000);

      this.setState(
        { quarterString: quarter[1], quarterMonth: quarter[0] }
        // () => {
        //   this.onRefresh();
        // }
      );
    }
  };

  renderQuarterButton = (quarter) => {
    const quarterButtonStyle =
      this.state.quarterString == quarter[1] ? styles.quarterButtonActive : styles.quarterButton;

    const quarterTextStyle =
      this.state.quarterString == quarter[1] ? styles.quarterButtonTextActive : styles.quarterButtonText;

    return (
      <TouchableOpacity onPress={() => this.setQuarterString(quarter)}>
        <View style={quarterButtonStyle}>
          <Text style={quarterTextStyle}>{quarter[0]}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderQuarterButtonsRow = () => {
    const { quarterMonth, quarterString } = this.state;
    const { dates } = this.props;

    if (dates.length > 0) {
      let date1 = dates[0];
      let date2 = dates[1];

      let newSelectedQuarter = [quarterMonth, quarterString];

      let date3 = quarterString
        ? quarterString != date1[1] && quarterString != date2[1]
          ? newSelectedQuarter
          : date2
        : date2;

      return (
        <View style={styles.quarterButtonsContainer}>
          {this.renderQuarterButton(date1)}
          {this.renderQuarterButton(date3)}

          <TouchableOpacity onPress={() => this.refs.QuarterList.open(quarterString)}>
            <Text style={styles.showMore}>Show more</Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderSummaryRow = ({ item, index }) => {
    const { showHistory } = this.props;

    const holdingPercent = !isNaN(item.Holding) ? roundNumber(parseFloat(item.Holding)) : item.Holding;

    const itemColor = item.Type in SUMMARY_COLORS ? SUMMARY_COLORS[item.Type] : SUMMARY_COLORS.other;
    // console.log("Summary holding id", item);
    const showHistoryIcon = item.holdingId ? true : false;

    return (
      <View style={styles.row}>
        <Text style={styles.promoter}>{item.Type}</Text>
        <View style={styles.progressBar}>
          <Slider
            minimumValue={0}
            maximumValue={100}
            value={holdingPercent}
            minimumTrackTintColor={itemColor}
            thumbTintColor={itemColor}
            disabled
          />
        </View>
        {showHistoryIcon ? (
          <TouchableOpacity onPress={() => showHistory(item)}>
            <Image source={require("../../../assets/icons/history-blue.png")} style={styles.recentIcon} />
          </TouchableOpacity>
        ) : (
          <View style={{ marginLeft: wp(7) }} />
        )}
      </View>
    );
  };

  render() {
    const { summary, dates, hide } = this.props;
    const { quarterString } = this.state;

    return (
      <View style={{ backgroundColor: colors.whiteTwo }}>
        <Text style={styles.heading}>SHAREHOLDING SUMMARY</Text>
        {this.renderQuarterButtonsRow()}

        {!hide && (
          <FlatList
            data={summary}
            extraData={this.props}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderSummaryRow}
            keyExtractor={(item, index) => index.toString()}
          />
        )}

        <SuperstarQuarterList
          ref="QuarterList"
          {...this.props}
          // selected={quarterString}
          data={dates}
          onPress={this.setQuarterString}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    borderBottomWidth: 1,
    borderBottomColor: "rgba(179,179,179,0.2)",
    backgroundColor: colors.white,
  },
  heading: {
    marginLeft: wp(3),
    // marginBottom: hp(1),
    marginTop: hp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  promoter: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  progressBar: {
    flex: 1.5,
    overflow: "hidden",
    marginHorizontal: wp(3),
  },
  recentIcon: {
    marginLeft: wp(2),
    marginVertical: wp(2),
  },
  quarterButton: {
    // minWidth: wp(20),
    paddingHorizontal: wp(2),
    paddingVertical: wp(2),
    marginRight: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  quarterButtonText: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
  },
  showMore: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
    paddingVertical: wp(2),
    paddingRight: wp(2),
  },
  quarterButtonActive: {
    minWidth: wp(20),
    paddingHorizontal: wp(2),
    paddingVertical: wp(2),
    marginRight: wp(3),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  quarterButtonTextActive: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.white,
  },
  quarterButtonsContainer: {
    flex: 1,
    flexDirection: "row",
    margin: wp(3),
    alignItems: "center",
    // justifyContent: 'center',
  },
});
