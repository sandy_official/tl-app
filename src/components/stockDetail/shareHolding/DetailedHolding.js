import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, TouchableWithoutFeedback, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ICON_CDN } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

const holdingList = [
  // {label}
];

export default class DetailedHolding extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      // showSuperstarHolding: false,
      // showPromoterHolding: false,
      // showInstitutionHolding: false,
      selectedDropdown: ""
    };
  }

  gotoSuperstar = shareHolder => {
    const superstarObj = {
      superstarId: -1,
      superstarString: shareHolder.holding_name,
      superstarName: shareHolder.holding_name,
      holdingId: shareHolder.holdingId,
      hideWarning: true
    };

    this.props.navigation.push("SuperstarInfo", superstarObj);
  };

  renderHoldingDropdownItems = ({ item, index }) => {
    const { showHistory } = this.props;

    const holderName = item.holding_name ? item.holding_name : "";
    const showPledgePercent = typeof item.shares_pledged_percentage == "number" && item.shares_pledged_percentage > 0;
    const pledgedPercent =
      showPledgePercent && APIDataFormatter("shares_pledged_percentage", item.shares_pledged_percentage);
    const holdingPercent = APIDataFormatter("holding_percent", item.holding_percent);

    const showHistoryIcon = item.holdingId ? true : false;

    // holding_id: 7105658;
    // holding_name: "GOVERNMENT OF SINGAPORE";
    // holding_percent: 1.98;

    return (
      <View style={styles.dropdownItem}>
        <TouchableWithoutFeedback onPress={() => this.gotoSuperstar(item)}>
          <View style={styles.holderCol}>
            <Text numberOfLines={1} ellipsizeMode="tail" style={styles.shareholderName}>
              {holderName}
            </Text>

            {showPledgePercent && (
              <Text style={styles.pledged}>
                Pledged: <Text style={styles.redText}>{pledgedPercent}</Text>
              </Text>
            )}
          </View>
        </TouchableWithoutFeedback>

        <View style={styles.holdingPercentContainer}>
          <Text style={styles.holdingPercent}>{holdingPercent}</Text>
        </View>

        {showHistoryIcon && (
          <TouchableOpacity onPress={() => showHistory(item)}>
            <Image source={require("../../../assets/icons/history-blue.png")} style={styles.holdingHistoryIcon} />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  selectDropdown = label => {
    if (label == this.state.selectedDropdown) this.setState({ selectedDropdown: "" });
    else this.setState({ selectedDropdown: label });
  };

  renderHoldingDropdown = label => {
    const { selectedDropdown } = this.state;
    const { institutional, superstar, promoter } = this.props;
    let listData = [];

    if (label == "Superstar Holding") listData = superstar;
    else if (label == "Promoter Holding") listData = promoter;
    else listData = institutional;

    const dropdownStyle =
      selectedDropdown == label ? [styles.dropdownLabelRow, styles.dropdownOpen] : styles.dropdownLabelRow;
    const dropdownIcon =
      selectedDropdown == label
        ? require("../../../assets/icons/arrow-up-black.png")
        : require("../../../assets/icons/arrow-down-black.png");

    return (
      <View>
        <TouchableOpacity style={dropdownStyle} onPress={() => this.selectDropdown(label)}>
          <Text style={styles.dropdownLabel}>{label}</Text>
          <Image source={dropdownIcon} style={styles.dropdownArrowIcon} />
        </TouchableOpacity>

        {selectedDropdown == label && (
          <FlatList
            contentContainerStyle={styles.list}
            data={listData}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderHoldingDropdownItems}
            ItemSeparatorComponent={() => <View style={styles.divider} />}
            keyExtractor={(item, index) => index.toString()}
          />
        )}
      </View>
    );
  };

  renderHoldings = () => {
    const { institutional, superstar, promoter } = this.props;

    return (
      <View>
        {superstar.length > 0 && (
          <View>
            {this.renderHoldingDropdown("Superstar Holding")}
            <View style={styles.divider} />
          </View>
        )}

        {promoter.length > 0 && (
          <View>
            {this.renderHoldingDropdown("Promoter Holding")}
            <View style={styles.divider} />
          </View>
        )}

        {institutional.length > 0 && this.renderHoldingDropdown("Institutional Holding")}
      </View>
    );
  };

  render() {
    return (
      <View style={{backgroundColor:colors.whiteTwo}}>
        <Text style={styles.heading}>DETAILED HOLDINGS</Text>
        {this.renderHoldings()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: ifIphoneX ? hp(3) : h[1]
  },
  heading: {
    marginLeft: wp(3),
    marginBottom: hp(2),
    marginTop: hp(4),
    fontSize: RF(2.4),
    fontWeight: "300",
    color:colors.black
  },
  dropdownLabelRow: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    alignItems: "center",
    backgroundColor: colors.white
  },
  list: {
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
     zIndex: 99
  },
  dropdownLabel: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "300",
    color:colors.black
  },
  dropdownArrowIcon: {
    // width: wp(5),
    // height: wp(5),
    resizeMode: "contain"
  },
  dropdownItem: {
    flexDirection: "row",
    padding: wp(3),
    backgroundColor: colors.white,
    alignItems: "center"
    // marginBottom: 1
  },
  shareholderName: {
    fontSize: RF(2.2),
    // fontWeight: "500",
    color: colors.primary_color,
    textTransform: "capitalize"
  },
  holdingPercentContainer: {
    minWidth: wp(15),
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(3),
    borderRadius: 3,
    alignItems: "center",
    justifyContent: "center"
  },
  holdingPercent: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    padding: 6
  },
  holdingHistoryIcon: {
    width: wp(6),
    height: wp(6),
    resizeMode: "contain",
    marginHorizontal: wp(1.5),
    marginVertical: wp(1)
  },
  divider: {
    flex: 1,
    height: 1,
    //  width: 0,
    marginHorizontal: wp(0),
    opacity: 0.2,
    backgroundColor: colors.greyish
   },
  holderCol: {
    flex: 1
  },
  pledged: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginTop: 6
  },
  redText: {
    color: colors.red
  },
  greenText: {
    color: colors.green
  },
  dropdownOpen: {
    backgroundColor: "rgba(36,83,161,0.03)"
  }
});
