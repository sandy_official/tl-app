import React from "react";
import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
// import RBSheet from "react-native-raw-bottom-sheet";
import Modal from "../../libs/react-native-modalbox";
import { ifIphoneX } from "react-native-iphone-x-helper";

export default class StockAddBottomSheet extends React.PureComponent {
  closeDialog = () => {
    this.RBSheet.close();
  };

  renderHeader = () => {
    const { stockName, actionText, closeIconPress } = this.props;

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={2} ellipsizeMode="tail">
          {actionText && actionText !== "" ? actionText : " Select an action for"} {stockName ? stockName : ""}
        </Text>

        <TouchableOpacity
          style={{ padding: wp(2) }}
          onPress={closeIconPress ? closeIconPress : () => this.closeDialog()}
        >
          <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { stockName, showAlert, showWatchlist, showPortfolio, modalMainView } = this.props;

    const modalStyle = {
      borderTopLeftRadius: wp(6),
      borderTopRightRadius: wp(6),
      backgroundColor: "white",
      height: hp(38)
    };

    return (
      <Modal
        ref={ref => (this.RBSheet = ref)}
        position={"bottom"}
        swipeToClose={false}
        style={[modalStyle, modalMainView]}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}

          {this.props.children ? (
            this.props.children
          ) : (
            <View>
              <TouchableOpacity onPress={() => showAlert()}>
                <Text style={styles.action}>Add Alert</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => showPortfolio()}>
                <Text style={styles.action}>Add to Portfolio</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => showWatchlist()}>
                <Text style={styles.action}>Add to Watchlist</Text>
              </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2.2),
    color: colors.black,
    paddingBottom: hp(2)
  },
  extraSpace: {
    paddingBottom: ifIphoneX ? hp(2) : 8
  },
  row: {
    // height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(5),
    marginTop: hp(2),
    marginRight: wp(4)
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain"
  },
  action: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    paddingVertical: wp(2.5)
  }
});
