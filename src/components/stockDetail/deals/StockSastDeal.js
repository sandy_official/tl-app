import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, formatNumber } from "../../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ICON_CDN } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockSastDeal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { expand: false };
  }

  gotoSuperstar = (shareHolder) => {
    const superstarObj = {
      superstarId: -1,
      superstarString: shareHolder.actor,
      superstarName: shareHolder.actor,
    };

    this.props.navigation.push("SuperstarInfo", superstarObj);
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {value}
      </Text>
    );
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderStockRows = (stock) => {
    // const { stock } = this.props;
    const quantity = APIDataFormatter("holding_after", stock.holding_after);
    const averagePrice = APIDataFormatter("average_price", stock.average_price);
    const percentTraded = APIDataFormatter("holdingP_change", stock.holdingP_change);

    return (
      <View style={styles.stockDetails}>
        <View style={[styles.detailRow, { flex: 1.4 }]}>
          {this.renderValue([styles.value, styles.left], quantity)}
          {/* stock.holding_after ? formatNumber(stock.holding_after) + "  (NSE)" : " - " + "  (NSE)" */}
          {this.renderLabel([styles.label, styles.left], "Quantity")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.center], averagePrice)}
          {this.renderLabel([styles.label, styles.center], "Average price")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue(this.getTextStylePercentTraded(stock.percent), percentTraded)}
          {/* {this.renderValue([styles.value, styles.right],  stock.percent ? stock.percent + "%" : "-")} */}
          {this.renderLabel([styles.label, styles.right], "%Traded")}
        </View>
      </View>
    );
  };

  getTextStylePercentTraded = (value) => {
    if (value) {
      return value >= 0 ? [styles.value, styles.right, styles.greenText] : [styles.value, styles.right, styles.redText];
    } else return [styles.value, styles.right];
  };

  renderButton = (label, positionStyle, icon, onPress) => {
    return (
      <View style={[positionStyle, styles.buttonContainer]}>
        <TouchableOpacity onPress={() => onPress()}>
          <View style={[styles.button]}>
            <Image source={icon} style={styles.buttonIcon} />
            <Text style={styles.buttonText}>{label}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  // renderButtonRow = () => {
  //   return (
  //     <View style={styles.buttonRow}>
  //       {this.renderButton("Alert", styles.left, { uri: ICON_CDN + "add-blue.png" }, () => {
  //         console.log("alert");
  //       })}

  //       {this.renderButton("Portfolio", styles.center, { uri: ICON_CDN + "add-blue.png" }, () => {
  //         console.log("portfolio");
  //       })}

  //       {this.renderButton("Watchlist", styles.right, { uri: ICON_CDN + "add-blue.png" }, () => {
  //         console.log("watchlist");
  //       })}
  //     </View>
  //   );
  // };

  renderOtherDetails = (stock) => {
    // const { stock } = this.props;
    const equityShare = APIDataFormatter("security_type_text", stock.security_type_text);
    const transactionType = " " + APIDataFormatter("transaction_type_text", stock.transaction_type_text);
    const actorCategory = APIDataFormatter("actor_category_text", stock.actor_category_text);
    const shareType = stock.get_sast_type_display == "Insider Trading" ? "IT" : stock.get_sast_type_display;

    const transactionTypeStyle =
      stock.transaction_type_text == "Acquisition"
        ? [styles.equityStatus]
        : [styles.equityStatus, { color: colors.red }];

    return (
      <View style={styles.otherDetails}>
        <View style={styles.otherDetailRow}>
          <Text style={styles.equityShare}>
            {equityShare}

            <Text style={transactionTypeStyle}>
              {transactionType}
              {/* Acquisition */}
            </Text>
          </Text>

          <Text style={styles.shareType}>{shareType}</Text>
        </View>

        <View style={styles.otherDetailRow}>
          <Text style={styles.employee}>{actorCategory}</Text>
          <Text style={styles.prefAllotment}>Pref Allotment</Text>
        </View>
      </View>
    );
  };

  renderCardItem = ({ item, index }) => {
    const reportDate = APIDataFormatter("report_date", item.report_date);
    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.gotoSuperstar(item)}>
          <View style={styles.StockCodeContainer}>
            <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
              {item.actor}
            </Text>
            <Text style={styles.tradeDate}>{reportDate}</Text>
          </View>
        </TouchableOpacity>

        {this.renderOtherDetails(item)}
        {this.renderStockRows(item)}
      </View>
    );
  };

  render() {
    const { stock } = this.props;

    return (
      <View>
        {/* {this.renderInsights()} */}

        <FlatList
          data={stock ? stock : []}
          extraData={this.props}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.list}
          renderItem={this.renderCardItem}
          keyExtractor={(item, index) => index.toString()}
          // ListFooterComponent={() => this.renderFooter()}
          removeClippedSubviews
          initialNumToRender={4}
          maxToRenderPerBatch={4}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: "2%",
    padding: wp(3),
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  StockCodeContainer: {
    flexDirection: "row",
    // marginBottom: hp(3)
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "300",
    marginRight: wp(4),
  },
  detailRow: {
    flex: 1,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: hp(2.2),
    marginBottom: 2,
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 4,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(3),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "300",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  buttonRow: {
    flexDirection: "row",
    marginTop: hp(2.5),
    marginBottom: hp(1),
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    height: hp(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
  },
  buttonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 5,
  },
  hide: {
    display: "none",
  },
  stockDetails: {
    flexDirection: "row",
  },
  otherDetails: {
    // marginTop: 4
  },
  otherDetailRow: {
    flexDirection: "row",
    marginTop: 5,
    alignItems: "center",
  },
  equityShare: {
    flex: 1,
    fontSize: RF(1.9),
    // fontWeight: "100"
  },
  equityStatus: {
    fontWeight: "300",
    color: colors.green,
  },
  tradeDate: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  // superstarName: {
  //   flex: 1,
  //   fontSize: RF(1.5),
  //   color: colors.steelGrey,
  //   marginRight: wp(10)
  // },
  // marketGift: {
  //   fontSize: RF(1.5),
  //   color: colors.steelGrey
  // },

  equityShare: {
    flex: 1,
    fontSize: RF(1.8),
    fontWeight: "100",
  },
  equityStatus: {
    fontWeight: "300",
    color: colors.green,
  },
  shareType: {
    fontSize: RF(1.5),
    color: "purple",
    fontWeight: "300",
  },
  employee: {
    flex: 1,
    // marginTop: 4,
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  prefAllotment: {
    // marginTop: 4,
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.green,
  },
  itText: {
    color: "purple",
  },
  sastText: {
    color: "violet",
  },

  insight: {
    fontSize: RF(2),
    padding: wp(3),
    marginBottom: 1,
    backgroundColor: colors.white,
  },
});
