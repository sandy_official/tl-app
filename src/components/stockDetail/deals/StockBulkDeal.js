import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, formatNumber, numberWithCommas } from "../../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ICON_CDN, MONTHS_SHORT } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockBulkDeal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { expand: false };
  }

  gotoSuperstar = (shareHolder) => {
    const superstarObj = {
      superstarId: -1,
      superstarString: shareHolder.client_name,
      superstarName: shareHolder.client_name,
    };

    this.props.navigation.push("SuperstarInfo", superstarObj);
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {value}
      </Text>
    );
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderStockRows = (stock) => {
    const quantity = APIDataFormatter("quantity", stock.quantity);
    const price = APIDataFormatter("price", stock.price);
    const stockPercent = APIDataFormatter("traded_percent", stock.traded_percent);

    const stockExchange = stock.exchange ? " (" + stock.exchange + ")" : " - ";
    const quantityExchange = quantity + stockExchange;

    const stockPercentStyle = [styles.value, styles.right];

    return (
      <View style={styles.stockDetails}>
        <View style={[styles.detailRow, { flex: 1.4 }]}>
          {this.renderValue([styles.value, styles.left], quantityExchange)}
          {this.renderLabel([styles.label, styles.left], "Quantity")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.center], price)}
          {this.renderLabel([styles.label, styles.center], "Average price")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue(stockPercentStyle, stockPercent)}
          {/* {this.renderValue([styles.value, styles.right],  stock.percent ? stock.percent + "%" : "-")} */}
          {this.renderLabel([styles.label, styles.right], "%Traded")}
        </View>
      </View>
    );
  };

  // getPercentTradedStyle = value => {
  //   return value > 0
  //     ? [styles.value, styles.right, styles.greenText]
  //     : [styles.value, styles.right, styles.redText];
  // };

  renderButton = (label, positionStyle, icon, onPress) => {
    return (
      <View style={[positionStyle, styles.buttonContainer]}>
        <TouchableOpacity onPress={() => onPress()}>
          <View style={[styles.button]}>
            <Image source={icon} style={styles.buttonIcon} />
            <Text style={styles.buttonText}>{label}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderButtonRow = () => {
    return (
      <View style={styles.buttonRow}>
        {this.renderButton("Alert", styles.left, { uri: ICON_CDN + "add-blue.png" }, () => {
          console.log("alert");
        })}

        {this.renderButton("Portfolio", styles.center, { uri: ICON_CDN + "add-blue.png" }, () => {
          console.log("portfolio");
        })}

        {this.renderButton("Watchlist", styles.right, { uri: ICON_CDN + "add-blue.png" }, () => {
          console.log("watchlist");
        })}
      </View>
    );
  };

  renderOtherDetails = (stock) => {
    const stockActionStyle =
      stock.get_action_display == "Sell" ? [styles.equityStatus, { color: colors.red }] : styles.equityStatus;

    return (
      <View style={styles.otherDetails}>
        <View style={styles.otherDetailRow}>
          <Text style={styles.equityShare}>
            {stock.get_deal_type_display}
            <Text style={stockActionStyle}>{" " + stock.get_action_display}</Text>
          </Text>
        </View>
      </View>
    );
  };

  // formatTradeDate = value => {
  //   if (typeof value == "string") {
  //     const date = value.split("-");
  //     return date[2] + " " + MONTHS_SHORT[date[1] - 1] + " " + date[0];
  //   } else value;
  // };

  renderCardItem = ({ item, index }) => {
    const tradeDate = APIDataFormatter("date", item.date);
    // console.log("item", item);
    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.gotoSuperstar(item)}>
          <View style={styles.StockCodeContainer}>
            <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
              {item.client_name}
            </Text>
            <Text style={styles.tradeDate}>{tradeDate}</Text>
          </View>
        </TouchableOpacity>

        {this.renderOtherDetails(item)}
        {this.renderStockRows(item)}

        {/* {this.state.expand && this.renderButtonRow()} */}
      </View>
    );
  };

  render() {
    const { stock } = this.props;

    return (
      <FlatList
        data={stock}
        extraData={this.props}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
        renderItem={this.renderCardItem}
        keyExtractor={(item, index) => index.toString()}
        // ListFooterComponent={() => this.renderFooter()}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
      />
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: "2%",
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 2,
    elevation: 2,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  StockCodeContainer: {
    flexDirection: "row",
    // marginBottom: hp(3)
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "300",
    marginRight: wp(4),
    textTransform: "capitalize",
  },
  detailRow: {
    flex: 1,
    //  minHeight:50
    // flexDirection:'column'
    // marginTop: wp(2),
    // marginBottom: 8
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 5,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(3),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "300",
    marginBottom: 3,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  buttonRow: {
    flexDirection: "row",
    marginTop: hp(2.5),
    marginBottom: hp(1),
  },
  buttonContainer: {
    flex: 1,
  },
  button: {
    height: hp(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
  },
  buttonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 5,
  },
  hide: {
    display: "none",
  },
  stockDetails: {
    flexDirection: "row",
    marginTop: wp(4),
  },
  otherDetails: {
    marginTop: 5,
  },
  otherDetailRow: {
    flexDirection: "row",
    marginBottom: 2,
    // marginTop: 5
  },
  equityShare: {
    flex: 1,
    fontSize: RF(1.9),
    // fontWeight: "100"
  },
  equityStatus: {
    fontWeight: "300",
    color: colors.green,
  },
  tradeDate: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  greenText: {
    color: colors.green,
  },
  redText: {
    color: colors.red,
  },
  // superstarName: {
  //   flex: 1,
  //   fontSize: RF(1.5),
  //   color: colors.steelGrey,
  //   marginRight: wp(10)
  // },
  // marketGift: {
  //   fontSize: RF(1.5),
  //   color: colors.steelGrey
  // }
});
