/**
 * Libraries
 */
import React from "react";
import { View, Text, TouchableOpacity, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import ChartView from "react-native-highcharts";
import RF from "react-native-responsive-fontsize";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../../styles/CommonStyles";
import { CHART_OPTIONS, STOCK_OVERVIEW_CHART_CONFIG } from "../../../utils/ChartConfigs";

const CHART_TIMELINE = ["1D", "1W", "1M", "3M", "6M", "1Y", "MAX"];

export default class StockOverviewChart extends React.Component {
  chartConfig = JSON.parse(JSON.stringify(STOCK_OVERVIEW_CHART_CONFIG));
  chartConfigLive = {};
  chartConfigEOD = {};

  constructor(props) {
    super(props);
    this.state = {
      selectedTimeline: "3M",
      selectedOption: 3,
    };
  }

  componentWillMount() {
    this.generateChartConfigs();
  }

  componentDidUpdate(prevProps) {
    const { chart } = this.props;
    // console.log("componentDidUpdate chart", this.props);
    // console.log(
    //   "componentDidUpdate conditions",
    // Object.keys(prevProps.chart).length != Object.keys(chart).length,
    //   Object.keys(this.chartConfigEOD).length == 0,
    //   Object.keys(this.chartConfigLive).length == 0
    // );
    if (Object.keys(this.chartConfigEOD).length == 0 || Object.keys(this.chartConfigLive).length == 0) {
      this.generateChartConfigs();
    }
  }

  generateChartConfigs = () => {
    const { chart } = this.props;

    const eodData = "eod" in chart ? chart.eod : [];
    const liveData = "live" in chart ? chart.live : [];

    if (eodData.length > 0 || liveData.length > 0) {
      // console.log("componentDidUpdate creating chart configs");

      this.chartConfigEOD = this.getStockChartConfig(eodData);
      this.chartConfig = this.chartConfigEOD;
      this.chartConfigLive = this.getStockChartConfig(liveData, true);
      // Hack to re-render. TBFixed.
      this.setState({ selectedOption: 3 });
    }
  };

  setChartTimeline = (option, index) => {
    const { selectedOption } = this.state;
    //this.getStockChartConfig().rangeSelector.selected = 0;

    if (typeof this.chartConfig.rangeSelector != undefined) {
      index < 2 ? (this.chartConfig = { ...this.chartConfigLive }) : (this.chartConfig = { ...this.chartConfigEOD });

      if (this.chartConfig.rangeSelector) {
        this.chartConfig.rangeSelector.selected = index;
      }

      this.setState({ selectedTimeline: option, selectedOption: index });
    }
  };

  componentDidMount() {
    // const { chart } = this.props;
    // this.chartConfigEOD = this.getStockChartConfig(chart.eod);
    // this.chartConfig = this.chartConfigEOD;
    // this.chartConfigLive = this.getStockChartConfig(chart.live);
    // console.log("Mounting..");
    // // Hack to re-render. TBFixed.
    // this.setState({ selectedOption: 6 });
  }

  getStockChartConfig = (chart, isLiveData) => {
    let config = JSON.parse(JSON.stringify(STOCK_OVERVIEW_CHART_CONFIG));
    config.rangeSelector.selected = 3;

    var ohlc = [];
    var volume = [];
    var categories = [];
    let prices = [];

    chart.forEach((data) => {
      let d = Date.parse(data.date);

      ohlc.push([d, data.last_price]);
      volume.push([d, data.volume]);
      categories.push(d);
      // prices.push(data.close_prices);
    });

    var series = [];
    series.push({
      id: "LTP",
      name: "LTP",
      data: ohlc.reverse(),
      type: "area",
      dataGrouping: { enabled: false },
      tooltip: { valueDecimals: 1 },
      color: "#00378a",
      // Linear gradient used as a color option
      fillColor: {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
          [0, "#bdc9db"], // start
          [0.75, "#ffffff"], // middle
          [1, "#ffffff"], // end
        ],
      },
      threshold: null,
    });

    series.push({
      id: "VOL",
      name: "VOL",
      data: volume.reverse(),
      type: "column",
      yAxis: 1,
      dataGrouping: { enabled: false },
      color: colors.greyish,
      gridLineWidth: 0,
      allowPointSelect: true,
    });

    config.chart.backgroundColor = colors.whiteTwo;
    config.series = series;
    config.navigator = { enabled: false };

    config.yAxis[0].min = null;

    config.chart.events = {
        load:function(){
            var chart = this;
            yAxisMin = chart.yAxis[0].min;
            if(yAxisMin < 0){
                chart.yAxis[0].setExtremes(0);
            }
        }
    };

    config.yAxis[1] = { startOnTick: false, endOnTick: false, tickPositions: [] };

    config.xAxis = {
      crosshair: { width: 2, color: colors.steelGrey, dashStyle: "shortdot", snap: false },
      // tickPixelInterval: wp(18)
    };

    !isLiveData && (config.xAxis.tickPixelInterval = wp(18));

    config.tooltip = {
      followTouchMove: true,
      followPointer: true,
      positioner: function () {
        return { x: chart.plotLeft, y: chart.plotTop };
      },
      formatter: function () {
        var ltpVal = this.points[0].y.toFixed(2);
        var volVal =
          this.points[1].y > 1000
            ? Highcharts.numberFormat(this.points[1].y / 1000, 0) + "K"
            : Highcharts.numberFormat(this.points[1].y, 0);

        return (
          `<span style="font-size:12px">` +
          Highcharts.dateFormat("%e %b %Y, ", new Date(this.x)) +
          Highcharts.dateFormat("%I:%M", new Date(this.x)) +
          `<span style="text-transform: lowercase">` +
          Highcharts.dateFormat("%p ", new Date(this.x)) +
          `</span>` +
          // "&nbsp;" +
          // Highcharts.dateFormat("%e %b %Y", new Date(this.x)) +
          "\u00A0\u00A0\u00A0\u00A0\u00A0LTP: " +
          ltpVal +
          "\u00A0\u00A0\u00A0\u00A0\u00A0Vol: " +
          volVal +
          "</span>"
        );
      },
      useHTML: true,
      borderWidth: 0,
      borderRadius: 0,
      shadow: false,
      backgroundColor: "transparent",
      style: { fontSize: "14px" },
      followTouchMove: true,
    };

    //console.log("chart config", config);
    return config;
  };

  renderChartTimeline = () => {
    const { selectedTimeline } = this.state;

    return (
      <View style={styles.timelineRow}>
        {CHART_TIMELINE.map((item, index) => {
          const containerStyle = selectedTimeline == item ? styles.activeTimeline : {};
          const itemStyle = selectedTimeline == item ? [styles.timelineText, styles.whiteText] : styles.timelineText;

          return (
            <View style={{ flex: 1 }}>
              <TouchableOpacity style={containerStyle} onPress={() => this.setChartTimeline(item, index)}>
                <Text style={itemStyle}>{item}</Text>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  };

  renderStockChart = () => {
    const { chart } = this.props;
    var Highcharts = "Highcharts";
    const showChart = Object.keys(chart).length > 0;
    // if (showChart) {
    return (
      <View style={styles.chartContainer}>
        <View style={{ marginHorizontal: wp(3) }}>
          <ChartView
            style={styles.chart}
            config={this.chartConfig}
            options={CHART_OPTIONS}
            isFillRequiredFromOutside={true}
            stock
          />
        </View>
        {this.renderChartTimeline()}
        {/* <TouchableOpacity>
            <Text style={styles.advanceChart}>Interactive advanced chart</Text>
          </TouchableOpacity> */}
      </View>
    );
    // }
  };

  render() {
    return <View>{this.renderStockChart()}</View>;
  }
}

const styles = StyleSheet.create({
  timelineRow: {
    flexDirection: "row",
    marginVertical: wp(3),
    // paddingVertical: 7.6,
    paddingHorizontal: wp(1),
    backgroundColor: "rgba(36,83,161,0.03)",
    alignItems: "center",
    justifyContent: "center",
  },
  timelineText: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    textAlign: "center",
    letterSpacing: 1,
    paddingVertical: 12.8,
  },
  activeTimeline: {
    // paddingVertical: 12.8,
    marginVertical: -wp(3),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
    fontWeight: "500",
  },
  chartContainer: {
    // marginVertical: wp(6),
    marginTop: wp(6),
    justifyContent: "center",
  },
  chart: { width: wp(94), height: hp(30), overflow: "hidden" },
});
