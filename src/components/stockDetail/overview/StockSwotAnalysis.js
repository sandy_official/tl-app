/**
 * Libraries
 */
import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView, FlatList } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../../styles/CommonStyles";
import SwotInfoModal from "./SwotInfoModal";

export default class StockSwotAnalysis extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { showSwotDetails: false, show: false };
  }

  // componentWillMount() {
  //   console.log("swot mount", this.state.show);
  //   setTimeout(() => {
  //     this.setState({ show: true });
  //   }, 800);
  // }

  // componentWillUnmount() {
  //   console.log("swot unmount", this.state.show);
  //   this.setState({ show: false });
  // }

  showSwotDetailsModal = (label, color, data) => {
    this.refs.swotModal.open(label, color, data);
  };

  hideSwotDetailsModal = () => {
    this.refs.swotModal.RBSheet.close();
  };

  renderSwotItem = (label, color, data) => {
    const count = data && data.length ? data.length : 0;

    const countContainerStyle = [styles.swotPointsContainer, { backgroundColor: color }];

    return (
      <TouchableOpacity
        style={[styles.card, styles.swotCard]}
        onPress={() => this.showSwotDetailsModal(label, color, data)}
        disabled={count == 0}
      >
        <View style={styles.swotLabelContainer}>
          <Text style={styles.swotLabel} numberOfLines={1}>
            {label}
          </Text>
        </View>
        <View style={countContainerStyle}>
          <Text style={styles.swotPoints}>{count}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderSwotAnalyses = () => {
    const { swot } = this.props;

    return (
      <View>
        <Text style={styles.heading}>SWOT ANALYSIS</Text>
        <View style={styles.swotRow}>
          {this.renderSwotItem("Strength", colors.primary_color, swot.strengths)}
          {this.renderSwotItem("Weakness", colors.squash, swot.weaknesses)}
          {this.renderSwotItem("Opportunity", colors.green, swot.opportunities)}
          {this.renderSwotItem("Threat", colors.red, swot.threats)}
        </View>
      </View>
    );
  };

  render() {
    const { show } = this.state;
    // if (show) {
    return (
      <View>
        {this.renderSwotAnalyses()}
        <SwotInfoModal ref="swotModal" {...this.props} />
      </View>
    );
    // } else return null;
  }
}

const styles = StyleSheet.create({
  heading: {
    flex: 1,
    marginLeft: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    marginTop: hp(3),
    color: colors.black,
  },
  card: {
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  swotDetailCard: {
    marginVertical: wp(1),
    marginHorizontal: wp(3),
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  swotRow: {
    flexDirection: "row",
    paddingVertical: wp(2),
    paddingHorizontal: wp(3) - 3,
  },
  swotCard: {
    flex: 1,
    overflow: "hidden",
    marginVertical: wp(1),
    marginHorizontal: 3,
  },
  swotLabelContainer: {
    flex: 1,
    alignItems: "center",
    paddingTop: wp(4),
    paddingBottom: wp(2.2),
    paddingHorizontal: 3,
  },
  swotLabel: {
    fontSize: RF(1.9),
    fontWeight: "200",
    color: colors.black,
  },
  swotPointsContainer: {
    padding: 3,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.green,
  },
  swotPoints: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
  },
  swotDetailTitle: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    padding: wp(3),
  },
  swotListItem: {
    padding: wp(3),
  },
  swotScreener: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  swotScreenerDescription: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginTop: 6,
  },
  swotScreenerInsight: {
    fontSize: RF(1.9),
    color: colors.black,
    marginTop: 6,
  },
  swotDetail: {
    overflow: "hidden",
    maxHeight: hp(60),
    backgroundColor: colors.white,
    borderRadius: 6,
    // shadowColor: "rgba(77, 77, 77, 0.3)",
    // shadowOffset: {
    //   width: 0,
    //   height: 0
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1,
    // elevation: 2
  },
  swotDetailTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  crossIcon: {
    marginRight: wp(3),
  },
});
