import React from "react";
import { View, StyleSheet, Text, FlatList, Image, TouchableOpacity, ScrollView } from "react-native";
import { colors } from "../../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";

export default class SwotInfoModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { label: "", color: "", data: [] };
  }

  closeDialog = () => {
    this.RBSheet.close();
  };

  open = (label, color, data) => {
    this.setState({ label: label, color: color, data: data });
    this.RBSheet.open();
  };

  gotoScreener = (item) => {
    const { navigate } = this.props.navigation;

    let screener = {
      screenId: item.screen_id,
      screener: item.title,
      description: item.description,
      bookmarkId: "bookmarkId" in item ? item.bookmarkId : "",
      isBookmarkable: "isBookmarkable" in item ? item.isBookmarkable : false, // false for candlestick
    };

    this.closeDialog();
    navigate("ScreenerStocks", screener);
  };

  renderSwotListItem = ({ item, index }) => {
    let backtest = item.insight ? item.insight : "Not backtested";
    let showBacktest = item.insight ? true : false;
    let showDescription = item.description ? true : false;

    return (
      <View style={styles.swotListItem}>
        <TouchableOpacity onPress={() => this.gotoScreener(item)}>
          <Text style={styles.swotScreener}>{item.title} </Text>
        </TouchableOpacity>
        {showDescription && <Text style={styles.swotScreenerDescription}>{item.description}</Text>}
        {showBacktest && <Text style={styles.swotScreenerInsight}>{backtest}</Text>}
      </View>
    );
  };

  renderSwotDetailList = () => {
    const { data, label, color } = this.state;
    const headerStyle = [styles.swotDetailTitleContainer, { backgroundColor: color }];
    const crossIcon = require("../../../assets/icons/cross-white.png");

    return (
      <View>
        <View style={headerStyle}>
          <Text style={styles.swotDetailTitle}>{label}</Text>

          <TouchableOpacity onPress={this.closeDialog}>
            <Image source={crossIcon} style={styles.crossIcon} />
          </TouchableOpacity>
        </View>

        {/* <ScrollView> */}
        <View style={{ height: hp(80) }}>
          <FlatList
            data={data}
            // extraData={this.props}
            renderItem={this.renderSwotListItem}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => <View style={styles.divider} />}
            removeClippedSubviews={false}
          />
        </View>

        {/* </ScrollView> */}
      </View>
    );
  };

  render() {
    const { info } = this.props;
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        closeOnDragDown={false}
        height={hp(90)}
        duration={300}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        {/* {this.renderHeader()} */}
        {this.renderSwotDetailList()}
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(3),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  swotDetailTitle: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    padding: wp(3),
  },
  swotListItem: {
    paddingHorizontal: wp(5),
    paddingVertical: wp(5),
  },
  swotScreener: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  swotScreenerDescription: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginTop: 6,
  },
  swotScreenerInsight: {
    fontSize: RF(1.9),
    color: colors.black,
    marginTop: 6,
  },
  swotDetail: {
    overflow: "hidden",
    maxHeight: hp(60),
    backgroundColor: colors.white,
    borderRadius: 6,
  },
  swotDetailTitleContainer: {
    flexDirection: "row",
    alignItems: "center",
    paddingTop: hp(2),
    paddingHorizontal: wp(3),
  },
  divider: {
    flex: 1,
    height: 0.5,
    backgroundColor: colors.greyish,
  },
});
