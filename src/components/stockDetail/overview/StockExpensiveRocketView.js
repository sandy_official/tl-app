import React from "react";
import { View, Text, Image, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import StockActionsView from "../../../components/common/StockActionsView";
import { roundNumber } from "../../../utils/Utils";

const SUMMARY_COLORS = [
  colors.blueViolet,
  colors.hotMagenta,
  colors.vibrantPurple,
  colors.squash,
  colors.barbiePink,
  colors.primary_color,
  colors.green,
];

export default class StockExpensiveRocketView extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      // refreshing: false,
      alertActive: false,
      portfolioActive: false,
      watchlistActive: false,
      showSwotDetails: false,
      // selectedStock: {},
      // watchlistStocks: {},
    };
  }

  // shouldComponentUpdate(nextProps, nextState) {
  //   const oldStockKeys = Object.keys(this.props.stock).length;
  //   const newStockKeys = Object.keys(nextProps.stock).length;
  //   if (oldStockKeys != newStockKeys) return true;
  //   else return false;
  // }

  getDvmIcon = (value) => {
    if (value == "positive") return require("../../../assets/icons/arrow-up-filled.png");
    else if (value == "negative") return require("../../../assets/icons/arrow-down-filled.png");
    else return require("../../../assets/icons/arrow-mid-filled.png");
  };

  getDvmColor = (value) => {
    if (value == "positive") return colors.green;
    else if (value == "negative") return colors.red;
    else if (value == "neutral") return colors.squash;
    else return colors.steelGrey;
  };

  getDvmInfo = (index) => {
    const { stock } = this.props;

    let score = 0;
    let insight = "";
    let color = "";

    switch (index) {
      case 0:
        score = stock.d_value;
        insight = stock.d_insight;
        color = stock.d_color;
        break;

      case 1:
        score = stock.v_value;
        insight = stock.v_insight;
        color = stock.v_color;
        break;

      case 2:
        score = stock.m_value;
        insight = stock.m_insight;
        color = stock.m_color;
        break;
    }

    try {
      score = roundNumber(score);
    } catch {
      (e) => console.log(e);
    }

    return {
      score: score,
      insight: insight,
      color: color,
      icon: this.getDvmIcon(color),
      backgroundColor: { backgroundColor: this.getDvmColor(color) },
    };
  };

  renderExpenseRocketItem = (label, index) => {
    const dvm = this.getDvmInfo(index);
    const cardStyle = [styles.expenseCard, dvm.backgroundColor];
    const score = dvm.score != null ? dvm.score : "-";

    return (
      <View style={cardStyle}>
        <View style={styles.expenseLabelContainer}>
          <Text style={styles.expenseLabel}>{label}</Text>
          {/* <Image source={dvm.icon} style={styles.expenseIcon} /> */}
        </View>
        <Text style={styles.expenseScore}>
          {score} <Text style={styles.expenseMaxScore}>{" /100"}</Text>
        </Text>
        <Text style={styles.expenseInsight} numberOfLines={2}>
          {dvm.insight}
        </Text>
      </View>
    );
  };

  renderExpensiveRocket = () => {
    const { alertBtnActive, portfolioActive } = this.state;
    const { stock, showAlerts, showPortfolio, showWatchlist, watchlistBtnState } = this.props;

    const dvmClassification = stock.DVM_classification_text ? stock.DVM_classification_text : "";

    const dvmColor = this.getDvmColor(stock.DVM_classification_color);
    const dvmClassificationStyle = [styles.heading, { color: dvmColor }];
    let showDVMInsight = typeof dvmClassification == "string" && dvmClassification.length > 0;

    return (
      <View>
        {showDVMInsight && <Text style={dvmClassificationStyle}>{dvmClassification}</Text>}
        {/* three colors green, red, yellow */}
        <View style={styles.expenseRow}>
          {this.renderExpenseRocketItem("Durability", 0)}
          {this.renderExpenseRocketItem("Valuation", 1)}
          {this.renderExpenseRocketItem("Momentum", 2)}
        </View>

        <View style={styles.stockActionRow}>
          <StockActionsView
            {...this.props}
            stock={stock}
            containerStyle={{ marginTop: wp(4) }}
            alertActive={alertBtnActive}
            portfolioActive={portfolioActive}
            watchlistBtnState={watchlistBtnState}
            showWatchlist={showWatchlist}
            showAlerts={showAlerts}
            showPortfolio={showPortfolio}
          />
        </View>
      </View>
    );
  };

  render() {
    return (
      <View>
        {this.renderExpensiveRocket()}
        <View style={styles.extraSpace} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    flex: 1,
    marginLeft: wp(3),
    // marginBottom: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    marginTop: hp(3),
  },
  expenseRow: {
    flexDirection: "row",
    marginHorizontal: wp(1.5),
    minHeight: wp(25),
    marginTop: wp(3),
  },
  expenseCard: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: colors.neutral,
    paddingHorizontal: wp(2),
    paddingVertical: wp(2.8),
    marginHorizontal: wp(1),
    // marginBottom: wp(3)
    // justifyContent: "center"
  },
  expenseLabelContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  expenseLabel: {
    flex: 1,
    fontSize: RF(1.9),
    fontWeight: "200",
    color: colors.white,
  },
  expenseIcon: {},
  expenseScore: {
    fontSize: RF(3),
    fontWeight: "200",
    color: colors.white,
    marginTop: wp(2.1),
  },
  expenseMaxScore: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  expenseInsight: {
    fontSize: RF(1.5),
    fontWeight: "200",
    color: colors.white,
    marginTop: wp(2.1),
  },
  yellowBackground: {
    backgroundColor: colors.squash,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  blueBackground: {
    backgroundColor: colors.blue,
  },
  stockActionRow: {
    marginHorizontal: wp(3),
  },
});
