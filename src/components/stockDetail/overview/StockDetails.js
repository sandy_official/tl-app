/**
 * Libraries
 */
import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, Platform } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../../styles/CommonStyles";
import { getDVMBackgroundColor } from "../../../utils/Utils";
import { MONTHS_SHORT } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

const DROPDOWN_DATA = ["1D", "1W", "1M", "1Y"];

export default class StockDetails extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dropdownPosition: {},
      dropdownSelection: "1D",
      showDropdown: false,
      show: false,
    };
  }

  formatDate = (str) => {
    if (str) {
      try {
        const d = new Date(str);
        let day = d.getDate();
        let month = MONTHS_SHORT[d.getMonth()];
        let year = d.getFullYear();

        if (day < 10) day = "0" + day;

        return month + " " + day + ", " + year;
      } catch (e) {
        console.log("Screener date parse error ", e);
        return str;
      }
    }
  };

  formatTime = (str) => {
    if (str) {
      try {
        const d = new Date(str);
        var hours = d.getHours();
        var minutes = d.getMinutes();

        return hours + ":" + minutes;
      } catch (e) {
        console.log("Screener date parse error ", e);
        return str;
      }
    }
  };

  getInsightColor = (value) => {
    if (value == "positive") return { color: colors.green };
    if (value == "negative") return { color: colors.red };
    else return { color: colors.squash };
  };

  getInsightIcon = (value) => {
    if (value == "positive") return require("../../../assets/icons/arrow-green.png");
    if (value == "negative") return require("../../../assets/icons/arrow-red.png");
    else return require("../../../assets/icons/arrow-yellow.png");
  };

  renderStockDetails = () => {
    const { stock } = this.props;

    const showStock = Object.keys(stock).length > 0;

    const stockName = "get_full_name" in stock ? stock.get_full_name : "";
    const industry = "industry_name" in stock ? stock.industry_name : "";
    const exchange = "default_exchange" in stock ? stock.default_exchange : "";
    const date = "updated" in stock ? this.formatDate(stock.updated) : "";
    const time = "updated" in stock ? this.formatTime(stock.updated) : "";
    const priceInsight = "price_insight" in stock ? stock.price_insight : "";
    const volume = "vol_day" in stock ? APIDataFormatter("vol_day", stock.vol_day) : "";

    const showDateTime = date || time || exchange ? true : false;
    const priceInsightStyle = [styles.priceIndicatorInfo, this.getInsightColor(stock.price_insight_color)];

    const insightIcon = this.getInsightIcon(stock.price_insight_color);

    // console.log("overview stock", stock);
    if (showStock) {
      return (
        <View style={{ marginHorizontal: wp(3), marginTop: wp(3) }}>
          {showDateTime && (
            <Text style={styles.dateTime}>
              {exchange} <Text style={styles.textDivider}>{" | "}</Text>
              {time}
              <Text style={styles.textDivider}>{" | "}</Text>
              {date}
            </Text>
          )}

          <View style={styles.stockNameRow}>
            <Text style={styles.stockName}>{stockName}</Text>
            <View style={styles.dvmRow}>
              <View style={getDVMBackgroundColor(stock.d_color)} />
              <View style={getDVMBackgroundColor(stock.v_color)} />
              <View style={getDVMBackgroundColor(stock.m_color)} />
            </View>
          </View>

          {!!industry && <Text style={styles.industry}>{"Industry: " + industry}</Text>}

          {this.renderCurrentPrice()}

          <View style={styles.currentPriceRow}>
            <Image source={insightIcon} style={styles.priceIndicatorIcon} />
            <Text style={priceInsightStyle}>{priceInsight}</Text>
            <Text>{"    "}</Text>
            <Text style={styles.volume}>
              Volume: <Text style={{ color: colors.black }}>{volume}</Text>
            </Text>
          </View>
        </View>
      );
    }
  };

  setDropdownPosition = (event) => {
    // if (event) {
    // var { x, y, width, height } = event.nativeEvent.layout;
    this.setState({
      dropdownPosition: event.nativeEvent.layout,
    });
    // console.log("e", event.nativeEvent.layout);
    // }
  };

  setDropdownSelection = (name) => {
    this.setState({
      dropdownSelection: name,
      showDropdown: false,
    });
  };

  renderDropdownList = () => {
    const { dropdownPosition, dropdownSelection } = this.state;
    const { x, y, width, height } = dropdownPosition;

    return (
      <View
        style={[
          styles.dropdownList,
          { top: wp(11), flexDirection: "row", borderTopLeftRadius: 5, paddingVertical: wp(1.5) },
          // { top: wp(10), flexDirection: "row", borderTopLeftRadius: 5}
        ]}
      >
        {DROPDOWN_DATA.map((item) => {
          const itemStyle =
            item == dropdownSelection
              ? [styles.dropdownListItem, styles.dropdownListSelected]
              : styles.dropdownListItem;

          return (
            <TouchableOpacity onPress={() => this.setDropdownSelection(item)} key={item}>
              <Text style={itemStyle}>{item}</Text>
            </TouchableOpacity>
          );
        }, this)}
      </View>
    );
  };

  renderDropdown = () => {
    const { showDropdown, dropdownSelection } = this.state;
    const dropdownIcon = showDropdown
      ? require("../../../assets/icons/arrow-up-blue.png")
      : require("../../../assets/icons/arrow-down-blue.png");

    return (
      <View style={styles.dropdownContainer}>
        <TouchableOpacity
          style={styles.dropdown}
          onLayout={this.setDropdownPosition}
          onPress={() => this.setState({ showDropdown: !showDropdown })}
        >
          <Text style={styles.dropdownText}>{dropdownSelection}</Text>
          <Image source={dropdownIcon} style={styles.dropdownIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  getPriceChange = () => {
    const { stock } = this.props;
    const { dropdownSelection } = this.state;

    let priceChange = 0;
    let priceChangePercent = 0;

    switch (dropdownSelection) {
      case "1D":
        priceChange = stock.day_change;
        priceChangePercent = stock.day_changeP;
        break;

      case "1W":
        priceChange = stock.week_change;
        priceChangePercent = stock.week_changeP;
        break;

      case "1M":
        priceChange = stock.month_change;
        priceChangePercent = stock.month_changeP;
        break;

      case "1Y":
        priceChange = stock.year_change;
        priceChangePercent = stock.year_changeP;
        break;
    }

    return {
      priceChange: priceChange,
      priceChangeP: priceChangePercent,
    };
  };

  renderCurrentPrice = () => {
    const { stock } = this.props;

    const currentPrice = "current_p" in stock ? APIDataFormatter("current_p", stock.current_p) : "";
    const priceChangeObj = this.getPriceChange();

    const priceIcon =
      stock.day_change >= 0
        ? require("../../../assets/icons/arrow-green.png")
        : require("../../../assets/icons/arrow-red.png");

    const priceChange = APIDataFormatter("priceChange", priceChangeObj.priceChange);

    const priceChangePercent = APIDataFormatter("priceChangeP", priceChangeObj.priceChangeP);

    const priceChangeContainerStyle =
      priceChangeObj.priceChange >= 0
        ? styles.currentPriceContainer
        : [styles.currentPriceContainer, styles.redBackground];

    return (
      <View style={styles.currentPriceRow}>
        <Image source={priceIcon} style={styles.currentPriceIcon} />
        <Text style={styles.currentPrice}>{currentPrice}</Text>

        <View style={priceChangeContainerStyle}>
          <Text style={[styles.priceChange]} numberOfLines={1}>
            {priceChange + " "}
            <Text style={[styles.priceChangePercent]}>{" " + priceChangePercent}</Text>
          </Text>
        </View>

        {this.renderDropdown()}
        {this.state.showDropdown && this.renderDropdownList()}
      </View>
    );
  };

  render() {
    const { show } = this.state;

    // if (show)
    return <View style={{ paddingBottom: wp(3) }}>{this.renderStockDetails()}</View>;
    // else return null;
  }
}

const styles = StyleSheet.create({
  currentPriceRow: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: wp(3),
  },
  currentPriceLabel: {
    fontSize: RF(2.2),
    color: colors.black,
    marginRight: wp(3),
  },
  currentPriceIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  currentPrice: {
    marginLeft: 6,
    fontSize: RF(3.2),
    fontWeight: "500",
    color: colors.black,
  },
  currentPriceContainer: {
    flexDirection: "row",
    borderRadius: 5,
    backgroundColor: colors.green,
    paddingHorizontal: wp(2.2),
    paddingVertical: 8,
    marginLeft: wp(2.5),
  },
  priceChange: {
    fontSize: RF(1.9),
    paddingHorizontal: wp(3),
    color: colors.white,
  },
  priceChangePercent: {
    fontSize: RF(2.2),
    paddingHorizontal: wp(3),
    color: colors.white,
    fontWeight: "500",
  },
  dateTime: {
    fontSize: RF(1.9),
    color: colors.steelGrey,
  },
  textDivider: {
    fontSize: RF(2.2),
  },
  stockNameRow: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: wp(3),
  },
  stockName: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.black,
  },
  dvmRow: {
    flexDirection: "row",
    alignItems: "center",
  },
  industry: {
    fontSize: 12,
    fontWeight: "300",
    color: colors.steelGrey,
    // color: colors.primary_color,
    marginTop: 6,
  },
  priceIndicatorIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  priceIndicatorInfo: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.green,
    marginLeft: 5,
  },
  volume: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.greyishBrown,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  dropdownContainer: {
    flex: 1,
    alignItems: "flex-end",
  },
  dropdown: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: wp(2),
    paddingLeft: wp(2),
  },
  dropdownText: {
    fontSize: RF(2.2),
    color: colors.black,
    marginRight: 6,
    letterSpacing: 2,
  },
  dropdownIcon: {
    width: wp(4.5),
    height: wp(4.5),
    resizeMode: "contain",
    marginRight: 3,
  },
  dropdownList: {
    position: "absolute",
    right: 0,
    marginTop: Platform.OS == "ios" ? wp(6) : 0,
    paddingVertical: wp(2),
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 1,
    zIndex: 100,
  },
  dropdownListItem: {
    paddingVertical: wp(3),
    paddingHorizontal: wp(6),
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    letterSpacing: 2,
  },
  dropdownListSelected: {
    color: colors.primary_color,
    fontWeight: "500",
  },
});
