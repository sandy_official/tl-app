import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import RF from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { MONTHS_SHORT } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class DividendHistoryList extends React.PureComponent {
  renderListColumns = () => {
    return (
      <View style={[styles.listRow, styles.greyBackground]}>
        <Text style={[styles.listItem, styles.column, styles.columnSpace]}>Ex-Date</Text>
        <Text style={[styles.listItem, styles.column, styles. dividendcolumnSpace]}>Dividend Amount</Text>
        <Text style={[styles.listItem, styles.column, styles.columnSpace]}>Dividend Type</Text>
        <Text style={[styles.listItem, styles.column]}>Record Date</Text>
      </View>
    );
  };

  // formatHistoryDate = value => {
  //   if (typeof value == "string") {
  //     const date = value.split("-");
  //     let month =
  //       MONTHS_SHORT[date[1] - 1] == "May"
  //         ? MONTHS_SHORT[date[1] - 1] + " "
  //         : MONTHS_SHORT[date[1] - 1] + ". ";

  //     return month + date[2] + ", " + date[0];
  //   } else return value;
  // };

  renderListItem = ({ item, index }) => {
    const exDate = APIDataFormatter("exdate", item.exdate);
    const recordDate = APIDataFormatter("record_date", item.record_date);

    // const itemStyle = index % 2 == 1 ? [styles.listRow, styles.greyBackground] : styles.listRow;

    return (
      <View style={[styles.listRow]}>
        <Text style={[styles.listItem, styles.columnSpace]}>{exDate}</Text>
        <Text style={[styles.listItem, styles. dividendcolumnSpace, { textAlign: "center" }]}>{item.amount}</Text>
        <Text style={[styles.listItem, styles.columnSpace]}>{item.dividend_type}</Text>
        <Text style={styles.listItem}>{recordDate}</Text>
      </View>
    );
  };

  renderNoHistory = () => {
    return (
      <View style={[styles.listRow]}>
        <Text style={styles.listItem}>{"No dividend history"}</Text>
      </View>
    );
  };

  render() {
    const { data } = this.props;

    return (
      <View>
        <Text style={styles.heading}>INSIGHTS</Text>

        {data.insight &&
          data.insight.map((insight, index) => {
            return (
              <Text style={styles.insight} key={index.toString()}>
                {insight}
              </Text>
            );
          })}

        {data.insight && <View style={{ marginBottom: wp(6) }} />}

        <View>
          <Text style={styles.heading}>DIVIDEND HISTORY</Text>
          {data.table && data.table.length > 0 ? (
            <FlatList
              data={data.table}
              // extraData={this.state}
              showsVerticalScrollIndicator={false}
              contentContainerStyle={styles.list}
              ListHeaderComponent={() => this.renderListColumns()}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              // ListFooterComponent={() => this.renderFooter()}
              ItemSeparatorComponent={() => <View style={styles.divider} />}
              removeClippedSubviews
              initialNumToRender={11}
              maxToRenderPerBatch={5}
            />
          ) : (
            this.renderNoHistory()
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1)
  },
  heading: {
    marginBottom: wp(4),
    marginTop: wp(5),
    marginHorizontal: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color:colors.black
  },
  insight: {
    fontSize: RF(2.2),
    padding: wp(3),
    marginBottom: 1,
    backgroundColor: colors.white,
    color:colors.black
  },
  listRow: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    backgroundColor: colors.white
  },
  listItem: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black
  },
  column: {
    fontWeight: "500",
    color: colors.greyishBrown
  },
  columnSpace: {
    paddingRight: wp(3)
  },
  dividendcolumnSpace: {
    paddingRight: wp(9)
  },
  greyBackground: {
    backgroundColor: "rgba(36, 83, 161,0.03)"
  },
  primaryBackground: {
    color: colors.greyishBrown,
    backgroundColor: "rgba(36, 83, 161,0.03)"
  },
  divider: {
    flex: 1,
    height: 1.5,
    marginHorizontal:wp(0),
    opacity: 0.1,
    backgroundColor: colors.greyishBrown
  }
});
