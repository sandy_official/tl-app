import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import RF from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { MONTHS_SHORT } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class BoardMeetingHistoryList extends React.PureComponent {
  renderListColumns = () => {
    return (
      <View style={[styles.listRow, styles.itemBackground]}>
        <Text style={[styles.date, styles.column, styles.columnSpace]}>Ex-Date</Text>
        <Text style={[styles.purpose, styles.column]}>Purpose</Text>
      </View>
    );
  };

  // formatHistoryDate = value => {
  //   if (typeof value == "string") {
  //     const date = value.split("-");
  //     let month =
  //       MONTHS_SHORT[date[1] - 1] == "May"
  //         ? MONTHS_SHORT[date[1] - 1] + " "
  //         : MONTHS_SHORT[date[1] - 1] + ". ";

  //     return month + date[2] + ", " + date[0];
  //   } else return value;
  // };

  renderListItem = ({ item, index }) => {
    const itemStyle = index % 2 == 1 ? [styles.listRow, styles.itemBackground] : styles.listRow;

    const boardMeetDate = APIDataFormatter("board_meet_date", item.board_meet_date);

    return (
      <View style={itemStyle}>
        <Text style={[styles.date, styles.columnSpace]}>{boardMeetDate}</Text>
        <Text style={styles.purpose}>{item.purpose}</Text>
      </View>
    );
  };

  renderNoHistory = () => {
    return (
      <View style={[styles.listRow]}>
        <Text style={styles.listItem}>{"No board meeting history"}</Text>
      </View>
    );
  };

  render() {
    const { data } = this.props;

    // console.log("Board Meetings :" + JSON.stringify(data))

    return (
      <View>
        <Text style={styles.heading}>BOARD MEETINGS HISTORY</Text>
        {data.length > 0 ? (
          <FlatList
            data={data}
            // extraData={this.state}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.list}
            ListHeaderComponent={() => this.renderListColumns()}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
            // ListFooterComponent={() => this.renderFooter()}
            removeClippedSubviews
            initialNumToRender={11}
            maxToRenderPerBatch={5}
          />
        ) : (
          this.renderNoHistory()
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  heading: {
    marginBottom: wp(4),
    marginTop: wp(6),
    marginHorizontal: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300"
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1)
  },
  listRow: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    backgroundColor: colors.white,
    alignItems: "center"
  },
  columnLabel: {
    fontWeight: "500"
  },
  column: {
    fontWeight: "500",
    color: colors.greyishBrown
  },
  date: {
    flex: 1.2,
    // fontWeight: "500",
    fontSize: RF(2.2),
    color: colors.black
  },
  purpose: {
    flex: 3,
    // fontWeight: "500",
    fontSize: RF(2.2),
    paddingLeft: wp(1),
    color: colors.black
  },
  columnSpace: {
    paddingRight: wp(3)
  },
  greyBackground: {
    backgroundColor: colors.whiteTwo
  },
  smallColumn: {
    flex: 1
  },
  textCenter: {
    textAlign: "center"
  },
  itemBackground: {
    backgroundColor: "rgba(36, 83, 161,0.03)"
  }
});
