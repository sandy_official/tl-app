import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  FlatList,
  Linking,
  TouchableWithoutFeedback,
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";
import { getDVMBackgroundColor } from "../../../utils/Utils";

export default class StockNewsItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { showRelated: false };
  }

  gotoStockDetails = (stock) => {
    const { navigation } = this.props;
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    if (stock.pageType == "Equity" || stock.pageType == "index")
      navigation.push("Stocks", {
        stockId: stock.stock_id,
        stockName: stock.stockName,
        pageType: stock.pageType,
        page: "news",
        stockCode: stockCode,
      });
  };

  openLinkInBrowser = (url) => {
    if (url) this.props.navigation.navigate("WebView", { url: url });
    // Linking.openURL(url);
  };

  toggleRelatedNews = () => {
    const { showRelated } = this.state;
    this.setState({ showRelated: !showRelated });
  };

  renderNewsRow = () => {
    const { data } = this.props;
    const newsDate = APIDataFormatter("pubDate", data.pubDate);
    // const imageUrl = "imageUrl" in data ? data.imageUrl : null;
    const url = data.url ? data.url : "";
    const source = data.source ? " | " + data.source : "";

    return (
      <View style={styles.row}>
        <View style={styles.titleContainer} onPress={() => this.openLinkInBrowser(url)}>
          <Text style={styles.newsTitle} numberOfLines={2} onPress={() => this.openLinkInBrowser(url)}>
            {data.title}
          </Text>
          <Text style={styles.date} numberOfLines={1}>
            {newsDate}
            <Text style={styles.newsType}>{source}</Text>
          </Text>
        </View>
        {/* {imageUrl ? (
          <Image source={{ uri: imageUrl }} style={styles.smallThumbnail} />
        ) : (
          <View
            style={[
              styles.smallThumbnail,
              { backgroundColor: colors.whiteTwo }
            ]}
          />
        )} */}
      </View>
    );
  };

  renderNewsHighlight = () => {
    const { showRelated } = this.state;
    const { data } = this.props;
    let buttonText =
      "related" in data && data.related != null
        ? showRelated
          ? "Show less"
          : "+" + data.related.length + " more"
        : "";

    const insight = "insight" in data && "title" in data.insight ? data.insight.title : "";
    const buttonStyle = showRelated ? styles.moreBtnActive : styles.moreBtn;
    const buttonTextStyle = showRelated ? [styles.moreBtnText, styles.blueText] : styles.moreBtnText;

    return (
      <View style={styles.highlightContainer}>
        {insight.length > 0 ? (
          <TouchableWithoutFeedback>
            <View style={[styles.highlightView, styles.alignCenter]}>
              <Text style={styles.highlight} numberOfLines={2}>
                {insight}
              </Text>
              {/* <View>
                <Image source={require("../../../assets/icons/arrow-right-blue.png")} style={styles.arrowIcon} />
              </View> */}
            </View>
          </TouchableWithoutFeedback>
        ) : (
          <View style={{ flex: 1 }} />
        )}

        {/* Buttom +More */}
        {data.related != null ? (
          <View style={styles.moreBtnContainer}>
            <TouchableOpacity style={buttonStyle} onPress={this.toggleRelatedNews}>
              <Text style={buttonTextStyle} numberOfLines={1}>
                {buttonText}
              </Text>
            </TouchableOpacity>
          </View>
        ) : null}
        {/* (
          <View style={styles.moreBtnContainer} />
        ) */}
      </View>
    );
  };

  renderMoreNewsList = () => {
    return <FlatList data={[]} extraData={this.state} horizontal showsHorizontalScrollIndicator={false} />;
  };

  renderBigThumbnail = () => {
    const { data } = this.props;

    const imageUrl = "imageUrl" in data ? data.imageUrl : null;
    return imageUrl ? <Image source={{ uri: imageUrl }} style={styles.bigThumbnail} resizeMode="stretch" /> : null;
  };

  renderStockDetail = () => {
    const { data, showStock } = this.props;

    if (showStock) {
      const greenArrow = require("../../../assets/icons/arrow-green.png");
      const redArrow = require("../../../assets/icons/arrow-red.png");

      const showPrice = data.pageType == "Equity" || data.pageType == "ETF" || data.pageType == "index";

      const priceChangeIcon = data.dayChangeP > 0 ? greenArrow : redArrow;
      const priceChangeStyle = data.dayChangeP > 0 ? styles.stockChangeP : [styles.stockChangeP, styles.redText];

      const dayChangeP = data.dayChangeP ? (data.dayChangeP > 0 ? "+" + data.dayChangeP : data.dayChangeP) : "";
      const showPriceArrow = data.currentPrice ? true : false;
      const showDayChange = dayChangeP ? true : false;

      const showDvm = data.dColor != "invalid" || data.vColor != "invalid" || data.mColor != "invalid";
      console.log(data);
      return (
        <TouchableOpacity onPress={() => this.gotoStockDetails(data)}>
          <View style={styles.stockRow}>
            <Text style={styles.stockName} numberOfLines={1}>
              {data.stockName}
            </Text>

            {showPrice && (
              <View style={styles.priceRow}>
                {showPriceArrow && <Image style={{ width: 6, height: 6 }} source={priceChangeIcon} />}
                <Text style={styles.stockPrice}>{data.currentPrice}</Text>
                {showDayChange && <Text style={priceChangeStyle}>({dayChangeP}%)</Text>}
              </View>
            )}

            {showDvm && (
              <View style={styles.dvmRow}>
                <View style={getDVMBackgroundColor(data.dColor)} />
                <View style={getDVMBackgroundColor(data.vColor)} />
                <View style={getDVMBackgroundColor(data.mColor)} />
              </View>
            )}
          </View>
        </TouchableOpacity>
      );
    }
  };

  renderRelatedNewsItem = ({ item, index }) => {
    const title = "title" in item ? item.title : "";
    const newsDate = APIDataFormatter("pubDate", item.pubDate);
    const source = "source" in item && item.source ? " | " + item.source : "";

    const url = item.url ? item.url : "";

    return (
      <TouchableOpacity style={styles.relatedContainer} onPress={() => this.openLinkInBrowser(url)}>
        <Text style={styles.related}>RELATED</Text>
        <Text style={styles.relatedNews} numberOfLines={2}>
          {title}
        </Text>
        <Text style={[styles.date, { fontSize: RF(1.5) }]} numberOfLines={1}>
          {newsDate}
          {/* 18 July 2019, 04:43PM |{" "} */}
          <Text style={styles.newsType}>{source}</Text>
        </Text>
      </TouchableOpacity>
    );
  };

  renderRelatedNews = () => {
    const { data } = this.props;
    const { showRelated } = this.state;

    if ("related" in data && data.related != null && showRelated) {
      return (
        <FlatList
          contentContainerStyle={styles.relatedNewsList}
          data={data.related}
          extraData={this.state}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderRelatedNewsItem}
        />
      );
    }
  };

  render() {
    // const { showRelated } = this.state;

    return (
      <View style={styles.newsContainer}>
        {this.renderBigThumbnail()}
        {this.renderStockDetail()}
        {this.renderNewsRow()}
        {this.renderNewsHighlight()}
        {this.renderRelatedNews()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  newsContainer: {
    backgroundColor: colors.white,
    paddingTop: wp(3),
    paddingBottom: wp(3),
  },
  row: {
    flexDirection: "row",
  },
  newsTitle: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginTop: 5,
    lineHeight: wp(5),
  },
  date: {
    fontSize: RF(1.8),
    color: colors.steelGrey,
    marginTop: 9,
  },
  newsType: {
    color: colors.greyishBrown,
  },
  smallThumbnail: {
    width: wp(17),
    height: wp(17),
    borderRadius: 3,
    marginRight: wp(3),
    resizeMode: "cover",
  },
  titleContainer: {
    flex: 1,
    paddingHorizontal: wp(3),
  },
  highlight: {
    flex: 1,
    // width: wp(60),
    fontSize: RF(1.8),
    color: colors.greyishBrown,
  },
  arrowIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginLeft: wp(3),
  },
  alignCenter: {
    alignItems: "center",
  },
  highlightContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: wp(3),
    marginBottom: wp(2),
  },
  highlightView: {
    flex: 3,
    flexDirection: "row",
    width: wp(5),
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    backgroundColor: colors.whiteTwo,
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
    marginRight: wp(5),
  },
  moreBtnContainer: {
    flex: 1,
    alignItems: "flex-end",
    marginRight: wp(3),
  },
  moreBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
  },
  moreBtnActive: {
    borderRadius: 5,
    borderColor: colors.primary_color,
    borderWidth: 1,
  },
  moreBtnText: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.white,
    padding: 9,
  },
  whiteText: {
    color: colors.white,
  },
  blueText: {
    color: colors.primary_color,
  },
  whiteBackground: {
    backgroundColor: colors.white,
  },
  blueBackground: {
    backgroundColor: colors.primary_color,
  },
  bigThumbnail: {
    //width: wp(100),
    width: wp(94),
    height: heightPercentageToDP(28),
    //height: 188,
    resizeMode: "cover",
    marginHorizontal: wp(3),
    marginBottom: wp(3),
  },
  relatedContainer: {
    width: wp(80),
    marginHorizontal: wp(2),
    padding: wp(2),
    marginVertical: wp(1),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  related: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.cement,
  },
  relatedNews: {
    fontSize: RF(1.8),
    color: colors.black,
    marginTop: 8,
    lineHeight: wp(5),
  },
  relatedNewsList: {
    paddingHorizontal: wp(3),
    paddingBottom: wp(1),
    paddingTop: wp(3),
  },

  stockName: {
    flex: 1.6,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.lightNavy,
    marginRight: wp(3),
  },
  priceRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginRight: wp(3),
  },
  stockPrice: {
    fontSize: RF(2.2),
    color: colors.black,
    marginLeft: 4,
  },
  stockChangeP: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.green,
    marginLeft: 5,
  },
  dvmRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  stockRow: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 12,
    paddingHorizontal: wp(3),
  },
  redText: {
    color: colors.red,
  },
});
