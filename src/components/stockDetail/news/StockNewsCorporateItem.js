import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, Linking } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { formatDateForNews } from "../../../utils/Utils";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockNewsCorporateItem extends React.PureComponent {
  // constructor(props) {
  //   super(props);
  //   this.state = { showRelated: false };
  // }

  openLinkInBrowser = (url) => {
    this.props.navigation.navigate("WebView", { url: url });
    // Linking.openURL(url);
  };

  getPostTypeStyle = (type) => {
    switch (type) {
      case "Corp. Note":
        return [styles.button, styles.blueBackground];
      case "agm":
      case "AGM":
        return [styles.button, styles.violetBackground];
      default:
        return styles.button;
    }
  };

  render() {
    const { data } = this.props;

    const description = APIDataFormatter("description", data.description);
    const pubDate = APIDataFormatter("pubDate", data.pubDate);
    const postType = APIDataFormatter("postType", data.postType);

    let postTypeStyle = this.getPostTypeStyle(postType);

    const linkUrl = data.pdfUrl ? data.pdfUrl : data.externalUrl;
    const pdfLinkIcon = data.pdfUrl
      ? require("../../../assets/icons/pdf.png")
      : require("../../../assets/icons/external-url.png");

    return (
      <View style={styles.container}>
        <Text style={styles.announcement} numberOfLines={2}>
          {data.title}
        </Text>
        <Text style={styles.dateTime} numberOfLines={1}>
          {pubDate}
        </Text>
        <Text style={styles.details} numberOfLines={2}>
          {description}
        </Text>

        <View style={styles.row}>
          {postType.length > 0 && (
            <View style={postTypeStyle}>
              <Text style={styles.btnName} numberOfLines={1}>
                {postType}
              </Text>
            </View>
          )}

          {linkUrl.length > 0 && (
            <TouchableOpacity style={styles.pdfContainer} onPress={() => this.openLinkInBrowser(linkUrl)}>
              <Image source={pdfLinkIcon} style={styles.pdf} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: colors.white,
    paddingVertical: wp(4),
    paddingHorizontal: wp(3),
  },
  row: {
    flexDirection: "row",
  },
  announcement: {
    fontSize: RF(2.2),
    lineHeight: wp(5),
    color: colors.black,
  },
  dateTime: {
    fontSize: RF(1.9),
    color: colors.steelGrey,
    marginTop: 6,
  },
  details: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginTop: wp(2.5),
  },
  row: {
    flexDirection: "row",
    marginTop: wp(3.5),
    alignItems: "center",
  },
  button: {
    borderRadius: 5,
    backgroundColor: colors.lightNavy,
    paddingHorizontal: wp(3),
    paddingVertical: wp(1.8),
  },
  pdf: {
    alignSelf: "flex-end",
    marginVertical: 2,
  },
  btnName: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.white,
  },
  pdfContainer: {
    flex: 1,
  },
  navyBackground: {
    backgroundColor: colors.lightNavy,
  },
  violetBackground: {
    backgroundColor: colors.blueViolet,
  },
  blueBackground: {
    backgroundColor: colors.brightBlue,
  },
});
