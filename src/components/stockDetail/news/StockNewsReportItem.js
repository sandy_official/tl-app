import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, ImageBackground, Linking } from "react-native";
import { getFormattedDateDdMmmYyyy } from "../../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ICON_CDN } from "../../../utils/Constants";
import StockActionsView from "../../common/StockActionsView";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockNewsReportItem extends React.PureComponent {
  constructor(props) {
    super(props);

    const openCard = props.position == 0 ? true : false;

    this.state = {
      expand: openCard,
      alertActive: false,
      portfolioActive: false,
      watchlistActive: props.WatchlistBtnState,
    };
  }

  /**
   * User must be logged in to view research reports
   */
  openLinkInBrowser = (url) => {
    const { isGuestUser, showLogin, navigation } = this.props;

    if (!isGuestUser) navigation.navigate("WebView", { url: url });
    // Linking.openURL(url);
    else showLogin();
  };

  getIndicatorIcon = (boolValue) => {
    if (boolValue) return require("../../../assets/icons/arrow-green.png");
    else return require("../../../assets/icons/arrow-red.png");
  };

  getIndicatorTextStyle = (boolValue) => {
    if (boolValue) return styles.greenText;
    else return styles.redText;
  };

  renderIndicators = () => {
    const { data } = this.props;

    // const recoUpgrade = true;
    const recoUpgrade = "recoUpgrade" in data ? data.recoUpgrade : false;
    const recoDowngrade = "recoDowngrade" in data ? data.recoDowngrade : false;
    const showReco = recoUpgrade || recoDowngrade;
    // const recoText = recoUpgrade ? "Reco Upgrade" : "reco Downgrade";
    const recoText = "Rating";

    // const targetUpgrade = true;
    const targetUpgrade = "priceUpgrade" in data ? data.priceUpgrade : false;
    const targetDowngrade = "priceDowngrade" in data ? data.priceDowngrade : false;
    const showTarget = targetUpgrade || targetDowngrade;
    // const targetText = targetUpgrade ? "Target Upgrade" : "Target Downgrade";
    const targetText = "Target";

    return (
      <View style={styles.indicatorContainer}>
        {showReco && (
          <View style={[styles.indicator]}>
            <Image source={this.getIndicatorIcon(recoUpgrade)} style={styles.indicatorIcon} />
            <Text style={[styles.indicatorLabel, this.getIndicatorTextStyle(recoUpgrade)]}>{recoText}</Text>
          </View>
        )}

        {showTarget && (
          <View style={[styles.indicator]}>
            <Image source={this.getIndicatorIcon(targetUpgrade)} style={styles.indicatorIcon} />
            <Text style={[styles.indicatorLabel, this.getIndicatorTextStyle(targetUpgrade)]}>{targetText}</Text>
          </View>
        )}
      </View>
    );
  };

  getRecoTypeBackground = (recoType) => {
    switch (recoType.toLowerCase()) {
      case "buy":
        return require("../../../assets/icons/tag-green.png");
      case "sell":
        return require("../../../assets/icons/tag-red.png");
      case "hold":
        return require("../../../assets/icons/tag-yellow.png");
      default:
        return require("../../../assets/icons/tag-grey.png");
    }
  };

  renderStockNameRow = () => {
    const { data } = this.props;

    // const stockName = data.get_full_name ? data.get_full_name : "";
    // const recoType = data.recoType ? data.recoType : "";
    const source = APIDataFormatter("source", data.source);
    const rType = data.rType ? data.rType : "";
    const reportDate = APIDataFormatter("pubDateNewsReport", data.pubDate);

    // console.log("rType", rType, rType.length);

    return (
      <View>
        <View style={styles.stockRow}>
          <View style={styles.StockNameContainer}>
            <Text style={styles.stockName} numberOfLines={2} ellipsizeMode="tail">
              {data.title}
            </Text>
            {/* {this.renderDVM()} */}
            {/* <Image
            source={{ uri: ICON_CDN + "dvm.png" }}
            // source={require("../../assets/icons/screeners/bookmark-screeners.png")}
            style={styles.dvmIcon}
          /> */}
          </View>
          {rType.length > 0 ? (
            <ImageBackground source={this.getRecoTypeBackground(rType)} style={styles.recoBg}>
              <Text style={styles.indicatorLabel} numberOfLines={1}>
                {rType}
              </Text>
            </ImageBackground>
          ) : null}
        </View>
        <View style={styles.subContentRow}>
          <Text style={[styles.date]} numberOfLines={1}>
            {reportDate}
            <Text style={[styles.broker]}>{" | "}</Text>
          </Text>
          <Text style={[styles.broker]}>{source}</Text>
        </View>
      </View>
    );
  };

  renderDatePdfRow = () => {
    const { navigation, hideBroker, data } = this.props;
    let linkUrl = data.pdfUrl ? data.pdfUrl : data.externalUrl;
    linkUrl = linkUrl ? linkUrl : "";
    const description = APIDataFormatter("description", data.description);

    // console.log("linkUrl", linkUrl, linkUrl.length);

    const pdLinkIcon = data.pdfUrl
      ? require("../../../assets/icons/pdf.png")
      : require("../../../assets/icons/external-url.png");

    return (
      <View style={styles.stockRow}>
        <Text style={styles.insight} numberOfLines={3}>
          {description}
        </Text>
        {linkUrl.length > 0 && (
          <View style={styles.pdfIconContainer}>
            <TouchableOpacity onPress={() => this.openLinkInBrowser(linkUrl)}>
              <Image source={pdLinkIcon} style={styles.pdfIcon} />
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  };

  renderButtonRow = (report) => {
    const { alertBtnActive, portfolioActive, watchlistActive } = this.state;

    const stock = { array: [], obj: report };

    return (
      <StockActionsView
        {...this.props}
        alertActive={alertBtnActive}
        portfolioActive={portfolioActive}
        watchlistActive={watchlistActive}
        stock={stock}
      />
    );
  };

  render() {
    // <TouchableOpacity onPress={() => null}>
    return (
      <View style={styles.card}>
        {this.renderStockNameRow()}
        {this.renderDatePdfRow()}
        {this.renderIndicators()}
        {/* <View style={styles.divider} /> */}

        {/* {this.renderDetailRows()} */}
        {/* {this.state.expand && this.renderDetailedReport()} */}
      </View>
    );
    // </TouchableOpacity>
  }
}

const styles = StyleSheet.create({
  card: {
    paddingVertical: wp(3.5),
    // paddingHorizontal: wp(3),
    // paddingVertical: wp(3),
    backgroundColor: colors.white,
    // borderRadius: 10,
    // shadowColor: "rgba(0, 0, 0, 0.1)",
    // shadowOffset: {
    //   width: 0,
    //   height: 1
    // },
    // shadowRadius: 10,
    // shadowOpacity: 1,
    // elevation: 3
    // overflow: "hidden"
  },
  stockRow: {
    flexDirection: "row",
    alignItems: "center",
  },
  StockNameContainer: {
    flex: 1,
    paddingLeft: wp(3),
    marginRight: wp(4),
  },
  stockName: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black,
  },
  dvmIcon: {
    width: wp(9),
    height: wp(3),
    resizeMode: "contain",
    marginTop: 8,
  },
  pdfIcon: {
    width: wp(6.2),
    height: wp(6.2),
    resizeMode: "contain",
    marginRight: wp(3),
    marginVertical: wp(2),
    marginLeft: wp(2),
  },
  subContentRow: {
    flex: 1,
    flexDirection: "row",
    marginTop: 6,
    marginBottom: wp(4),
    paddingHorizontal: wp(3),
    alignItems: "center",
  },
  date: {
    fontSize: RF(1.9),
    color: colors.steelGrey,
  },
  broker: {
    color: colors.greyishBrown,
  },
  subContent: {
    fontSize: RF(1.6),
    color: colors.warmGrey,
  },
  blueText: {
    color: colors.primary_color,
  },
  row: {
    flexDirection: "row",
    alignItems: "baseline",
    marginBottom: 2,
    paddingHorizontal: wp(3),
  },
  leftContent: {
    flex: 1,
  },
  rightContent: {
    alignItems: "flex-end",
  },
  indicatorContainer: {
    flexDirection: "row",
    marginTop: wp(3),
    // marginBottom: wp(3),
    paddingLeft: wp(3),
  },
  indicator: {
    flexDirection: "row",
    // padding: 8,
    marginRight: wp(5),
    // marginLeft: wp(3),
    borderRadius: 5,
    alignItems: "center",
  },
  indicatorIcon: {
    // width: wp(2.5),
    // height: wp(2.5),
    resizeMode: "contain",
    marginRight: 6,
  },
  indicatorLabel: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
  },
  // greenBackground: {
  //   backgroundColor: colors.green
  // },
  // yellowBackground: {
  //   backgroundColor: colors.yellow
  // },
  // redBackground: {
  //   backgroundColor: colors.red
  // },
  // greyBackground: {
  //   backgroundColor: colors.greyishBrown
  // },
  targetPrice: {
    fontSize: RF(3.2),
    fontWeight: "500",
    color: colors.black,
  },
  rowBottomMargin: {
    marginBottom: wp(4.5),
  },
  labelValue: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.greyish_brown,
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.green,
  },
  reportTitle: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginBottom: wp(1),
  },
  reportContainer: {
    paddingHorizontal: wp(3),
  },
  report: {
    opacity: 0.8,
    fontSize: RF(1.9),
    color: colors.greyish_brown,
    marginTop: wp(2),
    marginBottom: wp(3),
    textAlign: "justify",
  },
  reportButton: {
    flexDirection: "row",
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(1.2),
    paddingHorizontal: wp(2.5),
    alignSelf: "flex-end",
    marginTop: wp(3),
    alignItems: "center",
  },
  reportButtonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginRight: 4,
  },
  reportButtonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  stockActionBtn: {
    paddingHorizontal: wp(3),
    paddingVertical: 8,
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
    backgroundColor: colors.green,
  },
  divider: {
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
    marginBottom: wp(2),
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
  // buttonRow: {
  //   flexDirection: "row",
  //   marginTop: hp(2.5),
  //   marginBottom: hp(1)
  // },
  // buttonContainer: {
  //   flex: 1
  // },
  // button: {
  //   height: hp(4),
  //   flexDirection: "row",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 5,
  //   borderWidth: 1,
  //   borderColor: colors.primary_color
  // },
  // buttonIcon: {
  //   width: wp(4),
  //   height: wp(3),
  //   resizeMode: "contain"
  // },
  // buttonText: {
  //   fontSize: RF(2.2),
  //   color: colors.primary_color,
  //   marginLeft: 5
  // },
  // left: {
  //   flex: 1,
  //   textAlign: "left",
  //   paddingRight: wp(3)
  // },
  // center: {
  //   flex: 1,
  //   textAlign: "center"
  // },
  // right: {
  //   flex: 1,
  //   textAlign: "right",
  //   paddingLeft: wp(4)
  // },
  recoBg: {
    width: wp(32),
    height: wp(7.5),
    resizeMode: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: wp(2),
  },
  insight: {
    width: wp(58),
    fontSize: RF(1.9),
    color: colors.primary_color,
    marginLeft: wp(3),
    marginRight: wp(4),
  },
  pdfIconContainer: {
    width: wp(34),
    alignItems: "flex-end",
    justifyContent: "center",
  },
});
