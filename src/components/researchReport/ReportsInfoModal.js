import React from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, Image } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";

const infoData = [
  {
    title: "Rating",
    icon: require("../../assets/icons/arrow-green.png"),
    color: "green",
    description:
      "This means that the broker has upgraded their stock recommendation from their previous report (eg. from Sell to Buy)",
  },
  {
    title: "Rating",
    icon: require("../../assets/icons/arrow-red.png"),
    color: "red",
    description:
      "The broker has downgraded their stock recommendation from their previous report (eg. from Buy to Hold)",
  },
  {
    title: "Target",
    icon: require("../../assets/icons/arrow-green.png"),
    color: "green",
    description: "The broker has increased the stock's target price from their previous report",
  },
  {
    title: "Target",
    icon: require("../../assets/icons/arrow-red.png"),
    color: "red",
    description: "The broker has decreased the stock's target price from their previous report",
  },
];

export default class ReportsInfoModal extends React.PureComponent {
  closeDialog = () => {
    this.RBSheet.close();
  };

  renderInfo = () => {
    return infoData.map((item, i) => {
      const titleStyle = item.color == "green" ? styles.infoItem : [styles.infoItem, styles.redText];

      return (
        <View style={{ marginBottom: wp(4) }} key={i.toString()}>
          <View style={styles.infoRow}>
            <Image source={item.icon} />
            <Text style={titleStyle}>{item.title}</Text>
          </View>
          <Text style={styles.info}>{item.description}</Text>
        </View>
      );
    });
  };

  renderHeader = () => {
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Research Reports
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(75)}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {this.renderInfo()}
        </View>
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  crossIcon: {
    // width: wp(3),
    // height: wp(3),
    // padding: wp(3),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  infoRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
    alignItems: "center",
  },
  infoIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  infoItem: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.green,
    marginLeft: 5,
  },
  info: {
    fontSize: RF(2.2),
    color: colors.black,
  },
  redText: {
    color: colors.red,
  },
});
