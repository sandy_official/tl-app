import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  ImageBackground,
  Linking,
  TouchableWithoutFeedback,
} from "react-native";
import { getDVMBackgroundColor } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import StockActionsView from "../common/StockActionsView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class ResearchReportCard extends React.PureComponent {
  constructor(props) {
    super(props);

    const openCard = props.position == 0 ? true : false;

    this.state = {
      expand: openCard,
      alertActive: false,
      portfolioActive: false,
      // watchlistActive: props.WatchlistBtnState
    };
  }

  /**
   * User must be logged in to view research reports
   */
  openLinkInBrowser = (url) => {
    const { isGuestUser, showLogin, navigation } = this.props;

    if (!isGuestUser) {
      // Linking.openURL(url);
      navigation.navigate("WebView", { url: url });
    } else showLogin();
  };

  getIndicatorIcon = (boolValue) => {
    if (boolValue) return require("../../assets/icons/arrow-green.png");
    else return require("../../assets/icons/arrow-red.png");
  };

  getIndicatorTextStyle = (boolValue) => {
    if (boolValue) return styles.greenText;
    else return styles.redText;
  };

  gotoStockDetails = () => {
    const { navigation, data } = this.props;
    // console.log("report stock", data);
    let pageType = typeof data.page_type == "string" ? data.page_type.toLowerCase() : "";
    const stockCode = data.NSEcode ? data.NSEcode : data.BSEcode;
    if (pageType == "equity" || pageType == "unlisted") {
      navigation.push("Stocks", {
        stockId: data.stock_id,
        stockName: data.get_full_name,
        pageType: data.page_type,
        stockCode: stockCode,
        tab: pageType == "equity" ? 4 : pageType == "unlisted" ? 1 : 0,
      });
    }
  };

  renderIndicators = () => {
    const { data } = this.props;

    const recoUpgrade = data.recoUpgrade ? data.recoUpgrade : false;
    const recoDowngrade = data.recoDowngrade ? data.recoDowngrade : false;
    const showReco = recoUpgrade || recoDowngrade;
    const recoText = "Rating";
    // const recoText = recoUpgrade ? "Rating Upgrade" : "Reco Downgrade";

    const targetUpgrade = data.priceUpgrade ? data.priceUpgrade : false;
    const targetDowngrade = data.priceDowngrade ? data.priceDowngrade : false;
    const showTarget = targetUpgrade || targetDowngrade;
    const targetText = "Target";
    // const targetText = targetUpgrade ? "Target Upgrade" : "Target Downgrade";

    if (showReco || showTarget) {
      return (
        <View style={styles.indicatorContainer}>
          {showReco && (
            <View style={[styles.indicator]}>
              <Image source={this.getIndicatorIcon(recoUpgrade)} style={styles.indicatorIcon} />
              <Text style={[styles.indicatorLabel, this.getIndicatorTextStyle(recoUpgrade)]}>{recoText}</Text>
            </View>
          )}

          {showTarget && (
            <View style={[styles.indicator]}>
              <Image source={this.getIndicatorIcon(targetUpgrade)} style={styles.indicatorIcon} />
              <Text style={[styles.indicatorLabel, this.getIndicatorTextStyle(targetUpgrade)]}>{targetText}</Text>
            </View>
          )}

          {/* <View style={[styles.indicator, styles.yellowBackground]}>
          <Image
            source={{
              uri: ICON_CDN + "arrow-up-grey.png"
            }}
            // source={require("../../assets/icons/arrow-up-grey.png")}
            style={styles.indicatorIcon}
          />
          <Text style={styles.indicatorLabel}>Reco Upgrade</Text>
        </View> */}
        </View>
      );
    }
  };

  renderDetailedReport = () => {
    const { data } = this.props;
    const reportTitle = APIDataFormatter("reportTitle", data.reportTitle);
    const reportDescription = APIDataFormatter("reportDescription", data.reportDescription);

    return (
      <View style={styles.reportContainer}>
        <Text style={styles.reportTitle}>{reportTitle}</Text>
        <Text style={styles.report}>{reportDescription}</Text>

        {this.renderButtonRow(data)}

        {/* <TouchableOpacity>
          <View style={styles.reportButton}>
            <Image
              source={{
                uri: ICON_CDN + "arrow-up-grey.png"
              }}
              // source={require("../../assets/icons/arrow-up-grey.png")}
              style={styles.reportButtonIcon}
            />
            <Text style={styles.reportButtonText}>Go to broker report</Text>
          </View>
        </TouchableOpacity> */}
      </View>
    );
  };

  getRecoTypeBackground = (recoType) => {
    switch (recoType.toLowerCase()) {
      case "buy":
        return require("../../assets/icons/tag-green.png");
      case "sell":
        return require("../../assets/icons/tag-red.png");
      case "hold":
        return require("../../assets/icons/tag-yellow.png");
      default:
        return require("../../assets/icons/tag-grey.png");
    }
  };

  renderStockNameRow = () => {
    const { data } = this.props;

    const stockName = APIDataFormatter("get_full_name", data.get_full_name);
    const recoType = data.recoType;
    // const recoType = APIDataFormatter("recoType", data.recoType);
    let clickEnabled = data.page_type ? data.page_type == "Equity" || data.page_type == "Unlisted" : false;
    let showDvm = data.page_type && data.page_type == "Equity";

    return (
      <View style={styles.stockRow}>
        <View style={styles.StockNameContainer}>
          <TouchableOpacity style={{ flex: 1 }} onPress={this.gotoStockDetails} disabled={!clickEnabled}>
            <Text style={styles.stockName} numberOfLines={1} ellipsizeMode="tail">
              {stockName}
            </Text>
          </TouchableOpacity>
          {showDvm && this.renderDVM()}
          {/* <Image
            source={require("../../assets/icons/screeners/bookmark-screeners.png")}
            style={styles.dvmIcon}
          /> */}
        </View>
        {recoType ? (
          <ImageBackground source={this.getRecoTypeBackground(recoType)} style={styles.recoBg}>
            <Text style={styles.indicatorLabel} numberOfLines={1}>
              {recoType}
            </Text>
          </ImageBackground>
        ) : null}
      </View>
    );
  };

  renderDatePdfRow = () => {
    const { navigation, hideBroker, data } = this.props;
    const brokerName = APIDataFormatter("brokerName", data.brokerName);
    const recoDate = APIDataFormatter("recoDate", data.recoDate);
    const linkUrl = data.pdfURL ? data.pdfURL : data.externalHTMLURL;

    const pdLinkIcon = data.pdfURL
      ? require("../../assets/icons/pdf.png")
      : require("../../assets/icons/external-url.png");

    return (
      <View style={styles.stockRow}>
        <View style={styles.pdfRow}>
          {!hideBroker && (
            <TouchableOpacity onPress={() => navigation.push("ReportDetail", { broker: brokerName })}>
              <Text style={[styles.subContent, styles.brokerName]}>
                {brokerName}
                <Text style={[styles.date]}>{" | "}</Text>
              </Text>
            </TouchableOpacity>
          )}
          <Text style={[styles.date]}>{recoDate}</Text>
        </View>

        {linkUrl.length > 0 && (
          <TouchableOpacity onPress={() => this.openLinkInBrowser(linkUrl)}>
            <Image
              // source={{
              //   uri: ICON_CDN + "screeners/bookmark-screeners.png"
              // }}
              source={pdLinkIcon}
              style={styles.pdfIcon}
            />
          </TouchableOpacity>
        )}
      </View>
    );
  };

  renderDVM = () => {
    const { data } = this.props;
    return (
      <View style={styles.stockRow}>
        <View style={getDVMBackgroundColor(data.d_color)} />
        <View style={getDVMBackgroundColor(data.v_color)} />
        <View style={getDVMBackgroundColor(data.m_color)} />
      </View>
    );
  };

  renderButtonRow = (report) => {
    const { alertBtnActive, portfolioActive } = this.state;

    // const stock = { array: [], obj: report };

    return (
      <StockActionsView
        {...this.props}
        alertActive={alertBtnActive}
        portfolioActive={portfolioActive}
        // watchlistActive={WatchlistBtnState}
        stock={report}
      />
    );
  };

  renderDetailRows = () => {
    const { data } = this.props;

    let show = data.page_type && data.page_type == "Equity";

    if (show) {
      const currentPrice = APIDataFormatter("currentPrice", data.currentPrice);
      const targetPrice = APIDataFormatter("targetPrice", data.targetPrice);
      const upsidePercent =
        "upsideText" in data && data.upsideText
          ? data.upsideText
          : APIDataFormatter("upsidePercent", data.upsidePercent);
      const recoPrice = APIDataFormatter("recoPrice", data.recoPrice);
      const recoPriceChange = APIDataFormatter("changeSinceReco", data.changeSinceReco);

      const formattedReco = recoPrice ? recoPrice + " (" + recoPriceChange + ")" : "";

      const upsidePercentStyle =
        "upsideText" in data && data.upsideText
          ? { color: colors.greyishBrown }
          : data.upsidePercent
          ? data.upsideColor == "positive"
            ? styles.greenText
            : styles.redText
          : { color: colors.greyishBrown };

      const recoPriceStyle = recoPriceChange >= 0 ? styles.greenText : styles.redText;
      const currentPriceStyle = [styles.leftContent, styles.labelValue, { color: colors.greyishBrown }];
      const recoStyle = [styles.leftContent, styles.labelValue, recoPriceStyle];

      return (
        <View>
          <View style={styles.rowBottomMargin}>
            <View style={styles.row}>
              <Text style={currentPriceStyle}>{currentPrice}</Text>
              <Text style={[styles.targetPrice, styles.rightContent]}>{targetPrice}</Text>
            </View>

            <View style={styles.row}>
              <Text style={[styles.subContent, styles.leftContent]}>Last Traded Price</Text>
              <Text style={[styles.subContent, styles.rightContent]}>Target</Text>
            </View>
          </View>

          <View style={this.state.expand ? styles.rowBottomMargin : {}}>
            <View style={styles.row}>
              <Text style={recoStyle}>{formattedReco}</Text>
              <Text style={[styles.rightContent, styles.labelValue, upsidePercentStyle]}>{upsidePercent}</Text>
            </View>

            <View style={styles.row}>
              <Text style={[styles.subContent, styles.leftContent]}>Price at Reco (% change since reco)</Text>
              <Text style={[styles.subContent, styles.rightContent]}>Upside</Text>
            </View>
          </View>
        </View>
      );
    }
  };

  render() {
    const { data } = this.props;
    let show = data.page_type && data.page_type == "Equity";

    return (
      <View>
        <TouchableWithoutFeedback onPress={() => this.setState({ expand: !this.state.expand })}>
          <View style={styles.card}>
            {this.renderStockNameRow()}
            {this.renderDatePdfRow()}
            {this.renderIndicators()}
            {show && <View style={styles.divider} />}

            {this.renderDetailRows()}
            {this.state.expand && this.renderDetailedReport()}
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  // card: {
  //   marginVertical: "2%",
  //   marginHorizontal: "3%",
  //   // padding: "3%",
  //   borderRadius: 10,
  //   backgroundColor: colors.white,
  //   shadowColor: "rgba(0, 0, 0, 0.1)",
  //   shadowOffset: {
  //     width: 0,
  //     height: 1
  //   },
  //   shadowRadius: 5,
  //   shadowOpacity: 1,
  //   elevation: 2
  // },
  card: {
    marginVertical: wp(1.5),
    marginHorizontal: wp(3),
    paddingVertical: wp(3),
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 10,
    shadowOpacity: 1,
    elevation: 3,
    // overflow: "hidden"
  },
  stockRow: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: 8,
  },
  StockNameContainer: {
    flex: 1,
    paddingLeft: wp(2),
    marginRight: wp(4),
  },
  stockName: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "300",
    marginTop: wp(0),
    marginBottom: wp(1),
  },
  dvmIcon: {
    width: wp(9),
    height: wp(3),
    resizeMode: "contain",
    marginTop: 8,
  },
  pdfIcon: {
    width: wp(6.2),
    height: wp(6.2),
    resizeMode: "contain",
    marginRight: wp(3),
    marginVertical: wp(1.5),
    marginLeft: wp(2),
  },
  subContentRow: {
    flex: 1,
    flexDirection: "row",
    marginVertical: wp(4),
    paddingHorizontal: wp(3),
    alignItems: "center",
  },
  pdfRow: {
    flex: 1,
    flexDirection: "row",
    marginVertical: wp(1),
    paddingHorizontal: wp(2),
    alignItems: "center",
  },
  date: {
    fontSize: RF(1.6),
    color: colors.greyishBrown,
  },
  subContent: {
    fontSize: RF(1.6),
    color: colors.warmGrey,
  },
  blueText: {
    color: colors.primary_color,
  },
  row: {
    flexDirection: "row",
    alignItems: "baseline",
    marginBottom: 2,
    paddingHorizontal: wp(3),
  },
  leftContent: {
    flex: 1,
  },
  rightContent: {
    alignItems: "flex-end",
  },
  indicatorContainer: {
    flexDirection: "row",
    // marginTop: hp(1),
    marginBottom: wp(3),
    paddingLeft: wp(3),
  },
  indicator: {
    flexDirection: "row",
    // padding: 8,
    marginRight: wp(5),
    // marginLeft: wp(3),
    borderRadius: 5,
    alignItems: "center",
  },
  indicatorIcon: {
    width: wp(2),
    height: wp(2),
    resizeMode: "contain",
    marginRight: 6,
  },
  indicatorLabel: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
  },
  // greenBackground: {
  //   backgroundColor: colors.green
  // },
  // yellowBackground: {
  //   backgroundColor: colors.yellow
  // },
  // redBackground: {
  //   backgroundColor: colors.red
  // },
  // greyBackground: {
  //   backgroundColor: colors.greyishBrown
  // },
  targetPrice: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
  },
  rowBottomMargin: {
    marginBottom: wp(4.5),
  },
  labelValue: {
    fontSize: RF(2.4),
    fontWeight: "200",
    color: colors.greyish_brown,
    marginVertical: wp(2),
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.green,
  },
  reportTitle: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.black,
    marginBottom: wp(1),
  },
  reportContainer: {
    paddingHorizontal: wp(3),
    marginVertical: wp(3),
  },
  report: {
    opacity: 0.8,
    fontSize: RF(2.2),
    lineHeight: wp(6),
    color: colors.greyish_brown,
    marginTop: wp(2),
    marginBottom: wp(3),
    textAlign: "justify",
  },
  reportButton: {
    flexDirection: "row",
    borderRadius: 4,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(1.2),
    paddingHorizontal: wp(2.5),
    alignSelf: "flex-end",
    marginTop: wp(3),
    alignItems: "center",
  },
  reportButtonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginRight: 4,
  },
  reportButtonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  stockActionBtn: {
    paddingHorizontal: wp(3),
    paddingVertical: 8,
    borderTopLeftRadius: 2,
    borderBottomLeftRadius: 2,
    backgroundColor: colors.green,
  },
  divider: {
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
    marginBottom: wp(2),
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
  // buttonRow: {
  //   flexDirection: "row",
  //   marginTop: hp(2.5),
  //   marginBottom: hp(1)
  // },
  // buttonContainer: {
  //   flex: 1
  // },
  // button: {
  //   height: hp(4),
  //   flexDirection: "row",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 5,
  //   borderWidth: 1,
  //   borderColor: colors.primary_color
  // },
  // buttonIcon: {
  //   width: wp(4),
  //   height: wp(3),
  //   resizeMode: "contain"
  // },
  // buttonText: {
  //   fontSize: RF(2.2),
  //   color: colors.primary_color,
  //   marginLeft: 5
  // },
  // left: {
  //   flex: 1,
  //   textAlign: "left",
  //   paddingRight: wp(3)
  // },
  // center: {
  //   flex: 1,
  //   textAlign: "center"
  // },
  // right: {
  //   flex: 1,
  //   textAlign: "right",
  //   paddingLeft: wp(4)
  // },
  recoBg: {
    width: wp(34),
    height: wp(7.5),
    resizeMode: "stretch",
    alignItems: "center",
    justifyContent: "center",
    paddingLeft: wp(2),
  },
  brokerName: {
    color: colors.primary_color,
    paddingVertical: wp(2),
    marginLeft: wp(1),
  },
});
