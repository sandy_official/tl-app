import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { PRIMARY_COLOR, colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { getDVMBackgroundColor } from "../../utils/Utils";
import { ICON_CDN } from "../../utils/Constants";
import { APIDataFormatter } from "../../utils/APIDataFormatter";
export default class ReportStockItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isAdded: false };
  }

  gotoStockDetails = () => {
    const { navigation, data, saveSearch } = this.props;
    console.log("Stock search ", data);
    // const stockName = APIDataFormatter("get_full_name", data.get_full_name);
    const stockCode = data.NSEcode ? data.NSEcode : data.BSEcode;
    const stockName = data.get_full_name ? data.get_full_name : stockCode;
    const stockId = data.stock_id;
    const pageType = data.page_type;

    if (saveSearch) saveSearch(data);
    const now = new Date();
    // navigation.push("Stocks", { stockId: stockId, stockName: stockName, pageType: pageType, stockCode: stockCode });
    navigation.dispatch({
      key: "stocks" + stockId + now.valueOf(),
      type: "maxRoute",
      routeName: "Stocks",
      params: {
        stockId: stockId,
        stockName: stockName,
        pageType: pageType,
        stockCode: stockCode,
      },
    });
  };

  render() {
    const { navigation, data, saveSearch } = this.props;
    const stockName = APIDataFormatter("get_full_name", data.get_full_name);
    const stockCode = data.NSEcode ? data.NSEcode : data.BSEcode;

    return (
      <TouchableOpacity style={styles.container} onPress={this.gotoStockDetails}>
        <View style={styles.nameCodeContainer}>
          <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
            {stockCode}
          </Text>
          <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
            {stockName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    // marginTop: hp(1.5),
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
  },
  nameCodeContainer: {
    flex: 2.2,
    paddingRight: 18,
  },
  // priceContainer: {
  //   flex: 1.2,
  //   alignItems: "flex-start"
  // },
  // stockChangeContainer: {
  //   flex: 1.6,
  //   alignItems: "flex-end"
  //   // justifyContent: "flex-end"
  // },
  // addIconContainer: {
  //   flex: 0.5,
  //   alignItems: "flex-end",
  //   justifyContent: "center"
  // },
  code: {
    color: colors.primary_color,
    fontSize: RF(2.2),
    fontWeight: "300",
  },
  name: {
    fontSize: RF(1.9),
    marginTop: wp(2),
    color: colors.greyishBrown,
    opacity: 0.85,
  },
  price: {
    fontSize: RF(2.2),
    fontWeight: "500",
    marginBottom: 4,
    // marginLeft: 4
  },
  // row: {
  //   flexDirection: "row",
  //   marginTop: 8
  // },
  // colorBlock: {
  //   width: wp(2),
  //   height: wp(2),
  //   backgroundColor: "green",
  //   margin: 3,
  //   borderRadius: 2
  // },
  // changeInPercent: {
  //   color: "#fff",
  //   fontSize: RF(2.2),
  //   fontWeight: "500"
  // },
  // changeInPrice: {
  //   color: "#fff",
  //   fontSize: RF(2),
  //   marginTop: 2,
  //   opacity: 0.95
  // },
  // changeBox: {
  //   minWidth: wp(22),
  //   padding: wp(2),
  //   backgroundColor: colors.green,
  //   justifyContent: "center",
  //   borderRadius: 5
  // },
  // stockChangeSinceAdded: {
  //   fontSize: RF(1.6),
  //   marginTop: 8
  // },
  // greenText: {
  //   color: colors.green
  // },
  // redText: {
  //   color: colors.red
  // },
  // greyText: {
  //   color: colors.greyishBrown
  // },
  // addIcon: {
  //   width: wp(5),
  //   height: wp(5),
  //   padding: 8
  // },
  // redBackground: {
  //   backgroundColor: colors.red
  // }
});

//  return (
//    <TouchableOpacity
//      style={styles.container}
//      // onPress={() => navigation.push("ReportDetail")}
//    >
//      <View
//        style={styles.nameCodeContainer}
//        // onPress={() => this.props.navigation.navigate("BrokerDetail")}
//      >
//        <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
//          BANKBARODA
//          {/* {data.NSEcode
//                            ? data.NSEcode
//                            : data.BSECode} */}
//        </Text>
//        <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
//          Bank of Baroda
//          {/* {data.full_name} */}
//        </Text>
//        {/* <Text
//           style={[styles.stockChangeSinceAdded,styles.greenText]}
//           numberOfLines={1}
//           ellipsizeMode="tail"
//         >
//           +10.5% <Text style={styles.greyText}>since added</Text>
//         </Text> */}
//      </View>

//      <View style={styles.priceContainer}>
//        <Text style={styles.price}>
//          123
//          {/* {data.currentPrice} */}
//        </Text>
//        <View style={styles.row}>
//          <View style={getDVMBackgroundColor("positive")} />
//          <View style={getDVMBackgroundColor("positive")} />
//          <View style={getDVMBackgroundColor("positive")} />
//        </View>
//      </View>

//      <View style={styles.stockChangeContainer}>
//        <View
//          style={
//            //  data.day_changeP > 0
//            //  ?
//            styles.changeBox
//            //  : [
//            //  styles.changeBox,
//            //  styles.redBackground
//            //  ]
//          }
//        >
//          <Text style={styles.changeInPercent}>
//            +3
//            {/* {data.day_changeP > 0
//                              ? "+" + data.day_changeP
//                              : data.day_changeP} */}
//            %
//          </Text>
//          <Text style={styles.changeInPrice}>+23.5</Text>
//          {/* {!isNaN(data.day_change) ? (
//                            <Text
//                              style={styles.changeInPrice}
//                            >
//                              {data.day_change > 0
//                                ? "+" + data.day_change
//                                : data.day_change}
//                            </Text>
//                          ) : (
//                            <Text
//                              style={styles.changeInPrice}
//                            >
//                              {
//                                data.vol_day_times_vol_week_str
//                              }
//                            </Text>
//                          )} */}
//        </View>
//      </View>
//    </TouchableOpacity>
//  );
