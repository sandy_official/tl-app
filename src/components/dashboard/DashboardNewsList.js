/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../styles/CommonStyles";
import { getDVMBackgroundColor } from "../../utils/Utils";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class DashboardNewsList extends React.PureComponent {
  listRef = null;

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = { indicatorPos: 0 };
  }

  /**
   * on click of stock news card goto stock detail scree
   */
  gotoStockDetails = (stock) => {
    const { navigation } = this.props;

    if (stock.pageType) {
      const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
      navigation.push("Stocks", {
        stockId: stock.stock_id,
        stockName: stock.stockName,
        pageType: stock.pageType,
        stockCode: stockCode,
      });
    } else if (stock.url) {
      navigation.navigate("WebView", { url: stock.url });
    }
  };

  /**
   * Goto particular screen based on the menu item selected
   * @param {object} menuItemAction action assigned to menu item
   */
  gotoScreen = (action) => {
    if (action != null) this.props.navigation.navigate(action);
  };

  /**
   * Function to track list item position and set indicator dot
   */
  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState(
        { indicatorPos: viewableItems[0].index },
        () => this.listRef && this.listRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  /**
   * Render list position indicator dots
   * @returns {view} page indicator dots
   */
  renderPageIndicator = () => {
    const { stocks } = this.props;
    const { indicatorPos } = this.state;

    if (stocks != undefined && stocks.length > 1)
      return (
        <View style={styles.pageIndicator}>
          {stocks.map((item, index) => {
            const indicatorStyle = indicatorPos == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

            return <View style={indicatorStyle} key={index.toString()} />;
          })}
        </View>
      );
  };

  renderStockDetails = (stock) => {
    const priceChangeIcon =
      stock.dayChangeP > 0
        ? require("../../assets/icons/arrow-green.png")
        : require("../../assets/icons/arrow-red.png");

    const priceChangeStyle = stock.dayChangeP > 0 ? styles.stockChangeP : [styles.stockChangeP, styles.redText];

    const dayChangeP = stock.dayChangeP ? (stock.dayChangeP > 0 ? "+" + stock.dayChangeP : stock.dayChangeP) : "";

    const showPriceArrow = stock.currentPrice ? true : false;
    const showDayChange = dayChangeP ? true : false;
    let showPrice = showPriceArrow || showDayChange;

    let stockNameStyle = showPrice ? styles.stockName : [styles.stockName, { flex: 4 }];
    let showDVM = stock.dColor || stock.vColor || stock.mColor;

    return (
      <View style={styles.stockRow}>
        <Text style={stockNameStyle} numberOfLines={1}>
          {stock.stockName}
        </Text>

        {showPrice && (
          <View style={styles.priceRow}>
            {showPriceArrow && <Image style={styles.arrow} source={priceChangeIcon} />}
            <Text style={styles.stockPrice}>{stock.currentPrice}</Text>
            {showDayChange && <Text style={priceChangeStyle}>({dayChangeP}%)</Text>}
          </View>
        )}

        {showDVM && (
          <View style={styles.dvmRow}>
            <View style={getDVMBackgroundColor(stock.dColor)} />
            <View style={getDVMBackgroundColor(stock.vColor)} />
            <View style={getDVMBackgroundColor(stock.mColor)} />
          </View>
        )}
      </View>
    );
  };

  /**
   * Renders stock news item
   * @param {object} stock stock news object
   * @returns {view} stock news item
   */
  renderItem = ({ item, index }) => {
    const title = item.title ? item.title : "";
    const publishDate = APIDataFormatter("pubDate", item.pubDate);
    let stockCode = item.NSEcode ? item.NSEcode : item.BSEcode;
    stockCode = stockCode ? ` | ${stockCode}` : "";

    return (
      <TouchableOpacity style={[styles.updateItem]} onPress={() => this.gotoStockDetails(item)}>
        <Text style={styles.updateHeadline} numberOfLines={3}>
          {title}
        </Text>
        <Text style={styles.newsDate}>
          {publishDate}
          <Text style={styles.newsSource}>{stockCode}</Text>
        </Text>

        {this.renderStockDetails(item)}

        {/* <StockCardItem {...this.props} stock={stock} /> */}
        <LinearGradient colors={[colors.azul, colors.primary_color]} style={styles.linearGradient} />
      </TouchableOpacity>
    );
  };

  render() {
    const { stocks } = this.props;

    return (
      <View>
        <View style={styles.headingRow}>
          <Text style={[styles.heading]}>LATEST UPDATES</Text>
          {this.renderPageIndicator()}
        </View>

        <FlatList
          ref={(ref) => (this.listRef = ref)}
          contentContainerStyle={[styles.list]}
          data={stocks}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  headingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(6),
    alignItems: "center",
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  list: {
    paddingHorizontal: wp(1.8),
    height: hp(26),
  },
  pageIndicator: {
    flexDirection: "row",
    // marginTop: wp(6)
  },
  pageIndicatorActive: {
    width: wp(1.8),
    height: wp(1.8),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.3),
    height: wp(1.3),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  updateItem: {
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    marginVertical: wp(3),
    padding: wp(3),
    paddingBottom: wp(3.5),
    width: wp(92),
    marginHorizontal: wp(1.2),
  },
  updateHeadline: {
    fontSize: RF(2.4),
    color: colors.black,
  },
  newsDate: {
    fontSize: RF(1.9),
    color: colors.steelGrey,
    marginTop: 6.4,
  },
  newsSource: {
    color: colors.greyishBrown,
  },
  linearGradient: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    height: 5.2,
    borderBottomLeftRadius: 5,
    borderBottomRightRadius: 5,
  },
  stockName: {
    flex: 1.4,
    fontSize: RF(2.2),
    color: colors.lightNavy,
    marginRight: wp(3),
  },
  priceRow: {
    flex: 1.2,
    flexDirection: "row",
    alignItems: "center",
    marginRight: wp(3),
  },
  stockPrice: {
    fontSize: RF(2.2),
    color: colors.black,
    marginLeft: 4,
  },
  stockChangeP: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.green,
    marginLeft: 5,
  },
  dvmRow: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
  },
  stockRow: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: wp(4),
  },
  redText: {
    color: colors.red,
  },
  arrow: {
    width: wp(2.2),
    height: wp(2),
  },
});
