/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../styles/CommonStyles";

/**
 * @constant config Static config to generate menu items
 */
const menuItems = {
  markets: {
    name: "Markets",
    icon: require("../../assets/icons/dashboard/market-dashboard.png"),
    action: "Market",
    right: true,
    bottom: true
  },
  screeners: {
    name: "Screeners",
    icon: require("../../assets/icons/dashboard/screener-dashboard.png"),
    action: "Screener",
    right: true,
    bottom: true
  },
  portfolio: {
    name: "Portfolio",
    icon: require("../../assets/icons/dashboard/portfolio-dashboard.png"),
    action: "Portfolio",
    right: true,
    bottom: true
  },
  watchlist: {
    name: "Watchlist",
    icon: require("../../assets/icons/dashboard/watchlist-dashboard.png"),
    action: "Portfolio",
    extras: { tabIndex: 2 },
    bottom: true
  },
  superstars: {
    name: "Superstars",
    icon: require("../../assets/icons/dashboard/superstar-dashboard.png"),
    action: "Superstar",
    right: true
  },
  reports: {
    name: "Reports",
    icon: require("../../assets/icons/dashboard/reports-dashboard.png"),
    action: "Reports",
    right: true
  },
  news: {
    name: "News",
    icon: require("../../assets/icons/dashboard/news-dashboard.png"),
    action: "Market",
    extras: { tabIndex: 3 },
    right: true
  },
  alerts: {
    name: "Alerts",
    icon: require("../../assets/icons/dashboard/alert-dashboard.png"),
    action: "Alerts"
  }
};

/**
 * Dashboard screen - this is user dashboard screen
 */
export default class DashboardMenu extends React.PureComponent {
  /**
   * Goto particular screen based on the menu item selected
   * @param {object} menuItemAction action assigned to menu item
   */
  gotoScreen = obj => {
    const { navigate } = this.props.navigation;

    obj.extras ? navigate({ routeName: obj.action, params: obj.extras }) : navigate(obj.action);
  };

  /**
   * Dashboard menu item with icon and name
   * @param {Object} menuItem
   * @returns {View} menu item view
   */
  renderMenuItem = item => {
    return (
      <TouchableOpacity style={styles.menuItem} onPress={() => this.gotoScreen(item)}>
        <Image source={item.icon} resizeMode="contain" style={{ height: wp(10.5) }} />
        <Text style={styles.menuName}>{item.name}</Text>
        {item.bottom ? <View style={styles.dividerHorizontal} /> : null}
        {item.right ? <View style={styles.dividerVertical} /> : null}
      </TouchableOpacity>
    );
  };

  render() {
    return (
      <View style={styles.menuContainer}>
        <View style={styles.row}>
          {this.renderMenuItem(menuItems.markets)}
          {this.renderMenuItem(menuItems.screeners)}
          {this.renderMenuItem(menuItems.portfolio)}
          {this.renderMenuItem(menuItems.watchlist)}
        </View>

        <View style={styles.menuRowSpace} />

        <View style={styles.row}>
          {this.renderMenuItem(menuItems.superstars)}
          {this.renderMenuItem(menuItems.reports)}
          {this.renderMenuItem(menuItems.news)}
          {this.renderMenuItem(menuItems.alerts)}
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  row: {
    flexDirection: "row"
  },
  menuContainer: {
    backgroundColor: colors.white,
    paddingVertical: wp(5),
    paddingHorizontal: wp(3),
    marginBottom: wp(10)
  },
  menuItem: {
    flex: 1,
    minHeight: wp(15),
    alignItems: "center"
  },
  menuRowSpace: {
    height: wp(8.5)
  },
  menuName: {
    fontSize: RF(1.9),
    color: colors.black,
    fontWeight: "400",
    marginTop: 5
  },

  dividerVertical: {
    position: "absolute",
    right: -wp(0.5),
    top: -wp(4),
    bottom: -wp(5),
    width: 1,
    // backgroundColor: colors.primary_color,
    backgroundColor: colors.whiteTwo,
    marginVertical: wp(3)
  },
  dividerHorizontal: {
    position: "absolute",
    right: 0,
    left: 3,
    bottom: -wp(5.5),
    height: 1,
    backgroundColor: colors.whiteTwo,
    // backgroundColor: colors.primary_color,
    marginHorizontal: wp(3)
  }
});
