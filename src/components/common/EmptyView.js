import React from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

export default class EmptyView extends React.PureComponent {
  render() {
    const { message, heading, imageSrc, button, onButtonPress, style } = this.props;
    const showHeading = heading != undefined;
    const image = imageSrc != undefined ? imageSrc : require("../../assets/icons/alerts/illustrationReports.png");
    const showButton = button != undefined;

    return (
      <View style={[styles.container, style]}>
        <Image source={image} />
        {showHeading && <Text style={styles.heading}>{heading}</Text>}
        <Text style={styles.message}>{message}</Text>
        {showButton && (
          <TouchableOpacity style={styles.button} onPress={onButtonPress}>
            <Text style={styles.buttonText}>{button}</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

export const EmptyViewHoldings = (props) => {
  const { showHeading, imageSrc, showButton, heading, buttonTxt, message, onButtonPress } = props;
  return (
    <View style={styles.container}>
      <Image source={imageSrc} />
      {showHeading && <Text style={styles.heading}>{heading}</Text>}
      <Text style={styles.message}>{message}</Text>
      {showButton ? (
        <TouchableOpacity style={styles.button} onPress={onButtonPress}>
          <Text style={styles.buttonText}>{buttonTxt}</Text>
        </TouchableOpacity>
      ) : null}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    backgroundColor: colors.whiteTwo,
    // top: hp(10),
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: wp(5),
    paddingBottom: hp(10),
  },
  message: {
    fontSize: RF(2.2),
    color: colors.steelGrey,
    marginTop: wp(2),
    lineHeight: wp(5.5),
    textAlign: "center",
  },
  heading: {
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginTop: wp(6.2),
  },
  button: {
    width: wp(80),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(8),
    marginTop: hp(4),
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: 12,
    textAlign: "center",
  },
});
