import React from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export default class ServerErrorView extends React.PureComponent {
  render() {
    const { onRetry } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.messageContainer}>
          <Image
            source={require("../../assets/icons/server-error.png")}
            style={styles.errorImage}
          />
          <Text style={styles.title}>Give it another go</Text>
          <Text style={styles.message}>
            {"The server couldn’t return the requested page.\nPlease try again"}
          </Text>

          <TouchableOpacity style={styles.button} onPress={onRetry}>
            <Text style={styles.buttonText}>Try Again</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    position: "absolute",
    // top: hp(10),
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: colors.whiteTwo
  },
  errorImage: { marginLeft: -wp(5) },
  messageContainer: {
    alignItems: "center"
  },
  title: {
    fontSize: RF(2.8),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginTop: wp(6)
  },
  message: {
    fontSize: RF(2.2),
    color: colors.steelGrey,
    marginTop: wp(2.5),
    textAlign: "center",
    marginHorizontal: wp(5)
  },
  button: {
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 1.5,
    borderColor: colors.lightNavy,
    marginTop: wp(8)
  },
  buttonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.lightNavy,
    paddingVertical: 8,
    paddingHorizontal: wp(7)
  }
});
