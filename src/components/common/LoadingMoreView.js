import React from "react";
import { View, StyleSheet, ActivityIndicator, Text } from "react-native";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export default class LoadingMoreView extends React.PureComponent {
  render() {
    const { background } = this.props;
    const containerStyle = { backgroundColor: background };

    return (
      <View style={[styles.loaderContainer, containerStyle]}>
        <ActivityIndicator size="small" color={colors.lightNavy} />
        <Text style={styles.message}>Loading more results for you</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loaderContainer: {
    // position: "absolute",
    // left: 0,
    // right: 0,
    // bottom: 0,
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "transparent",
    marginHorizontal: wp(3),
    marginTop: wp(3)
  },
  message: {
    fontSize: RF(2.2),
    color: colors.lightNavy,
    marginLeft: wp(2)
  }
});
