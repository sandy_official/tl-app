import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, FlatList, Image } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { getSuperstarStockHolding } from "../../controllers/SuperstarController";
import LoadingView from "./LoadingView";
import { getStockShareHoldingHistory } from "../../controllers/StocksController";
import { APIDataFormatter } from "../../utils/APIDataFormatter";
import { connect } from "react-redux";

class StockHoldingHistoryBSheet extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isChange: false,
      selectedQuarter: "",
      data: [],
      isRefreshing: false,
    };
  }

  getData = () => {
    const { type, superstarId, holdingId, stock, userState, baseURL, appToken } = this.props;

    console.log("Holding history id", holdingId, superstarId, stock);
    this.setState({ isRefreshing: true });

    if (type == "superstar") {
      getSuperstarStockHolding(baseURL, appToken, userState.token, superstarId, stock.stock_id, (data) => {
        // console.log("Holding history data", data);
        this.setData(data);
      });
    } else if (type == "stock") {
      getStockShareHoldingHistory(baseURL, appToken, userState.token, holdingId, (data) => {
        this.setData(data);
      });
    }
  };

  setData = (data) => {
    this.setState({ isRefreshing: false });

    if (data.length > 0) {
      let newDataObj = {};
      data.forEach((item) => {
        let quarter = item.Quarter;

        if (quarter in newDataObj) newDataObj[quarter].push(item);
        else newDataObj[quarter] = [item];
      });
      // console.log(newDataObj);
      this.setState({ data: newDataObj });
    }
  };

  closeDialog = () => {
    //   // this.props.onPress(this.state.isChange);
    this.RBSheet.close();
  };

  openDialog = () => {
    this.RBSheet.open();
    this.setState({ data: [] }, () => this.getData());
  };

  setSelectedOption = () => {
    // this.props.onPress(this.state.selectedQuarter);
    // this.setState({ selectedQuarter: item[1] });
    this.RBSheet.close();

    // this.setState({ selectedOption: option.label, filterId: option.id });
    // this.props.setFilterId(option.id)
  };

  renderMonthEntry = (obj, index) => {
    const allKeys = Object.keys(obj);
    const isExtraData = allKeys.length > 5;
    const holderName = APIDataFormatter("Holder's name", obj["Holder's name"]);
    const totalShares = APIDataFormatter("Total no. shares held", obj["Total no. shares held"]);
    const holdingPercent = APIDataFormatter("Percent Holding", obj["Percent Holding"]);

    return (
      <View style={styles.superstarItem}>
        <View style={styles.superstarRow}>
          <Text style={styles.superstar} numberOfLines={1} ellipsizeMode="tail">
            {holderName}
          </Text>
          <Text style={styles.holding}>
            {holdingPercent + "%  "} <Text style={styles.greyText}>{"(" + totalShares + ")"}</Text>
          </Text>
        </View>

        {isExtraData && (
          <View style={styles.detailRow}>
            <View style={styles.detailItem}>
              <Text style={styles.detailValue}>
                {obj[allKeys[4]] + "%"} <Text style={styles.greyText}>({obj[allKeys[5]]})</Text>
              </Text>
              <Text style={styles.detailLabel}>Pledged</Text>
            </View>

            <View>
              <Text style={styles.detailValue}>
                {obj[allKeys[6]] + "%"} <Text style={styles.greyText}>({obj[allKeys[7]]})</Text>
              </Text>
              <Text style={styles.detailLabel}>Locked</Text>
            </View>
          </View>
        )}
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    let dataList = this.state.data[item];
    let firstItem = dataList[0];

    return (
      <View style={styles.history}>
        <Text style={styles.month}>{firstItem["Month Year"]}</Text>

        {dataList.map((superstar, i) => {
          return (
            <View key={item + i}>
              {this.renderMonthEntry(superstar, i)}
              {superstar.length - 1 != i && <View style={styles.shareholderDivider} />}
            </View>
          );
        })}
      </View>
    );
  };

  renderHeader = () => {
    const { stock } = this.props;
    const stockCode = stock ? (stock.NSEcode ? stock.NSEcode : stock.BSEcode) : "";

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={3}>
          {"Historical shareholding details for " + stockCode}
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderList = () => {
    const { data } = this.state;

    return (
      <FlatList
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
        data={Object.keys(data)}
        extraData={this.state}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
      />
    );
  };

  render() {
    const { isRefreshing, data } = this.state;

    const isDataReceived = data && Object.keys(data).length > 0;
    const showErrorMsg = !isRefreshing && !isDataReceived;

    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(95)}
        duration={300}
        closeOnDragDown={false}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {isDataReceived && this.renderList()}
        </View>

        {showErrorMsg && (
          <View style={styles.noHistoryContainer}>
            <Text style={styles.noHistory}>Holding history not found</Text>
          </View>
        )}

        {isRefreshing && <LoadingView />}
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockHoldingHistoryBSheet);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(5), //wp(3)
    paddingTop: wp(5),
  },
  title: {
    flex: 1,
    flexWrap: "wrap",
    // flexGrow: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
    // paddingHorizontal: wp(4)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
    // paddingHorizontal: wp(4)
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  row: {
    // height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    flexWrap: "wrap",
    // justifyContent: "center"
  },
  quarterContainer: {
    flexDirection: "row",
    alignItems: "center",
    // paddingLeft: wp(6),
    paddingVertical: wp(3),
  },
  quarterOption: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    opacity: 0.8,
    marginLeft: wp(2),
  },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  radioButtonActive: {
    borderColor: colors.primary_color,
  },
  radioButtonCircle: {
    width: wp(2.4),
    height: wp(2.4),
    borderRadius: wp(1.2),
    backgroundColor: colors.primary_color,
  },
  hide: {
    display: "none",
  },
  list: {
    marginBottom: ifIphoneX ? hp(3) : 0,
    // marginBottom: hp(10),
    paddingBottom: hp(4),
  },
  history: {
    paddingTop: wp(4),
  },
  month: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    paddingVertical: wp(1),
  },
  superstarItem: {
    // paddingVertical: wp(3)
  },
  superstarRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
  },
  superstar: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "200",
    color: colors.greyishBrown,
    marginRight: wp(4),
    textTransform: "capitalize",
  },
  holding: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
  },
  greyText: {
    color: colors.steelGrey,
    fontWeight: "300",
  },
  detailRow: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: wp(3.2),
  },
  detailItem: {
    flex: 1,
  },
  detailLabel: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
  },
  detailValue: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.greyishBrown,
    marginBottom: 5,
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: colors.greyishBrown,
  },
  shareholderDivider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  noHistoryContainer: {
    position: "absolute",
    top: hp(14),
    bottom: hp(14),
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
  },
  noHistory: {
    color: colors.black,
    fontSize: RF(2.2),
  },
});
