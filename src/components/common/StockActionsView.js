import React from "react";
import { View, StyleSheet, TouchableOpacity, Image, Text } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

export default class StockActionsView extends React.PureComponent {
  /**
   * Replace componentWillReceiveProps to getDerivedStateFromProps after updating react to v16+
   */
  // componentWillReceiveProps(nextProps) {
  //   if (this.props.WatchlistBtnState != nextProps.WatchlistBtnState) {
  //     this.forceUpdate();
  //   }
  // }

  renderActionButton = (label, positionStyle, icon, isActive, onPress) => {
    const btnStyle = isActive ? [styles.button, styles.buttonActive] : styles.button;

    const textStyle = isActive ? [styles.buttonText, styles.whiteText] : styles.buttonText;

    return (
      <View style={[positionStyle, styles.buttonContainer]}>
        <TouchableOpacity onPress={() => onPress()}>
          <View style={btnStyle}>
            <Image source={icon} style={styles.buttonIcon} />
            <Text style={textStyle}>{label}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderActionButtonRow = () => {
    const {
      stock,
      showWatchlist,
      showAlerts,
      showPortfolio,
      alertActive,
      portfolioActive,
      watchlistBtnState,
      containerStyle
    } = this.props;

    const iconActive = require("../../assets/icons/tick-white-blue.png");
    const iconInactive = require("../../assets/icons/add-blue.png");

    const alertBtnIcon = alertActive ? iconActive : iconInactive;
    const portfolioBtnIcon = portfolioActive ? iconActive : iconInactive;

    const watchlistBtnIcon = watchlistBtnState ? iconActive : iconInactive;

    return (
      <View style={[styles.buttonRow, containerStyle]}>
        {this.renderActionButton("Alert", styles.left, alertBtnIcon, alertActive, () => showAlerts(stock))}

        {this.renderActionButton("Portfolio", styles.center, portfolioBtnIcon, portfolioActive, () =>
          showPortfolio(stock)
        )}

        {this.renderActionButton("Watchlist", styles.right, watchlistBtnIcon, watchlistBtnState, () =>
          showWatchlist(stock)
        )}
        {/* this.state.watchlistBtnActive, */}
      </View>
    );
  };

  render() {
    return <View>{this.renderActionButtonRow()}</View>;
  }
}

const styles = StyleSheet.create({
  buttonRow: {
    flexDirection: "row",
    marginTop: wp(4)
    // marginBottom: wp(3)
  },
  buttonContainer: {
    flex: 1
  },
  button: {
    // height: hp(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    padding: wp(1.8)
  },
  buttonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain"
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 5
  },
  buttonActive: {
    backgroundColor: colors.primary_color
  },
  whiteText: {
    color: colors.white
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(3)
  },
  center: {
    flex: 1,
    textAlign: "center"
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4)
  }
});
