import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image, TextInput, Platform } from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import Modal from "../../libs/react-native-modalbox";
import { addNewPortfolio } from "../../controllers/PortfolioController";
import Toast from "react-native-easy-toast";
import LoadingView from "./LoadingView";
import { connect } from "react-redux";

class CreatePortfolioModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      description: "",
      inputFocused: null,
      refreshing: false,
    };
  }

  open = () => this.modal.open();

  closeDialog = () => {
    this.modal.close();
  };

  createPortfolio = () => {
    const { onUpdate, showPortfolioModal, userState, baseURL, appToken } = this.props;
    const title = this.state.name;
    const description = this.state.description;

    this.setState({ refreshing: true });

    addNewPortfolio(baseURL, appToken, userState.token, title, description, (isSuccess, response) => {
      this.setState({ refreshing: false });

      if (isSuccess) {
        onUpdate(response);
        this.closeDialog();
        showPortfolioModal();
      } else {
        response ? this.refs.toast.show(response, 1000) : this.refs.toast.show("Please try again", 1000);
      }
    });
  };

  setInputText = (type, value) => {
    type == "Enter Portfolio Title" ? this.setState({ name: value }) : this.setState({ description: value });
  };

  toggleTextInputFocus = (value) => {
    this.setState({ inputFocused: value });
  };

  renderTextInputBox = (label, mandatory) => {
    const inputBoxStyle =
      this.state.inputFocused == label ? [styles.inputContainer, styles.activeTextBox] : [styles.inputContainer];

    return (
      <View style={inputBoxStyle}>
        <Text style={styles.inputLabel}>
          {label}
          {mandatory && <Text style={styles.redText}> *</Text>}
        </Text>
        <TextInput
          style={styles.textInput}
          numberOfLines={1}
          onChangeText={(value) => this.setInputText(label, value)}
          onFocus={() => this.toggleTextInputFocus(label)}
          onBlur={() => this.toggleTextInputFocus(null)}
        />
      </View>
    );
  };

  renderCardHeader = () => {
    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Create New Portfolio
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderInputs = () => {
    return (
      <View>
        {this.renderTextInputBox("Enter Portfolio Title", true)}
        {this.renderTextInputBox("Enter Portfolio Description", false)}
      </View>
    );
  };

  renderCreateButton = () => {
    let buttonActive = this.state.name.length > 0;
    const buttonStyle = buttonActive ? styles.createWatchlistBtn : [styles.createWatchlistBtn, styles.greyBackground];

    const buttonText = this.state.isAddButtonActive
      ? styles.createWatchlistBtnText
      : [styles.createWatchlistBtnText, styles.whiteText];

    return (
      <TouchableOpacity onPress={() => this.createPortfolio()} disabled={!buttonActive}>
        <View style={buttonStyle}>
          <Text style={buttonText}>Create Portfolio</Text>
        </View>
      </TouchableOpacity>
    );
  };

  //Using Modal instead of RBsheet to scroll views whenever keyboard appears
  render() {
    const { refreshing } = this.state;
    return (
      <Modal
        style={styles.modalStyle}
        position={"bottom"}
        ref={(ref) => (this.modal = ref)}
        swipeArea={20}
        animationDuration={100}
      >
        <View style={styles.infoContainer}>
          {this.renderCardHeader()}
          {this.renderInputs()}
          {this.renderCreateButton()}
        </View>

        <Toast ref="toast" position="center" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {refreshing && <LoadingView />}
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};
export default connect(mapStateToProps, null, null, { forwardRef: true })(CreatePortfolioModal);

const styles = StyleSheet.create({
  modalStyle: {
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
    backgroundColor: "white",
    height: hp(50),
  },
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(6),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  addWatchlistBtn: {
    position: "absolute",
    left: wp(8),
    right: wp(8),
    bottom: wp(8),
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
  },
  addWatchlistBtnDisabled: {
    backgroundColor: colors.steelGrey,
  },
  addWatchlistBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 2,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  subscriptionDialog: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary_color,
    paddingTop: wp(4),
    paddingHorizontal: wp(8),
    paddingBottom: ifIphoneX ? hp(3) : wp(4),
  },
  subscribeMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(4),
  },
  plansBtn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
  },
  planBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "center",
  },
  inputContainer: {
    paddingBottom: wp(1.5),
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3.7) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  inputLength: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.steelGrey,
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(6),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(2.5),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  inputLabel: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  // multiInputContainer: {
  //   flexDirection: "row"
  // },
  inputRightMargin: {
    marginRight: wp(5),
  },
  redText: {
    color: colors.red,
  },
  flex1: {
    flex: 1,
  },
  inputRow: {
    flexDirection: "row",
  },
  inputIcon: {
    width: wp(5),
    height: wp(5),
    resizeMode: "contain",
  },
  // list: {
  //   backgroundColor: colors.white,
  //   borderRadius: 5,
  //   shadowColor: "rgba(0, 0, 0, 0.3)",
  //   shadowOffset: {
  //     width: 0,
  //     height: 2
  //   },
  //   shadowRadius: 5,
  //   shadowOpacity: 1,
  //   elevation: 2,
  //   zIndex: 99
  // },
  // listFooterText: {
  //   color: colors.lightNavy,
  //   fontSize: RF(2.2)
  // },
  // listItem: {
  //   padding: wp(3),
  //   color: colors.greyishBrown,
  //   fontSize: RF(2.2)
  // },
  // footerItem: {
  //   paddingHorizontal: wp(3),
  //   paddingVertical: wp(4),
  //   borderTopWidth: 1,
  //   // opacity: 0.2,
  //   borderTopColor: "rgba(179, 179, 179,0.2)"
  // }
});
