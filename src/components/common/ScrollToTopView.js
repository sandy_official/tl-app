import React from "react";
import { View, StyleSheet, Image, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";

export default class ScrollToTopView extends React.PureComponent {
  arrowIcon = require("../../assets/icons/scroll-arrow.png");

  scrollToTop = () => {
    const { listRef } = this.props;

    if (listRef) {
      if (listRef.scrollToIndex) {
        listRef.scrollToIndex({ index: 0, animated: true });
      } else if (listRef.scrollTo) {
        listRef.scrollTo({ x: 0, y: 0, animated: true });
      }
    }
  };

  render() {
    const { isVisible } = this.props;

    if (isVisible) {
      return (
        <TouchableOpacity style={styles.container} onPress={this.scrollToTop}>
          <Image source={this.arrowIcon} resizeMode="contain" />
        </TouchableOpacity>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    right: 0,
    bottom: hp(8),
    width: wp(14),
    height: wp(12),
    backgroundColor: colors.cobalt,
    borderTopLeftRadius: wp(9),
    borderBottomLeftRadius: wp(9),
    elevation: 2,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: wp(2),
  },
});
