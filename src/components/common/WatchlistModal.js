import React from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { pushPopStockFromWatchlist } from "../../controllers/WatchlistController";
import LoadingView from "./LoadingView";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import { getActiveWatchlistByStockId } from "../../actions/WatchlistActions";

class WatchlistModal extends React.PureComponent {
  componentDidUpdate(prevProps, prevState, snapshot) {
    const { stock, getActiveWatchlistByStockId, activeWatchlist, userState, baseURL, appToken } = this.props;
    let newStockId = Object.keys(stock).length > 0 ? stock.stock_id : -1;

    if (newStockId != this.state.stockId) {
      this.setState({ stock: stock, stockId: newStockId, isCreateWatchlistBtnActive: false });
      getActiveWatchlistByStockId(baseURL, appToken, userState.token, newStockId, activeWatchlist);
    }
  }

  // componentWillReceiveProps(nextProps) {
  //   const { stock, getActiveWatchlistByStockId, activeWatchlist, userState } = nextProps;

  //   let newStockId = Object.keys(stock).length > 0 ? stock.stock_id : -1;
  //   let oldStockId = Object.keys(this.props.stock).length > 0 ? this.props.stock.stock_id : -1;

  //   // if (prevState.showSubscriptionDialog) this.setState({ showSubscriptionDialog: false });

  //   if (newStockId != oldStockId) {
  //     this.setState({ stock: stock, stockId: newStockId, isCreateWatchlistBtnActive: false });
  //     getActiveWatchlistByStockId(newStockId, activeWatchlist, userState.token);
  //   }
  // }

  open = () => {
    const { stock, getActiveWatchlistByStockId, activeWatchlist, userState, baseURL, appToken } = this.props;

    this.RBSheet.open();

    let newStockId = Object.keys(stock).length > 0 ? stock.stock_id : -1;
    if (newStockId !== -1) {
      this.setState({ stock: stock, stockId: newStockId, isCreateWatchlistBtnActive: false });
      getActiveWatchlistByStockId(baseURL, appToken, userState.token, newStockId, activeWatchlist);
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      showSubscriptionDialog: false,
      isCreateWatchlistBtnActive: false,
      textFocused: null,
      watchlist: [],
      stockId: -1,
      isRefreshing: false,
      stock: {},
    };
  }

  // open = () => this.RBSheet.open();

  addRemoveStock = (selectedWatchlist, index) => {
    const { activeWatchlist, onUpdate, userState, baseURL, appToken } = this.props;
    const { stock, showSubscriptionDialog } = this.state;

    let stockId = stock.stock_id;
    let activeList = activeWatchlist[stockId] ? activeWatchlist[stockId] : [];

    const watchlistId = selectedWatchlist[0] == null ? "" : selectedWatchlist[0];
    const watchlistNum = watchlistId == "" ? null : watchlistId;
    const action = activeList.includes(watchlistNum) ? "pop" : "push";

    this.setState({ isRefreshing: true });
    if (showSubscriptionDialog) this.setState({ showSubscriptionDialog: false });

    pushPopStockFromWatchlist(
      baseURL,
      appToken,
      userState.token,
      watchlistId,
      stockId,
      action,
      (isSuccess, status, message) => {
        this.setState({ isRefreshing: false });

        if (isSuccess) {
          if (action == "push") {
            activeList.push(watchlistNum);
            this.refs.toast.show("Added to " + selectedWatchlist[1], 1000);
          } else {
            let itemPos = activeList.indexOf(watchlistNum);
            if (itemPos >= 0) activeList.splice(itemPos, 1);
            this.refs.toast.show("Removed from " + selectedWatchlist[1], 1000);
          }

          let newActiveList = Object.assign({}, activeWatchlist);
          activeList.length > 0 ? (newActiveList[stockId] = activeList) : delete newActiveList[stockId];

          onUpdate(newActiveList);
        } else {
          //show error message
          console.log("status, message", status, message);
          if (status == 4) {
            this.setState({ showSubscriptionDialog: true });
            this.refs.toast.show(message);
          } else this.refs.toast.show("Please try again");
        }
      }
    );
  };

  closeDialog = () => {
    this.setState({ showSubscriptionDialog: false });
    this.RBSheet.close();
  };

  gotoSubscription = () => {
    this.closeDialog();
    this.props.navigation.navigate("Subscription");
  };

  showWatchlistAdded = () => {
    this.refs.toast ? this.refs.toast.show("Added to Watchlist") : setTimeout(() => this.showWatchlistAdded(), 200);
  };

  toggleSubscriptionDialog = () => {
    this.setState({
      showSubscriptionDialog: !this.state.showSubscriptionDialog,
    });
  };

  openCreateWatchlistModal = () => {
    const { watchlist, userState, createWatchList } = this.props;

    this.closeDialog();
    createWatchList();
  };

  renderNewWatchlistBtn = () => {
    return (
      <TouchableWithoutFeedback onPress={() => this.openCreateWatchlistModal()}>
        <View style={styles.addWatchlistBtn}>
          <Image source={require("../../assets/icons/add-blue.png")} style={styles.crossIcon} />
          <Text style={styles.addWatchlistBtnText}>New Watchlist</Text>
        </View>
      </TouchableWithoutFeedback>
    );
  };

  renderSubscriptionDialog = () => {
    const { showSubscriptionDialog } = this.state;
    const message = "Subscribe now to add more stocks to watchlists and access investment strategies!";

    if (showSubscriptionDialog) {
      return (
        <View style={styles.subscriptionDialog}>
          <Text style={styles.subscribeMessage}>{message}</Text>

          <TouchableOpacity
            onPress={this.gotoSubscription}
            // disabled={selectBtnDisabled}
          >
            <View style={styles.plansBtn}>
              <Text style={styles.planBtnText}>See Plans</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;
    const { stock } = this.state;

    const stockId = stock.stock_id;
    const activeList = stockId in activeWatchlist ? activeWatchlist[stockId] : [];
    const watchlistId = item[0] ? item[0] : null;

    const icon = activeList.includes(watchlistId)
      ? require("../../assets/icons/tick-blue-white.png")
      : require("../../assets/icons/add-black.png");

    const textStyle = activeList.includes(watchlistId) ? styles.itemName : [styles.itemName, styles.greyText];

    return (
      <TouchableOpacity onPress={() => this.addRemoveStock(item, index)}>
        <View style={styles.itemRow}>
          <Image source={icon} style={styles.itemIcon} />
          <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
            {item[1]}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderHeader = () => {
    const { stock } = this.state;
    var stockCode = "NSEcode" in stock && stock.NSEcode ? stock.NSEcode : stock.BSEcode;

    const title = `Add ${stockCode} to:`;
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          {title}
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderWatchlist = () => {
    const { watchlist } = this.props;

    return (
      <ScrollView>
        <FlatList
          data={watchlist}
          extraData={this.props}
          // showsVerticalScrollIndicator={false}
          renderItem={this.renderItem}
          keyExtractor={(item, index) => index.toString()}
        />
      </ScrollView>
    );
  };

  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(55)}
        duration={0}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {this.renderWatchlist()}
        </View>

        {this.renderNewWatchlistBtn()}
        {this.renderSubscriptionDialog()}

        {this.state.isRefreshing && <LoadingView />}

        <Toast ref="toast" position="center" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, { getActiveWatchlistByStockId }, null, { forwardRef: true })(WatchlistModal);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: Platform.OS == "android" ? hp(10) : 0,
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  itemRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
    alignItems: "center",
  },
  itemIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  itemName: {
    marginLeft: wp(3),
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  addWatchlistBtn: {
    // position: "absolute",
    // left: wp(8),
    // right: wp(8),
    // bottom: wp(8),
    marginHorizontal: wp(6),
    marginBottom: wp(8),
    marginTop: wp(4),
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(1.5),
    alignItems: "center",
    justifyContent: "center",
    zIndex: 10,
  },
  addWatchlistBtnDisabled: {
    backgroundColor: colors.steelGrey,
  },
  addWatchlistBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 7,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  subscriptionDialog: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary_color,
    paddingTop: wp(4),
    paddingHorizontal: wp(8),
    paddingBottom: ifIphoneX ? hp(3) : wp(4),
    zIndex: 99,
    elevation: 2,
  },
  subscribeMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(4),
  },
  plansBtn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
  },
  planBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "center",
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  list: {
    height: hp(34),
    // overflow: "scroll"
  },
});
