/**
 * Libraries
 */
import React from "react";
import { View, StyleSheet, Linking, AppState, Text } from "react-native";
import { connect } from "react-redux";

/**
 * Custom components, methods and constants
 */
import { updateFirebaseSettings, setAppToken } from "../../actions/AppActions";
import { deepLinking } from "../../utils/DeepLinking";
import { getFirebaseAppSettings } from "../../controllers/FirebaseController";
import { loadWatchlist, getActiveWatchlist } from "../../actions/WatchlistActions";
import { loadPortfolio } from "../../actions/PortfolioActions";
import NotificationListener from "../../listeners/NotificationListener";
// import  } from "../../actions/UserActions";
import { APP_VERSION } from "../../utils/Constants";
import GlobalText from "../../styles/GlobalText";
// import { updateAppUrls } from "../../actions/AppActions";

class AppContainer extends React.PureComponent {
  state = { appState: null };

  componentDidMount() {
    setTimeout(this.init, 2000);
  }

  /**
   * Removing the App state listener
   */
  componentWillUnmount() {
    const { forceUpdateVersion } = this.props;

    if (APP_VERSION > forceUpdateVersion) {
      AppState.removeEventListener("change", this._handleAppStateChange);
      Linking.removeEventListener("url", null);
    }
  }

  init = () => {
    const { forceUpdateVersion } = this.props;

    if (APP_VERSION > forceUpdateVersion) {
      this.loadData();
      AppState.addEventListener("change", this._handleAppStateChange);

      Linking.addEventListener("url", (event) => {
        console.warn("URL", event.url);
        let url = event.url;
        url && this._handleUrl(url);
      });

      this._checkInitialUrl();
    }

    this.notificationListener = new NotificationListener(this._handleUrl);
    GlobalText.applyFontFamily("Roboto-Regular");
  };

  loadData = () => {
    // const { userState, baseURL, appToken, loadWatchlist, loadPortfolio, getActiveWatchlist } = this.props;
    // const { token, isGuestUser } = userState;

    this.getFirebaseData();

    // if (!isGuestUser) {
    //   loadPortfolio(baseURL, appToken, token);
    //   loadWatchlist(baseURL, appToken, token, "");
    //   getActiveWatchlist(baseURL, appToken, token);
    // }
  };

  getFirebaseData = () =>
    getFirebaseAppSettings((data) => {
      let appToken = "TrendlyneAppToken" in data.auth ? data.auth.TrendlyneAppToken : null;
      if (appToken != null && this.props.token == null) {
        this.props.setAppToken(appToken);
      }
      this.props.updateFirebaseSettings(data);
    });

  /**
   * Deep linking
   */
  _handleAppStateChange = async (nextAppState) => {
    const { appState } = this.state;
    console.log("setting app listener");
    // if (appState && appState.match(/inactive|background/) && nextAppState === "active") {
    if (appState && appState.match(/inactive/) && nextAppState === "active") {
      this._checkInitialUrl();
    }
    this.setState({ appState: nextAppState });
  };

  _checkInitialUrl = async () => {
    const url = await this._getInitialUrl();
    console.log("checking url", url);

    this._handleUrl(url);
  };

  _getInitialUrl = async () => {
    const url = await Linking.getInitialURL();
    return url;
  };

  _handleUrl = (url) => {
    const { navigation, userState } = this.props;
    // write your url handling logic here, such as navigation
    console.log("received URL: ", url);

    if (url) {
      const navObj = deepLinking(url);
      console.log("Parsed url", navObj);
      setTimeout(() => {
        console.log("Navigating", navigation.state.routeName);
        navigation && navigation.navigate("Home", navObj.params);
        navigation && navigation.navigate(navObj.screen, navObj.params);
      }, 200);
    }
  };

  /**
   * Render method
   */
  render() {
    return <View style={styles.container}>{this.props.children}</View>;
  }
}

mapStateToProps = (state) => {
  let firebase = state.app.firebase ? state.app.firebase : {};
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;
  let forceUpdateVersion = "forceUpdateVersion" in version ? version.forceUpdateVersion : 1;
  let token = state.user.token;

  return { userState: state.user, baseURL, appToken, token, forceUpdateVersion };
};

export default connect(
  mapStateToProps,
  { updateFirebaseSettings, loadWatchlist, getActiveWatchlist, loadPortfolio, setAppToken },
  null,
  { forwardRef: true }
)(AppContainer);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
});
