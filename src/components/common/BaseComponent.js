import React from "react";
import { View, StyleSheet } from "react-native";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";

/**
 * Custom components
 */
import LoginModal from "./LoginModal";
import LoadingView from "../../components/common/LoadingView";
import { globalStyles, colors } from "../../styles/CommonStyles";

/**
 * Bottom sheets
 */
import SuperstarAlertNonSub from "../superstar/SuperstarAlertNonSub";
import WatchlistModal from "./WatchlistModal";
import CreateWatchlistModal from "./CreateWatchlistModal";
import StockAlertsModal from "./StockAlertsModal";
import StockPortfolioModal from "./StockPortfolioModal";
import CreatePortfolioModal from "./CreatePortfolioModal";

/**
 * Redux actions
 */
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";

class BaseComponent extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedStock: {}, refreshing: false, isFirstCallComplete: false };
  }

  isLoading = () => this.state.refreshing;

  showLoading = () => {
    this.setState({ refreshing: true });
    // if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
  };

  stopLoading = () => this.setState({ refreshing: false });

  showLogin = () => this.refs.login.open();

  showSubscribe = () => this.refs.alertNonSubscriber.open();

  showToast = (message) => this.refs.toast.show(message);

  showAlerts = (stock) => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.stockAlerts.open());
      //   this.setState({ selectedStock: stock }, () => this.refs.stockAlerts.open());
    } else this.showLogin();
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.Watchlists.open());
    } else this.showLogin();
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.portfolio.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.showLogin();
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;

    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  showPortfolioModal = () => {
    this.refs.portfolio.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;
    const { selectedStock, refreshing } = this.state;

    return (
      <View style={{ flex: 1 }}>
        {this.props.children}

        {/* Stock Alerts */}
        <StockAlertsModal
          ref="stockAlerts"
          {...this.props}
          stock={selectedStock}
          // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
          // data={dateList}
          // onPress={quarter => this.setQuarterString(quarter)}
        />

        {/* WatchList */}
        <WatchlistModal
          ref="Watchlists"
          {...this.props}
          stock={selectedStock}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={(obj) => setActiveWatchlist(obj)}
          createWatchList={() => this.refs.createWatchlist.modal.open()}
        />

        <CreateWatchlistModal
          ref="createWatchlist"
          {...this.props}
          stock={selectedStock}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={this.onWatchlistCreated}
          openWatchlist={() => this.refs.Watchlists.open()}
        />

        {/* Portfolio */}
        <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={selectedStock}
          portfolio={portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        />

        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={selectedStock}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

        <SuperstarAlertNonSub ref="alertNonSubscriber" {...this.props} />
        <LoginModal ref="login" {...this.props} />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(BaseComponent);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
});
