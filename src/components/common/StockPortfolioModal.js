import React from "react";
import {
  View,
  StyleSheet,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  TextInput,
  Platform,
  TouchableWithoutFeedback,
} from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import Modal from "../../libs/react-native-modalbox";
import { MONTHS_FULL } from "../../utils/Constants";
import DateTimePicker from "react-native-modal-datetime-picker";
import { getOrdinalNumForDate } from "../../utils/Utils";
import { addStockToPortfolio } from "../../controllers/PortfolioController";
import Toast from "react-native-easy-toast";
import LoadingView from "./LoadingView";
import { connect } from "react-redux";

class StockPortfolioModal extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      portfolio: "",
      transactionDate: "",
      transactDateForApi: "",
      quantity: "",
      price: "",
      inputFocused: null,
      showPortfolioList: false,
      dropdownPosition: {},
      isDatePickerVisible: false,
      refreshing: false,
    };
  }

  open = () => this.modal.open();

  isFormValidated = () => {
    const { quantity, price } = this.state;

    if (isNaN(quantity)) this.refs.toast.show("Quantity must be a Number", 1000);
    else if (quantity <= 0) this.refs.toast.show("Enter valid Quantity", 1000);
    else if (isNaN(price)) this.refs.toast.show("Price must be a Number", 1000);
    else if (price <= 0) this.refs.toast.show("Enter valid Price", 1000);
    else return true;

    return false;
  };

  addToPortfolio = () => {
    const { stock, addedTo, userState, baseURL, appToken } = this.props;
    const { portfolio, transactDateForApi, quantity, price } = this.state;
    const stockId = stock.stock_id ? stock.stock_id : -1;
    const portfolioId = portfolio[0] ? portfolio[0] : -1;
    const transactDate = transactDateForApi ? transactDateForApi : "";
    const qty = quantity ? quantity : -1;
    const stockPrice = price ? price : -1;

    if (stockId != -1 && portfolioId != -1 && transactDate != "" && this.isFormValidated()) {
      this.setState({ refreshing: true });

      addStockToPortfolio(
        baseURL,
        appToken,
        userState.token,
        portfolioId,
        stockId,
        transactDate,
        qty,
        stockPrice,
        (response) => {
          this.setState({ refreshing: false });

          if (response) {
            addedTo();
            this.closeDialog();
          } else {
            this.refs.toast.show("Please try again", 1000);
          }
        }
      );
    }
  };

  closeDialog = () => {
    this.setState({
      portfolio: "",
      transactionDate: "",
      transactDateForApi: "",
      quantity: "",
      price: "",
      inputFocused: null,
      showPortfolioList: false,
      isDatePickerVisible: false,
    });
    this.modal.close();
  };

  showPortfolioAdded = () => {
    this.refs.toast ? this.refs.toast.show("Portfolio Added", 1000) : null;
  };

  toggleDropdown = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "dropdown" ? "" : "dropdown";
    this.setState({
      showPortfolioList: !this.state.showPortfolioList,
      inputFocused: focusedInput,
    });
  };

  toggleDatePicker = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "date" ? "" : "date";

    this.setState({
      isDatePickerVisible: !this.state.isDatePickerVisible,
      inputFocused: focusedInput,
    });
  };

  handleDatePicked = (date) => {
    this.toggleDatePicker();

    let day = date.getDate();
    let month = MONTHS_FULL[date.getMonth()];
    let year = date.getFullYear();

    let monthApi = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;

    // 2015-08-01
    let formattedDate = getOrdinalNumForDate(day) + " " + month + ", " + year;
    let dateForApi = year + "-" + monthApi + "-" + day;

    this.setState({
      transactionDate: formattedDate,
      transactDateForApi: dateForApi,
    });
    // console.log("A date has been picked: ", formattedDate);
  };

  setInputText = (type, value) => {
    type == "Price" ? this.setState({ price: value }) : this.setState({ quantity: value });
  };

  toggleTextInputFocus = (value) => {
    this.setState({ inputFocused: value });
  };

  updateDropdownPosition = (event) => {
    this.setState({
      dropdownPosition: event.nativeEvent.layout,
    });
  };

  selectPortfolio = (item) => {
    // console.log("Selected Portfolio", item);
    this.setState({ portfolio: item });
    this.toggleDropdown();
  };

  renderPortfolioItem = ({ item, index }) => {
    console.log("item =-->",item)
    return (
      <TouchableOpacity onPress={() => this.selectPortfolio(item)}>
        <Text style={styles.listItem}>{item[1]}</Text>
      </TouchableOpacity>
    );
  };

  resetForm = () => {
    this.setState({
      portfolio: "",
      transactionDate: "",
      quantity: "",
      price: "",
      inputFocused: null,
    });
  };

  createNewPortfolio = () => {
    this.props.createNew();
    this.toggleDropdown();
    this.resetForm();
    this.closeDialog();
  };

  renderPortfolioListFooter = () => {
    return (
      <TouchableWithoutFeedback style={styles.footerItem} onPress={this.createNewPortfolio}>
        <Text style={styles.listFooterText}>Create New Portfolio</Text>
      </TouchableWithoutFeedback>
    );
  };

  renderPortfolioList = () => {
    const { portfolio } = this.props;
    const { x, y, width, height } = this.state.dropdownPosition;
    const style = {
      position: "absolute",
      top: Platform.OS == "android" ? y + hp(8) : y + hp(7),
      left: x,
      width: width,
      maxHeight: hp(30),
      backgroundColor: colors.white,
    };
  // console.log("portfolio inside component -->",portfolio)
    if (this.state.showPortfolioList) {
      return (
        <View style={[style, styles.list]}>
          <FlatList
            data={portfolio}
            extraData={this.props}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderPortfolioItem}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={() => this.renderPortfolioListFooter()}
            keyboardShouldPersistTaps="always"
          />
        </View>
      );
    }
    return null;
  };

  renderTextInputBox = (label, extraStyle) => {
    const inputBoxStyle =
      this.state.inputFocused == label
        ? [styles.inputContainer, styles.activeTextBox, extraStyle]
        : [styles.inputContainer, extraStyle];

    return (
      <View style={inputBoxStyle}>
        <Text style={styles.inputLabel}>
          {label}
          <Text style={styles.redText}> *</Text>
        </Text>

        <TextInput
          // maxLength={length}
          style={[styles.textInput, styles.textInputAdjust]}
          keyboardType="numeric"
          maxLength={7}
          numberOfLines={1}
          onChangeText={(value) => this.setInputText(label, value)}
          onFocus={() => this.toggleTextInputFocus(label)}
          onBlur={() => this.toggleTextInputFocus(null)}
        />
      </View>
    );
  };

  renderCardHeader = () => {
    const { stock } = this.props;
    var stockCode = "";

    if (stock) {
      if ("NSEcode" in stock && stock.NSEcode) stockCode = stock.NSEcode;
      else if ("BSEcode" in stock) stockCode = stock.BSEcode;

      const title = `Add ${stockCode} to:`;
      const crossIcon = require("../../assets/icons/cross-blue.png");

      return (
        <View style={styles.row}>
          <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
            {title}
          </Text>
          <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
            <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderPortfolioDropdown = () => {
    const inputBoxStyle =
      this.state.inputFocused == "dropdown" ? [styles.inputContainer, styles.activeTextBox] : [styles.inputContainer];

    const dropdownIcon = this.state.showPortfolioList
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");

    return (
      <View style={inputBoxStyle} c>
        <Text style={[styles.inputLabel]}>
          Select a Portfolio <Text style={styles.redText}> *</Text>
        </Text>

        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDropdown}>
          <Text style={[styles.textInput, { flex: 1 }]}>{this.state.portfolio[1]}</Text>
          <Image source={dropdownIcon} style={styles.inputIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderDateField = () => {
    const inputBoxStyle =
      this.state.inputFocused == "date" ? [styles.inputContainer, styles.activeTextBox] : [styles.inputContainer];

    return (
      <View style={inputBoxStyle}>
        <Text style={[styles.inputLabel]}>  Transaction Date <Text style={styles.redText}> *</Text></Text>

        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDatePicker}>
          {/* <View style={styles.inputRow}> */}
          <Text style={[styles.textInput, { flex: 1 }]}>{this.state.transactionDate}</Text>
          <Image source={require("../../assets/icons/date-picker-blue.png")} style={styles.inputIcon} />
          {/* </View> */}
        </TouchableOpacity>
      </View>
    );
  };

  renderInputs = () => {
    const leftTextInputStyle = [styles.inputRightMargin, styles.flex1];
    const rightTextInputStyle = styles.flex1;
    return (
      <View onLayout={this.updateDropdownPosition}>
        {this.renderPortfolioDropdown()}
        {this.renderDateField()}
        <View style={styles.multiInputContainer}>
          {this.renderTextInputBox("Quantity", leftTextInputStyle)}
          {this.renderTextInputBox("Price", rightTextInputStyle)}
        </View>
      </View>
    );
  };

  isAddButtonActive = () => {
    return (
      this.state.portfolio.length > 0 &&
      this.state.transactionDate.length > 0 &&
      this.state.quantity.length > 0 &&
      this.state.price.length > 0
    );
  };

  renderAddButton = () => {
    let buttonActive = this.isAddButtonActive();
    const buttonStyle = buttonActive ? styles.createWatchlistBtn : [styles.createWatchlistBtn, styles.greyBackground];
    const buttonText = this.state.isAddButtonActive ? styles.createWatchlistBtnText : [styles.createWatchlistBtnText, styles.whiteText];
    const isWatchlistBtnDisabled = !buttonActive;

    return (
      <TouchableOpacity onPress={() => this.addToPortfolio()} disabled={isWatchlistBtnDisabled}>
        <View style={buttonStyle}>
          <Text style={buttonText}>Add to Portfolio</Text>
        </View>
      </TouchableOpacity>
    );
  };

  //Using Modal instead of RBsheet to scroll views whenever keyboard appears
  render() {
    const { refreshing } = this.props;

    return (
      <Modal style={styles.modalStyle} position={"bottom"} ref={(ref) => (this.modal = ref)} animationDuration={100}>
        <View style={styles.infoContainer}>
          {this.renderCardHeader()}
          {this.renderInputs()}
          {this.renderAddButton()}
        </View>

        <DateTimePicker
          isVisible={this.state.isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.toggleDatePicker}
        />

        {this.renderPortfolioList()}

        <Toast ref="toast" position="center" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {refreshing && <LoadingView />}
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockPortfolioModal);

const styles = StyleSheet.create({
  modalStyle: {
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
    backgroundColor: "white",
    height: Platform.OS == "ios" ? hp(55) : hp(58),
  },
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(6),
    marginTop: hp(2),
    marginRight: wp(4),
    // paddingHorizontal: wp(4)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
    // paddingHorizontal: wp(4)
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    // height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  itemRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
  },
  itemIcon: {
    width: wp(4),
    height: wp(3),
    resizeMode: "contain",
  },
  itemName: {
    marginLeft: wp(3),
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  addWatchlistBtn: {
    position: "absolute",
    left: wp(8),
    right: wp(8),
    bottom: wp(8),
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
  },
  addWatchlistBtnDisabled: {
    backgroundColor: colors.steelGrey,
  },
  addWatchlistBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 2,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  subscriptionDialog: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary_color,
    paddingTop: wp(4),
    paddingHorizontal: wp(8),
    paddingBottom: ifIphoneX ? hp(3) : wp(4),
  },
  subscribeMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(4),
  },
  plansBtn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
  },
  planBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "center",
  },
  inputContainer: {
    paddingBottom: wp(1.5),
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  textInput: {
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3.7) : 0,
  },
  textInputAdjust: {
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  inputLength: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.steelGrey,
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(6),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(2.5),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },

  inputLabel: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  multiInputContainer: {
    flexDirection: "row",
  },
  inputRightMargin: {
    marginRight: wp(5),
  },
  redText: {
    color: colors.red,
  },
  flex1: {
    flex: 1,
  },
  inputRow: {
    flexDirection: "row",
    paddingTop: wp(2),
    paddingBottom: wp(0.8),
  },
  inputIcon: {
    width: wp(5),
    height: wp(5),
    resizeMode: "contain",
  },
  list: {
    height: 160,
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    zIndex: 99,
  },
  listFooterText: {
    color: colors.lightNavy,
    fontSize: RF(2.2),
    // marginBottom:wp(6),
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
  },
  listItem: {
    padding: wp(3),
    color: colors.greyishBrown,
    fontSize: RF(2.2),
  },
  footerItem: {
    // paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    borderTopWidth: 1,
    // opacity: 0.2,
    borderTopColor: "rgba(179, 179, 179,0.2)",
  },
});
