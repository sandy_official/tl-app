import React from "react";
import { PagerTitleIndicator, IndicatorViewPager } from "../../libs/rn-viewpager";
import { globalStyles } from "../../styles/CommonStyles";
import firebase from "react-native-firebase";
import { setFirebaseAnalyticsEvent } from "../../controllers/FirebaseController";

scrollRetryCount = 0;

export default class TabComponent extends React.PureComponent {
  componentWillMount() {
    try {
      this.scrollToIndex();
    } catch (e) {
      console.log("tab auto scroll issue");
    }
  }

  setPage = (index) => this.tabs && this.tabs.setPage(index);

  scrollToIndex = () => {
    const { initialTab } = this.props;
    setTimeout(() => {
      if (this.tabs && initialTab) this.tabs.setPage(initialTab);
      else {
        scrollRetryCount++;
        this.scrollToIndex();
      }
    }, 200);
  };

  renderTabIndicator() {
    const { tabs } = this.props;

    return (
      <PagerTitleIndicator
        style={globalStyles.indicatorContainer}
        itemTextStyle={globalStyles.indicatorText}
        selectedItemTextStyle={globalStyles.indicatorSelectedText}
        itemStyle={globalStyles.tabSize}
        selectedItemStyle={globalStyles.tabSize}
        trackScroll={true}
        titles={tabs}
        selectedBorderStyle={{ backgroundColor: "transparent" }}
      />
    );
  }

  pageChanged = async (obj) => {
    const { tabs, analyticsParams } = this.props;
    const currentScreen = tabs[obj.position];
    firebase.analytics().setCurrentScreen(currentScreen);
    setFirebaseAnalyticsEvent(currentScreen, analyticsParams());
  };

  renderTabs = () => {
    const { initialTab, onTabSelect } = this.props;

    return (
      <IndicatorViewPager
        ref={(ref) => (this.tabs = ref)}
        style={{ flex: 1 }}
        indicator={this.renderTabIndicator()}
        pagerStyle={globalStyles.pagerStyle}
        onPageSelected={(obj) => {
          onTabSelect(obj);
          this.pageChanged(obj);
        }}
        // initialPage={initialTab}
      >
        {this.props.children}
      </IndicatorViewPager>
    );
  };

  render() {
    return this.renderTabs();
  }
}
