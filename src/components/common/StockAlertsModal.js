import React from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, Image } from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import Toast from "react-native-easy-toast";
import { setStockAlert, getActiveStockAlerts } from "../../controllers/StockAlertsController";
import LoadingView from "./LoadingView";
import { connect } from "react-redux";

class StockAlertsModal extends React.PureComponent {
  // componentDidUpdate(prevProps, prevState, snapshot) {
  //   let newStockId =
  //     Object.keys(this.props.stock).length > 0 ? this.props.stock.stock_id : -1;

  //   if (newStockId != this.state.stockId) {
  //     this.setState({ stockId: newStockId, alertOptions: {} }, () =>
  //       this.getActiveAlerts()
  //     );
  //   }
  // }

  constructor(props) {
    super(props);
    this.state = {
      // isChange: false,
      // selectedQuarter: "",
      alertOptions: {},
      stockId: -1,
      oldStockId: -1,
      refreshing: false,
    };
  }

  open = () => {
    const { stockId, oldStockId } = this.state;

    this.RBSheet.open();

    let newStockId = Object.keys(this.props.stock).length > 0 ? this.props.stock.stock_id : -1;

    if (newStockId != oldStockId) {
      this.setState({ stockId: newStockId, oldStockId: stockId, alertOptions: {} }, () => {
        this.getActiveAlerts();
      });
    } else this.getActiveAlerts();
  };

  refresh = () => {
    this.forceUpdate();
  };

  getActiveAlerts = () => {
    const { stock, userState, baseURL, appToken } = this.props;
    // console.log("stock Id : " + stock.stock_id);
    // console.log("Stock alerts", stock);

    if (Object.keys(stock).length > 0) {
      this.setState({ refreshing: true });

      getActiveStockAlerts(baseURL, appToken, userState.token, stock.stock_id, (data) => {
        this.setState({ refreshing: false });

        if ("volume" in data) {
          const volumeOptions = data.volume;
          if ("Week" in volumeOptions) this.state.alertOptions["vw" + volumeOptions.Week[0]] = true;

          if ("Month" in volumeOptions) this.state.alertOptions["vm" + volumeOptions.Month[0]] = true;
        }

        if ("price" in data) {
          const priceOptions = data.price;

          if ("Day" in priceOptions) {
            this.state.alertOptions["pd" + priceOptions.Day] = true;
          }

          if ("Week" in priceOptions) this.state.alertOptions["pw" + priceOptions.Week] = true;

          if ("Month" in priceOptions) this.state.alertOptions["pm" + priceOptions.Month] = true;
        }

        if ("research_report" in data && data["research_report"]) {
          this.state.alertOptions["report"] = true;
        }

        if ("sma" in data) {
          data.sma.forEach((item) => (this.state.alertOptions["sma" + item] = true));
        }

        this.forceUpdate();
      });
    }
  };

  addStockAlert = (buttonState, alertType, alertPeriod, threshold) => {
    const { stock, addedTo, userState, baseURL, appToken } = this.props;
    // console.log("stock id : " + stock.stock_id);
    // console.log("alert type: " + alertType);
    // console.log("alert period: " + alertPeriod);
    // console.log("threshold: " + threshold);

    const action = buttonState ? "add" : "delete";
    // console.log("action : " + action)

    setStockAlert(
      baseURL,
      appToken,
      userState.token,
      action,
      stock.stock_id,
      alertType,
      alertPeriod,
      threshold,
      (message) => {
        console.log(message);
      }
    );
  };

  closeDialog = () => {
    this.RBSheet.close();
  };

  showToast = (buttonState) => {
    if (buttonState) this.refs.toast.show("Alert Added");
    else this.refs.toast.show("Alert Removed");
  };

  changePriceAlerts = (key, group) => {
    var keyList = [];
    var alertPeriod = "";

    if (group == "priceDay") {
      alertPeriod = "Day";
      keyList = ["pd5", "pd10", "pd15"];
    } else if (group == "priceWeek") {
      alertPeriod = "Week";
      keyList = ["pw10", "pw15", "pw20"];
    } else {
      alertPeriod = "Month";
      keyList = ["pm15", "pm20"];
    }

    keyList.forEach((item) => {
      if (item != key) this.state.alertOptions[item] = false;
    });

    this.addStockAlert(!this.state.alertOptions[key], "price", alertPeriod, key.slice(2));
  };

  changeVolumeAlerts = (key, group) => {
    var keyList = [];
    var alertPeriod = "";

    if (group == "volumeWeek") {
      alertPeriod = "Week";
      keyList = ["vw3", "vw4"];
    } else {
      alertPeriod = "Month";
      keyList = ["vm3", "vm4"];
    }

    keyList.forEach((item) => {
      if (item != key) this.state.alertOptions[item] = false;
    });

    this.addStockAlert(!this.state.alertOptions[key], "volume", alertPeriod, key.slice(2) + "x");
  };

  setAlert = (key, groupName) => {
    if (groupName.length > 0) {
      switch (groupName) {
        case "priceDay":
        case "priceWeek":
        case "priceMonth":
          this.changePriceAlerts(key, groupName);
          break;
        case "volumeWeek":
        case "volumeMonth":
          this.changeVolumeAlerts(key, groupName);
          break;
      }

      this.state.alertOptions[key] = !this.state.alertOptions[key];
      this.showToast(this.state.alertOptions[key]);
      this.forceUpdate();
    } else {
      this.state.alertOptions[key] = !this.state.alertOptions[key];
      this.showToast(this.state.alertOptions[key]);
      this.forceUpdate();

      var alertType = key == "report" ? "research_report" : "sma";
      var alertPeriod = key == "report" ? "" : key.slice(3);
      this.addStockAlert(this.state.alertOptions[key], alertType, alertPeriod, "");
    }

    //Report download on report pages is allowed only for Sign in users

    // this.refs.toast.show("Alert Added");
  };

  renderOptionButton = (name, key, groupName, containerStyle) => {
    const buttonStyle = this.state.alertOptions[key]
      ? [styles.optionButton, containerStyle, styles.activeButton]
      : [styles.optionButton, containerStyle];

    const buttonTextStyle = this.state.alertOptions[key]
      ? [styles.optionButtonText, styles.activeButtonText]
      : [styles.optionButtonText];

    return (
      <TouchableOpacity onPress={() => this.setAlert(key, groupName)}>
        <View style={buttonStyle}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={buttonTextStyle}>
            {name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderPriceAlerts = () => {
    return (
      <View>
        <Text style={styles.alertCategory}>Price</Text>

        <View style={styles.optionRow}>
          <Text style={styles.alertType}>Day:</Text>
          {this.renderOptionButton("+/-5%", "pd5", "priceDay")}
          {this.renderOptionButton("+/-10%", "pd10", "priceDay")}
          {this.renderOptionButton("+/-15%", "pd15", "priceDay")}
        </View>

        <View style={styles.optionRow}>
          <Text style={styles.alertType}>Week:</Text>
          {this.renderOptionButton("+/-10%", "pw10", "priceWeek")}
          {this.renderOptionButton("+/-15%", "pw15", "priceWeek")}
          {this.renderOptionButton("+/-20%", "pw20", "priceWeek")}
        </View>

        <View style={styles.optionRow}>
          <Text style={styles.alertType}>Month:</Text>
          {this.renderOptionButton("+/-15%", "pm15", "priceMonth")}
          {this.renderOptionButton("+/-20%", "pm20", "priceMonth")}
          {/* <View style={{ flex: 1 }} /> */}
        </View>
      </View>
    );
  };

  renderSmaCrossoverAlerts = () => {
    return (
      <View>
        <Text style={styles.alertCategory}>SMA Crossover</Text>

        <View style={[styles.optionRow]}>
          {this.renderOptionButton("SMA 30", "sma30", "", styles.smaOption)}
          {this.renderOptionButton("SMA 50", "sma50", "", styles.smaOption)}
          {this.renderOptionButton("SMA 100", "sma100", "", styles.smaOption)}
        </View>

        <View style={[styles.optionRow]}>
          {this.renderOptionButton("SMA 150", "sma150", "", styles.smaOption)}
          {this.renderOptionButton("SMA 200", "sma200", "", styles.smaOption)}
        </View>
      </View>
    );
  };

  renderVolumeAlerts = () => {
    return (
      <View>
        <Text style={styles.alertCategory}>Volume</Text>

        <View style={styles.optionRow}>
          <Text style={styles.alertType}>Week:</Text>
          {this.renderOptionButton("3x", "vw3", "volumeWeek")}
          {this.renderOptionButton("4x", "vw4", "volumeWeek")}
          {/* <View style={{ flex: 1 }} /> */}
        </View>

        <View style={styles.optionRow}>
          <Text style={styles.alertType}>Month:</Text>
          {this.renderOptionButton("3x", "vm3", "volumeMonth")}
          {this.renderOptionButton("4x", "vm4", "volumeMonth")}
          {/* <View style={{ flex: 1 }} /> */}
        </View>
      </View>
    );
  };

  renderResearchReportOption = () => {
    const buttonStyle = this.state.alertOptions["report"]
      ? [styles.optionButton, styles.reportBtn, styles.activeButton]
      : [styles.optionButton, styles.reportBtn];

    const buttonTextStyle = this.state.alertOptions["report"]
      ? [styles.optionButtonText, styles.activeButtonText]
      : [styles.optionButtonText];

    return (
      <View>
        <Text style={styles.alertCategory}>Research Report</Text>

        <TouchableOpacity
          onPress={() => this.setAlert("report", "")}
          // disabled={selectBtnDisabled}
        >
          <View style={buttonStyle}>
            <Text style={buttonTextStyle}>New Research Report</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderHeader = () => {
    const { stock } = this.props;
    const stockCode = stock ? (stock.NSEcode ? stock.NSEcode : stock.BSEcode) : "";

    return (
      <View style={styles.row}>
        <Text style={styles.title}>Set up alerts for {stockCode}</Text>

        <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
          <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { refreshing } = this.state;

    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(95)}
        duration={300}
        closeOnDragDown={true}
        customStyles={{
          container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) },
        }}
      >
        <View style={styles.infoContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {this.renderHeader()}
            {this.renderPriceAlerts()}
            {this.renderSmaCrossoverAlerts()}
            {this.renderVolumeAlerts()}
            {this.renderResearchReportOption()}
          </ScrollView>

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {refreshing && <LoadingView />}
        </View>
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockAlertsModal);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    height: hp(5), //8
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(2),
  },
  alertCategory: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    marginTop: wp(5),
    marginBottom: wp(1),
  },
  optionRow: {
    flexDirection: "row",
    marginVertical: wp(2),
    alignItems: "center",
  },
  optionButton: {
    flex: 1,
    width: wp(19),
    marginRight: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  optionButtonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    padding: wp(1.5),
  },
  alertType: {
    width: wp(19),
    fontSize: RF(2.2),
    color: colors.greyishBrown,
  },
  smaRow: {
    flexDirection: "row",
  },
  smaOption: {
    width: wp(22),
  },
  reportBtn: {
    marginTop: wp(3),
    width: wp(55),
  },
  activeButton: {
    backgroundColor: colors.primary_color,
  },
  activeButtonText: {
    color: colors.white,
  },
});
