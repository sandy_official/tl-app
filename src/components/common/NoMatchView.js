import React from "react";
import { View, StyleSheet, Text, Image } from "react-native";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export default class NoMatchView extends React.PureComponent {
  render() {
    const { message, heading, imageSrc, style } = this.props;
    const showHeading = heading != undefined;
    const image = imageSrc != undefined ? imageSrc : require("../../assets/icons/no-match-grey.png");

    return (
      <View style={[styles.container, style]}>
        <View style={styles.messageContainer}>
          <Image source={image} />
          {showHeading && <Text style={styles.message}>{heading}</Text>}
          <Text style={styles.message}>{message}</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    // top: hp(10),
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor:'red'
  },
  messageContainer: {
    alignItems: "center",
  },
  message: {
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginTop: wp(6),
  },
});
