/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { statusCodes } from "react-native-google-signin";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import RBSheet from "react-native-raw-bottom-sheet";

/**
 * Custom components, methods and constants
 */
import { doFacebookLogin, doGoogleSignIn, getUserDetails } from "../../controllers/UserController";
import { doUserLogin } from "../../actions/UserActions";
import { colors } from "../../styles/CommonStyles";
import { setNavigationState } from "../../utils/Constants";
import { loadUserDetails } from "../../actions/UserActions";
import { clearAsync } from "../../controllers/AsyncController";
import { clearPortfolio } from "../../actions/PortfolioActions";
import { clearWatchlistAll } from "../../actions/WatchlistActions";
import { doUserLogout } from "../../actions/UserActions";
import { clearMarket } from "../../actions/MarketActions";

/**
 * Onboarding screen
 * shows list of features and Login methods
 */
class LoginModal extends React.PureComponent {
  state = { refreshing: false };

  open = () => {
    const { baseURL, appToken, doUserLogout, userState, clearPortfolio, clearWatchlistAll, clearMarket } = this.props;
    const { token, isLoggedIn } = userState;

    this.RBSheet.open();
    setNavigationState(this.props.navigation.state);

    if (isLoggedIn) {
      getUserDetails(baseURL, appToken, token, (data, status) => {
        if (status == 100) {
          clearAsync();
          doUserLogout();
          clearPortfolio();
          clearWatchlistAll();
          clearMarket();
        }
      });
    }
  };

  closeDialog = () => {
    this.RBSheet.close();
  };

  /**
   * Goto Email login screen
   */
  openLogin = () => {
    this.props.navigation.navigate("Login");
  };

  /**
   * Facebook sign in function
   */
  onFacebookLogin = async () => {
    const { navigation, doUserLogin, baseURL, appToken, loadUserDetails, refreshScreen } = this.props;

    this.setState({ refreshing: true });

    doFacebookLogin(
      baseURL,
      appToken,
      (token) => {
        this.setState({ refreshing: false });

        if (token) {
          doUserLogin(token);
          loadUserDetails(baseURL, appToken, token);
          refreshScreen && refreshScreen();
          this.closeDialog();
        } else {
          this.refs.toast.show("Login failed");
        }
      },
      (error) => {
        this.setState({ refreshing: false });
        console.log("Please try again later.", error);
      }
    );
  };

  /**
   * Google sign in function
   */
  onGoogleLogin = async () => {
    const { doUserLogin, baseURL, appToken, loadUserDetails, refreshScreen, googleKey } = this.props;
    this.setState({ refreshing: true });

    doGoogleSignIn(
      baseURL,
      appToken,
      googleKey,
      (token) => {
        // onSuccess
        this.setState({ refreshing: false });
        if (token != null) {
          doUserLogin(token);
          loadUserDetails(baseURL, appToken, token);
          refreshScreen && refreshScreen();
          this.closeDialog();
        } else {
          this.refs.toast.show("Login failed");
        }
      },
      (error) => {
        // onFailure
        this.setState({ refreshing: false });

        if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          // play services not available or outdated
          this.refs.toast.show("Google play service not available");
        } else {
          // some other error happened
          console.log("Google sign in something went wrong", error);
        }
      }
    );
  };

  /**
   * render login view with login buttons and signup link
   * @returns {view} view with login buttons
   */
  renderHeader = () => {
    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Log in or Sign up to Continue:
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderLoginCard = () => {
    return (
      <View>
        {this.renderLoginButtons()}

        <Text style={styles.description}>
          Already have an Account?{" "}
          <Text style={styles.loginText} onPress={this.openLogin}>
            Login
          </Text>
        </Text>
      </View>
    );
  };

  /**
   * Render social login buttons (Google and Facebook)
   * @returns {view} login button
   */
  renderLoginButtons = () => {
    const { refreshing } = this.state;
    const facebookLogo = require("../../assets/icons/login/facebook-logo.png");
    const googleLogo = require("../../assets/icons/login/google-logo.png");

    return (
      <View style={styles.buttonRow}>
        <TouchableOpacity style={styles.facebookBtn} onPress={this.onFacebookLogin} disabled={refreshing}>
          <Image style={styles.facebookIcon} source={facebookLogo} />
          <Text style={styles.buttonText}>Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.googleBtn} onPress={this.onGoogleLogin} disabled={refreshing}>
          <Image style={styles.googleIcon} source={googleLogo} />
          <Text style={styles.buttonText}>Google</Text>
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * Render method
   */
  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(35)}
        duration={0}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.container}>
          {this.renderHeader()}
          {this.renderLoginCard()}
        </View>
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let auth = "auth" in firebase ? firebase.auth : {};
  let googleKey = "GoogleSignIn" in auth ? auth.GoogleSignIn : null;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, googleKey, userState: state.user };
};

export default connect(
  mapStateToProps,
  { doUserLogin, loadUserDetails, doUserLogout, clearPortfolio, clearWatchlistAll, clearMarket },
  null,
  { forwardRef: true }
)(LoginModal);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: Platform.OS == "android" ? hp(10) : 0,
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  logInWith: {
    fontSize: RF(2.2),
    marginTop: wp(5),
    color: colors.steelGrey,
  },
  buttonRow: {
    flexDirection: "row",
    marginVertical: wp(5.5),
  },
  facebookBtn: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingVertical: wp(3),
    marginRight: wp(2),
  },
  googleBtn: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    paddingVertical: wp(3),
    resizeMode: "contain",
    backgroundColor: colors.tomato,
    marginLeft: wp(2),
  },
  googleIcon: {
    width: wp(5),
    height: wp(5),
  },
  facebookIcon: {
    width: wp(5.7),
    height: wp(6),
    marginTop: -wp(1.4),
    resizeMode: "contain",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    marginLeft: wp(2.2),
  },
  loginText: {
    color: colors.primary_color,
    fontWeight: "500",
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
});
