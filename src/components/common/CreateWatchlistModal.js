import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image, TextInput, Platform } from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import Modal from "../../libs/react-native-modalbox";
import { createNewWatchlist, pushPopStockFromWatchlist } from "../../controllers/WatchlistController";
import LoadingView from "./LoadingView";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";

class CreateWatchlistModal extends React.PureComponent {
  textInputRefs = {};

  constructor(props) {
    super(props);
    this.state = {
      watchlistName: "",
      watchlistDescription: "",
      textFocused: null,
      isRefreshing: false,
      isOpen: false,
      showSubscriptionDialog: false,
    };
  }

  open = () => {
    this.modal.open();
    this.setState({ showSubscriptionDialog: false });
  };

  closeDialog = () => {
    this.modal.close();
    this.props.openWatchlist();
  };

  gotoSubscription = () => {
    this.modal.close();
    this.props.navigation.navigate("Subscription");
  };

  addRemoveStock = (watchlistId) => {
    const { stock, watchlist, activeWatchlist, onUpdate, userState, baseURL, appToken } = this.props;
    const { watchlistName } = this.state;
    const stockId = stock.stock_id;

    pushPopStockFromWatchlist(baseURL, appToken, userState.token, watchlistId, stockId, "push", (isSuccess) => {
      // console.log(isSuccess);
      this.setState({ isRefreshing: false });

      if (isSuccess) {
        let newWatchlist = [...watchlist];
        newWatchlist.push([watchlistId, watchlistName]);

        let activeList = activeWatchlist[stockId] ? activeWatchlist[stockId] : [];

        activeList.push(watchlistId);
        let newActiveWatchlist = Object.assign({}, activeWatchlist);
        newActiveWatchlist[stockId] = activeList;

        onUpdate({ newWatchlist, newActiveWatchlist });
        this.closeDialog();
      } else {
        this.refs.toast.show("Please try again");
      }
    });
  };

  createWatchlist = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const title = this.state.watchlistName;
    const description = this.state.watchlistDescription;

    this.setState({ isRefreshing: true });

    createNewWatchlist(baseURL, appToken, token, title, description, (watchlistId, message, status) => {
      if (watchlistId != -1) {
        this.addRemoveStock(watchlistId);
      } else {
        this.setState({ isRefreshing: false });

        if (status == 4) this.setState({ showSubscriptionDialog: true });
        else if (message) this.refs.toast.show(message);
        // else this.refs.toast.show("Please try again");
      }
    });
  };

  setInputText = (type, value) => {
    if (type == "name") {
      if (value.length <= 60)
        this.setState({
          watchlistName: value,
          isCreateWatchlistBtnActive: value.length > 0,
        });
    } else {
      this.setState({
        watchlistDescription: value,
      });
    }
  };

  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value });
  };

  renderTextInputBox = (type, label, length, limit, mandatory) => {
    const inputBoxStyle =
      this.state.textFocused == type ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;

    return (
      <View>
        <Text style={styles.inputLabel}>
          {label}
          {mandatory && <Text style={styles.redText}> *</Text>}
        </Text>
        <View style={inputBoxStyle}>
          <TextInput
            ref={(ref) => (this.textInputRefs[type] = ref)}
            // placeholder={placeholder}
            maxLength={length}
            style={styles.textInput}
            placeholderTextColor={colors.steelGrey}
            onChangeText={(value) => this.setInputText(type, value)}
            onFocus={() => this.toggleTextInputFocus(type)}
            onBlur={() => this.toggleTextInputFocus(null)}
          />
          <Text style={styles.inputLength}>{limit}</Text>
        </View>
      </View>
    );
  };

  renderHeader = () => {
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Create New Watchlist
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  renderForm = () => {
    const { watchlistName, watchlistDescription } = this.state;

    const nameLength = 60 - watchlistName.length;
    const descriptionLength = 255 - watchlistDescription.length;

    return (
      <View>
        {this.renderTextInputBox("name", "Enter Watchlist Title", 60, nameLength, true)}
        {this.renderTextInputBox(
          "description",
          "Enter Watchlist description (optional)",
          255,
          descriptionLength,
          false
        )}
      </View>
    );
  };

  renderButton = () => {
    const { isCreateWatchlistBtnActive, watchlistName } = this.state;

    const buttonStyle = isCreateWatchlistBtnActive
      ? styles.createWatchlistBtn
      : [styles.createWatchlistBtn, styles.greyBackground];

    const buttonText = isCreateWatchlistBtnActive
      ? styles.createWatchlistBtnText
      : [styles.createWatchlistBtnText, styles.whiteText];

    const isWatchlistBtnDisabled = watchlistName.length == 0;

    return (
      <TouchableOpacity style={buttonStyle} onPress={() => this.createWatchlist()} disabled={isWatchlistBtnDisabled}>
        <Text style={buttonText}>Create & Add Stock to Watchlist</Text>
      </TouchableOpacity>
    );
  };

  renderSubscriptionDialog = () => {
    const { showSubscriptionDialog } = this.state;
    const message = "Subscribe now to create more watchlists and access investment strategies!";

    if (showSubscriptionDialog) {
      return (
        <View style={styles.subscriptionDialog}>
          <Text style={styles.subscribeMessage}>{message}</Text>

          <TouchableOpacity
            onPress={this.gotoSubscription}
            // disabled={selectBtnDisabled}
          >
            <View style={styles.plansBtn}>
              <Text style={styles.planBtnText}>See Plans</Text>
            </View>
          </TouchableOpacity>
        </View>
      );
    }
  };

  render() {
    const { isRefreshing } = this.state;

    const modalStyle = {
      borderTopLeftRadius: wp(6),
      borderTopRightRadius: wp(6),
      backgroundColor: "white",
      height: Platform.OS == "ios" ? hp(55) : hp(55),
    };

    return (
      <Modal
        style={modalStyle}
        position={"bottom"}
        ref={(ref) => (this.modal = ref)}
        swipeArea={20}
        animationDuration={0}
        onOpened={() => this.setState({ isOpen: true })}
        onClosed={() => this.setState({ isOpen: false })}
        {...this.props}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {this.renderForm()}
          {this.renderButton()}
        </View>
        {this.renderSubscriptionDialog()}
        <Toast ref="toast" position="center" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {isRefreshing && <LoadingView />}
      </Modal>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(CreateWatchlistModal);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(7),
    marginTop: hp(2),
    marginRight: wp(4),
    // paddingHorizontal: wp(4)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
    // paddingHorizontal: wp(4)
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    // height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  itemRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
  },
  itemIcon: {
    width: wp(4),
    height: wp(3),
    resizeMode: "contain",
  },
  itemName: {
    marginLeft: wp(3),
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  addWatchlistBtn: {
    position: "absolute",
    left: wp(8),
    right: wp(8),
    bottom: wp(8),
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
  },
  addWatchlistBtnDisabled: {
    backgroundColor: colors.steelGrey,
  },
  addWatchlistBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 2,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  subscriptionDialog: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary_color,
    paddingTop: wp(4),
    paddingHorizontal: wp(8),
    paddingBottom: ifIphoneX ? hp(3) : wp(4),
  },
  subscribeMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(4),
  },
  plansBtn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
    // alignItems: "center",
    // justifyContent: "center"
  },
  planBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "center",
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  inputLength: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.steelGrey,
    marginTop: wp(3.5),
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(5),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(2.5),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  inputLabel: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  redText: {
    color: colors.red,
  },
  subscriptionDialog: {
    position: "absolute",
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: colors.primary_color,
    paddingTop: wp(4),
    paddingHorizontal: wp(8),
    paddingBottom: ifIphoneX ? hp(3) : wp(4),
    zIndex: 99,
    elevation: 2,
  },
  subscribeMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(4),
  },
  plansBtn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
  },
  planBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "center",
  },
});
