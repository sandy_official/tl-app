import React from "react";
import { View, Text, TouchableOpacity, Image } from "react-native";
import { globalStyles, colors } from "../../styles/CommonStyles";
import LinearGradient from "react-native-linear-gradient";
import { widthPercentageToDP } from "react-native-responsive-screen";

export class SortOptionView extends React.PureComponent {
  /**
   * Get sort order label
   * @param {boolean} isSortAscending
   * @returns {string} sort order label
   */
  getSortLabel = (isSortAscending) => {
    const { customLabel } = this.props;
    if (customLabel) return isSortAscending ? customLabel[0] : customLabel[1];
    else return isSortAscending ? "Ascending" : "Descending";
  };

  renderSortLabel = (isSortAscending) => {
    const { sortOrder } = globalStyles;
    const sortLabel = this.getSortLabel(isSortAscending);

    return <Text style={sortOrder}>{sortLabel}</Text>;
  };

  render() {
    const { name, sortKey, selectedSort, ascending, onSelect } = this.props;
    const { sortOptionContainer, sortOptionNameSelected, sortOptionName, sortIconStyle } = globalStyles;

    const whiteIcon = require("../../assets/icons/portfolio/downArrowWhite.png");
    const blueIcon = require("../../assets/icons/portfolio/downArrowBlue.png");

    const isSelected = selectedSort === name;

    const sortStyle = isSelected
      ? [sortOptionContainer, { backgroundColor: colors.primary_color }]
      : sortOptionContainer;

    const sortTextStyle = isSelected ? sortOptionNameSelected : sortOptionName;

    const iconStyle = isSelected
      ? [sortIconStyle, { transform: [{ rotate: ascending ? "180deg" : "0deg" }] }]
      : sortIconStyle;

    const sortIcon = isSelected ? whiteIcon : blueIcon;

    let gradientStyle = { position: "absolute", top: 0, bottom: 0, left: 0, right: 0 };

    return (
      <TouchableOpacity style={sortStyle} onPress={() => onSelect(name, sortKey)}>
        {isSelected && <LinearGradient colors={[colors.cobalt, colors.lightNavy]} angle={180} style={gradientStyle} />}
        <Text style={[{ marginHorizontal: widthPercentageToDP(1.2) }, sortTextStyle]}>{name}</Text>
        <Image source={sortIcon} style={iconStyle} />
      </TouchableOpacity>
    );
  }
}
