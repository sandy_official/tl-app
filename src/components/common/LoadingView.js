import React from "react";
import { View, StyleSheet, ActivityIndicator } from "react-native";
import { colors } from "../../styles/CommonStyles";

export default class LoadingView extends React.PureComponent {
  render() {
    return (
      <View style={styles.loaderContainer}>
        <ActivityIndicator size="large" color={colors.primary_color} />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  loaderContainer: {
    flex: 1,
    position: "absolute",
    // top: hp(10),
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center"
  }
});
