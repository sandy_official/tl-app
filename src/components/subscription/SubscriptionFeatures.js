import React from "react";
import { View, Text, StyleSheet, FlatList, Image } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { connect } from "react-redux";

// const featuresList = [
//   {
//     name: "High Return Strategies",
//     description:
//       "Get access to premium investing strategies built with DVM scores. These algorithms perform to provide high returns.",
//     img: require("../../assets/icons/subscription/high-returns.png"),
//   },
//   {
//     name: "Screener Querying with 800+ parameters",
//     description:
//       "Write screeners across a large range of parameters, including delivery, broker upgrades, insider trades, financials.",
//     img: require("../../assets/icons/subscription/screeners-query.png"),
//   },
//   {
//     name: "Backtesting",
//     description:
//       "Backtests help investors find a good strategy. We are the only platform offering backtesting at both strategy and stock level.",
//     img: require("../../assets/icons/subscription/backtest.png"),
//   },
//   {
//     name: "Portfolio Analysis",
//     description:
//       "Analyze win/loss ratio, buy/sell decisions. Calculate and compare your portfolio NAV, see trends in price growth and valuation.",
//     img: require("../../assets/icons/subscription/portfolio-analysis.png"),
//   },
//   {
//     name: "Results with Real-Time Filters",
//     description:
//       'Real-time result filters let you customize result tracking. Example filter: "Net Profit Qtr > 12 cr AND Result Declared Date = Today"',
//     img: require("../../assets/icons/subscription/result-filters.png"),
//   },
//   {
//     name: "Custom Alerts",
//     description:
//       "Set different alert frequencies for investment strategies, superstar investors, and for your own and pre-built screeners.",
//     img: require("../../assets/icons/subscription/custom-alerts.png"),
//   },
//   {
//     name: "Track Superstars",
//     description:
//       "Track shareholdings of well-known investors. Get an alert when they make portfolio changes or bulk deals.",
//     img: require("../../assets/icons/subscription/track-superstar.png"),
//   },
// ];

class SubscriptionFeatures extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      indicatorPos: 0,
    };
  }

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0])
      this.setState(
        { indicatorPos: viewableItems[0].index },
        () => this.listRef && this.listRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  renderPageIndicator = () => {
    const { planImages } = this.props;
    const { indicatorPos } = this.state;

    if (Object.keys(planImages).length > 1)
      return (
        <View style={styles.pageIndicator}>
          {Object.keys(planImages).map((item, index) => {
            const indicatorStyle = indicatorPos == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
            return <View style={indicatorStyle} key={index.toString()} />;
          })}
        </View>
      );
  };

  renderFeatureItem = (imgSrc) => {
    return (
      <View style={[styles.featureItem, styles.card]}>
        <Image source={{ uri: imgSrc }} style={styles.featureImg} resizeMethod="auto" />
        {/* <Text style={styles.featureName} numberOfLines={1}>
          {item.name}
        </Text>
        <Text style={styles.featureDescription} numberOfLines={3}>
          {item.description}
        </Text> */}
      </View>
    );
  };

  render() {
    const { planImages } = this.props;

    return (
      <View>
        <View style={styles.headingRow}>
          <Text style={[styles.heading]}>INVEST LIKE A PRO</Text>
          {this.renderPageIndicator()}
        </View>

        <FlatList
          ref={(ref) => (this.listRef = ref)}
          contentContainerStyle={[styles.list]}
          data={Object.keys(planImages)}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => this.renderFeatureItem(planImages[item])}
          keyExtractor={(item, index) => item.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
        />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let planImages = "plan" in firebase ? firebase.plan : {};

  return { planImages };
};

export default connect(mapStateToProps)(SubscriptionFeatures);

const styles = StyleSheet.create({
  pageIndicator: {
    flexDirection: "row",
    // marginTop: wp(6)
  },
  pageIndicatorActive: {
    width: wp(1.8),
    height: wp(1.8),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.3),
    height: wp(1.3),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  list: {
    paddingHorizontal: wp(3),
  },
  card: {
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: { width: 0, height: 0 },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  headingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(6),
    alignItems: "center",
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  featureItem: {
    overflow: "hidden",
    // width: wp(92),
    // height: hp(51),
    marginRight: wp(2),
    marginVertical: wp(3),
    // borderRadius: 5,
  },
  featureImg: {
    flex: 1,
    width: wp(92),
    height: hp(51),
    alignSelf: "center",
    resizeMode: "stretch",
    // marginBottom: wp(5),
    // marginTop: 6,
  },
  // featureName: {
  //   fontSize: RF(2.2),
  //   fontWeight: "500",
  //   textAlign: "center",
  //   color: colors.white,
  //   paddingHorizontal: wp(3),
  //   paddingVertical: 9,
  //   backgroundColor: colors.brightBlue,
  // },
  // featureDescription: {
  //   fontSize: RF(2.1),
  //   lineHeight: wp(5.5),
  //   textAlign: "center",
  //   color: colors.greyishBrown,
  //   marginTop: 10,
  //   marginHorizontal: wp(5),
  //   marginBottom: wp(5),
  // },
});
