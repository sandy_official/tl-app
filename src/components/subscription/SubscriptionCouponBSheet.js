import React from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  TouchableWithoutFeedback,
  TextInput,
  Platform,
  FlatList,
  TouchableOpacity,
} from "react-native";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import DiscountCoupon from "./DiscountCoupon";
import Toast, { DURATION } from "react-native-easy-toast";

export default class SubscriptionCouponBSheet extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { textFocused: false, discountInput: "" };
  }

  // componentDidMount() {
  //   this.RBSheet.open();
  // }

  closeDialog = () => {
    this.RBSheet.close();
  };

  setInputText = (value) => {
    this.setState({ discountInput: value });
  };

  applyCoupon = (coupon) => {
    this.props.apply(coupon);
    this.RBSheet.close();
  };

  searchCoupon = () => {
    const { data } = this.props;
    const { discountInput } = this.state;
    let validCoupon = false;

    if (discountInput !== "") {
      data.map((coupon) => {
        if (coupon.code == discountInput.toUpperCase()) {
          this.applyCoupon(coupon);
          validCoupon = true;
        }
      });

      if (!validCoupon) {
        this.refs.toast.show("Invalid Discount Coupon", 1000);
      }
    }
  };

  renderCouponItem = ({ item, index }) => {
    const couponCode = item.code ? item.code : "";
    const couponOff = item.percentage ? "Get " + item.percentage + "% off" : "";

    return (
      <View>
        <View style={styles.couponRow}>
          <DiscountCoupon coupon={couponCode} discount={couponOff} />

          <View style={{ flex: 1 }}>
            <TouchableWithoutFeedback onPress={() => this.applyCoupon(item)}>
              <Text style={styles.applyBtn}>APPLY</Text>
            </TouchableWithoutFeedback>
          </View>
        </View>

        <Text style={styles.description}>{item.text}</Text>
      </View>
    );
  };

  renderCouponList = () => {
    const { data } = this.props;

    if (data.length > 0)
      return (
        <FlatList
          data={data}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderCouponItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.listDivider} />}
          keyboardShouldPersistTaps="always"
        />
      );
  };

  renderInputBox = () => {
    const inputBoxStyle = this.state.textFocused
      ? [styles.inputContainer, styles.activeTextBox]
      : styles.inputContainer;

    return (
      <View style={inputBoxStyle}>
        <TextInput
          style={styles.textInput}
          placeholder={"Enter Coupon Code"}
          placeholderTextColor={colors.steelGrey}
          maxLength={12}
          autoCapitalize="characters"
          onChangeText={this.setInputText}
          onFocus={() => this.setState({ textFocused: true })}
          onBlur={() => this.setState({ textFocused: false })}
        />

        <TouchableWithoutFeedback onPress={() => this.searchCoupon()}>
          <Text style={[styles.applyBtn, { marginBottom: 3 }]}>APPLY</Text>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  renderTitle = () => {
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Apply Discount
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        closeOnDragDown={false}
        height={hp(60)}
        duration={300}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderTitle()}
          {this.renderInputBox()}

          <View style={styles.dividerContainer}>
            <View style={styles.divider} />
            <Text style={styles.orText}>OR</Text>
            <View style={styles.divider} />
          </View>

          {this.renderCouponList()}

          <View style={styles.extraSpace} />

          <Toast ref="toast" position="center" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        </View>
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  extraSpace: {
    paddingBottom: Platform.OS == "ios" ? hp(3) : 0,
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(2.5) : 0,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  applyBtn: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
    alignSelf: "flex-end",
  },
  description: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginBottom: wp(3.2),
  },
  couponRow: {
    flexDirection: "row",
    marginVertical: wp(3.2),
    alignItems: "center",
  },
  listDivider: {
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  dividerContainer: {
    flexDirection: "row",
    alignItems: "center",
    marginHorizontal: -wp(8),
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  orText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
    padding: wp(3),
  },
});
