import React from "react";
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  ScrollView
} from "react-native";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";

export default class DiscountCoupon extends React.PureComponent {
  render() {
    const { coupon, discount } = this.props;

    return (
      <View style={styles.couponItem}>
        <View style={{ backgroundColor: "rgba(36, 83, 161, 0.05)" }}>
          <Text style={styles.coupon}>{coupon}</Text>
        </View>
        <Image
          source={require("../../assets/icons/subscription/dash-line-blue.png")}
          style={styles.couponDivider}
        />
        <Text style={styles.priceOff}>{discount}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  couponItem: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 1.2,
    borderColor: colors.primary_color
  },
  discountRow: {
    flexDirection: "row",
    marginTop: wp(4.2),
    marginBottom: wp(2.4)
  },
  boldText: {
    fontWeight: "500"
  },
  coupon: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.primary_color,
    // backgroundColor: "rgba(36, 83, 161, 0.05)",
    paddingHorizontal: 15.6,
    paddingVertical: 6.6
    // borderRightWidth: 1,
    // borderRightColor: colors.primary_color,
    // borderStyle: "dotted",
    // borderRadius: 1
  },
  priceOff: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.black,
    paddingHorizontal: 15.6,
    paddingVertical: 6.6
  },
  couponDivider: {
    resizeMode: "contain",
    maxHeight: wp(7),
    marginLeft: -3
  }
});
