import React from "react";
import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";

export default class SubscriptionPayInfoBSheet extends React.PureComponent {
  closeDialog = () => {
    this.RBSheet.close();
  };

  renderHeader = () => {
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
          Price lock and auto renew
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        closeOnDragDown={false}
        height={hp(30)}
        duration={300}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}

          <ScrollView showsVerticalScrollIndicator={false}>
            <Text style={styles.infoDetails}>
              Price lock protects you from paying increased subscription amount in the future.
            </Text>

            <View style={styles.extraSpace} />
          </ScrollView>
        </View>
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2.2),
    color: colors.black,
    paddingBottom: hp(2),
  },
  extraSpace: {
    paddingBottom: ifIphoneX ? hp(2) : 8,
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(4),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
    margin: wp(2),
  },
});
