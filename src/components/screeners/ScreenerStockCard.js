import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import {
  getDVMBackgroundColor,
  getFormattedDate,
  getFormattedDateMmDdYyyy,
  formatNumberToInr,
  formatNumber,
  roundNumber,
} from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import StockActionsView from "../common/StockActionsView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class ScreenerStockCard extends React.PureComponent {
  constructor(props) {
    super(props);
    const openCard = props.position == 0 ? true : false;
    this.state = { expand: openCard, alertActive: false, portfolioActive: false };
  }

  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    const stockCode = stock.obj.NSEcode ? stock.obj.NSEcode : stock.obj.BSEcode;

    navigation.push("Stocks", {
      stockId: stock.obj.stock_id,
      stockName: stock.obj.full_name || stock.obj.get_full_name,
      pageType: stock.obj.page_type,
      stockCode: stockCode,
    });
  };

  renderDVM = () => {
    const { stock } = this.props;
    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.obj.d_color)} />
        <View style={getDVMBackgroundColor(stock.obj.v_color)} />
        <View style={getDVMBackgroundColor(stock.obj.m_color)} />
      </View>
    );
  };

  formatStockItemValue = (item) => {
    let value = item.value + "";
    if (this.props.appliedPageName != "transactions") if (value.indexOf(".") != -1) value = roundNumber(value);
    // unique_name: "today_changes__sma_30_bel";
    // type: "boolean";
    // name: "price crossed below SMA30 today";
    // unit: " ";

    // if (item.unit != "" && item.unit != " " && item.unit != undefined) {
    // if (item.unit) {
    // console.log("If part  :" + JSON.stringify(item) + " unit :" + item.unit);

    switch (item.unit) {
      case "%":
        value = value != "" ? value + "%" : "-";
        break;

      // case "boolean":
      //   value = value ? "True" : "False";
      //   break;

      default:
        value = value ? value : " - ";
    }
    // } else {
    //   switch (item.type) {
    //     // case Boolean:
    //     case "boolean":
    //       value = value ? "True" : "False";
    //       break;

    //     default:
    //       value = value ? value : " - ";
    //   }
    // }
    //  console.log('Sort Key Item --->',item)
    if (item.unique_name == "lasteodrun" || item.unique_name == "addedDate") value = getFormattedDate(value);
    else if (item.unique_name == "qtr_end_date") value = getFormattedDateMmDdYyyy(value);
    else if (item.unique_name == "cagr") {
      let tmp_val = value;
      if (tmp_val != "null") {
        value = `${roundNumber(parseFloat(value))}%`;
        // value = `${parseFloat(value).toFixed(2)}%`;
      } else {
        value = "-";
      }
      // } else if (item.unique_name == "qty") value = roundNumber(parseFloat(value));
      // } else if (item.unique_name == "qty") value = parseFloat(value).toFixed(2);
    } else if (item.unique_name == "qty") value = parseInt(value);
    else if (
      item.unique_name == "openprice" ||
      item.unique_name == "closeprice" ||
      item.unique_name == "sold_value" ||
      item.unique_name == "total_invested"
    ) {
      value = `₹${formatNumberToInr(value, true)}`;
    }
    return value;
  };

  renderValue = (textStyle, item) => {
    let formattedValue = this.formatStockItemValue(item);
    // console.log("Value Formetted->", formattedValue);
    return (
      <Text style={[textStyle, this.changeColor(formattedValue, item.name)]} numberOfLines={1} ellipsizeMode="tail">
        {formattedValue}
      </Text>
    );
  };

  changeColor = (value, entity) => {
    const { appliedPageName } = this.props;
    let styleObj = {};
    if (appliedPageName && appliedPageName === "transactions") {
      if (typeof value === "number") {
        if (value && parseInt(value) < 0) {
          styleObj = { color: colors.red };
        } else if (value != "-" && value.toString().indexOf("-") != -1) {
          styleObj = { color: colors.red };
        }
      } else if (typeof value === "string") {
        if (value && value.indexOf("-") != -1 && value.indexOf("₹") != -1) {
          styleObj = { color: colors.red };
        }
      }
    } else if (appliedPageName && typeof(appliedPageName) === 'string' && appliedPageName.includes('watchlist')) {
      // console.log("styleObj", value, entity);
      if (entity === "Added on" && appliedPageName === 'watchlist_0') {
        styleObj = { color: colors.white };
      } else if (entity != "Added on" && entity != "LTP") {
        if (value && parseInt(value) < 0) {
          styleObj = { color: colors.red };
        } else if (value && parseInt(value) > 0) {
          styleObj = { color: colors.green };
        } else if (value && typeof value == "string" && value.indexOf("-") != -1 && value.indexOf("%") != -1) {
          styleObj = { color: colors.red };
        } else if (value && typeof value == "string" && value.indexOf("%") != -1) {
          styleObj = { color: colors.green };
        } else {
          styleObj = {};
        }
      }
    }
    return styleObj;
  };

  formatStockLabelValue = (givenLabel) => {
    const { appliedPageName } = this.props;
    let customLabel;
    if (appliedPageName && appliedPageName === "transactions") {
      switch (givenLabel) {
        case "Quantity":
          customLabel = "No. of Shares";
          return customLabel;
        case "Sold Value":
          customLabel = "Value";
          return customLabel;
        case "Total Invested":
          customLabel = "Investment Amt.";
          return customLabel;
        default:
          return givenLabel;
      }
    } else if (appliedPageName && appliedPageName === "watchlist") {
      switch (givenLabel) {
        case "Change% Since Added":
          customLabel = "Change since added";
          return customLabel;
        case "Day Change %":
          customLabel = "1 Day %";
          return customLabel;
        case "Week Change %":
          customLabel = "1 Week %";
          return customLabel;
        case "Month Change %":
          customLabel = "1 Month %";
          return customLabel;
        case "Qtr Change %":
          customLabel = "3 Month %";
          return customLabel;
        case "Half Yr Change %":
          customLabel = "6 Month %";
          return customLabel;
        case "Year Change %":
          customLabel = "1 Year %";
          return customLabel;
        default:
          return givenLabel;
      }
    } else {
      return givenLabel;
    }
  };

  renderLabel = (textStyle, label) => {
    let formattedValue = this.formatStockLabelValue(label);
    return (
      <Text style={[textStyle, this.changeLabelColor(formattedValue, label)]} numberOfLines={2} ellipsizeMode="tail">
        {formattedValue}
      </Text>
    );
  };

  changeLabelColor = (value, entity) => {
    const { appliedPageName } = this.props;
    let styleObj = {};
    if (appliedPageName && appliedPageName === "watchlist_0") {
      if (entity === "Added on") {
        styleObj = { color: colors.white };
      } else {
        styleObj = {};
      }
    } else {
      styleObj = {};
    }
    return styleObj;
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderListItem = ({ item, index }) => {
    let positionStyle = this.getItemPositionStyle(index);

    return (
      <View style={styles.detailRow}>
        {this.renderValue([styles.value, positionStyle], item)}
        {this.renderLabel([styles.label, positionStyle], item.name)}
      </View>
    );
  };

  renderButtonRow = () => {
    const { alertBtnActive, portfolioActive } = this.state;
    const { watchlistBtnState } = this.props;

    return (
      <StockActionsView
        {...this.props}
        alertActive={alertBtnActive}
        portfolioActive={portfolioActive}
        watchlistBtnState={watchlistBtnState}
      />
    );
  };

  adjustArraySize = (stock) => {
    let obj = { name: " ", value: " " };
    if (stock.length % 3 == 1) {
      stock.push(obj);
      stock.push(obj);
      return stock;
    } else if (stock.length % 3 == 2) {
      stock.push(obj);
      return stock;
    } else return stock;
  };

  renderStockName = () => {
    const { stock, relatedPortfolio, preventOverView } = this.props;
    const stockCode = stock.obj.NSEcode ? stock.obj.NSEcode : stock.obj.BSEcode;
    const amount = stock.obj.addedPrice ? roundNumber(stock.obj.addedPrice) : "";
    const addedOnDate = stock.obj.addedDate ? APIDataFormatter("addedDate", stock.obj.addedDate) : "";
    const portfolioName = stock.obj.portfolio;
    let showAddedOn = addedOnDate ? true : false;

    return (
      <View>
        <View style={[styles.StockCodeRow]} onPress={preventOverView ? null : this.gotoStockDetails}>
          <TouchableOpacity style={styles.StockCodeContainer} onPress={preventOverView ? null : this.gotoStockDetails}>
            <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
              {stockCode}
            </Text>
            {this.props.amountStatusWithDate ? (
              <Text style={(styles.stockCode, { color: colors.greyishBrown })} numberOfLines={1} ellipsizeMode="tail">
                {addedOnDate && amount ? `Added @ ₹ ${amount} on ${addedOnDate}` : ""}
              </Text>
            ) : null}
          </TouchableOpacity>
          {this.renderDVM()}
        </View>
        <View>
          {relatedPortfolio ? (
            <Text style={[styles.value, { fontWeight: "300" }]} numberOfLines={1} ellipsizeMode="tail">
              {portfolioName}
            </Text>
          ) : null}
        </View>
      </View>
    );
  };

  renderValueOf = (realizedGain, realizedPer) => {
    // if (realizedGain < 0) {
    //   return `-₹${formatNumberToInr(realizedGain)}(${(formatNumber(realizedPer))} %)`
    // } else {
    return `₹${formatNumberToInr(realizedGain, true)} (${roundNumber(parseFloat(formatNumber(realizedPer)))} %)`;
    // }
  };

  modifyEntities = (givenStock) => {
    let modifiedStock = [];
    modifiedStock = givenStock;
    let tempArr = [];
    let realizedGain = givenStock[givenStock.findIndex((ele) => ele.unique_name === "realized_gain")].value;
    let realizedPer = givenStock[givenStock.findIndex((ele) => ele.unique_name === "realized_gainP")].value;
    let realized_gain_obj = {
      unique_name: "realized_gain_obj",
      type: "string",
      name: "Realized Gain",
      value: this.renderValueOf(realizedGain, realizedPer),
    };
    modifiedStock.forEach((ele, ind) => {
      if (
        ele.unique_name === "qty" ||
        ele.unique_name === "open_date" ||
        ele.unique_name === "close_date" ||
        ele.unique_name === "cagr" ||
        ele.unique_name === "openprice" ||
        ele.unique_name === "closeprice" ||
        ele.unique_name === "total_invested" ||
        ele.unique_name === "realized_gain" ||
        ele.unique_name === "sold_value"
      ) {
        ele.unique_name == "realized_gain" ? tempArr.push(realized_gain_obj) : tempArr.push(ele);
      }
    });
    return tempArr;
  };

  render() {
    const { stock, appliedPageName } = this.props;
    let listData;
    if (appliedPageName && appliedPageName === "transactions") {
      const newStock = this.modifyEntities(stock.array);
      listData = this.state.expand
        ? this.adjustArraySize(newStock.slice(0))
        : this.adjustArraySize(newStock.slice(0, 3));
    } else {
      listData = this.state.expand
        ? this.adjustArraySize(stock.array.slice(0))
        : this.adjustArraySize(stock.array.slice(0, 3));
    }
    // console.log("List Data -->",stock.obj)
    const stockName = stock.obj.full_name;
    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
          {this.renderStockName()}
          <Text style={styles.stockFullName} numberOfLines={1} ellipsizeMode="tail">
            {stockName}
          </Text>
          <FlatList
            showsHorizontalScrollIndicator={false}
            data={listData}
            renderItem={this.renderListItem}
            keyExtractor={(item, index) => index.toString()}
            numColumns={3}
            extraData={this.state.expand}
          />

          {this.state.expand ? (this.props.children ? this.props.children : this.renderButtonRow()) : null}
          {/* {this.state.expand ? this.props.child ? this.props.child : this.renderButtonRow() : null} */}
          {/* {this.state.expand && this.renderButtonRow()} */}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: "2%",
    marginHorizontal: "3%",
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeRow: {
    flexDirection: "row",
  },
  StockCodeContainer: {
    flex: 1,
    paddingBottom: wp(1),
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "500",
    marginRight: wp(4),
  },
  detailRow: {
    flex: 1,
    marginTop: wp(3),
    marginBottom: wp(2),
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 6,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(1.9),
    fontWeight: "500",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.6),
    color: colors.cement,
  },
  hide: {
    display: "none",
  },
  stockFullName: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginBottom: wp(1),
  },
});
