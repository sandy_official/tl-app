import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, TouchableWithoutFeedback, Platform } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { TAB_BAR_HEIGHT, colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ScrollView } from "react-native-gesture-handler";
import {
  FUNDAMENTAL_OPTIONS,
  TECHNICAL_OPTIONS,
  SHARE_HOLDING_OPTIONS,
  BROKER_REPORT_OPTIONS,
} from "../../utils/ScreenerConstants";

export default class ScreenerFilters extends React.PureComponent {
  componentDidUpdate(oldProps) {
    const { currentFilterId, currentFilter } = this.props;

    if (oldProps.currentFilterId != currentFilterId) {
      this.setState({ selectedOption: currentFilter, filterId: currentFilterId });
    }
  }

  constructor(props) {
    super(props);
    this.state = { selectedDropdown: "", selectedOption: "", filterId: 0 };
  }

  setSelectedOption = (option) => {
    this.setState({ selectedOption: option.label, filterId: option.id });
    this.props.setFilter(option.id, option.label);
    // this.props.setFilterId(option.id)
  };

  selectDropdown = (label) => {
    if (this.state.selectedDropdown == label) this.setState({ selectedDropdown: "" });
    else this.setState({ selectedDropdown: label });
  };

  resetFilters = () => {
    this.setState({ selectedDropdown: "", selectedOption: "", filterId: 0 });
    this.props.setFilter(0, "");
  };

  renderFilterOption = (option) => {
    const { isSubscriber } = this.props;
    const { selectedOption } = this.state;

    let isPremium = "premium" in option && option.premium;
    const radioContainerStyle =
      selectedOption === option.label ? [styles.radioButton, styles.radioButtonActive] : styles.radioButton;
    const radioStyle = selectedOption === option.label ? styles.radioButtonCircle : styles.hide;
    let optionTextStyle = isPremium
      ? isSubscriber
        ? styles.dropdownOption
        : [styles.dropdownOption, { color: colors.steelGrey }]
      : styles.dropdownOption;

    return (
      <View key={option.id}>
        <TouchableOpacity onPress={() => this.setSelectedOption(option)} disabled={isPremium && !isSubscriber}>
          <View style={styles.dropdownOptionContainer}>
            <View style={radioContainerStyle}>
              <View style={radioStyle} />
            </View>
            <Text style={optionTextStyle}>{option.label}</Text>
            {isPremium && (
              <Image source={require("../../assets/icons/premium-icon-blue.png")} style={styles.premiumIcon} />
            )}
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderDropdown = (label, optionList) => {
    let showOptions = this.state.selectedDropdown == label;
    const dropdownIcon =
      this.state.selectedDropdown == label
        ? require("../../assets/icons/arrow-up-grey.png")
        : require("../../assets/icons/arrow-down-grey.png");

    return (
      <View style={styles.dropdownContainer}>
        <TouchableOpacity onPress={() => this.selectDropdown(label)}>
          <View style={styles.dropdown}>
            <Text style={styles.dropdownLabel}>{label}</Text>
            <Image source={dropdownIcon} style={styles.dropdownIcon} />
          </View>
        </TouchableOpacity>

        {showOptions &&
          optionList.map((option) => {
            return this.renderFilterOption(option);
          })}
      </View>
    );
  };

  renderFilters = () => {
    const { setFilter } = this.props;

    return (
      <View style={styles.filterView}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.filterHeadingRow}>
            <Text style={styles.filterHeading}>Change Columns</Text>

            <TouchableOpacity style={styles.resetButton} onPress={() => this.resetFilters()}>
              <Image source={require("../../assets/icons/refresh-blue.png")} style={styles.resetIcon} />
              <Text style={styles.resetText}>Reset</Text>
            </TouchableOpacity>
          </View>

          {this.renderDropdown("Fundamentals", FUNDAMENTAL_OPTIONS)}
          {this.renderDropdown("Technical", TECHNICAL_OPTIONS)}
          {this.renderDropdown("Share Holding", SHARE_HOLDING_OPTIONS)}
          {this.renderDropdown("Broker Report", BROKER_REPORT_OPTIONS)}

          <View style={styles.extraSpace} />
        </ScrollView>

        {/* <TouchableWithoutFeedback
          onPress={() =>
            setFilter(this.state.filterId, this.state.selectedOption)
          }
        >
          <View style={styles.filterApplyButton}>
            <Text style={styles.filterApplyText}>Apply</Text>
          </View>
        </TouchableWithoutFeedback> */}
      </View>
    );
  };

  render() {
    const { showFilter, toggle } = this.props;

    return (
      <View style={showFilter ? styles.filterContainer : styles.hide}>
        <TouchableWithoutFeedback onPress={() => toggle()}>
          <View style={styles.filterOverlay} />
        </TouchableWithoutFeedback>
        {this.renderFilters()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  filterContainer: {
    position: "absolute",
    flexDirection: "row",
    right: 0,
    left: 0,
    bottom: 0,
    top: Platform.OS == "ios" ? TAB_BAR_HEIGHT : TAB_BAR_HEIGHT + wp(2),
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  filterView: {
    flex: 2,
    backgroundColor: colors.white,
    paddingHorizontal: wp(3),
    paddingTop: hp(2.5),
  },
  filterOverlay: {
    flex: 1,
    backgroundColor: colors.black15,
  },
  filterApplyButton: {
    position: "absolute",
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    zIndex: 10,
    // paddingBottom: ifIphoneX ? hp(2) : 0
    // marginVertical: ifIphoneX ? hp(1) : 0,
  },
  filterApplyText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
    padding: wp(3),
    marginBottom: ifIphoneX ? wp(3) : 0,
  },
  filterHeadingRow: {
    flex: 1,
    flexDirection: "row",
    paddingTop: hp(1),
    paddingBottom: hp(2.2),
    borderBottomWidth: 0.3,
    borderBottomColor: colors.cement,
    marginBottom: hp(2.8),
    alignItems: "center",
  },
  filterHeading: {
    flex: 1,
    fontSize: RF(2.5),
    fontWeight: "500",
    paddingRight: wp(2),
  },
  resetButton: {
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
    padding: wp(1),
  },
  resetIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  resetText: {
    fontSize: RF(1.8),
    marginLeft: wp(1.5),
    color: colors.primary_color,
    fontWeight: "500",
  },
  dropdownContainer: {
    // flex:1
  },
  dropdown: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    paddingTop: hp(1),
    paddingBottom: hp(2),
  },
  dropdownIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  dropdownLabel: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.steelGrey,
    marginRight: wp(3),
  },
  dropdownOptionContainer: {
    flexDirection: "row",
    paddingLeft: wp(6),
    paddingVertical: wp(2),
    // justifyContent: "center"
    alignItems: "center",
  },
  dropdownOption: {
    fontSize: RF(1.8),
    color: colors.black,
    opacity: 0.8,
    marginLeft: wp(2),
    marginTop: -2,
  },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  radioButtonActive: {
    borderColor: colors.primary_color,
  },
  radioButtonCircle: {
    width: wp(2.4),
    height: wp(2.4),
    borderRadius: wp(1.2),
    backgroundColor: colors.primary_color,
  },
  hide: {
    display: "none",
  },
  extraSpace: {
    marginBottom: ifIphoneX ? hp(8) : hp(6),
  },
  premiumIcon: {
    width: wp(3.8),
    height: wp(3.8),
    resizeMode: "contain",
    marginLeft: wp(2),
    marginBottom: 2,
  },
});
