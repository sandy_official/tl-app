import React from "react";
import { View, StyleSheet, Text, ScrollView, TouchableOpacity, FlatList, Image } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";
import Toast from "react-native-easy-toast";
import globalStyles from "react-native-raw-bottom-sheet/src/style";
import { setStockAlert } from "../../controllers/StockAlertsController";
import { ICON_CDN } from "../../utils/Constants";
import { modifyScreenerAlert } from "../../controllers/ScreenerController";
import LoadingView from "../common/LoadingView";
import { connect } from "react-redux";

class ScreenerAlerts extends React.PureComponent {
  // componentDidMount() {
  //   this.RBSheet.open();
  // }

  componentDidUpdate(prevProps, prevState, snapshot) {
    let oldScreenId = Object.keys(prevProps.alertData).length > 0 ? prevProps.alertData.screenId : -1;
    let newScreenId = Object.keys(this.props.alertData).length > 0 ? this.props.alertData.screenId : -1;

    if (newScreenId != oldScreenId) {
      console.log("received update", this.props.alertData.currentFreq);
      this.setState({ alertOption: this.props.alertData.currentFreq });
    }
  }

  constructor(props) {
    super(props);
    this.state = {
      alertOption: null,
      refreshing: false,
    };
  }

  // addStockAlert = (buttonState, alertType, alertPeriod, threshold) => {
  //   const { stock } = this.props;
  //   // console.log("stock id : " + stock.stock_id);
  //   // console.log("alert type: " + alertType);
  //   // console.log("alert period: " + alertPeriod);
  //   // console.log("threshold: " + threshold);

  //   const action = buttonState ? "add" : "delete";
  //   // console.log("action : " + action)

  //   setStockAlert(
  //     action,
  //     stock.stock_id,
  //     alertType,
  //     alertPeriod,
  //     threshold,
  //     message => console.log(message)
  //   );
  // };

  closeDialog = () => {
    this.RBSheet.close();
  };

  showToast = (buttonState) => {
    if (buttonState) this.refs.toast.show("Alert Added");
    else this.refs.toast.show("Alert Removed");
  };

  setAlert = (name) => {
    const { alertData, onChange, userState, baseURL, appToken } = this.props;
    const { alertOption } = this.state;

    const data = Object.assign({}, alertData);
    const screenerId = alertData.screenId ? alertData.screenId : "";
    const token = userState.token;

    // console.log("Screener Id", alertData);

    if (screenerId) {
      this.setState({ refreshing: true });
      const isAlertActive = alertOption == name || alertData.currentFreq == name.toLowerCase();

      if (!isAlertActive) {
        modifyScreenerAlert(baseURL, appToken, token, screenerId, name.toLowerCase(), (res) => {
          if (res) {
            this.setState({ alertOption: name });
            data.currentFreq = name;
            onChange(data);
          }
          this.setState({ refreshing: false });
          console.log("Screener alert add response", res);
        });
      } else {
        modifyScreenerAlert(baseURL, appToken, token, screenerId, null, (res) => {
          if (res) {
            this.setState({ alertOption: "" });

            data.currentFreq = "";
            onChange(data);
          }
          this.setState({ refreshing: false });
          console.log("Screener alert remove response", res);
        });
      }
    }
  };

  getButtonStyle = (name, isButtonActive) => {
    const { alertOption } = this.state;
    let buttonStyle = [];
    let buttonTextStyle = [];

    const currentSelection = alertOption ? alertOption.toLowerCase() : "";

    if (isButtonActive) {
      if (currentSelection == name.toLowerCase()) {
        buttonStyle = [styles.optionButton, styles.activeButton];
        buttonTextStyle = [styles.optionButtonText, styles.activeButtonText];
        // return {button:buttonStyle,text:buttonTextStyle}
      } else {
        buttonStyle = [styles.optionButton];
        buttonTextStyle = [styles.optionButtonText];
      }
    } else {
      buttonStyle = [styles.optionButtonDisabled];
      buttonTextStyle = [styles.optionButtonDisableText];
    }

    return { button: buttonStyle, text: buttonTextStyle };
  };

  renderOptionButton = (name, isButtonActive) => {
    const buttonStyle = this.getButtonStyle(name, isButtonActive);

    return (
      // <View style={{ flex: 1 }}>
      <TouchableOpacity onPress={() => this.setAlert(name)} disabled={!isButtonActive}>
        <View style={buttonStyle.button}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={buttonStyle.text}>
            {name}
          </Text>
        </View>
      </TouchableOpacity>
      // </View>
    );
  };

  renderAlerts = () => {
    // const alertList = [ “yearly”, ”half yearly”,  “quarterly”, “monthly”, “weekly”, “daily”, “hourly”, “30 min”, “15 min”]
    const { name, alertData, allowed, stock } = this.props;

    if (this.state.alertOption == null)
      if ("currentFreq" in alertData && alertData.currentFreq) {
        const currentFreq = alertData.currentFreq.replace(/^\w/, (c) => c.toUpperCase());
        console.log("render alerts", alertData.currentFreq);
        this.setState({ alertOption: currentFreq });
      }

    const recommendedFreq = "recommendedFreq" in alertData ? alertData.recommendedFreq : "";

    return (
      <View>
        {recommendedFreq ? (
          <Text style={styles.recommended}>{"Recommended: " + recommendedFreq}</Text>
        ) : (
          <View style={{ marginTop: wp(5) }} />
        )}

        <View style={[styles.optionRow]}>
          {this.renderOptionButton("Quarterly", allowed.includes("quarterly"))}
          {this.renderOptionButton("Monthly", allowed.includes("monthly"))}
          {this.renderOptionButton("Weekly", allowed.includes("weekly"))}
        </View>

        <View style={[styles.optionRow]}>
          {this.renderOptionButton("Daily", allowed.includes("daily"))}
          {this.renderOptionButton("Hourly", allowed.includes("hourly"))}
          {this.renderOptionButton("30 Min", allowed.includes("30 min"))}
        </View>

        <View style={[styles.optionRow]}>{this.renderOptionButton("15 Min", allowed.includes("15 min"))}</View>
      </View>
    );
  };

  render() {
    const { name, alertData, allowed, stock } = this.props;
    const { refreshing } = this.state;

    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(45)}
        duration={300}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.row}>
              <Text style={styles.title} numberOfLines={3} ellipsizeMode="tail">
                Set up alerts for
                {" " + name}
              </Text>

              <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
                <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
              </TouchableOpacity>
            </View>

            {this.renderAlerts()}
          </ScrollView>

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {refreshing && <LoadingView />}
        </View>
      </RBSheet>
    );
  }
}

mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(ScreenerAlerts);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10)
  },
  title: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.black,
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginRight: wp(3),
    // marginBottom: wp(4)
    // paddingHorizontal: wp(4)
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    // height: hp(5), //8,
    flexDirection: "row",
    flexWrap: "wrap",
    alignItems: "flex-start",
    // justifyContent: "center",
    marginTop: hp(2),
    // marginBottom: wp(2.5),
  },
  recommended: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.lightNavy,
    marginTop: wp(5),
    marginBottom: wp(1.8),
  },
  optionRow: {
    flexDirection: "row",
    marginVertical: wp(2),
    alignItems: "center",
  },
  optionButton: {
    flex: 1,
    width: wp(25),
    marginRight: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  optionButtonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    padding: wp(1.5),
  },
  optionButtonDisabled: {
    flex: 1,
    width: wp(25),
    marginRight: wp(3),
    borderRadius: 5,
    // borderWidth: 1,
    // borderColor: colors.greyishBrown,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.steelGrey,
    opacity: 0.5,
  },
  optionButtonDisableText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: wp(1.5),
  },
  alertType: {
    width: wp(19),
    fontSize: RF(2.2),
    color: colors.greyishBrown,
  },
  smaRow: {
    flexDirection: "row",
    // flexWrap: 'wrap'
  },
  reportBtn: {
    marginTop: wp(3),
    width: wp(55),
  },
  activeButton: {
    backgroundColor: colors.primary_color,
  },
  activeButtonText: {
    color: colors.white,
  },
});
