import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, TextInput } from "react-native";
import {
  getDVMBackgroundColor,
  getFormattedDate,
  getFormattedDateMmDdYyyy,
  formatNumber,
  formatNumberToInr,
  roundNumber,
} from "../../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { CHIPS } from "../watchlist";

const iconActive = require("../../../assets/icons/tick-white-blue.png");
const iconInactive = require("../../../assets/icons/add-blue.png");
const emptyHoldings = require("../../../assets/icons/portfolio/holdingsIllustration.png");

export default class HoldingsCard extends React.PureComponent {
  constructor(props) {
    super(props);
    const openCard = this.props.position == 0 ? true : false;
    this.state = {
      expand: openCard,
      alertActive: false,
      portfolioActive: false,
    };
  }

  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    // console.log("&&&&&&&&-->", stock.obj);
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;

    navigation.push("Stocks", {
      stockId: stock.obj.stock_id,
      stockName: stock.obj.full_name ? stock.obj.full_name : stock.obj.shortname,
      pageType: stock.page_type,
      stockCode: stockCode,
    });
  };

  renderDVM = () => {
    const { stock } = this.props;
    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.obj.d_color)} />
        <View style={getDVMBackgroundColor(stock.obj.v_color)} />
        <View style={getDVMBackgroundColor(stock.obj.m_color)} />
      </View>
    );
  };

  formatStockItemValue = (item) => {
    // console.log('Item--->',item)
    let value = item.value + "";
    // unique_name: "today_changes__sma_30_bel";
    // type: "boolean";
    // name: "price crossed below SMA30 today";
    // unit: " ";

    // if (item.unit != "" && item.unit != " " && item.unit != undefined) {
    // if (item.unit) {
    // console.log("If part  type:" + item.type + " unit :" + item.unit);
    switch (item.unit) {
      case "%":
        value = value + "%";
        break;

      // case "boolean":
      //   value = value ? "True" : "False";
      //   break;

      default:
        value = value ? value : " - ";
    }
    // } else {
    //   switch (item.type) {
    //     // case Boolean:
    //     case "boolean":
    //       value = value ? "True" : "False";
    //       break;

    //     default:
    //       value = value ? value : " - ";
    //   }
    // }

    if (item.unique_name == "lasteodrun") value = getFormattedDate(value);
    else if (item.unique_name == "qtr_end_date") value = getFormattedDateMmDdYyyy(value);
    else if (
      item.unique_name == "current_value" ||
      item.unique_name == "total_invested" ||
      item.unique_name == "unrealized_gain"
    )
      value = formatNumberToInr(value, true);
    else if (item.unique_name == "qty") value = formatNumberToInr(value, true);
    return value;
  };

  renderValue = (textStyle, item) => {
    // console.log('-->Render Value',item)
    let formattedValue = this.formatStockItemValue(item);
    // console.log('Formetted value-->',formattedValue)
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {`${this.addSymbol(item.name)} ${formattedValue}`}
      </Text>
    );
  };

  addSymbol = (entityName) => {
    let rupeeSym = "₹";
    if (entityName === "Unrealized Gain" || entityName === "Total Invested" || entityName === "Current Value")
      return rupeeSym;
    else return "";
  };

  // conditionalLabel=(label)=>{
  //  console.log()
  //  let mylabel = label;
  //  switch(label){
  //    case '':
  //    break;

  //    case '':
  //    break;

  //    default :label
  //  }
  //   return mylabel;
  // }

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderListItem = ({ item, index }) => {
    // console.log("Card Items -->",item)
    let positionStyle = this.getItemPositionStyle(index);

    return (
      <View style={styles.detailRow}>
        {this.renderValue([styles.value, positionStyle, this.highlightNegatives(item)], item)}
        {this.renderLabel([styles.label, positionStyle], item.name)}
      </View>
    );
  };

  highlightNegatives = (entity) => {
    const { highlightNegatives } = this.props;
    let returningColor = {};
    if (highlightNegatives) {
      if (entity.name === "Unrealized Gain" && parseInt(this.formatStockItemValue(entity)) < 0) {
        returningColor = { color: colors.red };
        return returningColor;
      } else if (entity.name === "Unrealized Gain" && parseInt(this.formatStockItemValue(entity)) > 0) {
        returningColor = { color: colors.green };
        return returningColor;
      } else {
        return returningColor;
      }
    } else {
      return returningColor;
    }
  };

  renderButtonRow = () => {
    const { alertBtnActive, portfolioActive } = this.state;
    const { watchlistBtnState } = this.props;

    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingTop: wp(3),
          //  backgroundColor:'cyan'
        }}
      >
        <CardButton ButtonName="- Sell FIFO" disableImg onPress={this.props.firstButtonClick} {...this.props} />
        <CardButton
          ButtonName="Portfolio"
          onPress={this.props.secondButtonClick}
          buttonMainView={this.props.portfolioButtonMainView}
          buttonText={this.props.portfolioButtonText}
          iconSource={this.props.portfolioIconSource}
          {...this.props}
        />
        <CardButton ButtonName="View All" disableImg onPress={this.props.thirdButtonClick} {...this.props} />
      </View>
      // <StockActionsView
      //   {...this.props}
      //   alertActive={alertBtnActive}
      //   portfolioActive={portfolioActive}
      //   watchlistBtnState={watchlistBtnState}
      // />
    );
  };

  adjustArraySize = (stock) => {
    let obj = { name: " ", value: " " };
    if (stock.length % 3 == 1) {
      stock.push(obj);
      stock.push(obj);
      return stock;
    } else if (stock.length % 3 == 2) {
      stock.push(obj);
      return stock;
    } else return stock;
  };

  generateList = (stocks, type) => {
    const { disableDVM } = this.props;
    let tempArr = [];
    if (disableDVM) {
      let unrealizedGain = stocks[stocks.findIndex((ele) => ele.unique_name === "unrealized_gain")].value;
      let unrealizedPer = stocks[stocks.findIndex((ele) => ele.unique_name === "unrealized_gainP")].value;
      let unrealized_gain_obj = {
        unique_name: "unrealized_gain_obj",
        type: "string",
        name: "Unrealized Gain",
        value: `${formatNumberToInr(unrealizedGain, true)}(${roundNumber(unrealizedPer)} %)`,
      };
      stocks.forEach((ele, ind) => {
        if (ele.unique_name === "open_date") {
          tempArr.push(ele);
        }

        if (
          ele.unique_name === "open_date" ||
          ele.unique_name === "qty" ||
          ele.unique_name === "lastprice" ||
          ele.unique_name === "total_invested" ||
          ele.unique_name === "current_value" ||
          ele.unique_name === "unrealized_gain"
        ) {
          ele.unique_name == "unrealized_gain" ? tempArr.push(unrealized_gain_obj) : tempArr.push(ele);
        }
      });
    } else if (type === "holdingDetail") {
      stocks.forEach((ele, ind) => {
        if (
          ele.unique_name === "total_invested" ||
          ele.unique_name === "unrealized_gain" ||
          ele.unique_name === "current_value"
        ) {
          tempArr.push(ele);
        }
      });
    } else {
      stocks.forEach((ele, ind) => {
        if (ele.unique_name === "" || ele.unique_name === "qty" || ele.unique_name === "lastprice") {
          tempArr.push(ele);
        }
      });
    }
    return tempArr;
  };

  render() {
    const { stock, disableDVM, children, type, disableNavigation,position } = this.props;
    let listData, stockCode, amount, date, quantity;
    let unrealizedGain = stock.obj.unrealized_gain ? stock.obj.unrealized_gain : "--";
    // console.log("stock item-->", stock.obj);
    let unrealizedGainPercentage = stock.obj.unrealized_gainP ? stock.obj.unrealized_gainP : "-";
    if (!disableDVM) {
      listData = this.generateList(stock.array, "holdingDetail");
      stockCode = stock.obj.NSEcode ? stock.obj.NSEcode : stock.obj.BSEcode ? stock.obj.BSEcode : "----";
      quantity = !isNaN(stock.obj.qty) ? formatNumberToInr(stock.obj.qty, true) : "-";
    } else {
      listData = this.generateList(stock.array);
    }
    return (
      <View style={styles.card}>
        {disableDVM ? (
          <View style={{ justifyContent: "space-around" }}>
            {stock.obj && stock.obj.portfolio_name && <Text>{stock.obj.portfolio_name}</Text>}
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={listData}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              numColumns={3}
              extraData={this.state.expand}
              keyboardShouldPersistTaps="always"
            />
            {children}
          </View>
        ) : (
          <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
            <View style={styles.StockCodeContainer} onPress={this.gotoStockDetails}>
              <TouchableOpacity style={{}} onPress={disableNavigation ? null : this.gotoStockDetails}>
                <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
                  {stockCode ? stockCode : "Unnamed"}
                </Text>
              </TouchableOpacity>
              {this.renderDVM()}
            </View>
            <View
              style={[
                styles.ShareDetailRowView,
                { flexDirection: "row", alignItems: "center", justifyContent: "space-between" },
              ]}
            >
              <View>
                <Text style={[styles.sharesTxt, { fontWeight: "500" }]}>
                  {`${quantity} `}
                  <Text style={[styles.sharesTxt, , { fontWeight: "400" }]}>shares</Text>
                </Text>
              </View>
              <DetailChip
                ChipMainContainer={{
                  backgroundColor: parseInt(unrealizedGain ? unrealizedGain : 0) > 0 ? colors.mediumGreen : colors.red,
                }}
                firstDetail={formatNumberToInr(unrealizedGain, true)}
                secondDetail={`${roundNumber(unrealizedGainPercentage)}%`}
                secondChipText={{ fontWeight: "500" }}
              />
            </View>
            <FlatList
              showsHorizontalScrollIndicator={false}
              data={listData}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
              numColumns={3}
              extraData={this.state.expand}
              keyboardShouldPersistTaps="always"
            />
            {this.state.expand && this.renderButtonRow()}
          </TouchableOpacity>
        )}
      </View>
    );
  }
}

export const CardButton = (props) => {
  return (
    <TouchableOpacity {...props}>
      <View style={[styles.buttonMainView, props.buttonMainView]}>
        {props.disableImg ? null : (
          <Image source={props.iconSource ? props.iconSource : iconInactive} resizeMode="contain" />
        )}
        <Text style={[styles.buttonText, props.buttonText]}>{props.ButtonName}</Text>
      </View>
    </TouchableOpacity>
  );
};

export const DetailChip = (props) => {
  const { firstDetail, secondDetail, ChipMainContainer, firstChipText, secondChipText } = props;
  return (
    <View style={[styles.ChipMainContainer, ChipMainContainer]}>
      <Text style={[styles.firstChipText, firstChipText]}>
        {firstDetail}
        <Text style={[styles.secondChipText, secondChipText]}>{`  ${secondDetail}`}</Text>
      </Text>
    </View>
  );
};

export const EmptyHoldings = (props) => {
  return (
    <View style={{ alignItems: "center", justifyContent: "center", paddingVertical: wp(30) }}>
      <Image source={emptyHoldings} resizeMode="contain" />
      <View
        style={{
          paddingVertical: wp(3),
        }}
      >
        <Text
          style={{
            color: colors.greyishBrown,
            fontWeight: "bold",
            fontSize: RF(2.6),
            textAlign: "center",
          }}
        >
          How are your Investments doing?
        </Text>
      </View>
      <View
        style={{
          paddingVertical: wp(1),
          paddingHorizontal: wp(13),
        }}
      >
        <Text
          style={{
            color: colors.greyishBrown,
            fontSize: RF(2.4),
            textAlign: "center",
          }}
        >
          Add and manage your holdings here to get your portfolio health check
        </Text>
      </View>

      <View style={{ paddingTop: wp(30) }}>
        <CHIPS
          disableImage
          selected
          chipsMainView={styles.button}
          name="Add a Holding"
          onPress={props.onPressAddHoldings}
        />
      </View>
    </View>
  );
};

export const InputFieldWithTag = (props) => {
  const { inputBoxStyle, inputLabel, label } = props;
  // const inputBoxStyle = this.state.inputFocused == label ? [styles.inputContainer2, styles.activeTextBox, { paddingBottom: wp(0.5), marginBottom: wp(1) }] : [styles.inputContainer2];
  return (
    <View style={[styles.inputContainer2, inputBoxStyle]}>
      <Text style={[styles.inputLabel, inputLabel]}>
        {label}
        <Text style={styles.redText}> *</Text>
      </Text>
      <TextInput
        style={{ paddingVertical: 3 }}
        numberOfLines={1}
        maxLength={20}
        keyboardType="numeric"
        {...props}
        placeholder={props.placeholder}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  card: {
    marginVertical: "2%",
    marginHorizontal: "3%",
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 5,
    // backgroundColor:'yellow'
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "500",
    // marginRight: wp(4)
  },
  detailRow: {
    flex: 1,
    marginTop: wp(5),
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 6,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(1.9),
    fontWeight: "500",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.6),
    color: colors.cement,
  },
  hide: {
    display: "none",
  },

  buttonRow: {
    flexDirection: "row",
    marginTop: wp(6),
    marginBottom: wp(3),
  },
  buttonContainer: {
    flex: 1,
  },
  buttonMainView: {
    // height: hp(4),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    padding: wp(2),
    // paddingHorizontal:wp(0.5),
  },
  buttonIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 5,
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  sharesTxt: {
    fontSize: RF(1.9),
    color: colors.black,
  },
  whiteText: {
    color: colors.white,
  },
  ChipMainContainer: {
    backgroundColor: colors.primary_color,
    marginLeft: wp(4),
    paddingHorizontal: wp(2.2),
    paddingVertical: wp(1.2),
    borderRadius: 5,
  },
  firstChipText: {
    fontSize: RF(1.9),
    color: colors.white,
  },
  secondChipText: {
    fontWeight: "500",
    fontSize: RF(2.2),
    color: colors.white,
  },
  inputContainer2: {
    paddingBottom: wp(1),
    marginBottom: wp(3),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: { borderBottomWidth: 2, borderBottomColor: colors.primary_color },
  inputLabel: { fontSize: RF(1.9), color: colors.black },
  redText: { color: colors.red },
  button: {
    paddingHorizontal: wp(25),
    marginHorizontal: 0,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: wp(2.5),
  },
});
