//Root Library
import React from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";

//Responsive Compoenents
import { heightPercentageToDP as hp, widthPercentageToDP } from "react-native-responsive-screen";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";

//Constants
import { colors } from "../../../styles/CommonStyles";

import Modal from "../../../libs/react-native-modalbox";

//Page Constants
const watchlistIcon = require("../../../assets/icons/portfolio/watchlistIcon.png");
const addIcon = require("../../../assets/icons/portfolio/addIcon.png");
const downarrow = require("../../../assets/icons/arrow-right-blue.png");
const downarrowImgWhite = require("../../../assets/icons/portfolio/downArrowWhite.png");
const downarrowImgBlue = require("../../../assets/icons/portfolio/downArrowBlue.png");

//Tag Name with Icon
export const TagNameWithIcon = (props) => {
  return (
    <View style={[styles.headingContainer, props.headingContainer]}>
     {props.disableIcon ? null : <Image source={props.Icon ? props.Icon : watchlistIcon} resizeMode="contain" />}
      <View style={{ marginHorizontal: widthPercentageToDP(1) }}></View>
      <Text style={[styles.headingText, props.headingText]}>{props.TagName ? props.TagName : "WATCHLISTS"}</Text>
    </View>
  );
};

//Action Button With Icon
export const ActionButtonWithIcon = (props) => {
  return (
    <TouchableOpacity style={[styles.actionContainer, props.actionContainer]} {...props}>
      <Image source={props.Icon ? props.Icon : addIcon} resizeMode="contain" />
      <View style={[styles.gapMaintainer, props.gapMaintainer]}></View>
      <Text style={[styles.actionText, props.actionText]}>{props.ActionText ? props.ActionText : "New Watchlist"}</Text>
    </TouchableOpacity>
  );
};

export const ListItem = (props) => {
  return (
    <TouchableOpacity
      // style={{ backgroundColor: 'grey' }}
      {...props}
    >
      <View style={[styles.mainContainer]}>
        <View style={styles.itemInfoContainer}>
          <Text style={styles.itemNameText}>{props.name ? props.name : ""}</Text>
          <Text style={styles.itemDetail}>{props.description ? props.description : ""}</Text>
        </View>
        <View style={[styles.imageContainer]}>
          <Image source={downarrow} resizeMode="contain" />
        </View>
      </View>
    </TouchableOpacity>
  );
};

export const CHIPS = (props) => {
  let key = props.key;

  if (props.gradient) {
    return (
      <TouchableOpacity
        // style={[
        //   styles.chipsMainView,
        //   {
        //     // backgroundColor: props.selected ? colors.primary_color : colors.whiteTwo
        //   },
        //   props.chipsMainView
        // ]}
        {...props}
      >
        <LinearGradient
          colors={[
            props.selected ? colors.gradientCardStartColor : "#fff",
            props.selected ? colors.gradientCardEndColor : "#fff",
          ]}
          style={[
            styles.chipsMainView,
            props.chipsMainView,
            { borderWidth: props.selected ? widthPercentageToDP(0.1) : widthPercentageToDP(0.3) },
          ]}
        >
          <View style={[styles.chipsNameview]}>
            <Text style={[styles.chipText, { color: props.selected ? colors.whiteTwo : colors.primary_color }]}>
              {props.name ? props.name : "missing key"}
            </Text>
          </View>
          {props.disableImage ? null : (
            <View style={[styles.chipsImageView]}>
              <Image
                resizeMode="contain"
                style={{ transform: [{ rotate: props.doubleSelection ? "180deg" : "0deg" }] }}
                source={props.source ? props.source : props.selected ? downarrowImgWhite : downarrowImgBlue}
              />
            </View>
          )}
        </LinearGradient>
      </TouchableOpacity>
    );
  } else {
    return (
      <TouchableOpacity
        style={[
          styles.chipsMainView,
          {
            backgroundColor: props.selected ? colors.primary_color : colors.whiteTwo,
          },
          props.chipsMainView,
        ]}
        {...props}
      >
        <View style={[styles.chipsNameview]}>
          <Text style={[styles.chipText, { color: props.selected ? colors.whiteTwo : colors.primary_color }]}>
            {props.name ? props.name : "missing key"}
          </Text>
        </View>
        {props.disableImage ? null : (
          <View style={[styles.chipsImageView]}>
            <Image
              resizeMode="contain"
              style={{ transform: [{ rotate: props.doubleSelection ? "0deg" : "180deg" }] }}
              source={props.source ? props.source : props.selected ? downarrowImgWhite : downarrowImgBlue}
            />
          </View>
        )}
      </TouchableOpacity>
    );
  }
};

export const PopUpbutton = (props) => {
  return (
    <TouchableOpacity style={[styles.buttonMainView, props.buttonMainView]} {...props}>
      <Text style={[styles.buttonNameText, props.buttonNameText]}>{props.buttonName}</Text>
    </TouchableOpacity>
  );
};

export const CreateNewWatchlist = (props) => {
  // console.log("Modal Props -->",props)
  return (
    <Modal
      style={styles.modalStyle}
      position={"bottom"}
      swipeArea={20}
      animationDuration={0}
      ref={props.PopupRef}
      // onOpened={() => this.setState({ isOpen: true })}
      // onClosed={() => this.setState({ isOpen: false })}
      {...props}
    >
      <View style={styles.infoContainer}>
        {/* <KeyboardAwareScrollView showsVerticalScrollIndicator={false}> */}
        <View style={styles.row}>
          <Text style={styles.title} numberOfLines={1} ellipsizeMode="tail">
            Create New Watchlist
          </Text>

          <TouchableOpacity style={{ padding: wp(2) }} onPress={props.closeDialog}>
            <Image source={require("../../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
          </TouchableOpacity>
        </View>
        <View>{props.child}</View>
      </View>

      {/* <Toast
          ref="toast"
          position="center"
          style={globalStyles.toast}
          textStyle={globalStyles.toastText}
        />*/}
    </Modal>
  );
};

export const alphabeticalSort = (anyArray, type, bykey) => {
  if (anyArray.length != 0) {
    var highest_first_array;
    var lowest_first_array;
    // let compInd = anyArray[0].array.findIndex(ele => ele.name === bykey);
    // console.log("Array-->", anyArray[0].array[0], "type-->", typeof anyArray, "TYpe=-->", type, "Key==>", bykey);
    if (type === "A") {
      highest_first_array = anyArray.sort((a, b) => {
        targetValue_A = a.obj.NSEcode;
        targetValue_B = b.obj.NSEcode;
        if (targetValue_A < targetValue_B) return -1;
        if (targetValue_A > targetValue_B) return 1;
        return 0;
      });
      // console.log("highest first -->", highest_first_array)
      return highest_first_array;
    } else {
      lowest_first_array = anyArray.sort((a, b) => {
        targetValue_A = a.obj.NSEcode;
        targetValue_B = b.obj.NSEcode;
        if (targetValue_A < targetValue_B) return 1;
        if (targetValue_A > targetValue_B) return -1;
        return 0;
      });
      // console.log("lowest first -->", lowest_first_array)
      return lowest_first_array;
    }
  }
};

export const sortDataByKey = (anyArray, type, bykey) => {
  if (anyArray.length != 0) {
    let compInd = anyArray[0].array.findIndex((ele) => ele.name === bykey);
    let targetValue_A;
    let targetValue_B;
    var highest_first_array;
    var lowest_first_array;
    // console.log("Array-->", anyArray, "type-->", typeof anyArray, "TYpe=-->", type, "Key==>", bykey);
    // console.log("compInd", compInd)
    if (bykey ==='Added on' || bykey === "Sell Date" || bykey === "Buy Date" || bykey === "Open Date") {
      // console.log("Inside Date Sorting -->", bykey)
        if (type == "highest_first") {
          highest_first_array = anyArray.sort((a, b) => {
            targetValue_A = new Date(a.array[compInd].value).getTime();
            targetValue_B = new Date(b.array[compInd].value).getTime();
            if (targetValue_B == targetValue_A) return 0;
            if (targetValue_B > targetValue_A) return 1;
            if (targetValue_B < targetValue_A) return -1;
          });
          // console.log("Result -->", highest_first_array)
          return highest_first_array;
        } else {
          lowest_first_array = anyArray.sort((a, b) => {
            targetValue_A = new Date(a.array[compInd].value).getTime();
            targetValue_B = new Date(b.array[compInd].value).getTime();
            if (targetValue_B == targetValue_A) return 0;
            if (targetValue_B > targetValue_A) return -1;
            if (targetValue_B < targetValue_A) return 1;
          });
          // console.log("Result -->", lowest_first_array)
          return lowest_first_array;
        }
    } else {
      if (type == "highest_first") {
        highest_first_array = anyArray.sort((a, b) => {
          targetValue_A = a.array[compInd].value;
          targetValue_B = b.array[compInd].value;
          // console.log("Highest first --->", targetValue_A, "B ->", targetValue_B)
          if (targetValue_B == targetValue_A) return 0;
          if (targetValue_B > targetValue_A) return 1;
          if (targetValue_B < targetValue_A) return -1;
        });
        // console.log("Result -->", highest_first_array)
        return highest_first_array;
      } else {
        lowest_first_array = anyArray.sort((a, b) => {
          targetValue_A = a.array[compInd].value;
          targetValue_B = b.array[compInd].value;
          // console.log("Highest first --->", targetValue_A, "B ->", targetValue_B)
          if (targetValue_B == targetValue_A) return 0;
          if (targetValue_B > targetValue_A) return -1;
          if (targetValue_B < targetValue_A) return 1;
        });
        // console.log("Result -->", lowest_first_array)
        return lowest_first_array;
      }
    }
  } else {
    return [];
  }
};

const styles = StyleSheet.create({
  headingContainer: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "space-between",
  },
  headingText: {
    color: colors.black,
    fontSize: RF(2.4),
    fontWeight: "300",
  },
  actionContainer: {
    flexDirection: "row",
    alignItems: "center",
  },
  gapMaintainer: { marginHorizontal: widthPercentageToDP(0.5) },
  actionText: {
    color: colors.primary_color,
    fontSize: RF(2.2),
    fontWeight: "500",
  },
  mainContainer: {
    // flex: 1,
    flexDirection: "row",
    // minHeight: hp(11),
    // backgroundColor: 'red',
    paddingVertical: widthPercentageToDP(4),
    paddingHorizontal: widthPercentageToDP(3),
  },

  itemInfoContainer: {
    flex: 1,
    // backgroundColor:'yellow'
    paddingHorizontal: widthPercentageToDP(2),
  },
  itemNameText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
    // marginBottom: 7
  },
  itemDetail: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    opacity: 0.85,
  },
  imageContainer: {
    justifyContent: "center",
  },
  chipsMainView: {
    // backgroundColor:'cyan',
    flexDirection: "row",
    borderWidth: widthPercentageToDP(0.5),
    borderRadius: widthPercentageToDP(1.4),
    // justifyContent:'space-between',
    paddingVertical: widthPercentageToDP(1.8),
    paddingHorizontal: widthPercentageToDP(3),
    marginHorizontal: widthPercentageToDP(1.5),
    // marginVertical:widthPercentageToDP(20),
    borderColor: colors.primary_color,
  },
  chipsNameview: {
    // backgroundColor:'red',
    justifyContent: "center",
  },
  chipText: {
    fontSize: RF(1.9),
    // fontWeight: "500"
  },
  chipsImageView: {
    // backgroundColor:'pink',
    justifyContent: "center",
    alignItems: "flex-end",
    marginLeft: widthPercentageToDP(1.8),
  },
  buttonMainView: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: widthPercentageToDP(9),
    paddingVertical: widthPercentageToDP(3),
    borderRadius: 5,
    backgroundColor: colors.red,
    borderWidth: 1,
    borderColor: colors.red,
  },
  buttonNameText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  modalStyle: {
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
    backgroundColor: "white",
    height: hp(55),
  },
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    // backgroundColor: 'yellow',
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  row: {
    // height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(7),
    marginTop: hp(2),
    marginRight: wp(4),
    // paddingHorizontal: wp(4)
  },
});
