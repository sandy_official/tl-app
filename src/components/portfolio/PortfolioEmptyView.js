import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";

export default class PortfolioEmptyView extends React.PureComponent {
  portfolioIcon = require("../../assets/icons/portfolio-analysis.png");
  addIcon = require("../../assets/icons/add-white.png");
  heading = "Investment Insights Await You!";
  message = "Create and add stocks to your portfolio to get rich analysis on your investments";

  render() {
    const { onPress } = this.props;

    return (
      <LinearGradient colors={[colors.gradientCardStartColor, colors.gradientCardEndColor]} style={styles.container}>
        <Image source={this.portfolioIcon} />
        <Text style={styles.heading}>{this.heading}</Text>
        <Text style={styles.message}>{this.message}</Text>
        <TouchableOpacity style={styles.button} onPress={onPress}>
          <Image source={this.addIcon} />
          <Text style={styles.buttonText}>Create Portfolio</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: wp(5),
    // paddingBottom: hp(10)
  },
  message: {
    fontSize: RF(2.2),
    color: "rgba(255,255,255,0.75)",
    marginTop: wp(2),
    lineHeight: wp(5.5),
    textAlign: "center",
  },
  heading: {
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.white,
    marginTop: wp(6.2),
  },
  button: {
    flexDirection: "row",
    width: wp(80),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    marginHorizontal: wp(8),
    marginTop: hp(4),
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: wp(2),
    textAlign: "center",
    // marginLeft: wp(1)
  },
});
