import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList, ActivityIndicator } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import LinearGradient from "react-native-linear-gradient";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../../styles/CommonStyles";
import { getDVMBackgroundColor } from "../../../utils/Utils";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

import ChartView from "react-native-highcharts";
import { BAR_CHART_CONFIG, LINE_CHART_CONFIG, CHART_OPTIONS } from "../../../utils/ChartConfigs";
// export const renderPageIndicator = () => {
//     // const { data } = this.props;
//     // const { currentPosition } = this.state;
//      let data = [];
//     return (
//       <View style={styles.pageIndicator}>
//         {data.map((item, index) => {
//           const indicatorStyle = currentPosition == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
//           return <View style={indicatorStyle} key={index} />;
//         })}
//       </View>
//     );
// };

const portfolioImg = require("../../../assets/icons/portfolio/portfolioIcon.png");
const indexIcon = require("../../../assets/icons/portfolio/indexIcon.png");

export const Divider = (props) => {
  return <View style={[styles.DividerMainView, props.DividerMainView]}></View>;
};

export const ProgressDivider = (props) => {
  return (
    <View style={[styles.ProgressDividerMainView, props.ProgressDividerMainView]}>
      {/* props.totalNumbers.map((ele,ind)=>{
        return( */}
      <View></View>
      {/* ); */}
      })
    </View>
  );
};

export const ColoredCompTag = (props) => {
  const { Losers, Gainers, BarTextStyle } = props;
  const gainerTxts = ["Gainer", "Gainers"];
  const loserTxts = ["Loser", "Losers"];
  return (
    <View style={{ flexDirection: "row" }}>
      <View style={{ flex: 2, marginBottom: wp(1) }}>
        {/* <Text style={[styles.BarTextStyle,BarTextStyle]}>{`${Gainers} Gainers`}</Text> */}
        <Text style={[styles.BarTextStyle, BarTextStyle]}>{`${Gainers} ${
          Gainers < 1 ? gainerTxts[0] : gainerTxts[1]
        }`}</Text>
      </View>
      <View style={{ flex: 2, alignItems: "flex-end" }}>
        {/* <Text style={[styles.BarTextStyle,BarTextStyle]}>{`${Losers} Losers`}</Text> */}
        <Text style={[styles.BarTextStyle, BarTextStyle]}>{`${Losers} ${
          Losers < 1 ? loserTxts[0] : loserTxts[1]
        }`}</Text>
      </View>
    </View>
  );
};

export const ColoredBar = (props) => {
  const { Losers, Gainers } = props;
  if (Losers && Gainers) {
    return (
      <View>
        <ColoredCompTag Gainers={0} Losers={6} {...props} />
        <View style={{ flexDirection: "row", marginTop: wp(1) }}>
          <View style={[styles.BarViewLeft, { backgroundColor: colors.green, flex: Gainers ? Gainers : 0 }]}>
            <Text style={[styles.Smallfont, { color: colors.green }]}>{`Gainers`}</Text>
          </View>
          <View style={[styles.BarViewRight, { backgroundColor: colors.red, flex: Losers ? Losers : 0 }]}>
            <Text style={[styles.Smallfont, { color: colors.red }]}>{`Losers`}</Text>
          </View>
        </View>
      </View>
    );
  } else if (Losers === 0) {
    return (
      <View>
        <ColoredCompTag Gainers={Gainers} Losers={0} {...props} />
        <View
          style={[
            styles.SingleLine,
            {
              backgroundColor: colors.green,
              flex: 1,
            },
          ]}
        >
          <Text style={[styles.Smallfont, { color: colors.green }]}>{`${Gainers} Gainers`}</Text>
        </View>
      </View>
    );
  } else if (Gainers === 0) {
    return (
      <View>
        <ColoredCompTag Gainers={0} Losers={Losers} {...props} />
        <View style={[styles.SingleLine, { backgroundColor: colors.red, flex: 1 }]}>
          <Text style={[styles.Smallfont, { color: colors.red }]}>{`${Losers} Losers`}</Text>
        </View>
      </View>
    );
  } else {
    return null;
  }
};

export const InsightComponent = (props) => {
  const { InsightCompMainView, imgSrc, insightElementText, insightText, dividingActive } = props;

  return (
    <View style={[styles.InsightCompMainView, InsightCompMainView]}>
      <View style={{ flex: 1.1, alignItems: "flex-start" }}>
        <Image source={imgSrc} resizeMode="contain" />
      </View>
      <View style={{ flex: 10 }}>
        <Text style={[{ color: colors.black }, insightText]}>{insightElementText}</Text>
      </View>
    </View>
  );
};

export const CompareFactor = (props) => {
  const { img, name, containerStyle, color, textStyle } = props;
  return (
    <View style={[{ flexDirection: "row", alignItems: "center" }, containerStyle]}>
      {color ? (
        <View style={{ height: wp(3), width: wp(3), backgroundColor: color, borderRadius: 2 }} />
      ) : (
        <Image source={img ? img : portfolioImg} resizeMode="contain" style={{ height: wp(3), width: wp(3) }} />
      )}
      <View style={{ marginRight: wp(1.5) }}></View>
      <Text style={[{ color: colors.black }, textStyle]}>{name}</Text>
    </View>
  );
};

export const BarChartWithIndicator = (props) => {
  const {
    EnableLoader,
    LoadingProgress,
    ChartData,
    ChartMainView,
    ChartLabel,
    ChartLabelTextStyle,
    LoaderColor,
    ChartLoadingMainView,
    img,
  } = props;
  const showCharts = ChartData && ChartData.length > 0;
  if (showCharts) {
    return (
      <View style={[styles.chartCard, styles.barChartCard]}>
        {ChartLabel ? (
          <View style={{ justifyContent: "space-between", flexDirection: "row" }}>
            <View style={{ flex: 6 }}>
              <Text style={[styles.ChartLabelTextStyle, ChartLabelTextStyle]}>
                <Text style={{ textTransform: "capitalize" }}>{`${ChartLabel}`}</Text>
                {`'s Analysis (in %)`}
              </Text>
            </View>
            <View style={{ flex: 4, flexDirection: "row", justifyContent: "space-between" }}>
              <CompareFactor
                img={props.firstParamImg ? props.firstParamImg : portfolioImg}
                name={props.firstParameter}
              />
              <CompareFactor
                img={props.SecondParamImg ? props.SecondParamImg : indexIcon}
                name={props.secondParameter}
              />
            </View>
          </View>
        ) : null}

        <ChartView
          // style={[styles.chartContainer, styles.barChartContainer]}
          style={[styles.ChartMainView, ChartMainView]}
          options={CHART_OPTIONS}
          {...props}
        />
      </View>
    );
  } else {
    if (EnableLoader && LoadingProgress) {
      return (
        <View style={[styles.ChartLoadingMainView, ChartLoadingMainView]}>
          <ActivityIndicator size="large" color={LoaderColor} />
          <Text>Loading....</Text>
        </View>
      );
    } else {
      return null;
    }
  }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  DividerMainView: {
    borderBottomWidth: wp(0.2),
    borderBottomColor: colors.whiteTwo,
  },
  ProgressDividerMainView: {},
  BarViewLeft: {
    borderTopLeftRadius: 5,
    borderBottomLeftRadius: 5,
    // borderBottomWidth:5,
    // alignItems:'flex-start'
  },
  BarViewRight: {
    borderTopRightRadius: 5,
    borderBottomRightRadius: 5,
    // borderBottomWidth:5,
    // alignItems:'flex-end'
  },
  SingleLine: {
    borderRadius: 5,
  },
  BarTextStyle: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  InsightCompMainView: {
    flexDirection: "row",
    paddingVertical: wp(1),
    justifyContent: "center",
    alignItems: "center",
  },
  chartCard: {
    // paddingVertical:wp(1),
    // overflow: "hidden",
    // height: hp(35),
    // padding: wp(3),
    // borderRadius: 5,
    // backgroundColor: colors.white,
    // backgroundColor:'yellow'
    // shadowColor: "rgba(0, 0, 0, 0.1)",
    // shadowOffset: {
    //     width: 0,
    //     height: 1
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1,
    // elevation: 2
  },
  barChartCard: {
    overflow: "hidden",
    // width: wp(92),
    // marginRight: 9,
    // marginVertical: wp(3)
  },
  ChartMainView: { height: hp(40), width: wp(93), overflow: "hidden" },
  ChartLoadingMainView: {
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor:'red'
  },
  ChartLabelTextStyle: {
    fontWeight: "500",
    fontSize: RF(2.2),
    color: colors.black,
    marginVertical: wp(1),
  },
  Smallfont: {
    fontSize: RF(0.5),
  },
});
