import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";

export default class MFEmptyView extends React.PureComponent {
  stockIcon = require("../../assets/icons//portfolio/mutual-funds-summary-grey.png");
  addIcon = require("../../assets/icons/add-white.png");
  heading = "Missing out on Mutual Fund returns?";
  message = "Compare mutual funds and add these to your portfolio.";

  render() {
    const { onButtonPress, onHide } = this.props;

    return (
      <View style={styles.container}>
        <Image source={this.stockIcon} />
        <Text style={styles.heading}>{this.heading}</Text>
        <Text style={styles.message}>{this.message}</Text>

        <TouchableOpacity style={styles.button} onPress={onButtonPress}>
          {/* <Image source={this.addIcon} /> */}
          <Text style={styles.buttonText}>Add MF to your Portfolio</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.textButton} onPress={onHide}>
          <Text style={styles.dontShowMessage}>Don’t Show this again</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    height: hp(50),
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: wp(5),
    // paddingVertical: hp(5)
  },
  message: {
    fontSize: RF(2.2),
    color: colors.steelGrey,
    marginTop: wp(2),
    lineHeight: wp(5.5),
    textAlign: "center",
  },
  heading: {
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginTop: wp(6.2),
  },
  button: {
    flexDirection: "row",
    width: wp(80),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(8),
    marginTop: hp(4),
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: wp(2.4),
    textAlign: "center",
    // marginLeft: wp(1)
  },
  textButton: {
    marginTop: hp(4),
  },
  dontShowMessage: {
    fontSize: RF(2.2),
    color: colors.cobalt,
  },
});
