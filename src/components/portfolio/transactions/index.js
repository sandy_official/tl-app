//Root Library
import React from "react";
import { View, Platform, StyleSheet, Text, Image, TouchableOpacity, TextInput, ActivityIndicator } from "react-native";

//Responsive Compoenents
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

//Constants
import { colors, fontsize } from "../../../styles/CommonStyles";
import LinearGradient from "react-native-linear-gradient";

const crossIcon = require("../../../assets/icons/cross-blue.png");
const downArrowIconBlue = require("../../../assets/icons/arrow-down-blue.png");
const emptyTransactionImg = require("../../../assets/icons/noTransactionsIllustration.png");
export const SearchBoxwithCloseButton = (props) => {
  return (
    <View style={[styles.searchMainView, props.searchMainView]}>
      <View style={{ flex: 11, paddingVertical: wp(0.5), paddingHorizontal: wp(1) }}>
        <TextInput
          style={{ height: wp(10), justifyContent: "center", fontSize: RF(2.2), color: colors.black }}
          placeholderTextColor={colors.black}
          placeholder="Search a stock"
          ref={props.inputRef}
          maxLength={30}
          {...props}
        />
      </View>
      <TouchableOpacity
        style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        onPress={props.onClearPress}
      >
        <Image source={crossIcon} resizeMode="contain" />
      </TouchableOpacity>
    </View>
  );
};

export const GradientCardForDetail = (props) => {
  return (
    <LinearGradient
      colors={[colors.gradientCardStartColor, colors.gradientCardEndColor]}
      style={[styles.GradientCardMainView, props.GradientCardMainView]}
    >
      {props.child ? props.child : null}
    </LinearGradient>
  );
};

export const CardDetailText = (props) => {
  if (props.enableLoader) {
    return (
      <View style={[styles.CardTextView, props.CardTextView]}>
        {props.isLoading ? (
          <ActivityIndicator size={"small"} color={colors.white} {...props} />
        ) : (
            <Text style={[styles.CardText, props.CardText]}>{props.Name ? props.Name : "Unnamed"}</Text>
          )}
      </View>
    );
  } else if (props.biPartValues) {
    return (
      <View style={[styles.CardTextView, { flexDirection: 'row' }, props.CardTextView,]}>
        <Text style={[styles.CardText, props.CardText]}>{props.Name ? props.Name : "Unnamed"} </Text>
        <Text style={[styles.CardText,{ fontWeight: 'normal'},props.CardText2]}>{props.secondName ? props.secondName : "Unnamed"}</Text>
      </View>
    );
  } else {
    return (
      <View style={[styles.CardTextView, props.CardTextView]}>
        <Text style={[styles.CardText, props.CardText]}>{props.Name ? props.Name : "Unnamed"}</Text>
      </View>
    );
  }
};

export const SortingTagWithIcon = (props) => {
  return (
    <TouchableOpacity style={[styles.SortingTagMainView, props.SortingTagMainView]} {...props}>
      <View style={[styles.TagTextView, props.TagTextView]}>
        <Text style={[styles.TagTextStyle, props.TagTextStyle]}>{props.TagText}</Text>
      </View>
      <View style={[styles.ImageView, props.ImageView]}>
        <Image source={props.icon ? props.icon : downArrowIconBlue} style={[props.ImageStyle]} resizeMode="contain" />
      </View>
    </TouchableOpacity>
  );
};

export const EmptyTransaction = (props) => {
  return (
    <View style={[{ alignItems: "center", justifyContent: "center" }, props.mainView]}>
      <Image source={emptyTransactionImg} resizeMode="contain" />
      <View
        style={{
          paddingTop: wp(7),
        }}
      >
        <Text
          style={{color: colors.greyishBrown,fontWeight: "bold",
            fontSize: RF(2.4),textAlign: "center",}}
        > No Transactions yet
        </Text>
      </View>
      <View
        style={{
          paddingVertical: wp(3),
          paddingHorizontal: wp(10),
        }}
      >
        <Text style={{ color: colors.greyishBrown, fontSize: RF(1.9), textAlign: "center" }}>
          You haven't made any transactions so far, all your transaction history will show up here
        </Text>
      </View>
    </View>
  );
};

// this.convertDateObject("API_Formet", selectedTimePeriod.startDate)
convertDateObject = (into, date) => {
  // console.log("Type", into, "Date -->", date);
  let res_Date, day, year, monthApi, month, formattedDate;
  if (into === "API_Formet") {
    res_Date = new Date(date);
    day = res_Date.getDate();
    year = res_Date.getFullYear();
    monthApi = res_Date.getMonth() + 1 < 10 ? "0" + (res_Date.getMonth() + 1) : res_Date.getMonth() + 1;
    dateForApi = year + "-" + monthApi + "-" + day;
    // console.log("dateForApi", dateForApi)
    return dateForApi;
  } else {
    res_Date = new Date(date);
    day = res_Date.getDate();
    month = MONTHS_FULL[res_Date.getMonth()];
    year = res_Date.getFullYear();
    formattedDate = getOrdinalNumForDate(day) + " " + month + ", " + year;
    return { formetted: formattedDate, dateObj: res_Date };
  }
};

export function getDateRange(limit) {
  let tempRangeArr = [];
  let currentDate = new Date();
  let obj = {};
  for (let i = 0; i < limit; i++) {
    i === 0
      ? (obj = {
        endDate: convertDateObject("API_Formet", currentDate),
        startDate: convertDateObject("API_Formet", new Date(`04-01-${currentDate.getFullYear() - (i + 1)}`)),
        range: "Year till date",
      })
      : (obj = {
        endDate: convertDateObject("API_Formet",new Date(`03-31-${currentDate.getFullYear() - i}`)),
        startDate: convertDateObject("API_Formet",new Date(`04-01-${currentDate.getFullYear() - (i + 1)}`)),
        range: `Apr'${(currentDate.getFullYear() - (i + 1)).toString().slice(2, 4)} - Mar'${(
          currentDate.getFullYear() - i
        )
          .toString()
          .slice(2, 4)}`,
      });
    tempRangeArr.push(obj);
  }
  return tempRangeArr;
}

const styles = StyleSheet.create({
  searchMainView: {
    flexDirection: "row",
    paddingHorizontal: wp(2),
    alignItems: "center",
    paddingVertical: wp(2),
    marginHorizontal: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 3,
      height: 3,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 10,
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  GradientCardMainView: {
    marginTop: wp(1),
    marginBottom: wp(1),
    padding: wp(5),
    borderRadius: 5,
    justifyContent: "space-between",
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  CardTextView: {
    justifyContent: "center",
  },
  CardText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.white,
  },
  SortingTagMainView: {
    flexDirection: "row",
    paddingVertical: wp(0.5),
    alignItems: "center",
  },
  TagTextView: {
    justifyContent: "center",
    paddingHorizontal: wp(0.5),
  },
  TagTextStyle: {
    fontWeight: "500",
    color: colors.black,
    fontSize: RF(1.9),
  },
  ImageView: {
    justifyContent: "center",
    alignItems: "center",
    paddingHorizontal: wp(1),
  },
});
