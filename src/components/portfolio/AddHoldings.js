import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableWithoutFeedback,
  TouchableOpacity,
  Platform,
  Image,
  TextInput,
  FlatList,
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { searchStockByTerm } from "../../controllers/StocksController";
import DateTimePicker from "react-native-modal-datetime-picker";
import { fetcher } from "../../controllers/Fetcher";
import { POST_METHOD, MONTHS_FULL } from "../../utils/Constants";
import { getOrdinalNumForDate } from "../../utils/Utils";
import { PORTFOLIO_ADD_HOLDING_URL } from "../../utils/ApiEndPoints";
import Toast from "react-native-easy-toast";
import CreatePortfolioModal from "../common/CreatePortfolioModal";
import LoginModal from "../common/LoginModal";
import { connect } from "react-redux";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import { addNewPortfolio } from "../../controllers/PortfolioController";

class AddHoldings extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      actionText: "",
      inputFocused: "",
      toggleDropdown: false,
      stockNameStatus: { status: false, error: "" },
      portfolioStatus: { status: false, error: "" },
      transactionDateStatus: { status: false, error: "" },
      transactionPriceStatus: { status: false, error: "" },
      transactionQtyStatus: { status: false, error: "" },
      stockName: "",
      selectedPortFolio: ["*", "Select a Portfolio"],
      transactionDate: { date: new Date(), formetted: "Select Date" },
      transactionPrice: "",
      transactionQty: "",
      searchedStocks: [],
      dropdownPosition: {},
      showPortfolioList: false,
      currentListToShow: "",
      isDatePickerVisible: false,
      errorText: "",
      isCreateWatchlistBtnActive: false,
      textFocused: null,
      portfolioName: "",
      portfolioDescription: "",
    };
  }

  componentDidMount = () => {
    this.setState({ actionText: "Add a New Holding:" }, () => {
      this.portfolioListApi();
    });
  };

  portfolioListApi = () => {
    const { userState, baseURL, appToken } = this.props;
    if (!userState.isGuestUser) loadPortfolio(baseURL, appToken, userState.token);
  };

  handleCloseButton() {
    this.refs.ActionPopUp.RBSheet.close();
  }

  clearStates = () => {
    this.setState({
      stockNameStatus: { status: false, error: "" },
      portfolioStatus: { status: false, error: "" },
      transactionDateStatus: { status: false, error: "" },
      transactionPriceStatus: { status: false, error: "" },
      transactionQtyStatus: { status: false, error: "" },
      stockName: "",
      selectedPortFolio: ["*", "Select a Portfolio"],
      transactionDate: { date: new Date(), formetted: "Select Date" },
      transactionPrice: "",
      transactionQty: "",
      searchedStocks: [],
      errorText: "",
    });
  };

  handlePopup = () => {
    this.clearStates();
    this.setState({ actionText: "Add a New Holding" }, () => {
      this.refs.ActionPopUp.RBSheet.open();
    });
  };

  handlePopUpHeight = () => {
    let height = hp(63);
    return height;
  };

  setInputText = (text, type) => {
    let txt;
    const { baseURL, token, appToken } = this.props;
    if (type === "Quantity") {
      if (text === "") {
        this.setState(
          {
            inputFocused: "Quantity",
            transactionQtyStatus: { status: false, error: "Please enter transaction quantity." },
            transactionQty: text,
            errorText: "Please enter transaction quantity.",
          },
          () => {
            this.setState({ errorText: "Please enter transaction quantity." });
          }
        );
      } else {
        this.setState(
          {
            transactionQtyStatus: { status: true, error: "" },
            transactionQty: text,
            errorText: "",
            inputFocused: "Quantity",
          },
          () => {
            this.setState({ errorText: "" });
          }
        );
      }
    } else if (type === "Price") {
      if (text === "") {
        this.setState(
          {
            inputFocused: "Price",
            transactionPriceStatus: { status: false, error: "Please enter transaction price." },
            transactionPrice: text,
            errorText: "Please enter transaction price.",
          },
          () => {
            this.setState({ errorText: "Please enter transaction price." });
          }
        );
      } else {
        this.setState(
          {
            transactionPriceStatus: { status: true, error: "" },
            transactionPrice: text,
            errorText: "",
            inputFocused: "Price",
          },
          () => {
            this.setState({ errorText: "" });
          }
        );
      }
    } else if (type === "stocks") {
      if (text === "") {
        txt = "Please Select any stock.";
        this.setState({ stockNameStatus: { status: false, error: txt }, stockName: text, errorText: txt });
      } else {
        this.setState({ stockName: text, currentListToShow: "stocks", inputFocused: "stocks" }, () => {
          if (text.length > 1) {
            searchStockByTerm(baseURL, appToken, token, text, "all", (StockList) =>
              this.setState({ searchedStocks: StockList })
            );
          } else this.setState({ searchedStocks: [] });
        });
      }
    } else if (type == "name") {
      if (text.length <= 60) {
        this.setState({ portfolioName: text, isCreateWatchlistBtnActive: text.length > 0 });
      }
    } else if (type == "description") {
      this.setState({ portfolioDescription: text });
    }
  };

  onStockPress = (stock) => {
    this.setState({
      selectedStock: stock,
      currentListToShow: "",
      searchedStocks: [],
      errorText: "",
      stockNameStatus: { status: true, error: "" },
      stockName: stock.get_full_name ? stock.get_full_name : stock.completename ? stock.completename : "",
      stockId: stock.stock_id ? stock.stock_id : stock.BSEcode ? stock.BSEcode : "",
    });
  };

  renderStocksList = (item, index) => {
    return (
      <TouchableOpacity onPress={() => this.onStockPress(item)}>
        <Text style={styles.stockListItem}>{item.get_full_name ? item.get_full_name : item.completename ? item.completename : ""}</Text>
      </TouchableOpacity>
    );
  };

  renderStockDropdown = () => {
    const { inputFocused } = this.state;
    return (
      <View>
        <Text style={{ color: colors.greyishBrown, fontSize: RF(1.9) }}>
          {"Enter Stock Name"}
          <Text style={{ color: "red" }}> *</Text>
        </Text>
        <View
          style={[
            styles.inputContainer,
            {
              borderBottomColor:
                inputFocused === "stocks"
                  ? this.state.stockNameStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.stockNameStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            },
          ]}
        >
          <TextInput
            placeholder={"Stock or MF name"}
            // maxLength={15}
            numberOfLines={1}
            style={[styles.textInput, { color: this.state.onFocusInput ? colors.primary_color : colors.black }]}
            placeholderTextColor={colors.steelGrey}
            onChangeText={(value) => this.setInputText(value, "stocks")}
            value={this.state.stockName}
            // onFocus={() => this.toggleTextInputFocus(true)}
            // onBlur={() => this.toggleTextInputFocus(false)}
          />
        </View>
      </View>
    );
  };

  updateDropdownPosition = (event) => {
    this.setState({
      dropdownPosition: event.nativeEvent.layout,
    });
  };

  renderPortfolioItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => this.selectPortfolio(item)}>
        <Text style={[styles.listItem, { color: item[0] === "#" ? colors.primary_color : colors.greyishBrown }]}>
          {item[1]}
        </Text>
      </TouchableOpacity>
    );
  };

  selectPortfolio = (item) => {
    if (item[0] === "#") {
      this.setState(
        {
          actionText: "Create New Portfolio",
        },
        () => {}
      );
    }
    this.setState({
      selectedPortFolio: item,
      currentListToShow: "portfolio",
      portfolioStatus: {
        error: "",
        status: true,
      },
      errorText: "",
    });
    this.toggleDropdown();
  };

  adjustingverticalPosition = () => {
    let position;
    const { y } = this.state.dropdownPosition;
    if (this.state.currentListToShow == "stocks") {
      position = Platform.OS == "android" ? y + -hp(2) : y + -hp(3);
    } else {
      position = Platform.OS == "android" ? y + hp(8) : y + hp(7);
    }
    return position;
  };

  renderList = () => {
    const { portfolio, portfolioData } = this.props;
    let portfolioArray = portfolio ? portfolio : portfolioData ? portfolioData : [];
    // console.log("Portfolio ->", portfolioArray, portfolio, portfolioData)
    const id = portfolioArray.findIndex((ele) => ele[0] === "#");
    let listData = id === -1 ? [["#", "Create new portfolio"], ...this.props.portfolio] : this.props.portfolio;
    const { width } = this.state.dropdownPosition;
    const style = {
      position: "absolute",
      top: this.adjustingverticalPosition(),
      width: width,
      maxHeight: hp(30),
      backgroundColor: colors.whiteTwo,
    };
    if (this.state.showPortfolioList && this.state.currentListToShow === "portfolio") {
      return (
        <View style={[style, styles.list]}>
          <FlatList
            data={listData}
            extraData={this.props}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderPortfolioItem}
            keyExtractor={(item, index) => index.toString()}
            keyboardShouldPersistTaps="always"
            // ListFooterComponent={() => this.renderPortfolioListFooter()}
          />
        </View>
      );
    } else if (this.state.currentListToShow === "stocks") {
      return (
        <View style={[style, styles.list]}>
          <FlatList
            data={this.state.searchedStocks}
            contentContainerStyle={[styles.StocksContainer]}
            extraData={this.state.searchedStocks}
            keyExtractor={(item, index) => index.toString()}
            renderItem={({ item, index }) => this.renderStocksList(item, index)}
            keyboardShouldPersistTaps="always"
          />
        </View>
      );
    }
    return null;
  };

  renderPortfolioListFooter = () => {
    return (
      <TouchableWithoutFeedback style={styles.footerItem} onPress={this.createNewPortfolio}>
        <Text style={styles.listFooterText}>Create New Portfolio</Text>
      </TouchableWithoutFeedback>
    );
  };

  toggleDatePicker = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "date" ? "" : "date";
    this.setState({
      isDatePickerVisible: !this.state.isDatePickerVisible,
      inputFocused: focusedInput,
    });
  };

  handleDatePicked = (date) => {
    this.toggleDatePicker();
    let day = date.getDate();
    let month = MONTHS_FULL[date.getMonth()];
    let year = date.getFullYear();
    let formattedDate = getOrdinalNumForDate(day) + " " + month + ", " + year;
    this.setState(
      {
        transactionDate: {
          formetted: formattedDate,
          dateObj: date,
        },
        transactionDateStatus: {
          status: true,
          error: "",
        },
        errorText: "",
      },
      () => {
        this.setState({
          errorText: "",
        });
      }
    );
  };

  renderDateField = () => {
    const inputBoxStyle =
      this.state.inputFocused == "date" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];

    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.transactionDateStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          {"Transaction Date "}
          <Text style={{ color: "red" }}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={() => this.toggleDatePicker()}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.black, fontSize: RF(2.2) }]}>
            {this.state.transactionDate.formetted}
          </Text>
          <Image
            source={require("../../assets/icons/date-picker-blue.png")}
            style={styles.inputIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderErrorText = () => {
    const { errorText } = this.state;
    if (errorText != "")
      return (
        <View style={{ marginVertical: wp(2) }}>
          <Text style={[styles.redText, { fontSize: RF(1.3) }]}>{`* ${errorText}`}</Text>
        </View>
      );
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };

  onPortfolioCreated = (updatedPortfolio) => {
    const { portfolioName, portfolioId } = updatedPortfolio;
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
    this.portfolioListApi();
  };

  createPortfolio = () => {
    const { token, baseURL, appToken } = this.props;
    const title = this.state.portfolioName;
    const description = this.state.portfolioDescription;

    this.setState({ refreshing: true });

    addNewPortfolio(baseURL, appToken, token, title, description, (isSuccess, response) => {
      if (isSuccess) {
        this.onPortfolioCreated(response);
        setTimeout(() => {
          this.refs.toast.show(`${response.portfolioName} created successfully.`, 500);
        }, 300);
        this.setState({
          portfolioName: "",
          portfolioDescription: "",
          selectedPortFolio: ["*", "Select a Portfolio"],
          actionText: "Add a New Holding",
        });
      } else {
        setTimeout(() => {
          this.refs.toast.show("Please try again", 500);
        }, 300);
      }
    });
  };

  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value });
  };

  cancelPortfolio = () => {
    this.setState({
      actionText: "Add a New Holding",
      selectedPortFolio: ["*", "Select a Portfolio"],
    });
  };

  renderHoldindsActionChilds = () => {
    const { transactionQty, transactionPrice, inputFocused } = this.state;
    if (this.state.actionText === "Create New Portfolio") {
      const buttonStyle = this.state.isCreateWatchlistBtnActive
        ? styles.createWatchlistBtn
        : [styles.createWatchlistBtn, { backgroundColor: colors.steelGrey }];

      const buttonText = this.state.isCreateWatchlistBtnActive
        ? styles.createWatchlistBtnText
        : [styles.createWatchlistBtnText, { color: colors.white }];
      const isWatchlistBtnDisabled = this.state.portfolioName.length == 0;
      const { textFocused, portfolioName, portfolioDescription } = this.state;

      return (
        <View style={{ flex: 1, paddingTop: wp(10) }}>
          <Text style={styles.inputLabel}>
            Enter Portfolio Title
            <Text style={{ color: colors.red }}>* </Text>
          </Text>
          <View style={[styles.inputContainer, textFocused === "name" ? styles.activeTextBox : {}]}>
            <TextInput
              maxLength={60}
              style={styles.textInput}
              placeholderTextColor={colors.steelGrey}
              onChangeText={(value) => this.setInputText(value, "name")}
              onFocus={() => this.toggleTextInputFocus("name")}
              onBlur={() => this.toggleTextInputFocus(null)}
            />
            <Text style={styles.inputLength}>{60 - portfolioName.length}</Text>
          </View>
          <View>
            <Text style={styles.inputLabel}>Enter Portfolio description (optional)</Text>
            <View style={[styles.inputContainer, textFocused === "description" ? styles.activeTextBox : {}]}>
              <TextInput
                maxLength={260}
                style={styles.textInput}
                placeholderTextColor={colors.steelGrey}
                onChangeText={(value) => this.setInputText(value, "description")}
                onFocus={() => this.toggleTextInputFocus("description")}
                onBlur={() => this.toggleTextInputFocus(null)}
              />
              <Text style={styles.inputLength}>{260 - portfolioDescription.length}</Text>
            </View>
          </View>

          <TouchableOpacity
            style={buttonStyle}
            onPress={() => this.createPortfolio()}
            disabled={isWatchlistBtnDisabled}
          >
            <Text style={buttonText}>Create Portfolio</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.createWatchlistBtn, { backgroundColor: colors.steelGrey }]}
            onPress={() => this.cancelPortfolio()}
          >
            <Text style={buttonText}>Cancel</Text>
          </TouchableOpacity>
          {/* {this.state.isLoading && <LoadingView />} */}
          <Toast ref="toast" position="top" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        </View>
      );
    }
    return (
      <View style={{ flex: 1 }} onLayout={this.updateDropdownPosition}>
        {this.renderStockDropdown()}
        {this.renderPortfolioDropdown()}
        {this.renderList()}
        {this.renderDateField()}
        <DateTimePicker
          // minimumDate={minimumSellDate}
          // maximumDate={maximumSellDate}
          isVisible={this.state.isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.toggleDatePicker}
        />
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <InputField
            tagName={"Quantity"}
            numberOfLines={1}
            inputContainer={{
              borderBottomColor:
                inputFocused === "Quantity"
                  ? this.state.transactionQtyStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.transactionQtyStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            }}
            placeholderTextColor={colors.steelGrey}
            onChangeText={(value) => this.setInputText(value, "Quantity")}
            value={transactionQty}
          />
          <View style={{ marginHorizontal: wp(1) }}></View>
          <InputField
            tagName={"Price"}
            numberOfLines={1}
            inputContainer={{
              borderBottomColor:
                inputFocused === "Price"
                  ? this.state.transactionPriceStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.transactionPriceStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            }}
            placeholderTextColor={colors.steelGrey}
            onChangeText={(value) => this.setInputText(value, "Price")}
            value={transactionPrice}
          />
        </View>
        {this.renderErrorText()}
        <SubmitButton
          buttonName="Add to Portfolio"
          onPress={() => (this.checkBtnStatus().status ? this.handleSubmit() : this.handleValidation())}
          btnBackground={{ backgroundColor: this.checkBtnStatus().color }}
        />
        {/* <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={'selectedStock'}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        /> */}
        <LoginModal ref="loginModal" {...this.props} />
        <Toast ref="toast" position="top" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </View>
    );
  };

  checkBtnStatus = () => {
    const {
      stockNameStatus,
      portfolioStatus,
      transactionDateStatus,
      transactionPriceStatus,
      transactionQtyStatus,
    } = this.state;
    if (
      this.checkForm(stockNameStatus) &&
      this.checkForm(portfolioStatus) &&
      this.checkForm(transactionDateStatus) &&
      this.checkForm(transactionQtyStatus) &&
      this.checkForm(transactionPriceStatus)
    ) {
      return {
        color: colors.primary_color,
        status: true,
      };
    } else {
      return {
        color: colors.greyishBrown,
        status: false,
      };
    }
  };

  checkForm = (entity) => {
    let flag = false;
    if (entity.status) flag = true;
    else {
      this.setState({ errorText: entity.error });
    }
    return flag;
  };

  handleValidation = () => {
    let txt;
    const { stockName, selectedPortFolio, transactionDate, transactionQty, transactionPrice } = this.state;
    if (stockName == "") {
      txt = "Please select any stock.";
      this.setState({ errorText: txt, stockNameStatus: { status: false, error: txt } });
    } else if (selectedPortFolio[0] === "*") {
      txt = "Please select a portfolio.";
      this.setState({
        errorText: txt,
        portfolioStatus: {
          status: false,
          error: txt,
        },
      });
    } else if (transactionDate.formetted === "Select Date") {
      txt = "Please select transaction date.";
      this.setState({
        errorText: txt,
        transactionDateStatus: {
          status: false,
          error: txt,
        },
      });
    } else if (transactionQty == "") {
      txt = "Please enter transaction quantity.";
      this.setState(
        {
          errorText: txt,
          transactionQtyStatus: {
            status: false,
            error: txt,
          },
        },
        () => {
          this.setState({ errorText: txt });
        }
      );
    } else if (transactionPrice === "") {
      txt = "Please enter transaction price.";
      this.setState(
        {
          errorText: txt,
          transactionPriceStatus: {
            status: false,
            error: txt,
          },
        },
        () => {
          this.setState({ errorText: txt });
        }
      );
    }
  };

  convertRequestDateObj = (normalDate) => {
    let date = new Date(normalDate.dateObj);
    let day = date.getDate();
    let year = date.getFullYear();
    let monthApi = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    // 2015-08-01
    let dateForApi = year + "-" + monthApi + "-" + day;
    return dateForApi;
  };

  handleSubmit = () => {
    const { token, baseURL, appToken } = this.props;
    const URL = baseURL + PORTFOLIO_ADD_HOLDING_URL;
    const { selectedPortFolio, stockId, transactionDate, transactionQty, transactionPrice } = this.state;
    let requestData = {
      portfolioId: selectedPortFolio[0],
      stock_id: stockId,
      transactionDate: this.convertRequestDateObj(transactionDate),
      qty: transactionQty,
      price: transactionPrice,
      notes: "source:mobileapp",
    };
    this.setState({
      isLoading: true,
    });
    this.setState({ isLoading: true }, () => {
      fetcher(
        URL,
        POST_METHOD,
        requestData,
        null,
        appToken,
        token,
        (json) => {
          const { body } = json;
          // console.log("Add holdings API response -->", body);
          this.refs.toast.show(body.msg);
          this.props.addHoldingsResponse("created");
          setTimeout(() => {
            this.refs.ActionPopUp.RBSheet.close();
          }, 300);
        },
        (message) => {
          this.props.addHoldingsResponse("failed");
          setTimeout(() => {
            this.refs.toast.show(message, ",Please try again");
          }, 300);

          this.setState({isLoading: false,});
          // console.log("Add holdings API response -->1", message);
        }
      );
    });
  };

  toggleDropdown = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "dropdown" ? "" : "dropdown";
    this.setState({
      showPortfolioList: !this.state.showPortfolioList,
      inputFocused: focusedInput,
      currentListToShow: "portfolio",
    });
  };

  renderPortfolioDropdown = () => {
    const inputBoxStyle =
      this.state.inputFocused == "dropdown" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];
    const dropdownIcon = this.state.showPortfolioList
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");
    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.portfolioStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          Select a Portfolio<Text style={{ color: "red" }}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDropdown}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.black, fontSize: RF(2.2) }]}>
            {this.state.selectedPortFolio[1]}
          </Text>
          <Image source={dropdownIcon} style={styles.inputIcon} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <StockAddBottomSheet
        ref="ActionPopUp"
        modalMainView={{ height: this.handlePopUpHeight() }}
        actionText={this.state.actionText}
        closeIconPress={() => this.handleCloseButton()}
        children={this.renderHoldindsActionChilds()}
      />
    );
  }
}

let mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolioData: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    loadPortfolio,
    updatePortfolio,
  },
  null,
  { forwardRef: true }
)(AddHoldings);

export const InputField = (props) => {
  const { tagName, inputContainer } = props;
  return (
    <View style={{ flex: 1 }}>
      <Text style={{ color: colors.greyishBrown }}>
        {tagName}
        <Text style={{ color: colors.red }}>*</Text>
      </Text>
      <View style={[styles.inputContainer, inputContainer]}>
        <TextInput style={[styles.textInput]} keyboardType="numeric" {...props} />
      </View>
    </View>
  );
};

export const SubmitButton = (props) => {
  const { btnBackground, btnTxt, buttonName } = props;
  return (
    <TouchableOpacity style={[styles.btnBackground, btnBackground]} {...props}>
      <Text style={[styles.btnTxt, btnTxt]}>{buttonName}</Text>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(4),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
  },
  inputContainer2: {
    paddingBottom: wp(1),
    marginBottom: wp(3),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: { borderBottomWidth: 2, borderBottomColor: colors.primary_color },
  inputRow: { flexDirection: "row", paddingTop: wp(2), paddingBottom: wp(0.8) },
  stockListItem: { paddingHorizontal: wp(3), paddingVertical: wp(3), color: colors.greyishBrown, fontSize: RF(2.2) },
  listItem: { padding: wp(3), color: colors.greyishBrown, fontSize: RF(2.2) },
  footerItem: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    borderTopWidth: 1,
    borderTopColor: "rgba(179, 179, 179,0.2)",
  },
  redText: { color: "red" },
  list: {
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    zIndex: 99,
    shadowOffset: { width: 0, height: 2 },
  },
  inputLabel: { fontSize: RF(1.9), color: colors.greyishBrown },
  btnBackground: { borderRadius: 5, marginTop: wp(5), backgroundColor: colors.primary_color },
  btnTxt: { padding: wp(2.5), textAlign: "center", fontSize: RF(2.2), color: colors.white },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(5),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(3),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
});
