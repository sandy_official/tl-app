import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, getFormattedDate, getFormattedDateMmDdYyyy } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
const searchIcon = require("../../assets/icons/search-error-black.png");
import EmptyView from "../../components/common/EmptyView";
import NoMatchView from "../../components/common/NoMatchView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";
export default class SearchStockResultsComponent extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      stocks: [],
      filteredData: ["NSEcode", "BSEcode", "page_type", "stock_id"],
    };
  }
  handleListItem = (stockData) => {
    this.props.clickResult(stockData);
  };

  renderDVM = (D, V, M) => {
    return (
      <View style={{ flexDirection: "row" }}>
        <View style={getDVMBackgroundColor(D)} />
        <View style={getDVMBackgroundColor(V)} />
        <View style={getDVMBackgroundColor(M)} />
      </View>
    );
  };
  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {this.renderText(value, "value")}
      </Text>
    );
  };

  renderText = (value, type) => {
    return value;
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {this.renderText(label, "label")}
      </Text>
    );
  };

  renderItemTags = ({ item, index }) => {
    let positionStyle = this.getItemPositionStyle(index);

    return (
      <View style={styles.detailRow}>
        {this.renderValue([styles.value, positionStyle], item.KeyValue)}
        {this.renderLabel([styles.label, positionStyle], item.KeyName)}
      </View>
    );
  };

  filterArray = (allKeysArray) => {
    //filter your key by providing props into the search component.
    const { filteredData } = this.state;
    console.log("===>", allKeysArray);
    return allKeysArray.filter((element) => filteredData.findIndex((ele) => ele === element.KeyName) > 0);
  };

  genrateList = (ItemObject) => {
    let tempArray = [];
    const objectArray = Object.entries(ItemObject);
    objectArray.forEach(([key, value]) => {
      tempArray.push({ KeyName: key, KeyValue: value });
    });
    tempArray = this.filterArray(tempArray);
    return tempArray;
  };
  renderListItem = ({ item, index }) => {
    // const { MainView } = this.props;
    // const { stocks } = this.state;
    const { get_full_name, NSEcode, BSEcode, d_color, v_color, m_color } = item;
    console.log("Item-->", item);
    const stockName = APIDataFormatter("get_full_name", get_full_name);
    const stockCode = NSEcode ? NSEcode : BSEcode;
    return (
      <TouchableOpacity style={styles.container} onPress={() => this.handleListItem(item)}>
        <View style={styles.nameCodeContainer}>
          <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
            {stockCode ? stockCode : "Stock Code"}
          </Text>
          <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
            {stockName ? stockName : "Stock Name"}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    console.log("--->", this.props.stocks);
    return (
      <View style={{ height: hp(70) }}>
        {this.props.stocks.length > 0 ? (
          <FlatList
            showsVerticalcrollIndicator={false}
            // style={{backgroundColor:'yellow'}}
            data={this.props.stocks}
            contentContainerStyle={{
              paddingHorizontal: wp(3),
            }}
            renderItem={this.renderListItem}
            showsVerticalScrollIndicator={false}
            //   keyExtractor={(item, index) => index.toString()}
            extraData={this.state.stocks}
            keyboardShouldPersistTaps="always"
          />
        ) : this.props.searchedResultsReady ? (
          <View style={{ height: hp(60) }}>
            <NoMatchView message="No Results found" />
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  MainView: {
    // justifyContent: 'center',
    marginVertical: "2%",
    // paddingVertical: wp(2),
    paddingVertical: wp(1),
    paddingHorizontal: wp(2),
    borderRadius: 10,
    backgroundColor: colors.whiteTwo,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    paddingVertical: 5,
    // backgroundColor:'yellow'
  },
  stockCode: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "500",
    // marginRight: wp(4)
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "500",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.6),
    color: colors.cement,
  },
  detailRow: {
    flex: 1,
    marginTop: wp(5),
  },
  emptyMainView: {
    height: hp(60),
    justifyContent: "center",
    alignItems: "center",
    // backgroundColor: 'cyan'
  },
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    // marginTop: hp(1.5),
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
  },
  nameCodeContainer: {
    flex: 2.2,
    paddingRight: 18,
  },
  code: {
    color: colors.primary_color,
    fontSize: RF(2.2),
    fontWeight: "300",
  },
  name: {
    fontSize: RF(1.9),
    marginTop: wp(2),
    color: colors.greyishBrown,
    opacity: 0.85,
  },
  price: {
    fontSize: RF(2.2),
    fontWeight: "500",
    marginBottom: 4,
    // marginLeft: 4
  },
});
