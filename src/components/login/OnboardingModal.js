import React from "react";
import { View, StyleSheet, Text, TouchableOpacity, Image, TouchableWithoutFeedback } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { connect } from "react-redux";
import { doGuestLogin } from "../../actions/UserActions";
import { navigationState } from "../../utils/Constants";

const features = [
  "Create watchlists",
  "Get a custom newsfeed",
  "Set custom alerts",
  "See real-time insights on your stocks",
  "View your portfolio analytics",
];

class OnboardingModal extends React.PureComponent {
  // componentDidMount() {
  //   this.RBSheet.open();
  // }

  closeDialog = () => {
    this.RBSheet.close();
  };

  gotoDashboard = () => {
    const { navigate } = this.props.navigation;

    this.closeDialog();
    this.props.doGuestLogin();
    if (navigationState) navigate(navigationState);
    else navigate("App");
  };

  renderInfo = () => {
    return (
      <View>
        <Text style={styles.description}>You can skip this step, but free signup lets you:</Text>

        {features.map((item, i) => {
          return (
            <View style={styles.featureRow} key={i.toString()}>
              <View style={styles.point} />
              <Text style={styles.feature}>{item}</Text>
            </View>
          );
        })}

        <TouchableOpacity style={styles.btn} onPress={() => this.gotoDashboard()}>
          <Text style={styles.btnText}>Continue Without Features</Text>
        </TouchableOpacity>
      </View>
    );
  };

  renderHeader = () => {
    const { openDashboard } = this.props;
    const crossIcon = require("../../assets/icons/cross-blue.png");

    return (
      <View style={styles.row}>
        <Text style={styles.title} numberOfLines={2} ellipsizeMode="tail">
          Start without the Best Features
        </Text>

        <TouchableOpacity onPress={() => this.closeDialog()}>
          <Image source={crossIcon} style={styles.crossIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    return (
      <RBSheet
        ref={(ref) => (this.RBSheet = ref)}
        height={hp(55)}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          {this.renderHeader()}
          {this.renderInfo()}
        </View>
      </RBSheet>
    );
  }
}

const mapStateToProps = (state) => ({
  userState: state.user,
  watchlist: state.watchlist.watchlist,
  activeWatchlist: state.watchlist.activeWatchlist,
  portfolio: state.portfolio.portfolio,
});

export default connect(mapStateToProps, { doGuestLogin }, null, {
  forwardRef: true,
})(OnboardingModal);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    // marginBottom: hp(10),
    borderTopLeftRadius: wp(6),
    borderTopRightRadius: wp(6),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    marginRight: wp(1),
  },
  crossIcon: {
    // width: wp(3),
    // height: wp(3),
    // padding: wp(3),
    resizeMode: "contain",
    margin: wp(2),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  description: {
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: wp(3),
  },
  featureRow: {
    flexDirection: "row",
    marginVertical: wp(1.6),
    alignItems: "center",
  },
  feature: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    marginLeft: wp(1.2),
  },
  point: {
    width: wp(1.4),
    height: wp(1.4),
    backgroundColor: colors.primary_color,
    borderRadius: wp(0.7),
  },
  btn: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginTop: wp(6),
    marginTop: wp(3),
    // zIndex: 10
  },
  btnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    padding: wp(2.2),
  },
});
