import React from "react";
import { View, StyleSheet, Platform, Image, Text, TextInput, TouchableOpacity } from "react-native";
import RF from "react-native-responsive-fontsize";
import { Header } from "react-navigation";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../styles/CommonStyles";

// import { Colors } from "../constants/Colors";

export class Toolbar extends React.PureComponent {
  componentWillUpdate(newProps) {
    if (newProps.showSearch && this.searchBoxRef) {
      this.refs.searchBoxRef.focus();
    }
  }

  focusSearchBar = () => {
    this.refs.searchBoxRef && this.refs.searchBoxRef.focus();
  };

  getToolbarWidth = (children) => {
    if (children == 1) return wp(100) - wp(20);
    else if (children == 2) return wp(100) - wp(38);
    else return wp(100) - wp(43);
  };

  renderToolbar = () => {
    const { navigation, children, sort, title, backButton, drawerMenu, customTitle } = this.props;

    const backButtonIcon =
      Platform.OS == "ios" ? require("../assets/icons/back-ios.png") : require("../assets/icons/back-android.png");

    const toolbarWidth = children != undefined && children.length ? this.getToolbarWidth(children.length) : wp(70);

    const toolbarTitleStyle =
      backButton || drawerMenu
        ? [styles.title, { width: toolbarWidth }]
        : [styles.title, { width: toolbarWidth, marginLeft: "5%" }];

    return (
      <View style={styles.toolbar}>
        {backButton && (
          <TouchableOpacity style={styles.navigationBtnContainer} onPress={() => navigation.pop()}>
            <Image source={backButtonIcon} resizeMethod="auto" resizeMode="contain" style={styles.backButton} />
          </TouchableOpacity>
        )}

        {drawerMenu && (
          <TouchableOpacity style={styles.navigationBtnContainer} onPress={() => navigation.openDrawer()}>
            <Image
              source={require("../assets/icons/hamburger.png")}
              resizeMethod="auto"
              resizeMode="contain"
              style={styles.drawerMenu}
            />
          </TouchableOpacity>
        )}

        {customTitle ? (
          <View style={[styles.title, { width: toolbarWidth - wp(3) }]}>{customTitle}</View>
        ) : (
          <Text style={toolbarTitleStyle} numberOfLines={2} ellipsizeMode="tail">
            {title}
          </Text>
        )}

        <View style={styles.toolbarRight}>{children}</View>
      </View>
    );
  };

  renderSearchBar = () => {
    const { hideSearch, onChangeText, searchPlaceholder, search } = this.props;
    return (
      <View style={styles.toolbar}>
        <TouchableOpacity style={styles.navigationBtnContainer} onPress={() => hideSearch()}>
          <Image
            source={
              Platform.OS == "ios"
                ? require("../assets/icons/back-ios.png")
                : require("../assets/icons/back-android.png")
            }
            resizeMethod="auto"
            resizeMode="contain"
            style={styles.backButton}
          />
        </TouchableOpacity>

        <TextInput
          ref="searchBoxRef"
          autoFocus={true}
          placeholder={searchPlaceholder ? searchPlaceholder : "Search"}
          numberOfLines={1}
          selectionColor="rgba(255,255,255,0.7)"
          placeholderTextColor="rgba(255,255,255,0.7)"
          style={styles.searchInput}
          onChangeText={(text) => onChangeText(text)}
          returnKeyType={search ? "search" : "done"}
          enablesReturnKeyAutomatically
          onSubmitEditing={() => search && search()}
          blurOnSubmit={false}
          // onEndEditing={this.focusSearchBar}
        />
      </View>
    );
  };

  render() {
    const { showSearch } = this.props;

    if (showSearch) return this.renderSearchBar();
    else return this.renderToolbar();
  }
}

export class TextButton extends React.PureComponent {
  render() {
    const { name, color, textStyle, buttonStyle, onPress } = this.props;
    // const { navigate } = this.props.navigation;

    return (
      <TouchableOpacity onPress={() => onPress()}>
        <View style={[styles.textButton, buttonStyle, { backgroundColor: color }]}>
          <Text style={[styles.buttonText, textStyle]}>{name}</Text>
        </View>
      </TouchableOpacity>
    );
  }
}

export class ImageButton extends React.PureComponent {
  render() {
    const { source, onPress, mainContainer } = this.props;
    return (
      <TouchableOpacity style={[mainContainer]} onPress={() => onPress()}>
        <Image source={source} style={[styles.menuItem, this.props.menuItem]} />
      </TouchableOpacity>
    );
  }
}

const styles = StyleSheet.create({
  toolbar: {
    width: undefined,
    height: Header.HEIGHT,
    flexDirection: "row",
    alignItems: "center",
    backgroundColor: colors.primary_color,
    paddingRight: "3%",
    paddingVertical: 8,
    overflow: "hidden",
    ...Platform.select({
      ios: {
        // shadowColor: Colors.toolbarShadow
        // shadowOffset: {
        //   width: 0,
        //   height: 0
        // },
        // shadowRadius: 6.7,
        // shadowOpacity: 1
      },
      android: {
        // elevation: 2
      },
    }),
  },
  toolbarRight: {
    flex: 1,
    flexDirection: "row",
    justifyContent: "flex-end",
    alignItems: "center",
  },
  title: {
    // flex:3,
    // maxWidth: wp(60),
    fontSize: RF(2.5), //2.7
    color: "#ffffff",
    // fontWeight: "500"
  },
  backButton: {
    width: wp(5),
    height: hp(2.5),
    // marginRight: wp(4)
  },
  drawerMenu: {
    width: wp(5.5),
    height: hp(5),
    marginRight: wp(4),
  },
  textButton: {
    borderRadius: wp(6),
    // backgroundColor: Colors.orange,
    paddingHorizontal: wp(3),
    paddingVertical: hp(1),
    justifyContent: "center",
    alignItems: "center",
    marginLeft: wp(3),
    minWidth: wp(18),
  },
  buttonText: {
    fontSize: RF(1.5),
    // color: Colors.white
  },
  menuItem: {
    // width: wp(5),
    // height: wp(7),
    paddingVertical: wp(5),
    marginHorizontal: wp(3),
    resizeMode: "contain",
  },
  hide: {
    display: "none",
  },
  searchInput: {
    flex: 1,
    fontSize: RF(2.7),
    color: "#ffffff",
    paddingBottom: 6,
    // marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    // marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    // marginLeft: Platform.OS == "android" ? -wp(1) : 0
  },
  navigationBtnContainer: {
    paddingVertical: wp(3),
    paddingRight: wp(3),
    paddingLeft: "5%",
  },
});
