import React from "react";
import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";

export default class SuperstarAlertNonSub extends React.PureComponent {
  open = () => this.RBSheet.open();

  gotoSubscription = () => {
    this.RBSheet.close();
    this.props.navigation.navigate("Subscription");
  };

  renderFeatureIcon = (icon, positionStyle, name) => {
    return (
      <View style={[styles.rowItem, positionStyle]}>
        <View style={styles.featureIconContainer}>
          <Image source={icon} style={styles.featureIcon} />
        </View>
        <Text style={[styles.featureName, styles.leftItem]}>{name}</Text>
      </View>
    );
  };

  renderFeaturesRow = () => {
    return (
      <View style={styles.featureContainer}>
        <View style={styles.featureRow}>
          {this.renderFeatureIcon(require("../../assets/icons/star-white.png"), styles.left, "Superstar\nAlerts")}

          {this.renderFeatureIcon(
            require("../../assets/icons/search-white-blue.png"),
            styles.centerItem,
            "Portfolio\nAnalysis"
          )}

          {this.renderFeatureIcon(require("../../assets/icons/dvm.png"), styles.rightItem, "DVM\nScores")}
        </View>
      </View>
    );
  };

  render() {
    const { navigate } = this.props.navigation;
    return (
      <RBSheet
        ref={ref => (this.RBSheet = ref)}
        height={hp(62)}
        duration={300}
        closeOnDragDown={true}
        customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
      >
        <View style={styles.infoContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.row}>
              <Text style={styles.title}>You seem like a Pro!</Text>

              <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.RBSheet.close()}>
                <Image source={require("../../assets/icons/cross-white.png")} style={styles.crossIcon} />
              </TouchableOpacity>
            </View>

            <Text style={styles.description}>Get exclusive features to help you invest better!</Text>

            {this.renderFeaturesRow()}

            <Text style={styles.planLabel}>And many more benefits starting from just</Text>

            <Text style={styles.planPrice}>
              <Text style={styles.rupeeSymbol}>&#8377;</Text> 246/month
            </Text>

            <TouchableOpacity onPress={this.gotoSubscription}>
              <View style={styles.plansButton}>
                <Text style={styles.plansButtonText}>See Plans</Text>
              </View>
            </TouchableOpacity>
          </ScrollView>
        </View>
      </RBSheet>
    );
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(8),
    paddingTop: wp(5)
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.white,
    marginBottom: wp(3),
    marginTop: hp(2)
    // paddingHorizontal: wp(4)
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  extraSpace: {
    marginBottom: hp(1)
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain"
  },
  featureRow: {
    flexDirection: "row"
    // justifyContent: "space-between",
  },
  rowItem: {
    flex: 1
  },
  featureContainer: {
    // flex: 1,
    marginVertical: hp(4),
    justifyContent: "center"
    // alignItems: "center"
  },
  featureIconContainer: {
    width: wp(19),
    height: wp(19),
    backgroundColor: "rgba(255,255,255,0.05)",
    borderRadius: wp(14),
    padding: wp(6),
    alignItems: "center",
    justifyContent: "center"
  },
  featureIcon: {
    width: wp(10),
    height: wp(8),
    resizeMode: "contain"
  },
  featureName: {
    flex: 1,
    width: wp(19),
    marginTop: 10,
    textAlign: "center",
    color: colors.white
  },
  description: {
    opacity: 0.8,
    fontSize: RF(2.2),
    color: colors.white,
    marginRight: wp(4)
  },
  leftItem: {
    alignItems: "flex-start"
  },
  centerItem: {
    alignItems: "center"
  },
  rightItem: {
    alignItems: "flex-end"
  },
  planLabel: {
    fontSize: RF(2.2),
    color: colors.white
  },
  planPrice: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.white,
    textAlign: "center",
    marginTop: 2
  },
  rupeeSymbol: {
    fontSize: RF(3.1)
  },
  plansButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.white,
    paddingVertical: wp(3),
    marginTop: hp(3)
  },
  plansButtonText: {
    fontSize: RF(2.2),
    color: colors.white
  }
});
