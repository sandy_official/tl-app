import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ScrollView } from "react-native-gesture-handler";
import { getObjectListFromAsync, storeObjectListInAsync } from "../../controllers/AsyncController";
import { RECENT_SUPERSTAR_SEARCH } from "../../utils/Constants";
import { Toolbar } from "../Toolbar";
import LoadingView from "../common/LoadingView";
import { connect } from "react-redux";
import { loadSuperstar } from "../../actions/SuperstarActions";

class SuperstarSearch extends React.PureComponent {
  componentDidMount() {
    const { superstars, loadSuperstar, userState, baseURL, appToken } = this.props;

    getObjectListFromAsync(RECENT_SUPERSTAR_SEARCH, (data) => {
      data && this.setState({ recentSearch: data });
    });

    if (superstars.length == 0) loadSuperstar(baseURL, appToken, userState.token);
  }

  constructor(props) {
    super(props);

    this.state = {
      expandSuperstars: false,
      expandPopular: false,
      recentSearch: [],
      searchQuery: "",
    };
  }

  isDuplicateObject = (superstar) => {
    let recentSearches = this.state.recentSearch;
    let superstarList = [];

    Object.keys(recentSearches).map((key) => {
      let superstarObject = recentSearches[key];
      let superstarName = typeof superstarObject == "string" ? superstarObject : superstarObject["Superstar name"];

      superstarList.push(superstarName);
    });

    let isDuplicate =
      typeof superstar == "string"
        ? superstarList.includes(superstar)
        : superstarList.includes(superstar["Superstar name"]);

    this.setState({ recentSearch: isDuplicate ? recentSearches : [...recentSearches, superstar] });

    return isDuplicate;
  };

  renderToolbar = () => {
    const { navigation } = this.props;

    return (
      <Toolbar
        title="Superstars"
        backButton
        {...this.props}
        searchPlaceholder={"Search superstar or investor"}
        showSearch
        hideSearch={() => navigation.pop()}
        onChangeText={(query) => this.setState({ searchQuery: query })}
        search={() => this.openSuperstar(this.state.searchQuery)}
      />
    );
  };

  gotoSuperstar = (obj) => {
    const { navigation } = this.props;
    this.setState({ showSearch: false });
    navigation.push("SuperstarInfo", obj);
  };

  openSuperstar = (superstar) => {
    if (!this.isDuplicateObject(superstar)) storeObjectListInAsync(RECENT_SUPERSTAR_SEARCH, superstar);

    let superstarObj =
      superstar instanceof Object
        ? { superstarId: superstar.superstarId, superstarName: superstar["Superstar name"], superstarString: "" }
        : { superstarId: -1, superstarName: superstar, superstarString: superstar };

    this.gotoSuperstar(superstarObj);
  };

  renderListHeader = (title) => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>{title ? title : "Most popular searches"}</Text>
      </View>
    );
  };

  expandSearchList = (type) => {
    if (type == "popular") this.setState({ expandPopular: true });
    else this.setState({ expandSuperstars: true });
  };

  renderListFooter = (type, data) => {
    const { expandPopular, expandSuperstars } = this.state;
    const showAllButton = type == "popular" ? expandPopular : expandSuperstars;

    if (!showAllButton && data.length > 5) {
      return (
        <TouchableOpacity style={styles.center} onPress={() => this.expandSearchList(type)}>
          <View style={styles.loadMoreBtn}>
            <Text style={styles.loadMoreBtnText}>See all {data.length}</Text>
          </View>
        </TouchableOpacity>
      );
    } else return null;
  };

  renderListItem = ({ item, index }) => {
    const extraMargin = index == 0 ? styles.extraTopMargin : null;
    const superstarName = item["Superstar name"] ? item["Superstar name"] : item;

    return (
      <TouchableOpacity onPress={() => this.openSuperstar(item)}>
        <View style={[styles.listItemContainer, extraMargin]}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.listItem}>
            {superstarName}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  searchForSuperstar = (superstars) => {
    const { searchQuery } = this.state;
    let newSuperstarList = [];

    superstars.forEach((obj) => {
      let superstar = obj["Superstar name"].toLowerCase();
      if (~superstar.indexOf(searchQuery.toLowerCase())) newSuperstarList.push(obj);
    });
    // console.log(newSuperstarList.splice(0, 5))
    return newSuperstarList;
  };

  renderSearchList = (type, title, dataList) => {
    const { expandPopular, expandSuperstars } = this.state;
    const data = JSON.parse(JSON.stringify(dataList));
    let newList = [];

    if (data.length > 0) {
      if (type == "popular") newList = expandPopular ? data : data.splice(0, 5);
      else newList = expandSuperstars ? data : data.splice(0, 5);

      return (
        <FlatList
          contentContainerStyle={styles.list}
          data={newList}
          // extraData={this.props}
          renderItem={this.renderListItem}
          ListHeaderComponent={() => this.renderListHeader(title)}
          ListFooterComponent={() => this.renderListFooter(type, data)}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
          keyboardShouldPersistTaps="always"
        />
      );
    }
  };

  renderSingleItem = () => {
    const { searchQuery } = this.state;

    return (
      <TouchableOpacity onPress={() => this.openSuperstar(searchQuery)}>
        <View style={[styles.listItemContainer, styles.whiteBackground]}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.listItem}>
            {searchQuery}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderQueryStringSearch = () => {
    const { searchQuery } = this.state;

    if (searchQuery.length > 0) {
      return (
        <View>
          {this.renderListHeader("Search by name")}
          {this.renderSingleItem()}
        </View>
      );
    }
  };

  renderRecentSearchList = () => {
    const { recentSearch } = this.state;

    if (recentSearch && recentSearch.length > 0)
      return (
        <View>
          <FlatList
            contentContainerStyle={styles.list}
            data={recentSearch}
            extraData={recentSearch}
            renderItem={this.renderListItem}
            ListHeaderComponent={() => this.renderListHeader("Recent")}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            keyboardShouldPersistTaps="always"
          />
        </View>
      );
  };

  renderRecentPopularList = () => {
    return (
      <View>
        {this.renderRecentSearchList()}
        {this.renderSearchList("popular", "Most popular searches", this.props.popular)}
      </View>
    );
  };

  render() {
    const { searchQuery } = this.state;
    const { superstars } = this.props;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        {superstars.length == 0 && <LoadingView />}

        {superstars.length > 0 && (
          <ScrollView keyboardShouldPersistTaps="always">
            {searchQuery.length > 0
              ? this.renderSearchList("superstar", "Superstar(s)", this.searchForSuperstar(superstars))
              : this.renderRecentPopularList()}

            {this.renderQueryStringSearch()}
          </ScrollView>
        )}
      </View>
    );
  }
}

mapStateToProps = (state) => {
  const { popular, individual, institutional, fii } = state.superstar.superstars;
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    superstars: [...individual, ...institutional, ...fii],
    popular: popular,
  };
};

export default connect(mapStateToProps, { loadSuperstar })(SuperstarSearch);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingBottom: hp(4),
    backgroundColor: colors.whiteTwo,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  list: {
    paddingBottom: ifIphoneX ? hp(2) : 0,
    backgroundColor: colors.white,
  },
  listItemContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(3),
  },
  listItem: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    backgroundColor: colors.whiteTwo,
  },
  headerText: {
    opacity: 0.85,
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.greyish,
    alignItems: "center",
    justifyContent: "center",
  },
  loadMoreBtn: {
    width: wp(40),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(2.5),
    marginTop: hp(2),
  },
  loadMoreBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    textAlign: "center",
  },
  center: {
    alignItems: "center",
    justifyContent: "center",
  },
  whiteBackground: {
    backgroundColor: colors.white,
  },
  extraTopMargin: {
    marginTop: wp(2),
  },
});
