import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, formatNumber, numberWithCommas, formatQuantity } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ICON_CDN } from "../../utils/Constants";
import StockActionsView from "../common/StockActionsView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class InsiderTradedCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      alertActive: false,
      portfolioActive: false,
    };
  }

  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.get_full_name,
      pageType: stock.page_type,
      page: stock.page_type == "Equity" ? "deals" : "overview",
      stockCode : stockCode,
    });
  };

  renderDVM = () => {
    const { stock } = this.props;

    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.d_color)} />
        <View style={getDVMBackgroundColor(stock.v_color)} />
        <View style={getDVMBackgroundColor(stock.m_color)} />
      </View>
    );
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {value}
      </Text>
    );
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderStockRows = () => {
    const { stock } = this.props;

    const averagePrice = APIDataFormatter("average_price", stock.average_price);
    const holdingChange = APIDataFormatter("holding_change", stock.holding_change);
    const holdingChangePercent = APIDataFormatter("holdingP_change", stock.holdingP_change);

    return (
      <View style={styles.stockDetails}>
        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.left], holdingChange)}
          {this.renderLabel([styles.label, styles.left], "Quantity")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.center], averagePrice)}
          {this.renderLabel([styles.label, styles.center], "Avg. Price")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.right], holdingChangePercent)}
          {this.renderLabel([styles.label, styles.right], "%Traded")}
        </View>
      </View>
    );
  };

  renderButtonRow = () => {
    const { stock, watchlistBtnState } = this.props;
    const { alertBtnActive, portfolioActive } = this.state;

    return (
      <StockActionsView
        {...this.props}
        stock={stock}
        alertActive={alertBtnActive}
        portfolioActive={portfolioActive}
        watchlistBtnState={watchlistBtnState}
      />
    );
  };

  renderOtherDetails = () => {
    const { stock } = this.props;

    const transactionStyle =
      stock.transaction_type_text == "Acquisition" ? styles.equityStatus : [styles.equityStatus, { color: colors.red }];

    const sastStyle =
      stock.get_sast_type_display == "SAST" ? [styles.shareType, styles.sastText] : [styles.shareType, styles.itText];

    const securityType = APIDataFormatter("security_type_text", stock.security_type_text);

    const transactionType = " " + APIDataFormatter("transaction_type_text", stock.transaction_type_text);

    const stockActor = APIDataFormatter("actor", stock.actor);
    const sastType = stock.get_sast_type_display == "SAST" ? "SAST" : "IT";
    const stockMode = stock.mode ? stock.mode : " - ";
    const reportDate = "   " + APIDataFormatter("report_date", stock.report_date);

    return (
      <View style={styles.otherDetails}>
        <View style={styles.otherDetailRow}>
          <Text style={styles.equityShare}>
            {securityType}
            <Text style={transactionStyle}>{transactionType}</Text>
          </Text>
          <Text style={sastStyle}>
            {sastType}
            <Text style={styles.tradeDate}>{reportDate}</Text>
          </Text>
        </View>

        <View style={styles.otherDetailRow}>
          <Text style={styles.superstarName} numberOfLines={1} ellipsizeMode="tail">
            {stockActor}
          </Text>
          <Text style={styles.marketGift}>{stockMode}</Text>
        </View>
      </View>
    );
  };

  render() {
    const { stock } = this.props;

    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
          <View style={styles.StockCodeContainer}>
            <TouchableOpacity style={{ flex: 1 }} onPress={this.gotoStockDetails}>
              <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
                {stock.NSEcode ? stock.NSEcode : stock.BSEcode}
              </Text>
            </TouchableOpacity>
            {this.renderDVM()}
          </View>

          {this.renderOtherDetails()}
          {this.renderStockRows()}

          {this.state.expand && this.renderButtonRow()}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: wp(2.2),
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 0,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
    // marginBottom: hp(3)
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "200",
    marginRight: wp(4),
  },
  detailRow: {
    flex: 1,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: wp(2),
    marginBottom: 4,
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 4,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "200",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
  },
  // buttonRow: {
  //   flexDirection: "row",
  //   marginTop: hp(2.5),
  //   marginBottom: hp(1)
  // },
  // buttonContainer: {
  //   flex: 1
  // },
  // button: {
  //   height: hp(4),
  //   flexDirection: "row",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 5,
  //   borderWidth: 1,
  //   borderColor: colors.primary_color
  // },
  // button: {
  //   height: hp(4),
  //   flexDirection: "row",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 5,
  //   borderWidth: 1,
  //   borderColor: colors.primary_color
  // },
  // buttonIcon: {
  //   width: wp(3),
  //   height: wp(3),
  //   resizeMode: "contain"
  // },
  // buttonText: {
  //   fontSize: RF(2.2),
  //   color: colors.primary_color,
  //   marginLeft: 5
  // },
  // buttonTextActive: {
  //   fontSize: RF(2.2),
  //   color: colors.primary_color,
  //   marginLeft: 5
  // },
  hide: {
    display: "none",
  },
  stockDetails: {
    flexDirection: "row",
  },
  otherDetails: {
    marginVertical: 2,
  },
  otherDetailRow: {
    flexDirection: "row",
    marginVertical: 1.5,
  },
  equityShare: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    // fontWeight: "100"
  },
  equityStatus: {
    fontWeight: "300",
    color: colors.green,
  },
  shareType: {
    fontSize: RF(1.5),
    fontWeight: "500",
  },
  sastText: {
    color: colors.hotMagenta,
  },
  itText: {
    color: colors.blueViolet,
  },
  tradeDate: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  superstarName: {
    flex: 1,
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginRight: wp(10),
  },
  marketGift: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
});
