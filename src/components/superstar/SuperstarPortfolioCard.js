import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, formatNumberToInr, roundNumber } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ICON_CDN } from "../../utils/Constants";
import StockActionsView from "../common/StockActionsView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class SuperstarPortfolioCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      alertActive: false,
      portfolioActive: false,
    };
  }

  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    
    console.log("superstar portfolio stock", stock);
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.full_name,
      pageType: stock.page_type,
      stockCode: stockCode,
    });
  };

  renderDVM = () => {
    const { stock } = this.props;

    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.d_color)} />
        <View style={getDVMBackgroundColor(stock.v_color)} />
        <View style={getDVMBackgroundColor(stock.m_color)} />
      </View>
    );
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {value}
      </Text>
    );
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderStockRows = () => {
    const { stock } = this.props;

    return (
      <View style={styles.stockDetails}>
        <View style={styles.leftRow}>
          {this.renderValue([styles.value, styles.left], this.getFormattedHoldingValue(stock))}
          {this.renderLabel([styles.label, styles.left], "Holding Value")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.center], stock["Current Price (Rs.)"])}
          {this.renderLabel([styles.label, styles.center], "Current Price")}
        </View>

        <View style={styles.RightRow}>
          {this.renderValue(
            [styles.value, styles.right, this.getQtrChangeTextStyle(stock["Change from Previous Qtr"])],
            isNaN(stock["Change from Previous Qtr"])
              ? stock["Change from Previous Qtr"]
              : roundNumber(stock["Change from Previous Qtr"]) + "%"
          )}
          {this.renderLabel([styles.label, styles.right], "%Change")}
        </View>
      </View>
    );
  };

  getFormattedHoldingValue = (stock) => {
    if (stock["Holding Value (Rs.)"]) {
      let holdingValue = APIDataFormatter("Holding Value (Rs.)", stock["Holding Value (Rs.)"]);

      let holdingPercent = APIDataFormatter("Holding Percent", stock["Holding Percent"]);

      return (
        <Text>
          {holdingValue} <Text style={styles.greyText}>({holdingPercent} )</Text>
        </Text>
      );
    } else return <Text>-</Text>;
  };

  getQtrChangeTextStyle = (value) => {
    if (isNaN(value)) {
      if (value == "Below 1% First Time") return { color: colors.red };
      //red
      else if (value == "Filing Awaited" || value == "Filing awaited for current qtr") return { color: colors.black };
      //grey/black
      else if (value == "NEW" || value == "New") return { color: colors.green }; //green
    } else {
      if (value < 0) return { color: colors.red };
      else return { color: colors.green };
    }
  };

  renderButtonRow = (stock) => {
    const { alertBtnActive, portfolioActive } = this.state;
    const { watchlistBtnState } = this.props;

    return (
      <StockActionsView
        {...this.props}
        stock={stock}
        alertActive={alertBtnActive}
        portfolioActive={portfolioActive}
        watchlistBtnState={watchlistBtnState}
      />
    );
  };

  renderOtherDetails = () => {
    const { stock, showHistory } = this.props;

    return (
      <View style={styles.otherDetailRow}>
        <Text style={styles.superstarName} numberOfLines={1} ellipsizeMode="tail">
          {stock.full_name}
        </Text>
        <TouchableOpacity onPress={() => showHistory(stock)}>
          <Text style={styles.showHistory}>Show History</Text>
        </TouchableOpacity>
      </View>
    );
  };

  formatHolderList = (holders) => {
    // console.log("Holders", holders);
    // let holderList = holders.split("&");
    // return holderList.length > 1
    //   ? holderList[0] + " +" + (holderList.length - 1) + " more"
    //   : holderList[0];
    return holders;
  };

  renderHolders = () => {
    const { stock } = this.props;
    let holders = APIDataFormatter("Holders Name", stock["Holders Name"]);

    return (
      <View style={styles.otherDetailRow}>
        <Text style={styles.superstarName} numberOfLines={1} ellipsizeMode="tail">
          {holders}
        </Text>
      </View>
    );
  };

  render() {
    const { stock } = this.props;

    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
          <View style={styles.StockCodeContainer}>
            <TouchableOpacity style={{ flex: 1 }} onPress={this.gotoStockDetails}>
              <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
                {stock.NSEcode ? stock.NSEcode : stock.BSEcode}
              </Text>
            </TouchableOpacity>
            {this.renderDVM()}
          </View>

          {this.renderOtherDetails()}
          {this.renderHolders()}
          {this.renderStockRows()}

          {this.state.expand && this.renderButtonRow(stock)}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: "1%",
    padding: "2%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
    marginBottom: wp(0.5),
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "300",
    marginRight: wp(4),
  },
  detailRow: {
    flex: 1,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: hp(2.2),
    marginBottom: 8,
  },
  leftRow: {
    flex: 1.6,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: hp(2.2),
    marginBottom: 8,
  },
  RightRow: {
    flex: 1.3,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: hp(2.2),
    marginBottom: 8,
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 4,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1.4,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "300",
    marginBottom: 6,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  greyText: {
    color: colors.steelGrey,
  },
  // buttonRow: {
  //   flexDirection: "row",
  //   marginTop: hp(2.5),
  //   marginBottom: hp(1)
  // },
  // buttonContainer: {
  //   flex: 1
  // },
  // button: {
  //   height: hp(4),
  //   flexDirection: "row",
  //   alignItems: "center",
  //   justifyContent: "center",
  //   borderRadius: 5,
  //   borderWidth: 1,
  //   borderColor: colors.primary_color
  // },
  // buttonIcon: {
  //   width: wp(3),
  //   height: wp(3),
  //   resizeMode: "contain"
  // },
  // buttonText: {
  //   fontSize: RF(2.2),
  //   color: colors.primary_color,
  //   marginLeft: 5
  // },
  hide: {
    display: "none",
  },
  stockDetails: {
    flexDirection: "row",
    marginVertical: 2,
    marginTop: wp(0.5),
  },
  otherDetailRow: {
    flexDirection: "row",
    marginVertical: 1,
    marginBottom: wp(0.5),
  },
  superstarName: {
    flex: 1,
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginRight: wp(4),
  },
  showHistory: {
    fontSize: RF(1.5),
    color: colors.primary_color,
    paddingLeft: wp(1),
    paddingBottom: wp(1),
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
});
