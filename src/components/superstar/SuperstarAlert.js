import React from "react";
import { View, StyleSheet, Text, ScrollView, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { setSuperstarAlerts } from "../../controllers/SuperstarAlertsController";
import { ICON_CDN } from "../../utils/Constants";
import { connect } from "react-redux";

class SuperstarAlert extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isRemoved: null };
  }

  open = () => {
    this.RBSheet.open();
  };

  setAlerts = (alertType) => {
    const { userState, baseURL, appToken } = this.props;
    setSuperstarAlerts(baseURL, appToken, userState.token, alertType, (message) => console.log(message));
  };

  changeAlerts = (type) => {
    const { buttonState, setButtonState, showPlans, userState } = this.props;

    // if (isUserSubscribed()) {
    switch (type) {
      case "daily":
        this.setState({ isRemoved: !!buttonState.daily });
        setButtonState({ daily: !buttonState.daily, weekly: false });
        this.setAlerts("daily");
        break;

      case "weekly":
        this.setState({ isRemoved: !!buttonState.weekly });
        setButtonState({ daily: false, weekly: !buttonState.weekly });
        this.setAlerts("weekly");
        break;

      case "remove":
        this.setState({ isRemoved: true });
        setButtonState({ daily: false, weekly: false });
        this.setAlerts("");
        break;
    }
    // }
    // else {
    //   showPlans();
    //   // this.RBSheet.close();
    // }
  };

  closeDialog = () => {
    this.props.close(this.state.isRemoved);
    this.setState({ isRemoved: null });
    this.RBSheet.close();
  };

  render() {
    const { buttonState, setButtonState } = this.props;

    const dailyButtonStyle = buttonState.daily ? styles.buttonActive : styles.button;
    const dailyButtonTextStyle = buttonState.daily ? styles.buttonTextActive : styles.buttonText;

    const weeklyButtonStyle = buttonState.weekly ? styles.buttonActive : styles.button;
    const WeeklyButtonTextStyle = buttonState.weekly ? styles.buttonTextActive : styles.buttonText;

    const removeButtonStyle =
      buttonState.daily || buttonState.weekly ? [styles.removeAlert, styles.redText] : styles.removeAlert;

    const removeButtonState = buttonState.daily || buttonState.weekly ? false : true;

    return (
      <RBSheet
        ref={(ref) => {
          this.RBSheet = ref;
        }}
        height={ifIphoneX ? hp(26) : hp(24)}
        duration={300}
        closeOnDragDown={true}
        customStyles={{
          container: {
            borderTopLeftRadius: wp(6),
            borderTopRightRadius: wp(6),
          },
        }}
      >
        <View style={styles.infoContainer}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.row}>
              <Text style={styles.title}>Superstar Alerts</Text>

              <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
                <Image source={require("../../assets/icons/cross-blue.png")} style={styles.crossIcon} />
              </TouchableOpacity>
            </View>

            <Text style={styles.infoDetails}>
              This single alert tracks all superstar shareholding changes and bulk block deals
            </Text>

            <View style={styles.extraSpace} />

            <View style={styles.row}>
              <View style={styles.buttonRow}>
                <TouchableOpacity onPress={() => this.changeAlerts("daily")}>
                  <View style={dailyButtonStyle}>
                    <Text style={dailyButtonTextStyle}>Daily</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.changeAlerts("weekly")}>
                  <View style={weeklyButtonStyle}>
                    <Text style={WeeklyButtonTextStyle}>Weekly</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <TouchableOpacity onPress={() => this.changeAlerts("remove")} disabled={removeButtonState}>
                <Text style={removeButtonStyle}>Remove Alert</Text>
              </TouchableOpacity>
            </View>
          </ScrollView>
        </View>
      </RBSheet>
    );
  }
}

mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(SuperstarAlert);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
    // paddingHorizontal: wp(4)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4),
    // paddingHorizontal: wp(4)
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  button: {
    width: wp(22),
    paddingVertical: wp(2),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
  },
  buttonActive: {
    width: wp(22),
    paddingVertical: wp(2),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  buttonTextActive: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  removeAlert: {
    fontSize: RF(2.2),
    color: colors.steelGrey,
    paddingVertical: wp(2),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  redText: {
    color: colors.red,
  },
});
