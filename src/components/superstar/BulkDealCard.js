import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, FlatList } from "react-native";
import { getDVMBackgroundColor, formatNumber, numberWithCommas, formatQuantity } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ICON_CDN } from "../../utils/Constants";
import StockActionsView from "../common/StockActionsView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

export default class BulkDealCard extends React.PureComponent {
  // componentDidMount() {
  //   this.setState({ alertBtnActive: this.props.WatchlistBtnState });
  // }
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      alertActive: false,
      portfolioActive: false,
    };
  }

  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    console.log("deals stock", stock);
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.get_full_name,
      pageType: stock.page_type,
      page: stock.page_type == "Equity" ? "deals" : "overview",
      stockCode: stockCode,
    });
  };

  renderDVM = () => {
    const { stock } = this.props;

    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.d_color)} />
        <View style={getDVMBackgroundColor(stock.v_color)} />
        <View style={getDVMBackgroundColor(stock.m_color)} />
      </View>
    );
  };

  renderValue = (textStyle, value) => {
    return (
      <Text style={textStyle} numberOfLines={1} ellipsizeMode="tail">
        {value}
      </Text>
    );
  };

  renderLabel = (textStyle, label) => {
    return (
      <Text style={textStyle} numberOfLines={2} ellipsizeMode="tail">
        {label}
      </Text>
    );
  };

  getItemPositionStyle = (index) => {
    if (index % 3 == 0) return styles.left;
    else if (index % 3 == 1) return styles.center;
    else if (index % 3 == 2) return styles.right;
    else return styles.left;
  };

  renderStockRows = (item, index) => {
    const { stock } = this.props;
    const stockQuantity = APIDataFormatter("quantity", stock.quantity);
    const stockPrice = APIDataFormatter("price", stock.price);
    const stockPercentChange = APIDataFormatter("traded_percent", stock.traded_percent);

    const stockExchange = stock.exchange ? "(" + stock.exchange + ")" : " - ";
    const quantityExchange = stockQuantity + " " + stockExchange;

    return (
      <View style={styles.stockDetails}>
        <View style={[styles.detailRow, { flex: 1.4 }]}>
          {this.renderValue([styles.value, styles.left], quantityExchange)}
          {this.renderLabel([styles.label, styles.left], "Quantity")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.center], stockPrice)}
          {this.renderLabel([styles.label, styles.center], "Price")}
        </View>

        <View style={styles.detailRow}>
          {this.renderValue([styles.value, styles.right], stockPercentChange)}
          {this.renderLabel([styles.label, styles.right], "%Traded")}
        </View>
      </View>
    );
  };

  renderButton = (label, positionStyle, icon, isActive, onPress) => {
    const btnStyle = isActive ? [styles.button, styles.buttonActive] : styles.button;

    const textStyle = isActive ? [styles.buttonText, styles.whiteText] : styles.buttonText;

    return (
      <View style={[positionStyle, styles.buttonContainer]}>
        <TouchableOpacity onPress={() => onPress()}>
          <View style={btnStyle}>
            <Image source={icon} style={styles.buttonIcon} />
            <Text style={textStyle}>{label}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  renderButtonRow = () => {
    const { stock, watchlistBtnState } = this.props;
    const { alertActive, portfolioActive } = this.state;

    return (
      <StockActionsView
        {...this.props}
        stock={stock}
        alertActive={alertActive}
        portfolioActive={portfolioActive}
        watchlistBtnState={watchlistBtnState}
      />
    );
  };

  renderOtherDetails = () => {
    const { stock } = this.props;

    const stockActionStyle =
      stock.get_action_display == "Sell" ? [styles.equityStatus, styles.redText] : styles.equityStatus;

    const stockAction = stock.get_action_display;

    const clientName = APIDataFormatter("client_name", stock.client_name);
    const stockDate = APIDataFormatter("date", stock.date);
    const dealType = APIDataFormatter("get_deal_type_display", stock.get_deal_type_display) + "  ";

    return (
      <View style={styles.otherDetails}>
        <View style={styles.otherDetailRow}>
          <Text style={styles.equityShare}>
            {dealType}
            <Text style={stockActionStyle}>{stockAction}</Text>
          </Text>

          <Text style={styles.tradeDate}>{stockDate}</Text>
        </View>

        <View style={styles.otherDetailRow}>
          <Text style={styles.superstarName} numberOfLines={1} ellipsizeMode="tail">
            {clientName}
          </Text>
          {/* <Text style={styles.marketGift}>Market/Gift</Text> */}
        </View>
      </View>
    );
  };

  render() {
    const { stock } = this.props;
    // console.log("Bulk block data", stock);

    return (
      <View style={styles.card}>
        <TouchableOpacity onPress={() => this.setState({ expand: !this.state.expand })}>
          <View style={styles.StockCodeContainer}>
            <TouchableOpacity style={{ flex: 1 }} onPress={this.gotoStockDetails}>
              <Text style={styles.stockCode} numberOfLines={1} ellipsizeMode="tail">
                {stock.NSEcode ? stock.NSEcode : stock.BSEcode}
              </Text>
            </TouchableOpacity>
            {this.renderDVM()}
          </View>

          {this.renderOtherDetails()}
          {this.renderStockRows()}

          {this.state.expand && this.renderButtonRow(stock)}
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: wp(2.2),
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
    // marginBottom: hp(3)
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "200",
    marginRight: wp(4),
  },
  detailRow: {
    flex: 1,
    //  minHeight:50
    // flexDirection:'column'
    marginTop: wp(2),
    marginBottom: 8,
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 4,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(3),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.1),
    fontWeight: "200",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
  },
  hide: {
    display: "none",
  },
  stockDetails: {
    flexDirection: "row",
  },
  otherDetails: {
    marginVertical: 2,
  },
  otherDetailRow: {
    flexDirection: "row",
    marginVertical: 1.5,
  },
  equityShare: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    // fontWeight: "100"
  },
  equityStatus: {
    fontWeight: "200",
    color: colors.green,
  },
  tradeDate: {
    fontSize: RF(1.5),
    color: colors.greyishBrown,
  },
  superstarName: {
    flex: 1,
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginRight: wp(10),
    // textTransform: "capitalize"
    // textTransform: "",
    //stextTransform: "lowercase"
  },
  marketGift: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
  },
  buttonActive: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
  redText: { color: colors.red },
});
