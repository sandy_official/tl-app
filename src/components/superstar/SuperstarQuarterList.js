import React from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  FlatList,
  Image
} from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import RBSheet from "react-native-raw-bottom-sheet";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ICON_CDN } from "../../utils/Constants";

export default class SuperstarQuarterList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isChange: false, selectedQuarter: "" };
  }

  open = quarter => {
    console.log("current quarter", quarter);
    this.setState({ selectedQuarter: quarter });
    this.RBSheet.open();
  };

  closeDialog = () => {
    //   // this.props.onPress(this.state.isChange);
    this.RBSheet.close();
  };

  setSelectedOption = item => {
    this.setState({ selectedQuarter: item });
    this.props.onPress(item);
    this.RBSheet.close();
  };

  renderListItem = ({ item, index }) => {
    const { selectedQuarter } = this.state;
    console.log(selectedQuarter, item);

    const isSelected = selectedQuarter === item[1] || selectedQuarter === item[0] || selectedQuarter[1] === item[1];
    const radioButtonContainerStyle = isSelected ? [styles.radioButton, styles.radioButtonActive] : styles.radioButton;
    const radioButtonStyle = isSelected ? styles.radioButtonCircle : styles.hide;

    return (
      <View>
        <TouchableOpacity onPress={() => this.setSelectedOption(item)}>
          <View style={styles.quarterContainer}>
            <View style={radioButtonContainerStyle}>
              <View style={radioButtonStyle} />
            </View>
            <Text style={styles.quarterOption}>{item[0]}</Text>
          </View>
        </TouchableOpacity>
      </View>
    );
  };

  render() {
    const { data } = this.props;

    // const selectButtonStyle = this.state.selectedQuarter
    //   ? styles.selectButton
    //   : [styles.selectButton, styles.selectButtonDisabled];

    // const selectBtnDisabled = this.state.selectedQuarter ? false : true;

    const crossIcon = require("../../assets/icons/cross-blue.png");

    if (data && data.length > 0) {
      return (
        <RBSheet
          ref={ref => (this.RBSheet = ref)}
          height={hp(95)}
          duration={300}
          closeOnDragDown={true}
          customStyles={{ container: { borderTopLeftRadius: wp(6), borderTopRightRadius: wp(6) } }}
        >
          <View style={styles.infoContainer}>
            <View style={styles.row}>
              <Text style={styles.title}>Quarter Ending</Text>

              <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
                <Image source={crossIcon} style={styles.crossIcon} />
              </TouchableOpacity>
            </View>

            <FlatList
              showsVerticalScrollIndicator={false}
              contentContainerStyle={styles.list}
              data={data}
              extraData={this.state}
              renderItem={this.renderListItem}
              keyExtractor={(item, index) => index.toString()}
            />
          </View>

          {/* <TouchableWithoutFeedback onPress={() => this.setSelectedOption()} disabled={selectBtnDisabled}>
            <View style={selectButtonStyle}>
              <Text style={styles.selectButtonText}>Select Quarter</Text>
            </View>
          </TouchableWithoutFeedback> */}
        </RBSheet>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(8),
    paddingTop: wp(5),
    marginBottom: hp(10)
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2)
    // paddingHorizontal: wp(4)
  },
  infoDetails: {
    opacity: 0.85,
    fontSize: RF(2),
    color: colors.black,
    marginBottom: wp(4)
    // paddingHorizontal: wp(4)
  },
  extraSpace: {
    marginBottom: hp(1)
  },
  row: {
    height: hp(8),
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  quarterContainer: {
    flexDirection: "row",
    alignItems: "center",
    // paddingLeft: wp(6),
    paddingVertical: wp(3)
  },
  quarterOption: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    opacity: 0.8,
    marginLeft: wp(2)
  },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center"
  },
  radioButtonActive: {
    borderColor: colors.primary_color
  },
  radioButtonCircle: {
    width: wp(2.4),
    height: wp(2.4),
    borderRadius: wp(1.2),
    backgroundColor: colors.primary_color
  },
  hide: {
    display: "none"
  },
  list: {
    // marginBottom: hp(6),
    paddingBottom: hp(2)
  },
  selectButton: {
    position: "absolute",
    left: wp(8),
    right: wp(8),
    bottom: wp(8),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
    // elevation:1,
    zIndex: 10
  },
  selectButtonDisabled: {
    backgroundColor: colors.steelGrey
  },
  selectButtonText: {
    fontSize: RF(2.2),
    color: colors.white
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain"
  }
});
