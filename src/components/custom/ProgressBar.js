import React from "react";
import { View, Text, StyleSheet } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";

export default class ProgressBar extends React.PureComponent {
  render() {
    return (
      <View style={styles.container}>
        <View style={styles.greyLine} />
        <View style={styles.colorLine} />
        <View style={styles.valueContainer}>
          <Text style={styles.value}>73.9%</Text>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: { flex: 1,width: wp(50), height: hp(8),overflow:'hidden'},
  greyLine: {
      position:'absolute',
      top:10,
      left:10,
      right:10,
      height:4,
      backgroundColor: colors.greyishBrown,
      opacity:0.2,
      borderRadius: 2
  },
  colorLine: {
    position:'absolute',
    top:10,
    left:10,
    right:10,
    // marginRight: 10,
    height:4,
    borderRadius: 2,
    backgroundColor: colors.yellow,
},
  valueContainer: {
    position:'absolute',
    left:wp(50)/10,
    // height:30,
    backgroundColor: colors.primary_color,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius:3
  },
  value: {
      color:colors.white,
      fontSize: 15,
      fontWeight: '500',
      paddingHorizontal:10,
      paddingVertical: 6,
  }
});
