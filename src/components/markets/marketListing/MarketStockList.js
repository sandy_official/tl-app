import React from "react";
import { View, Text, StyleSheet, FlatList, RefreshControl } from "react-native";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import StockListItem from "./StockListItem";
import { getStocksList } from "../../../controllers/MarketController";
import { ifIphoneX } from "react-native-iphone-x-helper";
import Toast from "react-native-easy-toast";
import globalStyles from "react-native-raw-bottom-sheet/src/style";
import LoadingView from "../../common/LoadingView";
import CreateWatchlistModal from "../../common/CreateWatchlistModal";
import WatchlistModal from "../../common/WatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../../actions/WatchlistActions";
import NoMatchView from "../../common/NoMatchView";
import LoadingMoreView from "../../common/LoadingMoreView";
import LoginModal from "../../common/LoginModal";
import { fetcher } from "../../../controllers/Fetcher";
import { SCREENER_STOCKS_URL, SCREENER_WATCHLIST_STOCKS_URL } from "../../../utils/ApiEndPoints";
import { mapHeadersData } from "../../../utils/Utils";
import BaseComponent from "../../common/BaseComponent";
import { POST_METHOD } from "../../../utils/Constants";
import ScrollToTopView from "../../common/ScrollToTopView";

class MarketStockList extends React.PureComponent {
  state = {
    data: [],
    pageNumber: 0,
    refreshing: false,
    isFirstCallComplete: false,
    isNextPage: true,
    selectedStock: {},
    isLoadingMore: false,
    baseRef: null,
    showScrollToTop: false,
  };

  componentDidMount() {
    const { baseRef } = this.props;

    if (this.props.isSelected) {
      baseRef && baseRef.showLoading();
      this.getData();
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    const { pk, groupType, groupName, baseRef, baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;
    const { isNextPage, refreshing } = this.state;

    console.log("calling market api");

    if (isGuestUser && groupType == "user") {
      baseRef && baseRef.showLogin();
      this.stopLoading();

      console.log("ref", baseRef);
      return;
    }

    if (isNextPage || refreshing) {
      let pageNumber = this.state.pageNumber;
      let URL = groupType == "user" ? baseURL + SCREENER_WATCHLIST_STOCKS_URL : baseURL + SCREENER_STOCKS_URL;

      let body = {
        screenpk: pk,
        groupType: groupType,
        groupName: groupName,
        perPageCount: 20,
        pageNumber: pageNumber,
      };

      fetcher(
        URL,
        POST_METHOD,
        body,
        baseRef,
        appToken,
        token,
        (json) => {
          let body = json.body;
          let header = json.head;
          let stocks = mapHeadersData(body.tableHeaders, body.tableData);

          if (stocks.length > 0) {
            let oldStockList = refreshing ? [] : this.state.data;

            this.setState({
              data: oldStockList.concat(stocks),
              pageNumber: ++pageNumber,
              isNextPage: header.isNextPage,
            });
          }

          this.stopLoading();
        },
        (error) => {
          this.stopLoading();
        }
      );
    } else this.stopLoading();
  };

  stopLoading = () => {
    const { baseRef } = this.props;

    if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false, isLoadingMore: false });
    baseRef && baseRef.stopLoading();
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    this.setState({ isLoadingMore: true }, () => this.getData());
  };

  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => {
      this.getData();
    });
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
  };

  renderColumns = () => {
    return (
      <View style={styles.columnContainer}>
        <View style={{ flex: 1, marginLeft: wp(1) }}>
          <Text style={styles.columnName}>Stock</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={[styles.columnName, styles.textCenter]}>Last Price</Text>
        </View>
        <View style={{ flex: 1 }}>
          <Text style={styles.columnName}>Change</Text>
        </View>
      </View>
    );
  };

  renderStockList = () => {
    const { data, refreshing, isLoadingMore } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={data}
        extraData={{ ...this.props, data }}
        contentContainerStyle={styles.list}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderListItem}
        keyExtractor={this.extractItemKey}
        ListHeaderComponent={() => this.renderColumns()}
        ListFooterComponent={() => {
          return isLoadingMore && <LoadingMoreView />;
        }}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        onEndReached={this.onLoadMore}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={10}
        maxToRenderPerBatch={10}
        onScroll={this.handleScroll}
      />
    );
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  renderListItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;

    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <View style={{ flexDirection: "row", padding: wp(1) }}>
        <StockListItem
          {...this.props}
          data={item}
          showWatchlist={this.showWatchlist}
          WatchlistBtnState={isWatchlistActive}
        />
      </View>
    );
  };

  extractItemKey = (item, index) => index.toString();

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, groupType, label, isSelected } = this.props;
    const { selectedStock, data, isFirstCallComplete, showScrollToTop } = this.state;

    const showLoader = data.length == 0 && !isFirstCallComplete;
    const showEmpty = data.length == 0 && isFirstCallComplete;
    const emptyListMessage = groupType == "user" ? "No matches from your " + label : "No matches from " + label;

    console.log("isSelected", this.props.groupName, isSelected);

    if (isSelected) {
      return (
        <View style={styles.container}>
          {this.state.data.length > 0 && (
            <View>
              {/* {this.renderColumns()} */}
              {this.renderStockList()}
            </View>
          )}

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <Toast
            ref="toast"
            position="bottom"
            positionValue={hp(28)}
            style={globalStyles.toast}
            textStyle={globalStyles.toastText}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          <LoginModal ref="loginModal" {...this.props} />
          {showEmpty && <NoMatchView message={emptyListMessage} />}
          {showLoader && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
  };
};

export default connect(mapStateToProps, { setActiveWatchlist, updateWatchlist }, null, { forwardRef: true })(
  MarketStockList
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    // marginTop: hp(6)
  },
  columnContainer: {
    paddingHorizontal: wp(3),
    flexDirection: "row",
    alignItems: "center",
    paddingTop: wp(2),
    marginTop: hp(1),
  },
  columnName: {
    fontSize: RF(2),
    textAlign: "left",
    color: colors.steelGrey,
  },
  textCenter: {
    textAlign: "center",
  },
  list: {
    // flex:1,
    minHeight: hp(80),
    // paddingLeft: wp(1),
    // paddingVertical: 4

    paddingBottom: ifIphoneX ? hp(3) : hp(1),
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(2),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(1.5),
    margin: 3,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
});
