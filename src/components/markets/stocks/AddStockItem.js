import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { PRIMARY_COLOR, colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { getDVMBackgroundColor, numberWithCommas } from "../../../utils/Utils";

export default class AddStockItem extends React.PureComponent {
  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    debugger;
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.full_name,
      pageType: stock.page_type,
      stockCode: stockCode,
    });
  };

  onAddPress = () => {
    const { showWatchlist, stock, isPortfolio, showPortfolio } = this.props;
    isPortfolio ? showPortfolio(stock) : showWatchlist(stock);
  };

  renderStockNameCode = () => {
    const { stock } = this.props;

    return (
      <TouchableOpacity style={styles.nameCodeContainer} onPress={this.gotoStockDetails}>
        <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
          {stock.NSEcode ? stock.NSEcode : stock.BSEcode}
        </Text>
        <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
          {stock.full_name}
        </Text>
      </TouchableOpacity>
    );
  };

  renderDvmPrice = () => {
    const { stock } = this.props;
    const currentPrice = stock.currentPrice ? numberWithCommas(stock.currentPrice, true) : stock.currentPrice;

    return (
      <View style={styles.priceContainer}>
        <Text style={styles.price}>{currentPrice}</Text>
        <View style={styles.row}>
          <View style={getDVMBackgroundColor(stock.d_color)} />
          <View style={getDVMBackgroundColor(stock.v_color)} />
          <View style={getDVMBackgroundColor(stock.m_color)} />
        </View>
      </View>
    );
  };

  renderAddToWatchlist = () => {
    const { watchlist, stock } = this.props;

    const stockId = stock.stock_id;
    const isWatchlistActive = stockId in watchlist && watchlist[stockId].length > 0;

    const icon = isWatchlistActive
      ? require("../../../assets/icons/check-mark-blue.png")
      : require("../../../assets/icons/add-black.png");

    return (
      <TouchableOpacity style={styles.addIconContainer} onPress={this.onAddPress}>
        <Image source={icon} style={styles.addIcon} />
      </TouchableOpacity>
    );
  };

  render() {
    const { stock } = this.props;
    const containerStyle =
      stock.day_change > 0 ? [styles.container, styles.greenBorder] : [styles.container, styles.redBorder];

    return (
      <View style={containerStyle}>
        {this.renderStockNameCode()}
        {this.renderDvmPrice()}
        {this.renderAddToWatchlist()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(1.5),
    paddingHorizontal: wp(3),
    paddingVertical: wp(1.5)
  },
  nameCodeContainer: {
    flex: 3,
    paddingRight: 18
  },
  priceContainer: {
    flex: 1.5,
    alignItems: "flex-end",
    marginRight: wp(15)
  },
  addIconContainer: {
    flex: 0.8,
    alignItems: "center",
    justifyContent: "center",
    padding: wp(3)
  },
  code: {
    color: colors.lightNavy,
    fontSize: RF(2.2),
    fontWeight: "500"
  },
  name: {
    fontSize: RF(1.9),
    marginTop: hp(1),
    color: colors.greyishBrown,
    opacity: 0.85
  },
  price: {
    fontSize: RF(2.2),
    fontWeight: "500",
    marginBottom: 4
  },
  row: {
    flexDirection: "row",
    marginTop: 8
  },
  colorBlock: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.green,
    margin: 3,
    borderRadius: 2
  },
  changeInPercent: {
    color: "#fff",
    fontSize: RF(2.2),
    fontWeight: "500"
  },
  changeInPrice: {
    color: "#fff",
    fontSize: RF(2),
    marginTop: 2,
    opacity: 0.95
  },
  changeBox: {
    width: wp(22),
    padding: wp(2),
    backgroundColor: colors.green,
    justifyContent: "center",
    borderRadius: 5
  },
  greenBorder: {
    borderLeftWidth: 4,
    borderLeftColor: colors.green
  },
  redBorder: {
    borderLeftWidth: 4,
    borderLeftColor: colors.red
  },
  addIcon: {
    width: wp(5),
    height: wp(5),
    resizeMode: "contain"
    // padding: 8
  }
});
