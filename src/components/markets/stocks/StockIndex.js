import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../../styles/CommonStyles";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";

export default class StockIndex extends React.PureComponent {
  gotoStockDetails = stock => {
    const { navigation } = this.props;
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.full_name,
      pageType: stock.page_type,
      stockCode: stockCode,
    });
  };

  renderIndexNameValue = data => {
    const currentPriceStyle =
      data.day_changeP >= 0 ? [styles.stockIndexPoints, styles.greenText] : [styles.stockIndexPoints, styles.redText];

    return (
      <View>
        <Text style={styles.stockIndex} numberOfLines={1}>
          {data.NSEcode ? data.NSEcode : data.BSECode}
          <Text>{"  "}</Text>
          <Text style={currentPriceStyle}>{data.currentPrice}</Text>
        </Text>
      </View>
    );
  };

  renderIndexChangeRow = data => {
    const pointStyle =
      data.day_changeP >= 0 ? [styles.stockIndexPoints, styles.greenText] : [styles.stockIndexPoints, styles.redText];

    const pointIcon =
      data.day_changeP >= 0
        ? require("../../../assets/icons/arrow-green.png")
        : require("../../../assets/icons/arrow-red.png");

    return (
      <View style={styles.stockIndexChangeRow}>
        <Image source={pointIcon} style={styles.stockValueIcon} />
        <Text style={pointStyle}>
          {data.day_change} ({data.day_changeP}%)
        </Text>
      </View>
    );
  };

  renderStockIndexes = () => {
    const { data } = this.props;

    return (
      <View style={styles.stockIndexRow}>
        {/* Stock index 1 */}
        <View style={[styles.stockIndexContainer, styles.stockIndexDivider]}>
          <TouchableOpacity onPress={() => this.gotoStockDetails(data[0])}>
            {this.renderIndexNameValue(data[0])}
            {this.renderIndexChangeRow(data[0])}
          </TouchableOpacity>
        </View>

        {/* Stock index 2 */}
        <View style={styles.stockIndexContainer}>
          <TouchableOpacity onPress={() => this.gotoStockDetails(data[1])}>
            {this.renderIndexNameValue(data[1])}
            {this.renderIndexChangeRow(data[1])}
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    return <View>{this.renderStockIndexes()}</View>;
  }
}

const styles = StyleSheet.create({
  stockIndexRow: {
    flexDirection: "row",
    alignItems: "center"
  },
  stockIndexContainer: {
    flex: 1,
    marginTop: wp(5),
    marginBottom: wp(2),
    paddingHorizontal: "3%",
    alignItems: "center"
  },
  stockIndexDivider: {
    borderRightWidth: 0.3,
    borderRightColor: colors.lightNavy
    // marginRight: "1%",
    // marginLeft:'-2%'
  },
  stockIndex: {
    fontSize: RF(2),
    color: colors.primary_color,
    fontWeight: "300"
  },
  stockIndexPoints: {
    fontSize: RF(2),
    marginLeft: 4,
    fontWeight: "300"
  },
  stockIndexChangeRow: {
    flexDirection: "row",
    marginTop: 6,
    alignItems: "center"
  },
  stockValueIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginTop: 2
  },
  greenText: {
    color: colors.green
  },
  redText: {
    color: colors.red
  },
  yellowText: {
    color: colors.yellow
  }
});
