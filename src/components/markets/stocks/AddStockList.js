import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import StockCardItem from "./StockCardItem";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import AddStockItem from "./AddStockItem";

export default class AddStockList extends React.PureComponent {
  renderListItem = ({ item, index }) => {
    return <AddStockItem {...this.props} stock={item} />;
  };

  extractItemKey = (item, index) => index.toString();

  render() {
    const { stockList, cardTitle, userState } = this.props;
    const label = userState.isLoggedIn ? cardTitle : `Add Stocks to your ${cardTitle}`;
    const showViewAllButton = stockList > 3;

    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Text style={styles.label}>{label}</Text>
          {showViewAllButton && (
            <TouchableOpacity onPress={this.gotoMarketListing}>
              <Text style={styles.viewAll}>View all</Text>
            </TouchableOpacity>
          )}
        </View>

        <FlatList
          data={stockList}
          extraData={this.props}
          horizontal={true}
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderListItem}
          keyExtractor={this.extractItemKey}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    minHeight: hp(30),
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
    paddingVertical: wp(3),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2
    // borderColor: '#000',
    // borderWidth: 1,
  },
  label: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    paddingLeft: wp(3)
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  viewAll: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  viewAll: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color
  }
});
