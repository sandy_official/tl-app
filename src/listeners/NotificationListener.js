import firebase from "react-native-firebase";
import { Platform } from "react-native";
import AsyncStorage from "@react-native-community/async-storage";
// import { ts } from "./FirebaseHelpers";

export default class NotificationListener {
  constructor(handleUrl) {
    this.checkPermission();
    this.handleUrl = handleUrl;
    this.displayListener = this.notificationDisplayedListener();
    this.notificationListener = this.notificationListener();
    this.openedListener = this.notificationOpenedListener();
  }

  remove = () => {
    // this.displayListener();
    // this.notificationListener();
    // this.openedListener();
  };

  notificationDisplayedListener = () => {
    // app in foreground
    firebase.notifications().onNotificationDisplayed((notification) => {
      console.log("onNotificationDisplayed");
      console.log(notification);
      // firebase.notifications().displayNotification(notification);
    });
  };

  notificationListener = () => {
    // app in foreground
    firebase.notifications().onNotification((notification) => {
      console.log("notificationListener");
      console.log(notification);

      const channel = new firebase.notifications.Android.Channel(
        "trendlyne",
        "trendlyne",
        firebase.notifications.Android.Importance.Max
      );
      // ).setDescription("A natural description of the channel");

      if (Platform.OS === "android") {
        const localNotification = new firebase.notifications.Notification({
          sound: "default",
          show_in_foreground: true,
        })
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification.body)
          .setData(notification.data)
          .android.setChannelId("trendlyne") // e.g. the id you chose above
          .android.setSmallIcon("@drawable/ic_notifications") // create this icon in Android Studio
          .android.setColor("#000000") // you can set a color here
          .android.setPriority(firebase.notifications.Android.Priority.High);

        firebase.notifications().android.createChannel(channel);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch((err) => console.error(err));
      } else if (Platform.OS === "ios") {
        const localNotification = new firebase.notifications.Notification()
          .setNotificationId(notification.notificationId)
          .setTitle(notification.title)
          .setSubtitle(notification.subtitle)
          .setBody(notification.body)
          .setData(notification.data)
          .ios.setBadge(notification.ios.badge);

        firebase
          .notifications()
          .displayNotification(localNotification)
          .catch((err) => console.error(err));
      }
      // firebase.notifications().removeDeliveredNotification(localNotification.notificationId);
    });
  };

  notificationOpenedListener = () => {
    // app in background
    firebase.notifications().onNotificationOpened((notificationOpen) => {
      console.log("notificationOpenedListener");
      console.log(notificationOpen);
      const { action, notification } = notificationOpen;
      firebase.notifications().removeDeliveredNotification(notification.notificationId);
      console.log("OPEN:", notification);
      if (notification._data.screenRedirectUrl) this.handleUrl(notification._data.screenRedirectUrl);
    });
  };

  notificationTokenListener = (userId) => {
    // listens for changes to the user's notification token and updates database upon change
    firebase.messaging().onTokenRefresh((notificationToken) => {
      console.log("notificationTokenListener");
      console.log(notificationToken);

      return firebase
        .firestore()
        .collection("users")
        .doc(userId)
        .update({ pushToken: notificationToken, updatedAt: null }) //ts
        .then((ref) => {
          console.log("savePushToken success");
        })
        .catch((e) => {
          console.error(e);
        });
    });
  };

  checkPermission = async () => {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  };

  getToken = async () => {
    // let fcmToken = await AsyncStorage.getItem("fcmToken");
    // if (!fcmToken) {
    let fcmToken = await firebase.messaging().getToken();
    console.warn("fcm token", fcmToken);
    if (fcmToken) {
      // user has a device token
      await AsyncStorage.setItem("fcmToken", fcmToken);
    }
    // }
  };

  requestPermission = async () => {
    try {
      await firebase.messaging().requestPermission();
      console.log("permissions request");

      // User has authorised
      this.getToken();
    } catch (error) {
      // User has rejected permissions
      console.log("permission rejected");
    }
  };
}
