import React from "react";
import { StyleSheet, Platform } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

//Colors
export const colors = {
  // green_blue: "#00d684",
  steelGrey: "#808285",
  white: "#ffffff",
  primary_color: "#2453a1",
  red: "#e3352b",
  green: "#31a745",
  iceBlue: "#fdfeff",
  lightNavy: "#174078",
  pinkishGrey: "#c6c6c6",
  black: "#000000",
  greyishBrown: "#4d4d4d",
  yellow: "#f9b233",
  warmGrey: "#9d9d9d",
  whiteTwo: "#f6f6f6",
  black15: "rgba(0, 0, 0, 0.15)",
  greyish: "#b3b3b3",
  cement: "#9d9d9c",
  deepRed: "#a20900",
  hotMagenta: "#d500b7",
  blueViolet: "#6400eb",
  squash: "#f7941e",
  brightBlue: "#006aff",
  vibrantPurple: "#a500d9",
  barbiePink: "#fb3b94",
  blush: "#f4aeaa",
  strongBlue: "#2b00f7",
  marineBlue: "#002e4f",
  emeraldGreen: "#007d15",
  greyishBrownTwo: "#505050",
  azul: "#1867d3",
  tomato: "#e63c20",
  whiteThree: "#eaeaea",
  peacockBlue: "#005797",
  cerulean: "#0076d9",
  mediumGreen: "#31a745",
  gradientCardStartColor: "rgb(15,55,109)",
  gradientCardEndColor: "rgb(25,91,181)",
  cobalt: "#2453a1",
  blueBlue: "#195bb5",
};

//font sizes
export const fontsize = {
  10: RF(1.5),
  11: RF(1.7),
  12: RF(1.9),
  13: RF(2.1),
  14: RF(2.2),
  15: RF(2.3),
  16: RF(2.4),
  20: RF(2.8),
};

//Dimenssions
export const TAB_BAR_HEIGHT = hp(6);

export const globalStyles = StyleSheet.create({
  sortOptionRow: {
    flexDirection: "row",
    marginHorizontal: wp(1),
    marginVertical: wp(2),
  },
  sortOptionContainer: {
    overflow: "hidden",
    // minWidth: wp(15),
    flexDirection: "row",
    // height: Platform.OS == "ios" ? hp(6.2) : hp(7.2),
    borderRadius: 5,
    borderColor: colors.primary_color,
    borderWidth: 0.5,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
    alignItems: "center",
  },
  sortOption: {
    justifyContent: "center",
  },
  sortOptionName: {
    fontSize: RF(2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  sortOptionNameSelected: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.white,
  },
  sortIconStyle: {
    resizeMode: "contain",
    marginLeft: 10,
  },
  sortOrder: {
    fontSize: RF(1.6),
    color: colors.white,
    opacity: 0.7,
  },
  indicatorContainer: {
    height: Platform.OS == "ios" ? TAB_BAR_HEIGHT : TAB_BAR_HEIGHT + wp(2),
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  indicatorText: {
    color: colors.white,
    opacity: 0.55,
    fontSize: RF(2.2),
  },
  indicatorSelectedText: {
    color: colors.white,
    // fontWeight: "bold",
    fontSize: RF(2.2),
  },
  tabSize: {
    minWidth: wp(20),
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
  },
  pagerStyle: {
    marginTop: Platform.OS == "ios" ? TAB_BAR_HEIGHT : TAB_BAR_HEIGHT + wp(2),
  },
  toastText: {
    fontSize: RF(2.1),
    color: colors.white,
    paddingHorizontal: wp(1),
    // paddingHorizontal: wp(2)
  },
  toast: {
    bottom: hp(12),
    borderRadius: wp(10),
    backgroundColor: colors.lightNavy,
    marginHorizontal: wp(6),
    paddingVertical: wp(1.8),
  },
});

export const fontStyle = {
  markFont: {
    fontFamily: "Abel-Regular", //testing font
  },
  primary: {
    fontFamily: "Roboto-Regular",
  },
};
