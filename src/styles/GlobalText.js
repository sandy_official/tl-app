import React from "react";
import { Text } from "react-native";

export default class GlobalText {
  static applyFontFamily = (fontName) => {
    let oldRender = Text.render;

    Text.render = function (...args) {
      let origin = oldRender.call(this, ...args);

      return React.cloneElement(origin, {
        style: [{ fontFamily: fontName }, origin.props.style],
      });
    };
  };
}
