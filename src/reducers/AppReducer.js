import {
  UPDATE_FIREBASE_SETTINGS,
  CLEAR_FIREBASE_SETTINGS,
  ADD_MF_IN_PORTFOLIO_SUGGESTION,
  ADD_STOCKS_IN_PORTFOLIO_SUGGESTION,
  APP_TOKEN,
} from "../actions/ActionTypes";

const initialState = {
  firebase: {},
  appToken: null,
  preferences: { showPortfolioAddMFSuggestion: true, showPortfolioAddStocksSuggestion: true },
};

export const appReducer = (state = initialState, action) => {
  let obj = {};

  switch (action.type) {
    case APP_TOKEN:
      return { ...state, appToken: action.payload };

    case UPDATE_FIREBASE_SETTINGS:
      return { ...state, firebase: action.payload };

    case CLEAR_FIREBASE_SETTINGS:
      return { ...state, firebase: {} };

    case ADD_MF_IN_PORTFOLIO_SUGGESTION:
      return { ...state, preferences: { ...state.preferences, showPortfolioAddMFSuggestion: action.payload } };

    case ADD_STOCKS_IN_PORTFOLIO_SUGGESTION:
      return { ...state, preferences: { ...state.preferences, showPortfolioAddStocksSuggestion: action.payload } };

    default:
      return state;
  }
};
