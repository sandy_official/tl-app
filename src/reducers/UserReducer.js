import {
  USER_LOGIN,
  USER_LOGOUT,
  GUEST_USER,
  USER_DETAILS,
  USER_DASHBOARD,
  // UPDATE_TOKEN
} from "../actions/ActionTypes";

const initialState = {
  token: null,
  appToken: null,
  isLoggedIn: false,
  isGuestUser: false,
  isSubscriber: false,
  fullName: "",
  email: "",
  profilePhoto: "",
  isSubscriber: false,
  isEnterpriseUser: false,
  dashboard: {},
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN:
      return {
        ...state,
        token: action.payload,
        isLoggedIn: true,
        isGuestUser: false,
      };

    case GUEST_USER:
      return { ...state, isGuestUser: true, token: state.appToken };

    // case UPDATE_TOKEN:
    //   return { ...state, token: action.payload, appToken: action.payload };

    case USER_DETAILS:
      return { ...state, ...action.payload };

    case USER_DASHBOARD:
      return { ...state, dashboard: action.payload };

    case USER_LOGOUT:
      return initialState;

    default:
      return state;
  }
};
