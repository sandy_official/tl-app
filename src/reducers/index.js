import { combineReducers } from "redux";
import { watchlistReducer } from "./WatchlistReducer";
import { portfolioReducer } from "./PortfolioReducer";
import { superstarReducer } from "./SuperstarReducer";
import { userReducer } from "./UserReducer";
import { appReducer } from "./AppReducer";
import { marketReducer } from "./MarketReducer";

export const rootReducer = combineReducers({
  app: appReducer,
  watchlist: watchlistReducer,
  portfolio: portfolioReducer,
  superstar: superstarReducer,
  user: userReducer,
  market: marketReducer
});
