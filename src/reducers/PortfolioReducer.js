import {
  LOAD_PORTFOLIO,
  UPDATE_PORTFOLIO,
  CLEAR_PORTFOLIO
} from "../actions/ActionTypes";

const initialState = { portfolio: [] };

export const portfolioReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_PORTFOLIO:
    case UPDATE_PORTFOLIO:
      return { ...state, portfolio: action.payload };

    case CLEAR_PORTFOLIO:
      return { ...state, portfolio: [] };

    default:
      return state;
  }
};
