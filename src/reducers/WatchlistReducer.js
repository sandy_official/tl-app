import {
  LOAD_WATCHLIST,
  SET_ACTIVE_WATCHLIST,
  UPDATE_WATCHLIST,
  SET_ACTIVE_WATCHLIST_BY_STOCK_ID,
  CLEAR_WATCHLIST,
  CLEAR_WATCHLIST_ALL,
  CLEAR_ACTIVE_WATCHLIST
} from "../actions/ActionTypes";

const initialState = { watchlist: [], activeWatchlist: {} };

export const watchlistReducer = (state = initialState, action) => {
  const { activeWatchlist } = state;

  switch (action.type) {
    case LOAD_WATCHLIST:
    case UPDATE_WATCHLIST:
      return { ...state, watchlist: action.payload };

    case SET_ACTIVE_WATCHLIST:
    case SET_ACTIVE_WATCHLIST_BY_STOCK_ID:
      return { ...state, activeWatchlist: action.payload };

    case CLEAR_WATCHLIST:
      return { ...state, watchlist: [] };

    case CLEAR_ACTIVE_WATCHLIST:
      return { ...state, activeWatchlist: {} };

    case CLEAR_WATCHLIST_ALL:
      return { ...state, watchlist: [], activeWatchlist: {} };

    default:
      return state;
  }
};
