import { LOAD_SUPERSTAR, CLEAR_SUPERSTAR } from "../actions/ActionTypes";

const initialState = {
  superstars: { individual: [], popular: [], institutional: [], fii: [] }
};

export const superstarReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_SUPERSTAR:
      return { ...state, superstars: action.payload };

    case CLEAR_SUPERSTAR:
      return {
        ...state,
        superstars: { individual: [], popular: [], institutional: [], fii: [] }
      };

    default:
      return state;
  }
};
