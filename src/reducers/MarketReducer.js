import { LOAD_INDEX_DATA, LOAD_INDICES_CARDS, CLEAR_MARKET, LOAD_USERS_CARDS } from "../actions/ActionTypes";

const initialState = { niftySensex: [], indices: [], userData: [] };

export const marketReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOAD_INDEX_DATA:
      return { ...state, niftySensex: action.payload };

    case LOAD_INDICES_CARDS:
      return { ...state, indices: action.payload };

    case LOAD_USERS_CARDS:
      return { ...state, userData: action.payload };

    case CLEAR_MARKET:
      return { ...initialState };

    default:
      return state;
  }
};
