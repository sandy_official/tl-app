import { createStore, applyMiddleware } from "redux";
import { rootReducer } from "../reducers";
import thunk from "redux-thunk";
import logger from "redux-logger";
import { composeWithDevTools } from "redux-devtools-extension";
// import { NativeModules } from "react-native";
import { persistStore, persistReducer } from "redux-persist";
import AsyncStorage from "@react-native-community/async-storage";

// if (__DEV__) {
//   NativeModules.DevSettings.setIsDebuggingRemotely(true);
// }

const persistConfig = { key: "trendlyne", storage: AsyncStorage, timeout: null };
const persistedReducer = persistReducer(persistConfig, rootReducer);

/**
 * Remove middleware from production
 */

const store = createStore(persistedReducer, composeWithDevTools(applyMiddleware(thunk, logger)));

const persistor = persistStore(store);

export { store, persistor };
