import {
  UPDATE_FIREBASE_SETTINGS,
  CLEAR_FIREBASE_SETTINGS,
  ADD_MF_IN_PORTFOLIO_SUGGESTION,
  ADD_STOCKS_IN_PORTFOLIO_SUGGESTION,
  APP_TOKEN,
} from "./ActionTypes";
import { TEST_SERVER_URL } from "../utils/Constants";

export const setAppToken = (token) => (dispatch) => {
  dispatch({ type: APP_TOKEN, payload: token });
};

export const updateFirebaseSettings = (obj) => (dispatch) => {
  // obj.version.baseUrl = TEST_SERVER_URL;
  dispatch({ type: UPDATE_FIREBASE_SETTINGS, payload: obj });
};

export const clearFirebaseSettings = () => (dispatch) => {
  dispatch({ type: CLEAR_FIREBASE_SETTINGS });
};

export const showPortfolioAddMFSuggestion = (flag) => (dispatch) => {
  dispatch({ type: ADD_MF_IN_PORTFOLIO_SUGGESTION, payload: flag });
};

export const showPortfolioAddStocksSuggestion = (flag) => (dispatch) => {
  dispatch({ type: ADD_STOCKS_IN_PORTFOLIO_SUGGESTION, payload: flag });
};
