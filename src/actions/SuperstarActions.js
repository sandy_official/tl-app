import { getSuperstarList } from "../controllers/SuperstarController";
import { LOAD_SUPERSTAR, CLEAR_SUPERSTAR } from "./ActionTypes";
import { fetcher } from "../controllers/Fetcher";
import { GET_SUPERSTAR_LIST } from "../utils/ApiEndPoints";
import { POST_METHOD } from "../utils/Constants";

const mapSuperstarList = (json) => {
  let columnList = [];
  let superstarList = {};

  json.tableHeaders.forEach((column) => {
    columnList.push(column.name);
  });

  Object.keys(json.tableData).forEach((key) => {
    json.tableData[key].forEach((superstar) => {
      let obj = {};

      superstar.forEach((item, i) => {
        obj[columnList[i]] = item;
      });

      if (!superstarList[key]) superstarList[key] = [];

      superstarList[key].push(obj);
    });
  });

  return superstarList;
};

export const loadSuperstar = (baseURL, appToken, token) => (dispatch) => {
  const URL = baseURL + GET_SUPERSTAR_LIST;
  let body = { segment: "all" };

  fetcher(
    URL,
    POST_METHOD,
    body,
    null,
    appToken,
    token,
    (json) => {
      const data = mapSuperstarList(json.body);
      console.log("Superstar data", data);
      let popular = "popularData" in data ? data.popularData : [];
      let individual = "individualData" in data ? data.individualData : [];
      let institutional = "institutionalData" in data ? data.institutionalData : [];
      let fii = "fiiData" in data ? data.fiiData : [];

      let superstars = { popular: popular, individual: individual, institutional: institutional, fii: fii };
      dispatch({ type: LOAD_SUPERSTAR, payload: superstars });
    },
    (error) => {
      // error message
    }
  );
};

export const clearSuperstar = () => (dispatch) => {
  dispatch({ type: CLEAR_SUPERSTAR });
};
