import { USER_LOGIN, USER_LOGOUT, GUEST_USER, USER_DETAILS, USER_DASHBOARD, UPDATE_TOKEN } from "./ActionTypes";
import { getUserDetails, getUserDashboard } from "../controllers/UserController";

export const doUserLogin = (token) => (dispatch) => {
  dispatch({ type: USER_LOGIN, payload: token });
};

export const doUserLogout = () => (dispatch) => {
  dispatch({ type: USER_LOGOUT });
};

export const doGuestLogin = () => (dispatch) => {
  dispatch({ type: GUEST_USER });
};

// export const updateToken = token => dispatch => {
//   dispatch({ type: UPDATE_TOKEN, payload: token });
// };

export const loadUserDetails = (baseURL, appToken, token) => (dispatch) => {
  getUserDetails(baseURL, appToken, token, (details) => {
    if (Object.keys(details).length > 0) {
      dispatch({ type: USER_DETAILS, payload: details });
    }
  });
};

export const loadUserDashboard = (baseURL, appToken, token) => (dispatch) => {
  getUserDashboard(baseURL, appToken, token, (dashboard) => {
    if (Object.keys(dashboard).length > 0) {
      dispatch({ type: USER_DASHBOARD, payload: dashboard });
    }
  });
};
