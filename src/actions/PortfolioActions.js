import { LOAD_PORTFOLIO, UPDATE_PORTFOLIO, CLEAR_PORTFOLIO } from "./ActionTypes";
import { getPortfolioListApi } from "../controllers/PortfolioController";

export const loadPortfolio = (baseURL, appToken, token) => (dispatch) => {
  getPortfolioListApi(baseURL, appToken, token, (portfolioList) => {
    dispatch({ type: LOAD_PORTFOLIO, payload: portfolioList });
  });
};

export const updatePortfolio = (portfolio) => (dispatch) => {
  dispatch({ type: UPDATE_PORTFOLIO, payload: [...portfolio] });
};

export const clearPortfolio = () => (dispatch) => {
  dispatch({ type: CLEAR_PORTFOLIO });
};
