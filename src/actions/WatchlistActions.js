import {
  LOAD_WATCHLIST,
  SET_ACTIVE_WATCHLIST,
  UPDATE_WATCHLIST,
  SET_ACTIVE_WATCHLIST_BY_STOCK_ID,
  CLEAR_WATCHLIST,
  CLEAR_ACTIVE_WATCHLIST,
  CLEAR_WATCHLIST_ALL,
} from "./ActionTypes";
import { getWatchlistApi, getWatchlistActiveStock } from "../controllers/WatchlistController";

export const loadWatchlist = (baseURL, appToken, token, stockId) => (dispatch) => {
  getWatchlistApi(baseURL, appToken, token, stockId, (data) => {
    dispatch({ type: LOAD_WATCHLIST, payload: data });
  });
};

export const updateWatchlist = (watchlist) => (dispatch) => {
  dispatch({ type: UPDATE_WATCHLIST, payload: [...watchlist] });
};

export const getActiveWatchlist = (baseURL, appToken, token) => (dispatch) => {
  getWatchlistActiveStock(baseURL, appToken, token, (stockList) => {
    dispatch({ type: SET_ACTIVE_WATCHLIST, payload: stockList });
  });
};

export const getActiveWatchlistByStockId = (baseURL, appToken, token, stockId, activeWatchlist) => (dispatch) => {
  getWatchlistApi(baseURL, appToken, token, stockId, (watchlist, activeIn) => {
    const activeList = Object.assign({}, activeWatchlist);
    if (activeIn.length > 0) activeList[stockId] = activeIn;

    dispatch({ type: SET_ACTIVE_WATCHLIST_BY_STOCK_ID, payload: activeList });
    dispatch({ type: LOAD_WATCHLIST, payload: watchlist });
  });
};

export const setActiveWatchlist = (activeWatchlist) => (dispatch) => {
  dispatch({ type: SET_ACTIVE_WATCHLIST, payload: Object.assign({}, activeWatchlist) });
};

/**
 * Clear watchlist data from the store
 */
export const clearWatchlist = () => (dispatch) => {
  dispatch({ type: CLEAR_WATCHLIST });
};

export const clearActiveWatchlist = () => (dispatch) => {
  dispatch({ type: CLEAR_ACTIVE_WATCHLIST });
};

export const clearWatchlistAll = () => (dispatch) => {
  dispatch({ type: CLEAR_WATCHLIST_ALL });
};
