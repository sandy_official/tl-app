import { LOAD_INDICES_CARDS, CLEAR_MARKET, LOAD_INDEX_DATA, LOAD_USERS_CARDS } from "./ActionTypes";
import { getNiftySensexPoints, getStocksCardData } from "../controllers/MarketController";

export const loadIndicesCards = (baseURL, appToken, token, endPoint, cb) => (dispatch) => {
  getStocksCardData(baseURL, appToken, token, endPoint, (data) => {
    cb(data);
    dispatch({ type: LOAD_INDICES_CARDS, payload: data });
  });
};

export const loadUsersCards = (data) => (dispatch) => {
  dispatch({ type: LOAD_USERS_CARDS, payload: data });
};

export const loadIndexData = (baseURL, appToken, token) => (dispatch) => {
  getNiftySensexPoints(baseURL, appToken, token, (data) => {
    dispatch({ type: LOAD_INDEX_DATA, payload: data.reverse() });
  });
};

export const clearMarket = () => (dispatch) => {
  // dispatch({ type: CLEAR_MARKET });
};
