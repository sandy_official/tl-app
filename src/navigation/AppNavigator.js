import React from "react";
import {
  createStackNavigator,
  createSwitchNavigator,
  createAppContainer,
  createDrawerNavigator,
} from "react-navigation";

import { widthPercentageToDP as wp } from "react-native-responsive-screen";

// Market Screens
import MarketTabScreen from "../views/markets/MarketTabScreen";
import MarketListingScreen from "../views/markets/MarketListingScreen";
import MarketSearchScreen from "../views/markets/MarketSearchScreen";

// Superstar Screens
import SuperstarTabScreen from "../views/superstars/SuperstarTabScreen";
import SuperstarScreen from "../views/superstars/SuperstarScreen";
import SuperstarSearch from "../components/superstar/SuperstarSearch";

// Screener Screens
import ScreenerListScreen from "../views/screeners/ScreenerListScreen";
import ScreenerCategoriesScreen from "../views/screeners/ScreenerCategoriesScreen";
import ScreenerStockTabScreen from "../views/screeners/ScreenerStockTabScreen";
import ScreenerSearchResult from "../views/screeners/ScreenerSearchScreen";

//portfolio Screens
import PortfolioTabScreen from "../views/portfolio/portfolioTabScreen";

import WatchlistScreen from "../views/portfolio/watchlistScreen";
import WatchlistDetailScreen from "../views/portfolio/watchlistDetailScreen";

import TransactionsScreen from "../views/portfolio/transactionsScreen";

import HoldingsScreen from "../views/portfolio/holdingsScreen";
import HoldingsDetailScreen from "../views/portfolio/holdingDetailScreen";

import SummaryScreen from "../views/portfolio/summaryScreen";

// Stock Details Screen
import StockTabScreen from "../views/stockDetail/StockTabScreen";
import StockSearchScreen from "../views/stockDetail/StockSearchScreen";

// Research Report Screens
import ResearchReportScreen from "../views/researchReport/ResearchReportScreen";
import BrokerDetailScreen from "../views/researchReport/BrokerDetailScreen";
import ReportSearchScreen from "../views/researchReport/ReportSearchScreen";

// Subscription Screens
import SubscriptionScreen from "../views/subscription/SubscriptionScreen";
import SubscriptionPayOptionsScreen from "../views/subscription/SubscriptionPayOptionsScreen";
import SubscriptionFormScreen from "../views/subscription/SubscriptionFormScreen";
import SubscriptionDetailsScreen from "../views/subscription/SubscriptionDetailsScreen";

// Contact Us Screens
import ContactUsScreen from "../views/contactUs/ConatctUsScreen";
import DrawerMenu from "./DrawerMenu";

// Dashboard Screen
import DashboardScreen from "../views/dashboard/DashboardScreen";

// Onboarding, Login & SignUp Screens
import LoginScreen from "../views/login/LoginScreen";
import OnboardingScreen from "../views/login/OnboardingScreen";
import SignUpScreen from "../views/login/SignUpScreen";
import SplashScreen from "../views/login/SplashScreen";
import SubscribeMessageScreen from "../views/SubscribeMessageScreen";
import ScreenerHistoryDetailScreen from "../views/alerts/ScreenerHistoryDetailScreen";

//Alerts
import AlertsTabScreen from "../views/alerts/AlertsTabScreen";
import ScreenerHistoryScreen from "../views/alerts/ScreenerHistoryScreen";
import AlertStockListScreen from "../views/alerts/AlertStockListScreen";
import NotificationsScreen from "../views/alerts/NotificationsScreen";
import WebViewScreen from "../views/WebViewScreen";

const AuthStack = createStackNavigator(
  {
    Onboard: { screen: OnboardingScreen },
    Login: { screen: LoginScreen },
    SignUp: { screen: SignUpScreen },
  },
  {
    headerMode: "none",
  }
);

const AppStackNavigator = createStackNavigator(
  {
    //Dashboard
    Home: { screen: DashboardScreen },

    // //Market
    Market: { screen: MarketTabScreen },
    MarketListing: { screen: MarketListingScreen },
    // MarketSearch: { screen: MarketSearchScreen },

    // Screener
    Screener: { screen: ScreenerCategoriesScreen },
    ScreenerList: { screen: ScreenerListScreen },
    ScreenerStocks: { screen: ScreenerStockTabScreen },
    ScreenerSearch: { screen: ScreenerSearchResult },

    // // Superstar
    Superstar: { screen: SuperstarTabScreen },
    SuperstarInfo: { screen: SuperstarScreen },
    SuperstarSearch: { screen: SuperstarSearch },

    // Stocks
    Stocks: { screen: StockTabScreen },
    StockSearch: { screen: StockSearchScreen },

    //Reports
    Reports: { screen: ResearchReportScreen },
    ReportDetail: { screen: BrokerDetailScreen },
    ReportSearch: { screen: ReportSearchScreen },

    // Subscription
    Subscription: { screen: SubscriptionScreen },
    SubscriptionPayMode: { screen: SubscriptionPayOptionsScreen },
    SubscriptionForm: { screen: SubscriptionFormScreen },
    SubscriptionDetails: { screen: SubscriptionDetailsScreen },

    //PortFolio Screens
    Portfolio: { screen: PortfolioTabScreen },
    Watchlists: { screen: WatchlistScreen },
    WatchlistDetails: { screen: WatchlistDetailScreen },
    Transactions: { screen: TransactionsScreen },
    Holdings: { screen: HoldingsScreen },
    HoldingsDetail: { screen: HoldingsDetailScreen },
    Summary: { screen: SummaryScreen },

    // Alerts
    Alerts: { screen: AlertsTabScreen },
    ScreenerHistory: { screen: ScreenerHistoryScreen },
    ScreenerHistoryDetail: { screen: ScreenerHistoryDetailScreen },
    AlertsStock: { screen: AlertStockListScreen },
    Notifications: { screen: NotificationsScreen },

    //ContactUs
    ContactUs: { screen: ContactUsScreen },

    //Subscribe Message
    SubscribeMessage: { screen: SubscribeMessageScreen },

    WebView: { screen: WebViewScreen },
  },
  {
    headerMode: "none",
    // contentComponent: props => <DrawerMenu {...props} />
  }
);


const prevGetStateForActionHomeStack = AppStackNavigator.router.getStateForAction;

AppStackNavigator.router.getStateForAction = (action, state) => {
  if (state && action.type === "maxRoute") {
    let routes = state.routes;
    routes.push(action);
    if (routes.length > 5) {
      routes = routes.filter((val, index) => { if (index != 1 ) return true })
    }
    return {
      ...state,
      routes,
      index:routes.length - 1,
    };
  }
  // console.log("prevGetStateForActionHomeStack", action, state)
  return prevGetStateForActionHomeStack(action, state);
}


const AppNavigator = createDrawerNavigator(
  {
    NavStack: AppStackNavigator,
  },
  {
    headerMode: "none",
    drawerWidth: wp(70),
    contentComponent: (props) => <DrawerMenu {...props} />,
  }
);

const AppSwitchNavigator = createSwitchNavigator(
  {
    AuthLoading: SplashScreen,
    App: AppNavigator,
    Auth: AuthStack,
  },
  {
    headerMode: "none",
    initialRouteName: "AuthLoading",
  }
);

export default createAppContainer(AppSwitchNavigator);
