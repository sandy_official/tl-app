import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../styles/CommonStyles";
import DrawerMenuList from "./DrawerMenuList";
import { connect } from "react-redux";
import { doUserLogout } from "../actions/UserActions";
import { clearPortfolio } from "../actions/PortfolioActions";
import { clearWatchlistAll } from "../actions/WatchlistActions";
import { clearAsync } from "../controllers/AsyncController";
import { clearMarket } from "../actions/MarketActions";
import { setNavigationState } from "../utils/Constants";

class DrawerMenu extends React.PureComponent {
  _toggleDrawerMenu() {
    this.props.navigation.closeDrawer();
  }

  gotoOnboarding = () => {
    clearAsync();
    this.props.doUserLogout();
    this.props.clearPortfolio();
    this.props.clearWatchlistAll();
    this.props.clearMarket();

    setNavigationState(this.props.navigation.state);
    this.props.navigation.navigate("Onboard");
  };

  gotoPlans = () => {
    console.log("subscription clicked");
    this.props.navigation.navigate("Subscription");
  };

  renderProfile = () => {
    const { fullName, email, profilePhoto, isLoggedIn } = this.props.userState;
    const { bestOfferShortStr } = this.props.dashboard ? this.props.dashboard : {};

    const showPhoto = profilePhoto ? true : false;
    const name = isLoggedIn ? fullName : "";
    const nameFirstChar = typeof fullName == "string" && fullName.length > 0 ? fullName.charAt(0) : "";

    const showOffer = bestOfferShortStr ? true : false;

    return (
      <View style={styles.drawerHeader}>
        <View style={styles.userInfo}>
          <View style={styles.nameCircle}>
            <Text style={styles.firstNameLetter}>{nameFirstChar}</Text>
          </View>

          <View style={{ paddingHorizontal: wp(3) }}>
            <Text style={styles.userName}>{name + " "}</Text>
            <Text style={styles.userEmail}>{email + " "}</Text>
          </View>
        </View>

        {showOffer && (
          <Text style={styles.offer} onPress={this.gotoPlans}>
            {bestOfferShortStr} <Text style={styles.seePlans}>See all plans</Text>
          </Text>
        )}
      </View>
    );
  };

  renderNewUser = () => {
    const logoIcon = require("../assets/icons/logo-white.png");

    return (
      <View style={styles.drawerHeader}>
        <Image source={logoIcon} style={styles.trendlyne} />
        <Text style={styles.trendlyneDesc}>
          Get started with Trendlyne and get all the tools you need for investing smartly!
        </Text>

        <View style={styles.btnRow}>
          <TouchableOpacity style={styles.registerBtn} onPress={this.gotoOnboarding}>
            <Text style={styles.register}>Register </Text>
          </TouchableOpacity>

          <TouchableOpacity style={styles.loginBtn} onPress={this.gotoOnboarding}>
            <Text style={styles.login}>Login </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  render() {
    const { isLoggedIn } = this.props.userState;

    return (
      <View style={styles.container}>
        {isLoggedIn ? this.renderProfile() : this.renderNewUser()}
        <ScrollView>
          <DrawerMenuList {...this.props} />
        </ScrollView>
      </View>
    );
  }
}

mapStateToProps = (state) => ({
  userState: state.user,
  dashboard: state.user.dashboard,
});

export default connect(mapStateToProps, { doUserLogout, clearPortfolio, clearWatchlistAll, clearMarket })(DrawerMenu);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  menu: {
    flex: 1,
  },
  nameCircle: {
    backgroundColor: colors.white,
    width: wp(12),
    height: wp(12),
    borderRadius: wp(6),
    alignItems: "center",
    justifyContent: "center",
  },
  firstNameLetter: {
    fontSize: RF(3.2),
    color: colors.primary_color,
  },
  drawerHeader: {
    backgroundColor: colors.primary_color,
    // minHeight: hp(22),
    marginBottom: wp(2),
  },
  userInfo: {
    flexDirection: "row",
    paddingHorizontal: wp(4.2),
    paddingVertical: wp(5.2),
  },
  userName: {
    fontSize: RF(2.4),
    letterSpacing: 0,
    color: colors.white,
  },
  userEmail: {
    width: wp(50),
    fontSize: RF(1.9),
    color: colors.white,
    marginTop: 5,
    textAlign: "justify",
  },
  offer: {
    backgroundColor: colors.lightNavy,
    paddingVertical: wp(3.2),
    paddingHorizontal: wp(5.2),
    fontSize: RF(1.9),
    color: colors.white,
    marginTop: wp(3),
  },
  seePlans: {
    fontWeight: "500",
    textDecorationLine: "underline",
  },
  trendlyne: {
    height: wp(7),
    resizeMode: "contain",
    marginHorizontal: wp(4.2),
    marginTop: wp(5.2),
  },
  trendlyneDesc: {
    fontSize: RF(1.9),
    color: colors.white,
    paddingHorizontal: wp(4.2),
    marginTop: wp(3.5),
    marginBottom: wp(4),
  },
  registerBtn: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderStyle: "solid",
    borderWidth: 1.3,
    borderColor: colors.white,
    marginRight: wp(2),
    padding: 4.3,
  },
  register: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
    padding: 5.6,
  },
  loginBtn: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginLeft: wp(2),
    borderRadius: 5,
    backgroundColor: colors.white,
    padding: 5.6,
  },
  login: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.primary_color,
    padding: 6.6,
  },
  btnRow: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp(4.2),
    marginBottom: wp(3.2),
  },
});
