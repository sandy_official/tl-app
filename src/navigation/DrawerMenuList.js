import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../styles/CommonStyles";
import { doUserLogout } from "../actions/UserActions";
import { doLogoutApi } from "../controllers/UserController";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import LoadingView from "../components/common/LoadingView";
import { clearPortfolio } from "../actions/PortfolioActions";
import { clearWatchlistAll } from "../actions/WatchlistActions";
import { clearAsync } from "../controllers/AsyncController";
import { clearMarket } from "../actions/MarketActions";
import { GoogleSignin } from "react-native-google-signin";
import { setNavigationState } from "../utils/Constants";
import { LoginManager, AccessToken } from "react-native-fbsdk";

const menu = [
  {
    name: "Home",
    action: "Home",
    icon: require("../assets/icons/dashboard/home-dashboard.png"),
  },
  {
    name: "Markets",
    icon: require("../assets/icons/dashboard/market-dashboard.png"),
    options: [
      { option: "Stocks", action: "Market", extras: { tabIndex: 0 } },
      // { option: "Indices", action: "Market" },
      { option: "Insider Trades", action: "Market", extras: { tabIndex: 1 } },
      { option: "Bulk Block Deals", action: "Market", extras: { tabIndex: 2 } },
      { option: "Trending News", action: "Market", extras: { tabIndex: 3 } },
    ],
  },
  {
    name: "Screeners",
    action: "Screener",
    icon: require("../assets/icons/dashboard/screener-dashboard.png"),
  },
  {
    name: "Superstars",
    action: "Superstar",
    icon: require("../assets/icons/dashboard/superstar-dashboard.png"),
  },
  {
    name: "Portfolio",
    action: "Portfolio",
    icon: require("../assets/icons/dashboard/portfolio-dashboard.png"),
    options: [
      { option: "Summary", action: "Portfolio", extras: { tabIndex: 0 } },
      { option: "All Holdings", action: "Portfolio", extras: { tabIndex: 1 } },
      { option: "Watchlist", action: "Portfolio", extras: { tabIndex: 2 } },
      { option: "Realized P&L", action: "Portfolio", extras: { tabIndex: 3 } },
    ],
  },
  {
    name: "Research Reports",
    action: "Reports",
    icon: require("../assets/icons/dashboard/reports-dashboard.png"),
  },
  {
    name: "News",
    icon: require("../assets/icons/dashboard/news-dashboard.png"),
    options: [
      { option: "Trending News", action: "Market", extras: { tabIndex: 3 } },
      { option: "My News", action: "Alerts", extras: { tabIndex: 3 } },
    ],
  },
  {
    name: "Alerts",
    icon: require("../assets/icons/dashboard/alert-dashboard.png"),
    options: [
      { option: "Stocks", action: "Alerts", extras: { tabIndex: 0 } },
      { option: "Screeners", action: "Alerts", extras: { tabIndex: 1 } },
      { option: "Superstars", action: "Alerts", extras: { tabIndex: 2 } },
      { option: "News", action: "Alerts", extras: { tabIndex: 3 } },
      { option: "Notifications", action: "Alerts", extras: { tabIndex: 4 } },
    ],
  },
  {
    name: "Subscription Plans",
    action: "Subscription",
    icon: require("../assets/icons/premium-icon-blue.png"),
  },
  {
    name: "Support",
    action: "ContactUs",
    icon: require("../assets/icons/dashboard/support-dashboard.png"),
  },
  {
    name: "Logout",
    icon: require("../assets/icons/dashboard/logout-dashboard.png"),
    action: "Onboard",
    color: colors.primary_color,
  },
];

class DrawerMenuList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { selectedOption: "", refreshing: false };
  }

  // doGoogleSignOut = async () => {
  //   let isSignedIn = await GoogleSignin.isSignedIn();
  //   if (isSignedIn) await GoogleSignin.signOut();
  // };

  // doFacebookLogout = async () => {
  //   try {
  //     AccessToken.getCurrentAccessToken().then(
  //       (data) => {
  //         if (data) {
  //           LoginManager.logOut();
  //         }
  //       } //Refresh it every time
  //     );
  //   } catch (e) {
  //     console.log("facebook logout error", e);
  //   }
  // };

  doLogout = (obj) => {
    const {
      navigation,
      doUserLogout,
      userState,
      clearPortfolio,
      clearWatchlistAll,
      clearMarket,
      baseURL,
      appToken,
    } = this.props;

    this.setState({ refreshing: true });

    doLogoutApi(baseURL, appToken, userState.token, (response) => {
      this.setState({ refreshing: false });

      if (response) {
        // this.doGoogleSignOut();
        // this.doFacebookLogout();
        clearAsync();
        doUserLogout();
        clearPortfolio();
        clearWatchlistAll();
        clearMarket();
        navigation.closeDrawer();
        setNavigationState(null);
        navigation.navigate(obj.action);
      } else this.refs.toast.show("Please try again");
    });
  };

  gotoScreen = (obj) => {
    const { navigation } = this.props;

    if (obj.action != null) {
      if (obj.name == "Logout") this.doLogout(obj);
      else {
        try {
          let navStack = navigation.state.routes[0].routes;
          console.log("navStack", navigation);

          if (navStack[navStack.length - 1].routeName == obj.action) {
            navigation._childrenNavigation.NavStack.replace({
              routeName: obj.action,
              params: obj.extras ? obj.extras : {},
            });
          } else {
            navigation.navigate({ routeName: obj.action, params: obj.extras ? obj.extras : {} });
            navigation.navigate(obj.action);
          }

          this.setState({ selectedOption: "" });
          navigation.closeDrawer();
        } catch (e) {
          console.warn("Drawer menu navStack error");
        }
      }
    }
  };

  selectDropdown = (name) => {
    const { selectedOption } = this.state;

    if (selectedOption == name) this.setState({ selectedOption: "" });
    else this.setState({ selectedOption: name });
  };

  renderDropdown = (item) => {
    const isSelected = this.state.selectedOption == item.name;
    const dropdownIcon = isSelected
      ? require("../assets/icons/arrow-up-blue.png")
      : require("../assets/icons/arrow-down-blue.png");

    return (
      <View style={styles.dropdown}>
        <TouchableOpacity style={styles.dropdownHeader} onPress={() => this.selectDropdown(item.name)}>
          <Image source={item.icon} style={styles.dropdownImage} />
          <Text style={styles.dropdownName}>{item.name}</Text>
          <Image source={dropdownIcon} style={styles.dropdownArrow} />
        </TouchableOpacity>

        {isSelected &&
          item.options.map((opt) => {
            return (
              <TouchableOpacity onPress={() => this.gotoScreen(opt)}>
                <Text style={styles.dropdownItemText}>{opt.option}</Text>
              </TouchableOpacity>
            );
          })}
      </View>
    );
  };

  renderMenuItem = (item) => {
    const textStyle = item.color ? [styles.dropdownName, { color: item.color }] : styles.dropdownName;

    return (
      <TouchableOpacity style={[styles.dropdown, styles.dropdownHeader]} onPress={() => this.gotoScreen(item)}>
        <Image source={item.icon} style={styles.dropdownImage} />
        <Text style={textStyle}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  render() {
    const { isLoggedIn } = this.props.userState;
    const { refreshing } = this.state;

    return (
      <View style={styles.container}>
        {menu.map((item) => {
          if (item.name == "Logout") return isLoggedIn ? this.renderMenuItem(item) : null;
          else return "options" in item ? this.renderDropdown(item) : this.renderMenuItem(item);
        })}

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, {
  doUserLogout,
  clearPortfolio,
  clearWatchlistAll,
  clearMarket,
})(DrawerMenuList);

const styles = StyleSheet.create({
  container: {
    paddingBottom: wp(10),
  },
  dropdown: {
    marginVertical: wp(1.4),
    paddingHorizontal: wp(4.2),
  },
  dropdownHeader: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: wp(2.6),
  },
  dropdownImage: {
    width: wp(5.5),
    height: wp(6.5),
    resizeMode: "contain",
  },
  dropdownName: {
    flex: 1,
    fontSize: RF(2.4),
    color: colors.greyishBrownTwo,
    paddingHorizontal: wp(4.2),
  },
  dropdownArrow: {
    width: wp(5),
    height: wp(3.3),
    resizeMode: "contain",
  },
  dropdownItemText: {
    fontSize: 14,
    letterSpacing: 0,
    color: colors.greyishBrownTwo,
    paddingHorizontal: wp(4.2),
    marginLeft: wp(8),
    marginVertical: wp(2.6),
  },
});
