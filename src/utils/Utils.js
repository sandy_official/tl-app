import AsyncStorage from "@react-native-community/async-storage";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors } from "../styles/CommonStyles";
import { userSubscribed, EMAIL_REGEX, MONTHS_SHORT } from "./Constants";

export const getFetchOptions = (appToken, token, cb) => {
  let fetchOptions = {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      // KEY: isUserSubscribed() ? SUBSCRIBED_USER_TOKEN : NORMAL_USER_TOKEN,
      deviceid: "62c7c7042511c086",
      requestCode: "TLmappv1",
      TrendlyneAppToken: appToken,
    },
  };

  if (token != null) fetchOptions.headers.KEY = token;

  cb(fetchOptions);
};

// export const checkCookies = (response) => {
// console.log(response.headers.map["set-cookie"]);
// getCookie((cookie) => {
//   if (cookie == "" || cookie == null) {
// setCookie(response.headers.map["set-cookie"]);
// for (const [name, value] of response.headers) {
//   if (name === "set-cookie") {
//     setCookie(value);
//   }
// }
// }
// });

// getToken((token) => {
//   if (token == null) {
//     let newToken = extractToken(response);
//     // console.log("New token is:" + newToken);

//     if (newToken != null && newToken != undefined) storeToken(newToken);
//     // else console("Failed to store csrf token");
//   }
// });
// };

// export const isUserSubscribed = () => {
//   return userSubscribed;
// };

export const getDVMBackgroundColor = (value) => {
  const colorBlock = {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.green,
    margin: 3,
    borderRadius: 2,
  };

  if (value == "positive") return [colorBlock, { backgroundColor: colors.green }];
  else if (value == "negative") return [colorBlock, { backgroundColor: colors.red }];
  else if (value == "neutral") return [colorBlock, { backgroundColor: colors.yellow }];
  else return [colorBlock, { backgroundColor: colors.steelGrey }];
};

export const getOrdinalNumForDate = (n) => {
  return n + (n > 0 ? ["th", "st", "nd", "rd"][(n > 3 && n < 21) || n % 10 > 3 ? 0 : n % 10] : "");
};

export const getFormattedDate = (str) => {
  if (str) {
    try {
      const d = new Date(str);
      let day = d.getDate();
      let month = d.getMonth() + 1;
      let year = d.getFullYear();
      if (day < 10) day = "0" + day;
      if (month < 10) month = "0" + month;

      return day + "-" + month + "-" + year;
    } catch (e) {
      console.log("Screener date parse error ", e);
      return str;
    }
  }
};

export const getFormattedDateDdMmmYyyy = (str) => {
  if (str) {
    try {
      const d = new Date(str);
      let day = d.getDate();
      let month = d.getMonth();
      let year = d.getFullYear();
      if (day < 10) day = "0" + day;
      // if (month < 10) month = "0" + month;

      return day + " " + MONTHS_SHORT[month] + " " + year;
    } catch (e) {
      console.log("Screener date parse error ", e);
      return str;
    }
  }
};

export const formatDateForNews = (str) => {
  if (str) {
    try {
      const d = new Date(str);
      let day = d.getDate();
      let month = MONTHS_SHORT[d.getMonth()];
      let year = d.getFullYear();
      var hours = d.getHours();
      var minutes = d.getMinutes();
      var ampm = hours >= 12 ? "PM" : "AM";

      if (day < 10) day = "0" + day;

      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      //  if (month < 10) month = "0" + month;

      return day + " " + month + " " + year + ", " + hours + ":" + minutes + ampm;
    } catch (e) {
      console.log("Screener date parse error ", e);
      return str;
    }
  }
};

export const dateStringFromObj = (res_Date) => {
  let day, year, monthApi, return_date;
  day = res_Date.getDate();
  year = res_Date.getFullYear();
  monthApi = res_Date.getMonth() + 1 < 10 ? "0" + (res_Date.getMonth() + 1) : res_Date.getMonth() + 1;
  return_date = year + "-" + monthApi + "-" + day;
  return return_date;
};

export const getFormattedDateMmDdYyyy = (value) => {
  // console.log("qtr_end_date", value);
  if (typeof value == "string") {
    const date = value.split("-");
    let formattedValue = MONTHS_SHORT[date[1] - 1] + " " + date[2] + ", " + date[0];
    return formattedValue ? formattedValue : " - ";
  } else return " - ";
};

export const sortObjectList = (objList, key, isAscending) => {
  return objList && Array.isArray(objList)
    ? [...objList].sort(function (a, b) {
        if (a[key] == null) return isAscending ? -1 : 1;
        if (b[key] == null) return isAscending ? 1 : -1;

        if (isNaN(a[key]) && isNaN(b[key]))
          return isAscending ? a[key].localeCompare(b[key]) : b[key].localeCompare(a[key]);
        else return isAscending ? a[key] - b[key] : b[key] - a[key];
      })
    : objList;
};

export const parseTableColumns = (column) => {
  return {
    unique_name: column.unique_name,
    name: column.name,
    type: column.type,
    unit: column.unit,
  };
};

export const parseScreenData = (screen) => {
  return {
    pk: screen.screenId,
    description: screen.description,
    title: screen.title,
    name: screen.name,
  };
};

export const validateEmail = (email) => {
  var pattern = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return pattern.test(email);
};

export const validateMobileNumber = (mobileNumber) => {
  var pattern = /^([+]\d{2})?\d{10}$/;
  return pattern.test(mobileNumber);
};

export const validatePinCode = (pinCode) => {
  var pattern = /^[1-9][0-9]{5}$/;
  return pattern.test(pinCode);
};

export const validateGstNumber = (gstCode) => {
  let pattern = /\d{2}[A-Z]{5}\d{4}[A-Z]{1}[A-Z\d]{1}[Z]{1}[A-Z\d]{1}/;
  return pattern.test(gstCode);
};

/**
 * format number to comma separated indian currency format
 * @param {number} number
 */
export const formatNumberToInr = (num, roundNum) => {
  let formattedNumber = num;
  let number = parseFloat(num) < 0 ? parseFloat(num) * -1 : parseFloat(num);
  //   let number = num < 0 ? num * -1 : num;
  let label = "";

  if (number >= 1000 && number < 100000) {
    formattedNumber = number / 1000;
    label = "K";
  } else if (number >= 100000 && number < 10000000) {
    formattedNumber = number / 100000;
    label = "L";
  } else if (number >= 10000000) {
    formattedNumber = number / 10000000;
    label = "Cr";
  }

  if (roundNum) formattedNumber = roundNumber(formattedNumber);

  if (num < 0 && (formattedNumber + "").indexOf("-") == -1) {
    return `-${numberWithCommas(formattedNumber)}${label}`;
  }

  return numberWithCommas(formattedNumber) + label;

  // return (
  //   formattedNumber.toLocaleString("en-IN", {
  //     maximumFractionDigits: 1,
  //   }) + label
  // );
};

// export const formetNumberWithSymbol = (num) => {
//   let formattedNumber = num;
//   let number = num < 0 ? num * -1 : num;
//   let label = "";

//   if (number >= 1000 && number < 100000) {
//     formattedNumber = number / 1000;
//     label = "K";
//   } else if (number >= 100000 && number < 10000000) {
//     formattedNumber = number / 100000;
//     label = " L";
//   } else if (number >= 10000000) {
//     formattedNumber = number / 10000000;
//     label = " Cr";
//   }

//   formattedNumber = Math.round(formattedNumber);

//   if (num < 0) return `-${numberWithCommas(formattedNumber)} ${label}`;
//   return numberWithCommas(formattedNumber) + label;
// };

export const formatVolume = (number) => {
  let formattedNumber = number;
  let label = "";

  if (number >= 1000 && number < 1000000) {
    formattedNumber = number / 1000;
    label = "K";
  } else if (number >= 1000000 && number < 1000000000) {
    formattedNumber = number / 1000000;
    label = "M";
  } else if (number >= 1000000000) {
    formattedNumber = number / 1000000000;
    label = "B";
  }

  return roundNumber(formattedNumber) + " " + label;
  // return formattedNumber.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, "$&,") + label;
};

export const formatNumber = (number) => {
  return number.toLocaleString("en-IN", {
    maximumFractionDigits: 1,
  });
};

export const truncateNumWithoutRounding = (num, dec) => {
  const calcDec = Math.pow(10, dec);
  return Math.trunc(num * calcDec) / calcDec;
};

export const numberWithCommas = (num, keepDecimals, roundNum) => {
  var n1, n2;
  if (roundNum) num = roundNumber(num);
  num = num + "" || "";
  n1 = num.split(".");
  if (keepDecimals) n2 = n1[1] ? n1[1] : null;
  else n2 = n1[1] ? n1[1].slice(0, 1) : null;
  n1 = n1[0].replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
  num = n2 ? n1 + "." + n2 : n1;
  return num;
  // return x.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
  // return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  // return x.toFixed(1).replace(/\d(?=(\d{3})+\.)/g, "$&,");
};

export const formatQuantity = (n) => {
  return n.toString().replace(/(\d)(?=(\d\d)+\d$)/g, "$1,");
};

export const checkTimestampDifference = (timestamp) => {
  try {
    let formattedTimestamp = isNaN(timestamp) ? parseInt(timestamp) : timestamp;
    var difference = Date.now() - formattedTimestamp;
    var daysDifference = Math.floor(difference / 1000 / 60 / 60 / 24);
    return daysDifference;
  } catch (e) {
    console.log(e);
    return 0;
  }
};

export const isValidEmail = (email) => {
  return EMAIL_REGEX.test(email);
};

export const mapHeadersData = (headers, data) => {
  let dataList = [];

  if (headers && data) {
    data.map((dataItem) => {
      let dataObj = {};
      dataItem.map((item, i) => {
        if (headers[i] != undefined) {
          let colName = "unique_name" in headers[i] ? headers[i].unique_name : headers[i].name;
          dataObj[colName] = item;
        }
      });

      dataList.push(dataObj);
    });
  }
  return dataList;
};

export const roundNumber = (num) => {
  return num ? Math.round(num * 10) / 10 : num;
};
