export const ALERTS_CATEGORIES = [
    {
      name: "All Active Alerts",
      icon: require("../assets/icons/alerts/alertsIcon.png"),
      alertType:"All Active Alerts"
    },
    {
      name: "Price",
      icon: require("../assets/icons/alerts/priceIcon.png"),
      alertType:"price"
    },
    {
      name: "Moving Averages",
      icon: require("../assets/icons/alerts/movingAveragesIcon.png"),
      alertType:"sma"
    },
    {
      name: "Volume",
      icon: require("../assets/icons/alerts/volumeIcon.png"),
      alertType:"volume"
    },
    {
      name: "Research Reports",
      icon: require("../assets/icons/alerts/reportsIcon.png"),
      alertType:"research_report"
    }
];