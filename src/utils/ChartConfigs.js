import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../styles/CommonStyles";

export const CHART_OPTIONS = { global: { useUTC: false }, lang: { decimalPoint: ".", thousandsSep: "," } };

export const BAR_CHART_CONFIG = {
  chart: { type: "column" },
  title: { text: "" },
  yAxis: { title: false, labels: { enabled: true } },
  xAxis: [
    { categories: [], labels: { style: { fontSize: RF(1.3) } } },
    {
      categories: [],
      linkedTo: 0,
      opposite: true,
    },
  ],
  plotOptions: {
    series: {
      pointWidth: wp(10),
      borderRadiusTopLeft: "50%",
      borderRadiusTopRight: "50%",
      // dataLabels: {
      //   enabled: true,
      //   inside: false,
      //   crop: false,
      //   overflow: "none"
      // }
    },
    column: { stacking: "normal" },
  },
  credits: { enabled: false },
  series: [
    { showInLegend: false, data: [], stack: "holding", color: "rgba(50,100,50,0.1)" },
    // { dataLabels: [{ align: "top", format: "({point})" }], showInLegend: false, data: [], stack: "holding", color: "" },
    // { showInLegend: false, stack: "holding", data: [] }
  ],
  exporting: { enabled: false },
  tooltip: { pointFormat: "{point.y}" },
};

export const LINE_CHART_CONFIG = {
  chart: { type: "line" },
  title: { text: "" },
  yAxis: { title: false },
  xAxis: { categories: [], labels: { style: { fontSize: RF(1.4) } } },
  tooltip: { shared: true, crosshairs: true },
  plotOptions: { series: { showInLegend: false, dataLabels: { enabled: true } } },
  credits: { enabled: false },
  series: [
    { dataLabels: [{ format: "{point.label}" }] },
    { enableMouseTracking: true, data: [], color: colors.primary_color },
  ],
  exporting: { enabled: false },
};

export const FUNDAMENTAL_CHART_CONFIG = {
  chart: { type: "column" },
  title: { text: "" },
  yAxis: { title: false, labels: { enabled: true } },
  xAxis: [{ categories: [], labels: { style: { fontSize: RF(1.3) } } }],
  plotOptions: {
    series: {
      pointWidth: wp(10),
      borderRadiusTopLeft: "50%",
      borderRadiusTopRight: "50%",
      // dataLabels: {
      //   enabled: true,
      //   inside: false,
      //   crop: false,
      //   overflow: "none"
      // }
    },
    column: { stacking: "normal" },
  },
  credits: { enabled: false },
  series: [
    { showInLegend: false, data: [], stack: "holding", color: "rgba(50,100,50,0.1)" },
    { dataLabels: [{ align: "top", format: "({point})" }], showInLegend: false, data: [], stack: "holding", color: "" },
    { showInLegend: false, stack: "holding", data: [] },
  ],
  exporting: { enabled: false },
  tooltip: { pointFormat: "{point.y}" },
};

export const STOCK_OVERVIEW_CHART_CONFIG = {
  chart: { type: "area" },
  yAxis: [
    { tickPixelInterval: wp(10), min: 0, labels: { align: "left" }, resize: { enabled: true } },
    { labels: { enabled: false, align: "left" }, offset: 0 },
  ],
  rangeSelector: {
    buttons: [
      { type: "day", count: 1, text: "1d" },
      { type: "week", count: 1, text: "1w" },
      { type: "month", count: 1, text: "1m" },
      { type: "month", count: 3, text: "3m" },
      { type: "month", count: 6, text: "6m" },
      { type: "year", count: 1, text: "1y" },
      { type: "max", count: 1, text: "max" },
    ],
    inputEnabled: false,
    buttonTheme: { visibility: "hidden" },
    labelStyle: { visibility: "hidden" },
  },
  scrollbar: { enabled: false },
  credits: { enabled: false },
  exporting: { enabled: false },
  plotOptions: {
    series: { lineWidth: 2 },
    area: { marker: { enabled: false, symbol: "circle", radius: 2, states: { hover: { enabled: true } } } },
  },
  tooltip: {
    followTouchMove: true,
  },
};
