export const deepLinking = (url) => {
  let urlData = url.indexOf("?") != -1 ? url.split("?") : [url];
  let path =
    url.indexOf("https://") != -1
      ? urlData[0].replace("https://trendlyne.com", "")
      : urlData[0].replace("app://trendlyne.com", "");

  let params = url.indexOf("?") != -1 ? extractURLParams(urlData[1]) : {};
  console.log("path url", path, params);

  switch (path) {
    case "":
      return { screen: "Home" };

    /** Market */
    case "/market":
      return { screen: "Market" };

    // case "/market-search":
    //   return { screen: "MarketSearch" };

    /** Superstars */
    case "/portfolio/superstar-shareholders/index":
      return { screen: "Superstar" };

    /** Subscription */
    case "/subscription":
      return { screen: "Subscription" };

    /** Reports */
    case "/research-reports":
      return { screen: "Reports" };

    case "/research-reports/broker":
      return { screen: "ReportDetail", params: params };

    /** Screeners */
    case "/screeners/category":
      return { screen: "Screener" };

    case "/screeners/list":
      return { screen: "ScreenerList", params: params };

    case "/screeners":
      return { screen: "ScreenerStocks", params: params };

    /** Superstars */
    case "/superstars":
      if (Object.keys(params).length == 0) return { screen: "Superstar" };
      return { screen: "SuperstarInfo", params: params };

    /** Contact */
    case "/contact":
      return { screen: "ContactUs" };

    case "/stocks":
      return { screen: "Stocks", params: params };

    default:
      return { screen: "WebView", params: { url: url } };
  }
};

const extractURLParams = (urlParams) => {
  let params = {};
  urlParams.split("&").map((item, i) => {
    let data = item.indexOf(":") != -1 ? item.split(":") : item.indexOf("=") != -1 ? item.split("=") : item;

    if (Array.isArray(data)) {
      params[data[0]] =
        typeof data[1] == "string" && data[1].indexOf("%20") != -1 ? getFormattedString(data[1]) : data[1];
    }
  });
  return params;
};

getFormattedString = (str) => {
  return typeof str == "string" ? str.replace(/%20/g, " ") : str;
};
