/**
 * Login
 */
export const EMAIL_LOGIN_API_URL = "auth/login/";
export const EMAIL_SIGN_UP_API_URL = "auth/registration/";
export const GOOGLE_LOGIN_API_URL = "auth/google/";
export const FACEBOOK_LOGIN_API_URL = "auth/facebook/";
export const LOGOUT_API_URL = "auth/logout/";

/**
 * User
 */
export const USER_INFO_API_URL = "userinfo/";
export const USER_DASHBOARD_API_URL = "dashboard/";

/**
 * Search
 */
export const SEARCH_API_URL = "search/";

/**
 * Market section
 */
export const GET_CUSTOM_PARAMETER_URL = "getCustomParameters/";
export const GET_MARKET_DATA_URL = "marketsData/";

/**
 * Screener section
 */
export const SCREENER_STOCKS_URL = "screener/";
export const SCREENER_WATCHLIST_STOCKS_URL = "screener/user/";
export const MY_SCREENERS_URL = "screeners/list/myscreeners/";
export const BOOKMARKED_SCREENER_URL = "screeners/list/bookmark/";
export const SCREENERS_BY_TAG_URL = "screeners/list/tags/";
export const GET_ACTIVE_SCREENER_ALERTS_URL = "screeners/alert/get-active/";
export const SCREENER_BOOKMARK_STATUS_URL = "screeners/bookmark/status/";
export const ADD_SCREENER_BOOKMARK_URL = "screeners/bookmark/add/";
export const DELETE_SCREENER_BOOKMARK_URL = "screeners/bookmark/delete/";
export const MODIFY_SCREENER_ALERTS_URL = "screeners/alert/modify/";
export const CANDLESTICK_DESCRIPTION_URL = "descriptions/";

/**
 * Superstar Section
 */
export const GET_ACTIVE_SUPERSTAR_ALERTS_URL = "superstar/alert/get-active/";
export const GET_SUPERSTAR_LIST = "getSuperStarIndex/";
export const SUPERSTAR_PORTFOLIO_URL = "superStarPortfolio/";
export const SUPERSTAR_BB_DEALS_URL = "superstar/bulk-block-deals/";
export const SUPERSTAR_IT_SAST_DEALS_URL = "superstar/insider-trading-sast/";
export const SET_SUPERSTAR_ALERTS = "superstar/alert/add-remove/";
export const HOLDING_LOOKUP_URL = "holding-lookup/";

/**
 * Stock Details section
 */
// Overview
export const STOCK_OVERVIEW_URL = "stock/overview/";
export const STOCK_OHLC_CHART_URL = "stock/ohlc/";
// Fundamental
export const STOCK_FUNDAMENTAL_URL = "stock/fundamental/";
// Corporate Actions
export const STOCK_DIVIDEND_URL = "stock/dividend/";
export const STOCK_BONUS_SPLIT_URL = "stock/bonussplit/";
export const STOCK_BOARD_MEETINGS_URL = "stock/board-meetings/";
// Deals
export const GET_STOCK_BULK_BLOCK_DEALS_URL = "stock/bulk-block-deals-stock/";
export const GET_STOCK_INSIDER_TRADES_DEALS_URL = "stock/insider-trading-sast-stock/";
// Stock news
export const STOCK_NEWSFEED_URL = "newsfeed/stock/";
export const BROKER_NEWSFEED_URL = "newsfeed/broker/";
export const CORPORATE_ANNOUNCEMENTS_NEWSFEED_URL = "newsfeed/corporate/announcements/";
// Reports
export const RESEARCH_REPORTS_BY_STOCK_URL = "research-reports/by-stock/";
// Shareholding
export const STOCK_SHAREHOLDING_URL = "stock/share-holding-page/";
export const STOCK_HOLDING_HISTORY_URL = "stock/holding/history/";
// Technical Analyses
export const STOCK_TECHNICAL_ANALYSIS_URL = "stock/technical-analysis/";

/**
 * Alerts section
 */

// Screener alerts
export const USER_ACTIVE_ALERTS_URL = "screeners/alert/user-active-alerts/";
export const SCREENER_ALERTS_HISTORY_URL = "screeners/alert/get-alert-history/";
export const GET_MY_ALERTS_URL = "alert/my-alerts/";

// Notifications
export const GET_MY_NOTIFICATIONS_URL = "notifications/my-notifications/";

/**
 * Portfolio
 */
export const GET_PORTFOLIO_LIST_URL = "portfolio/list/";
export const ADD_PORTFOLIO_URL = "portfolio/add/";
export const PORTFOLIO_ADD_HOLDING_URL = "portfolio/addHolding/";
export const PORTFOLIO_HOLDINGS_URL = "portfolio/holdings/"; //FOR ALL HOLDINGS.
export const PORTFOLIO_HOLDINGS_LIST_URL = "portfolio/holdings/list/"; //FOR DETAILS OF PERTICULAR HOLDING.
export const PORTFOLIO_HOLDINGS_SELLFIFO_URL = "portfolio/sellFIFO/"; //FOR SELLING FIFO OF PERTICULAR HOLDING.
export const PORTFOLIO_HOLDINGS_SELL_URL = "portfolio/sellHolding/"; //FOR SELLING HOLDINGS
export const PORTFOLIO_HOLDINGS_EDIT_URL = "portfolio/editHolding/"; //FOR EDITING HOLDINGS
export const PORTFOLIO_HOLDINGS_DELETE_URL = "portfolio/holding/delete/"; // FOR DELETING HOLDINGS

export const PORTFOLIO_SUMMARY_URL = "portfolio/summary/"; //FOR GETTING SUMMARY CONSOLIDATED DATA
export const PORTFOLIO_ANALYSIS_URL = "portfolio/analysis/"; //FOR GETTING SUMMARY CONSOLIDATED DATA

export const PORTFOLIO_TRANSACTIONS_URL = "portfolio/transactions/"; //Transactions list data.
export const DELETE_TRANSACTIONS_URL = "portfolio/transaction/delete/"; // For Selling transactions.
export const EDIT_TRANSACTION_URL = "portfolio/editTransaction/"; // Edit Transaction Api.

export const PORTFOLIO_ANALYSER_URL = "portfolio/analyser/"; // FOR CHART DATA OF SUMMARY

/**
 * Watchlist
 */
export const GET_ALL_WATCHLIST_URL = "watchlist/display/";
export const GET_ALL_WATCHLIST_WITH_COUNT_URL = "watchlist/display/with-count/";
export const GET_ACTIVE_WATCHLIST_URL = "watchlist/fetchactive/";
export const WATCHLIST_STOCK_LIST_URL = "watchlist/tabledata/"; //FOR WATCHLIST STOCK DETAIL.
export const WATCHLIST_EDIT_URL = "watchlist/edit/";
export const WATCHLIST_PUSH_URL = "watchlist/push/"; //FOR ADDNIG STOCKS TO WATCHLIST.
export const WATCHLIST_DELETE_URL = "watchlist/delete/"; //FOR DELETING PERTICULAR WATCHLIST.
export const PUSH_TO_WATCHLIST_URL = "watchlist/push/";
export const POP_FROM_WATCHLIST_URL = "watchlist/pop/";
export const CREATE_WATCHLIST_URL = "watchlist/add/";

/**
 * Contact
 */
export const CONTACT_URL = "contact/";

/**
 * News
 */
export const TRENDING_NEWS_URL = "trending/news/";
export const MY_NEWS_FEED_URL = "my-newsfeed/";

/**
 * Research reports
 */
export const REPORTS_BY_TYPE_URL = "research-reports/by-type/";
export const REPORTS_BY_BROKER_URL = "research-reports/by-broker/";
export const RESEARCH_REPORT_BROKER_LIST_URL = "research-reports/broker-list/";

/**
 * Subscription
 */
export const SUBSCRIPTION_PLAN_URL = "subscription/plans/";
export const START_TRANSACTION_URL = "subscription/startTransaction/";
export const SEND_TRANSACTION_SUCCESS_RESPONSE_URL = "subsciption/response/";
export const GET_SUBSCRIPTION_CUSTOMER_INFO_URL = "subscription/getCustomerInfo/";
export const SET_SUBSCRIPTION_CUSTOMER_INFO_URL = "subscription/setCustomerInfo/";

/**
 * Stock Alerts
 */
export const ACTIVE_STOCK_ALERTS_URL = "stock/alert/active/";
export const ADD_STOCK_ALERT_URL = "stock/alert/add/";
export const DELETE_STOCK_ALERT_URL = "stock/alert/delete/";
