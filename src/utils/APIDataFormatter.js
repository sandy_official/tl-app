import {
  numberWithCommas,
  formatNumber,
  getFormattedDate,
  truncateNumWithoutRounding,
  formatDateForNews,
  getFormattedDateDdMmmYyyy,
  formatVolume,
  formatNumberToInr,
  roundNumber,
} from "./Utils";
import { MONTHS_SHORT } from "./Constants";

export const APIDataFormatter = (key, value) => {
  switch (key) {
    case "average_price":
    case "currentPrice":
    case "current_p":
    case "dayChange":
    case "EMA":
    case "high":
    case "holding_after":
    case "holding_change":
    case "low":
    case "price":
    case "quantity":
    case "recoPrice":
    case "SMA":
    case "targetPrice":
    case "Total no. shares held":
      return value != null && value != undefined && !isNaN(value) ? numberWithCommas(value) : " - ";

    case "actor":
    case "actor_category_text":
    case "brokerName":
    case "client_name":
    case "Companies Held":
    case "currentPrice":
    case "description":
    case "get_action_display":
    case "get_deal_type_display":
    case "get_full_name":
    case "Holder's name":
    case "Percent Holding":
    case "period":
    case "postType":
    case "recoType":
    case "reportDescription":
    case "reportTitle":
    case "security_type_text":
    case "sstr":
    case "source":
    case "transaction_type_text":
    case "vol_day_times_vol_week_str":
      return value ? value : " - ";

    case "vol_day":
    case "cvol":
      return value != null && value != undefined && !isNaN(value) ? formatVolume(value) : " - ";

    // case "recoDate":
    //   return value ? getFormattedDate(value).replace(/-/g, "/") : " - ";

    case "day_change":
    case "priceChange":
      return value && !isNaN(value) ? (value >= 0 ? "+" + value : value) : " - ";

    case "dayChangeP":
    case "day_changeP":
    case "priceChangeP":
      return value && !isNaN(value) ? (value >= 0 ? "+" + value + "%" : value + "%") : " - ";

    case "holdingP_change":
    case "holding_percent":
    case "Holding Percent":
    case "traded_percent":
      return value != undefined && value != null ? roundNumber(value) + "%" : " - ";

    case "change":
      return value != undefined && value != null && !isNaN(value)
        ? value >= 0
          ? "+" + numberWithCommas(value)
          : numberWithCommas(value)
        : " - ";

    case "delivery_avg":
    case "upsidePercent":
      return value != undefined && value != null && !isNaN(value) ? roundNumber(value) + "%" : " - ";

    case "changeSinceReco":
      return value != undefined && value != null && !isNaN(value)
        ? Math.round(parseFloat(value) * 100) / 100 + "%"
        : " - ";

    case "changePercent":
      return value != null && !isNaN(value)
        ? value > 0
          ? " +" + roundNumber(value) + "%"
          : roundNumber(value) + "%"
        : " - ";

    case "shares_pledged_percentage":
      return !isNaN(value) ? value + "%" : " - ";

    case "net worth":
    case "Holding Value (Rs.)":
      return value != null && value != undefined && !isNaN(value) ? formatNumberToInr(value, true) : " - ";

    case "board_meet_date":
    case "exdate":
    case "record_date":
      return value ? formatHistoryDate(value) : " - ";

    case "date":
    case "report_date":
      return value ? formatTradeDate(value) : " - ";

    case "pubDate":
      return value ? formatDateForNews(value) : " - ";

    case "pubDateNewsReport":
    case "recoDate":
    case "addedDate":
      return value ? getFormattedDateDdMmmYyyy(value) : " - ";

    case "triggered":
      return value ? formatTriggerDate(value) : value;

    default:
      return value;
  }
};

const formatHistoryDate = (value) => {
  if (typeof value == "string") {
    const date = value.split("-");
    let month = MONTHS_SHORT[date[1] - 1] == "May" ? MONTHS_SHORT[date[1] - 1] + " " : MONTHS_SHORT[date[1] - 1] + ". ";

    return month + date[2] + ", " + date[0];
  } else return value;
};

const formetWatchlistStockDate = (value) => {
  let formettedDate; // given.-->27-Jan-2020
  let month;
  let date;
  if (value) {
    formettedDate = new Date(value);
    date = parseInt(formettedDate.getDate());
    date = date > 9 ? date : `0${date}`;
    month = parseInt(formettedDate.getMonth()) + 1;
    month = month > 9 ? month : `0${month}`;
    formettedDate = `${date}-${month}-${formettedDate.getFullYear()}`;
    return formettedDate; // expected-->27-01-2020
  } else {
    return value;
  }
};

const formatTradeDate = (value) => {
  if (typeof value == "string") {
    const date = value.split("-");
    return date[2] + " " + MONTHS_SHORT[date[1] - 1] + " " + date[0];
  } else value;
};

const formatTriggerDate = (value) => {
  let dateStr = formatDateForNews(value);
  let array = dateStr.split(" ");
  array[1] = array[1].charAt(0) + array[1].substring(1, 3).toLowerCase();

  return array.join(" ");
};
