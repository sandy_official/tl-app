export const RECENT_SCREENERS = "recent_screeners";

/**
 * Screener Filters
 */
export const FUNDAMENTAL_OPTIONS = [
  { id: 384, label: "Trendlyne Scores", premium: true },
  { id: 385, label: "Qtr. Results" },
  { id: 386, label: "Growth & Scores" },
  { id: 387, label: "Valuation" },
  { id: 388, label: "Income" },
  { id: 389, label: "Return Ratios" },
  { id: 390, label: "Cash Flow" },
];
export const TECHNICAL_OPTIONS = [
  { id: 391, label: "Moving Averages" },
  { id: 392, label: "MACD RSI MFI" },
  { id: 393, label: "Volume" },
  { id: 394, label: "52W Range" },
  { id: 395, label: "Others" },
];
export const SHARE_HOLDING_OPTIONS = [
  { id: 396, label: "Summary" },
  { id: 397, label: "Promoter Holding" },
  { id: 398, label: "FII Holdings" },
  { id: 399, label: "MF Holdings" },
  { id: 400, label: "Institutional Holdings" },
];
export const BROKER_REPORT_OPTIONS = [{ id: 401, label: "Summary " }];

/**
 * screener category list
 */
export const SCREENER_CATEGORIES = [
  {
    name: "My Screeners",
    description: "Screeners created by you",
    icon: require("../assets/icons/screeners/my-screeners.png"),
  },
  {
    name: "Bookmarks",
    description: "Screeners bookmarked by you",
    icon: require("../assets/icons/screeners/bookmark-screeners.png"),
  },
  {
    name: "Expert",
    description: "Multi-query screeners by experts, highest return DVM and most popular screeners",
    icon: require("../assets/icons/screeners/expert-screener.png"),
  },
  {
    name: "Moving Average",
    description: "Stocks crossing SMA, close to crossing their SMA or trading above their SMA",
    icon: require("../assets/icons/screeners/moving-average-screeners.png"),
  },
  {
    name: "Price/Volume",
    description: "Monitor highly traded stocks, top gainers and losers",
    icon: require("../assets/icons/screeners/price-volume-screeners.png"),
  },
  {
    name: "Fundamentals",
    description: "Screen stocks based on various fundamental patterns",
    icon: require("../assets/icons/screeners/fundamental-screeners.png"),
  },
  {
    name: "Shareholding",
    description: "Screen stocks by changes in shareholding by promoters, FPI/FFI, mutual funds and DII",
    icon: require("../assets/icons/screeners/shareholding-screeners.png"),
  },
  {
    name: "Technical",
    description:
      "Screeners tracking various technicals (including RSI, MFI, SMA/EMAs, breakouts) as well as momentum via Trendlyne's trademark Momentum Score.",
    icon: require("../assets/icons/screeners/technical-screeners.png"),
  },
  {
    name: "Delivery",
    description:
      "Screeners tracking delivery percentage over day, week and month as well as combined delivery and volume screeners.",
    icon: require("../assets/icons/screeners/delivery-screeners.png"),
  },
  {
    name: "Candle Sticks",
    description: "Screen stocks by candlestick patterns",
    icon: require("../assets/icons/screeners/candlestick-screeners.png"),
  },
  {
    name: "Red Flags",
    description: "Screeners tracking red flag issues such as auditor resignations, investigations, FDA warnings.",
    icon: require("../assets/icons/screeners/red-flags-screeners.png"),
  },
  // {
  //   name: "Future",
  //   description:
  //     "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  //   icon: require("../assets/icons/screeners/future-screeners.png")
  // },
  // {
  //   name: "Options",
  //   description:
  //     "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
  //   icon: require("../assets/icons/screeners/options-screeners.png")
  // }
];

/**
 * Static screener list
 */
export const SCREENERS = {
  candleStick: {
    bullish: [
      {
        title: "Dragonfly Doji (Bullish Reversal)",
        description: "",
        screenId: 5906,
      },
      {
        title: "Hammer (Bullish Reversal)",
        description: "",
        screenId: 5889,
      },
      {
        title: "Inverted Hammer (Bullish Reversal)",
        description: "",
        screenId: 5885,
      },
      {
        title: "White Marubozu Candlestick (Bullish)",
        description: "",
        screenId: 5899,
      },
      {
        title: "Bullish Harami Cross (Bullish Reversal)",
        description: "",
        screenId: 5887,
      },
      {
        title: "Bullish Engulfing Pattern (Bullish Reversal)",
        description: "",
        screenId: 5892,
      },
      {
        title: "Piercing Line (Bullish Reversal)",
        description: "",
        screenId: 5897,
      },
      {
        title: "Bullish Harami (Bullish Reversal)",
        description: "",
        screenId: 5905,
      },
      {
        title: "Upside Tasuki Gap (Bullish Continuation)",
        description: "",
        screenId: 5888,
      },
      {
        title: "Morning Star (Bullish Reversal)",
        description: "",
        screenId: 5898,
      },
      {
        title: "Three White Soldiers (Bullish Reversal)",
        description: "",
        screenId: 5902,
      },
      {
        title: "Bullish Kicking (Bullish Reversal)",
        description: "",
        screenId: 5891,
      },
      {
        title: "Abandoned Baby Bottom or Bullish Abandoned Baby (Bullish Reversal)",
        description: "",
        screenId: 5890,
      },
    ],
    bearish: [
      {
        title: "Black Marubozu (Bearish)",
        description: "",
        screenId: 5896,
      },
      {
        title: "Hanging Man (Bearish Reversal)",
        description: "",
        screenId: 5901,
      },
      {
        title: "Shooting Star (Bearish Reversal)",
        description: "",
        screenId: 5904,
      },
      {
        title: "Bearish Harami Cross (Bearish Reversal)",
        description: "",
        screenId: 5886,
      },
      {
        title: "Bearish Engulfing (Bearish Reversal)",
        description: "",
        screenId: 5893,
      },
      {
        title: "Bearish Harami (Bearish Reversal)",
        description: "",
        screenId: 5894,
      },
      {
        title: "Darkcloud cover (Bearish Reversal)",
        description: "",
        screenId: 5895,
      },
      {
        title: "Downside Tasuki Gap (Bearish Continuation)",
        description: "",
        screenId: 5900,
      },
      {
        title: "Abandoned Baby Top or Bearish Abandoned Baby (Bearish Reversal)",
        description: "",
        screenId: 5903,
      },
      {
        title: "Identical Three Crows (Bearish Reversal)",
        description: "",
        screenId: 5884,
      },
    ],
  },
  delivery: [
    {
      title: "Rising Delivery Percentage Compared to Previous Day and Month, Strong Volumes",
      description: "Rising Delivery Percentage Compared to Previous Day and Month, Strong Volumes",
      screenId: 9589,
    },

    {
      title: "High Delivery Percentage",
      description: "Stocks with high delivery percentage EOD",
      screenId: 9588,
    },
    {
      title: "Rising Delivery Percentage compared to Previous Day and Month, and Rising Momentum Score (Subscription)",
      description: "Stocks which are seeing rising delivery percentage, strong volumes, and gaining in momentum",
      screenId: 10663,
    },
    {
      title: "Delivery Screener: 20% Higher Delivery over the Week compared to Month",
      description: "Delivery Screener: 20% Higher Delivery over the Week compared to Month",
      screenId: 10118,
    },
    {
      title: "Rising Delivery Percentage Compared to Prev Day",
      description: "Stocks which are seeing rising delivery percentage compared to previous day",
      screenId: 9844,
    },
  ],
  technical: {
    bullish: [
      {
        title: "Relative Strength Index (RSI) bullish",
        description:
          "Stocks with Relative Strength Index (RSI) above 70 are considered overbought. This implies that stock may show pullback. Some traders, in an attempt to avoid false signals from the RSI, use more extreme RSI values as buy or sell signals, such as RSI readings above 80 to indicate overbought conditions and RSI readings below 20 to indicate oversold conditions. The investing strategy for this screener is higher risk.",
        screenId: 5388,
      },
      {
        title: "MACD Crossover Above Signal Line",
        description: "Stocks with MACD Crossover Above",
        screenId: 22717,
      },
      {
        title: "Overbought by Money Flow Index (MFI)",
        description:
          "Technical Analysis: Stocks with Money Flow Index (MFI) above 80 are considered overbought. This implies that stock may show pullback. * MFI values shown have been calculated at the end of the day.",
        screenId: 5393,
      },

      {
        title: "Overbought on both MFI and RSI",
        description: "These stocks are overbought on both RSI and MFI and may shift out of bullishness soon",
        screenId: 27,
      },

      {
        title: "Weekly Momentum Gainers",
        description: "This screener shows stocks that are gaining in momentum (buying interest).",
        screenId: 8008,
      },
    ],
    bearish: [
      {
        title: "Relative Strength Index (RSI) bearish",
        description:
          "Technical Analysis: Stocks with Relative Strength Index (RSI) below 30 are considered oversold. This implies that stock may rebound. Some traders, in an attempt to avoid false signals from the RSI, use more extreme RSI values as buy or sell signals, such as RSI readings above 80 to indicate overbought conditions and RSI readings below 20 to indicate oversold conditions.",
        screenId: 5389,
      },

      {
        title: "Oversold by Money Flow Index (MFI)",
        description:
          "Technical Analysis: Stocks with Money Flow Index (MFI) below 20 are considered oversold. This implies that stock may rebound. * MFI values shown have been calculated at the end of the day.",
        screenId: 5394,
      },

      {
        title: "Oversold on both RSI and MFI",
        description: "These stocks may show price turnaround since they are oversold on both RSI and MFI",
        screenId: 28,
      },

      {
        title: "MACD Crossover Below Signal Line",
        description: "Stocks that saw MACD Crossover Below",
        screenId: 22719,
      },

      {
        title: "Lowest Momentum Scores (Technical Scores)",
        description:
          "Stocks with low Trendlye Momentum score. Momentum scors is calculated on basis of technical indicators like SMA, RSI, MFI, MACD, ROC, ATR, ADX and other technical values.",
        screenId: 6157,
      },
    ],
  },
  // {
  //   title: "Bullish or Bearish Candlestick Strength - Indices",
  //   description:
  //     "This screener checks the bullish or bearish strength (bullish minus bearish candlesticks) at the indices level.",
  //   screenId: 20688
  // },
  // {
  //   title: "Red Flags: Weakening Technicals and Share Price Decline",
  //   description:
  //     "Stocks showing MACD below signal line, and weakening share price",
  //   screenId: 16995
  // },
  // {
  //   title:
  //     "30 Day SMA crossing over 200 Day SMA, and current price greater than open",
  //   description:
  //     "This screener selects stocks based on rising short term price average.",
  //   screenId: 7589
  // },
  // {
  //   title:
  //     "Bullish Stocks - Stocks with Highest Trendlyne Momentum Score (greater than 70)",
  //   description:
  //     "This screener compares the performance of high Technical Momentum Score stocks to benchmarks. As seen in the screener, stocks with high Momentum Scores significantly outperform indices.This screener is a dynamic strategy that changes stocks based on daily momentum, and momentum changes. Stocks enter and exit this screener on an ongoing basis. To follow this strategy, set a screener alert. For this strategy, we recommend a daily or weekly alert. This is a shorter-term strategy due to its focus on technicals.",
  //   screenId: 3057
  // },

  // {
  //   title:
  //     "Rising Delivery Percentage Compared to Previous Day and Month, Strong Volumes",
  //   description:
  //     "Stocks with rising delivery percentage, and above a required week volume",
  //   screenId: 9589
  // },
  // {
  //   title: "Momentum Play Screener - Weekly Strategy",
  //   description:
  //     "The Pure Momentum Strategy chooses stocks gaining in momentum every week. Stocks are cycled weekly. A weekly alert is recommended on this strategy.",
  //   screenId: 9142
  // },

  //   ({
  //     title:
  //       "Stocks whose current price is 20% higher than week low, and more than previous close",
  //     description:
  //       "Live screen that identifies stocks whose current price is 20% higher than the Week Low. Stock should also currently be greater than previous close.",
  //     screenId: 9335
  //   },
  //   {
  //     title: "High Delivery Percentage",
  //     description: "Stocks with high delivery percentage EOD",
  //     screenId: 9588
  //   },
  //   {
  //     title: "Weekly Strategy: High Momentum Score Stocks (subscription)",
  //     description:
  //       "A strategy that picks stocks with mid financials, and high momentum. A weekly alert is recommended on this strategy. This strategy works better if followed consistently week on week, particularly in a choppy market.",
  //     screenId: 7557
  //   },
  //   {
  //     title: "Trendlynes growth stocks with good technical score",
  //     description:
  //       "Stocks in the top 25% percentile of equity universe technically which have shown good profit and revenue growth over the past 4 quarters. This screener is a dynamic strategy that changes stocks based on quarterly revenue and profit changes, as well as momentum change. Stocks enter and exit this screener on an ongoing basis. To follow this strategy, set a screener alert. For this strategy, we recommend a monthly alert. This is a longer-term strategy due to its mix of fundamentals and technicals. Changing stocks here with a very high frequency can negatively impact your returns.",
  //     screenId: 29
  //   },
  //   {
  //     title:
  //       "Trendlynes high return technically strong value stocks (subscription)",
  //     description:
  //       "Value stocks having Piotroski Score of greater than or equal to 5 trading above all their SMAs and having a medium term technical score in the top 25% of the equity universe.This screener is a dynamic strategy that changes based on changes in SMAs, piotroski and momentum scores. Stocks enter and exit this screener on an ongoing basis. To follow this strategy, set a screener alert. For this strategy, we recommend a monthly or weekly alert. See backtest returns for both frequencies.",
  //     screenId: 26
  //   },
  //   {
  //     title: "Stocks gaining versus previous close, open price and RSI",
  //     description:
  //       "Identifies stocks that are seeing an upward trend versus previous close and open price",
  //     screenId: 11113
  //   },
  //   {
  //     title: "Momentum Score (Technical Score) Daily Gainers",
  //     description: "Momentum Score (Technical Score) Daily Gainers",
  //     screenId: 6159
  //   },
  //   {
  //     title: "Momentum Score (Technical Score) Weekly Gainers",
  //     description:
  //       "Trendlyne Momentum Score gainers over the last week. The momentum score is calculated on basis of technical indicators like SMA, RSI, MFI, MACD, ROC, ATR, ADX and other technical values.",
  //     screenId: 6160
  //   },
  //   {
  //     title:
  //       "Good Aggregate Candlestick Strength (total bullish - bearish candlesticks)",
  //     description:
  //       "The Good Aggregate Candlestick Strength Screener looks at the total of (bullish candlestick indicators - bearish candlestick indicators) for a stock, giving it a total positive or negative value. The higher the positive number, the more bullish patterns the stock is seeing. The lower the negative number, the more bearish patterns the stock is seeing.",
  //     screenId: 6245
  //   },
  //   {
  //     title: "High Momentum Scores (Technical Scores greater than 50)",
  //     description:
  //       "Stocks with High Trendlye Momentum score. Momentum scores are calculated on basis of technical indicators like SMA, RSI, MFI, MACD, ROC, ATR, ADX and other technical values.",
  //     screenId: 6158
  //   },
  //   {
  //     title: "High Volume Stocks: Top Gainers Today",
  //     description: "",
  //     screenId: 7585
  //   },
  //   {
  //     title: "Rising Delivery Percentage Compared to Prev Day",
  //     description:
  //       "Stocks which are seeing rising delivery percentage compared to previous day",
  //     screenId: 9844
  //   },
  //   {
  //     title: "MACD crossed above signal line previous end of day",
  //     description: "MACD crossed above signal line previous end of day",
  //     screenId: 5922
  //   },
  //   {
  //     title: "MACD crossed below signal line previous end of day",
  //     description:
  //       "When the MACD crosses below the signal line, it indicates a bearish signal.",
  //     screenId: 5921
  //   },
  //   {
  //     title: "MACD crossed below zero line previous end of day",
  //     description:
  //       "When the MACD crosses below the zero line, then a possible sell signal is generated.",
  //     screenId: 5919
  //   },
  //   {
  //     title: "MACD crossed above zero line previous end of day",
  //     description:
  //       "A possible buy signal is generated when the MACD crosses above the zero line.",
  //     screenId: 5918
  //   },
  //   {
  //     title: "Bottom Fishing",
  //     description:
  //       "This strategy is based on bottom fishers strategy. This looks for oversold stocks where the downward pressure is lesser than potential for trend reversal. We try and restrict the downside by combining fundamental expectation and technical score. Choosing stocks with more than 25% fall from 52 Week high implies that these stocks have recently fallen out of favour. By combining the analyst expectations we try and find stocks which have been punished more than they deserved. Pro tip: An empty set or very few stocks in the screener implies that the market is over heated in general.",
  //     screenId: 1526
  //   },
  //   {
  //     title: "Momentum stocks",
  //     description:
  //       "PEG less than 1 = room for growth100% price appreciation last 52 weeks = price momentum yoy revenue and profit growth more than 15% = promising past growthMACD less than MACD Signal = at the buy zone",
  //     screenId: 1085
  //   },
  //   {
  //     title: "Good Technical Strong with high growth",
  //     description: "Stocks having very good technicals .",
  //     screenId: 717
  //   })
  // ],
  shareholding: [
    {
      title: "Big Deal (Insider and SAST) sells last week greater than 1% of total shares",
      description:
        "Stocks that saw Insider and SAST sells by promoters and other insiders last week greater than 1% of total shares. This is a live screener that continues to update. To get entries and exits, set a screener alert.",
      screenId: 19892,
    },
    {
      title: "Big Deal (Insider and SAST) sells last month greater than 1% of total shares",
      description:
        "Stocks that saw Insider and SAST sells by promoters and other insiders last month greater than 1% of total shares. This is a live screener that continues to update. To get entries and exits, set a screener alert.",
      screenId: 19891,
    },
    {
      title: "Companies where substantial shares pledged by insiders invoked by lenders",
      description:
        "Stocks where more than 0.5% of total shares were invoked by lenders for non-payment of debts. To get notified of stock entries/exits in this screener set an alert.",
      screenId: 19893,
    },
    {
      title: "Big Deal (Insider and SAST) buys last week greater than 1% of total shares",
      description:
        "Stocks that saw Insider and SAST buys by promoters and other insiders last week greater than 1% of total shares. This is a live screener that continues to update. To get entries and exits, set a screener alert.",
      screenId: 19890,
    },
    {
      title: "Big Deal (Insider and SAST) buys last month greater than 1% of total shares",
      description:
        "Stocks that saw Insider and SAST buys by promoters and other insiders last month greater than 1% of total shares. This is a live screener that continues to update. To get notified of entries and exits, set a screener alert.",
      screenId: 19889,
    },
    {
      title: "Big Changes in FII holding in companies - latest quarter",
      description: "Big Changes in FII holding in companies - latest quarter",
      screenId: 11500,
    },
    {
      title: "Promoters are buying these growth stocks",
      description:
        "Growth stocks with good net profit growth on trailing twelve month basis where promoters have increased their stakes over the past one year.",
      screenId: 23,
    },
    {
      title: "Mutual Funds and FII/DIIs have increased their shareholding in most recent quarter",
      description:
        "Companies where both mutual funds (MFs) and FIIs/DIIs (institutional investors) have increased their stake QoQ",
      screenId: 19814,
    },
    {
      title: "Promoter shareholding increase",
      description: "Stocks where promoter is increasing shareholding",
      screenId: 8702,
    },
    {
      title: "Promoter holding increased more than 2% QoQ",
      description: "Stock screener for stocks where promoters increased their holding by more than 2% QoQ",
      screenId: 17343,
    },
    {
      title: "Significant Change in Promoter Shareholding - Latest Quarter",
      description:
        "Stocks that have seen significant increase or decrease in promoter shareholding in the most recent quarter (greater than 3% or -3%)",
      screenId: 11385,
    },
  ],
  fundamentals: [
    {
      title: "All positives",
      description:
        "Screener with stocks having low PE, low Debt/ Equity, High ROCE, High ROE. Stocks which have given best but realistic boundaries for all important ratios.",
      screenId: 15180,
    },
    {
      title: "Growth stocks",
      description:
        "A comprehensive screen to find growth stocks. Find companies with quarterly profit growth of over 10% and some other power matrix.",
      screenId: 24376,
    },
    {
      title: "Highest YoY quarterly profit growth",
      description: "Stocks with highest Quarter on Quarter growth in profits.",
      screenId: 15177,
    },
    {
      title: "Low PE and high EPS growth",
      description: "This screener shows stocks with low PE and high EPS growth",
      screenId: 15179,
    },
    {
      title: "Peter Lynch stock screen",
      description:
        'The screen identifies companies that are "fast growers" looking for consistently profitable, relatively unknown, low debt, reasonably priced stocks with high , but not excessive, growth. Among the criteria used, the strategy looks for stocks with a low price to earnings growth rate (PEG).',
      screenId: 0,
    },
    {
      title: "Quarterly growers",
      description: "These are the stocks whose Net profit is constantly increasing since past three quarters.",
      screenId: 15176,
    },
    {
      title: "Search for value stock",
      description:
        "Screeneing value stocks with sound history of performance ie having high ROE averaging for the last 5 years and positive dividend yield.",
      screenId: 15181,
    },
    {
      title: "Value stocks",
      description: "The screener shows stocks with high operating profit margin, ROCE, low D/E ratio.",
      screenId: 15224,
    },
  ],
  movingAverage: {
    bullish: [
      {
        title: "Golden cross",
        description:
          "Technical Screener for stocks whose SMA 50 recently crossed above their SMA 200 . This is commonly known as Golden Cross and is an important technical indicator for bullish stocks. Learn more with our SMA explainer.",
        screenId: 15193,
      },
      {
        title: "Positive Breakouts - Short trend",
        description:
          "Technical screener for stocks which have recently crossed above their SMA-30 (Simple Moving Averages)",
        screenId: 15208,
      },
      {
        title: "Positive Breakouts - Medium trend",
        description:
          "Technical screener for stocks which have recently crossed above their SMA-50 (Simple Moving Averages)",
        screenId: 15209,
      },
      {
        title: "Positive Breakouts - Long trend",
        description:
          "Technical screener for stocks which have recently crossed above their SMA-200 (Simple Moving Averages)",
        screenId: 15210,
      },
    ],
    bearish: [
      {
        title: "Death Cross",
        description:
          "Technical Screener for stocks whose SMA 50 recently crossed below their SMA 200 . This is commonly known as Death cross and is an important technical indicator for bearish stocks.",
        screenId: 15211,
      },
      {
        title: "Negative Breakouts - Short trend",
        description:
          "Technical screener for stocks which have recently crossed below their SMA-30 (Simple Moving Averages)",
        screenId: 15212,
      },
      {
        title: "Negative Breakouts - Medium trend",
        description:
          "Technical screener for stocks which have recently crossed below their SMA-50 (Simple Moving Averages)",
        screenId: 15213,
      },
      {
        title: "Negative Breakouts - Long trend",
        description:
          "Technical screener for stocks which have recently crossed below their SMA-200 (Simple Moving Averages)",
        screenId: 15214,
      },
    ],
  },
  priceVolume: [
    {
      title: "Volume Shockers: most active, high volume stocks ",
      description: "",
      screenId: 15161,
    },
    {
      title: "Volume Shockers: high volume, top gainers",
      description: "",
      screenId: 15162,
    },
    {
      title: "Volume Shockers: high volume, top losers ",
      description: "",
      screenId: 15163,
    },
    {
      title: "Near 52 Week High",
      description: "",
      screenId: 15165,
    },
    {
      title: "Near 52 Week Low",
      description: "",
      screenId: 15166,
    },
    {
      title: "Highest fall from 52 Week High",
      description: "Price screener for weak Stocks which have fallen the most from their 52 week highs on BSE and NSE",
      screenId: 15202,
    },
    {
      title: "Highest recovery from 52 Week Low",
      description:
        "Price screener for strong Stocks which have recovered the most from their 52 week lows on BSE and NSE",
      screenId: 15203,
    },
    {
      title: "Top Gainers: Top gainers in trading for today in Nifty 500",
      description: "Technical screener for the top gainer stocks today in Nifty 500",
      screenId: 24371,
    },
    {
      title: "Top Loser: Top losers in trading today for today in Nifty 500",
      description: "Technical screener for the top gainer stocks today in Nifty 500",
      screenId: 24372,
    },
    {
      title: "New 52 Week Highs in Nifty 500",
      description: "Price screener for strong Stocks which have made a new 52 week high in Nifty 500",
      screenId: 24374,
    },
    {
      title: "New 52 Week Lows in Nifty 500",
      description: "Price screener for weak Stocks which have made a new 52 week low in Nifty 500",
      screenId: 24375,
    },
  ],
};
