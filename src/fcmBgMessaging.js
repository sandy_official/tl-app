import firebase from "react-native-firebase";
import { RemoteMessage } from "react-native-firebase";

export default async message => {
  // handle your message
  // write your code here for background messages

  //This handler method must return a promise and resolve within 60 seconds.
  console.warn("Push notification", message);
  return Promise.resolve();
};
