import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, ScrollView } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors, globalStyles } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import DiscountCoupon from "../../components/subscription/DiscountCoupon";
import SubscriptionCouponBSheet from "../../components/subscription/SubscriptionCouponBSheet";
import { Toolbar } from "../../components/Toolbar";
import { numberWithCommas } from "../../utils/Utils";
import Toast from "react-native-easy-toast";
import RazorpayCheckout from "react-native-razorpay";
import { startPaymentTransaction, sendTransactionSuccessResponse } from "../../controllers/SubscriptionController";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";

class SubscriptionDetailsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedCoupon: null,
      plan: {},
      offers: [],
      subscriptionPlan: "",
      customer: {},
      payMethod: "",
      planKey: "",
      amount: 0,
      finalPrice: "",
      couponDiscount: 0,
      currentPlan: {},
      isRefreshing: false,
      isSubscriber: false,
      active_plan: {},
      isCreditCard: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const subscriptionPlan = navigation.getParam("subscriptionPlan", "");
    const plan = navigation.getParam("plan", {});
    const offers = navigation.getParam("offers", {});
    const customer = navigation.getParam("customer", {});
    const payMethod = navigation.getParam("payMethod", "");
    const planKey = navigation.getParam("planKey", "");
    const amount = navigation.getParam("amount", 0);
    const currentPlan = navigation.getParam("currentPlan", 0);
    const isSubscriber = navigation.getParam("isSubscriber", false);
    const active_plan = navigation.getParam("active_plan", {});
    const creditCard = navigation.getParam("isCreditCard", {});

    this.setState({
      plan: plan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      customer: customer,
      payMethod: payMethod,
      planKey: planKey,
      amount: amount,
      finalPrice: amount,
      currentPlan: currentPlan,
      isSubscriber: isSubscriber,
      active_plan: active_plan,
      isCreditCard: creditCard,
    });
  }

  startRazorPayTransaction = (orderId, notes, subscriptionId) => {
    const { baseURL, appToken, razorPayIcon, razorPayKey } = this.props;
    const { token } = this.props.userState;
    const { finalPrice, customer } = this.state;
    const { Name, Email } = customer;

    var options = {
      description: "Trendlyne Plan",
      // image: "https://d29443a4txen2y.cloudfront.net/static/Mobile_icon_black.png",
      image: razorPayIcon,
      currency: "INR",
      // key: "rzp_test_pbqEWUA450sKW1",
      key: razorPayKey,
      amount: (finalPrice * 100).toFixed(0),
      name: "Trendlyne",
      prefill: {
        email: Email,
        contact: customer["Contact Number"],
        name: Name,
      },
      theme: { color: colors.primary_color },
      notes: notes,
      // subscription_id:
    };

    if (subscriptionId) options.subscription_id = subscriptionId;
    console.log("razorpay options", options);

    RazorpayCheckout.open(options)
      .then((data) => {
        const razorpayPaymentId = data.razorpay_payment_id;
        // alert(`Success: ${data.razorpay_payment_id}`);

        sendTransactionSuccessResponse(
          baseURL,
          appToken,
          token,
          orderId,
          razorpayPaymentId,
          data.razorpay_signature,
          subscriptionId,
          (isSuccess, response) => {
            this.setState({ isRefreshing: false });

            if (isSuccess) this.refs.toast.show("Success response sent");
            else this.refs.toast.show("Failed to send success response");
          }
        );
        console.log("Razorpay success", data);
      })
      .catch((error) => {
        // handle failure
        this.setState({ isRefreshing: false });
        console.log("Razorpay failure", error);
        // alert(`Error: ${error.code} | ${error.description}`);
      });
  };

  payNow = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { subscriptionPlan, planKey, selectedCoupon, isCreditCard } = this.state;

    const paymentMethod = isCreditCard ? "creditcard" : "nebanking";
    const discountPercent = selectedCoupon != null ? selectedCoupon.percentage : 0;
    const couponCode = selectedCoupon != null ? selectedCoupon.code : "";

    this.setState({ isRefreshing: true });

    startPaymentTransaction(
      baseURL,
      appToken,
      token,
      subscriptionPlan,
      paymentMethod,
      planKey,
      discountPercent,
      couponCode,
      (isSuccess, data) => {
        if (isSuccess) {
          const orderId = data.transaction["order_id"];
          const subscriptionId = data.transaction.autorenew_info
            ? data.transaction.autorenew_info.autorenew_subsciptionID
            : null;
          const notes = data.notes;
          this.startRazorPayTransaction(orderId, notes, subscriptionId);
        } else {
          // Failure
          this.setState({ isRefreshing: false });
        }
      }
    );
  };

  changePayMode = () => {
    const { plan, offers, subscriptionPlan, payMethod, customer } = this.state;

    const data = {
      plan: plan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      payMethod: payMethod,
      skipScreen: true,
      customer: customer,
    };

    console.log("details", data);
    // this.props.navigation.pop();
    // this.props.navigation.pop();
    this.props.navigation.navigate("SubscriptionPayMode", data);
  };

  changeUserInfo = () => {
    const { plan, offers, subscriptionPlan, payMethod, planKey, amount } = this.state;

    const data = {
      plan: plan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      payMethod: payMethod,
      planKey: planKey,
      amount: amount,
    };

    this.props.navigation.pop();
    this.props.navigation.navigate("SubscriptionForm", data);
  };

  setCoupon = (coupon) => {
    const { amount } = this.state;

    if (coupon != null) {
      const discount = coupon ? (amount / 100) * coupon.percentage : 0;
      const finalPrice = amount - discount;

      this.setState({
        selectedCoupon: coupon,
        couponDiscount: discount,
        finalPrice: finalPrice,
      });

      this.refs.toast.show("Discount Coupon Applied", 1000);
    } else {
      this.setState({
        selectedCoupon: null,
        couponDiscount: 0,
        finalPrice: amount,
      });

      this.refs.toast.show("Discount Coupon Removed", 1000);
    }
  };

  showCoupons = () => {
    this.refs.coupons.RBSheet.open();
  };

  getPlanInfo = () => {
    const { planKey } = this.state;

    if (planKey == "annual") return { name: "Annual", info: "One Time Payment" };
    else if (planKey == "annual_autorenew") return { name: "Annual", info: "with Price Lock and Auto Renew" };
    else if (planKey == "quarterly_autorenew") return { name: "Quarterly", info: "with Price Lock and Auto Renew" };
    else return {};
  };

  renderPlanAmount = () => {
    const { payMethod, planKey, amount } = this.state;
    const planInfo = this.getPlanInfo();
    const price = !isNaN(amount) ? numberWithCommas(amount.toFixed(0)) : 0;

    return (
      <View style={{ paddingHorizontal: wp(3) }}>
        <Text style={styles.heading}>PAYMENT METHOD AND PRICING PLAN</Text>

        <View style={styles.detailContainer}>
          <View style={[styles.row]}>
            <Text style={styles.payOption}>{payMethod}</Text>
            <TouchableOpacity onPress={this.changePayMode}>
              <Text style={styles.changeBtn}>Change</Text>
            </TouchableOpacity>
          </View>

          <View style={[styles.row]}>
            <Text style={styles.payOption}>
              {planInfo.name + " "}
              <Text style={styles.description}>{planInfo.info}</Text>
            </Text>
            <TouchableOpacity>
              <Text style={styles.payAmount}>&#8377;{price}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  renderPlanAmountSubscriber = () => {
    const { payMethod, planKey, amount, currentPlan, active_plan } = this.state;
    const planInfo = this.getPlanInfo();
    const prevPlan = active_plan.plan ? `from ${active_plan.plan} ${active_plan.variant_name}` : "";

    const actualPrice = !isNaN(currentPlan.actualPrice)
      ? numberWithCommas(currentPlan.actualPrice.toFixed(0))
      : currentPlan.actualPrice;

    const prevPlanAmount = currentPlan.upgradePrice - currentPlan.actualPrice;
    const prevPlanAmountFormatted = numberWithCommas(prevPlanAmount.toFixed(0));
    const price = !isNaN(amount) ? numberWithCommas(amount.toFixed(0)) : 0;

    return (
      <View style={{ paddingHorizontal: wp(3), marginBottom: wp(4.1) }}>
        <Text style={styles.heading}>PAYMENT METHOD AND PRICING PLAN</Text>

        <View style={styles.detailContainer}>
          <Text style={styles.payOption}>{payMethod}</Text>
          {/* <Text style={styles.payOption}>{payMethod}</Text> */}

          <View style={[styles.row, { marginBottom: wp(2.2) }]}>
            <Text style={[{ flex: 1 }, styles.description]}>
              <Text style={styles.blackText}>{planInfo.name + " "}</Text>
              {planInfo.info}
            </Text>
            <Text style={[styles.description, styles.blackText]}>&#8377;{actualPrice}</Text>
          </View>

          <View style={[styles.row, { marginBottom: wp(2.2) }]}>
            <Text style={[{ flex: 1 }, styles.description, styles.blackText]}>Remaining amount {prevPlan}</Text>
            <Text style={[styles.description, , styles.blackText]}>&#8377;{prevPlanAmountFormatted}</Text>
          </View>
        </View>

        <View style={styles.payRow}>
          <Text style={styles.youPay}>You Pay</Text>
          <Text style={styles.youPayAmount}>&#8377;{price}</Text>
        </View>
      </View>
    );
  };

  renderBillingInfo = () => {
    const { customer } = this.state;
    const name = customer.Name ? customer.Name : "";
    const address = customer.Address ? customer.Address : "";
    // const city = customer.City ? customer.City + ", " : "";
    const state = customer.State ? customer.State : "";
    const country = customer.Country ? customer.Country : "";
    const pinCode = customer["PIN Code"] ? customer["PIN Code"] : "";

    let formattedAddress = address
      ? name + ",\n" + address + ", " + state + ", " + country + " - " + pinCode
      : name + ",\n" + state + ", " + country;

    return (
      <View style={{ paddingHorizontal: wp(3) }}>
        <Text style={styles.heading}>BILLING INFORMATION</Text>

        <View style={styles.detailContainer}>
          <View style={[styles.row]}>
            <Text style={styles.address}>{formattedAddress}</Text>
            <TouchableOpacity onPress={this.changeUserInfo}>
              <Text style={styles.changeBtn}>Change</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };

  renderApplyCoupon = () => {
    const containerStyle = [{ paddingHorizontal: wp(3), alignItems: "center" }, styles.row];

    return (
      <TouchableOpacity style={containerStyle} onPress={this.showCoupons}>
        <Text style={styles.heading}>APPLY DISCOUNT COUPON</Text>
        <Image source={require("../../assets/icons/arrow-right-blue.png")} style={styles.arrowIcon} />
      </TouchableOpacity>
    );
  };

  renderPayBtn = () => {
    const { finalPrice } = this.state;
    const formattedPrice = finalPrice ? numberWithCommas(finalPrice.toFixed(0)) : "";

    if (finalPrice != "") {
      return (
        <TouchableOpacity style={styles.payBtn} onPress={this.payNow}>
          <Text style={styles.payBtnText}>Pay &#8377;{formattedPrice}</Text>
        </TouchableOpacity>
      );
    }
  };

  renderAppliedCoupon = () => {
    const { selectedCoupon, amount, couponDiscount } = this.state;
    const couponCode = selectedCoupon ? selectedCoupon.code : "";
    const couponOff = selectedCoupon ? "Get " + selectedCoupon.percentage + "% off" : "";
    const discount = selectedCoupon ? numberWithCommas(couponDiscount.toFixed(0)) : "";

    return (
      <View style={{ paddingHorizontal: wp(3) }}>
        <View style={styles.discountRow}>
          <DiscountCoupon coupon={couponCode} discount={couponOff} />

          <View style={{ flex: 1 }}>
            <TouchableOpacity style={styles.removeBtn} onPress={() => this.setCoupon(null)}>
              <Text style={styles.removeBtnText}>Remove</Text>
            </TouchableOpacity>
          </View>
        </View>

        <Text style={styles.description}>
          Discount of <Text style={styles.boldText}>&#8377;{discount}</Text> applied successfully!
        </Text>
      </View>
    );
  };

  render() {
    const { selectedCoupon, offers, subscriptionPlan, isRefreshing, isSubscriber } = this.state;
    const title = subscriptionPlan + " Subscription";

    console.log("payment state", this.state);

    return (
      <View style={styles.container}>
        <Toolbar title={title} backButton {...this.props} />

        <ScrollView>
          <View style={{ paddingTop: wp(2) }} />
          {isSubscriber ? this.renderPlanAmountSubscriber() : this.renderPlanAmount()}

          <View style={styles.divider} />

          {this.renderBillingInfo()}
          <View style={styles.divider} />

          {selectedCoupon == null ? this.renderApplyCoupon() : this.renderAppliedCoupon()}

          {this.renderPayBtn()}
        </ScrollView>

        <SubscriptionCouponBSheet ref="coupons" apply={this.setCoupon} data={offers} />

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {isRefreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  let auth = "auth" in firebase ? firebase.auth : {};
  let other = "other" in firebase ? firebase.other : {};

  return {
    baseURL,
    appToken,
    userState: state.user,
    razorPayKey: auth.RazorPayKey,
    razorPayIcon: other.IconForRazorPay,
  };
};

export default connect(mapStateToProps)(SubscriptionDetailsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
    // paddingHorizontal: wp(3)
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
    marginTop: wp(4.2),
  },
  row: {
    flexDirection: "row",
  },
  payOption: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    marginRight: wp(3),
    marginBottom: 11,
  },
  changeBtn: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  payAmount: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  address: {
    flex: 1,
    fontSize: RF(2.2),
    lineHeight: wp(5.5),
    color: colors.black,
    marginRight: wp(3),
    marginBottom: 11,
  },
  description: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  detailContainer: {
    paddingTop: wp(3.2),
    paddingBottom: wp(1.2),
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: colors.greyish,
    opacity: 0.2,
  },
  arrowIcon: {
    marginTop: wp(4.2),
  },
  payBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(3),
    marginTop: wp(10),
    marginBottom: hp(3),
    padding: 12,
    alignItems: "center",
  },
  payBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  removeBtn: {
    alignSelf: "flex-end",
  },
  removeBtnText: {
    fontSize: RF(2.2),
    color: colors.red,
  },
  discountRow: {
    flexDirection: "row",
    marginTop: wp(4.2),
    marginBottom: wp(2.4),
    alignItems: "center",
  },
  boldText: {
    fontWeight: "500",
  },
  payRow: {
    flexDirection: "row",
    backgroundColor: "rgba(0,46,79,0.08)",
    paddingHorizontal: wp(3.2),
    paddingVertical: wp(2.1),
    marginHorizontal: -wp(3),
  },
  youPay: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.black,
  },
  youPayAmount: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.black,
  },
  blackText: {
    color: colors.black,
  },
});
