import React from "react";
import {
  View,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  TouchableWithoutFeedback,
  ScrollView,
  ImageBackground,
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { Toolbar } from "../../components/Toolbar";
import { numberWithCommas } from "../../utils/Utils";
import SubscriptionPayInfoBSheet from "../../components/subscription/SubscriptionPayInfoBSheet";

const annualData = {
  label: "Annual",
  newPrice: "5,041",
  oldPrice: "5,450",
  save: "438",
  renew: "Annually",
  bestDeal: true,
  color: colors.strongBlue,
};

const quarterData = { label: "Quarterly", newPrice: "5,041", renew: "Quarterly", color: colors.brightBlue };

const oneTimePayData = {
  label: "Annual",
  description: "One Time Payment",
  newPrice: "5,950",
  color: colors.marineBlue,
};

export default class SubscriptionPayOptionsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      selectedPlan: "",
      selectedOption: "Netbanking, Other cards, UPI & Wallets",
      showPayMethodDropdown: false,
      showPlansDropdown: true,
      plan: {},
      offers: [],
      subscriptionPlan: "",
      isSubscriber: false,
      active_plan: {},
      isCreditCard: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const subscriptionPlan = navigation.getParam("subscriptionPlan", "");
    const plan = navigation.getParam("plan", {});
    const offers = navigation.getParam("offers", []);
    const isSubscriber = navigation.getParam("isSubscriber", false);
    const active_plan = navigation.getParam("active_plan", {});

    const prevPayMethod = navigation.getParam("payMethod", "Netbanking, Other cards, UPI & Wallets");
    const creditCard = navigation.getParam("isCreditCard", false);

    this.setState({
      plan: plan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      isSubscriber: isSubscriber,
      active_plan: active_plan,
    });

    this.setState({ selectedOption: prevPayMethod, isCreditCard: creditCard });
  }

  gotoDetailForm = (key, amount) => {
    const { navigation } = this.props;
    const {
      plan,
      offers,
      selectedPlan,
      selectedOption,
      subscriptionPlan,
      isSubscriber,
      active_plan,
      isCreditCard,
    } = this.state;

    const currentPlan = plan[key];
    const skip = navigation.getParam("skipScreen", false);
    const customer = navigation.getParam("customer", {});

    const data = {
      plan: plan,
      currentPlan: currentPlan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      payMethod: selectedOption,
      planKey: key,
      amount: amount,
      skipScreen: skip,
      customer: customer,
      isSubscriber: isSubscriber,
      active_plan: active_plan,
      isCreditCard: isCreditCard,
    };

    // console.log("pay state", this.state);
    // console.log("pay data", data);

    if (skip) navigation.navigate("SubscriptionDetails", data);
    else navigation.navigate("SubscriptionForm", data);
  };

  showPayInfo = () => {
    this.refs.payInfo.RBSheet.open();
  };

  togglePayMethodDropdown = () => {
    this.setState({ showPayMethodDropdown: !this.state.showPayMethodDropdown });
  };

  togglePlansDropdown = () => {
    this.setState({ showPlansDropdown: !this.state.showPlansDropdown });
  };

  selectPlan = (name) => {
    name == this.state.selectedPlan ? this.setState({ selectedPlan: "" }) : this.setState({ selectedPlan: name });
  };

  setSelectedOption = (option, creditCard) => {
    // const { onSelect } = this.props;
    // onSelect(option);
    this.setState({ selectedOption: option, isCreditCard: creditCard });
    this.togglePayMethodDropdown();
  };

  getDiscountAmount = (price, discountPercent) => {
    if (!isNaN(price) && !isNaN(discountPercent)) return ((price / 100) * discountPercent).toFixed(0);
    else return 0;
  };

  renderPlanItem = (item, key) => {
    const { selectedPlan, plan, isCreditCard } = this.state;
    const showInfo =
      selectedPlan == key ||
      (selectedPlan == "" && item.bestDeal) ||
      (!isCreditCard && selectedPlan != key && !item.bestDeal);

    const currentPlan = plan[key] ? plan[key] : {};
    // const showDiscount = key == "annual_autorenew" ? true : false;
    // const canUpgrade = currentPlan.is_upgrade ? true : false;

    // const annualPrice = plan["annual"]
    //   ? plan["annual"].is_upgrade
    //     ? plan["annual"].upgradePrice
    //     : plan["annual"].actualPrice
    //   : null;

    const currentPrice = currentPlan.is_upgrade ? currentPlan.upgradePrice : currentPlan.actualPrice;

    // const discountAmount =
    //   showDiscount && annualPrice ? annualPrice - currentPrice : 0;
    // const discountFormatted = numberWithCommas(discountAmount.toFixed(0));

    const newPriceFormatted = numberWithCommas(currentPrice.toFixed(0));

    // const oldPriceFormatted = annualPrice
    //   ? numberWithCommas(annualPrice.toFixed(0))
    //   : "";

    const amountUnformatted = currentPlan.is_upgrade ? currentPlan.upgradePrice : currentPlan.actualPrice;

    const buttonText = currentPlan.subscribeText ? currentPlan.subscribeText : "Select";
    const isButtonDisabled = currentPlan.disabled ? currentPlan.disabled : false;
    const buttonStyle = isButtonDisabled
      ? [styles.subscribeBtn, { backgroundColor: colors.steelGrey }]
      : styles.subscribeBtn;

    const headerStyle = showInfo
      ? [styles.cardHeader, { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 }, { backgroundColor: item.color }]
      : [styles.cardHeader, { backgroundColor: item.color }];

    item.bestDeal ? headerStyle.push({ borderTopLeftRadius: 0 }) : null;

    return (
      <View style={styles.planContainer}>
        {item.bestDeal && (
          <View style={styles.popularContainer}>
            <Text style={styles.popular}>BEST DEAL</Text>
          </View>
        )}

        <TouchableOpacity style={headerStyle} onPress={() => this.selectPlan(key)}>
          <View style={{ flex: 1 }}>
            <Text style={styles.planName}>
              {item.label} {item.description && <Text style={styles.license}>{item.description}</Text>}
            </Text>
          </View>

          <View style={{ flexDirection: "row", alignItems: "center" }}>
            <Text style={[styles.annualPrice]}>
              <Text style={styles.bold}>&#8377;{newPriceFormatted} </Text>
              {/* {showDiscount ? (
                <Text style={styles.oldPrice}>&#8377;{oldPriceFormatted}</Text>
              ) : null} */}
            </Text>

            {/* {showDiscount ? (
              <View style={styles.saveContainer}>
                <Text style={styles.save}>Save &#8377;{discountFormatted}</Text>
              </View>
            ) : null} */}
          </View>
        </TouchableOpacity>

        {showInfo && (
          <View style={[styles.card, styles.featureCard]}>
            <View style={styles.featureRow}>
              <View style={styles.dot} />
              {item.description ? (
                <Text style={styles.feature}>
                  Subscription for only 1 year, <Text style={styles.featureHighlight}>no auto renew</Text> will occur
                </Text>
              ) : (
                <Text style={styles.feature}>
                  Auto Renew will happen <Text style={styles.featureHighlight}>{item.renew}</Text>
                </Text>
              )}
            </View>

            <TouchableOpacity
              style={buttonStyle}
              onPress={() => this.gotoDetailForm(key, amountUnformatted)}
              disabled={isButtonDisabled}
            >
              <Text style={styles.subscribeBtnText}>{buttonText}</Text>
            </TouchableOpacity>
          </View>
        )}
      </View>
    );
  };

  renderDropdownOption = (option, isCreditCard) => {
    const { selectedOption } = this.state;

    const optionStyle = selectedOption === option ? [styles.radioButton, styles.radioButtonActive] : styles.radioButton;
    const radioButtonStyle = selectedOption === option ? styles.radioButtonCircle : styles.hide;
    const optionTextStyle = selectedOption == option ? styles.dropdownOptionActive : styles.dropdownOption;

    return (
      <View key={option}>
        <TouchableWithoutFeedback onPress={() => this.setSelectedOption(option, isCreditCard)}>
          <View style={styles.dropdownOptionContainer}>
            <View style={optionStyle}>
              <View style={radioButtonStyle} />
            </View>
            <Text style={optionTextStyle}>{option}</Text>
          </View>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  renderPayOptions = () => {
    const { showPayMethodDropdown, selectedOption } = this.state;
    const dropdownIcon = showPayMethodDropdown
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");

    return (
      <View>
        <TouchableOpacity style={styles.headingRow} onPress={this.togglePayMethodDropdown}>
          <Text style={styles.heading}>CHOOSE PAYMENT METHOD</Text>
          <Image source={dropdownIcon} />
        </TouchableOpacity>

        {showPayMethodDropdown && (
          <View style={{ marginBottom: wp(4) }}>
            {this.renderDropdownOption("Indian Master/Visa Credit Card", true)}
            {this.renderDropdownOption("Netbanking, Other cards, UPI & Wallets", false)}
          </View>
        )}

        {!showPayMethodDropdown && (
          <TouchableOpacity onPress={this.togglePayMethodDropdown}>
            <Text style={styles.paymentMode}>{selectedOption}</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  };

  renderAutoRenewPlans = () => {
    const { selectedOption, plan, isCreditCard } = this.state;

    /** For annual auto renew */
    const showAnnual = plan.annual_autorenew && plan.annual_autorenew.showPlan ? true : false;
    const canUseCreditCardAnnual = showAnnual ? plan.annual_autorenew.creditCard && isCreditCard : false;
    const canUseOtherPaymentsAnnual = showAnnual ? !plan.annual_autorenew.otherPayments && !isCreditCard : false;

    /**For quarterly autorenew */
    const showQuarterly = plan.quarterly_autorenew && plan.quarterly_autorenew.showPlan ? true : false;
    const canUseCreditCardQuarterly = showQuarterly ? plan.quarterly_autorenew.creditCard && isCreditCard : false;
    const canUseOtherPaymentsQuarterly = showQuarterly
      ? !plan.quarterly_autorenew.otherPayments && !isCreditCard
      : false;

    return (
      <View>
        <View style={styles.subheadingRow}>
          <Text style={styles.subheading}>With price lock and auto renew</Text>

          <TouchableOpacity onPress={this.showPayInfo}>
            <Image source={require("../../assets/icons/info-blue.png")} />
          </TouchableOpacity>
        </View>

        {/* Annual autorenew */}
        {canUseCreditCardAnnual && this.renderPlanItem(annualData, "annual_autorenew")}

        {canUseOtherPaymentsAnnual && (
          <View>
            <View style={{ marginTop: 12 }} />
            {this.renderPlanBlock("annual_autorenew")}
          </View>
        )}

        {/* Quarterly Autorenew */}
        {canUseCreditCardQuarterly && this.renderPlanItem(quarterData, "quarterly_autorenew")}

        {canUseOtherPaymentsQuarterly && (
          <View>
            <View style={{ marginTop: 12 }} />
            {this.renderPlanBlock("quarterly_autorenew")}
          </View>
        )}
      </View>
    );
  };

  renderOneTimePlans = () => {
    return (
      <View>
        <View style={styles.subheadingRow}>
          <Text style={styles.subheading}>One time payment</Text>
        </View>

        {this.renderPlanItem(oneTimePayData, "annual")}
      </View>
    );
  };

  renderPlanBlock = (key) => {
    const { plan } = this.state;

    const currentPlan = plan[key] ? plan[key] : {};
    const showAnnual = key == "annual_autorenew";
    const showDiscount = currentPlan.is_upgrade && key != "annual" ? true : false;

    const background = showAnnual
      ? require("../../assets/icons/subscription/annual-plan-bg.png")
      : require("../../assets/icons/subscription/quarterly-plan-bg.png");

    const newPrice = showDiscount
      ? numberWithCommas(currentPlan.upgradePrice.toFixed(0))
      : numberWithCommas(currentPlan.actualPrice.toFixed(0));

    const planInfo = showAnnual ? "get Annual Plan at ₹" + newPrice : "get Quarterly Plan at ₹" + newPrice;

    return (
      <View style={{ marginHorizontal: wp(3) }}>
        {showAnnual && (
          <View style={styles.popularContainer}>
            <Text style={styles.popular}>BEST DEAL</Text>
          </View>
        )}

        <ImageBackground source={background} style={styles.imgBg} resizeMode="contain">
          <Text style={styles.planBlockMessage}>
            {"Change Payment Mode to Credit Card \nto "}
            <Text style={styles.boldText}>{planInfo}</Text>
          </Text>
        </ImageBackground>
      </View>
    );
  };

  renderChoosePlan = () => {
    const { showPlansDropdown, selectedOption, plan, isCreditCard } = this.state;
    const dropdownIcon = showPlansDropdown
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");

    const showAnnual = plan.annual && plan.annual.showPlan ? true : false;
    const canUseCreditCard = showAnnual ? plan.annual.creditCard && isCreditCard : false;

    //check if annual requires creditCard then hide and show
    if (selectedOption == "") {
      return (
        <View>
          <View style={styles.headingRow}>
            <Text style={[styles.heading, styles.disabled]}>CHOOSE PRICING PLAN</Text>
            <Image source={require("../../assets/icons/arrow-down-grey.png")} style={styles.greyDropdownIcon} />
          </View>
          <Text style={styles.choosePayMethod}>Choose a payment method to see various pricing plans!</Text>
        </View>
      );
    } else {
      return (
        <View>
          <TouchableOpacity style={styles.headingRow} onPress={this.togglePlansDropdown}>
            <Text style={styles.heading}>CHOOSE PRICING PLAN</Text>
            <Image source={dropdownIcon} />
          </TouchableOpacity>

          {showPlansDropdown && (
            <View>
              {/* <View style={{ marginTop: 12 }} /> */}
              {!canUseCreditCard && showAnnual && this.renderOneTimePlans()}

              {!canUseCreditCard && showAnnual && <View style={{ marginTop: wp(7) }} />}
              {this.renderAutoRenewPlans()}
              {canUseCreditCard && showAnnual && <View style={{ marginTop: wp(7) }} />}

              {/* <View style={{ marginTop: 12 }} /> */}
              {canUseCreditCard && showAnnual && this.renderOneTimePlans()}
            </View>
          )}
        </View>
      );
    }
  };

  render() {
    const { subscriptionPlan, plan } = this.state;
    const title = subscriptionPlan + " Subscription";
    const showPlans = Object.keys(plan).length > 0;

    return (
      <View style={styles.container}>
        <Toolbar title={title} backButton {...this.props} />
        <ScrollView>
          {this.renderPayOptions()}
          {showPlans && this.renderChoosePlan()}
          <View style={{ marginBottom: wp(3) }} />
        </ScrollView>
        <SubscriptionPayInfoBSheet ref="payInfo" />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  card: {
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  headingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(6),
    alignItems: "center",
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  subheadingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(4.2),
    alignItems: "center",
  },
  subheading: {
    // flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    marginRight: 8.3,
  },
  dropdownOptionContainer: {
    flexDirection: "row",
    paddingVertical: wp(2.2),
    marginHorizontal: wp(3),
    alignItems: "center",
  },
  dropdownOption: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    marginLeft: 11,
  },
  dropdownOptionActive: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "500",
    marginLeft: 11,
  },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  radioButtonActive: {
    borderColor: colors.primary_color,
  },
  radioButtonCircle: {
    width: wp(2.4),
    height: wp(2.4),
    borderRadius: wp(1.2),
    backgroundColor: colors.primary_color,
  },
  paymentMode: {
    fontSize: RF(2.2),
    color: colors.black,
    marginTop: wp(2.2),
    marginHorizontal: wp(3),
  },
  //Cleanup required
  planContainer: {
    marginHorizontal: wp(3),
    marginTop: wp(4),
  },
  annualPrice: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "right",
  },
  quarterPrice: {
    fontSize: RF(1.9),
    color: colors.white,
    marginTop: 6,
    textAlign: "right",
  },
  bold: {
    fontWeight: "500",
  },
  planName: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.white,
  },
  tapShow: {
    fontSize: RF(1.9),
    color: "rgba(255,255,255,0.75)",
    marginTop: 6,
  },
  featureCard: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    padding: wp(3),
  },
  dot: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: colors.greyishBrown,
  },
  featureRow: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 11,
  },
  feature: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginLeft: 6,
  },
  subscribeBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(4),
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  subscribeBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    margin: 11,
  },
  popularContainer: {
    width: wp(24),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: colors.green,
    // paddingHorizontal: 10.5,
    paddingVertical: 4.5,
    alignItems: "center",
    justifyContent: "center",
  },
  popular: {
    fontSize: RF(1.5),
    color: colors.white,
  },
  featureHighlight: {
    fontWeight: "500",
    color: colors.black,
  },
  license: {
    fontSize: RF(1.9),
    color: colors.white,
  },
  // planList: {
  //   paddingBottom: wp(2)
  // },
  cardHeader: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: colors.brightBlue,
    paddingHorizontal: wp(3),
    paddingVertical: wp(3.5),
  },
  oldPrice: {
    opacity: 0.7,
    fontSize: RF(1.9),
    color: colors.white,
    textDecorationLine: "line-through",
  },
  saveContainer: {
    borderRadius: 1,
    backgroundColor: colors.squash,
    marginLeft: 10.4,
  },
  save: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.black,
    paddingHorizontal: 10.4,
    paddingVertical: 7.5,
  },
  planBlockMessage: {
    fontSize: RF(1.9),
    textAlign: "center",
    color: colors.white,
    padding: 12,
    lineHeight: wp(5),
  },
  imgBg: {
    width: wp(94),
    marginTop: -4,
    marginLeft: -0.5,
  },
  boldText: {
    fontWeight: "500",
  },
  disabled: {
    color: colors.steelGrey,
  },
  choosePayMethod: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginTop: wp(2.2),
    marginHorizontal: wp(3),
  },
  greyDropdownIcon: {
    height: wp(4.5),
    width: wp(4.5),
    resizeMode: "contain",
  },
});
