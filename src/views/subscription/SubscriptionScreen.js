import React from "react";
import { View, Text, StyleSheet, FlatList, Image, TouchableOpacity, ScrollView } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { Toolbar } from "../../components/Toolbar";
import { getSubscriptionPlans } from "../../controllers/SubscriptionController";
import LoadingView from "../../components/common/LoadingView";
import { numberWithCommas } from "../../utils/Utils";
import SubscriptionFeatures from "../../components/subscription/SubscriptionFeatures";
import { connect } from "react-redux";
import LoginModal from "../../components/common/LoginModal";

const plans = [
  {
    name: "Basic",
    features: ["Weekly", "20", "Quarterly", "20", "60"],
    popular: false,
    license: false,
    color: colors.brightBlue,
  },
  {
    name: "GuruQ",
    features: ["Daily", "60", "Monthly", "60", "200"],
    popular: true,
    license: false,
    color: colors.peacockBlue,
  },
  {
    name: "StratQ",
    quarterPrice: "",
    features: ["Hourly", "150", "Weekly", "300", "1200"],
    popular: false,
    license: true,
    color: colors.marineBlue,
  },
];

class SubscriptionScreen extends React.PureComponent {
  componentDidMount() {
    this.setNavigationListener();
  }

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    !!this.navListener && this.navListener.remove();
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedPlan: "GuruQ",
      refreshing: false,
      all_plans: {},
      active_plan: {},
      discountData: {},
      initial_txn_exist: false,
    };
  }

  setNavigationListener = () => {
    const { navigation } = this.props;
    this.navListener = navigation.addListener("willFocus", (payload) => {
      this.getData();
    });
  };

  refreshScreen = () => {
    this.getData();
  };

  gotoPayMode = () => {
    const { all_plans, discountData, selectedPlan, active_plan } = this.state;
    const { isGuestUser } = this.props.userState;
    const data = {
      plan: all_plans[selectedPlan],
      offers: discountData.available_offers[selectedPlan],
      subscriptionPlan: selectedPlan,
      isSubscriber: Object.keys(active_plan).length > 0,
      active_plan: active_plan,
    };
    if (!isGuestUser) {
      this.props.navigation.navigate("SubscriptionPayMode", data);
    } else this.refs.loginModal.open();
  };

  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    this.setState({ refreshing: true });

    getSubscriptionPlans(baseURL, appToken, token, (isSuccess, data) => {
      if (isSuccess) {
        this.setState(Object.assign({ refreshing: false }, data));
        // console.log("plans state", data);
      }
    });
  };

  selectPlan = (name) => {
    name == this.state.selectedPlan ? this.setState({ selectedPlan: "" }) : this.setState({ selectedPlan: name });
  };

  renderPlanDetails = (data) => {
    const { active_plan, all_plans, selectedPlan } = this.state;

    const isActivePlan = Object.keys(active_plan).length > 0;
    const activePlan = isActivePlan ? all_plans[active_plan.plan] : "";
    const activePlanBtnText = isActivePlan
      ? active_plan.plan == selectedPlan
        ? "Modify"
        : activePlan[active_plan.variant_type].subscribeText
      : "Subscribe";

    const features = data.features;
    const buttonText =
      data.name == active_plan.plan
        ? activePlanBtnText
        : data.annual.subscribeText
        ? data.annual.subscribeText
        : "Subscribe";

    return (
      <View style={[styles.card, styles.featureCard]}>
        <View style={styles.featureRow}>
          <View style={styles.dot} />
          <Text style={styles.feature}>
            Set strategy and custom alerts <Text>{features[0]}</Text>
          </Text>
        </View>

        <View style={styles.featureRow}>
          <View style={styles.dot} />
          <Text style={styles.feature}>
            <Text>{features[1]}</Text> rebalances per backtest
          </Text>
        </View>

        <View style={styles.featureRow}>
          <View style={styles.dot} />
          <Text style={styles.feature}>
            Run screener time machine <Text>{features[2]}</Text>
          </Text>
        </View>

        <View style={styles.featureRow}>
          <View style={styles.dot} />
          <Text style={styles.feature}>
            Set strategy alerts on upto <Text>{features[3]}</Text> screeners
          </Text>
        </View>

        <View style={styles.featureRow}>
          <View style={styles.dot} />
          <Text style={styles.feature}>
            <Text>{features[4]}</Text> backtests annually
          </Text>
        </View>

        <TouchableOpacity style={styles.subscribeBtn} onPress={this.gotoPayMode}>
          <Text style={styles.subscribeBtnText}>{buttonText}</Text>
        </TouchableOpacity>
      </View>
    );
  };

  showPlan = (plan) => {
    const annual = plan.annual ? plan.annual : {};
    const annual_autorenew = plan.annual_autorenew ? plan.annual_autorenew : {};
    const quarterly_autorenew = plan.annual_autorenew ? plan.annual_autorenew : {};

    return annual.showPlan || annual_autorenew.showPlan || quarterly_autorenew.showPlan ? true : false;
  };

  renderPlanItem = ({ item, index }) => {
    const { all_plans } = this.state;
    const data = all_plans[item.name] ? all_plans[item.name] : {};
    const showPlan = this.showPlan(data);

    if (showPlan) {
      const showAnnualPrice = data.annual ? true : false;

      const annualPrice = showAnnualPrice
        ? data.annual.is_upgrade
          ? numberWithCommas(data.annual.upgradePrice.toFixed(0))
          : numberWithCommas(data.annual.actualPrice.toFixed(0))
        : "";

      const annualAutoRenewPrice = item.license
        ? data.annual_autorenew.is_upgrade
          ? numberWithCommas(data.annual_autorenew.upgradePrice.toFixed(0))
          : numberWithCommas(data.annual_autorenew.actualPrice.toFixed(0))
        : "";

      const showQuarterlyPrice = data.quarterly_autorenew ? true : false;
      const quarterlyPrice = showQuarterlyPrice
        ? data.quarterly_autorenew.is_upgrade
          ? numberWithCommas(data.quarterly_autorenew.upgradePrice.toFixed(0))
          : numberWithCommas(data.quarterly_autorenew.actualPrice.toFixed(0))
        : "";

      const showInfo = this.state.selectedPlan == item.name;
      const message = showInfo ? "Tap to hide features" : "Tap to view features";
      // const showQuarterPrice = item.quarterPrice ? true : false;

      const headerStyle = showInfo
        ? [
            styles.cardHeader,
            { borderBottomLeftRadius: 0, borderBottomRightRadius: 0 },
            { backgroundColor: item.color },
          ]
        : [styles.cardHeader, { borderColor: item.color, borderWidth: 1 }];

      item.popular ? headerStyle.push({ borderTopLeftRadius: 0 }) : null;

      const planNameStyle = showInfo ? styles.planName : [styles.planName, { color: item.color }];
      const annualPriceStyle = showInfo ? styles.annualPrice : [styles.annualPrice, { color: item.color }];
      const quarterPriceStyle = showInfo ? styles.quarterPrice : [styles.quarterPrice, { color: item.color }];
      const tapMessageStyle = showInfo ? styles.tapShow : [styles.tapShow, { color: colors.greyishBrown }];
      const licenseStyle = showInfo ? styles.license : [styles.license, { color: item.color }];

      return (
        <View style={styles.planContainer}>
          {item.popular && (
            <View style={styles.popularContainer}>
              <Text style={styles.popular}>POPULAR</Text>
            </View>
          )}

          <TouchableOpacity style={headerStyle} onPress={() => this.selectPlan(item.name)}>
            <View style={{ flex: 1 }}>
              <Text style={planNameStyle}>
                {item.name} {item.license && <Text style={licenseStyle}>{"LICENSE"}</Text>}
              </Text>
              <Text style={tapMessageStyle}>{message}</Text>
            </View>

            <View>
              {showAnnualPrice && (
                <Text style={annualPriceStyle}>
                  <Text style={styles.bold}>&#8377;{annualPrice}</Text> Yr.
                </Text>
              )}

              {showQuarterlyPrice && (
                <Text style={quarterPriceStyle}>
                  <Text style={styles.bold}>&#8377;{quarterlyPrice}</Text> Qtr.
                </Text>
              )}

              {item.license && (
                <Text style={quarterPriceStyle}>
                  <Text style={styles.bold}>&#8377;{annualAutoRenewPrice}</Text> Yr. Auto-renew
                </Text>
              )}
            </View>
          </TouchableOpacity>

          {showInfo && this.renderPlanDetails(Object.assign(data, item))}
        </View>
      );
    }
  };

  renderSubscriptionPlans = () => {
    const { refreshing, all_plans } = this.state;
    const showPlans = Object.keys(all_plans).length > 0 && !refreshing;

    return (
      <View style={styles.subscriptionContainer}>
        <Text style={[styles.subscriptionHeading]}>SUBSCRIPTION PLANS</Text>
        <Text style={styles.subscriptionDetails}>
          All plans include access to high return strategies, expert screener access, best DVM stocks, superstar and
          stock alerts, and an ad-free experience.
        </Text>

        {this.renderActivePlan()}

        {showPlans && (
          <FlatList
            contentContainerStyle={styles.planList}
            data={plans}
            extraData={this.state}
            renderItem={this.renderPlanItem}
            keyExtractor={(item, index) => index.toString()}
          />
        )}

        {this.renderContactUs()}
      </View>
    );
  };

  renderContactUs = () => {
    const { navigate } = this.props.navigation;

    return (
      <Text style={styles.footerText}>
        If you are an <Text style={styles.bold}>advisor or enterprise user</Text> looking for a different plan, please{" "}
        <Text style={styles.contactUs} onPress={() => navigate("ContactUs")}>
          contact us
        </Text>
      </Text>
    );
  };

  renderDiscountBanner = () => {
    const { discountData } = this.state;
    const showDiscountBanner = discountData.discountBanner ? true : false;
    const discountBanner = showDiscountBanner ? discountData.discountBanner : "";

    if (showDiscountBanner) {
      return (
        <View style={styles.offerContainer}>
          <Text style={styles.offer}>{discountBanner}</Text>
        </View>
      );
    }
  };

  renderActivePlan = () => {
    const { active_plan } = this.state;

    if (Object.keys(active_plan).length > 0) {
      const { plan, variant_name, planAmount, variant_type } = active_plan;
      const amount = planAmount ? numberWithCommas(planAmount) : planAmount;
      const formattedText =
        variant_type != "annual"
          ? `Active Plan: ${plan} ${variant_name} for \u20B9${amount} with Auto renew and Price lock`
          : `Active Plan: ${plan} ${variant_name} One Time for \u20B9${amount}`;

      return <Text style={styles.activePlanText}>{formattedText}</Text>;
    }
  };

  render() {
    const { refreshing } = this.state.refreshing;

    return (
      <View style={styles.container}>
        <Toolbar title="Subscribe" drawerMenu {...this.props} />

        <ScrollView>
          {this.renderDiscountBanner()}
          <SubscriptionFeatures />
          {this.renderSubscriptionPlans()}
        </ScrollView>

        <LoginModal ref="loginModal" {...this.props} refreshScreen={this.refreshScreen} />
        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(SubscriptionScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  offerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
    borderBottomWidth: 4.2,
    borderBottomColor: "rgba(36,83,161,0.15)",
    backgroundColor: colors.whiteTwo,
  },
  offer: {
    flex: 1,
    fontSize: RF(1.9),
    // fontWeight: "500",
    color: colors.lightNavy,
    lineHeight: wp(5.5),
  },
  card: {
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  headingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(6),
    alignItems: "center",
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  subscriptionDetails: {
    fontSize: RF(2.2),
    color: colors.black,
    marginHorizontal: wp(3),
  },
  subscriptionContainer: {
    backgroundColor: colors.white,
    marginTop: wp(5),
  },
  subscriptionHeading: {
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
    marginTop: wp(5),
    marginBottom: wp(3),
    marginHorizontal: wp(3),
  },
  cardHeader: {
    flexDirection: "row",
    alignItems: "center",
    borderRadius: 5,
    backgroundColor: colors.white,
    padding: wp(3),
  },
  planContainer: {
    marginHorizontal: wp(3),
    marginTop: wp(4),
  },
  annualPrice: {
    fontSize: RF(2.2),
    color: colors.white,
    textAlign: "right",
  },
  quarterPrice: {
    fontSize: RF(1.9),
    color: colors.white,
    marginTop: 6,
    textAlign: "right",
  },
  bold: {
    fontWeight: "500",
  },
  planName: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.white,
  },
  tapShow: {
    fontSize: RF(1.9),
    color: "rgba(255,255,255,0.75)",
    marginTop: 6,
  },
  featureCard: {
    borderTopLeftRadius: 0,
    borderTopRightRadius: 0,
    padding: wp(3),
  },
  dot: {
    width: 6,
    height: 6,
    borderRadius: 3,
    backgroundColor: colors.greyishBrown,
  },
  featureRow: {
    flexDirection: "row",
    alignItems: "center",
    marginBottom: 11,
  },
  feature: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginLeft: 6,
  },
  subscribeBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(4),
    marginTop: 10,
    alignItems: "center",
    justifyContent: "center",
  },
  subscribeBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    margin: 11,
  },
  footerText: {
    fontSize: RF(2.2),
    lineHeight: wp(5),
    color: colors.black,
    marginHorizontal: wp(3),
    marginTop: wp(5),
    marginBottom: wp(8),
    // marginBottom: (Platform.OS = "ios" ? wp(8) : wp(6))
  },
  contactUs: {
    color: colors.primary_color,
    fontWeight: "500",
  },
  popularContainer: {
    width: wp(24),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: colors.squash,
    // paddingHorizontal: 10.5,
    paddingVertical: 4.5,
    alignItems: "center",
    justifyContent: "center",
  },
  popular: {
    fontSize: RF(1.5),
    color: colors.black,
  },
  featureHighlight: {
    fontWeight: "500",
    color: colors.black,
  },
  license: {
    fontSize: RF(1.9),
    color: "rgba(255, 255, 255, 0.6)",
  },
  planList: {
    paddingBottom: wp(2),
  },
  activePlanText: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.lightNavy,
    paddingHorizontal: wp(3.2),
    paddingVertical: 12,
    backgroundColor: "rgba(36,83,161,0.08)",
    marginBottom: wp(1),
    marginTop: wp(4),
  },
});
