import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity, Platform, TextInput, Picker, FlatList } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors, globalStyles } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { STATES_OF_INDIA, COUNTRY_LIST } from "../../utils/Constants";
import { Toolbar } from "../../components/Toolbar";
import { getCustomerInfo, setCustomerInfo } from "../../controllers/SubscriptionController";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import { validateMobileNumber, validatePinCode, validateGstNumber } from "../../utils/Utils";
import { connect } from "react-redux";

let form = [
  { label: "Name", inputType: "default", mandatory: true, type: "text", maxLength: 30 },
  { label: "Contact Number", inputType: "number-pad", mandatory: true, type: "text", maxLength: 10 },
  {
    label: "Email ID",
    inputType: "email-address",
    mandatory: true,
    type: "text",
    key: "Email",
    maxLength: 60,
    disable: true,
  },
  { label: "State", inputType: "default", mandatory: true, type: "dropdown" },
  { label: "Country", inputType: "default", mandatory: true, type: "dropdown", value: "India" },
  { label: "GST", type: "button" },
  { label: "Address", inputType: "default", mandatory: true, type: "text", maxLength: 120 },
  // { label: "City", inputType: "default", type: "text", maxLength: 30 },
  { label: "PIN Code", inputType: "number-pad", mandatory: true, type: "text", maxLength: 6 },
  { label: "GST Number", inputType: "default", mandatory: true, type: "text", maxLength: 15 },
];

class SubscriptionFormScreen extends React.PureComponent {
  textInputRefs = {};

  constructor(props) {
    super(props);
    this.state = {
      watchlistName: "",
      watchlistDescription: "",
      textFocused: null,
      isRefreshing: false,
      showDropdown: "",
      dropdownPosition: {},
      isFromValidated: false,
      City: "",
      State: "",
      Name: "",
      Country: "India",
      countryCode: "IN",
      Address: "",
      "Contact Number": "",
      "PIN Code": "",
      Email: "",
      "GST Number": "",
      addGST: false,
      isScrollable: true,
      plan: {},
      offers: [],
      subscriptionPlan: "",
      payMethod: "",
      planKey: "",
      amount: 0,
      currentPlan: {},
      isSubscriber: false,
      active_plan: {},
      isCreditCard: false,
    };
  }

  componentDidMount() {
    const { navigation } = this.props;
    const subscriptionPlan = navigation.getParam("subscriptionPlan", "");
    const plan = navigation.getParam("plan", {});
    const offers = navigation.getParam("offers", {});
    const payMethod = navigation.getParam("payMethod", "");
    const planKey = navigation.getParam("planKey", "");
    const amount = navigation.getParam("amount", 0);
    const currentPlan = navigation.getParam("currentPlan", {});
    const isSubscriber = navigation.getParam("isSubscriber", false);
    const active_plan = navigation.getParam("active_plan", {});
    const creditCard = navigation.getParam("isCreditCard", false);

    this.setState({
      plan: plan,
      currentPlan: currentPlan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      payMethod: payMethod,
      planKey: planKey,
      amount: amount,
      isSubscriber: isSubscriber,
      active_plan: active_plan,
      isCreditCard: creditCard,
    });

    this.getData();
  }

  // componentWillMount() {
  //   this.saveForm();
  // }

  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    this.setState({ isRefreshing: true });

    getCustomerInfo(baseURL, appToken, token, (data) =>
      this.setState(Object.assign({ isRefreshing: false }, data), () => {
        if (this.state["GST Number"]) this.setState({ addGST: true });

        for (let i = 0; i < COUNTRY_LIST.length; i++) {
          if (COUNTRY_LIST[i].code == this.state.Country) {
            this.setState({ Country: COUNTRY_LIST[i].name, countryCode: COUNTRY_LIST[i].code });
            break;
          }
        }
      })
    );
  };

  saveForm = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { Name, Email, City, State, countryCode, addGST } = this.state;

    let country = countryCode.trim();
    let contactNumber = parseInt(this.state["Contact Number"].trim());
    let address = this.state.Address.trim();
    let pinCode = this.state["PIN Code"].trim();
    let gstNumber = this.state["GST Number"].trim();

    this.setState({ isRefreshing: true });

    setCustomerInfo(
      baseURL,
      appToken,
      token,
      Name.trim(),
      contactNumber,
      Email.trim(),
      State.trim(),
      country,
      address,
      pinCode,
      gstNumber,
      (success, error) => {
        this.setState({ isRefreshing: false });
        if (success) {
          this.gotoSubscriptionDetails();
        } else {
          error ? this.refs.toast.show(error, 1000) : null;
        }
      }
    );
  };

  gotoSubscriptionDetails = () => {
    const {
      plan,
      offers,
      selectedPlan,
      payMethod,
      planKey,
      Name,
      Address,
      Email,
      City,
      State,
      Country,
      amount,
      currentPlan,
      subscriptionPlan,
      isSubscriber,
      active_plan,
      isCreditCard,
    } = this.state;
    const customerInfo = {
      Name: Name.trim(),
      Address: Address.trim(),
      Email: Email.trim(),
      "Contact Number": parseInt(this.state["Contact Number"].trim()),
      City: City.trim(),
      State: State.trim(),
      Country: Country.trim(),
      "PIN Code": this.state["PIN Code"].trim(),
    };

    const data = {
      plan: plan,
      currentPlan: currentPlan,
      offers: offers,
      subscriptionPlan: subscriptionPlan,
      customer: customerInfo,
      payMethod: payMethod,
      planKey: planKey,
      amount: amount,
      isSubscriber: isSubscriber,
      active_plan: active_plan,
      isCreditCard: isCreditCard,
    };
    this.props.navigation.navigate("SubscriptionDetails", data);
  };

  validateForm = () => {
    const { Name, Email, Address, State, Country, addGST } = this.state;

    if (Name.trim().length == 0) {
      this.refs.toast.show("Please enter name ", 1000);
    } else if (this.state["Contact Number"].trim().length == 0) {
      this.refs.toast.show("Please enter contact number ", 1000);
    } else if (State.trim().length == 0) {
      this.refs.toast.show("Please select state ", 1000);
    } else if (!validateMobileNumber(this.state["Contact Number"].trim())) {
      this.refs.toast.show("Please enter valid contact number ", 1000);
    } else {
      if (addGST && this.state["GST Number"].length > 0) {
        if (!validateGstNumber(this.state["GST Number"])) {
          this.refs.toast.show("Please enter valid GST Number ", 1000);
        } else if (Address.trim().length == 0) {
          this.refs.toast.show("Please enter Address ", 1000);
        } else if (this.state["PIN Code"].trim().length == 0) {
          this.refs.toast.show("Please enter pin code ", 1000);
        } else if (!validatePinCode(this.state["PIN Code"].trim())) {
          this.refs.toast.show("Please enter valid pin code ", 1000);
        } else {
          this.setState({ isFromValidated: true });
          this.saveForm();
        }
      } else {
        this.setState({ isFromValidated: true });
        this.saveForm();
      }
      console.log("Valid");
    }
  };

  updateDropdownPosition = (event, label) => {
    let obj = { ...this.state.dropdownPosition, [label]: event.nativeEvent.layout };
    this.setState({ dropdownPosition: obj });
  };

  setInputText = (item, value) => {
    const key = item.key ? item.key : item.label;
    this.setState({ [key]: value });
  };

  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value, showDropdown: false, isScrollable: true });
  };

  toggleDropdown = (value) => {
    const { showDropdown } = this.state;
    showDropdown == value
      ? this.setState({ showDropdown: "", textFocused: "", isScrollable: true })
      : this.setState({ showDropdown: value, textFocused: value, isScrollable: false });
  };

  toggleGSTForm = () => this.setState({ addGST: !this.state.addGST, isScrollable: true });

  setDropdownSelection = (item) => {
    const { showDropdown, countryCode } = this.state;
    if (showDropdown == "State") {
      if (item.name == "Outside India" && countryCode == "IN") this.setState({ Country: "", countryCode: "" });
      else if (item.name != "Outside India" && countryCode != "IN") this.setState({ Country: "", countryCode: "" });
      this.setState({ [showDropdown]: item.name });
    } else {
      if (item.code != "IN") this.setState({ State: "Outside India" });
      this.setState({ [showDropdown]: item.name, countryCode: item.code });
    }

    this.setState({ showDropdown: "", textFocused: "", isScrollable: true });
  };

  renderInputBox = (item) => {
    const { textFocused } = this.state;
    const label = item.mandatory ? item.label : item.label + " (optional)";
    const key = item.key ? item.key : item.label;
    const inputBoxStyle =
      textFocused == item.label ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;

    return (
      <View style={inputBoxStyle}>
        <Text style={styles.inputLabel} key={item.label}>
          {label}
          {item.mandatory && <Text style={styles.redText}> *</Text>}
        </Text>

        <TextInput
          ref={(ref) => (this.textInputRefs[item.name] = ref)}
          // placeholder={placeholder}
          style={styles.textInput}
          maxLength={item.maxLength}
          keyboardType={item.inputType}
          editable={item.disable ? false : true}
          placeholderTextColor={colors.steelGrey}
          value={this.state[key]}
          onChangeText={(value) => this.setInputText(item, value)}
          onFocus={() => this.toggleTextInputFocus(item.label)}
          onBlur={() => this.toggleTextInputFocus(null)}
        />
      </View>
    );
  };

  renderDropdownItem = ({ item }) => {
    return (
      <TouchableOpacity onPress={() => this.setDropdownSelection(item)}>
        <Text style={styles.listItem}>{item.name}</Text>
      </TouchableOpacity>
    );
  };

  // renderStateList = () => {
  //   var { x, y, width, height } = this.state.dropdownPosition;
  //   // console.log("pos", this.state.dropdownPosition);
  //   const style = {
  //     position: "absolute",
  //     top: y,
  //     left: x,
  //     width: width,
  //     // height: hp(10),
  //     maxHeight: hp(25),
  //     backgroundColor: colors.white,
  //   };

  //   // return (
  //   //   <Modal
  //   //     visible={true}
  //   //     transparent={true}
  //   //     // onRequestClose={this.blur}
  //   //     // supportedOrientations
  //   //     // style={[style, styles.dropdownList]}
  //   //   >
  //   //     <View style={[style, styles.dropdownList]}>
  //   //       {/* <View style={{ maxHeight: hp(25) }}> */}
  //   //       <FlatList
  //   //         // contentContainerStyle={{ maxHeight: hp(20) }}
  //   //         data={STATES_OF_INDIA}
  //   //         extraData={this.state}
  //   //         showsVerticalScrollIndicator={false}
  //   //         renderItem={this.renderDropdownItem}
  //   //         keyExtractor={(item, index) => index.toString()}
  //   //       />
  //   //     </View>
  //   //     {/* </View> */}
  //   //   </Modal>
  //   // );

  //   return (
  //     <Picker
  //       selectedValue={this.state.State}
  //       style={{ height: hp(8), width: width }}
  //       onValueChange={(itemValue, itemIndex) => {
  //         this.setState({ State: itemValue });
  //         // this.validateForm();
  //         form[6].value = itemValue;
  //       }}
  //     >
  //       {STATES_OF_INDIA.map((item) => (
  //         <Picker.Item label={item.name} value={item.name} />
  //       ))}
  //     </Picker>
  //   );
  // };

  renderDropdownList = () => {
    const { showDropdown } = this.state;

    if (showDropdown) {
      var { x, y, width, height } = this.state.dropdownPosition[showDropdown];
      const style = {
        position: "absolute",
        top: y + hp(8.1),
        left: x,
        // bottom: wp(6),
        width: width,
        // height: hp(25),
        maxHeight: hp(30),
        backgroundColor: colors.white,
      };

      const LIST_DATA = showDropdown == "State" ? STATES_OF_INDIA : COUNTRY_LIST;

      return (
        <View style={[style, styles.dropdownList]}>
          {/* <View style={{ maxHeight: hp(25) }}> */}
          <FlatList
            contentContainerStyle={{ paddingVertical: wp(3) }}
            data={LIST_DATA}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderDropdownItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
    }
  };

  renderDropdown = (item) => {
    const { showDropdown, textFocused } = this.state;
    const dropdownIcon =
      showDropdown == item.label
        ? require("../../assets/icons/arrow-up-blue.png")
        : require("../../assets/icons/arrow-down-blue.png");

    const label = item.mandatory ? item.label : item.label + " (optional)";
    const key = item.key ? item.key : item.label;
    const inputBoxStyle =
      textFocused == item.label ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;
    let value = this.state[label];

    return (
      <View style={inputBoxStyle} onLayout={(e) => this.updateDropdownPosition(e, label)}>
        <Text style={styles.inputLabel} key={item.label}>
          {label}
          {item.mandatory && <Text style={styles.redText}> *</Text>}
        </Text>
        <View style={styles.dropdown}>
          <TouchableOpacity style={[styles.row]} onPress={() => this.toggleDropdown(item.label)}>
            <Text style={[styles.textInput, { marginLeft: 0 }]}>{value}</Text>
            <Image source={dropdownIcon} style={styles.dropdownIcon} />
          </TouchableOpacity>
          {/* {showDropdown == item.label && this.renderDropdownList()} */}
        </View>
      </View>
    );
  };

  // renderFormItem = (item, index) => {
  //   const { showDropdown } = this.state;
  //   const inputBoxStyle =
  //     this.state.textFocused == item.label ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;

  //   const label = item.mandatory ? item.label : item.label + " (optional)";

  //   return (
  //     <View onLayout={this.updateDropdownPosition} key={item.label}>
  //       <Text style={styles.inputLabel} key={item.label}>
  //         {label}
  //         {item.mandatory && <Text style={styles.redText}> *</Text>}
  //       </Text>
  //       {/* <View style={inputBoxStyle}>
  //         {item.type == "text"
  //           ? this.renderInputBox(item)
  //           : this.renderDropdown(item)}
  //       </View> */}

  //       {item.type == "text" ? <View style={inputBoxStyle}>{this.renderInputBox(item)}</View> : null}

  //       {/* {showDropdown == item.label &&
  //         item.label == "State" &&
  //         this.renderDropdownList()} */}

  //       {item.label == "State" && this.renderStateList()}
  //       {item.label == "State" ? <View style={styles.DropDownStyle}></View> : null}
  //       {item.label == "Country" && this.renderCountryList()}
  //       {item.label == "Country" ? <View style={styles.DropDownStyle}></View> : null}
  //     </View>
  //   );
  // };

  renderButton = (item) => {
    if (item.label == "GST") {
      const { addGST } = this.state;
      let label = addGST ? "Hide GST" : "Add GST";

      return (
        <TouchableOpacity onPress={this.toggleGSTForm}>
          <Text style={styles.gstToggleStyle}>{label}</Text>
        </TouchableOpacity>
      );
    }
  };

  renderSubscriptionForm = () => {
    const { addGST } = this.state;

    return form.map((item, index) => {
      if (index > 5 && !addGST) return null;

      switch (item.type) {
        case "text":
          return this.renderInputBox(item);
        case "dropdown":
          return this.renderDropdown(item);
        case "button":
          return this.renderButton(item);
      }
    });
  };

  render() {
    const { isFromValidated, subscriptionPlan, isScrollable } = this.state;

    const title = subscriptionPlan + " Subscription";

    const buttonStyle = isFromValidated
      ? styles.createWatchlistBtn
      : // : [styles.createWatchlistBtn, styles.greyBackground];
        [styles.createWatchlistBtn];

    const buttonText = isFromValidated
      ? styles.createWatchlistBtnText
      : [styles.createWatchlistBtnText, styles.whiteText];

    return (
      <View style={styles.container}>
        <Toolbar title={title} backButton {...this.props} />

        <KeyboardAwareScrollView
          style={{ paddingHorizontal: wp(3) }}
          showsVerticalScrollIndicator={false}
          scrollEnabled={isScrollable}
        >
          <Text style={styles.heading}>BILLING INFORMATION</Text>

          {this.renderSubscriptionForm()}
          {this.renderDropdownList()}

          <TouchableOpacity style={buttonStyle} onPress={this.validateForm}>
            <Text style={buttonText}>Review and Pay</Text>
          </TouchableOpacity>

          <View style={styles.extraSpace} />
        </KeyboardAwareScrollView>

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        {this.state.isRefreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(SubscriptionFormScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
    marginTop: wp(6),
    marginBottom: wp(5.5),
  },
  // infoDetails: {
  //   opacity: 0.85,
  //   fontSize: RF(2),
  //   color: colors.black,
  //   marginBottom: wp(4),
  // },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  extraSpace: {
    flex: 1,
    height: hp(20),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  itemRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
  },
  itemIcon: {
    width: wp(4),
    height: wp(3),
    resizeMode: "contain",
  },
  itemName: {
    marginLeft: wp(3),
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  addWatchlistBtn: {
    position: "absolute",
    left: wp(8),
    right: wp(8),
    bottom: wp(8),
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
  },
  addWatchlistBtnDisabled: {
    backgroundColor: colors.steelGrey,
  },
  addWatchlistBtnText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: 2,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  inputContainer: {
    // flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black,

    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  inputLength: {
    fontSize: RF(1.5),
    fontWeight: "500",
    color: colors.steelGrey,
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(5),
    // marginBottom: hp(6),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(2.5),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  inputLabel: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  redText: {
    color: colors.red,
  },
  dropdown: {
    flex: 1,
    marginTop: Platform.OS == "android" ? 10 : 0,
    marginBottom: Platform.OS == "android" ? 12 : 0,
  },
  dropdownIcon: {
    marginLeft: wp(4),
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: "1%",
  },
  dropdownList: {
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    zIndex: 999,
  },
  DropDownStyle: {
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
    marginBottom: wp(6),
  },
  gstToggleStyle: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    paddingBottom: wp(5),
  },
  listItem: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
    color: colors.greyishBrown,
    fontSize: RF(2.2),
  },
});
