import React from "react";
import {
  View,
  StyleSheet,
  Text,
  ScrollView,
  Image,
  TouchableOpacity
} from "react-native";
import { colors } from "../styles/CommonStyles";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";

export default class SubscribeMessageScreen extends React.PureComponent {
  gotoSubscription = () => {
    this.props.navigation.navigate("Subscription");
  };

  renderFeatureIcon = (icon, positionStyle, name) => {
    return (
      <View style={[styles.rowItem, positionStyle]}>
        <View style={styles.featureIconContainer}>
          <Image source={icon} style={styles.featureIcon} />
        </View>
        <Text style={[styles.featureName, styles.leftItem]}>{name}</Text>
      </View>
    );
  };

  renderFeaturesRow = () => {
    return (
      <View style={styles.featureContainer}>
        <View style={styles.featureRow}>
          {this.renderFeatureIcon(
            require("../assets/icons/bell-blue.png"),
            styles.left,
            "Customised\nAlerts"
          )}

          {this.renderFeatureIcon(
            require("../assets/icons/search-blue.png"),
            styles.centerItem,
            "Portfolio\nAnalysis"
          )}

          {this.renderFeatureIcon(
            require("../assets/icons/dvm.png"),
            styles.rightItem,
            "DVM\nScores"
          )}
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <Text style={styles.title}>You seem like a Pro!</Text>

          <Text style={styles.description}>
            Get exclusive features to help you invest better!
          </Text>

          {this.renderFeaturesRow()}

          <Text style={styles.planLabel}>
            And many more benefits starting from just
          </Text>

          <Text style={styles.planPrice}>&#8377; 246/month</Text>

          <TouchableOpacity
            style={styles.plansButton}
            onPress={this.gotoSubscription}
          >
            <Text style={styles.plansButtonText}>See Plans</Text>
          </TouchableOpacity>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
    paddingHorizontal: wp(8),
    paddingTop: wp(30)
  },
  center: {
    alignItems: "center",
    justifyContent: "center"
  },
  title: {
    flex: 1,
    fontSize: RF(2.6),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(3),
    marginTop: hp(2)
    // paddingHorizontal: wp(4)
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  extraSpace: {
    marginBottom: hp(1)
  },
  featureRow: {
    flexDirection: "row"
    // justifyContent: "space-between",
  },
  rowItem: {
    flex: 1
  },
  featureContainer: {
    // flex: 1,
    marginVertical: hp(6),
    justifyContent: "center",
    alignItems: "center"
  },
  featureIconContainer: {
    width: wp(18),
    height: wp(18),
    backgroundColor: "rgba(36,83,161,0.1)",
    borderRadius: wp(9.5),
    padding: wp(5),
    alignItems: "center",
    justifyContent: "center"
  },
  featureIcon: {
    width: wp(9),
    height: wp(7),
    resizeMode: "contain"
  },
  featureName: {
    flex: 1,
    width: wp(20),
    marginTop: 10,
    textAlign: "center",
    fontWeight: "500",
    color: colors.greyishBrown
  },
  description: {
    opacity: 0.8,
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    marginRight: wp(4)
  },
  leftItem: {
    alignItems: "flex-start"
  },
  centerItem: {
    alignItems: "center"
  },
  rightItem: {
    alignItems: "flex-end"
  },
  planLabel: {
    fontSize: RF(2.2),
    color: colors.greyishBrown
  },
  planPrice: {
    fontSize: RF(2.8),
    fontWeight: "500",
    color: colors.black,
    textAlign: "center",
    marginTop: wp(2.2)
  },
  plansButton: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingVertical: wp(3),
    marginTop: hp(3.5)
  },
  plansButtonText: {
    fontSize: RF(2.2),
    color: colors.primary_color
  }
});
