import React from "react";
import { View, Text, Image, StyleSheet, TouchableOpacity, FlatList } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { getScreenersByTag } from "../../controllers/ScreenerController";
import { RECENT_SCREENERS } from "../../utils/Constants";
import { storeObjectListInAsync, getObjectListFromAsync } from "../../controllers/AsyncController";
import { Toolbar } from "../../components/Toolbar";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";
import NoMatchView from "../../components/common/NoMatchView";
import LoadingMoreView from "../../components/common/LoadingMoreView";

class ScreenerSearchScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      screeners: [],
      query: "",
      pageNumber: 0,
      perPageCount: 20,
      isNextPage: true,
      searchQuery: "",
      isRefreshing: false,
      isLoadingMore: false,
    };
  }

  doSearch = () => {
    const { navigation, baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { searchQuery, isRefreshing, perPageCount, screeners, pageNumber } = this.state;

    console.log("state", this.state);
    if (searchQuery.length > 0) {
      // this.setState({ isRefreshing: true });

      getScreenersByTag(
        baseURL,
        appToken,
        token,
        searchQuery,
        "",
        pageNumber,
        perPageCount,
        (data, isNextPage) => {
          this.setState({ screeners: data ? screeners.concat(data) : screeners });

          this.setState({
            isRefreshing: false,
            pageNumber: ++this.state.pageNumber,
            isNextPage: isNextPage,
            isLoadingMore: false,
          });
        },
        (statusCode, error) => {
          this.setState({ isRefreshing: false });
          if (statusCode == 4) navigation.navigate("SubscribeMessage");
        }
      );
    } else {
      this.resetScreenerList();
    }
  };

  resetScreenerList = () => {
    this.setState({
      searchQuery: "",
      screeners: [],
      pageNumber: 0,
      isNextPage: true,
      isRefreshing: false,
      isLoadingMore: false,
    });
  };

  loadMore = () => {
    const { isLoadingMore, isNextPage } = this.state;
    if (!isLoadingMore && isNextPage) this.setState({ isLoadingMore: true }, () => this.doSearch());
  };

  isDuplicateScreener = (screenerId, cb) => {
    let breakLoop = false;

    getObjectListFromAsync(RECENT_SCREENERS, (recentScreeners) => {
      if (recentScreeners.length > 0) {
        recentScreeners.map((screener, index) => {
          if (screener.screenId == screenerId) {
            cb(true);
            breakLoop = true;
          } else if (index == recentScreeners.length - 1 && !breakLoop) {
            cb(false);
          }
        });
      } else cb(false);
    });
  };

  onSearchChange = (query) => {
    this.setState({ searchQuery: query, screeners: [], isNextPage: true, pageNumber: 0, isRefreshing: true }, () =>
      this.doSearch()
    );
  };

  renderToolbar = () => {
    const { navigation } = this.props;

    return (
      <Toolbar
        title="Screeners"
        backButton
        {...this.props}
        showSearch
        hideSearch={() => navigation.pop()}
        searchPlaceholder="Search Screeners"
        onChangeText={this.onSearchChange}
      />
    );
  };

  openScreenerStockList = (item, index) => {
    const { navigation } = this.props;

    // console.log("Screener Search Item: ", item);

    let screener = {
      screenId: item.screenId,
      screener: item.title,
      description: this.state.screeners[index],
      bookmarkId: "bookmarkId" in item ? item.bookmarkId : "",
      isBookmarkable: "isBookmarkable" in item ? item.isBookmarkable : true,
    };

    this.isDuplicateScreener(item.screenId, (isDuplicate) => {
      if (!isDuplicate) {
        console.log("Duplicate screener ", isDuplicate);
        storeObjectListInAsync(RECENT_SCREENERS, screener).then(() => navigation.push("ScreenerStocks", screener));
      } else navigation.push("ScreenerStocks", screener);
    });
  };

  splitLetter(item) {
    let str = "";
    let words = item && "title" in item && typeof item.title == "string" ? item.title.split(" ") : [];

    if (Array.isArray(words)) {
      words.map((item) => {
        if (item.length > 0 && this.checkSpecialChar(item[0]) && str.length < 3) str += item[0];
      });
    }
    return str;
  }

  checkSpecialChar = (str) => {
    switch (str) {
      case "(":
      case "-":
      case ":":
        return false;
      default:
        return true;
    }
  };

  renderScreenerIcon = (item, index) => {
    const icon = item.iconURL ? item.iconURL : null;
    var colors = ["#00c4f5", "#006aff", "#6400eb", "#d500b7", "#fb3b94", "#a500d9"];

    if (icon) {
      return (
        <View style={styles.iconContainer}>
          <Image source={{ uri: icon }} style={styles.screenerIcon} />
        </View>
      );
    } else {
      return (
        <View style={styles.iconContainer}>
          <View
            style={[styles.noImageBox, { backgroundColor: colors[index && index < 5 ? index : parseInt(index) % 6] }]}
          >
            <Text style={styles.initial}>{this.splitLetter(item)}</Text>
          </View>
        </View>
      );
    }
  };

  renderSearchItem = ({ item, index }) => {
    let isPremium = typeof item.title == "string" && item.title.indexOf("subscription") != -1;

    return (
      <TouchableOpacity onPress={() => this.openScreenerStockList(item, index)}>
        <View style={styles.candleStickItem}>
          {this.renderScreenerIcon(item, index)}
          <View style={styles.nameContainer}>
            <Text style={styles.name} numberOfLines={3} ellipsizeMode="tail">
              {item.title}
            </Text>
            {/* <Text style={styles.performance} numberOfLines={2} ellipsizeMode="tail">
              Returns over 6.33 yrs <Text style={styles.boldText}>1374.67</Text> %
            </Text> */}
          </View>
          {isPremium && (
            <Image source={require("../../assets/icons/premium-icon-blue.png")} style={styles.premiumIcon} />
          )}
        </View>
      </TouchableOpacity>
    );
  };

  render() {
    const { isRefreshing, screeners, searchQuery, isLoadingMore } = this.state;

    const noSearchResult = !isRefreshing && searchQuery.length > 1 && screeners.length == 0;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        {isRefreshing && <LoadingView />}

        <FlatList
          contentContainerStyle={styles.list}
          data={screeners}
          extraData={this.state.screeners}
          renderItem={this.renderSearchItem}
          keyExtractor={(item, index) => item.screenId}
          ListFooterComponent={() => isLoadingMore && <LoadingMoreView />}
          showsVerticalScrollIndicator={false}
          onEndReached={this.loadMore}
          onEndReachedThreshold={0.7}
          removeClippedSubviews
          initialNumToRender={3}
          maxToRenderPerBatch={5}
          keyboardShouldPersistTaps="always"
        />

        {noSearchResult && <NoMatchView message="No Results found" />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ScreenerSearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
  },
  candleStickItem: {
    flex: 1,
    flexDirection: "row",
    paddingVertical: wp(4),
    alignItems: "center",
  },
  iconContainer: {
    width: wp(18),
    padding: "1%",
    alignItems: "center",
  },
  screenerIcon: {
    width: wp(8.5),
    height: wp(8.5),
    resizeMode: "contain",
  },
  noImageBox: {
    width: wp(13),
    height: wp(14),
    borderRadius: 5,
    // backgroundColor: colors.whiteTwo,
    padding: "1%",
    alignItems: "center",
    justifyContent: "center",
  },
  initial: {
    // fontSize: wp(2),
    color: colors.white,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  nameContainer: {
    flex: 1,
    paddingRight: "3%",
    justifyContent: "center",
  },
  name: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
    marginBottom: 5,
  },
  performance: {
    fontSize: RF(1.8),
    color: colors.greyishBrown,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : 0,
  },
  boldText: {
    fontWeight: "500",
  },
  premiumIcon: {
    marginHorizontal: wp(3),
    marginBottom: wp(1),
  },
});
