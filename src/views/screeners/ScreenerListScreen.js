import React from "react";
import { View, Image, StyleSheet, TouchableOpacity, Text, RefreshControl } from "react-native";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { FlatList } from "react-native-gesture-handler";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { getMyScreeners, getScreenersByTag, getBookmarkedScreeners } from "../../controllers/ScreenerController";
import { SCREENERS, RECENT_SCREENERS } from "../../utils/ScreenerConstants";
import ScreenerBottomSheet from "../../components/screeners/ScreenerBottomSheet";
import { storeObjectListInAsync, getObjectListFromAsync } from "../../controllers/AsyncController";
import ScreenerCategoryBottomSheet from "../../components/screeners/ScreenerCategoryBottomSheet";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";
import NoMatchView from "../../components/common/NoMatchView";
import LoginModal from "../../components/common/LoginModal";
import SuperstarAlertNonSub from "../../components/superstar/SuperstarAlertNonSub";

class ScreenerListScreen extends React.PureComponent {
  componentDidMount = () => {
    const title = this.props.navigation.getParam("title");
    if (title == "Candle Sticks" || title == "Technical") this.setState({ showBullishBearish: true });

    this.getScreenerList();
  };

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      selectedOption: 0,
      showBullishBearish: false,
      refreshing: false,
      isFirstCallComplete: false,
      selectedScreener: 0,
      isNextPage: true,
      isMyScreener: false,
      pageNumber: 0,
      recentScreeners: [],
    };
  }

  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => this.getScreenerList());
  };

  getScreenerList = () => {
    const title = this.props.navigation.getParam("title");
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;
    const { isNextPage, pageNumber, selectedOption } = this.state;

    switch (title) {
      case "My Screeners":
        if (isGuestUser) {
          this.setState({ isFirstCallComplete: true, refreshing: false });
          this.refs.login.open();
        } else {
          isNextPage &&
            getMyScreeners(baseURL, appToken, token, pageNumber, 20, (data, isNextPage, status) => {
              this.setData(data, isNextPage);

              if (status == 100) {
                this.refs.login.open();
              }
            });
        }
        break;

      case "Expert":
        isNextPage && this.getData("expert");
        break;

      case "Red Flags":
        isNextPage && this.getData("threat");
        break;

      case "Candle Sticks":
        selectedOption == 0 ? this.getData(["candlestick", "bullish"]) : this.getData(["candlestick", "bearish"]);

        // this.setState({
        //   data: selectedOption == 0 ? SCREENERS.candleStick.bullish : SCREENERS.candleStick.bearish,
        //   refreshing: false
        // });
        break;

      case "Fundamentals":
        this.getData("fundamental");
        break;

      case "Moving Average":
        this.getData("moving_avg");
        break;

      case "Price/Volume":
        this.getData("price_volume");
        break;

      case "Shareholding":
        this.getData("shareholding");
        break;

      case "Technical":
        selectedOption == 0 ? this.getData(["technical", "bullish"]) : this.getData(["technical", "bearish"]);
        break;

      case "Delivery":
        this.getData("delivery");
        break;

      case "Bookmarks":
        isNextPage &&
          getBookmarkedScreeners(baseURL, appToken, token, (data, isNextPage, status) => {
            this.setState({ data: [] }, () => this.setData(data, isNextPage));

            if (status == 100) {
              this.refs.login.open();
            }
          });
        // this.setState({ data: SCREENERS.delivery, refreshing: false  });
        break;

      case "Future":
      // this.setState({ data: SCREENERS.delivery, refreshing: false  });
      // break;

      case "Options":
      // this.setState({ data: SCREENERS.delivery, refreshing: false  });
      // break;

      default:
        this.setState({ isFirstCallComplete: true, refreshing: false });
        console.log("No Match found for screeners");
    }
  };

  getData = (tag) => {
    const { baseURL, appToken } = this.props;
    const { navigate } = this.props.navigation;
    const { token } = this.props.userState;
    const { pageNumber, refreshing } = this.state;

    getScreenersByTag(
      baseURL,
      appToken,
      token,
      "",
      tag,
      pageNumber,
      20,
      (data, isNextPage) => {
        if (refreshing) this.setState({ refreshing: false, data: [] }, () => this.setData(data, isNextPage));
        else this.setData(data, isNextPage);
      },
      (statusCode, error) => {
        if (refreshing) this.setState({ refreshing: false });
        if (statusCode == 4) navigate("SubscribeMessage");
      }
    );
  };

  setData = (data, isNextPage) => {
    if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });

    this.setState({
      data: data.length > 0 ? [...this.state.data, ...data] : this.state.data,
      refreshing: false,
      pageNumber: ++this.state.pageNumber,
      isNextPage: isNextPage,
    });
  };

  renderToolbar = () => {
    const { navigate } = this.props.navigation;
    const title = this.props.navigation.getParam("title");

    return (
      <Toolbar title={title} {...this.props} backButton>
        <ImageButton
          source={require("../../assets/icons/info-white.png")}
          onPress={() => this.refs.categoryBottomSheet.RBSheet.open()}
        />
        <ImageButton
          source={require("../../assets/icons/search-white.png")}
          onPress={() => navigate("ScreenerSearch")}
        />
      </Toolbar>
    );
  };

  renderOptions = (label, option) => {
    const { selectedOption } = this.state;

    const optionContainerStyle = selectedOption == option ? styles.optionSelected : {};
    const optionStyle = selectedOption == option ? styles.optionSelectedText : styles.optionText;
    const optionObj = { selectedOption: option, pageNumber: 0, refreshing: true };

    return (
      <TouchableOpacity
        style={[styles.commonContainerStyle, optionContainerStyle]}
        onPress={() => this.setState(optionObj, () => this.getScreenerList())}
      >
        <Text style={optionStyle}>{label}</Text>
      </TouchableOpacity>
    );
  };

  isDuplicateScreener = (screenerId, cb) => {
    let breakLoop = false;

    getObjectListFromAsync(RECENT_SCREENERS, (recentScreeners) => {
      if (recentScreeners.length > 0) {
        recentScreeners.map((screener, index) => {
          if (screener.screenId == screenerId) {
            cb(true);
            breakLoop = true;
          } else if (index == recentScreeners.length - 1 && !breakLoop) {
            cb(false);
          }
        });
      } else cb(false);
    });
  };

  openScreenerStockList = (item, isPremium) => {
    const { isSubscriber } = this.props.userState;
    const { navigate } = this.props.navigation;

    let screener = {
      screenId: item.screenId,
      screener: item.title,
      description: this.state.data[this.state.selectedScreener],
      bookmarkId: "bookmarkId" in item ? item.bookmarkId : "",
      isBookmarkable: "isBookmarkable" in item ? item.isBookmarkable : false, // false for candlestick
    };

    let isScreenerAvailable = isPremium ? (isSubscriber ? true : false) : true;

    if (isScreenerAvailable) {
      this.isDuplicateScreener(item.screenId, (isDuplicate) => {
        !isDuplicate
          ? storeObjectListInAsync(RECENT_SCREENERS, screener).then(() => navigate("ScreenerStocks", screener))
          : navigate("ScreenerStocks", screener);
      });
    } else {
      this.refs.subscriberModal && this.refs.subscriberModal.open();
    }
  };

  splitLetter(item) {
    let str = "";
    let words = item && "title" in item && typeof item.title == "string" ? item.title.split(" ") : [];

    if (Array.isArray(words)) {
      words.forEach((item) => {
        if (item.length > 0 && this.checkSpecialChar(item[0]) && str.length < 3) str += item[0];
      });
    }

    return str;
  }

  checkSpecialChar = (str) => {
    switch (str) {
      case "(":
        return false;
      case "-":
        return false;
      case ":":
        return false;
      default:
        return true;
    }
  };

  renderScreenerIcon = (item, index) => {
    const icon = item.iconURL ? item.iconURL : null;
    var colors = ["#00c4f5", "#006aff", "#6400eb", "#d500b7", "#fb3b94", "#a500d9"];

    if (icon) {
      return (
        <View style={styles.iconContainer}>
          <Image source={{ uri: icon }} style={styles.screenerIcon} />
        </View>
      );
    } else {
      return (
        <View style={styles.iconContainer}>
          <View
            style={[styles.noImageBox, { backgroundColor: colors[index && index < 5 ? index : parseInt(index) % 6] }]}
          >
            <Text style={styles.initial}>{this.splitLetter(item)}</Text>
          </View>
        </View>
      );
    }
  };

  renderBacktestInfo = (item) => {
    const showReturns = item.backtestReturns && item.backtestDuration;

    if (showReturns)
      return (
        <Text style={styles.performance} numberOfLines={2} ellipsizeMode="tail">
          Returns over {item.backtestDuration}
          <Text style={styles.boldText}>{" " + item.backtestReturns}</Text>%
        </Text>
      );
  };

  renderInfoIcon = (item, index, isPremium) => {
    return (
      <TouchableOpacity style={styles.infoIconContainer} onPress={() => this.openBottomSheet(index)}>
        {isPremium && <Image source={require("../../assets/icons/premium-icon-blue.png")} style={styles.premiumIcon} />}
        <Image source={require("../../assets/icons/info-blue.png")} style={styles.infoIcon} />
      </TouchableOpacity>
    );
  };

  renderScreenerItem = ({ item, index }) => {
    let isPremium = "isPremium" in item ? item.isPremium : false;

    return (
      <TouchableOpacity onPress={() => this.openScreenerStockList(item, isPremium)}>
        <View style={styles.candleStickItem}>
          {this.renderScreenerIcon(item, index)}

          <View style={styles.nameContainer}>
            <Text style={styles.name} numberOfLines={3} ellipsizeMode="tail">
              {item.title}
            </Text>

            {this.renderBacktestInfo(item)}
          </View>
          {this.renderInfoIcon(item, index, isPremium)}
        </View>
      </TouchableOpacity>
    );
  };

  openBottomSheet = (index) => {
    this.setState({ selectedScreener: index }, () => this.refs.bottomSheet.RBSheet.open());
  };

  renderListHeader = () => {
    const { data, showBullishBearish } = this.state;

    if (data.length > 0 && showBullishBearish) {
      return (
        <View style={[styles.optionList]}>
          {this.renderOptions("Bullish", 0)}
          <View style={{ marginHorizontal: wp(2) }}></View>
          {this.renderOptions("Bearish", 1)}
        </View>
      );
    } else return null;
  };

  renderListWithOptions = () => {
    if (this.state.data.length > 0)
      return (
        <FlatList
          contentContainerStyle={styles.list}
          horizontal={false}
          data={this.state.data}
          renderItem={this.renderScreenerItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={this.renderListHeader}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          showsVerticalScrollIndicator={false}
          refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
          onEndReached={this.getScreenerList}
          onEndReachedThreshold={0.7}
          removeClippedSubviews
          initialNumToRender={8}
          maxToRenderPerBatch={5}
        />
      );
  };

  render() {
    const categoryInfo = this.props.navigation.getParam("info");

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        {this.renderListWithOptions()}

        <ScreenerBottomSheet ref="bottomSheet" {...this.props} info={this.state.data[this.state.selectedScreener]} />
        <ScreenerCategoryBottomSheet ref="categoryBottomSheet" {...this.props} info={categoryInfo} />

        {this.state.data.length == 0 && this.state.isFirstCallComplete && <NoMatchView message="No Results found" />}
        {this.state.data.length == 0 && !this.state.isFirstCallComplete && <LoadingView />}
        <LoginModal ref="login" {...this.props} />
        <SuperstarAlertNonSub ref="subscriberModal" {...this.props} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ScreenerListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  candleStickItem: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    // minHeight: hp(9),
    paddingVertical: wp(4),
  },
  iconContainer: {
    width: wp(18),
    padding: "1%",
    alignItems: "center",
    justifyContent: "flex-start",
  },
  screenerIcon: {
    width: wp(8.5),
    height: wp(8.5),
    resizeMode: "contain",
  },
  noImageBox: {
    width: wp(13),
    height: wp(14),
    borderRadius: 5,
    //backgroundColor: colors.whiteTwo,
    padding: "1%",
    alignItems: "center",
    justifyContent: "center",
  },
  initial: {
    fontSize: RF(2.4),
    color: colors.white,
    fontWeight: "500",
    textTransform: "uppercase",
  },
  nameContainer: {
    flex: 1,
    paddingRight: wp(2),
    justifyContent: "center",
  },
  name: {
    fontSize: RF(2.2),
    fontWeight: "400",
    color: colors.primary_color,
    // marginBottom: 6
  },
  performance: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    opacity: 0.85,
    marginTop: 6,
  },
  infoIconContainer: {
    padding: wp(3),
    alignItems: "center",
  },
  premiumIcon: {
    marginBottom: wp(0.7),
  },
  infoIcon: {
    // height: wp(5),
    // width: wp(5),
    resizeMode: "contain",
    // padding: "4%",
    // marginTop: wp(3)
  },
  // list: {
  //   paddingBottom: ifIphoneX ? hp(4) : 0,
  // },
  optionList: {
    //   flex:1,
    flexDirection: "row",
    padding: "3%",
    backgroundColor: colors.whiteTwo,
    paddingVertical: hp(2.6),
  },
  commonContainerStyle: {
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: wp(2.2),
    paddingHorizontal: wp(4.5),
    borderWidth: 1,
    borderColor: colors.primary_color,
    borderRadius: 5,
    backgroundColor: colors.whiteTwo,
  },
  optionSelected: {
    backgroundColor: colors.primary_color,
    borderWidth: 0.2,
  },
  optionSelectedText: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.white,
  },
  optionText: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : 0,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  boldText: {
    fontWeight: "500",
  },
  // loaderContainer: {
  //   flex: 1,
  //   position: "absolute",
  //   // top: hp(10),
  //   top: 0,
  //   bottom: 0,
  //   left: 0,
  //   right: 0,
  //   justifyContent: "center",
  //   alignItems: "center"
  // },
  // infoContainer: { flex: 1, backgroundColor: colors.white, padding: wp(3) },
  // infoRectangle: {
  //   width: wp(25),
  //   height: hp(0.7),
  //   backgroundColor: colors.whiteTwo,
  //   margin: wp(1),
  //   borderRadius: 8
  // },
  // gestureArea: { alignItems: "center", marginBottom: hp(0.5) },
  // infoTitle: {
  //   fontSize: RF(2.6),
  //   fontWeight: "500",
  //   color: colors.greyishBrown,
  //   marginBottom: hp(1.6),
  //   marginTop: hp(2),
  //   paddingHorizontal: wp(4)
  // },
  // infoDetails: {
  //   opacity: 0.85,
  //   fontSize: RF(2),
  //   color: colors.black,
  //   marginBottom: hp(2),
  //   paddingHorizontal: wp(4)
  //   // textAlign:'justify'
  // }
});
