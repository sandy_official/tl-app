import React from "react";
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { SCREENER_CATEGORIES, RECENT_SCREENERS } from "../../utils/ScreenerConstants";
import { getObjectListFromAsync } from "../../controllers/AsyncController";

export default class ScreenerCategoriesScreen extends React.PureComponent {
  componentDidMount() {
    this.setNavigationListener();
  }

  componentWillUnmount() {
    this.navListener.remove();
  }

  constructor(props) {
    super(props);
    this.state = {
      recentScreeners: [],
    };
  }

  setNavigationListener = () => {
    this.navListener = this.props.navigation.addListener("willFocus", () => {
      getObjectListFromAsync(RECENT_SCREENERS, (screenerList) => {
        screenerList != null &&
          this.setState({
            recentScreeners: screenerList,
          });
      });
    });
  };

  renderToolbar = () => {
    const { navigate } = this.props.navigation;

    return (
      <Toolbar title="Screeners" drawerMenu {...this.props}>
        <ImageButton
          source={require("../../assets/icons/search-white.png")}
          onPress={() => navigate("ScreenerSearch")}
        />
      </Toolbar>
    );
  };

  renderRecentListItem = (item, index) => {
    const { navigate } = this.props.navigation;

    return (
      <TouchableOpacity onPress={() => navigate("ScreenerStocks", item)}>
        <View style={styles.recentItemContainer}>
          <Text style={styles.recentItem} numberOfLines={2} ellipsizeMode="tail">
            {item.screener}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderRecentlyViewed = () => {
    const { recentScreeners } = this.state;

    return (
      <View style={styles.recentListContainer}>
        <Text style={styles.recentListLabel}>Viewed Recently</Text>

        <FlatList
          contentContainerStyle={styles.recentList}
          horizontal={true}
          data={recentScreeners}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => this.renderRecentListItem(item, index)}
          keyExtractor={(item, index) => index.toString()}
        />
      </View>
    );
  };

  renderScreenerItem = ({ item, index }) => {
    const { navigate } = this.props.navigation;
    return (
      <TouchableOpacity
        key={index.toString()}
        onPress={() => navigate("ScreenerList", { title: item.name, info: item.description })}
      >
        <View style={styles.screenerItem}>
          <View style={styles.screenerIconContainer}>
            <Image source={item.icon} style={styles.screenerIcon} />
          </View>

          <View style={styles.screenerInfo}>
            <Text style={styles.screenerName}>{item.name}</Text>
            <Text
              style={styles.screenerDetail}
              // numberOfLines={3}
              ellipsizeMode="tail"
            >
              {item.description}
            </Text>
          </View>

          <Image source={require("../../assets/icons/arrow-right-blue.png")} style={styles.screenerArrowIcon} />
        </View>
      </TouchableOpacity>
    );
  };

  renderScreeners = () => {
    return (
      <FlatList
        contentContainerStyle={styles.screenerList}
        data={SCREENER_CATEGORIES}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderScreenerItem}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        removeClippedSubviews
        initialNumToRender={8}
        maxToRenderPerBatch={5}
      />
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <ScrollView showsVerticalScrollIndicator={false}>
          {this.state.recentScreeners.length > 0 && this.renderRecentlyViewed()}
          {this.renderScreeners()}
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  screenerItem: {
    flex: 1,
    flexDirection: "row",
    minHeight: hp(11),
    backgroundColor: colors.white,
    // marginBottom: 0.3,
    paddingVertical: wp(4),
  },
  screenerIconContainer: {
    width: wp(18),
    paddingTop: wp(1),
    alignItems: "center",
  },
  screenerIcon: {
    width: wp(7),
    height: wp(8),
    resizeMode: "contain",
  },
  screenerInfo: {
    flex: 1,
    paddingRight: "3%",
  },
  screenerName: {
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.primary_color,
    marginBottom: 5.5,
  },
  screenerDetail: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    opacity: 0.85,
  },
  screenerArrowIcon: {
    height: wp(4.5),
    width: wp(4.5),
    resizeMode: "contain",
    marginTop: 3,
    marginRight: "4%",
  },
  recentItemContainer: {
    width: wp(36),
    height: wp(18),
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(2.5),
    // paddingVertical: wp(2.5),
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
    borderRadius: 5,
  },
  recentItem: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.white,
  },
  recentList: {
    padding: wp(3),
  },
  recentListLabel: {
    fontSize: RF(1.6),
    color: colors.warmGrey,
    marginLeft: wp(3),
    fontWeight: "500",
  },
  recentListContainer: {
    paddingTop: wp(4),
    marginBottom: hp(1.8),
    backgroundColor: colors.white,
  },
  extraSpacing: {
    flex: 1,
    height: ifIphoneX ? hp(4) : 0,
    backgroundColor: colors.white,
  },
  screenerList: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    backgroundColor: colors.white,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
});
