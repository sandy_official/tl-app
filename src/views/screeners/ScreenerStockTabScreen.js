import React from "react";
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { globalStyles, colors } from "../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import RF from "react-native-responsive-fontsize";
import ScreenerStockListScreen from "./ScrennerStockListScreen";
import ScreenerFilters from "../../components/screeners/ScreenerFilters";
import ScreenerAlerts from "../../components/screeners/ScreenerAlerts";
import {
  getScreenerBookmarkStatus,
  addRemoveScreenerBookmark,
  getScreenerAlerts,
} from "../../controllers/ScreenerController";
import LoadingView from "../../components/common/LoadingView";
import SuperstarAlertNonSub from "../../components/superstar/SuperstarAlertNonSub";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import LoginModal from "../../components/common/LoginModal";
import TabComponent from "../../components/common/TabComponent";
import BaseComponent from "../../components/common/BaseComponent";

class ScreenerStockTabScreen extends React.PureComponent {
  tabs = ["All", "Nifty 50", "BSE 500", "My Stocks"];

  componentDidMount() {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    const screenerId = this.props.navigation.getParam("screenId");
    const screenerName = this.props.navigation.getParam("screener");
    const bookmarkId = this.props.navigation.getParam("bookmarkId");
    const isBookmarkable = this.props.navigation.getParam("isBookmarkable");

    // setTimeout(() => {
    //   this.refs.allStocks && this.refs.allStocks.getData();
    // }, 500);

    this.setState(
      {
        screenerId: screenerId,
        screenerName: screenerName,
        bookmarkId: bookmarkId,
        isBookmarkable: isBookmarkable,
      },
      () => this.checkBookmarkStatus()
    );

    setTimeout(() => {
      getScreenerAlerts(baseURL, appToken, token, screenerId, (data) => {
        if (Object.keys(data).length > 0)
          this.setState({
            alertData: data.alertsData,
            allowedFreq: data.allowedFreq,
            maxAllowedFreq: data.maxAllowedFreq,
          });
      });
    }, 500);
  }

  setBookmarkId = (screen) => {
    this.setState(
      {
        bookmarkId: screen.bookmarkId,
        isBookmarkable: screen.isBookmarkable,
      },
      () => this.checkBookmarkStatus()
    );
  };
  // componentWillUpdate() {
  //   if (this.refs.allStocks && !this.state.isApiCalled[0]) {
  //     this.setState({ isApiCalled: [true, false, false, false] });
  //     this.refs.allStocks.getData();
  //   }
  // }

  constructor(props) {
    super(props);
    this.state = {
      screenerId: null,
      screenerName: "",
      showFilters: false,
      filterId: 0,
      filterName: "",
      watchlistStocks: [],
      selectedTab: 2,
      bookmarkId: "",
      isBookmarkable: false,
      isBookmarked: false,
      refreshing: false,
      isApiCalled: [false, false, false, false],
      alertData: {},
      maxAllowedFreq: null,
      allowedFreq: [],
      baseRef: null,
    };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [false, false, false, false] }, () => this.setTabSelection({ position: selectedTab }));
  };

  showAlertsDialog = () => {
    const { isSubscriber, isGuestUser } = this.props.userState;

    if (isGuestUser) this.refs.loginModal.open();
    else if (isSubscriber) this.refs.ScreenerAlerts.RBSheet.open();
    else this.refs.alertNonSubBSheet.RBSheet.open();
  };

  showSubscribe = () => {
    this.refs.alertNonSubBSheet.RBSheet.open();
  };

  checkBookmarkStatus = () => {
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;
    const { isBookmarkable, bookmarkId } = this.state;

    if (!isGuestUser) {
      setTimeout(() => {
        isBookmarkable &&
          getScreenerBookmarkStatus(baseURL, appToken, token, bookmarkId, (data) => {
            this.setState({ isBookmarked: data[0].bookmarkStatus });
          }),
          500;
      });
    }
  };

  addRemoveBookmark = () => {
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;
    const { isBookmarked, bookmarkId } = this.state;
    const action = isBookmarked ? "remove" : "add";

    if (!isGuestUser) {
      this.setState({ refreshing: true });

      addRemoveScreenerBookmark(baseURL, appToken, token, action, bookmarkId, (res) => {
        this.setState({ refreshing: false });

        if (action == "add" && res) {
          this.setState({ isBookmarked: true });
          this.refs.toast.show("Bookmark Added");
        } else if (res) {
          this.setState({ isBookmarked: false });
          this.refs.toast.show("Bookmark Removed");
        } else this.refs.toast.show("Please try again");
      });
    } else this.refs.loginModal.open();
  };

  renderCustomToolbarTitle = () => {
    const toolbarLabel = this.props.navigation.getParam("screener");

    return (
      <TouchableOpacity>
        <Text style={styles.toolbarTitle} numberOfLines={2} ellipsizeMode="tail">
          {toolbarLabel}
        </Text>
      </TouchableOpacity>
    );
  };

  renderToolbar = () => {
    const { isBookmarked, alertData, isBookmarkable } = this.state;
    const bookmarkIcon = isBookmarked
      ? require("../../assets/icons/bookmark-filled-white.png")
      : require("../../assets/icons/bookmark-white.png");

    const alertIcon =
      alertData.currentFreq && alertData.currentFreq
        ? require("../../assets/icons/alert-added-white.png")
        : require("../../assets/icons/alert-add-white.png");

    const filterIcon = require("../../assets/icons/filter-white.png");

    return (
      <Toolbar backButton {...this.props} customTitle={this.renderCustomToolbarTitle()}>
        <ImageButton source={alertIcon} onPress={this.showAlertsDialog} />

        {isBookmarkable && <ImageButton source={bookmarkIcon} onPress={this.addRemoveBookmark} />}
        <ImageButton source={filterIcon} onPress={() => this.toggleFilter()} />
      </Toolbar>
    );
  };

  toggleFilter = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  setFilter = (id, name) => {
    let apiCalled = [false, false, false, false];
    apiCalled[this.state.selectedTab] = true;

    this.setState({ isApiCalled: apiCalled });
    this.setState({ filterId: id, filterName: name, showFilters: false });
  };

  setTabSelection = (obj) => {
    const tabPosition = obj.position;
    this.setState({ selectedTab: tabPosition });
    console.log("tabs", obj.position);

    if (!this.state.isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          this.refs.allStocks && this.refs.allStocks.getData();
          break;
        case 1:
          this.refs.nifty50 && this.refs.nifty50.getData();
          break;
        case 2:
          this.refs.bse500 && this.refs.bse500.getData();
          break;
        case 3:
          this.refs.myStocks && this.refs.myStocks.getData();
          break;
      }

      let apiCalls = this.state.isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls });
    }
  };
  getAnalyticsParams = () => {
    return {};
  };
  renderTabs = () => {
    const { selectedTab, baseRef } = this.state;
    const pk = this.props.navigation.getParam("screenId");
    // console.log(pk);

    return (
      <TabComponent
        tabs={this.tabs}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
        analyticsParams={this.getAnalyticsParams}
      >
        <View onLayout={(event) => console.log(event.nativeEvent.layout)}>
          <ScreenerStockListScreen
            {...this.props}
            ref="allStocks"
            pk={pk}
            groupType=""
            groupName=""
            filterId={this.state.filterId}
            watchlistStocks={this.state.watchlistStocks}
            isSelected={selectedTab == 0}
            setBookmarkId={this.setBookmarkId}
            showSubscribe={this.showSubscribe}
            baseRef={baseRef}
          />
        </View>
        <View>
          <ScreenerStockListScreen
            {...this.props}
            ref="nifty50"
            pk={pk}
            groupType="index"
            groupName="NIFTY50"
            filterId={this.state.filterId}
            watchlistStocks={this.state.watchlistStocks}
            isSelected={selectedTab == 1}
            showSubscribe={this.showSubscribe}
            baseRef={baseRef}
          />
        </View>
        <View>
          <ScreenerStockListScreen
            {...this.props}
            ref="bse500"
            pk={pk}
            groupType="index"
            groupName="BSE500"
            filterId={this.state.filterId}
            watchlistStocks={this.state.watchlistStocks}
            isSelected={selectedTab == 2}
            showSubscribe={this.showSubscribe}
            baseRef={baseRef}
          />
        </View>
        <View>
          <ScreenerStockListScreen
            {...this.props}
            ref="myStocks"
            pk={pk}
            groupType="user"
            groupName="all"
            filterId={this.state.filterId}
            watchlistStocks={this.state.watchlistStocks}
            isSelected={selectedTab == 3}
            showSubscribe={this.showSubscribe}
            baseRef={baseRef}
          />
        </View>
      </TabComponent>
    );
  };

  render() {
    const { screenerName, allowedFreq, alertData, filterName, filterId, showFilters, refreshing } = this.state;
    const { isSubscriber } = this.props.userState;

    return (
      <BaseComponent ref={(ref) => this.setState({ baseRef: ref })} refreshScreen={this.refreshScreen} {...this.props}>
        {/* <View style={styles.container}> */}
        {this.renderToolbar()}

        {this.renderTabs()}

        <ScreenerFilters
          currentFilter={filterName}
          currentFilterId={filterId}
          showFilter={showFilters}
          toggle={() => this.toggleFilter()}
          setFilter={(id, name) => this.setFilter(id, name)}
          isSubscriber={isSubscriber}
        />

        <ScreenerAlerts
          ref="ScreenerAlerts"
          name={screenerName}
          alertData={alertData}
          allowed={allowedFreq}
          onChange={(data) => this.setState({ alertData: data })}
        />

        <SuperstarAlertNonSub
          ref="alertNonSubBSheet"
          {...this.props}
          // buttonState={this.state.alertButtonState}
          // setButtonState={obj => this.setState({ alertButtonState: obj })}
        />

        <Toast
          ref="toast"
          position="bottom"
          // positionValue={hp(28)}
          style={globalStyles.toast}
          textStyle={globalStyles.toastText}
        />

        <LoginModal ref="loginModal" {...this.props} />
        {refreshing && <LoadingView />}
        {/* </View> */}
      </BaseComponent>
    );
  }
}
const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ScreenerStockTabScreen);

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: colors.whiteTwo
  // },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
  toolbarTitle: {
    fontSize: RF(2.6),
    color: colors.white,
    marginRight: wp(4),
  },
});
