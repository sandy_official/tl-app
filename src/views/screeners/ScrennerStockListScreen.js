import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ScrollView, RefreshControl } from "react-native";
import ScreenerStockCard from "../../components/screeners/ScreenerStockCard";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { getScreenerData } from "../../controllers/ScreenerController";
import Toast from "react-native-easy-toast";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { SortOptionView } from "../../components/common/SortOptionView";
import LoadingView from "../../components/common/LoadingView";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import NoMatchView from "../../components/common/NoMatchView";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";
import { fetcher } from "../../controllers/Fetcher";
import { POST_METHOD } from "../../utils/Constants";
import { SCREENER_WATCHLIST_STOCKS_URL, SCREENER_STOCKS_URL } from "../../utils/ApiEndPoints";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class ScreenerStockListScreen extends React.PureComponent {
  clearStockList = false;

  // componentDidMount() {
  //   this.props.isSelected && this.getData();
  // }

  componentWillReceiveProps(nextProps) {
    if (nextProps.filterId !== this.props.filterId) {
      console.warn("calling api for filterId");
      this.getFilterData(nextProps);
    }

    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }

    // else if (nextProps.isSelected && this.state.stocks.length == 0) {
    //   console.warn("calling api for getdate");
    //   this.getData();
    // }
  }

  getFilterData = (nextProps) => {
    this.clearStockList = true;
    // this.filterChanged = false;
    this.setState(
      {
        filterId: nextProps.filterId,
        selectedStock: {},
        refreshing: true,
        pageNumber: 0,
        isNextPage: true,
        isFirstCallComplete: true,
        showScrollToTop: false,
        // stocks: []
      },
      () => nextProps.isSelected && this.getData()
    );
  };

  constructor(props) {
    super(props);
    this.state = {
      selectedSort: null,
      isSortAscending: false,
      isNextPage: true,
      sortOrder: "",
      description: "",
      title: "",
      sortBy: "",
      pageNumber: 0,
      pageCount: 20,
      stocks: [],
      screen: {},
      refreshing: false,
      isFirstCallComplete: false,
      filterId: 0,
      selectedStock: {},
      stockInWatchlist: false,
      isLoadingMore: false,
    };
  }

  // componentWillUpdate(newProps, newState) {
  //   newProps.isSelected && newState.stocks.length == 0 && this.getData();
  // }

  getData = () => {
    // console.log("get data", this.props.isSelected);
    const { pk, groupName, groupType, filterId, userState, baseURL, appToken, setBookmarkId, baseRef } = this.props;
    const { pageNumber, pageCount, sortBy, sortOrder, isNextPage } = this.state;

    const isWatchlist = groupType == "user";

    if (pk && isNextPage) {
      let URL = isWatchlist ? baseURL + SCREENER_WATCHLIST_STOCKS_URL : baseURL + SCREENER_STOCKS_URL;
      let body = {
        screenpk: pk,
        groupType: groupType,
        groupName: groupName,
        perPageCount: pageCount,
        pageNumber: pageNumber,
        filterListId: filterId == 0 ? null : filterId,
        sortBy: sortBy ? sortBy : null,
        order: sortOrder ? sortOrder : null,
      };
      console.warn("req", body);
      fetcher(
        URL,
        POST_METHOD,
        body,
        baseRef,
        appToken,
        userState.token,
        (json) => {
          let data = this.mapScreenerData(json.body);

          if (Object.keys(data).length > 0) {
            const oldStockList = this.state.refreshing ? [] : this.state.stocks;
            const newStockList = this.clearStockList ? data.stocks : oldStockList.concat(data.stocks);
            this.clearStockList = false;

            this.setState({
              isNextPage: data.isNextPage,
              description: data.description,
              title: data.title,
              sortOrder: data.order,
              isSortAscending: data.order != "DESC",
              sortBy: data.sortBy,
              pageNumber: ++data.pageNumber,
              // pageCount: data.thisPageCount,
              stocks: newStockList,
              screen: data.screen,
            });

            this.sortOptionsList && this.sortOptionsList.scrollToIndex({ index: 0 });
            setBookmarkId ? setBookmarkId(data.screen) : null;
            this.stopLoading();
          }
        },
        (error) => {
          this.stopLoading();
        }
      );
    } else this.setState({ refreshing: false, isLoadingMore: false });

    // this.setState({ filterId: filterId });
  };

  mapScreenerData = (json) => {
    let stockList = [];
    let columnList = json.tableHeaders;
    let screenData = json.screen ? json.screen : {};

    // stock details
    json.tableData.forEach((stockIndex) => {
      let stockObj = [];
      let stockArray = [];
      let dvmValueCrossed = false; // to get data which is after dvm in json object

      stockIndex.forEach((item, i) => {
        let column = Object.assign({}, columnList[i]);
        let columnName = column.unique_name ? column.unique_name : column.name;

        if (dvmValueCrossed) {
          column["value"] = item;
          stockArray.push(column);
        }

        if (column.unique_name == "m_color") dvmValueCrossed = true;
        stockObj[columnName] = item;
      });

      stockList.push({ obj: stockObj, array: stockArray });
    });

    return {
      isNextPage: json.isNextPage,
      order: json.order,
      description: json.description,
      title: json.title,
      sortBy: json.sortBy,
      pageNumber: json.thisPageNumber,
      pageCount: json.thisPageCount,
      stocks: stockList,
      screen: screenData,
    };
  };

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;

    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false, isLoadingMore: false });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    const { refreshing, isLoadingMore } = this.state;
    if (!refreshing && !isLoadingMore) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  // doInitialSorting = stocks => {
  //   try {
  //     const sortOptions =
  //       newStockList.length > 0 && "array" in stocks[0]
  //         ? JSON.parse(JSON.stringify(this.state.stocks[0].array))
  //         : [];

  //     this.setSortOrder(sortOptions[0].name, sortOptions[0].unique_name);
  //   } catch {
  //     e => console.log(e);
  //   }
  // };

  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => {
      this.getData();
    });
  };

  renderFooter = () => {
    const { isNextPage, isLoadingMore } = this.state;

    if (isLoadingMore) return <LoadingMoreView />;
    else if (isNextPage) {
      return (
        <TouchableOpacity onPress={this.onLoadMore}>
          <View style={styles.footerButton}>
            <Text style={styles.footerButtonText}>
              Load More
              {/* (126 remaining) */}
            </Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.listEndFooter}>
          <Text style={styles.listEndFooterText}>That's all folks!</Text>
        </View>
      );
    }
  };

  renderScreenerBacktest = () => {
    const { screen } = this.state;
    // console.log(this.state.screen);
    if (
      "backtestReturns" in screen &&
      "backtestDuration" in screen &&
      "backtestStockGroup" in screen &&
      "benchmarkReturns" in screen
    ) {
      const backtestReturns = screen.backtestReturns;
      const backtestDuration = screen.backtestDuration;
      const backtestGroup = screen.backtestStockGroup;
      const benchmarkReturns = screen.benchmarkReturns;

      const background = backtestReturns >= benchmarkReturns ? styles.greenBackground : styles.redBackground;

      return (
        <View style={[styles.performanceView, background]}>
          <Text style={styles.performanceDetail} numberOfLines={1} ellipsizeMode="tail">
            {backtestReturns}% returns over {backtestDuration}
          </Text>
          <Text style={styles.performanceIndex}>{backtestGroup}</Text>
        </View>
      );
    }
  };

  setSortOrder = (label, sortId) => {
    const { isSortAscending, sortBy } = this.state;

    this.clearStockList = true;

    if (sortId === sortBy) {
      const sortOrder = isSortAscending ? "DESC" : "ASC";

      // console.log("sort order", sortId, sortBy, isSortAscending, sortOrder);

      this.setState(
        {
          isSortAscending: !isSortAscending,
          sortOrder: sortOrder,
          refreshing: true,
          pageNumber: 0,
          isNextPage: true,
        },
        () => {
          this.getData();
        }
      );
    } else {
      this.setState(
        {
          selectedSort: label,
          isSortAscending: false,
          sortOrder: "DESC",
          sortBy: sortId,
          refreshing: true,
          pageNumber: 0,
          isNextPage: true,
        },
        () => this.getData()
      );
    }
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  renderSortingOption = (label, key, index) => {
    if (this.state.selectedSort == null && index == 0) {
      this.setState({ selectedSort: label });
    }

    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={this.state.selectedSort}
        ascending={this.state.isSortAscending}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
        key={key}
      />
    );
  };

  isStockEmpty = () => {
    const stocks = this.state.stocks;
    if (stocks.length > 0 && "array" in stocks[0]) return false;
    return true;
  };

  renderSortingOptionsRow = () => {
    const stock = !this.isStockEmpty() ? JSON.parse(JSON.stringify(this.state.stocks[0].array)) : [];

    return (
      <FlatList
        ref={(ref) => (this.sortOptionsList = ref)}
        data={stock}
        extraData={this.state.stocks}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={globalStyles.sortOptionRow}
        renderItem={({ item, index }) => this.renderSortingOption(item.name, item.unique_name, index)}
        keyExtractor={(item, index) => index.toString()}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
      />
    );
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (stock) => {
    const { isGuestUser } = this.props.userState;
    if (!isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderStockItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;

    const stockId = item.obj.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <ScreenerStockCard
        {...this.props}
        stock={item}
        position={index}
        watchlistBtnState={isWatchlistActive}
        showWatchlist={() => this.showWatchlist(item)}
        showAlerts={() => this.showAlerts(item)}
        showPortfolio={() => this.showPortfolio(item)}
      />
    );
  };

  renderListHeader = () => {
    return (
      <View>
        <Text style={styles.sortBy}>Sort By</Text>
        {this.renderSortingOptionsRow()}
      </View>
    );
  };

  renderStockList = () => {
    const { stocks } = this.state;

    return (
      <FlatList
        data={stocks}
        extraData={{ ...this.props, stocks }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
        renderItem={this.renderStockItem}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={this.renderListHeader}
        ListFooterComponent={this.renderFooter}
        removeClippedSubviews
        initialNumToRender={3}
        maxToRenderPerBatch={10}
      />
    );
  };

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected } = this.props;
    const { stocks, screen, selectedStock, refreshing, showScrollToTop } = this.state;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {stocks.length > 0 && (
            <ScrollView
              ref={(ref) => (this.listRef = ref)}
              onScroll={this.handleScroll}
              showsVerticalScrollIndicator={false}
              refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
            >
              {Object.keys(screen).length > 0 && this.renderScreenerBacktest()}
              {this.renderStockList()}
            </ScrollView>
          )}

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockAlertsModal
            ref="stockAlerts"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            addedTo={(name) => this.refs.toast.show("Added to Primary", 1000)}
            // data={this.state.dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          <LoginModal ref="loginModal" {...this.props} />
          {this.state.isFirstCallComplete && stocks.length == 0 && <NoMatchView message="No Results found" />}
          {!this.state.isFirstCallComplete && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(ScreenerStockListScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    // paddingHorizontal: "3%"
  },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
    marginHorizontal: "3%",
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: wp(3),
    color: colors.primary_color,
  },
  sortBy: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
    marginLeft: wp(3),
    marginTop: wp(3),
  },
  performanceView: {
    flexDirection: "row",
    minHeight: hp(3.5),
    alignItems: "center",
    justifyContent: "center",
  },
  performanceDetail: {
    flex: 1,
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
  },
  performanceIndex: {
    fontSize: RF(1.8),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
    textAlign: "right",
  },
  hide: {
    display: "none",
  },
  listEndFooter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(1),
  },
  listEndFooterText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.pinkishGrey,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
});
