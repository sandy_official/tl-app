import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../styles/CommonStyles";
import { connect } from "react-redux";
import { APP_VERSION } from "../utils/Constants";

class ForceUpdateScreen extends React.PureComponent {
  logo = require("../assets/icons/logo.png");
  title = `Please update\nTrendlyne Markets`;
  description =
    "This version of the app is no longer supported. Get back to your market insights by installing the latest version.";

  state = { show: false };

  componentDidMount() {
    setTimeout(() => this.setState({ show: true }), 3500);
  }

  render() {
    const { forceUpdateVersion } = this.props;

    if (APP_VERSION <= forceUpdateVersion && this.state.show) {
      return (
        <View style={styles.container}>
          <Image source={this.logo} style={styles.logo} />
          <Text style={styles.title}>{this.title}</Text>
          <Text style={styles.description}>{this.description}</Text>

          <TouchableOpacity style={styles.button}>
            <Text style={styles.buttonText}>Update</Text>
          </TouchableOpacity>
        </View>
      );
    } else return null;
  }
}

mapStateToProps = state => {
  let firebase = state.app.firebase ? state.app.firebase : {};
  let version = "version" in firebase ? firebase.version : {};
  let forceUpdateVersion = "forceUpdateVersion" in version ? version.forceUpdateVersion : 1;

  return { forceUpdateVersion };
};

export default connect(mapStateToProps)(ForceUpdateScreen);

const styles = StyleSheet.create({
  container: {
    position: "absolute",
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.whiteTwo,
    paddingHorizontal: wp(9),
    paddingBottom: hp(10.5)
  },
  logo: {
    width: wp(20),
    height: wp(25),
    resizeMode: "contain"
  },
  title: {
    fontSize: RF(3.2),
    fontWeight: "500",
    lineHeight: wp(9),
    color: colors.black,
    textAlign: "center",
    marginTop: hp(7)
  },
  description: {
    fontSize: RF(2.35),
    textAlign: "center",
    color: colors.black,
    lineHeight: wp(6.5),
    marginTop: hp(3)
  },
  button: {
    position: "absolute",
    bottom: hp(4),
    left: wp(4),
    right: wp(4),
    borderRadius: 5,
    backgroundColor: colors.cobalt
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    margin: wp(2.5),
    textAlign: "center"
  }
});
