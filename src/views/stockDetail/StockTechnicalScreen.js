import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import PriceChangeCardList from "../../components/stockDetail/technical/PriceChangeCardList";
import MomentumList from "../../components/stockDetail/technical/MomentumList";
import VolumeAverageList from "../../components/stockDetail/technical/VolumeAverageList";
import ActiveCandlesticks from "../../components/stockDetail/technical/ActiveCandlesticks";
import { getStockTechnical } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";
import { colors } from "../../styles/CommonStyles";

class StockTechnicalScreen extends React.PureComponent {
  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      candlesticks: { bullish: [], bearish: [] },
      momentum: {},
      movingAverages: {},
      priceAnalysis: {},
      stock: {},
      technical: [],
      volume: {},
    };
  }

  getData = () => {
    const { stockId, userState, baseURL, appToken } = this.props;

    this.setState({ refreshing: true });
    getStockTechnical(baseURL, appToken, userState.token, stockId, (data) =>
      this.setState(Object.assign(data, { refreshing: false }))
    );
  };

  render() {
    const { isSelected, pageType } = this.props;
    const { refreshing, candlesticks, momentum, movingAverages, priceAnalysis, stock, technical, volume } = this.state;

    const showPriceChange = Object.keys(priceAnalysis).length > 0;
    const showVolumeAverageList =
      (Object.keys(movingAverages).length > 0 || Object.keys(volume).length > 0) && pageType != "index";
    const showMomentumList = Object.keys(momentum).length > 0 || technical.length > 0;

    if (isSelected) {
      return (
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {showPriceChange && <PriceChangeCardList data={priceAnalysis} stock={stock} />}
            {showMomentumList && <MomentumList momentum={momentum} technical={technical} stock={stock} />}
            {showVolumeAverageList && <VolumeAverageList data={[volume, movingAverages]} />}
            {showPriceChange && <ActiveCandlesticks data={candlesticks} />}
            <View style={styles.extraSpace} />
          </ScrollView>

          {refreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockTechnicalScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  extraSpace: {
    marginBottom: ifIphoneX ? wp(3) : 0,
  },
});
