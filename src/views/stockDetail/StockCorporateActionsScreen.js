import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import DividendHistoryList from "../../components/stockDetail/corporateActions/DividendHistoryList";
import BonusHistoryList from "../../components/stockDetail/corporateActions/BonusHistoryList";
import BoardMeetingHistoryList from "../../components/stockDetail/corporateActions/BoardMeetingHistoryList";
import { getCorporateActions } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class StockCorporateActionsScreen extends React.PureComponent {
  state = {
    selectedOption: "Dividend",
    stock: {},
    meetings: [],
    dividend: { table: [], insight: [] },
    bonus: { table: [], insight: [] },
    split: { table: [], insight: [] },
    refreshing: false,
    isFirstCallComplete: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  loadData = () => {
    this.getData("Dividend", null);
  };

  getData = (identifier, action) => {
    const { stockId, userState, baseURL, appToken } = this.props;
    this.setState({ refreshing: true });

    getCorporateActions(baseURL, appToken, userState.token, stockId, identifier, action, (data) => {
      switch (identifier) {
        case "Dividend":
          this.setState({ stock: data.stock, dividend: data });
          break;

        case "Bonus":
          this.setState({ stock: data.stock, bonus: data });
          break;

        case "Split":
          this.setState({ stock: data.stock, split: data });
          break;

        case "Board Meetings":
          this.setState({ stocks: data.stock, meetings: data.table });
          break;
      }

      this.setState({ refreshing: false });

      if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    });
  };

  setOptionSelection = (option) => {
    this.setState({ selectedOption: option });

    let id = "Dividend";
    let action = null;

    switch (option) {
      case "Dividend":
        id = "Dividend";
        action = null;
        break;
      case "Bonus":
        id = "Bonus";
        action = "bonus";
        break;
      case "Split":
        id = "Split";
        action = "split";
        break;
      case "Board Meetings":
        id = "Board Meetings";
        action = null;
        break;
    }

    this.getData(id, action);
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderOptions = (option) => {
    const optionStyle = this.state.selectedOption == option ? styles.optionSelected : styles.option;

    const optionTextStyle = this.state.selectedOption == option ? styles.optionSelectedText : styles.optionText;

    return (
      <TouchableOpacity onPress={() => this.setOptionSelection(option)}>
        <View style={optionStyle}>
          <Text style={optionTextStyle}>{option}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderOptionsRow = () => {
    return (
      <View style={styles.optionRow}>
        {this.renderOptions("Dividend")}
        {this.renderOptions("Bonus")}
        {this.renderOptions("Split")}
        {this.renderOptions("Board Meetings")}
      </View>
    );
  };

  renderList = () => {
    switch (this.state.selectedOption) {
      case "Dividend":
        return <DividendHistoryList data={this.state.dividend} />;
      case "Bonus":
        return <BonusHistoryList data={this.state.bonus} type="BONUS" />;
      case "Split":
        return <BonusHistoryList data={this.state.split} type="SPLIT" />;
      case "Board Meetings":
        return <BoardMeetingHistoryList data={this.state.meetings} />;
    }
  };

  // getListTitle = () => {
  //   switch (this.state.selectedOption) {
  //     case "Board Meetings":
  //       return <Text style={styles.heading}>BOARD MEETINGS HISTORY</Text>;
  //     default:
  //       return null;
  //   }
  // };

  render() {
    const { isSelected } = this.props;
    const { refreshing, isFirstCallComplete, selectedOption, showScrollToTop } = this.state;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {Object.keys(this.state.stock).length > 0 && (
            <ScrollView
              ref={(ref) => (this.listRef = ref)}
              showsVerticalScrollIndicator={false}
              onScroll={this.handleScroll}
              refreshControl={
                <RefreshControl
                  refreshing={refreshing}
                  onRefresh={() => this.setState({ refreshing: true }, () => this.setOptionSelection(selectedOption))}
                />
              }
            >
              {this.renderOptionsRow()}
              {/* {this.getListTitle()} */}
              {this.renderList()}
            </ScrollView>
          )}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />

          {!isFirstCallComplete && <LoadingView />}
        </View>
      );
    }
    return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockCorporateActionsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  optionRow: {
    flexDirection: "row",
    marginTop: hp(3),
    marginHorizontal: wp(3),
  },
  option: {
    marginRight: wp(2),
    paddingVertical: wp(2),
    paddingHorizontal: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  optionSelected: {
    marginRight: wp(2),
    paddingVertical: wp(2),
    paddingHorizontal: wp(3),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  optionText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
  },
  optionSelectedText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.white,
  },
  insight: {
    fontSize: RF(2.4),
    fontWeight: "300",
    padding: wp(3),
    // marginBottom: 1,
    backgroundColor: colors.white,
  },
  listRow: {
    padding: wp(3),
    backgroundColor: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.whiteTwo,
  },
  column: {
    fontWeight: "500",
  },
  listItem: {
    flex: 1,
    fontSize: RF(2),
  },
});
