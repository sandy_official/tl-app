import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import { getStockNews } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import StockNewsItem from "../../components/stockDetail/news/StockNewsItem";
import StockNewsReportItem from "../../components/stockDetail/news/StockNewsReportItem";
import StockNewsCorporateItem from "../../components/stockDetail/news/StockNewsCorporateItem";
import { connect } from "react-redux";
import LoginModal from "../../components/common/LoginModal";
import NoMatchView from "../../components/common/NoMatchView";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class StockNewsScreen extends React.PureComponent {
  state = {
    selectedOption: "News",
    news: [],
    reports: [],
    corporate: [],
    newsPageNumber: 1,
    reportsPageNumber: 1,
    corporatePageNumber: 1,
    newsNextPage: true,
    reportsNextPage: true,
    corporateNextPage: true,
    refreshing: false,
    isFirstCallComplete: false,
    newsQsTime: "",
    reportsQsTime: "",
    corporateQsTime: "",
    isFirstCallComplete: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    // console.log("Stock News screen ");

    const {
      isFirstCallComplete,
      selectedOption,
      news,
      newsPageNumber,
      newsNextPage,
      reports,
      reportsPageNumber,
      reportsNextPage,
      corporate,
      corporatePageNumber,
      corporateNextPage,
      refreshing,
      newsQsTime,
      reportsQsTime,
      corporateQsTime,
    } = this.state;

    const { stockId, userState, baseURL, appToken, pageType, stockCode } = this.props;
    let pageNumber = 1;
    let isNextPage = true;
    let qsTime = "";

    if (selectedOption == "News") {
      pageNumber = newsPageNumber;
      isNextPage = newsNextPage;
      qsTime = newsQsTime;
    } else if (selectedOption == "Reports") {
      pageNumber = reportsPageNumber;
      isNextPage = reportsNextPage;
      qsTime = reportsQsTime;
    } else {
      pageNumber = corporatePageNumber;
      isNextPage = corporateNextPage;
      qsTime = corporateQsTime;
    }

    if (isNextPage) {
      getStockNews(
        baseURL,
        appToken,
        userState.token,
        stockId,
        pageNumber,
        20,
        selectedOption,
        qsTime,
        pageType,
        stockCode,
        (data) => {
          console.log("item====&&&&&&&&", data);

          if (Object.keys(data).length > 0) {
            switch (selectedOption) {
              case "News":
                this.setState({
                  news: refreshing ? data.news : news.concat(data.news),
                  newsQsTime: data.qsTime,
                  newsNextPage: data.isNextPage,
                  newsPageNumber: ++data.pageNumber,
                });
                break;

              case "Reports":
                this.setState({
                  reports: refreshing ? data.news : [...reports, ...data.news],
                  reportsQsTime: data.qsTime,
                  reportsNextPage: data.isNextPage,
                  reportsPageNumber: ++data.pageNumber,
                });
                break;

              default:
                this.setState({
                  corporate: refreshing ? data.news : corporate.concat(data.news),
                  corporateQsTime: data.qsTime,
                  corporateNextPage: data.isNextPage,
                  corporatePageNumber: ++data.pageNumber,
                });
            }
          }

          this.setState({ refreshing: false });
          if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        }
      );
    }
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  setOptionSelection = (option) => {
    const { news, reports, corporate } = this.state;
    this.setState({ selectedOption: option }, () => {
      if (option == "News") {
        if (news.length == 0) this.getData();
      } else if (option == "Reports") {
        if (reports.length == 0) this.getData();
      } else {
        if (corporate.length == 0) this.getData();
      }
    });
  };

  renderOptions = (option) => {
    const optionStyle = this.state.selectedOption == option ? styles.optionSelected : styles.option;
    const optionTextStyle = this.state.selectedOption == option ? styles.optionSelectedText : styles.optionText;

    return (
      <TouchableOpacity onPress={() => this.setOptionSelection(option)}>
        <View style={optionStyle}>
          <Text style={optionTextStyle}>{option}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderOptionsRow = () => {
    const { pageType } = this.props;

    if (pageType != "index")
      return (
        <View style={styles.optionRow}>
          {this.renderOptions("News")}
          {this.renderOptions("Reports")}
          {this.renderOptions("Corporate Announcements")}
        </View>
      );
  };

  refreshData = () => {
    const { selectedOption } = this.state;

    this.setState({ refreshing: true });

    if (selectedOption == "News") {
      this.setState({ newsPageNumber: 0, newsNextPage: true, refreshing: true }, () => this.getData());
    } else if (selectedOption == "Reports") {
      this.setState({ reportsPageNumber: 0, reportsNextPage: true, refreshing: true }, () => this.getData());
    } else {
      this.setState({ corporatePageNumber: 0, corporateNextPage: true, refreshing: true }, () => this.getData());
    }
  };

  renderRefreshControl = () => {
    const { refreshing } = this.state;
    return <RefreshControl refreshing={refreshing} onRefresh={() => this.refreshData()} />;
  };

  renderNewsItem = ({ item, index }) => {
    const { userState } = this.props;

    return (
      <StockNewsItem
        {...this.props}
        data={item}
        isGuestUser={userState.isGuestUser}
        showLogin={() => this.refs.loginModal.open()}
      />
    );
  };

  renderNewsList = () => {
    const { news } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={news}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderNewsItem}
        ListHeaderComponent={this.renderOptionsRow()}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        refreshControl={this.renderRefreshControl()}
        onEndReached={this.getData}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
        onScroll={this.handleScroll}
      />
    );
  };

  showLogin = () => this.refs.loginModal.open();

  renderNewsReportItem = ({ item, index }) => {
    const { isGuestUser } = this.props.userState;
    return <StockNewsReportItem {...this.props} data={item} isGuestUser={isGuestUser} showLogin={this.showLogin} />;
  };

  renderReportList = () => {
    const { reports } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={reports}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderNewsReportItem}
        ListHeaderComponent={this.renderOptionsRow()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        refreshControl={this.renderRefreshControl()}
        onEndReached={this.getData}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
        onScroll={this.handleScroll}
      />
    );
  };

  renderAnnouncementList = () => {
    const { corporate } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={corporate}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => <StockNewsCorporateItem {...this.props} data={item} />}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        refreshControl={this.renderRefreshControl()}
        onEndReached={this.getData}
        ListHeaderComponent={this.renderOptionsRow()}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
        onScroll={this.handleScroll}
      />
    );
  };

  renderList = () => {
    switch (this.state.selectedOption) {
      case "News":
        return this.renderNewsList();
      case "Reports":
        return this.renderReportList();
      case "Corporate Announcements":
        return this.renderAnnouncementList();
    }
  };

  render() {
    const { isSelected, pageType } = this.props;
    const { isFirstCallComplete, refreshing, news, showScrollToTop } = this.state;

    let showEmptyState = isFirstCallComplete && pageType == "index" && Array.isArray(news) && news.length == 0;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {/* {this.renderOptionsRow()} */}
          {this.renderList()}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />

          <LoginModal ref="loginModal" {...this.props} />
          {!isFirstCallComplete && <LoadingView />}
          {showEmptyState && <NoMatchView heading={"No News Found"} />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockNewsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
    backgroundColor: colors.white,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  optionRow: {
    flexDirection: "row",
    marginTop: hp(3),
    marginHorizontal: wp(3),
    marginBottom: wp(3),
  },
  option: {
    marginRight: wp(2),
    paddingVertical: wp(2),
    paddingHorizontal: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  optionSelected: {
    marginRight: wp(2),
    paddingVertical: wp(2) + 1,
    paddingHorizontal: wp(3) + 1,
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  optionText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "200",
    color: colors.primary_color,
  },
  optionSelectedText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "200",
    color: colors.white,
  },
  insight: {
    fontSize: RF(2.4),
    fontWeight: "300",
    padding: wp(3),
    // marginBottom: 1,
    backgroundColor: colors.white,
  },
  listRow: {
    padding: wp(3),
    backgroundColor: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.whiteTwo,
  },
  column: {
    fontWeight: "500",
  },
  listItem: {
    flex: 1,
    fontSize: RF(2),
  },
  divider: {
    flex: 1,
    // width:2,
    height: 1,
    opacity: 0.2,
    marginHorizontal: wp(0),
    backgroundColor: colors.greyish,
  },
});
