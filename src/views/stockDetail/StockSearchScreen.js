import React from "react";
import { View, Text, StyleSheet, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import ReportStockItem from "../../components/researchReport/ReportStockItem";
import { Toolbar } from "../../components/Toolbar";
import LoadingView from "../../components/common/LoadingView";
import { searchStockByTerm } from "../../controllers/StocksController";
import { getObjectListFromAsync, storeObjectListInAsync } from "../../controllers/AsyncController";
import { RECENT_STOCK_SEARCH } from "../../utils/Constants";
import { connect } from "react-redux";
import NoMatchView from "../../components/common/NoMatchView";

class StockSearchScreen extends React.PureComponent {
  componentDidMount() {
    // this.getPopularStocks();

    getObjectListFromAsync(RECENT_STOCK_SEARCH, (data) => {
      this.setState({ recentSearch: data ? data : [] });
    });
  }

  constructor(props) {
    super(props);
    this.state = {
      searchQuery: "",
      navigation: props.navigation,
      stocks: [],
      popularStocks: [],
      isFirstCallComplete: false,
      refreshing: false,
      recentSearch: [],
    };
  }

  isDuplicateObject = (stock) => {
    let recentSearches = this.state.recentSearch.length > 0 ? this.state.recentSearch : [];
    let stockList = [];

    Object.keys(recentSearches).map((key) => {
      let stockObject = recentSearches[key];
      let stockId = stockObject.stock_id;

      stockList.push(stockId);
    });

    let isDuplicate = stockList.includes(stock.stock_id);
    this.setState({ recentSearch: isDuplicate ? recentSearches : [...recentSearches, stock] });

    return isDuplicate;
  };

  // getPopularStocks = () => {
  //   this.setState({ refreshing: true });

  //   getResearchReportsByType("new", 0, data => {
  //     console.log("reports", data.reports);
  //     this.setState({
  //       popularStocks: data.reports,
  //       refreshing: false
  //     });

  //     if (!this.state.isFirstCallComplete)
  //       this.setState({ isFirstCallComplete: true });
  //   });
  // };

  searchStock = (query) => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { selectedTab } = this.state;
    console.log(this.state.stocks);

    if (query.length > 1) {
      this.setState({ refreshing: true });

      searchStockByTerm(baseURL, appToken, token, query, "stock", (StockList) =>
        this.setState({ stocks: StockList, refreshing: false })
      );
    } else this.setState({ stocks: [] });
  };

  startSearch = (query) => {
    this.setState({ searchQuery: query });
    this.searchStock(query);
  };

  saveSearch = (stock) => {
    if (!this.isDuplicateObject(stock)) {
      storeObjectListInAsync(RECENT_STOCK_SEARCH, stock);
    }
  };

  renderToolbar = () => {
    const { navigation } = this.props;

    return (
      <Toolbar
        title="Research Reports"
        // backButton
        {...this.props}
        showSearch
        hideSearch={() => navigation.pop()}
        searchPlaceholder="Search stock"
        onChangeText={this.startSearch}
      />
    );
  };

  renderListHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Recent searches</Text>
      </View>
    );
  };

  renderStockList = () => {
    const { stocks, refreshing, searchQuery, recentSearch } = this.state;
    const isSearching = searchQuery.length > 0;
    const stockList = isSearching ? stocks : recentSearch.slice(0, 5);

    // get_full_name;
    return (
      <View style={{ flex: 1 }}>
        {searchQuery.length > 1 && stocks.length == 0 && !refreshing && this.renderNoResultFound()}

        <FlatList
          contentContainerStyle={styles.list}
          data={stockList}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => <ReportStockItem {...this.props} data={item} saveSearch={this.saveSearch} />}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={() => !isSearching && this.renderListHeader()}
          keyboardShouldPersistTaps="always"
        />
        {refreshing && <LoadingView />}
      </View>
    );
  };

  renderNoResultFound = () => {
    return <NoMatchView message="No Results Found" />;
    // return <Text style={styles.noResultFound}>No Results Found</Text>;
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        {this.renderStockList()}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(StockSearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : h(1),
  },
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    backgroundColor: colors.whiteTwo,
  },
  headerText: {
    opacity: 0.85,
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    alignItems: "center",
    justifyContent: "center",
  },
  listItemContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(3),
  },
  listItem: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  extraTopMargin: {
    marginTop: wp(2),
  },
  noResultFound: {
    opacity: 0.85,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
    paddingHorizontal: wp(3),
    paddingVertical: wp(5),
  },
});
