import React from "react";
import { View, StyleSheet, FlatList, Text } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { getStockReports } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import StockReportCard from "../../components/markets/stocks/StockReportCard";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import Toast from "react-native-easy-toast";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import { globalStyles, colors } from "../../styles/CommonStyles";
import LoginModal from "../../components/common/LoginModal";
import RF from "react-native-responsive-fontsize";
import { roundNumber } from "../../utils/Utils";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class StockReportScreen extends React.PureComponent {
  state = {
    showSearch: false,
    reports: [],
    stock: {},
    insights: {},
    isRefreshing: false,
    isFirstCallComplete: false,
    brokerName: "",
    pageNumber: 0,
    perPageCount: 20,
    watchlistStocks: {},
    isNextPage: true,
    selectedReport: {},
    showScrollToTop: false,
  };

  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    console.log("Stock Reports screen ");

    const { stockId, userState, baseURL, appToken } = this.props;
    const { pageNumber, perPageCount } = this.state;
    this.setState({ isRefreshing: true });

    getStockReports(baseURL, appToken, userState.token, stockId, pageNumber, perPageCount, (data) => {
      this.setState({ isRefreshing: false });
      this.setState({
        reports: [...this.state.reports, ...data.reports],
        stock: data.stock,
        insights: data.insights,
        isNextPage: data.isNextPage,
        pageNumber: data.pageNumber,
        isRefreshing: false,
      });
    });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;

    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  showAlerts = (report) => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.stockAlerts.RBSheet.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = (report) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  showWatchlist = (report) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  renderListHeader = () => {
    const { insights } = this.state;

    if (Object.keys(insights).length > 0) {
      // let brokerAvgRating = insights.brokerAvgRating ? insights.brokerAvgRating : {};
      let brokerAvgTarget = insights.brokerAvgTarget ? insights.brokerAvgTarget : {};
      let numReportsLastYear = insights.numReportsLastYear ? insights.numReportsLastYear : {};
      let upgrades = insights.upgrades ? insights.upgrades : {};
      // let downgrades = insights.downgrades ? insights.downgrades : {};

      let reportCount = numReportsLastYear.value ? numReportsLastYear.value : 0;
      let brokerTarget = !isNaN(brokerAvgTarget.value) ? roundNumber(brokerAvgTarget.value) : 0;
      let upside = !isNaN(upgrades.value) ? roundNumber(upgrades.value) : 0;

      return (
        <View>
          <Text style={styles.summary}>SUMMARY</Text>
          <View style={styles.summaryContainer}>
            <Text style={[styles.insight]}>
              Total no. of reports are <Text style={[styles.boldText]}>{reportCount}</Text> which have an average broker
              target is
              <Text style={[styles.boldText, styles.greenText]}> {brokerTarget}</Text> with an upside of
              <Text style={[styles.boldText, styles.greenText]}> {upside}%</Text>
            </Text>
          </View>
        </View>
      );
    } else return null;
  };

  renderReportItem = ({ item, index }) => {
    const { stock } = this.state;
    const { activeWatchlist, userState } = this.props;

    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <StockReportCard
        {...this.props}
        position={index}
        // hideBroker
        data={Object.assign(item, stock)}
        showAlerts={this.showAlerts}
        showWatchlist={this.showWatchlist}
        watchlistBtnState={isWatchlistActive}
        showPortfolio={this.showPortfolio}
        isGuestUser={userState.isGuestUser}
        showLogin={() => this.refs.loginModal.open()}
      />
    );
  };

  render() {
    const { reports, isRefreshing, isNextPage, stock, showScrollToTop } = this.state;
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected } = this.props;

    if (isSelected) {
      return (
        <View style={styles.container}>
          <FlatList
            ref={(ref) => (this.listRef = ref)}
            contentContainerStyle={styles.list}
            data={reports}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            // removeClippedSubviews={true}
            renderItem={this.renderReportItem}
            ListHeaderComponent={this.renderListHeader}
            keyExtractor={(item, index) => index.toString()}
            // refreshControl={this.renderRefreshControl()}
            onEndReached={() => {
              isNextPage && this.getData();
            }}
            onEndReachedThreshold={0.7}
            removeClippedSubviews
            initialNumToRender={3}
            maxToRenderPerBatch={5}
            onScroll={this.handleScroll}
          />

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={stock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={stock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockAlertsModal
            ref="stockAlerts"
            {...this.props}
            stock={stock}
            // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
            // data={this.state.dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={stock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={stock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />

          <LoginModal ref="loginModal" {...this.props} />

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {isRefreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(StockReportScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingVertical: wp(1.5),
  },
  extraSpace: {
    marginBottom: ifIphoneX ? wp(3) : 0,
  },
  summaryContainer: {
    backgroundColor: colors.white,
    padding: wp(3),
    marginBottom: wp(2),
  },
  summary: {
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
    margin: wp(3),
  },
  insight: {
    fontSize: RF(2.2),
    color: colors.black,
    lineHeight: wp(5.5),
  },
  boldText: {
    fontWeight: "500",
  },
  greenText: {
    color: colors.green,
  },
});
