import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList, ScrollView, Image } from "react-native";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { FUNDAMENTAL_CHART_CONFIG } from "../../utils/ChartConfigs";
import { FundamentalBSheet } from "../../components/stockDetail/fundamental/FundamentalBSheet";
import { getFundamentalData } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import SuperstarQuarterList from "../../components/superstar/SuperstarQuarterList";
import FundamentalInfoBSheet from "../../components/stockDetail/fundamental/FundamentalInfoBSheet";
import { connect } from "react-redux";
import FundamentalItem from "../../components/stockDetail/fundamental/FundamentalItem";
import NoMatchView from "../../components/common/NoMatchView";
import { numberWithCommas, roundNumber } from "../../utils/Utils";

class StockFundamentalScreen extends React.PureComponent {
  cacheFormat = {
    Results: { Quarterly: {}, Annual: {} },
    Financials: { "Cash Flow": {}, "Balance Sheet": {} },
    Ratios: {
      Liquidity: {},
      Profitability: {},
      Valuation: {},
      "Cash Flow": {},
      Return: {},
    },
  };

  chartConfigs = { ...this.cacheFormat };
  dataCache = { ...this.cacheFormat };

  componentDidMount() {
    console.log("Fundamental", this.props.isSelected);
    this.props.isSelected && this.getData();
  }

  constructor(props) {
    super(props);
    this.state = {
      selectedIndicator: "",
      selectedChart: "",
      indicatorPos: 0,
      selectedTab: "Results",
      resultOption: "Quarterly",
      financialOption: "Cash Flow",
      ratioOption: "Liquidity",
      financials: {},
      results: {},
      ratios: {},
      preferConsolidated: true,
      isRefreshing: false,
      currentQuarter: "",
      dateList: [],
      currentData: {},
      infoData: {},
      isFirstCallComplete: false,
    };

    props.isSelected && this.getData();
  }

  getData = async () => {
    const { stockId, userState, baseURL, appToken } = this.props;
    this.setState({ isRefreshing: true });

    getFundamentalData(baseURL, appToken, userState.token, stockId, (data) => {
      Object.keys(data).length > 0 && this.setState(data, () => this.setQuarters());
      this.setState({ isRefreshing: false, isFirstCallComplete: true });
    });
  };

  setQuarters = async () => {
    const { preferConsolidated, dateList } = this.state;

    const data = this.getListData();
    const keysRemap = [];
    const keys =
      preferConsolidated && "consolidated" in data
        ? Object.keys(data.consolidated)
        : "standalone" in data
        ? Object.keys(data.standalone)
        : [];

    keys.map((key, i) => keysRemap.push([key, i]));

    this.setState({ currentData: data });
    if (keys[0]) this.setState({ currentQuarter: keys[0], dateList: keysRemap });
  };

  getPrevQuarterStr = (qtrString) => {
    let prevQtrString = "";
    let qtr_list = qtrString.split(" ");
    if (qtr_list.length > 0) {
      let year = qtr_list[1];
      year = parseInt(year);
      let qtr = qtr_list[0];
      prevQtrString = qtr + " " + (year - 1);
    }
    return prevQtrString;
  };

  generateChartData = (currentData, currentQuarter, cb) => {
    const { preferConsolidated } = this.state;

    let data = preferConsolidated ? currentData.consolidated : currentData.standalone;
    let quarterData = currentQuarter[0] in data ? data[currentQuarter[0]] : [];

    quarterData.map((item, itemPos) => {
      this.getBarChartConfigs(true, itemPos, null, item.unique_name + itemPos);
    });
  };

  setQuarterString = (quarter) => {
    this.setState({ currentQuarter: quarter[0] });
    console.log(quarter);
  };

  setIndicator = (indicator) => {
    const { selectedIndicator } = this.state;

    if (selectedIndicator == indicator) this.setState({ selectedIndicator: "", selectedChart: "" });
    else
      this.setState({
        selectedIndicator: indicator,
        // selectedChart: ""
      });
  };

  setChartOption = (indicator) => {
    const { selectedChart } = this.state;

    if (selectedChart == indicator) this.setState({ selectedChart: "", selectedIndicator: "" });
    else
      this.setState({
        selectedChart: indicator,
        // selectedIndicator: ""
      });
  };

  showInfo = (data) => {
    this.setState({ infoData: data });
    this.refs.info.open();
  };

  getChartData = (index, pIndex) => {
    const { dateList, currentQuarter, currentData, preferConsolidated } = this.state;
    let currentQuarterValue = 0;

    let type = preferConsolidated ? "consolidated" : "standalone";
    let series = [];
    let categories = [];
    let barLabels = [];
    let i = 0;

    try {
      dateList.map((quarterItem, i) => {
        if (quarterItem[0] == currentQuarter) quarterIndex = i;
      });

      for (i = 0; i < dateList.length; i++) {
        if (i < 5) {
          const currentQuarterData = currentData[type][dateList[i][0]];
          let currentValue = null;
          categories.push(dateList[i][0]);

          currentValue =
            pIndex != null ? currentQuarterData[pIndex].children[index].value : currentQuarterData[index].value;

          if (currentQuarter == dateList[i][0]) currentQuarterValue = currentValue;

          let barColor =
            currentValue == currentQuarterValue
              ? currentValue > 0
                ? colors.primary_color
                : colors.red
              : currentValue > 0
              ? "rgba(36,83,161,0.4)"
              : colors.blush;

          series.push({ y: currentValue, color: barColor });

          let formattedCurrentValue =
            currentValue && !isNaN(currentValue) ? numberWithCommas(currentValue, true, true) : currentValue;
          // console.log("current value", formattedCurrentValue);
          barLabels.push(formattedCurrentValue);
        }
      }
      return {
        series: series,
        category: categories,
        currentValue: currentQuarterValue,
        barLabels: barLabels,
      };
    } catch (e) {
      console.log(e);
      return { series: series, category: categories, currentValue: currentQuarterValue, barLabels: barLabels };
    }
  };

  // uniqueNames = [];

  getCacheData = (uniqueName, isForChart) => {
    const { selectedTab, resultOption, preferConsolidated, currentQuarter, financialOption, ratioOption } = this.state;
    const type = preferConsolidated ? "Consolidated" : "Standalone";

    const categoryObj = isForChart ? this.chartConfigs[selectedTab] : this.dataCache[selectedTab];
    let selectedOption = "";

    switch (selectedTab) {
      case "Results":
        selectedOption = resultOption;
        break;
      case "Financials":
        selectedOption = financialOption;
        break;
      case "Ratios":
        selectedOption = ratioOption;
        break;
    }

    const isQuarterAvailable = categoryObj[selectedOption][currentQuarter] ? true : false;
    const isTypeAvailable = isQuarterAvailable && categoryObj[selectedOption][currentQuarter][type] ? true : false;

    return isTypeAvailable && categoryObj[selectedOption][currentQuarter][type][uniqueName]
      ? categoryObj[selectedOption][currentQuarter][type][uniqueName]
      : null;
  };

  setCacheData = (uniqueName, config, isForChart) => {
    const { selectedTab, resultOption, preferConsolidated, currentQuarter, financialOption, ratioOption } = this.state;
    const type = preferConsolidated ? "Consolidated" : "Standalone";
    let selectedOption = "";
    let currentData = isForChart ? this.chartConfigs : this.dataCache;

    switch (selectedTab) {
      case "Results":
        selectedOption = resultOption;
        break;
      case "Financials":
        selectedOption = financialOption;
        break;
      case "Ratios":
        selectedOption = ratioOption;
        break;
    }

    let isQuarterAvailable = currentData[selectedTab][selectedOption][currentQuarter] ? true : false;
    let isTypeAvailable =
      isQuarterAvailable && currentData[selectedTab][selectedOption][currentQuarter][type] ? true : false;

    isTypeAvailable
      ? (currentData[selectedTab][selectedOption][currentQuarter][type][uniqueName] = config)
      : (currentData[selectedTab][selectedOption][currentQuarter] = { [type]: { [uniqueName]: config } });

    isForChart ? (this.chartConfigs = currentData) : (this.dataCache = currentData);
  };

  calculateYoyChange = (uniqueName, index, pIndex) => {
    let yoyCache = this.getCacheData(uniqueName, false);
    let isYoyExist = yoyCache ? true : false;

    // console.log("Yoy cache", yoyCache, isYoyExist);

    if (!isYoyExist) {
      const { selectedTab, resultOption, dateList, currentQuarter, currentData, preferConsolidated } = this.state;
      // if (selectedTab != "Results" && resultOption != "Quarterly") {
      // let prevQuarter = "";
      let yoy = " - ";

      try {
        // dateList.map((quarterItem, i) => {
        //   if (quarterItem[0] == currentQuarter) {
        //     if (selectedTab == "Results" && resultOption == "Quarterly") prevQuarter = dateList[i + 4];
        //     else prevQuarter = dateList[i + 1];
        //   }
        // });

        let prevQuarter = this.getPrevQuarterStr(currentQuarter);
        // console.log("Prev quarter", prevQuarter);

        let type = preferConsolidated ? "consolidated" : "standalone";
        const currentQuarterData = currentData[type][currentQuarter];
        const prevQuarterData = currentData[type][prevQuarter];

        let currentValue = "";
        let prevValue = "";

        if (pIndex != null) {
          currentValue = currentQuarterData[pIndex].children[index].value;
          prevValue = prevQuarterData[pIndex].children[index].value;
        } else {
          currentValue = currentQuarterData[index].value;
          prevValue = prevQuarterData[index].value;
        }

        if (!isNaN(currentValue) && !isNaN(prevValue)) {
          yoy = ((currentValue - prevValue) / prevValue) * 100;
          yoy = !isNaN(yoy) && Number.isFinite(yoy) ? roundNumber(yoy) : " - ";
        }
        this.setCacheData(uniqueName, yoy, false);
        return yoy;
      } catch {
        (e) => console.log(e);
      }
    } else return yoyCache;
  };

  getBarChartConfigs = (isForThumbnail, childIndex, pIndex, uniqueName) => {
    const savedConfig = this.getCacheData(uniqueName, true);
    const isConfigAvailable = savedConfig ? true : false;

    if (!isConfigAvailable || !isForThumbnail) {
      let config = JSON.parse(JSON.stringify(FUNDAMENTAL_CHART_CONFIG));
      const chartData = this.getChartData(childIndex, pIndex);

      config.xAxis[0].categories = ["", "", "", "", ""];
      config.series[0].data = chartData ? chartData.series.reverse() : [];

      if (isForThumbnail) {
        config.plotOptions.series.pointWidth = wp(3);
        config.plotOptions.series.groupPadding = 0;
        config.xAxis[0].labels.enabled = false;
        config.xAxis[0].lineWidth = 0;
        config.yAxis.labels.enabled = false;
        config.yAxis.gridLineWidth = 0;
        config.plotOptions.series.enableMouseTracking = false;
        // config.plotOptions.series.dataLabels.enabled = false;
      } else {
        config.xAxis[0].categories = chartData.category.reverse();
        config.xAxis[1] = {
          categories: chartData.barLabels.reverse(),
          linkedTo: 0,
          opposite: true,
          labels: {
            formatter: function () {
              return `<span style="color:black;font-size:12px;font-weight:500">${this.value}</span>`;
            },
          },
        };
        config.plotOptions.series.pointWidth = wp(8);
        config.xAxis[0].labels.enabled = true;
        config.xAxis[0].lineWidth = 1;
        config.yAxis.labels.enabled = true;
        config.yAxis.gridLineWidth = 1;
        config.plotOptions.series.enableMouseTracking = true;
        config.tooltip = { pointFormat: "{point.y}" };
        // config.series[0].dataLabels = { format: "{point.y:,.1f}", color: "black" };
      }

      config.plotOptions.series.animation = false;
      config.plotOptions.series.point = {
        events: {
          click: function () {
            var cat = this.category;
            var categoryStr = "category is " + cat;
            window.__REACT_WEB_VIEW_BRIDGE.postMessage(categoryStr);
          },
        },
      };

      isForThumbnail && this.setCacheData(uniqueName, config, true);
      return config;
    } else {
      return savedConfig;
    }
  };

  onMessage = (m) => {
    const data = m.nativeEvent.data ? m.nativeEvent.data.split(" ") : [];
    if (data.length > 0) {
      const quarter = data[data.length - 2] + " " + data[data.length - 1];
      this.setState({ currentQuarter: quarter });
    }
    // console.log("click from the chart", m.nativeEvent.data);
  };

  setTabSelection = (type) => {
    if (this.state.selectedTab != type && this.state[type.toLowerCase()]) {
      // this.setState({ selectedTab: type });
      this.setState({ selectedTab: type }, () => this.setQuarters());
    }
  };

  setDropdownSelection = (option) => {
    const { selectedTab } = this.state;

    if (selectedTab == "Results") this.setState({ resultOption: option }, () => this.setQuarters());
    else if (selectedTab == "Financials") this.setState({ financialOption: option }, () => this.setQuarters());
    else if (selectedTab == "Ratios") this.setState({ ratioOption: option }, () => this.setQuarters());
  };

  setTypeSelection = (flag) => {
    this.setState({ preferConsolidated: flag }, () => this.setQuarters());
  };

  showDropdownOptions = () => {
    const { selectedTab } = this.state;

    if (selectedTab == "Results") this.refs.results.RBSheet.open();
    else if (selectedTab == "Financials") this.refs.financial.RBSheet.open();
    else if (selectedTab == "Ratios") this.refs.ratios.RBSheet.open();
  };

  // renderPageIndicator = data => {
  //   const { indicatorPos } = this.state;
  //   if (data.length > 1)
  //     return (
  //       <View style={styles.pageIndicator}>
  //         {data.map((item, index) => {
  //           const indicatorStyle = indicatorPos == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

  //           return <View style={indicatorStyle} key={index.toString()} />;
  //         })}
  //       </View>
  //     );
  // };

  getSelectedOption = () => {
    const { selectedTab, resultOption, financialOption, ratioOption } = this.state;

    if (selectedTab == "Results") return resultOption;
    else if (selectedTab == "Financials") return financialOption;
    else return ratioOption;
  };

  renderQuarterOptions = () => {
    const { preferConsolidated } = this.state;

    const standaloneContainerStyle = !preferConsolidated ? [styles.typeOption, styles.blueBg] : styles.typeOption;
    const consolidatedContainerStyle = preferConsolidated ? [styles.typeOption, styles.blueBg] : styles.typeOption;
    const standaloneTextStyle = !preferConsolidated ? [styles.type, styles.whiteText] : styles.type;
    const consolidatedTextStyle = preferConsolidated ? [styles.type, styles.whiteText] : styles.type;

    return (
      <View style={styles.quarterRow}>
        <TouchableOpacity style={styles.quarterBtn} onPress={this.showDropdownOptions}>
          <Text style={[styles.quarterBtnText, { marginHorizontal: wp(1.3) }]}>{this.getSelectedOption()}</Text>
          <Image source={require("../../assets/icons/arrow-down-blue.png")} style={styles.quarterBtnIcon} />
        </TouchableOpacity>

        <View style={styles.typeOptionsContainer}>
          <TouchableOpacity style={standaloneContainerStyle} onPress={() => this.setTypeSelection(false)}>
            <Text style={standaloneTextStyle}>Standalone</Text>
          </TouchableOpacity>

          <TouchableOpacity style={consolidatedContainerStyle} onPress={() => this.setTypeSelection(true)}>
            <Text style={consolidatedTextStyle}>Consolidated</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderOptionRows = () => {
    const { selectedTab } = this.state;
    const tabOptions = ["Results", "Financials", "Ratios"];

    return (
      <View style={styles.optionRow}>
        {tabOptions.map((option) => {
          const optionContainerStyle = selectedTab == option ? styles.optionContainerActive : styles.optionContainer;
          const optionStyle = selectedTab == option ? [styles.option, styles.whiteText] : styles.option;

          return (
            <TouchableOpacity style={optionContainerStyle} key={option} onPress={() => this.setTabSelection(option)}>
              <Text style={optionStyle}>{option + " "}</Text>
            </TouchableOpacity>
          );
        })}
      </View>
    );
  };

  renderListHeader = () => {
    const { currentQuarter } = this.state;

    return (
      <View style={styles.header}>
        <View style={[styles.leftCol]}>
          <Text style={[styles.colName]}>Indicator</Text>
        </View>

        <View style={[styles.centerCol]}>
          <Text style={[styles.colName, { textAlign: "right" }]} numberOfLines={2}>
            Rs Cr.
          </Text>
          <Text style={styles.yoy}>(%change YoY)</Text>
        </View>

        <TouchableOpacity
          style={[styles.rightCol, styles.row]}
          onPress={() => (this.refs.quarterList.RBSheet != null ? this.refs.quarterList.open(currentQuarter) : null)}
        >
          <Text style={[styles.colName, styles.blueText]}>{currentQuarter + " "}</Text>

          <Image source={require("../../assets/icons/arrow-down-blue.png")} style={styles.quarterBtnIcon} />
        </TouchableOpacity>
      </View>
    );
  };

  getListData = () => {
    const { selectedTab, results, financials, ratios, resultOption, financialOption, ratioOption } = this.state;

    if (selectedTab == "Results") {
      switch (resultOption) {
        case "Quarterly":
          return "quarterly" in results ? results.quarterly : {};
        case "Annual":
          return "annual" in results ? results.annual : {};
        default:
          return {};
      }
    } else if (selectedTab == "Financials") {
      switch (financialOption) {
        case "Balance Sheet":
          return "balanceSheet" in financials ? financials.balanceSheet : {};
        case "Cash Flow":
          return "cashFlow" in financials ? financials.cashFlow : {};
        default:
          return {};
      }
    } else if (selectedTab == "Ratios") {
      switch (ratioOption) {
        case "Liquidity":
          return "liquidity" in ratios ? ratios.liquidity : {};
        case "Profitability":
          return "profitability" in ratios ? ratios.profitability : {};
        case "Valuation":
          return "valuation" in ratios ? ratios.valuation : {};
        case "Cash Flow":
          return "cashFlow" in ratios ? ratios.cashFlow : {};
        case "Return":
          return "return" in ratios ? ratios.return : {};
        default:
          return {};
      }
    } else return {};
  };

  renderListItem = ({ item, index }) => {
    const { selectedIndicator, selectedChart, selectedTab } = this.state;

    return (
      <FundamentalItem
        data={item}
        index={index}
        selectedIndicator={selectedIndicator}
        selectedChart={selectedChart}
        selectedTab={selectedTab}
        getChartConfig={(isThumbnail, itemPos, pIndex, uniqueName) =>
          this.getBarChartConfigs(isThumbnail, itemPos, pIndex, uniqueName)
        }
        getYoy={(uniqueName, itemPos, pIndex) => this.calculateYoyChange(uniqueName, itemPos, pIndex)}
        showInfo={this.showInfo}
        setChartOption={this.setChartOption}
        setIndicator={this.setIndicator}
        onMessage={this.onMessage}
      />
    );
  };

  renderListHeaders = () => {
    return (
      <View>
        {this.renderOptionRows()}
        {this.renderQuarterOptions()}
        {this.renderListHeader()}
      </View>
    );
  };

  scrollToBottom() {
    this.setState({ currentQuarter: this.state.currentQuarter });
  }

  renderList = () => {
    const { preferConsolidated, currentQuarter, currentData } = this.state;
    // const data = this.getListData();
    const data = currentData ? currentData : {};

    if (Object.keys(data).length > 0) {
      const dataList = preferConsolidated ? data.consolidated : data.standalone;

      return (
        <FlatList
          ref="myView"
          style={{ flex: 1 }}
          onLayout={() => this.scrollToBottom()}
          showsVerticalScrollIndicator={false}
          data={dataList[currentQuarter]}
          extraData={this.state.currentData}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => item.unique_name}
          ListHeaderComponent={this.renderListHeaders()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          removeClippedSubviews={false}
          // initialNumToRender={6}
          // maxToRenderPerBatch={2}
          // initialScrollIndex={6}
          // maintainVisibleContentPosition={{
          //   minIndexForVisible: 0
          // }}
        />
      );
    }
  };

  render() {
    const { isSelected } = this.props;
    const {
      isRefreshing,
      dateList,
      selectedTab,
      resultOption,
      infoData,
      isFirstCallComplete,
      currentData,
    } = this.state;

    const isEmpty = isFirstCallComplete && currentData ? Object.keys(currentData).length == 0 : false;

    const quartersYears =
      selectedTab == "Results" && resultOption == "Quarterly"
        ? dateList.slice(0, dateList.length - 4)
        : dateList.slice(0, dateList.length - 1);

    if (isSelected) {
      return (
        <View style={styles.container}>
          {this.renderList()}

          <FundamentalBSheet ref="results" type="results" onSelect={this.setDropdownSelection} />
          <FundamentalBSheet ref="financial" type="financial" onSelect={this.setDropdownSelection} />
          <FundamentalBSheet ref="ratios" type="ratios" onSelect={this.setDropdownSelection} />

          <SuperstarQuarterList
            ref="quarterList"
            {...this.props}
            data={quartersYears}
            onPress={this.setQuarterString}
          />

          <FundamentalInfoBSheet ref="info" data={infoData} />
          {isEmpty && <NoMatchView message="No Results found" />}
          {isRefreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockFundamentalScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
  },
  optionRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(5),
    marginBottom: wp(4),
  },
  optionContainer: {
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    marginRight: wp(2),
  },
  optionContainerActive: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginRight: wp(2),
  },
  option: {
    fontSize: RF(1.9),
    fontWeight: "400",
    color: colors.primary_color,
    marginHorizontal: 12,
    marginVertical: 8,
  },
  quarterBtn: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  quarterBtnText: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  quarterBtnIcon: {
    width: wp(3),
    height: wp(3),
    resizeMode: "contain",
    marginLeft: 4,
    alignItems: "center",
    justifyContent: "center",
    margin: wp(0.6),
  },
  quarterRow: {
    flexDirection: "row",
    padding: wp(3),
    backgroundColor: colors.white,
    alignItems: "center",
  },
  typeOptionsContainer: {
    flexDirection: "row",
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
  },
  typeOption: {
    alignItems: "center",
    justifyContent: "center",
  },
  typeActive: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  type: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginVertical: 5,
    marginHorizontal: wp(3),
  },
  blueBg: {
    backgroundColor: colors.primary_color,
  },
  whiteText: {
    color: colors.white,
  },
  blueText: {
    color: colors.primary_color,
  },
  header: {
    flexDirection: "row",
    marginTop: wp(5),
    marginHorizontal: wp(0),
    marginBottom: wp(3),
    paddingBottom: wp(3),
    paddingTop: wp(1),
    // marginVertical:wp(7),
    alignItems: "baseline",
    backgroundColor: colors.whiteTwo,
    // justifyContent: "flex-end"
  },
  leftCol: {
    flex: 1.2,
    marginRight: wp(3),
    alignSelf: "flex-end",
    // flexWrap: "wrap"
  },
  centerCol: {
    flex: 1,
    alignSelf: "flex-end",
    // backgroundColor:colors.whiteTwo
  },
  rightCol: {
    flex: 1,
    marginLeft: wp(2),
    marginRight: wp(4),
    justifyContent: "flex-end",
    alignSelf: "flex-end",
    alignItems: "flex-end",
  },
  row: {
    flexDirection: "row",
  },
  colName: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.black,
    marginLeft: wp(8.5),
  },
  divider: {
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  yoy: {
    textAlign: "right",
  },
});
