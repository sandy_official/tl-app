/**
 * Libraries
 */
import React from "react";
import { View, Text, Image, TouchableOpacity, StyleSheet, FlatList, ScrollView } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import Slider from "../../libs/react-native-slider";
import { connect } from "react-redux";
import Toast from "react-native-easy-toast";

/**
 * Custom components, methods and constants
 */
import { colors, globalStyles } from "../../styles/CommonStyles";
import MomentumItem from "../../components/stockDetail/technical/MomentumItem";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { getStockChartData } from "../../controllers/StocksController";
import StockExpensiveRocketView from "../../components/stockDetail/overview/StockExpensiveRocketView";
import MomentumInfoBSheet from "../../components/stockDetail/technical/MomentumInfoBSheet";
import StockHoldingHistoryBSheet from "../../components/common/StockHoldingHistoryBSheet";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import StockDetails from "../../components/stockDetail/overview/StockDetails";
import StockSwotAnalysis from "../../components/stockDetail/overview/StockSwotAnalysis";
import StockOverviewChart from "../../components/stockDetail/overview/StockOverviewChart";
import LoadingView from "../../components/common/LoadingView";
import LoginModal from "../../components/common/LoginModal";
import { fetcher } from "../../controllers/Fetcher";
import { STOCK_OVERVIEW_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import { mapHeadersData } from "../../utils/Utils";

const SUMMARY_COLORS = [
  colors.blueViolet,
  colors.hotMagenta,
  colors.vibrantPurple,
  colors.squash,
  colors.barbiePink,
  colors.primary_color,
  colors.green,
];

// const CHART_TIMELINE = ["1D", "1W", "1M", "3M", "6M", "1Y"];

class StockOverviewScreen extends React.PureComponent {
  fundamentalRef = null;

  componentWillMount() {
    this.props.isSelected && this.getData();
  }

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      alertActive: false,
      portfolioActive: false,
      watchlistActive: false,
      showSwotDetails: false,
      selectedStock: {},
      watchlistStocks: {},
      stock: {},
      shareholding: [],
      swot: {},
      technical: [],
      fundamental: [],
      chart: {},
      selectedMomentum: {},
      indicatorPos: 0,
      holdingId: "",
      selectedTimeline: "1D",
    };
  }

  componentDidUpdate() {
    // console.log("state", this.state);
  }

  getData = () => {
    const { stockId, userState, baseref, baseURL, appToken } = this.props;
    const body = { stock_id: stockId };
    const URL = baseURL + STOCK_OVERVIEW_URL;

    this.setState({ refreshing: true });

    fetcher(
      URL,
      POST_METHOD,
      body,
      baseref,
      appToken,
      userState.token,
      (json) => {
        this.mapStockOverviewData(json.body);
        // this.setState({ refreshing: false });
        this.getChartData();
      },
      (error) => {
        this.setState({ refreshing: false });
      }
    );
  };

  getChartData = () => {
    const { stockId, userState, baseURL, appToken } = this.props;

    getStockChartData(baseURL, appToken, userState.token, stockId, (data) => {
      this.setState({ chart: data, refreshing: false });
    });
  };

  mapStockOverviewData = (json) => {
    let stock = {};
    let swot = {};
    const stockHeaders = json.stockHeaders;

    // console.log("Stock API", json.stockData, stockHeaders);

    json.stockData.map((item, i) => {
      stock[stockHeaders[i].unique_name] = item;
    });

    if (Object.keys(json.SWOTData).length > 0) {
      const swotHeaders = json.SWOTData.tableHeaders;
      const swotData = json.SWOTData.tableData;
      const swotKeys = Object.keys(swotData);

      swotKeys.map((key) => {
        swot[key] = mapHeadersData(swotHeaders, swotData[key]);
      });
    }
    // this.setState({ stock: stock, swot: swot });

    let summaryData = [];
    if ("summaryData" in json && json.summaryData.length > 0) {
      const summaryHeaders = json.summaryData[0];
      json.summaryData.map((item, i) => {
        if (i != 0) {
          let summaryObj = {};
          summaryObj[summaryHeaders[0]] = item[0];
          summaryObj[summaryHeaders[1]] = item[1];
          summaryObj[summaryHeaders[2]] = item[2];
          summaryData.push(summaryObj);
        }
      });
    }

    let i = 0;
    let fundamental = [];
    for (i; i < json.fundamentalData.length; i += 3) {
      temp = json.fundamentalData.slice(i, i + 3);
      fundamental.push(temp);
    }
    this.setState({
      stock: stock,
      swot: swot,
      shareholding: summaryData,
      technical: json.technicalData,
      fundamental: fundamental,
      // refreshing: false
    });
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState(
        { indicatorPos: viewableItems[0].index }
        // () =>
        //   this.fundamentalRef && this.fundamentalRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  showPopup = (momentum) => {
    this.setState({ selectedMomentum: momentum }, () => this.refs.info.RBSheet.open());
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  showAlerts = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.stockAlerts.open();
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = () => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.refs.portfolio.modal.open();
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  showWatchlist = () => {
    const { userState } = this.props;
    const { stock } = this.state;

    if (!userState.isGuestUser) {
      this.refs.Watchlists.open();
    } else this.refs.loginModal.open();
  };

  showShareholdingHistory = (obj) => {
    // console.log("hId", obj);
    this.setState({ holdingId: obj.holdingId }, () => {
      this.refs.StockHoldingHistory.openDialog();
    });
  };

  renderPageIndicator = () => {
    const { fundamental, stock, indicatorPos } = this.state;

    return (
      <View style={styles.pageIndicator}>
        {fundamental.map((item, index) => {
          const indicatorStyle = indicatorPos == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

          return <View style={indicatorStyle} key={index.toString()} />;
        })}
      </View>
    );
  };

  renderFundamentalCard = (data) => {
    // console.log("data", data);

    return (
      <FlatList
        // contentContainerStyle={{ }}
        data={data}
        // extraData={this.state}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => <MomentumItem data={item} onSelect={this.showPopup} />}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
      />
    );
  };

  renderFundamentalItem = ({ item, index }) => {
    return <View style={[styles.card, styles.fundamentalItem]}>{this.renderFundamentalCard(item)}</View>;
  };

  renderFundamentals = () => {
    const { fundamental } = this.state;
    const { openFundamental } = this.props;
    // console.log("data", fundamental);

    return (
      <View>
        <View style={styles.fundamentalRow}>
          <TouchableOpacity onPress={() => openFundamental()}>
            <Text style={[styles.heading, { marginTop: 0, flex: 1 }]} numberOfLines={1}>
              FUNDAMENTALS
              <Text style={styles.fundamental_viewMore}>{`    View more`}</Text>
            </Text>
          </TouchableOpacity>
          <View style={{ flex: 1, alignItems: "flex-end" }}>{this.renderPageIndicator()}</View>
        </View>
        {/* Four colors green, red, yellow & grey */}
        <FlatList
          ref={(ref) => (this.fundamentalRef = ref)}
          contentContainerStyle={[styles.list]}
          data={fundamental}
          extraData={this.state}
          horizontal
          showsHorizontalScrollIndicator={false}
          renderItem={this.renderFundamentalItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
          removeClippedSubviews
          initialNumToRender={2}
          maxToRenderPerBatch={1}
        />
      </View>
    );
  };

  renderTechnicalItem = (item, index) => <MomentumItem data={item.item} onSelect={this.showPopup} />;

  renderTechnical = () => {
    const { technical, stock } = this.state;
    const { openTechnical } = this.props;

    return (
      <View style={{ marginTop: -wp(1) }}>
        <View style={[styles.row, { marginBottom: 0 }]}>
          <Text style={styles.headingInRow}>TECHNICALS</Text>
          <TouchableOpacity onPress={() => openTechnical()}>
            <Text style={styles.viewMore}>View more</Text>
          </TouchableOpacity>
        </View>

        {/* Four colors green, red, yellow & grey */}
        <View style={[styles.list, styles.tech_container, { backgroundColor: colors.white }]}>
          <FlatList
            // contentContainerStyle={[styles.list, styles.card]}
            data={technical}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderTechnicalItem}
            keyExtractor={(item, index) => index.toString()}
            ItemSeparatorComponent={() => <View style={styles.divider} />}
            removeClippedSubviews
            initialNumToRender={2}
            maxToRenderPerBatch={1}
          />
        </View>
      </View>
    );
  };

  renderSummaryRow = (item, i) => {
    const showHistoryIcon = item.holdingId ? true : false;

    return (
      <View style={styles.summaryRow} key={i.toString()}>
        <Text style={styles.promoter}>{item.Type}</Text>
        <View style={styles.progressBar}>
          <Slider
            minimumValue={0}
            maximumValue={100}
            value={item.Holding}
            minimumTrackTintColor={SUMMARY_COLORS[i]}
            thumbTintColor={SUMMARY_COLORS[i]}
            disabled
          />
        </View>
        {showHistoryIcon ? (
          <TouchableOpacity onPress={() => this.showShareholdingHistory(item)}>
            <Image
              // source={{ uri: ICON_CDN + "screeners/bookmark-screeners.png" }}
              source={require("../../assets/icons/history-blue.png")}
              style={styles.recentIcon}
            />
          </TouchableOpacity>
        ) : (
          <View style={{ marginLeft: wp(7) }} />
        )}
      </View>
    );
  };

  renderShareHolding = () => {
    const { shareholding } = this.state;
    const { openShareholding } = this.props;

    return (
      <View>
        <View style={styles.row}>
          <Text style={styles.headingInRow}>SHAREHOLDING SUMMARY</Text>
          <TouchableOpacity onPress={() => openShareholding()}>
            <Text style={styles.viewMore}>View more</Text>
          </TouchableOpacity>
        </View>
        {shareholding.map((item, i) => {
          return this.renderSummaryRow(item, i);
        })}
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    const { shareholding, stock, swot, fundamental, technical, chart } = this.state;
    const { activeWatchlist, pageType } = this.props;

    // const showStockDetails = Object.keys(stock).length > 0;

    switch (index) {
      case 0:
        return <StockDetails stock={stock} />;

      case 1:
        let showStockDetails = Object.keys(stock).length > 0;
        let isWatchlistActive = stock.stock_id in activeWatchlist && activeWatchlist[stock.stock_id].length > 0;

        return showStockDetails
          ? showStockDetails && (
              <StockExpensiveRocketView
                stock={stock}
                // swot={swot}
                showAlerts={() => this.showAlerts()}
                showPortfolio={() => this.showPortfolio()}
                showWatchlist={() => this.showWatchlist()}
                watchlistBtnState={isWatchlistActive}
              />
            )
          : null;

      case 2:
        let showSwot =
          Object.keys(swot).length > 0 &&
          (swot.strengths.length > 0 ||
            swot.threats.length > 0 ||
            swot.opportunities.length > 0 ||
            swot.weaknesses.length > 0);

        return showSwot ? <StockSwotAnalysis {...this.props} swot={swot} /> : null;

      case 3:
        // let showChart = Object.keys(chart).length > 0;
        let showChart = Object.keys(stock).length > 0;
        return showChart ? <StockOverviewChart chart={chart} /> : null;

      case 4:
        let showFundamental = fundamental.length > 0;
        return showFundamental && pageType != "index" ? this.renderFundamentals() : null;

      case 5:
        let showTechnical = technical.length > 0;
        return showTechnical ? this.renderTechnical() : null;

      case 6:
        let showShareholding = shareholding.length > 0;
        return showShareholding && pageType != "index" ? this.renderShareHolding() : null;
    }
  };

  renderList = () => {
    const { stock } = this.state;

    if (Object.keys(stock).length > 0)
      return (
        <FlatList
          data={[{}, {}, {}, {}, {}, {}, {}]}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderListItem}
          keyExtractor={(item, index) => index.toString()}
          removeClippedSubviews={false}
        />
      );
  };

  render() {
    const { refreshing, stock, selectedMomentum, holdingId } = this.state;

    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected } = this.props;

    const momentumTitle = selectedMomentum.name ? selectedMomentum.name : "";
    const momentumDescription = selectedMomentum.lt ? selectedMomentum.lt : "";

    if (isSelected) {
      return (
        <View style={styles.container}>
          {this.renderList()}

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={stock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={stock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockAlertsModal
            ref="stockAlerts"
            {...this.props}
            stock={stock}
            addedTo={(name) => this.refs.toast.show("Added to Primary", 1000)}
            // data={this.state.dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={stock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio")}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={stock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          {/* {this.state.showDropdown && this.renderDropdownList()} */}

          <MomentumInfoBSheet ref="info" title={momentumTitle} info={momentumDescription} />

          <StockHoldingHistoryBSheet
            ref="StockHoldingHistory"
            {...this.props}
            type="stock"
            stock={stock}
            holdingId={holdingId}
            // data={dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

          <LoginModal ref="loginModal" {...this.props} />
          {refreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(mapStateToProps, { setActiveWatchlist, updateWatchlist, updatePortfolio, loadPortfolio }, null, {
  forwardRef: true,
})(StockOverviewScreen);

const styles = StyleSheet.create({
  container: {
    position: "relative",
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    flex: 1,
    marginLeft: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    marginTop: hp(3),
    color: colors.black,
  },
  headingInRow: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  extraSpace: {
    marginBottom: ifIphoneX ? wp(3) : 0,
  },
  list: {
    margin: wp(3),
  },
  card: {
    backgroundColor: colors.white,
    borderRadius: 6,
    shadowColor: "rgba(77, 77, 77, 0.3)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  recentIcon: {
    marginLeft: wp(1),
    marginVertical: wp(1),
  },
  tech_container: {
    paddingHorizontal: wp(3),
    marginVertical: wp(1),
    backgroundColor: colors.white,
    marginLeft: 0,
    marginRight: 0,
  },
  recentIcon: {
    marginLeft: wp(1),
    marginVertical: wp(1),
  },
  promoter: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.black,
  },
  progressBar: {
    flex: 1.4,
    overflow: "hidden",
    marginHorizontal: wp(3),
  },
  summaryRow: {
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    borderBottomWidth: 1,
    borderBottomColor: "rgba(179,179,179,0.2)",
    backgroundColor: colors.white,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  fundamental_viewMore: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
    paddingVertical: wp(1),
    marginHorizontal: wp(3),
    paddingRight: wp(37),
  },
  viewMore: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
    paddingVertical: wp(1),
    paddingLeft: wp(2),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: hp(3),
    marginBottom: wp(3),
    paddingHorizontal: wp(3),
  },
  textDivider: {
    fontSize: RF(2.2),
  },
  yellowBackground: {
    backgroundColor: colors.squash,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  blueBackground: {
    backgroundColor: colors.blue,
  },
  redText: {
    color: colors.red,
  },
  // recentIcon: {
  //   marginLeft: wp(1)
  // },
  advanceChart: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.primary_color,
    textAlign: "center",
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(1.8),
    height: wp(1.8),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.3),
    height: wp(1.3),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  fundamentalRow: {
    flexDirection: "row",
    alignItems: "center",
    marginRight: wp(3),
    marginTop: hp(3),
  },
  fundamentalItem: { width: wp(89), marginRight: wp(3), marginBottom: wp(1) },
});
