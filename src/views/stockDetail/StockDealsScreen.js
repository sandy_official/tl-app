import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, RefreshControl, Platform } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { sortObjectList } from "../../utils/Utils";
import StockBulkDeal from "../../components/stockDetail/deals/StockBulkDeal";
import StockSastDeal from "../../components/stockDetail/deals/StockSastDeal";
import { getStockDeals } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import { SortOptionView } from "../../components/common/SortOptionView";
import { connect } from "react-redux";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class StockDealsScreen extends React.PureComponent {
  state = {
    selectedDeal: 0,
    selectedSort: "Date",
    isSortAscending: false,

    selectedSortBulk: "Date",
    bulkSortKey: "date",
    isBulkSortAscending: false,

    selectedSortIT: "Date",
    isItSortAscending: false,
    itSortKey: "report_date",

    stock: {},
    bulkBlock: { table: [], insight: [] },
    sastIT: { table: [], insight: [] },
    refreshing: false,
    isFirstCallComplete: false,
  };

  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    const { stockId, userState, baseURL, appToken } = this.props;
    const { selectedDeal } = this.state;
    // const superstarId = this.props.navigation.getParam("superstarId");
    let type = selectedDeal == 0 ? "bulkBlock" : "sast";

    // getStockDeals(stockId, stockCode, data => {
    getStockDeals(baseURL, appToken, userState.token, stockId, type, (data) => {
      if (Object.keys(data).length > 0) {
        this.setState({ stock: data.stock, refreshing: false });
        this.setState({
          selectedSort: "Date",
          isSortAscending: false,

          selectedSortBulk: "Date",
          bulkSortKey: "date",
          isBulkSortAscending: false,

          selectedSortIT: "Date",
          isItSortAscending: false,
          itSortKey: "report_date",
        });

        if (this.state.selectedDeal == 0) this.setState({ bulkBlock: data });
        else this.setState({ sastIT: data });
      }

      this.setState({ refreshing: false });
      if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    });
  };

  onRefresh = () => {
    this.setState({ refreshing: true }, () => this.getData());
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  setDealOption = (index) => {
    const { bulkBlock, sastIT } = this.state;
    this.setState({ selectedDeal: index });

    if (index == 0) {
      if (bulkBlock.table.length == 0) this.onRefresh();
    } else {
      if (sastIT.table.length == 0) this.onRefresh();
    }
  };

  // setSortOrder = (label, key) => {
  //   // const { selectedSortBulk, isBulkSortAscending, selectedSortIT, isItSortAscending, selectedDeal } = this.state;
  //   this.sortData(label, key);
  // };

  getSortOrder = (label) => {
    if (label == "Deal Type") return this.state.isSortAscending ? "Bulk" : "Block";
    else return this.state.isSortAscending ? "Ascending" : "Descending";
  };

  setSortOrder = (label, key) => {
    const {
      selectedDeal,
      bulkBlock,
      isBulkSortAscending,
      sastIT,
      selectedSortBulk,
      selectedSortIT,
      isItSortAscending,
    } = this.state;

    if (selectedDeal == 0) {
      let isSortAscending = label == selectedSortBulk ? !isBulkSortAscending : false;
      let sortedData = sortObjectList(bulkBlock.table, key, isSortAscending);

      this.setState({ selectedSortBulk: label, isBulkSortAscending: isSortAscending });
      this.setState({ bulkBlock: { table: sortedData, insight: bulkBlock.insight } });

      console.log("after sort", sortedData);
    } else {
      let isSortAscending = label == selectedSortIT ? !isItSortAscending : false;
      let sortedData = sortObjectList(sastIT.table, key, isSortAscending);

      this.setState({ selectedSortIT: label, isItSortAscending: isSortAscending });
      this.setState({ sastIT: { table: sortedData, insight: sastIT.insight } });
    }
  };

  renderSortingOption = (label, key, customLabel) => {
    const { selectedSortBulk, isBulkSortAscending, selectedSortIT, isItSortAscending, selectedDeal } = this.state;

    const selectedSortOption = selectedDeal == 0 ? selectedSortBulk : selectedSortIT;
    const selectedSortOrder = selectedDeal == 0 ? isBulkSortAscending : isItSortAscending;

    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={selectedSortOption}
        ascending={selectedSortOrder}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
        customLabel={customLabel}
      />
    );
  };

  renderBBDealsSortOptionsRow = () => {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={globalStyles.sortOptionRow}>
          {this.renderSortingOption("Date", "date")}
          {this.renderSortingOption("Deal Type", "get_deal_type_display", ["Block", "Bulk"])}
          {this.renderSortingOption("%Traded", "traded_percent")}
          {this.renderSortingOption("Actions", "get_action_display")}
          {this.renderSortingOption("Quantity", "quantity")}
        </View>
      </ScrollView>
    );
  };

  renderItDealSortOptionsRow = () => {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={globalStyles.sortOptionRow}>
          {this.renderSortingOption("Date", "report_date")}
          {this.renderSortingOption("%Traded", "holdingP_change")}
          {this.renderSortingOption("Quantity", "holding_after")}
          {this.renderSortingOption("Client Name", "actor")}
          {this.renderSortingOption("Category", "actor_category_text")}
          {this.renderSortingOption("Actions", "transaction_type_text")}
        </View>
      </ScrollView>
    );
  };

  renderDealOption = (option, index) => {
    const { selectedDeal } = this.state;

    const optionStyle = selectedDeal == index ? styles.dealOptionSelected : styles.dealOption;
    const optionTextStyle = selectedDeal == index ? styles.dealOptionSelectedText : styles.dealOptionText;

    return (
      <TouchableOpacity onPress={() => this.setDealOption(index)}>
        <View style={optionStyle}>
          <Text style={optionTextStyle}>{option}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderDealOptionRow = () => {
    return (
      <View style={styles.dealOptionRow}>
        {this.renderDealOption("Bulk Block Deals", 0)}
        {this.renderDealOption("Insider Trading & SAST", 1)}
      </View>
    );
  };

  renderInsights = () => {
    let insight = this.state.sastIT.insight;
    // console.log("Insights : " + insight.length)

    if (insight && insight.length > 0)
      return (
        <View>
          <Text style={styles.heading}>INSIGHTS</Text>
          {insight.map((item) => {
            return (
              <Text style={styles.insight} key={item}>
                {item}
              </Text>
            );
          })}
        </View>
      );
  };

  render() {
    const { isSelected } = this.props;
    const { isFirstCallComplete, refreshing, stock, selectedDeal, bulkBlock, sastIT, showScrollToTop } = this.state;

    const listTitle = selectedDeal == 0 ? "BULK AND BLOCK DEALS ON NSE AND BSE" : "INSIDER TRADING & SAST";

    if (isSelected) {
      return (
        <View style={styles.container}>
          {stock && Object.keys(stock).length > 0 && (
            <ScrollView
              ref={(ref) => (this.listRef = ref)}
              showsVerticalScrollIndicator={false}
              onScroll={this.handleScroll}
              refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
            >
              {this.renderDealOptionRow()}
              {selectedDeal == 1 && this.renderInsights()}
              <Text style={styles.heading}>{listTitle}</Text>

              <Text style={styles.sortBy}>Sort By</Text>
              {selectedDeal == 0 && this.renderBBDealsSortOptionsRow()}
              {selectedDeal == 1 && this.renderItDealSortOptionsRow()}

              {selectedDeal == 0 && bulkBlock.table.length > 0 && (
                <StockBulkDeal {...this.props} stock={bulkBlock.table} />
              )}
              {selectedDeal == 1 && sastIT.table.length > 0 && (
                <StockSastDeal {...this.props} stock={sastIT.table} insight={sastIT.insight} />
              )}
            </ScrollView>
          )}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          {!isFirstCallComplete && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockDealsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    paddingTop: wp(5),
    paddingBottom: wp(3),
    paddingLeft: wp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.primary_color,
  },
  sortBy: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    paddingLeft: wp(3),
    paddingTop: wp(3),
  },
  performanceView: {
    minHeight: hp(3),
    backgroundColor: colors.green,
    alignItems: "center",
    justifyContent: "center",
  },
  performanceDetail: {
    fontSize: RF(1.8),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
  },
  sortOptionRow: {
    flexDirection: "row",
    marginHorizontal: wp(1),
    marginVertical: wp(2),
  },
  sortOptionContainer: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    borderColor: colors.primary_color,
    borderWidth: 1,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  sortOptionContainerSelected: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
  },
  sortOption: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sortOptionName: {
    flex: 1,
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  sortOptionNameSelected: {
    flex: 1,
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.white,
  },
  sortIcon: {
    width: wp(2),
    height: wp(2),
    resizeMode: "contain",
  },
  sortOrder: {
    fontSize: RF(1.6),
    color: colors.white,
    opacity: 0.7,
  },
  hide: {
    display: "none",
  },
  // listEndFooter: {
  //   flex: 1,
  //   alignItems: "center",
  //   justifyContent: "center",
  //   marginTop: hp(1)
  // },
  // listEndFooterText: {
  //   fontSize: RF(2.2),
  //   fontWeight: "500",
  //   padding: "3%",
  //   color: colors.pinkishGrey
  // },
  // loaderContainer: {
  //   flex: 1,
  //   justifyContent: "center",
  //   alignItems: "center"
  // },
  dealOptionRow: {
    flexDirection: "row",
    marginTop: hp(3),
    marginHorizontal: wp(3),
  },
  dealOption: {
    marginRight: wp(3),
    paddingVertical: wp(2),
    paddingHorizontal: wp(2.5),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
  },
  dealOptionSelected: {
    marginRight: wp(3),
    paddingVertical: wp(2),
    paddingHorizontal: wp(2.5),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    justifyContent: "center",
    alignItems: "center",
  },
  dealOptionText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
  },
  dealOptionSelectedText: {
    // flex: 1,
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.white,
  },
  insight: {
    fontSize: RF(2),
    padding: wp(3),
    marginBottom: 1,
    backgroundColor: colors.white,
    color: colors.black,
  },
});
