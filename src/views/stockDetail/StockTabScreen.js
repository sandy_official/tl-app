import React from "react";
import { View, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import StockTechnicalScreen from "./StockTechnicalScreen";
import StockDealsScreen from "./StockDealsScreen";
import StockCorporateActionsScreen from "./StockCorporateActionsScreen";
import StockShareHoldingScreen from "./StockShareHoldingScreen";
import StockOverviewScreen from "./StockOverviewScreen";
import StockNewsScreen from "./StockNewsScreen";
import StockReportScreen from "./StockReportScreen";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";
import StockFundamentalScreen from "./StockFundamentalScreen";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio } from "../../actions/PortfolioActions";
import Toast from "react-native-easy-toast";
import LoginModal from "../../components/common/LoginModal";
import BaseComponent from "../../components/common/BaseComponent";
import TabComponent from "../../components/common/TabComponent";

class StockTabScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    const stockId = this.props.navigation.getParam("stockId");
    const stockName = this.props.navigation.getParam("stockName");
    const stockCode = this.props.navigation.getParam("stockCode");

    let tab = this.props.navigation.getParam("tab", 0);
    let page = this.getTabPosition();
    let initialTab = page ? page : tab;
    // const stockId = "360";
    // const stockName = "Infy";

    console.log("Stock Id", stockId);
    console.log("Stock Name", stockName);

    this.state = {
      stockId: stockId,
      stockName: stockName,
      stockCode: stockCode,
      pageType: "Equity",
      selectedTab: initialTab,
      isApiCalled: [],
      baseRef: null,
    };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [] }, () => this.setTabSelection({ position: selectedTab }));
  };

  renderToolbar = () => {
    const { stockName } = this.state;
    const { navigation } = this.props;
    const now = new Date();
    return (
      <Toolbar title={stockName} backButton {...this.props}>
        <ImageButton
          source={require("../../assets/icons/search-white.png")}
          onPress={() => navigation.dispatch({
            key: "stocksSearch" + now.valueOf(),
            type: 'maxRoute',
            routeName: 'StockSearch',
            params: {
            },
          })}
        />
        <ImageButton
          source={require("../../assets/icons/add-white.png")}
          onPress={() => this.refs.stockAdd.RBSheet.open()}
        />
      </Toolbar>
    );
  };

  setTabSelection = (obj) => {
    const { isApiCalled } = this.state;
    const pageType = this.props.navigation.getParam("pageType", "Equity");

    const tabPosition = obj.position;
    this.setState({ selectedTab: tabPosition });

    // console.log("page type", pageType ? pageType.toLowerCase() : pageType);

    if (!isApiCalled[tabPosition]) {
      switch (pageType ? pageType.toLowerCase() : pageType) {
        case "index":
        case "etf":
          switch (tabPosition) {
            case 0:
              this.refs.overview.getData();
              console.log("Overview");
              break;
            case 1:
              console.log("Technical");
              this.refs.technical.getData();
              break;
            case 2:
              console.log("Stock News");
              this.refs.news.getData();
              break;
          }
          break;

        case "unlisted":
          switch (tabPosition) {
            case 0:
              console.log("Fundamental");
              this.refs.fundamental.getData();
              break;
            case 1:
              console.log("Stock Reports");
              this.refs.report.getData();
              break;
            case 2:
              console.log("Stock News");
              this.refs.news.getData();
              break;
          }
          break;

        default:
          switch (tabPosition) {
            case 0:
              this.refs.overview.getData();
              console.log("Overview");
              break;
            case 1:
              console.log("Fundamental");
              this.refs.fundamental.getData();
              break;
            case 2:
              console.log("Technical");
              this.refs.technical.getData();
              break;
            case 3:
              console.log("Shareholding");
              this.refs.shareholding.getData();
              break;
            case 4:
              console.log("Stock Reports");
              this.refs.report.getData();
              break;
            case 5:
              console.log("Stock News");
              this.refs.news.getData();
              break;
            case 6:
              console.log("Corporate Actions");
              this.refs.corporate.loadData();
              break;
            case 7:
              console.log("Stock Deals");
              this.refs.deals.getData();
              break;
            default:
              null;
          }
          break;
      }

      let apiCalls = this.state.isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls });
    }
  };

  gotoTechnical = () => {
    const pageType = this.props.navigation.getParam("pageType", "Equity");
    let tabPos = pageType && (pageType.toLowerCase() == "index" || pageType.toLowerCase() == "etf") ? 1 : 2;
    this.refs.tabs ? this.refs.tabs.setPage(tabPos) : console.log("ref undefined");
  };

  gotoShareholding = () => this.refs.tabs && this.refs.tabs.setPage(3);

  openFundamental = () => this.refs.tabs && this.refs.tabs.setPage(1);

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;

    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.watchlists.showWatchlistAdded();
  };

  showAlert = () => {
    const { isGuestUser } = this.props.userState;

    this.refs.stockAdd.RBSheet.close();

    if (!isGuestUser) {
      this.refs.stockAlerts.open();
      // this.refs.stockAlerts.refresh();
    } else this.refs.loginModal.open();
  };

  showPortfolio = () => {
    const { isGuestUser } = this.props.userState;

    this.refs.stockAdd.RBSheet.close();

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };

  showWatchlist = () => {
    const { isGuestUser } = this.props.userState;

    this.refs.stockAdd.RBSheet.close();

    if (!isGuestUser) {
      this.refs.watchlists.open();
    } else this.refs.loginModal.open();
  };

  getTabTitles = () => {
    const pageType = this.props.navigation.getParam("pageType", "Equity");

    switch (pageType ? pageType.toLowerCase() : pageType) {
      case "index":
      case "etf":
        return ["Overview", "Technicals", "News"];

      case "unlisted":
        return ["Fundamentals", "Reports", "News"];

      default:
        return [
          "Overview",
          "Fundamentals",
          "Technicals",
          "Shareholding",
          "Reports",
          "News",
          "Corporate Actions",
          "Deals",
        ];
    }
  };

  getTabPosition = () => {
    let page = this.props.navigation.getParam("page", "overview");
    console.log("page received", page);
    switch (page) {
      case "overview":
        return 0;
      case "fundamental":
        return 1;
      case "technical":
        return 2;
      case "shareholding":
        return 3;
      case "reports":
        return 4;
      case "news":
        return 5;
      case "corporate":
        return 6;
      case "deals":
        return 7;
      default:
        return null;
    }
  };

  getAnalyticsParams = () => {
    const { stockId, selectedTab, stockName } = this.state;
    return {
      screenName: selectedTab,
      stock_id: stockId,
      stockName: stockName,
    };
  };
  renderEquityTabs = () => {
    const { stockId, selectedTab } = this.state;
    // let tab = this.props.navigation.getParam("tab", 0);
    // let page = this.getTabPosition();
    // let initialTab = page ? page : tab;

    return (
      <TabComponent
        ref="tabs"
        tabs={this.getTabTitles()}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
        analyticsParams={this.getAnalyticsParams}
      >
        <View>
          <StockOverviewScreen
            ref="overview"
            {...this.props}
            isSelected={selectedTab == 0}
            stockId={stockId}
            openFundamental={this.openFundamental}
            openShareholding={this.gotoShareholding}
            openTechnical={this.gotoTechnical}
          />
        </View>
        <View>
          {/* <Text>Fundamental UI Pending</Text> */}
          <StockFundamentalScreen ref="fundamental" {...this.props} isSelected={selectedTab == 1} stockId={stockId} />
        </View>
        <View>
          <StockTechnicalScreen ref="technical" {...this.props} isSelected={selectedTab == 2} stockId={stockId} />
        </View>
        <View>
          <StockShareHoldingScreen ref="shareholding" {...this.props} isSelected={selectedTab == 3} stockId={stockId} />
        </View>
        <View>
          <StockReportScreen ref="report" {...this.props} isSelected={selectedTab == 4} stockId={stockId} />
        </View>
        <View>
          <StockNewsScreen ref="news" {...this.props} isSelected={selectedTab == 5} stockId={stockId} />
        </View>
        <View>
          <StockCorporateActionsScreen
            ref="corporate"
            {...this.props}
            isSelected={selectedTab == 6}
            stockId={stockId}
          />
        </View>
        <View>
          <StockDealsScreen ref="deals" {...this.props} isSelected={selectedTab == 7} stockId={stockId} />
        </View>
      </TabComponent>
    );
  };

  renderUnlistedTabs = () => {
    const { stockId, selectedTab } = this.state;
    // const tab = this.props.navigation.getParam("tab", 0);

    return (
      <TabComponent
        ref="tabs"
        tabs={this.getTabTitles()}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
        analyticsParams={this.getAnalyticsParams}
      >
        <View>
          <StockFundamentalScreen ref="fundamental" {...this.props} isSelected={selectedTab == 0} stockId={stockId} />
        </View>
        <View>
          <StockReportScreen ref="report" {...this.props} isSelected={selectedTab == 1} stockId={stockId} />
        </View>
        <View>
          <StockNewsScreen ref="news" {...this.props} isSelected={selectedTab == 2} stockId={stockId} />
        </View>
      </TabComponent>
    );
  };

  renderIndexTabs = () => {
    const { stockId, selectedTab, stockName, stockCode } = this.state;
    let pageType = this.props.navigation.getParam("pageType", "Equity");
    pageType = pageType ? pageType.toLowerCase() : pageType;

    return (
      <TabComponent
        ref="tabs"
        tabs={this.getTabTitles()}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
        analyticsParams={this.getAnalyticsParams}
      >
        <View>
          <StockOverviewScreen
            ref="overview"
            pageType={pageType}
            {...this.props}
            isSelected={selectedTab == 0}
            stockId={stockId}
            openShareholding={this.gotoShareholding}
            openTechnical={this.gotoTechnical}
          />
        </View>
        <View>
          <StockTechnicalScreen
            ref="technical"
            pageType={pageType}
            {...this.props}
            isSelected={selectedTab == 1}
            stockId={stockId}
          />
        </View>
        <View>
          <StockNewsScreen
            ref="news"
            pageType={pageType}
            stockName={stockName}
            stockCode={stockCode}
            {...this.props}
            isSelected={selectedTab == 2}
            stockId={stockId}
          />
        </View>
      </TabComponent>
    );
  };

  renderTabs = () => {
    const pageType = this.props.navigation.getParam("pageType", "Equity");

    console.log("pageTye", pageType);

    switch (pageType ? pageType.toLowerCase() : pageType) {
      case "index":
      case "etf":
        return this.renderIndexTabs();

      case "unlisted":
        return this.renderUnlistedTabs();

      default:
        return this.renderEquityTabs();
    }
  };

  render() {
    const { stockId, stockName } = this.state;
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;
    const stock = { stock_id: stockId, NSEcode: stockName };

    return (
      <BaseComponent ref={(ref) => this.setState({ baseRef: ref })} refreshScreen={this.refreshScreen} {...this.props}>
        {this.renderToolbar()}
        {this.renderTabs()}

        <WatchlistModal
          ref="watchlists"
          {...this.props}
          stock={stock}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={(obj) => setActiveWatchlist(obj)}
          createWatchList={() => this.refs.createWatchlist.modal.open()}
        />

        <CreateWatchlistModal
          ref="createWatchlist"
          {...this.props}
          stock={stock}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={this.onWatchlistCreated}
          openWatchlist={() => this.refs.watchlists.open()}
        />

        <StockAlertsModal
          ref="stockAlerts"
          {...this.props}
          stock={stock}
          // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
          // data={this.state.dateList}
          // onPress={quarter => this.setQuarterString(quarter)}
        />

        <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={stock}
          portfolio={portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        />

        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={stock}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

        <StockAddBottomSheet
          ref="stockAdd"
          stockName={stockName}
          showAlert={this.showAlert}
          showWatchlist={this.showWatchlist}
          showPortfolio={this.showPortfolio}
        />

        <LoginModal ref="loginModal" {...this.props} />

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </BaseComponent>
    );
  }
}

const mapStateToProps = (state) => ({
  userState: state.user,
  watchlist: state.watchlist.watchlist,
  activeWatchlist: state.watchlist.activeWatchlist,
  portfolio: state.portfolio.portfolio,
});

export default connect(mapStateToProps, {
  setActiveWatchlist,
  updateWatchlist,
  updatePortfolio,
})(StockTabScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
});
