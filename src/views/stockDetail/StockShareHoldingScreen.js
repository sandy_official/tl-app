import React from "react";
import { View, Text, StyleSheet, Image, ScrollView, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import DetailedHolding from "../../components/stockDetail/shareHolding/DetailedHolding";
import ShareHoldingSummary from "../../components/stockDetail/shareHolding/ShareHoldingSummary";
import { BAR_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";
import ChartView from "react-native-highcharts";
import { getStockShareHolding } from "../../controllers/StocksController";
import LoadingView from "../../components/common/LoadingView";
import StockHoldingHistoryBSheet from "../../components/common/StockHoldingHistoryBSheet";
import { connect } from "react-redux";
import { numberWithCommas, roundNumber } from "../../utils/Utils";
import NoMatchView from "../../components/common/NoMatchView";
import EmptyView from "../../components/common/EmptyView";

class StockShareHoldingScreen extends React.PureComponent {
  holdingRef = null;
  chartConfigs = {};

  componentDidMount() {
    this.props.isSelected && this.getData();
  }

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      summary: [],
      institutional: [],
      promoter: [],
      superstar: [],
      chart: {},
      insights: {},
      dateList: [],
      stock: {},
      quarterString: null,
      holdingId: "",
      quarterName: "",
      showEmptyState: false,
    };
  }

  getData = () => {
    const { quarterString, showEmptyState } = this.state;
    const { stockId, userState, baseURL, appToken } = this.props;

    this.setState({ refreshing: true });

    getStockShareHolding(
      baseURL,
      appToken,
      userState.token,
      stockId,
      quarterString,
      (data) => {
        console.log("Shareholding screen", data);
        this.setState(Object.assign({ refreshing: false }, data));

        if (showEmptyState) {
          this.setState({ showEmptyState: false });
        }
      },
      (status, message) => {
        if (status == 1) {
          this.setState({ showEmptyState: true });
        }
        this.setState({ refreshing: false });
      }
    );
  };

  setQuarter = (quarter) => {
    if (Array.isArray(quarter))
      this.setState({ quarterString: quarter[1], quarterName: quarter[0] }, () => this.getData());
  };

  openSuperstar = (obj) => this.props.navigation.navigate("SuperstarInfo", obj);

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState(
        { currentPosition: viewableItems[0].index },
        () => this.holdingRef && this.holdingRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  showShareholdingHistory = (obj) => {
    console.log("hId", obj);
    this.setState({ holdingId: obj.holdingId }, () => {
      this.refs.StockHoldingHistory.openDialog();
    });
  };

  renderPageIndicator = () => {
    const { currentPosition, chart } = this.state;

    return (
      <View style={styles.pageIndicator}>
        {Object.keys(chart).map((key, i) => {
          const pageIndicatorStyle = currentPosition == i ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

          return <View style={pageIndicatorStyle} key={key} />;
        })}
      </View>
    );
  };

  getBarChartConfigs = (key) => {
    const { chart } = this.state;

    if (!this.chartConfigs[key]) {
      const data = chart[key] ? chart[key] : [];
      const showPledgeChart = data[0] ? data[0].length > 3 : false;
      let config = JSON.parse(JSON.stringify(BAR_CHART_CONFIG));

      console.log("shareholding key ", key, data);
      let categories = [];
      let series1 = [];
      let series2 = [];

      data.reverse().forEach((item, index) => {
        if (index < data.length - 1) {
          let holding = item[1];
          let formattedHolding = !isNaN(holding) ? parseFloat(numberWithCommas(holding)) : holding;
          let quarter = typeof item[0] == "string" ? item[0].split(" ") : "";
          let formattedQuarter = quarter[0] && quarter[1] ? `${quarter[0].toUpperCase()} ' ${quarter[1].slice(2)}` : "";
          categories.push(formattedQuarter);

          series1.push(formattedHolding);
          if (showPledgeChart) {
            // let formattedValue = !isNaN(item[3]) ? numberWithCommas(item[3], true, true) : item[1];
            series2.push(item[3]);
          }
        }
      });

      config.xAxis[0].categories = categories;
      config.xAxis[0].labels.x = 15;
      config.series[0].data = series1;
      config.series[0].color = colors.lightNavy;
      config.plotOptions.series.pointWidth = null;
      // config.plotOptions.series.pointWidth = wp(8);

      if (showPledgeChart) {
        config.series.push({ data: series2, color: colors.red, showInLegend: false });

        config.xAxis.push({ categories: series1, linkedTo: 0, opposite: true });
        config.xAxis[2].labels = {
          formatter: function () {
            return `<span style="color:#174078;font-size:12px;font-weight:500">${this.value}%</span>`;
          },
        };
        config.xAxis[1].categories = series2;
        config.xAxis[1].labels = {
          formatter: function () {
            return `<span style="color:#e3352b;font-size:12px;font-weight:500">${this.value}%</span>`;
          },
        };
        // config.plotOptions.column.groupPadding = 0.;
        config.plotOptions.series.pointPadding = 0;
      } else {
        config.xAxis[1].categories = series1;
        config.xAxis[1].labels = {
          formatter: function () {
            return `<span style="color:#174078;font-size:12px;font-weight:500">${this.value}%</span>`;
          },
        };
      }
      config.xAxis[0].labels.align = "right";
      config.plotOptions.column.stacking = "";
      config.plotOptions.series.borderWidth = 0;

      console.log("chart config", config);
      this.chartConfigs[key] = config;
      return config;
    } else return this.chartConfigs[key];
  };

  renderChartHeader = (key) => {
    const data = this.state.chart[key] ? this.state.chart[key] : [];
    const showPledgeChart = data[0] ? data[0].length > 3 : false;

    const blueBlockStyle = [styles.chartColorBlock, styles.blueBackground];
    const blueBlockLabelStyle = [styles.chartColorBlockLabel, styles.rightExtraMargin];

    return (
      <View style={styles.cardTitleRow}>
        <Text style={styles.barChartLabel}>{key + " Holding"}</Text>

        {key == "Promoter" ? (
          <View style={styles.chartIndicatorRow}>
            <View style={blueBlockStyle} />
            <Text style={blueBlockLabelStyle}>Promoter</Text>

            {showPledgeChart && (
              <View style={{ flexDirection: "row" }}>
                <View style={[styles.chartColorBlock, styles.redBackground]} />
                <Text style={styles.chartColorBlockLabel}>Pledges</Text>
              </View>
            )}
          </View>
        ) : (
          <View style={styles.chartIndicatorRow}>
            <View style={[styles.chartColorBlock, styles.blueBackground]} />
            <Text style={styles.chartColorBlockLabel}>Institutional</Text>
          </View>
        )}
      </View>
    );
  };

  renderSeriesRows = (key) => {
    const data = this.state.chart[key] ? this.state.chart[key] : [];
    const showPledgeChart = data[0] ? data[0].length > 3 : false;
    let series1 = [];
    let series2 = [];

    this.state.chart[key].map((item, index) => {
      if (index > 0) {
        const value1 = !isNaN(item[1]) ? roundNumber(item[1]) : item[1];
        series1.push(value1);

        if (showPledgeChart) {
          const value2 = !isNaN(item[3]) ? roundNumber(item[3]) : item[3];
          series2.push(value2);
        }
      }
    });

    const blueLabelStyle = [styles.barChartTopLabel, styles.blueText];
    const redLabelStyle = [styles.barChartTopLabel, styles.redText];

    return (
      <View>
        <View style={styles.barChartTopLabelRow}>
          {series1.reverse().map((item, i) => (
            <Text style={blueLabelStyle} key={i.toString()}>
              {item}
            </Text>
          ))}
        </View>

        <View style={styles.barChartTopLabelRow}>
          {showPledgeChart &&
            series2.reverse().map((item, i) => (
              <Text style={redLabelStyle} key={i.toString()}>
                {item}
              </Text>
            ))}
        </View>
      </View>
    );
  };

  getInsightIcon = (value) => {
    if (value == "positive") return require("../../assets/icons/arrow-green.png");
    else if (value == "negative") return require("../../assets/icons/arrow-red.png");
    else return require("../../assets/icons/arrow-yellow.png");
  };

  renderInsights = (key) => {
    const insights = this.state.insights[key];
    const data = this.state.chart[key] ? this.state.chart[key] : [];
    const showPledgeChart = data[0] ? data[0].length > 3 : false;
    let series1 = [];
    let series2 = [];

    const insightLabelStyle = [styles.barChartLabel, styles.insightLabel];

    this.state.chart[key].map((item, index) => {
      if (index > 0) {
        series1.push(item[1]);
        if (showPledgeChart) series2.push(item[3]);
      }
    });

    if (insights && insights.length > 0) {
      return (
        <View>
          <Text style={insightLabelStyle}>Insights</Text>
          {insights.map((item, index) => {
            const insightStyle =
              this.state.insights.length - 1 == index ? [styles.insightRow, styles.divider] : styles.insightRow;

            return (
              <View style={insightStyle} key={index.toString()}>
                <Image source={this.getInsightIcon(item[2])} style={styles.insightIcon} />
                <Text style={styles.insight} numberOfLines={3}>
                  {item[1]}
                </Text>
              </View>
            );
          })}
        </View>
      );
    }
  };

  renderBarCharts = ({ item, index }) => {
    const { chart } = this.state;

    if (chart[item].length > 0)
      return (
        <View style={[styles.chartCard, styles.barChartCard]}>
          {this.renderChartHeader(item)}
          {/*this.renderSeriesRows(item)*/}

          <ChartView
            style={[styles.chartContainer, styles.barChartContainer]}
            config={this.getBarChartConfigs(item)}
            options={CHART_OPTIONS}
          />

          {this.renderInsights(item)}
        </View>
      );
  };

  renderBarChartList = () => {
    const { chart, insights } = this.state;

    const chartKeys = Object.keys(chart);
    const showCharts = chartKeys.length > 0 || Object.keys(insights).length > 0;
    const showPageIndicators = chart[chartKeys[1]] ? chart[chartKeys[1]].length > 0 : false;

    if (showCharts) {
      return (
        <View style={{ backgroundColor: colors.whiteTwo }}>
          <View style={styles.barChartTitleRow}>
            <Text style={styles.heading}>HOLDING PATTERN</Text>
            {showPageIndicators && this.renderPageIndicator()}
          </View>

          <FlatList
            ref={(ref) => (holdingRef = ref)}
            data={Object.keys(chart)}
            extraData={this.state}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.barChartList}
            renderItem={this.renderBarCharts}
            keyExtractor={(item, index) => index.toString()}
            removeClippedSubviews={false}
            onViewableItemsChanged={this._onViewableItemsChanged}
            viewabilityConfig={this._viewabilityConfig}
          />
        </View>
      );
    }
  };

  render() {
    const { isSelected } = this.props;
    const {
      refreshing,
      institutional,
      promoter,
      superstar,
      summary,
      dateList,
      quarterString,
      quarterName,
      holdingId,
      stock,
      showEmptyState,
    } = this.state;

    const showSummary = summary.length > 0;
    const showHolding = superstar.length > 0 || promoter.length > 0 || institutional.length > 0;

    if (isSelected) {
      return (
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            {showSummary && (
              <ShareHoldingSummary
                summary={summary}
                dates={dateList}
                quarter={quarterString}
                quarterName={quarterName}
                onQuarterChange={(quarter) => this.setQuarter(quarter)}
                showHistory={this.showShareholdingHistory}
                hide={showEmptyState}
              />
            )}

            {!showEmptyState && this.renderBarChartList()}

            {!showEmptyState && showHolding && (
              <DetailedHolding
                {...this.props}
                superstar={superstar}
                promoter={promoter}
                institutional={institutional}
                showHistory={this.showShareholdingHistory}
                openSuperstar={this.openSuperstar}
              />
            )}

            {<View style={styles.extraSpacing} />}
          </ScrollView>

          <StockHoldingHistoryBSheet
            ref="StockHoldingHistory"
            type="stock"
            stock={stock}
            holdingId={holdingId}
            // data={dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          {showEmptyState && (
            <EmptyView
              style={{ top: hp(18) }}
              heading="No Data Available"
              imageSrc={require("../../assets/icons/no-match-grey.png")}
            />
          )}
          {refreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(StockShareHoldingScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    flex: 1,
    marginLeft: wp(3),
    marginTop: hp(4),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  chartCard: {
    paddingHorizontal: wp(3),
    paddingTop: wp(3),
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  chartContainer: {
    height: hp(28),
    overflow: "hidden",
  },
  barChartCard: {
    width: wp(90),
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
  },
  barChartContainer: {
    // width: wp(82)
  },
  barChartList: {
    paddingHorizontal: wp(1.5),
    // paddingVertical: wp(1)
  },
  pageIndicator: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: hp(4),
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  cardTitleRow: {
    // flex: 1,
    flexDirection: "row",
    marginBottom: wp(6),
    alignItems: "center",
  },
  barChartTitleRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: hp(2),
    marginRight: wp(3),
  },
  barChartLabel: {
    flex: 1,
    textAlign: "left",
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  chartIndicatorRow: {
    flexDirection: "row",
    alignItems: "center",
  },
  chartColorBlock: {
    width: wp(3.5),
    height: wp(3.5),
    borderRadius: 3,
  },
  chartColorBlockLabel: {
    fontSize: RF(1.9),
    paddingLeft: 5,
    color: colors.black,
  },
  blueBackground: {
    backgroundColor: colors.primary_color,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  rightExtraMargin: {
    marginRight: wp(3),
  },
  insightLabel: {
    marginTop: hp(2),
  },
  insightRow: {
    flexDirection: "row",
    paddingVertical: wp(3),
    marginHorizontal: -wp(3),
    alignItems: "center",
    borderBottomColor: colors.whiteTwo,
    borderBottomWidth: 1,
  },
  insightIcon: {
    width: wp(8),
    height: wp(3),
    resizeMode: "contain",
    marginHorizontal: wp(2),
  },
  insight: {
    flex: 1,
    fontSize: RF(2.2),
    marginRight: wp(3),
    color: colors.black,
  },
  extraSpacing: {
    paddingBottom: ifIphoneX ? hp(3) : h[1],
  },
  divider: {
    borderBottomColor: colors.whiteTwo,
    borderBottomWidth: 1,
  },
  barChartTopLabelRow: {
    flexDirection: "row",
    marginLeft: wp(8.5),
    marginRight: wp(6.5),
    marginBottom: 5,
  },
  barChartTopLabel: {
    flex: 1,
    fontSize: RF(1.6),
    fontWeight: "300",
    textAlign: "center",
    marginLeft: wp(2),
  },
  blueText: {
    color: colors.lightNavy,
  },
  redText: {
    color: colors.red,
  },
});
