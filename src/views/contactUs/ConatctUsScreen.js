/**
 * Libraries
 */
import React from "react";
import { View, Text, Image, StyleSheet, TextInput, TouchableOpacity, Platform } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";

/**
 * Custom components, methods and constants
 */
import { Toolbar } from "../../components/Toolbar";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { isValidEmail } from "../../utils/Utils";
import { getContactUsForm, setContactUsForm } from "../../controllers/ContactUsController";
import LoadingView from "../../components/common/LoadingView";

/**
 * @constant config Static config to generate contact us form fields
 */
const forms = [
  { label: "Full Name", mandatory: true, key: "Name", numberOfLines: 1 },
  { label: "Email ID", mandatory: true, key: "Email", numberOfLines: 1 },
  {
    label: "Contact Number",
    mandatory: false,
    key: "Contact Number",
    inputType: "number-pad",
    numberOfLines: 1,
    maxLength: 10,
  },
  /*{
    label: "Subject",
    mandatory: true,
    key: "Subject",
    // numberOfLines: 2,
    multiline: true
  },*/
  { label: "Message", placeholder: "Explain your concern", mandatory: true, key: "Message", multiline: true },
];

/**
 * Contact us screen - contact trendlyne by submitting queries
 */
class ContactUsScreen extends React.PureComponent {
  /**
   * Call API to get user information for form
   */
  componentDidMount() {
    this.getData();
  }

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = {
      Name: "",
      Email: "",
      "Contact Number": "",
      //Subject: "",
      Message: "",
      isSubmitDisabled: true,
      isRefreshing: false,
    };
  }

  /**
   * Call api to get user information for form
   */
  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;

    if (isGuestUser) return;
    this.setState({ isRefreshing: true });

    getContactUsForm(baseURL, appToken, token, (data) => {
      this.setState(Object.assign({}, data, { isRefreshing: false }));
    });
  };

  /**
   * Validate contact us form fields
   * @returns {boolean} if valid return true else false
   */
  isValidForm = () => {
    const { Name, "Contact Number": contact, Message, Email } = this.state;

    if (Name.length == 0) this.refs.toast.show("Please Enter Name");
    //else if (Subject.length == 0) this.refs.toast.show("Please Enter Subject");
    else if (Message.length == 0) this.refs.toast.show("Please Enter Message");
    else if (Email.length == 0 && !isValidEmail(Email)) this.refs.toast.show("Please Enter Valid Email");
    else if (contact.length > 0) {
      if (isNaN(contact) || contact.length != 10) this.refs.toast.show("Please Enter Valid Contact Number");
      else return true;
    } else return true;

    return false;
  };

  /**
   * Submit contact us form
   */
  setData = () => {
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;
    const { Name, "Contact Number": contact, Message, Email } = this.state;

    if (this.isValidForm()) {
      this.setState({ isRefreshing: true });

      /**
       * Call API function to send form data
       * on success reset the form
       */
      setContactUsForm(baseURL, appToken, token, Name, Email, contact, Message, (isSuccess, response) => {
        this.setState({ isRefreshing: false });
        this.refs.toast.show(response);

        if (isSuccess)
          this.setState({
            // Name: "",
            "Contact Number": "",
            //Subject: "",
            Message: "",
          });
      });
    }
  };

  /**
   * Change submit button state to enabled/disabled based on text input fields validation
   */
  setSubmitBtnState = () => {
    const { Name, Email, Message, isSubmitDisabled } = this.state;

    if (Name.length > 0 && Email.length > 0 && Message.length > 0) {
      this.setState({ isSubmitDisabled: false });
    } else this.setState({ isSubmitDisabled: true });
  };

  /**
   * Set value of text input fields to this.state
   */
  setInputText = (key, value) => {
    this.setState({ [key]: value }, this.setSubmitBtnState);
  };

  /**
   * Render form submit button
   * @returns {view} submit button
   */
  renderSubmit = () => {
    const { isSubmitDisabled } = this.state;

    const btnStyle = isSubmitDisabled ? [styles.submitBtn, styles.submitDisabled] : styles.submitBtn;

    return (
      <TouchableOpacity style={btnStyle} disabled={isSubmitDisabled} onPress={this.setData}>
        <Text style={styles.submitBtnText}>Submit</Text>
      </TouchableOpacity>
    );
  };

  /**
   * Render text input field
   * @param {object} config text field config object
   * @returns {view} text input field
   */
  renderTextInputBox = (item) => {
    const placeholder = item.placeholder ? item.placeholder : "";
    const inputType = item.inputType ? item.inputType : "default";

    return (
      <View style={styles.inputContainer} key={item.key}>
        <Text style={styles.inputLabel}>
          {item.label}
          {item.mandatory && <Text style={styles.redText}> *</Text>}
        </Text>
        <TextInput
          style={styles.textInput}
          // editable={item.editable}
          keyboardType={inputType}
          placeholder={placeholder}
          maxLength={item.maxLength}
          numberOfLines={item.numberOfLines}
          multiline={item.multiline}
          value={this.state[item.key]}
          onChangeText={(value) => this.setInputText(item.key, value)}
          //   onFocus={() => this.toggleTextInputFocus(label)}
          //   onBlur={() => this.toggleTextInputFocus(null)}
        />
      </View>
    );
  };

  /**
   * Render contact us form with text input fields from form config data
   * @returns {view} contact us form
   */
  renderForm = () => {
    return (
      <View style={styles.form}>
        <Text style={styles.formTitle}>Send us your questions/feedback</Text>
        {forms.map((item) => this.renderTextInputBox(item))}
      </View>
    );
  };

  /**
   * Render form header details view
   * @returns {view} form header
   */
  renderHeader = () => {
    return (
      <View style={styles.contactUsContainer}>
        <View style={styles.contactUsRow}>
          <View style={styles.contactMessageContainer}>
            <Text style={styles.contactUsMessage}>{"WE’D LOVE TO \nHEAR FROM YOU"}</Text>
            <Text style={styles.emailUs}>Email us on</Text>
            <Text style={styles.contactEmail}>contact@trendlyne.com</Text>
          </View>

          <View style={styles.contactUsIconContainer}>
            <Image source={require("../../assets/icons/contact-us.png")} style={styles.contactUsIcon} />
          </View>
        </View>
        <Text style={styles.or}>OR</Text>
      </View>
    );
  };

  /**
   * Render
   */
  render() {
    const { isRefreshing } = this.state;

    return (
      <View style={styles.container}>
        <Toolbar title="Contact Us" backButton {...this.props} />

        <KeyboardAwareScrollView>
          {this.renderHeader()}
          {this.renderForm()}
          {this.renderSubmit()}
        </KeyboardAwareScrollView>
        {isRefreshing && <LoadingView />}

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ContactUsScreen);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  contactUsContainer: {
    backgroundColor: colors.whiteTwo,
    paddingBottom: wp(3),
  },
  contactUsRow: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginLeft: wp(7),
    marginRight: wp(7),
    marginTop: wp(7),
  },
  contactMessageContainer: {
    flex: 1,
    // alignItems: "center",
    justifyContent: "center",
    marginRight: wp(6),
  },
  contactUsIconContainer: {
    // flex: 1,
    alignItems: "flex-end",
    justifyContent: "center",
  },
  contactUsIcon: {
    width: wp(25),
    height: wp(25),
    resizeMode: "contain",
  },
  contactUsMessage: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  emailUs: {
    fontSize: RF(2.2),
    letterSpacing: 0,
    color: colors.black,
    marginTop: wp(2.5),
  },
  contactEmail: {
    fontSize: RF(2.2),
    fontWeight: "500",
    letterSpacing: 0,
    color: colors.primary_color,
  },
  or: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
    textAlign: "center",
    marginTop: wp(3),
  },
  formTitle: {
    fontSize: RF(2.2),
    color: colors.black,
  },
  form: {
    marginHorizontal: wp(7),
    marginTop: wp(6),
  },
  formTitle: {
    fontSize: RF(2.2),
    color: colors.black,
  },
  inputContainer: {
    marginTop: wp(6),
    paddingBottom: wp(1.5),
    // marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    // flex: 1,
    fontSize: RF(2.2),
    color: colors.black,
    paddingTop: 6,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  inputLabel: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  redText: {
    color: colors.red,
  },
  submitBtn: {
    flex: 1,
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    margin: wp(7),
  },
  submitBtnText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: 11,
  },
  submitDisabled: {
    backgroundColor: colors.steelGrey,
  },
});
