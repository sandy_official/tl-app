import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, FlatList, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { BAR_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";
import ChartView from "react-native-highcharts";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import ResearchReportCard from "../../components/researchReport/ResearchReportCard";
import { getResearchReportsByType } from "../../controllers/ReportsController";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import ReportsInfoModal from "../../components/researchReport/ReportsInfoModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class ResearchReportScreen extends React.PureComponent {
  chartConfigs = {};

  state = {
    selectedOption: "New",
    isRefreshing: false,
    reports: [],
    chartData: {},
    isFirstCallComplete: false,
    pageNumber: 0,
    perPageCount: 20,
    isNextPage: true,
    selectedReport: {},
    currentPosition: 0,
    isLoadingMore: false,
    showScrollToTop: false,
  };

  componentDidMount = () => {
    this.getData();
  };

  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const {
      isNextPage,
      pageNumber,
      selectedOption,
      isRefreshing,
      reports,
      perPageCount,
      isFirstCallComplete,
    } = this.state;
    const rType = selectedOption.toLowerCase();

    if (isNextPage && !isRefreshing) {
      getResearchReportsByType(baseURL, appToken, token, rType, pageNumber, perPageCount, (data) => {
        const reportsList = pageNumber == 0 ? data.reports : reports.concat(data.reports);

        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        this.setState({ isRefreshing: false, isLoadingMore: false });
        this.setState({ chartData: data.charts, pageNumber: ++data.pageNumber, isNextPage: data.isNextPage });
        this.setState({ reports: reportsList, pageNumber: ++data.pageNumber, isNextPage: data.isNextPage });
        console.log("report", reportsList);
      });
    } else {
      this.setState({ isRefreshing: false, isLoadingMore: false });
    }
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  onLoadMore = () => {
    const { isLoadingMore } = this.state;
    if (!isLoadingMore) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  onRefresh = () => {
    this.setState({ isRefreshing: true, isNextPage: true, pageNumber: 0 }, () => this.getData());
  };

  renderSeriesRows = (data) => {
    let series = [];

    data.map((item, index) => {
      if (index > 0) {
        const value = item[1];
        // const value1 = !isNaN(item[0]) ? item[0].toFixed(1) : item[0];
        series.push(value);
      }
    });

    return (
      <View style={styles.barChartTopLabelRow}>
        {series.map((item, i) => (
          <Text style={[styles.barChartTopLabel, styles.blueText]} key={i.toString()}>
            {item}
          </Text>
        ))}
      </View>
    );
  };

  renderToolbar = () => {
    const { navigate } = this.props.navigation;

    const infoIcon = require("../../assets/icons/info-white.png");
    const searchIcon = require("../../assets/icons/search-white.png");

    return (
      <Toolbar title="Research Reports" drawerMenu {...this.props}>
        <ImageButton source={infoIcon} onPress={() => this.refs.infoScreen.RBSheet.open()} />
        <ImageButton source={searchIcon} onPress={() => navigate("ReportSearch")} />
      </Toolbar>
    );
  };

  renderPageIndicator = () => {
    const { chartData, currentPosition } = this.state;

    return (
      <View style={styles.pageIndicator}>
        {Object.keys(chartData).map((key, i) => {
          const indicatorStyle = currentPosition == i ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
          return <View style={indicatorStyle} key={key} />;
        })}
      </View>
    );
  };

  getBarChartConfigs = (data, key) => {
    if (!this.chartConfigs[key]) {
      console.log("key - creating existing", key);
      let categories = [];
      let reportCount = [];
      let config = JSON.parse(JSON.stringify(BAR_CHART_CONFIG));

      data.forEach((item, i) => {
        if (i !== 0)
          if (item[0] == "Stock") {
            reportCount.push(item[0]);
            categories.push(item[1]);
          } else {
            categories.push(item[0]);
            reportCount.push(item[1]);
          }
      });

      config.xAxis[0].categories = categories;
      config.xAxis[1].categories = reportCount;
      config.series[0].data = reportCount;
      config.series[0].color = colors.primary_color;
      config.plotOptions.series.pointWidth = wp(4);
      config.yAxis.tickInterval = 1;
      config.xAxis[1].labels = {
        formatter: function () {
          return `<span style="color:#174078;font-size:12px;font-weight:500">${this.value}</span>`;
        },
      };

      this.chartConfigs[key] = config;
      return config;
    } else return this.chartConfigs[key];
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState({ currentPosition: viewableItems[0].index });
      // () => this.chartListRef && this.chartListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  onReportOptionSelect = (option) => {
    if (this.state.selectedOption != option)
      this.setState({ selectedOption: option, isNextPage: true, pageNumber: 0, isFirstCallComplete: false }, () =>
        this.getData()
      );
  };

  renderOptionButton = (option) => {
    const { selectedOption } = this.state;

    const optionButtonStyle = selectedOption == option ? styles.optionButtonActive : styles.optionButton;
    const optionTextStyle = selectedOption == option ? styles.optionButtonTextActive : styles.optionButtonText;

    return (
      <TouchableOpacity onPress={() => this.onReportOptionSelect(option)}>
        <View style={optionButtonStyle}>
          <Text style={optionTextStyle}>{option + " "}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderOptionsRow = () => {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={styles.optionButtonsContainer}>
          {this.renderOptionButton("New")}
          {this.renderOptionButton("Buy")}
          {this.renderOptionButton("Sell")}
          {this.renderOptionButton("Upside")}
          {this.renderOptionButton("Upgrades")}
          {this.renderOptionButton("Downgrades")}
          {this.renderOptionButton("IPO")}
        </View>
      </ScrollView>
    );
  };

  renderBarCharts = ({ item, index }) => {
    const { chartData } = this.state;
    const data = this.state.chartData[item];
    const cardTitle = item;
    const showCharts = Object.keys(chartData).length > 0;

    if (showCharts) {
      return (
        <View style={[styles.chartCard, styles.barChartCard]}>
          <View style={styles.cardTitleRow}>
            <Text style={styles.barChartLabel}>{cardTitle}</Text>

            <View style={styles.chartIndicatorRow}>
              <View style={[styles.chartColorBlock, styles.blueBackground]} />
              <Text style={[styles.chartColorBlockLabel, styles.rightExtraMargin]}>No. of Reports</Text>
            </View>
          </View>

          {/* {this.renderSeriesRows(data)} */}

          <ChartView
            style={[styles.chartContainer, styles.barChartContainer]}
            config={this.getBarChartConfigs(data, item)}
            options={CHART_OPTIONS}
          />
        </View>
      );
    }
  };

  renderBarChartList = () => {
    const { chartData } = this.state;

    return (
      <View>
        <View style={styles.barChartTitleRow}>
          <Text style={styles.heading}>ACTIVELY RESEARCHED STOCKS</Text>
          {this.renderPageIndicator()}
        </View>

        <FlatList
          ref={(ref) => (this.chartListRef = ref)}
          data={Object.keys(chartData)}
          extraData={chartData}
          horizontal
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={styles.barChartList}
          renderItem={this.renderBarCharts}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
          // removeClippedSubviews={false}
        />
      </View>
    );
  };

  showWatchlist = (report) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (report) => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = (report) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderStockListHeader = () => {
    return (
      <View>
        <Text style={styles.filterBy}>FILTER REPORTS BY</Text>
        {this.renderOptionsRow()}
      </View>
    );
  };

  renderStockItem = ({ item, index }) => {
    const { activeWatchlist, userState } = this.props;

    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <ResearchReportCard
        {...this.props}
        data={item}
        position={index}
        showAlerts={this.showAlerts}
        showWatchlist={this.showWatchlist}
        watchlistBtnState={isWatchlistActive}
        showPortfolio={this.showPortfolio}
        isGuestUser={userState.isGuestUser}
        showLogin={() => this.refs.loginModal.open()}
      />
    );
  };

  renderFooter = () => {
    const { isNextPage, isLoadingMore } = this.state;
    if (isLoadingMore) return <LoadingMoreView />;
    else if (isNextPage) {
      return (
        <TouchableOpacity onPress={this.onLoadMore}>
          <View style={styles.footerButton}>
            <Text style={styles.footerButtonText}>
              Load More
              {/* (126 remaining) */}
            </Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.listEndFooter}>
          <Text style={styles.listEndFooterText}>That's all folks!</Text>
        </View>
      );
    }
  };

  renderStockList = () => {
    const { reports, isLoadingMore } = this.state;
    const { watchlist, activeWatchlist, portfolio } = this.state;
    const extraData = { watchlist, activeWatchlist, portfolio, reports };

    return (
      <FlatList
        data={reports}
        extraData={extraData}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.extraSpacing}
        renderItem={this.renderStockItem}
        keyExtractor={(item, index) => index.toString()}
        // ListHeaderComponent={this.renderStockListHeader}
        ListFooterComponent={this.renderFooter}
        removeClippedSubviews={false}
        initialNumToRender={3}
        maxToRenderPerBatch={20}
        // onEndReached={() => this.getData()}
        // onEndReached={this.onLoadMore}
        // onEndReachedThreshold={0.7}
      />
    );
  };

  renderRefreshControl = () => {
    const { isFirstCallComplete, isRefreshing } = this.state;
    return <RefreshControl refreshing={isFirstCallComplete && isRefreshing} onRefresh={this.onRefresh} />;
  };

  render() {
    const { isFirstCallComplete, selectedReport, chartData, showScrollToTop } = this.state;
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;

    const showCharts = Object.keys(chartData).length > 0;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <ScrollView
          ref={(ref) => (this.listRef = ref)}
          nestedScrollEnabled
          onScroll={this.handleScroll}
          refreshControl={this.renderRefreshControl()}
        >
          {showCharts && this.renderBarChartList()}
          {showCharts && this.renderStockListHeader()}
          {isFirstCallComplete && this.renderStockList()}
        </ScrollView>

        <WatchlistModal
          ref="Watchlists"
          {...this.props}
          stock={selectedReport}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={(obj) => setActiveWatchlist(obj)}
          createWatchList={() => this.refs.createWatchlist.modal.open()}
        />

        <CreateWatchlistModal
          ref="createWatchlist"
          {...this.props}
          stock={selectedReport}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={this.onWatchlistCreated}
          openWatchlist={() => this.refs.Watchlists.open()}
        />

        <StockAlertsModal
          ref="stockAlerts"
          {...this.props}
          stock={selectedReport}
          // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
          // data={dateList}
          // onPress={quarter => this.setQuarterString(quarter)}
        />

        <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={selectedReport}
          portfolio={portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        />

        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={selectedReport}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        <LoginModal ref="loginModal" {...this.props} />
        <ReportsInfoModal ref="infoScreen" />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        {!isFirstCallComplete && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(mapStateToProps, {
  setActiveWatchlist,
  updateWatchlist,
  updatePortfolio,
  loadPortfolio,
})(ResearchReportScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    flex: 1,
    marginLeft: wp(3),
    marginTop: hp(3),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  filterBy: {
    flex: 1,
    marginLeft: wp(3),
    marginTop: hp(4) - wp(1),
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  chartCard: {
    overflow: "hidden",
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
    paddingTop: wp(3),
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 6,
  },
  chartContainer: {
    height: hp(28),
  },
  barChartCard: {
    width: wp(88),
    marginHorizontal: wp(1.5),
    marginTop: wp(1),
    marginBottom: wp(2),
  },
  barChartContainer: {
    overflow: "hidden",
    width: wp(82),
  },
  barChartList: {
    paddingHorizontal: wp(1.5),
  },
  pageIndicator: {
    flexDirection: "row",
    alignItems: "center",
    marginTop: hp(3),
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  cardTitleRow: {
    flex: 1,
    flexDirection: "row",
    marginBottom: wp(6),
    alignItems: "center",
  },
  barChartTitleRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    marginBottom: hp(2),
    marginRight: wp(3),
  },
  barChartLabel: {
    flex: 1,
    textAlign: "left",
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.black,
  },
  chartIndicatorRow: {
    flexDirection: "row",
    alignItems: "center",
  },
  chartColorBlock: {
    width: wp(3.5),
    height: wp(3.5),
    borderRadius: 3,
  },
  chartColorBlockLabel: {
    fontSize: RF(1.9),
    paddingLeft: wp(1),
    fontWeight: "300",
    color: colors.black,
  },
  blueBackground: {
    backgroundColor: colors.primary_color,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  rightExtraMargin: {
    marginRight: wp(3),
  },
  extraSpacing: {
    paddingBottom: ifIphoneX ? hp(3) : h[1],
  },
  divider: {
    borderBottomColor: colors.whiteTwo,
    borderBottomWidth: 1,
  },
  optionButton: {
    minWidth: wp(20),
    padding: wp(2.5),
    marginRight: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  optionButtonText: {
    fontSize: RF(2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  optionButtonActive: {
    minWidth: wp(20),
    padding: wp(2.5),
    marginRight: wp(3),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  optionButtonTextActive: {
    fontSize: RF(2),
    fontWeight: "300",
    color: colors.white,
  },
  optionButtonsContainer: {
    flex: 1,
    flexDirection: "row",
    // marginTop: wp(3),
    marginVertical: wp(1.5),
    alignItems: "center",
    padding: wp(2.5),
  },
  barChartTopLabelRow: {
    flexDirection: "row",
    marginLeft: wp(8.5),
    marginRight: wp(4.0),
    marginBottom: 6,
  },
  barChartTopLabel: {
    flex: 1,
    fontSize: RF(1.5),
    fontWeight: "500",
    textAlign: "center",
    marginLeft: wp(2),
    color: colors.primary_color,
  },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
    marginHorizontal: "3%",
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: wp(3),
    color: colors.primary_color,
  },
  listEndFooter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(1),
  },
  listEndFooterText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.pinkishGrey,
  },
});
