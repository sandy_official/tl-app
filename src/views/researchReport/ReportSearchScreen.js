import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import ReportStockItem from "../../components/researchReport/ReportStockItem";
import { Toolbar } from "../../components/Toolbar";
import { getBrokerSearchList, getResearchReportsByType } from "../../controllers/ReportsController";
import LoadingView from "../../components/common/LoadingView";
import { searchStockByTerm } from "../../controllers/StocksController";
import { connect } from "react-redux";
import TabComponent from "../../components/common/TabComponent";
import NoMatchView from "../../components/common/NoMatchView";

class ReportSearchScreen extends React.PureComponent {
  tabs = ["Stocks", "Broker"];

  componentDidMount() {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    this.getPopularStocks();

    setTimeout(() => {
      getBrokerSearchList(baseURL, appToken, token, (brokerList) =>
        this.setState({ brokers: brokerList, isFirstCallComplete: true })
      );
    }, 1000);
  }

  constructor(props) {
    super(props);
    this.state = {
      searchQuery: "",
      navigation: props.navigation,
      brokers: [],
      stocks: [],
      popularStocks: [],
      isFirstCallComplete: false,
      selectedTab: 0,
      refreshing: false,
    };
  }

  getPopularStocks = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { isFirstCallComplete } = this.state;

    this.setState({ refreshing: true });

    getResearchReportsByType(baseURL, appToken, token, "new", 0, 20, (data) => {
      let reports = [];

      if (!isFirstCallComplete) {
        this.setState({ isFirstCallComplete: true });
        this.refs.toolbar ? this.refs.toolbar.focusSearchBar() : null;
      }

      let list = data && "reports" in data && Array.isArray(data.reports) ? data.reports : [];
      let stocks = {};

      for (let i = 0; i < list.length; i++) {
        if (!(list[i].get_full_name in stocks)) {
          stocks[list[i].get_full_name] = 0;
          if (list[i].page_type == "Equity") reports.push(list[i]);
          if (reports.length == 5) break;
        }
      }

      this.setState({ popularStocks: reports, refreshing: false });
    });
  };

  searchStock = (query) => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    if (query.length > 1) {
      this.setState({ refreshing: true });
      searchStockByTerm(baseURL, appToken, token, query, "stock", (StockList) =>
        this.setState({ stocks: StockList, refreshing: false })
      );
    } else this.setState({ stocks: [] });
  };

  searchBroker = () => {
    const { searchQuery, brokers } = this.state;
    if (searchQuery.length > 0) {
      let newBrokerList = [];

      brokers.forEach((item) => {
        let broker = item.toLowerCase();
        if (~broker.indexOf(searchQuery.toLowerCase())) newBrokerList.push(item);
      });
      return newBrokerList;
    } else return brokers;
  };

  openDetailScreen = (brokerName) => {
    this.props.navigation.push("ReportDetail", { broker: brokerName });
  };

  startSearch = (query) => {
    this.setState({ searchQuery: query });
    this.searchStock(query);
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const { selectedTab } = this.state;
    const searchPlaceholder = selectedTab == 0 ? "Search reports by stock" : "Search reports by broker";

    return (
      <Toolbar
        title="Research Reports"
        // backButton
        ref="toolbar"
        {...this.props}
        showSearch
        hideSearch={() => navigation.pop()}
        searchPlaceholder={searchPlaceholder}
        onChangeText={this.startSearch}
      />
    );
  };

  renderListHeader = () => {
    return (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>Most popular searches</Text>
      </View>
    );
  };

  renderBrokerListItem = ({ item, index }) => {
    const extraMargin = index == 0 ? styles.extraTopMargin : null;

    return (
      <TouchableOpacity onPress={() => this.openDetailScreen(item)}>
        <View style={[styles.listItemContainer, extraMargin]}>
          <Text numberOfLines={1} ellipsizeMode="tail" style={styles.listItem}>
            {item}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderStockList = () => {
    const { stocks, refreshing, searchQuery, popularStocks } = this.state;
    const isSearching = searchQuery.length > 0;
    const stockList = isSearching ? stocks : popularStocks.slice(0, 5);
    const showNoResults = searchQuery.length > 1 && stocks.length == 0 && !refreshing;
    let showList = stockList.length > 0;

    // get_full_name;
    return (
      <View style={{ flex: 1 }}>
        {showList && (
          <FlatList
            contentContainerStyle={styles.list}
            data={stockList}
            extraData={this.state}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => <ReportStockItem {...this.props} data={item} />}
            keyExtractor={(item, index) => index.toString()}
            ListHeaderComponent={() => !isSearching && this.renderListHeader()}
            removeClippedSubviews
            initialNumToRender={9}
            maxToRenderPerBatch={5}
            keyboardShouldPersistTaps="always"
          />
        )}

        {showNoResults && this.renderNoResultFound()}
        {refreshing && <LoadingView />}
      </View>
    );
  };

  renderBrokerList = () => {
    const { isFirstCallComplete, searchQuery } = this.state;
    const isSearching = searchQuery.length > 0;
    const brokersList = this.searchBroker();
    const data = isSearching ? brokersList : brokersList.slice(0, 5);
    const showNoResults = isSearching && data.length == 0;

    return (
      <View style={{ flex: 1 }}>
        <FlatList
          contentContainerStyle={styles.list}
          data={data}
          extraData={this.state}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderBrokerListItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={() => (!isSearching ? this.renderListHeader() : null)}
          removeClippedSubviews
          initialNumToRender={9}
          maxToRenderPerBatch={5}
          keyboardShouldPersistTaps="always"
        />

        {showNoResults && this.renderNoResultFound()}
        {!isFirstCallComplete && <LoadingView />}
      </View>
    );
  };

  renderNoResultFound = () => {
    return <NoMatchView message="No Results Found" />;
    // return <Text style={styles.noResultFound}>No Results Found</Text>;
  };

  setTabSelection = (obj) => {
    const { searchQuery } = this.state;
    this.setState({ selectedTab: obj.position });
    // this.searchStock(searchQuery);
  };
  getAnalyticsParams = () => {
    return {};
  };
  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <TabComponent
          analyticsParams={this.getAnalyticsParams}
          tabs={this.tabs}
          initialTab={0}
          onTabSelect={this.setTabSelection}
        >
          <View>{this.renderStockList()}</View>
          <View>{this.renderBrokerList()}</View>
        </TabComponent>
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ReportSearchScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    // paddingTop: wp(2),
    paddingBottom: ifIphoneX ? hp(3) : h[1],
  },
  headerContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(4),
    backgroundColor: colors.whiteTwo,
    // marginBottom: wp(3)
  },
  headerText: {
    opacity: 0.85,
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.pinkishGrey,
    alignItems: "center",
    justifyContent: "center",
  },
  listItemContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(3),
  },
  listItem: {
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.primary_color,
  },
  extraTopMargin: {
    marginTop: wp(2),
  },
  noResultFound: {
    opacity: 0.85,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.greyishBrown,
    paddingHorizontal: wp(3),
    paddingVertical: wp(5),
  },
});
