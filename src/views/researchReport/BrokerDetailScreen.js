import React from "react";
import { View, StyleSheet, FlatList, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import ResearchReportCard from "../../components/researchReport/ResearchReportCard";
import { ICON_CDN } from "../../utils/Constants";
import { getResearchReportByBroker } from "../../controllers/ReportsController";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import ReportsInfoModal from "../../components/researchReport/ReportsInfoModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class BrokerDetailScreen extends React.PureComponent {
  state = {
    showSearch: false,
    selectedOption: "New",
    reports: [],
    isRefreshing: false,
    isFirstCallComplete: false,
    brokerName: "",
    pageNumber: 0,
    perPageCount: 20,
    isNextPage: true,
    selectedReport: {},
    isLoadingMore: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    const brokerName = this.props.navigation.getParam("broker");
    this.setState({ brokerName: brokerName }, () => this.getData());
    console.log("broker name", brokerName);
  }

  getData() {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const {
      brokerName,
      pageNumber,
      perPageCount,
      isNextPage,
      isRefreshing,
      reports,
      isLoadingMore,
      isFirstCallComplete,
    } = this.state;

    if (isNextPage && (isRefreshing || isLoadingMore || !isFirstCallComplete)) {
      getResearchReportByBroker(baseURL, appToken, token, brokerName, pageNumber, perPageCount, (data) => {
        const reportsList = pageNumber == 0 ? data.reports : reports.concat(data.reports);

        if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        this.setState({ isRefreshing: false, isLoadingMore: false });
        this.setState({ reports: reportsList, pageNumber: ++data.pageNumber, isNextPage: data.isNextPage });
      });
    } else {
      this.setState({ isRefreshing: false, isLoadingMore: false });
    }
  }

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    const { isLoadingMore, isNextPage } = this.state;
    if (!isLoadingMore && isNextPage) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  onRefresh = () => {
    this.setState({ isNextPage: true, pageNumber: 0 }, () => this.getData());
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;

    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const { brokerName } = this.state;

    const infoIcon = require("../../assets/icons/info-white.png");
    const searchIcon = require("../../assets/icons/search-white.png");

    return (
      <Toolbar title={brokerName} backButton {...this.props}>
        <ImageButton source={infoIcon} onPress={() => this.refs.infoScreen.RBSheet.open()} />
        <ImageButton source={searchIcon} onPress={() => navigation.push("ReportSearch")} />
      </Toolbar>
    );
  };

  showWatchlist = (report) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (report) => {
    const { isGuestUser } = this.props.userState;
    if (!isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = (report) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedReport: report }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderReportItem = ({ item, index }) => {
    const { activeWatchlist, userState } = this.props;
    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <ResearchReportCard
        {...this.props}
        position={index}
        hideBroker
        data={item}
        showAlerts={this.showAlerts}
        showWatchlist={this.showWatchlist}
        watchlistBtnState={isWatchlistActive}
        showPortfolio={this.showPortfolio}
        isGuestUser={userState.isGuestUser}
        showLogin={() => this.refs.loginModal.open()}
      />
    );
  };

  renderRefreshControl = () => {
    const { isFirstCallComplete, isRefreshing } = this.state;
    return <RefreshControl refreshing={isFirstCallComplete && isRefreshing} onRefresh={this.onRefresh} />;
  };

  renderBrokerList = () => {
    const { reports, isLoadingMore, isRefreshing } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        contentContainerStyle={styles.list}
        data={reports}
        extraData={{ ...this.props, reports }}
        showsVerticalScrollIndicator={false}
        removeClippedSubviews={true}
        renderItem={this.renderReportItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={this.renderRefreshControl()}
        ListFooterComponent={() => isLoadingMore && <LoadingMoreView />}
        onEndReached={this.onLoadMore}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={3}
        maxToRenderPerBatch={5}
        onScroll={this.handleScroll}
      />
    );
  };

  render() {
    const { isFirstCallComplete, selectedReport, showScrollToTop } = this.state;
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        {this.renderBrokerList()}

        <WatchlistModal
          ref="Watchlists"
          {...this.props}
          stock={selectedReport}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={(obj) => setActiveWatchlist(obj)}
          createWatchList={() => this.refs.createWatchlist.modal.open()}
        />

        <CreateWatchlistModal
          ref="createWatchlist"
          {...this.props}
          stock={selectedReport}
          watchlist={watchlist}
          activeWatchlist={activeWatchlist}
          onUpdate={this.onWatchlistCreated}
          openWatchlist={() => this.refs.Watchlists.open()}
        />

        <StockAlertsModal
          ref="stockAlerts"
          {...this.props}
          stock={selectedReport}
          // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
          // data={this.state.dateList}
          // onPress={quarter => this.setQuarterString(quarter)}
        />

        <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={selectedReport}
          portfolio={portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        />

        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={selectedReport}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        <LoginModal ref="loginModal" {...this.props} />
        <ReportsInfoModal ref="infoScreen" />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
        {!isFirstCallComplete && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(mapStateToProps, {
  setActiveWatchlist,
  updateWatchlist,
  updatePortfolio,
  loadPortfolio,
})(BrokerDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingTop: wp(2),
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
  },
});
