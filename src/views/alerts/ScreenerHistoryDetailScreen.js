import React from "react";
import { View, StyleSheet, FlatList, Text, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { colors } from "../../styles/CommonStyles";
import { Toolbar } from "../../components/Toolbar";
import { connect } from "react-redux";
import { getDVMBackgroundColor, mapHeadersData } from "../../utils/Utils";
import RF from "react-native-responsive-fontsize";
import { fetcher } from "../../controllers/Fetcher";
import BaseComponent from "../../components/common/BaseComponent";
import { GET_CUSTOM_PARAMETER_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import TabComponent from "../../components/common/TabComponent";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class ScreenerHistoryDetailScreen extends React.PureComponent {
  state = {
    selectedTab: 2,
    isApiCalled: [false, false, false],
    refreshing: false,
    isFirstCallComplete: false,
    screenPk: "",
    entriesStockPkList: [],
    exitsStockPkList: [],
    activeStockPkList: [],
    entries: [],
    exits: [],
    active: [],
    baseRef: null,
    showScrollToTop: false,
  };

  componentWillMount() {
    this.setInitialSet();
  }

  /**
   * API call to get data
   */
  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { selectedTab, isFirstCallComplete, baseRef } = this.state;

    if (!this.isDataAvailable()) {
      const stockPkList = this.getStockPkList();
      const URL = baseURL + GET_CUSTOM_PARAMETER_URL;
      const columns = ["get_full_name", "d_color", "v_color", "m_color"];
      const body = { stockPkList: stockPkList, columns: columns };

      baseRef && baseRef.showLoading();

      fetcher(
        URL,
        POST_METHOD,
        body,
        baseRef,
        appToken,
        token,
        (json) => {
          const stockHeaders = json.body.tableHeaders;
          const stockData = json.body.tableData;
          const stockList = mapHeadersData(stockHeaders, stockData);

          switch (selectedTab) {
            case 0:
              this.setState({ entries: stockList });
              break;
            case 1:
              this.setState({ exits: stockList });
              break;
            case 2:
              this.setState({ active: stockList });
              break;
          }

          baseRef && baseRef.stopLoading();
          if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        },
        (error) => {
          baseRef && baseRef.stopLoading();
        }
      );
    }
  };

  /**
   * Get navigation params and add them into the state
   */
  setInitialSet = () => {
    const { navigation } = this.props;
    const screenId = navigation.getParam("screenPk", "");
    const entriesStockPkList = navigation.getParam("entries", []);
    const exitsStockPkList = navigation.getParam("exits", []);
    const activeStockPkList = navigation.getParam("active", []);

    console.log("history details", entriesStockPkList, exitsStockPkList, activeStockPkList);

    this.setState(
      {
        screenPk: screenId,
        entriesStockPkList: entriesStockPkList,
        exitsStockPkList: exitsStockPkList,
        activeStockPkList: activeStockPkList,
      },
      () => this.getData() //call api after state update
    );
  };

  gotoStockDetails = (item) => {
    const { navigation } = this.props;
    let pageType = typeof item.page_type == "string" ? item.page_type.toLowerCase() : "";
    const stockCode = item.NSEcode ? item.NSEcode : item.BSEcode;
    if (pageType == "equity" || pageType == "unlisted" || pageType == "index") {
      navigation.push("Stocks", {
        stockId: item.stock_id,
        stockName: item.full_name,
        pageType: item.page_type,
        stockCode: stockCode,
      });
    }
  };

  getStockPkList = () => {
    const { selectedTab, entriesStockPkList, exitsStockPkList, activeStockPkList } = this.state;

    switch (selectedTab) {
      case 0:
        return entriesStockPkList;
      case 1:
        return exitsStockPkList;
      case 2:
        return activeStockPkList;
    }
  };

  isDataAvailable = () => {
    const { selectedTab, entries, exits, active } = this.state;

    switch (selectedTab) {
      case 0:
        return entries.length > 0;
      case 1:
        return exits.length > 0;
      case 2:
        return active.length > 0;
    }
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderDVM = (stock) => {
    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(stock.d_color)} />
        <View style={getDVMBackgroundColor(stock.v_color)} />
        <View style={getDVMBackgroundColor(stock.m_color)} />
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    // console.log("stock item", item);
    return (
      <TouchableOpacity onPress={() => this.gotoStockDetails(item)} style={styles.stockRow}>
        <Text style={styles.stockName}>{item.full_name}</Text>
        {this.renderDVM(item)}
      </TouchableOpacity>
    );
  };

  renderStockList = (stocks) => {
    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={stocks}
        extraData={this.state}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderListItem}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        keyExtractor={(item, index) => index.toString()}
        removeClippedSubviews
        initialNumToRender={12}
        maxToRenderPerBatch={5}
        onScroll={this.handleScroll}
      />
    );
  };

  setTabSelection = (obj) => {
    const tabPosition = obj.position;
    if (tabPosition != this.state.selectedTab) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }

    this.setState({ selectedTab: tabPosition }, () => this.getData());
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const title = navigation.getParam("screenerName");

    return <Toolbar title={title} drawerMenu {...this.props} />;
  };
  getAnalyticsParams = () => {
    const { screenPk } = this.state;
    return {
      screenPk: screenPk,
    };
  };
  renderTabs = () => {
    const { entriesStockPkList, exitsStockPkList, activeStockPkList } = this.state;
    const { entries, exits, active } = this.state;

    const tab1 = "Entries (" + entriesStockPkList.length + ")";
    const tab2 = "Exits (" + exitsStockPkList.length + ")";
    const tab3 = "Active (" + activeStockPkList.length + ")";

    return (
      <TabComponent
        analyticsParams={this.getAnalyticsParams}
        tabs={[tab1, tab2, tab3]}
        initialTab={2}
        onTabSelect={this.setTabSelection}
      >
        <View>{this.renderStockList(entries)}</View>
        <View>{this.renderStockList(exits)}</View>
        <View>{this.renderStockList(active)}</View>
      </TabComponent>
    );
  };

  render() {
    const { showScrollToTop } = this.state;
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <BaseComponent ref={(ref) => this.setState({ baseRef: ref })} {...this.props}>
          {this.renderTabs()}
        </BaseComponent>
        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ScreenerHistoryDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
  stockRow: {
    flexDirection: "row",
    padding: wp(3.2),
    backgroundColor: colors.white,
  },
  stockName: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  row: {
    flexDirection: "row",
  },
});
