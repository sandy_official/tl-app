import React from "react";
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { connect } from "react-redux";
import ScreenerAlerts from "../../components/screeners/ScreenerAlerts";
import { fetcher } from "../../controllers/Fetcher";
import { USER_ACTIVE_ALERTS_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import { mapHeadersData } from "../../utils/Utils";
import EmptyView from "../../components/common/EmptyView";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class AlertScreenerScreen extends React.PureComponent {
  state = {
    screeners: [],
    maxAllowedFreq: "",
    allowedFreqList: [],
    // refreshing: false,
    isFirstCallComplete: false,
    selectedScreener: {},
    screenerName: "",
    // showScrollToTop: false,
  };

  gotoScreenerHistory = (item) => {
    const { navigate } = this.props.navigation;
    navigate("ScreenerHistory", { screenPk: item.screenId, screenerName: item.screenTitle });
  };

  getData = () => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const { isFirstCallComplete } = this.state;
    const URL = baseURL + USER_ACTIVE_ALERTS_URL;

    // this.setState({ refreshing: true });

    baseRef.showLoading();

    fetcher(
      URL,
      POST_METHOD,
      null,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body, head } = json;
        const data = {
          screeners: mapHeadersData(body.alertHeaders, body.alertData),
          maxAllowedFreq: body.maxAllowedFreq,
          allowedFreqList: body.allowedFreqList,
        };

        console.log("Screener alerts", data);
        this.setState(data);
        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        baseRef && baseRef.stopLoading();
      },
      (message) => {
        //handle error here
        baseRef && baseRef.stopLoading();
        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  // handleScroll = (event) => {
  //   if (event) {
  //     if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
  //       this.showScrollToTop = true;
  //       this.setState({ showScrollToTop: true });
  //     } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
  //       this.showScrollToTop = false;
  //       this.setState({ showScrollToTop: false });
  //     }
  //   }
  // };

  editAlerts = (item) => {
    this.setState({ selectedScreener: item, screenerName: item.screenTitle });
    this.refs.ScreenerAlerts.RBSheet.open();
  };

  renderScreenerIcon = (item) => {
    const icon = item.iconURL ? item.iconURL : null;

    if (icon) {
      return (
        <View style={styles.iconContainer}>
          <Image source={{ uri: icon }} style={styles.screenerIcon} />
        </View>
      );
    } else {
      return (
        <View style={styles.iconContainer}>
          <View style={styles.noImageBox}>
            <Text style={styles.initial}>{item.screenTitle.substr(0, 2)}</Text>
          </View>
        </View>
      );
    }
  };

  renderScreenerItem = ({ item, index }) => {
    const { navigate } = this.props.navigation;
    const editIcon = require("../../assets/icons/edit-blue.png");

    return (
      <TouchableOpacity
        key={index.toString()}
        style={styles.screenerItem}
        onPress={() => this.gotoScreenerHistory(item)}
      >
        {this.renderScreenerIcon(item)}
        <Text style={styles.screenerName}>{item.screenTitle}</Text>

        <TouchableOpacity style={styles.editIconStyle} onPress={() => this.editAlerts(item)}>
          <View style={{ padding: 4 }}>
            <Image source={editIcon} />
          </View>
        </TouchableOpacity>
      </TouchableOpacity>
    );
  };

  renderScreeners = () => {
    const { screeners } = this.state;

    if (screeners.length > 0)
      return (
        <FlatList
          contentContainerStyle={styles.screenerList}
          data={screeners}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderScreenerItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          removeClippedSubviews
          initialNumToRender={8}
          maxToRenderPerBatch={5}
        />
      );
  };

  render() {
    const { isSelected } = this.props;
    const {
      selectedScreener,
      screenerName,
      allowedFreqList,
      isFirstCallComplete,
      screeners,
      showScrollToTop,
    } = this.state;

    const screenerIcon = require("../../assets/icons/alerts/screener-alert.png");
    const heading = "You haven’t set up any Screener alerts";
    const message = "Set up alerts on your preferred screeners and get updates on entries, exits and active stocks";

    const isEmpty = isFirstCallComplete && screeners.length == 0;
    if (isSelected) {
      return (
        <View style={styles.container}>
          <ScrollView
            // ref={(ref) => (this.listRef = ref)}
            // onScroll={this.handleScroll}
            showsVerticalScrollIndicator={false}
          >
            {this.renderScreeners()}
          </ScrollView>

          <ScreenerAlerts
            ref="ScreenerAlerts"
            name={screenerName}
            alertData={selectedScreener}
            allowed={allowedFreqList}
            onChange={(data) => this.setState({ selectedScreener: data })}
          />

          {/* <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} /> */}
          {isEmpty && <EmptyView heading={heading} message={message} imageSrc={screenerIcon} />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(AlertScreenerScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  screenerItem: {
    flex: 1,
    flexDirection: "row",
    minHeight: hp(11),
    backgroundColor: colors.white,
    // marginBottom: 0.3,
    paddingVertical: wp(4),
    alignItems: "center",
  },
  screenerIconContainer: {
    width: wp(18),
    paddingTop: wp(1),
    alignItems: "center",
  },
  screenerIcon: {
    width: wp(7),
    height: wp(8),
    resizeMode: "contain",
  },
  // screenerInfo: {
  //   flex: 1,
  //   paddingRight: "3%"
  // },
  screenerName: {
    flex: 1,
    paddingRight: wp(1),
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  editIconStyle: {
    resizeMode: "contain",
    marginRight: wp(2),
    paddingVertical: wp(2),
    paddingHorizontal: wp(2),
  },
  extraSpacing: {
    flex: 1,
    height: ifIphoneX ? hp(4) : 0,
    backgroundColor: colors.white,
  },
  screenerList: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    backgroundColor: colors.white,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  noImageBox: {
    width: wp(13),
    height: wp(14),
    borderRadius: 5,
    backgroundColor: colors.whiteTwo,
    padding: "1%",
    alignItems: "center",
    justifyContent: "center",
  },
  initial: {
    fontSize: wp(5),
    color: colors.primary_color,
    fontWeight: "500",
  },
  iconContainer: {
    width: wp(18),
    padding: "1%",
    alignItems: "center",
    justifyContent: "flex-start",
  },
});
