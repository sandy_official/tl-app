import React from "react";
import { View, Text, FlatList, Image, StyleSheet, TouchableOpacity, ScrollView } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { ALERTS_CATEGORIES } from "../../utils/AlertsConstants";

export default class AlertStockScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = { stocks: [] };
  }

  renderScreenerItem = ({ item, index }) => {
    const { navigate } = this.props.navigation;
    // console.log("alertype",alertType)

    return (
      <TouchableOpacity
        key={index.toString()}
        onPress={() => navigate("AlertsStock", { title: item.name, alertType: item.alertType })}
      >
        <View style={styles.screenerItem}>
          <View style={styles.screenerIconContainer}>
            <Image source={item.icon} style={styles.screenerIcon} />
          </View>
          <View style={[{ flex: 1, justifyContent: "flex-end", paddingTop: 5 }]}>
            <Text style={styles.screenerName}>{item.name}</Text>
          </View>
          <Image source={require("../../assets/icons/arrow-right-blue.png")} style={[styles.screenerArrowIcon]} />
        </View>
      </TouchableOpacity>
    );
  };

  renderScreeners = () => {
    return (
      <View>
        <FlatList
          contentContainerStyle={styles.screenerList}
          data={ALERTS_CATEGORIES}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderScreenerItem}
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
        />
      </View>
    );
  };

  render() {
    const { isSelected } = this.props;

    if (isSelected) {
      return (
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>{this.renderScreeners()}</ScrollView>
        </View>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo
  },
  screenerItem: {
    flexDirection: "row",
    minHeight: hp(10),
    backgroundColor: colors.white,
    alignItems: "center",
    paddingVertical: wp(2)
  },
  screenerIconContainer: {
    width: wp(18),
    paddingTop: wp(1),
    alignItems: "center"
  },
  screenerIcon: {
    width: wp(7),
    height: wp(8),
    resizeMode: "contain"
  },
  screenerName: {
    fontSize: RF(2.2),
    color: colors.primary_color
  },
  screenerArrowIcon: {
    height: wp(4.5),
    width: wp(4.5),
    resizeMode: "contain",
    marginTop: 3,
    marginRight: "4%"
  },
  recentItemContainer: {
    width: wp(36),
    height: wp(18),
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(2.5),
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
    borderRadius: 5
  },
  recentItem: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white
  },
  recentList: {
    padding: wp(3)
  },
  recentListLabel: {
    fontSize: RF(1.6),
    color: colors.warmGrey,
    marginLeft: wp(3),
    fontWeight: "500"
  },
  recentListContainer: {
    paddingTop: wp(4),
    marginBottom: hp(1.8),
    backgroundColor: colors.white
  },
  extraSpacing: {
    flex: 1,
    height: ifIphoneX ? hp(4) : 0,
    backgroundColor: colors.white
  },
  screenerList: {
    // paddingBottom: ifIphoneX ? hp(3) : hp(1),
    backgroundColor: colors.white
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish
  }
});
