import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import { getNotificationAlert } from "../../controllers/AlertsController";
import { connect } from "react-redux";
import EmptyView from "../../components/common/EmptyView";
import LoadingView from "../../components/common/LoadingView";
import { deepLinking } from "../../utils/DeepLinking";

class NotificationsScreen extends React.PureComponent {
  componentDidMount() {
    if (this.props.isSelected) this.getData();
  }

  constructor(props) {
    super(props);

    this.state = {
      selectedOption: "All",
      newsPageNumber: 1,
      reportsPageNumber: 1,
      corporatePageNumber: 1,
      newsNextPage: true,
      reportsNextPage: true,
      corporateNextPage: true,
      refreshing: false,
      isFirstCallComplete: false,
      newsQsTime: "",
      reportsQsTime: "",
      corporateQsTime: "",
      notifications: [],
      tempNotifications: [],
      allNotifications: [],
      notification: "",
      nColor: "",
      timeStamp: "",
    };
  }

  /**
   * this needs to be changed in next release
   */
  getData = () => {
    const { userState, baseURL, appToken, baseRef } = this.props;
    const { token, isGuestUser } = userState;
    const { selectedOption } = this.state;

    // let type = selectedOption == "All" ? "" : selectedOption.toLowerCase();
    if (!isGuestUser) {
      this.setState({ refreshing: true });

      getNotificationAlert(baseURL, appToken, token, "", (data) => {
        let notifications = data.notifications ? data.notifications : [];

        this.setState({
          notifications: notifications,
          tempNotifications: notifications,
          allNotifications: notifications,
          isFirstCallComplete: true,
          refreshing: false,
        });
      });
    }
  };

  handleUrl = (url) => {
    const { navigation } = this.props;

    if (typeof url == "string") {
      console.warn("url", url);
      const navObj = deepLinking(url);
      navigation && navigation.navigate(navObj.screen, navObj.params);
    }
  };

  setOptionSelection = (option) => {
    const { tempNotifications, allNotifications } = this.state;

    this.setState({ selectedOption: option });

    if (option == "All") {
      this.setState({ notifications: allNotifications });
    } else if (option == "Positive") {
      this.setState({ notifications: tempNotifications.filter((ele) => ele.nColor === "positive") });
    } else if (option == "Negative") {
      this.setState({ notifications: tempNotifications.filter((ele) => ele.nColor === "negative") });
    } else if (option == "Neutral") {
      this.setState({ notifications: tempNotifications.filter((ele) => ele.nColor === "neutral") });
    }
  };

  renderOptions = (option) => {
    const { selectedOption } = this.state;
    const optionStyle = selectedOption == option ? styles.optionSelected : styles.option;
    const optionTextStyle = selectedOption == option ? styles.optionSelectedText : styles.optionText;

    return (
      <TouchableOpacity onPress={() => this.setOptionSelection(option)}>
        <View style={optionStyle}>
          <Text style={optionTextStyle}>{option}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderOptionsRow = () => {
    return (
      <View style={styles.optionRow}>
        {this.renderOptions("All")}
        {this.renderOptions("Positive")}
        {this.renderOptions("Negative")}
        {this.renderOptions("Neutral")}
      </View>
    );
  };

  refreshData = () => {
    const { selectedOption } = this.state;
    this.setState({ refreshing: true });

    if (selectedOption == "All") {
      this.setState({ newsPageNumber: 0, newsNextPage: true, refreshing: true }, () => this.getData());
    } else if (selectedOption == "Positive") {
      this.setState({ reportsPageNumber: 0, reportsNextPage: true, refreshing: true }, () => this.getData());
    } else if (selectedOption == "Negative") {
      this.setState({ reportsPageNumber: 0, reportsNextPage: true, refreshing: true }, () => this.getData());
    } else {
      this.setState({ corporatePageNumber: 0, corporateNextPage: true, refreshing: true }, () => this.getData());
    }
  };

  renderRefreshControl = () => {
    const { refreshing } = this.state;
    return <RefreshControl refreshing={refreshing} onRefresh={() => this.refreshData()} />;
  };

  changeColor = (notificationType) => {
    let color = colors.steelGrey;

    if (notificationType === "neutral") {
      color = colors.yellow;
    } else if (notificationType === "negative") {
      color = colors.red;
    } else if (notificationType === "positive") {
      color = colors.mediumGreen;
    }

    return color;
  };

  renderNotificationsList = ({ item, index }) => {
    const { screenRedirectUrl, timeStamp, notification, nColor } = item;

    return (
      <TouchableOpacity
        style={[styles.row, { alignItems: "center" }]}
        onPress={() => this.handleUrl(screenRedirectUrl)}
      >
        <View style={{ flex: 0.1, height: wp(16), backgroundColor: this.changeColor(nColor) }}></View>
        <View style={[styles.titleContainer]}>
          <Text style={styles.date} numberOfLines={1}>
            {timeStamp}
          </Text>
          <Text style={styles.notificationsTitle}>{notification}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderList = () => {
    const { notifications } = this.state;

    return (
      <FlatList
        data={notifications}
        extraData={this.state}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderNotificationsList}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
      />
    );
  };

  render() {
    const { isSelected } = this.props;
    const { refreshing, isFirstCallComplete } = this.state;
    const { allNotifications } = this.state;

    let showList = isFirstCallComplete && allNotifications.length > 0;

    if (isSelected) {
      return (
        <View style={{ flex: 1 }}>
          {showList && (
            <View style={styles.container}>
              {this.renderOptionsRow()}
              {this.renderList()}
            </View>
          )}

          {!refreshing && !showList && (
            <View style={{ flex: 1 }}>
              <EmptyView
                imageSrc={require("../../assets/icons/alerts/cloudIllustration.png")}
                heading="Seems like a quite day"
                message="There haven't been any updates so far, we'll notify you as soon as we see something interesting"
              />
            </View>
          )}

          {refreshing && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(NotificationsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
    backgroundColor: colors.white,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  optionRow: {
    flexDirection: "row",
    //marginHorizontal: wp(1),
    backgroundColor: colors.whiteTwo,
    paddingVertical: wp(4),
    paddingHorizontal: "4%",
  },
  option: {
    marginRight: wp(2),
    paddingVertical: wp(2),
    paddingHorizontal: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    paddingHorizontal: wp(4.5),
    justifyContent: "center",
    alignItems: "center",
  },
  optionSelected: {
    marginRight: wp(2),
    paddingVertical: wp(2) + 1,
    paddingHorizontal: wp(4) + 1,
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingHorizontal: wp(4.5),
    justifyContent: "center",
    alignItems: "center",
  },
  optionText: {
    // flex: 1,
    fontSize: RF(1.9),

    color: colors.primary_color,
  },
  optionSelectedText: {
    // flex: 1,
    fontSize: RF(1.9),

    color: colors.white,
  },
  insight: {
    fontSize: RF(2.4),
    fontWeight: "300",
    padding: wp(3),
    // marginBottom: 1,
    backgroundColor: colors.white,
  },
  listRow: {
    padding: wp(3),
    backgroundColor: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.whiteTwo,
  },
  column: {
    fontWeight: "500",
  },
  listItem: {
    flex: 1,
    fontSize: RF(2),
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    marginHorizontal: wp(3),
    backgroundColor: colors.greyish,
  },
  titleContainer: {
    flex: 13,
    paddingHorizontal: wp(3),
  },
  date: {
    fontSize: RF(1.9),
    color: colors.steelGrey,
    marginTop: 5,
  },
  newsType: {
    color: colors.greyishBrown,
  },
  row: {
    flexDirection: "row",
    paddingVertical: wp(3),
    backgroundColor: colors.white,
  },
  notificationType: {
    backgroundColor: colors.mediumGreen,
  },
  notificationType1: {
    backgroundColor: colors.red,
  },
  notificationType2: {
    backgroundColor: colors.steelGrey,
  },
  notificationsTitle: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginTop: 5,
    lineHeight: wp(5),
    fontWeight: "500",
  },
});
