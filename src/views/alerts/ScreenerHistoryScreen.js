import React from "react";
import { View, Text, Image, TouchableOpacity, FlatList, StyleSheet } from "react-native";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import { Toolbar } from "../../components/Toolbar";
import { fetcher } from "../../controllers/Fetcher";
import { SCREENER_ALERTS_HISTORY_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import { mapHeadersData } from "../../utils/Utils";
import BaseComponent from "../../components/common/BaseComponent";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class ScreenerHistoryScreen extends React.PureComponent {
  state = {
    history: [],
    screenPk: "",
    isFirstCallComplete: false,
    refreshing: false,
    baseRef: null,
    showScrollToTop: false,
  };

  componentDidMount() {
    const { navigation } = this.props;
    const screenId = navigation.getParam("screenPk", "");
    this.setState({ screenPk: screenId }, () => this.getData());
  }

  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { screenPk, isFirstCallComplete, baseRef } = this.state;
    const URL = baseURL + SCREENER_ALERTS_HISTORY_URL;
    const body = { screenPk: screenPk };
    baseRef && baseRef.showLoading();

    fetcher(
      URL,
      POST_METHOD,
      body,
      null,
      appToken,
      token,
      (json) => {
        const screenerHeaders = json.body.alertHeaders;
        const screenerData = json.body.alertData;
        const data = mapHeadersData(screenerHeaders, screenerData);
        this.setState({ history: data });

        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        baseRef && baseRef.stopLoading();
      },
      (error) => {
        baseRef && baseRef.stopLoading();
      }
    );
  };

  gotoHistoryDetail = (screener) => {
    const { navigation } = this.props;
    const { screenPk } = this.state;
    const name = navigation.getParam("screenerName");

    navigation.navigate("ScreenerHistoryDetail", {
      screenPk: screenPk,
      screenerName: name,
      entries: screener.Entries,
      exits: screener.Exits,
      active: screener.Active,
    });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderTriggers = (entries, exits, active) => {
    const entryIcon = require("../../assets/icons/screeners/screener-entry.png");
    const exitIcon = require("../../assets/icons/screeners/screener-exit.png");
    const activeIcon = require("../../assets/icons/screeners/screener-active.png");

    let containerStyle = styles.triggerContainer;
    let textStyle = styles.triggerCount;

    let entryText = entries;
    let exitText = exits;
    let activeText = active;

    if (!entries && !exits && !active) {
      containerStyle = styles.headers;
      textStyle = styles.triggerName;

      entryText = "Entries";
      exitText = "Exits";
      activeText = "Active Stocks";
    }

    return (
      <View style={containerStyle}>
        <Image source={entryIcon} style={styles.triggerIcon} />
        <Text style={[textStyle, styles.greenText]}>{entryText}</Text>

        <Image source={exitIcon} style={styles.triggerIcon} />
        <Text style={[styles.triggerName, styles.redText]}>{exitText}</Text>

        <Image source={activeIcon} style={styles.triggerIcon} />
        <Text style={[styles.triggerName, styles.blueText]}>{activeText}</Text>
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    const showMessage = item.flagMessage ? true : false;
    const entries = item.Entries ? item.Entries.length : 0;
    const exits = item.Exits ? item.Exits.length : 0;
    const active = item.Active ? item.Active.length : 0;

    return (
      <TouchableOpacity style={styles.listItem} onPress={() => this.gotoHistoryDetail(item)}>
        <View style={styles.detailContainer}>
          <Text style={styles.time}>{item.runTime}</Text>
          {showMessage && <Text style={styles.detail}>{item.flagMessage}</Text>}
        </View>
        {this.renderTriggers(entries, exits, active)}
      </TouchableOpacity>
    );
  };

  renderList = () => {
    const { history } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={history}
        extraData={this.state}
        showsVerticalScrollIndicator={false}
        renderItem={this.renderListItem}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        keyExtractor={(item, index) => index.toString()}
        removeClippedSubviews
        initialNumToRender={8}
        maxToRenderPerBatch={5}
        onScroll={this.handleScroll}
      />
    );
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const title = navigation.getParam("screenerName");

    return <Toolbar title={title} {...this.props} backButton />;
  };

  render() {
    const { refreshing, showScrollToTop } = this.state;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <BaseComponent ref={(ref) => this.setState({ baseRef: ref })} {...this.props}>
          {this.renderTriggers()}
          {this.renderList()}
        </BaseComponent>

        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(ScreenerHistoryScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  headers: {
    flexDirection: "row",
    marginHorizontal: wp(3.2),
    marginVertical: wp(3.5),
    alignItems: "center",
  },
  triggerIcon: {
    marginRight: wp(1.2),
  },
  triggerName: {
    fontSize: RF(1.9),
    marginRight: wp(3.2),
  },
  greenText: {
    color: colors.green,
  },
  redText: {
    color: colors.red,
  },
  blueText: {
    color: colors.primary_color,
  },
  triggerCount: {
    marginRight: wp(2),
  },
  listItem: {
    flexDirection: "row",
    backgroundColor: colors.white,
    padding: wp(3.5),
    alignItems: "center",
  },
  detailContainer: {
    flex: 1,
    justifyContent: "center",
  },
  triggerContainer: {
    flexDirection: "row",
    marginLeft: wp(4),
  },
  time: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginBottom: 6,
  },
  detail: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
});
