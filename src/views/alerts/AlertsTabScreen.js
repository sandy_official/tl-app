import React from "react";
import { View, StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { Toolbar } from "../../components/Toolbar";
import { connect } from "react-redux";
import MyNewsFeedScreen from "./MyNewsFeedScreen";
import AlertScreenerScreen from "./AlertScreenerScreen";
import BaseComponent from "../../components/common/BaseComponent";
import TabComponent from "../../components/common/TabComponent";
import AlertSuperstarScreen from "./AlertSuperstarScreen";
import AlertStockScreen from "./AlertStockScreen";
import NotificationsScreen from "./NotificationsScreen";

class AlertsTabScreen extends React.PureComponent {
  tabs = ["Stock", "Screener", "Superstar", "News", "Notifications"];

  // componentDidMount() {
  //   const { navigation } = this.props;
  //   const tabIndex = navigation.getParam("tabIndex", 0);
  //   this.setState({ selectedTab: tabIndex }, () => this.setTabSelection({ position: tabIndex }));
  // }

  constructor(props) {
    super(props);

    const tabIndex = props.navigation.getParam("tabIndex", 0);

    this.state = {
      selectedTab: tabIndex,
      isRefreshing: false,
      isApiCalled: [false, false, false, false, false],
      baseRef: null,
    };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [false, false, false, false, false] }, () =>
      this.setTabSelection({ position: selectedTab })
    );
  };

  setTabSelection = (obj) => {
    const { baseRef, isApiCalled } = this.state;
    let validRef = false;

    const tabPosition = obj.position;
    this.setState({ selectedTab: tabPosition });

    if (!isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          // validRef = this.refs.news ? true : false;
          // validRef ? this.refs.news.getData() : null;
          break;
        case 1:
          validRef = this.refs.screener ? true : false;
          validRef ? this.refs.screener.getData() : null;
          break;
        case 2:
          // validRef = this.refs.bbDeals ? true : false;
          // validRef ? this.refs.bbDeals.getData() : null;
          break;
        case 3:
          validRef = this.refs.news ? true : false;
          baseRef ? baseRef.showLoading() : null;
          validRef ? this.refs.news.getData() : null;
          break;
        case 4:
          validRef = this.refs.notification ? true : false;
          validRef ? this.refs.notification.getData() : null;
      }

      let apiCalls = isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls });
    }
  };

  renderToolbar = () => <Toolbar title="Alerts" drawerMenu {...this.props} />;

  getAnalyticsParams = () => {
    return {};
  };

  renderTabs = () => {
    const { selectedTab, baseRef } = this.state;

    return (
      <TabComponent
        analyticsParams={this.getAnalyticsParams}
        tabs={this.tabs}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
      >
        <View>
          <AlertStockScreen {...this.props} isSelected={selectedTab == 0} baseRef={baseRef} />
        </View>
        <View>
          <AlertScreenerScreen ref="screener" {...this.props} isSelected={selectedTab == 1} baseRef={baseRef} />
        </View>
        <View>
          <AlertSuperstarScreen {...this.props} baseRef={baseRef} isSelected={selectedTab == 2} />
        </View>
        <View>
          <MyNewsFeedScreen ref="news" {...this.props} isSelected={selectedTab == 3} baseRef={baseRef} />
        </View>
        <View>
          <NotificationsScreen ref="notification" {...this.props} isSelected={selectedTab == 4} baseRef={baseRef} />
        </View>
      </TabComponent>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <BaseComponent
          ref={(ref) => this.setState({ baseRef: ref })}
          refreshScreen={this.refreshScreen}
          {...this.props}
        >
          {this.renderTabs()}
        </BaseComponent>
      </View>
    );
  }
}

mapStateToProps = (state) => ({
  userState: state.user,
  // superstars: state.superstar.superstars
});

export default connect(mapStateToProps)(AlertsTabScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
});
