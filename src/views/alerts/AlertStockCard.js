import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, TouchableWithoutFeedback, Image, Linking } from "react-native";
import { getDVMBackgroundColor, roundNumber } from "../../utils/Utils";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import { APIDataFormatter } from "../../utils/APIDataFormatter";

var title;
export default class AlertStockCard extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      expand: false,
      getfullname: "",
      dayTypeValue: "",
      periodCloseValue: "",
      periodCloseValue: "",
      periodChangeValue: "",
      periodChangeValue: "",
      triggered: "",
      insights: "",
      alertPeriod: "",
      threshold: "",
      broker: "",
      alertActive: false,
      portfolioActive: false,
      alertType: "",
      alertName: "",
      target_price: "",
      reco_type: "",
      pdf_url: "",
      page_url: "",
      isFirstCallComplete: false,
      data: [],
      stock: [],
      selectedStock: {},
      emptyItem: [],
    };
  }

  editAlerts = (stock) => {
    this.setState({ selectedStock: stock }, () => this.refs.stockAlerts.open());
  };

  renderDVM = (item) => {
    return (
      <View style={styles.row}>
        <View style={getDVMBackgroundColor(item.d_color)} />
        <View style={getDVMBackgroundColor(item.v_color)} />
        <View style={getDVMBackgroundColor(item.m_color)} />
      </View>
    );
  };

  renderToolbar = () => {
    const title = this.props.navigation.getParam("title");

    return (
      <Toolbar title={title} {...this.props} backButton>
        <ImageButton
          source={require("../../assets/icons/info-white.png")}
          onPress={() => this.refs.categoryBottomSheet.RBSheet.open()}
        />
      </Toolbar>
    );
  };

  openLinkInBrowser = (url) => {
    const { isGuestUser, showLogin, navigation } = this.props;

    if (!isGuestUser) {
      // Linking.openURL(url);
      navigation.navigate("WebView", { url: url });
    } else showLogin();
  };

  gotoStockDetails = () => {
    const { navigation, item } = this.props;
    const stockCode = item.NSEcode ? item.NSEcode : item.BSEcode;
    let pageType = typeof item.page_type == "string" ? item.page_type.toLowerCase() : "";

    if (pageType == "equity" || pageType == "unlisted" || pageType == "index") {
      navigation.push("Stocks", { stockId: item.stock_id, stockName: item.get_full_name, pageType: item.page_type, stockCode: stockCode });
    }
  };

  renderReportDetails = (item, index) => {
    let price = "target_price" in item.metadata ? item.metadata.target_price : null;
    let targetPrice = typeof price == "number" ? roundNumber(price) : price;
    let recoType = item.metadata.reco_type;
    let color = "";

    switch (item.metadata.reco_type) {
      case "Buy":
        color = styles.textColor;
        break;

      case "Hold":
        color = styles.textColor1;
        break;

      case "Sell":
        color = styles.textColor2;
        break;

      case "Purchase":
        color = styles.textColor;
        break;

      default:
        color = styles.colorText4;
    }

    return (
      <View style={styles.StockCard}>
        <View style={styles.StockCodeContainer}>
          <Text style={[styles.stockCode1, color]} numberOfLines={3} ellipsizeMode="tail">
            {recoType}
          </Text>
          <Text style={styles.label1} numberOfLines={3} ellipsizeMode="tail">
            {targetPrice}
          </Text>
        </View>

        <View style={styles.StockCodeContainer}>
          <Text style={styles.stockR} numberOfLines={3} ellipsizeMode="tail">
            Recommendation
          </Text>
          <Text style={styles.label} numberOfLines={3} ellipsizeMode="tail">
            Target Price
          </Text>
        </View>
      </View>
    );
  };

  getValue = (item) => {
    switch (item.alertType) {
      case "price":
        return ` ${item.threshold} %`;

      case "sma":
        return item.threshold.toLowerCase();

      default:
        return `${item.threshold}`;
    }
  };

  renderButtonRow = (item, index) => {
    let textTransform = "";

    switch (item.alertType) {
      case "sma":
        textTransform = styles.smaText;
        break;

      case "price":
        textTransform = styles.alertText;
        break;

      default:
        textTransform = styles.alertText;
    }

    return (
      <View>
        <Text style={[styles.expand, textTransform]} numberOfLines={3} ellipsizeMode="tail">
          {item.alertType + "  |  "}
          <Text>{item.alertPeriod + "  |  "}</Text>
          <Text style={{ textTransform: item.alertType === "sma" ? "capitalize" : "none" }}>{this.getValue(item)}</Text>
        </Text>
        <Text style={styles.expand1} numberOfLines={3} ellipsizeMode="tail">
          {item.insights}
        </Text>
      </View>
    );
  };

  getIndicatorIcon = (boolValue) => {
    if (boolValue) return require("../../assets/icons/arrow-green.png");
    else return require("../../assets/icons/arrow-red.png");
  };

  getIndicatorTextStyle = (boolValue) => {
    if (boolValue) return styles.greenText;
    else return styles.redText;
  };

  renderTargetItem = (item) => {
    const { navigation } = this.props;

    // if (item && "metadata" in item) {
    const { insights, metadata } = item;
    const { pdf_url, page_url, priceUpgrade, priceDowngrade } = metadata ? metadata : {};

    const pdfUrl = pdf_url ? pdf_url : "";
    const pageUrl = page_url ? page_url : "";

    const targetUpgrade = priceUpgrade ? priceUpgrade : false;
    const targetDowngrade = priceDowngrade ? priceDowngrade : false;
    const showTarget = targetUpgrade || targetDowngrade;
    const targetText = "Target";

    let showInsights = insights ? true : false;

    return (
      <View>
        <View style={[styles.subContentRow]}>
          {showTarget ? (
            <View style={[styles.indicator]}>
              <Image source={this.getIndicatorIcon(targetUpgrade)} style={styles.indicatorIcon} />
              <Text style={[styles.indicatorLabel, this.getIndicatorTextStyle(targetUpgrade)]}>{targetText}</Text>
            </View>
          ) : (
            <View style={{ flex: 1 }} />
          )}

          {pageUrl !== "" ? (
            <TouchableOpacity style={[styles.externalLinkContainer]} onPress={() => this.openLinkInBrowser(pageUrl)}>
              <Image source={require("../../assets/icons/alerts/externalLinkIcon.png")} resizeMode="contain" />
            </TouchableOpacity>
          ) : null}

          {pdfUrl !== "" ? (
            <TouchableOpacity style={[styles.pdfIconStyle]} onPress={() => this.openLinkInBrowser(pdfUrl)}>
              <Image source={require("../../assets/icons/alerts/pdfIcon.png")} resizeMode="contain" />
            </TouchableOpacity>
          ) : null}
        </View>

        {showInsights && <Text style={styles.reportInsights}>{insights}</Text>}
      </View>
    );
    // }
  };

  renderOtherDetails = (item, index) => {
    const { alertType, dayTypeValue, periodCloseValue, periodChangeValue } = item;

    console.warn("");

    let col1 = "Day Price";
    let col2 = "";
    let col3 = "";

    let val1 = dayTypeValue;
    let val2 = periodCloseValue;
    let val3 =
      periodChangeValue && typeof periodChangeValue == "string" && periodChangeValue.indexOf("%") != -1
        ? roundNumber(periodChangeValue.split(" ")[0]) + " %"
        : periodChangeValue;

    if (alertType == "price") {
      col2 = "Month Close";
      col3 = "Change %";
    } else if (alertType == "sma") {
      col2 = "SMA 50";
      col3 = "Crossover";
    } else if (alertType == "volume") {
      col1 = "Day Volume";
      col2 = "Month Average Volume";
      col3 = "Times";
    }

    if (!isNaN(val1)) val1 = roundNumber(val1);
    if (!isNaN(val2)) val2 = roundNumber(val2);
    if (!isNaN(val3)) val3 = roundNumber(val3);

    let color = "";
    if (item.periodChangeValue === "ABOVE" || item.periodChangeValue.toLowerCase() === "above") {
      color = styles.aboveText;
    } else if (item.periodChangeValue === "BELOW" || item.periodChangeValue.toLowerCase() === "below") {
      color = styles.belowText;
    } else if (parseInt(item.periodChangeValue) > 0) {
      color = styles.colorText3;
    } else if (parseInt(item.periodChangeValue) < 0) {
      color = styles.colorText2;
    } else {
      color = styles.colorText4;
    }

    return (
      <View style={styles.stockDetails}>
        <View style={[styles.detailRow]}>
          <Text style={[styles.value, styles.left]}> {val1} </Text>
          <Text style={[styles.label, styles.left]}> {col1} </Text>
        </View>
        <View style={styles.detailRow}>
          <Text style={[styles.value, styles.center]}> {val2} </Text>
          <Text style={[styles.label, styles.center]}> {col2} </Text>
        </View>
        <View style={styles.detailRow}>
          <Text style={[styles.value, styles.right, color]}> {val3} </Text>
          <Text style={[styles.label, styles.right]}> {col3} </Text>
        </View>
      </View>
    );
  };

  render() {
    const { item } = this.props;
    const { selectedStock } = this.state;
    const { navigate } = this.props.navigation;
    let alertTypeStyle = "";
    if (item.alertType == "price") {
      alertTypeStyle = styles.alertType;
    } else if (item.alertType == "research_report") {
      alertTypeStyle = styles.alertType1;
    } else if (item.alertType == "sma") {
      alertTypeStyle = styles.alertType2;
    } else if (item.alertType == "volume") {
      alertTypeStyle = styles.alertType3;
    }

    const isReport = item.alertType == "research_report";
    const alertType = this.props.navigation.getParam("alertType");
    const stockDate = APIDataFormatter("triggered", item.triggered);

    return (
      <TouchableWithoutFeedback onPress={() => this.setState({ expand: !this.state.expand })}>
        <View style={styles.card}>
          <TouchableOpacity onPress={this.gotoStockDetails}>
            <View style={styles.StockCodeContainer}>
              <Text style={styles.stockCode} numberOfLines={3} ellipsizeMode="tail">
                {item.get_full_name}
              </Text>
              {this.renderDVM(item)}
            </View>
          </TouchableOpacity>
          <View style={[styles.StockCodeContainer]}>
            <Text style={[styles.stockalert, alertTypeStyle]} numberOfLines={3} ellipsizeMode="tail">
              {item.alertType === "research_report" ? "Report" : item.alertType}
              <Text style={styles.stockCode3}> {"  " + stockDate}</Text>
            </Text>

            <ImageButton
              source={require("../../assets/icons/editIcon.png")}
              onPress={() => this.editAlerts(item)}
              menuItem={{ marginHorizontal: 0, paddingLeft: wp(2), paddingTop: wp(1.6) }}
              // style={styles.screenerArrowIcon}
            />
            <StockAlertsModal ref="stockAlerts" {...this.props} stock={selectedStock} />
          </View>
          {isReport ? this.renderReportDetails(item) : this.renderOtherDetails(item)}

          {isReport
            ? this.state.expand && this.renderTargetItem(item)
            : this.state.expand && this.renderButtonRow(item)}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

const styles = StyleSheet.create({
  card: {
    marginVertical: "2%",
    marginHorizontal: "3%",
    padding: "3%",
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  StockCodeContainer: {
    flexDirection: "row",
  },
  StockCard: {
    marginVertical: wp(3),
  },
  StockCodeContainer1: {
    flexDirection: "column",
  },
  row: {
    flexDirection: "row",
  },
  stockCode: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    fontWeight: "300",
    marginRight: wp(4),
    marginBottom: wp(1),
  },
  BrokerName: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.primary_color,
    marginRight: wp(4),
  },
  stockR: {
    flex: 1,
    fontSize: RF(1.5),
    color: colors.warmGrey,
    marginRight: wp(4),
  },
  stockalert: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.primary_color,
    fontWeight: "300",
    // marginRight: wp(4),
    marginTop: wp(1.6),
  },
  stockCode1: {
    flex: 1,
    fontSize: RF(1.9),
    fontWeight: "500",
    marginRight: wp(4),
    color: colors.red,
    marginBottom: 5,
  },
  stockCode3: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.steelGrey,
    // marginRight: wp(6),
    marginTop: wp(4),
    textAlign: "center",
    fontWeight: "300",
    // textTransform: "none"
  },
  expand: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.black,
    fontWeight: "500",
    marginRight: wp(4),
    marginBottom: 2,
    marginTop: wp(5),
    //textTransform: "capitalize"
  },
  expand1: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.black,
    marginRight: wp(4),
    marginBottom: 4,
    marginTop: wp(1.6),
  },
  expand3: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.red,
    fontWeight: "500",
    marginRight: wp(4),
    marginBottom: 4,
    marginTop: wp(4),
  },
  detailRow: {
    flex: 1,
    marginTop: wp(2),
  },
  valuesRow: {
    flexDirection: "row",
    marginBottom: 6,
  },
  labelRow: {
    flexDirection: "row",
  },
  left: {
    flex: 1,
    textAlign: "left",
    paddingRight: wp(4),
  },
  center: {
    flex: 1,
    textAlign: "center",
  },
  right: {
    flex: 1,
    textAlign: "right",
    paddingLeft: wp(4),
  },
  value: {
    fontSize: RF(2.2),
    fontWeight: "300",
    marginBottom: 5,
    color: colors.greyishBrown,
  },
  label5: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
    flex: 1,
    fontWeight: "500",
    marginTop: wp(3),
  },
  label1: {
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    fontWeight: "500",
    marginBottom: 5,
  },
  label: {
    fontSize: RF(1.5),
    color: colors.warmGrey,
    //marginTop: wp(1),
  },
  hide: {
    display: "none",
  },
  screenerArrowIcon: {
    height: wp(4.5),
    width: wp(4.5),
    resizeMode: "contain",
    marginBottom: wp(12),
    marginRight: "4%",
  },
  stockDetails: {
    flexDirection: "row",
    marginTop: wp(1.8),
  },
  equityShare: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.greyishBrown,
  },
  tradeDate: {
    fontSize: RF(1.5),
    color: colors.greyishBrown,
  },
  alertType: {
    fontWeight: "500",
    color: colors.cerulean,
    textTransform: "capitalize",
  },
  alertType1: {
    fontWeight: "500",
    color: colors.vibrantPurple,
    textTransform: "capitalize",
  },
  alertType2: {
    fontWeight: "500",
    color: colors.blueViolet,
    textTransform: "uppercase",
  },
  alertType3: {
    fontWeight: "500",
    color: colors.barbiePink,
    textTransform: "capitalize",
  },
  alertType4: {
    fontWeight: "500",
    color: colors.cerulean,
    textTransform: "capitalize",
  },
  expandT: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.black,
    marginRight: wp(4),
    marginBottom: 2,
    marginTop: wp(5),
  },
  subContentRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  externalLinkContainer: {
    justifyContent: "flex-start",
    // flex: 10,
    alignItems: "flex-end",
    // marginBottom: 2,
    // marginTop: wp(3),
  },
  pdfIconStyle: {
    // width: wp(6.2),
    height: wp(6.2),
    resizeMode: "contain",
    // marginVertical: wp(2),
    marginLeft: wp(6),
  },
  stockChangeP: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.green,
    marginLeft: 5,
  },
  redText: {
    color: colors.red,
  },
  greenText: {
    color: colors.mediumGreen,
  },

  colorText: {
    color: colors.mediumGreen,
    textTransform: "lowercase",
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  colorText1: {
    color: colors.red,
    textTransform: "lowercase",
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  colorText2: {
    color: colors.red,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  colorText3: {
    color: colors.mediumGreen,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  colorText4: {
    color: colors.steelGrey,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  textColor: {
    color: colors.mediumGreen,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  textColor1: {
    color: colors.yellow,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  textColor2: {
    color: colors.red,
    fontSize: RF(1.9),
    fontWeight: "500",
  },
  aboveText: {
    color: colors.mediumGreen,
    textTransform: "lowercase",
    fontSize: RF(1.9),
    fontWeight: "500",
    textTransform: "capitalize",
  },
  belowText: {
    color: colors.red,
    textTransform: "lowercase",
    fontSize: RF(1.9),
    fontWeight: "300",
    textTransform: "capitalize",
  },
  smaText: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.black,
    fontWeight: "500",
    marginRight: wp(4),
    marginBottom: 2,
    marginTop: wp(5),
    textTransform: "uppercase",
  },
  smaText: {
    textTransform: "uppercase",
  },
  alertText: {
    textTransform: "capitalize",
  },
  indicator: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  indicatorIcon: {
    width: wp(2),
    height: wp(2),
    resizeMode: "contain",
    marginRight: 6,
  },
  indicatorLabel: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
  },
  reportInsights: {
    fontSize: RF(1.9),
    color: colors.black,
    marginTop: wp(3),
  },
});
