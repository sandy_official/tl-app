import React from "react";
import { View, StyleSheet, Text, Image, TouchableOpacity } from "react-native";
import { colors } from "../../styles/CommonStyles";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { setSuperstarAlerts } from "../../controllers/SuperstarAlertsController";
import { connect } from "react-redux";
import { getSuperstarAlerts } from "../../controllers/SuperstarAlertsController";
import LoginModal from "../../components/common/LoginModal";

class AlertSuperstarScreen extends React.PureComponent {
  componentDidMount() {
    const { userState } = this.props;
    this.checkSuperstarAlerts();
  }

  constructor(props) {
    super(props);
    this.state = { isRemoved: null, selectedAlert: "", alertButtonState: { daily: false, weekly: false } };
  }

  checkSuperstarAlerts = () => {
    const { userState, baseURL, appToken } = this.props;

    getSuperstarAlerts(baseURL, appToken, userState.token, (alertType) => {
      if (alertType != "") {
        if (alertType == "daily") this.setState({ alertButtonState: { daily: true, weekly: false } });
        else if (alertType == "weekly") this.setState({ alertButtonState: { daily: false, weekly: true } });
      }
    });
  };

  setAlerts = (alertType) => {
    const { baseURL, appToken } = this.props;
    const { token, isSubscriber, isGuestUser } = this.props.userState;

    console.log("alertType", alertType);

    if (isGuestUser) this.refs.loginModal.open();
    else if (isSubscriber)
      setSuperstarAlerts(baseURL, appToken, token, alertType, (message) => this.setState({ selectedAlert: alertType }));
    else this.props.baseRef.showSubscribe();
    // else if (isSubscriber) this.refs.alertBSheet.RBSheet.open();
    // else this.refs.alertNonSubBSheet.RBSheet.open();
  };

  changeAlerts = (type) => {
    const { buttonState, setButtonState, userState } = this.props;
    console.log("userstate", userState);
    if (userState.isSubscriber) {
      switch (type) {
        case "daily":
          this.setState({ isRemoved: !!buttonState.daily });
          this.setAlerts("daily");
          break;

        case "weekly":
          this.setState({ isRemoved: !!buttonState.weekly });
          this.setAlerts("weekly");
          break;

        case "remove":
          this.setState({ isRemoved: true });
          this.setAlerts("remove");
          break;
      }
    } else {
      this.props.baseRef.showSubscribe();
    }
  };

  closeDialog = () => {
    this.props.close(this.state.isRemoved);
    this.setState({ isRemoved: null });
    this.RBSheet.close();
  };

  setAlerts1 = (isRemoved) => {
    if (isRemoved != null) {
      // console.log(isRemoved);
      const message = isRemoved ? "Superstar Alert Removed" : "Superstar Alert Added";

      this.refs.toast.show(message, 1000);
    }
  };

  render() {
    const { isSelected } = this.props;
    const { selectedAlert } = this.state;
    const { email } = this.props.userState;

    const dailyButtonStyle = selectedAlert == "daily" ? styles.buttonActive : styles.button;
    const dailyButtonTextStyle = selectedAlert == "daily" ? styles.buttonTextActive : styles.buttonText;

    const weeklyButtonStyle = selectedAlert == "weekly" ? styles.buttonActive : styles.button;
    const WeeklyButtonTextStyle = selectedAlert == "weekly" ? styles.buttonTextActive : styles.buttonText;

    const removeButtonStyle =
      selectedAlert == "daily" || selectedAlert == "weekly" ? [styles.removeAlert, styles.redText] : styles.removeAlert;

    const removeButtonState = selectedAlert == "daily" || selectedAlert == "weekly" ? false : true;

    if (isSelected) {
      return (
        <View style={styles.infoContainer}>
          <View
            style={{ backgroundColor: colors.white, width: wp(100), paddingHorizontal: wp(4), paddingBottom: hp(5) }}
          >
            <View style={styles.row}>
              <Text style={styles.title}>Set Superstar Alerts</Text>

              {/* <TouchableOpacity style={{ padding: wp(2) }} onPress={() => this.closeDialog()}>
                <Image source={{ uri: ICON_CDN + "cross-blue.png" }} style={styles.crossIcon} />
              </TouchableOpacity> */}
            </View>

            <Text style={styles.infoDetails}>
              This single alert tracks all superstar shareholding changes and bulk block deals
            </Text>

            <Text style={styles.infoDetails1}>{`All superstar alerts are sent to ${email}`}</Text>

            <View style={styles.extraSpace} />

            <View style={styles.row}>
              <View style={styles.buttonRow}>
                <TouchableOpacity onPress={() => this.setAlerts("daily")}>
                  <View style={dailyButtonStyle}>
                    <Text style={dailyButtonTextStyle}>Daily</Text>
                  </View>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => this.setAlerts("weekly")}>
                  <View style={weeklyButtonStyle}>
                    <Text style={WeeklyButtonTextStyle}>Weekly</Text>
                  </View>
                </TouchableOpacity>
              </View>

              <TouchableOpacity onPress={() => this.setAlerts("remove")} disabled={removeButtonState}>
                <Text style={removeButtonStyle}>Remove Alert</Text>
              </TouchableOpacity>
            </View>
          </View>

          <LoginModal ref="loginModal" {...this.props} />
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(AlertSuperstarScreen);

const styles = StyleSheet.create({
  infoContainer: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  title: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.black,
    marginBottom: wp(2.5),
    marginTop: hp(2),
  },
  infoDetails: {
    opacity: 0.95,
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: wp(4),
  },
  infoDetails1: {
    opacity: 0.85,
    fontSize: RF(1.9),
    color: colors.greyishBrown,
    marginBottom: wp(3),
  },
  extraSpace: {
    marginBottom: hp(1),
  },
  row: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  buttonRow: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
  },
  button: {
    width: wp(22),
    paddingVertical: wp(2),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
  },
  buttonActive: {
    width: wp(22),
    paddingVertical: wp(2),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginRight: wp(3),
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  },
  buttonTextActive: {
    fontSize: RF(2.2),
    color: colors.white,
  },
  removeAlert: {
    fontSize: RF(1.9),
    color: colors.red,
    paddingVertical: wp(2),
    marginRight: wp(9),
  },
  crossIcon: {
    width: wp(4),
    height: wp(4),
    resizeMode: "contain",
  },
  redText: {
    color: colors.red,
  },
});
