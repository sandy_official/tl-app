import React from "react";
import { View, Text, StyleSheet, FlatList, RefreshControl, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Toolbar } from "../../components/Toolbar";
import { colors } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { connect } from "react-redux";
import { getStockAlert } from "../../controllers/AlertsController";
import AlertStockCard from "./AlertStockCard";
import EmptyView from "../../components/common/EmptyView";
import LoadingView from "../../components/common/LoadingView";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class AlertStockListScreen extends React.PureComponent {
  emptyScreenData = {};

  state = {
    alertType: "",
    alertName: "",
    stock: [],
    selectedStock: {},
    refreshing: false,
    isNextPage: true,
    pageNumber: 0,
    perPageCount: 20,
    isFirstCallComplete: false,
    isLoadingMore: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    const name = this.props.navigation.getParam("title");
    const type = this.props.navigation.getParam("alertType");

    this.setState({ alertType: type, alertName: name });
    this.emptyScreenData = this.getEmptyScreenObj();

    this.getData();
  }

  getData = () => {
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const alertType = this.props.navigation.getParam("alertType");
    const { pageNumber, isNextPage, refreshing, isLoadingMore, isFirstCallComplete, perPageCount } = this.state;
    this.setState({ isRefreshing: true });

    getStockAlert(
      baseURL,
      appToken,
      token,
      alertType,
      pageNumber,
      perPageCount,
      (data) => {
        this.stopLoading();

        if (Object.keys(data).length > 0) {
          const oldStockList = this.state.refreshing ? [] : this.state.stock;
          const newStockList = oldStockList.concat(data.stock);

          this.setState({
            isRefreshing: false,
            pageNumber: ++data.pageNumber,
            isFirstCallComplete: true,
            // perPageCount: data.perPageCount,
            isNextPage: data.isNextPage,
            stock: newStockList,
          });
        }
      },
      (statusCode, error) => {
        this.stopLoading();
        if (statusCode == 4) navigation.navigate("SubscribeMessage");
      }
    );
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const title = navigation.getParam("title");
    // const alertType =navigation.getParam("alertType");

    return <Toolbar title={title} {...this.props} backButton></Toolbar>;
  };

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;

    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false, isLoadingMore: false });
  };

  onLoadMore = () => {
    const { isLoadingMore, isNextPage } = this.state;
    if (!isLoadingMore && isNextPage) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0 }, () => {
      this.getData();
    });
  };

  getEmptyScreenObj = () => {
    const alertType = this.props.navigation.getParam("alertType");

    if (alertType == "All Active Alerts") {
      return {
        heading: "You haven't set up any Stock Alerts",
        message:
          "Set up an alert on any stock and get instant updates on price, moving averages, stock reports and more.",
        image: require("../../assets/icons/alerts/stockAlertsIllustration.png"),
      };
    } else if (alertType == "price") {
      return {
        heading: "You haven't set up any Price Alerts",
        message: "Set up day, week, and month price alerts on any stock and stay updated",
        image: require("../../assets/icons/alerts/illustrationPrice.png"),
      };
    } else if (alertType == "sma") {
      return {
        heading: "You haven't set up any MA Alerts",
        message: "Set up Moving average alerts on any stock and get updates when it crosses above or below an SMA",
        image: require("../../assets/icons/alerts/illustrationMa.png"),
      };
    } else if (alertType == "volume") {
      return {
        heading: "You haven't set up any Volume Alerts",
        message: "Set up volume change alerts on any stock and get updates when the volume rises or falls.",
        image: require("../../assets/icons/alerts/illustrationVolume.png"),
      };
    } else if (alertType == "research_report") {
      return {
        heading: "You haven't set up any Report Alerts",
        message: "Set up Research Report alert on a stock and get updates when any brocker releases new report on it.",
        image: require("../../assets/icons/alerts/illustrationReports.png"),
        button: "Go to Research Report",
      };
    }
  };

  renderListItem = ({ item, index }) => {
    const { userState } = this.props;

    return (
      <AlertStockCard
        {...this.props}
        item={item}
        isGuestUser={userState.isGuestUser}
        showLogin={() => this.refs.loginModal.open()}
      />
    );
  };

  renderEmptyView = () => {
    const emptyObj = this.emptyScreenData ? this.emptyScreenData : {};

    return (
      <View style={{ flex: 1 }}>
        <EmptyView
          imageSrc={emptyObj.image}
          heading={emptyObj.heading}
          message={emptyObj.message}
          button={emptyObj.button}
          onButtonPress={() => this.props.navigation.navigate("Reports")}
        />
      </View>
    );
  };

  renderList = () => {
    const { stock, refreshing, isLoadingMore } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        horizontal={false}
        data={stock}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
        refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
        onEndReached={this.onLoadMore}
        onEndReachedThreshold={0.7}
        onScroll={this.handleScroll}
        ListFooterComponent={() => isLoadingMore && <LoadingMoreView />}
      />
    );
  };

  render() {
    const { isFirstCallComplete, stock, showScrollToTop } = this.state;
    const showEmptyView = isFirstCallComplete && stock.length == 0;

    return (
      <View style={{ flex: 1, backgroundColor: colors.whiteTwo }}>
        {this.renderToolbar()}
        {showEmptyView ? this.renderEmptyView() : this.renderList()}

        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        <LoginModal ref="loginModal" {...this.props} />
        {!isFirstCallComplete && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
  };
};

export default connect(mapStateToProps)(AlertStockListScreen);
