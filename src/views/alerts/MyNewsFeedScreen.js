import React from "react";
import { View, FlatList, StyleSheet, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { colors } from "../../styles/CommonStyles";
import StockNewsItem from "../../components/stockDetail/news/StockNewsItem";
import { connect } from "react-redux";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import { fetcher } from "../../controllers/Fetcher";
import { MY_NEWS_FEED_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import NoMatchView from "../../components/common/NoMatchView";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class MyNewsFeedScreen extends React.PureComponent {
  // componentDidMount() {
  //   this.getData();
  // }

  state = {
    news: [],
    newsPageNumber: 1,
    newsNextPage: true,
    // refreshing: false,
    isFirstCallComplete: false,
    newsQsTime: "",
    isLoadingMore: false,
    showScrollToTop: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    const { baseRef, baseURL, appToken } = this.props;
    const { token } = this.props.userState;
    const { news, newsPageNumber, newsNextPage, newsQsTime, isLoadingMore, isFirstCallComplete } = this.state;
    const isLoading = baseRef ? baseRef.isLoading() : false;

    if (newsNextPage && (isLoading || isLoadingMore || !isFirstCallComplete)) {
      const URL = baseURL + MY_NEWS_FEED_URL;

      let body = { perPageCount: 20, pageNumber: newsPageNumber, qsTime: newsQsTime ? newsQsTime : "" };

      fetcher(
        URL,
        POST_METHOD,
        body,
        baseRef,
        appToken,
        token,
        (json) => {
          const { body, head } = json;
          const newsList = baseRef && baseRef.isLoading() ? body.newsList : news.concat(body.newsList);
          this.setState({ news: newsList });

          this.setState({
            newsQsTime: body.qsTime,
            newsNextPage: head.isNextPage,
            newsPageNumber: ++head.thisPageNumber,
            isLoadingMore: false,
          });

          baseRef && baseRef.stopLoading();
          if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        },
        (message) => {
          baseRef && baseRef.stopLoading();
          if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        }
      );
    } else {
      baseRef && baseRef.stopLoading();
      this.setState({ isLoadingMore: false });
    }
  };

  refreshData = () => {
    const { baseRef } = this.props;
    baseRef && baseRef.showLoading();

    this.setState({ newsPageNumber: 0, newsNextPage: true }, () => this.getData());
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    const { isLoadingMore, newsNextPage } = this.state;
    if (!isLoadingMore && newsNextPage) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  renderRefreshControl = () => {
    const { refreshing } = this.state;
    return <RefreshControl refreshing={refreshing} onRefresh={() => this.refreshData()} />;
  };

  renderNewsList = () => {
    const { news, isLoadingMore } = this.state;
    // console.log("news", news);

    if (news.length > 0)
      return (
        <FlatList
          ref={(ref) => (this.listRef = ref)}
          data={news}
          contentContainerStyle={styles.list}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) => <StockNewsItem {...this.props} data={item} showStock />}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          refreshControl={this.renderRefreshControl()}
          onEndReached={this.onLoadMore}
          onEndReachedThreshold={0.7}
          removeClippedSubviews
          initialNumToRender={4}
          maxToRenderPerBatch={4}
          onScroll={this.handleScroll}
          ListFooterComponent={() => isLoadingMore && <LoadingMoreView />}
        />
      );
  };

  render() {
    const { isSelected } = this.props;
    const { isFirstCallComplete, news, showScrollToTop } = this.state;

    const isEmpty = isFirstCallComplete && news.length == 0;
    // const heading = "You haven’t set up any Screener alerts";
    // const message = "Set up alerts on your preferred screeners and get updates on entries, exits and active stocks";

    if (isSelected) {
      return (
        <View style={styles.container}>
          {isEmpty ? <NoMatchView heading={"No Results Found"} /> : this.renderNewsList()}
          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};
export default connect(mapStateToProps, null, null, { forwardRef: true })(MyNewsFeedScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    backgroundColor: colors.white,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    marginHorizontal: wp(0),
    backgroundColor: colors.greyish,
  },
});
