import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ScrollView, RefreshControl, Platform } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import Toast from "react-native-easy-toast";
import ChartView from "react-native-highcharts";
import { colors, globalStyles } from "../../styles/CommonStyles";
import SuperstarPortfolioCard from "../../components/superstar/SuperstarPortfolioCard";
import { sortObjectList, mapHeadersData, numberWithCommas } from "../../utils/Utils";
import { BAR_CHART_CONFIG, LINE_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";
import SuperstarQuarterList from "../../components/superstar/SuperstarQuarterList";

import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";

import { SortOptionView } from "../../components/common/SortOptionView";
import StockHoldingHistoryBSheet from "../../components/common/StockHoldingHistoryBSheet";
import LoadingView from "../../components/common/LoadingView";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import LoginModal from "../../components/common/LoginModal";
import { fetcher } from "../../controllers/Fetcher";
import { SUPERSTAR_PORTFOLIO_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import EmptyView from "../../components/common/EmptyView";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class SuperstarPortfolioScreen extends React.PureComponent {
  holdingListRef = null;
  chartConfigs = {};

  state = {
    superstarId: -1,
    selectedSort: "Holding %",
    sortKey: "Holding Percent",
    isSortAscending: false,
    stocks: [],
    lineChartData: [],
    barChartData: {},
    dateList: [],
    quarterString: "",
    quarterMonth: "",
    totalNetWorth: 0,
    refreshing: false,
    isFirstCallComplete: false,
    currentPosition: 0,
    showSearchWarning: false,
    selectedStock: {},
    showScrollToTop: false,
  };

  componentDidMount() {
    this.getProfileData();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  // componentWillUpdate(newProps, newState) {
  //   newProps.isSelected && newState.stocks.length == 0 && this.getProfileData();
  // }

  getProfileData = () => {
    const { navigation, query, superstarId, userState, baseURL, appToken } = this.props;
    const URL = baseURL + SUPERSTAR_PORTFOLIO_URL;
    // const { selectedSort, sortKey } = this.state;

    const superstarString = navigation.getParam("superstarString", "");
    // const hideWarning = navigation.getParam("hideWarning", false);

    const queryString = query.length > 0 ? query : superstarString;
    if (superstarId != -1) this.setState({ superstarId: superstarId });

    this.setState({ refreshing: true });

    let body = { QuarterString: this.state.quarterString };
    body = queryString.length > 0 ? { ...body, searchString: queryString } : { ...body, superstarId: superstarId };

    fetcher(
      URL,
      POST_METHOD,
      body,
      null,
      appToken,
      userState.token,
      (json) => {
        let data = this.mapApiData(json.body);
        this.setApiData(data, queryString);
      },
      (error) => {
        this.stopLoading();
      }
    );
  };

  mapApiData = (json) => {
    // let barChartColumns = json.chartData.increase[0]
    let holdingIncrease = [];
    let holdingDecrease = [];

    if (json.chartData) {
      holdingIncrease = json.chartData.increase ? json.chartData.increase : [];
      holdingDecrease = json.chartData.decrease ? json.chartData.decrease : [];
    }
    let netWorthData = json.netWorthData ? json.netWorthData : [];

    holdingIncrease.shift();
    holdingDecrease.shift();
    // netWorthData.shift();

    let tableHeaders = json.tableHeaders;
    let tableData = json.tableData;
    let stocks = mapHeadersData(tableHeaders, tableData);

    // if (stocks.length > 0)
    return {
      stocks: stocks,
      barChart: { increase: holdingIncrease, decrease: holdingDecrease },
      netWorthData: netWorthData,
      dateList: json.dateList,
      quarterString: json.quarterString,
      totalNetWorth: json.totalNetWorth,
      showSearchWarning: "resultTruncated" in json ? json.resultTruncated : false,
    };
    // else {
    //   this.setQuarterString(json.dateList.reverse()[1]);
    // }
  };

  setApiData = (data, queryString) => {
    const { navigation } = this.props;
    const { selectedSort, sortKey } = this.state;

    let netWorthData =
      Array.isArray(data.netWorthData) &&
      data.netWorthData.length > 1 &&
      Array.isArray(data.netWorthData[1]) &&
      data.netWorthData[1].length > 0
        ? data.netWorthData
        : [];
    // const hideWarning = navigation.getParam("hideWarning", false);

    // const showLineCharts = data.lineChart.length > 0;
    // const showBarCharts = Object.keys(data.barChart).length > 0;

    // this.barChartConfig.increase =
    //   showBarCharts && data.barChart.increase
    //     ? this.getBarChartConfigs(data.barChart.increase, colors.green)
    //     : BAR_CHART_CONFIG;

    // this.barChartConfig.decrease =
    //   showBarCharts && data.barChart.decrease
    //     ? this.getBarChartConfigs(data.barChart.decrease, colors.red)
    //     : BAR_CHART_CONFIG;

    // this.lineChartConfig = showLineCharts ? this.getLineChartConfigs(data.lineChart) : LINE_CHART_CONFIG;

    const sortedData = !!selectedSort && !!sortKey ? this.sortNewData(data.stocks) : data.stocks;

    if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    // if (queryString.length > 0 && !hideWarning) this.setState({ searchWarning: true });
    // else this.setState({ searchWarning: false });

    this.setState({
      stocks: sortedData,
      lineChartData: netWorthData,
      barChartData: data.barChart,
      dateList: data.dateList.reverse(),
      quarterString: data.quarterString,
      refreshing: false,
      selectedStock: {},
    });
  };

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;

    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  sortNewData = (data) => {
    const { selectedSort, sortKey, isSortAscending } = this.state;
    let sortedData = sortObjectList(data, sortKey, isSortAscending);
    return selectedSort == "% Change" ? this.orderChangeInStock(sortedData) : sortedData;
  };

  setQuarterString = (quarter) => {
    if (quarter[1] != this.state.quarterString) {
      this.refs.toast.show("Quarter Changed", 1000);
      this.setState({ quarterString: quarter[1], quarterMonth: quarter[0] }, () => {
        this.onRefresh();
      });
    }
  };

  getBarChartConfigs = (key, data) => {
    // if (!this.chartConfigs[key]) {
    let seriesData = [];
    let labels = [];
    let dataLabels = [];
    let config = JSON.parse(JSON.stringify(BAR_CHART_CONFIG));
    let barColor = key == "increase" ? colors.green : colors.red;

    data.slice(0, 5).forEach((item) => {
      let holding = item[1];
      let formattedLabel = !isNaN(holding) ? numberWithCommas(holding, true, true) : holding;

      dataLabels.push(formattedLabel);
      labels.push(item[0]);
      seriesData.push(holding);
    });

    config.series[0].data = seriesData;
    config.series[0].color = barColor;
    config.xAxis[0].categories = labels;
    config.xAxis[1].categories = dataLabels;
    config.xAxis[1].labels = {
      formatter: function () {
        var color = this.value >= 0 ? "#31a745" : "#e3352b";
        return `<span style="color:${color};font-size:12px;font-weight:500">${this.value}%</span>`;
      },
    };

    // this.chartConfigs[key] = config;
    return config;
    // } else return this.chartConfigs[key];
  };

  getLineChartConfigs = (data) => {
    let key = "line";
    if (!this.chartConfigs[key]) {
      let seriesData = [];
      let labels = [];
      let config = JSON.parse(JSON.stringify(LINE_CHART_CONFIG));

      data
        .slice(1)
        .slice(-5)
        .forEach((item) => {
          labels.push(item[0]);
          seriesData.push({ y: item[1], label: item[2] });
        });

      config.xAxis.categories = labels;
      config.series[0].data = seriesData;
      config.series[0].color = colors.primary_color;
      config.tooltip = {
        formatter: function () {
          return this.x + "<br><b>" + this.y + "</b>";
        },
      };

      this.chartConfigs[key] = config;
      return config;
    } else return this.chartConfigs[key];
  };

  onRefresh = () => {
    this.setState({ refreshing: true }, () => this.getProfileData());
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState(
        { currentPosition: viewableItems[0].index },
        () =>
          this.holdingListRef && this.holdingListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  // renderFooter = () => {
  // return(
  //   <View style={styles.listEndFooter}>
  //     <Text style={styles.listEndFooterText}>That's all folks!</Text>
  //   </View>
  // )

  //   return (
  //     <TouchableOpacity>
  //       <View style={styles.footerButton}>
  //         <Text style={styles.footerButtonText}>Load More (126 remaining)</Text>
  //       </View>
  //     </TouchableOpacity>
  //   );
  // };

  setSortOrder = (label, key) => {
    const { stocks, isSortAscending, selectedSort } = this.state;

    const sortOrder = label == selectedSort ? !isSortAscending : false;
    const sortedData = sortObjectList(stocks, key, sortOrder);
    const orderedData = label == "% Change" ? this.orderChangeInStock(sortedData, sortOrder) : sortedData;

    this.setState({ selectedSort: label, sortKey: key, isSortAscending: sortOrder });
    this.setState({ stocks: orderedData });
  };

  orderChangeInStock = (stocks, isSortAscending) => {
    let sortedStocks = [];
    let belowFirstTime = [];
    let fillingAwaited = [];
    let newStocks = [];

    stocks.forEach((stock) => {
      const value = stock["Change from Previous Qtr"];

      if (isNaN(value)) {
        if (value == "Below 1% First Time") belowFirstTime.push(stock);
        else if (value == "Filing Awaited" || value == "Filing awaited for current qtr") fillingAwaited.push(stock);
        else if (value == "NEW" || value == "New") newStocks.push(stock);
      } else sortedStocks.push(stock);
    });

    return isSortAscending
      ? [...newStocks, ...sortedStocks, ...belowFirstTime, ...fillingAwaited]
      : [...fillingAwaited, ...belowFirstTime, ...sortedStocks, ...newStocks];
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  renderPageIndicator = () => {
    const { currentPosition } = this.state;

    return (
      <View style={styles.pageIndicator}>
        <View style={currentPosition == 0 ? styles.pageIndicatorActive : styles.pageIndicatorInactive} />
        <View style={currentPosition == 1 ? styles.pageIndicatorActive : styles.pageIndicatorInactive} />
      </View>
    );
  };

  renderSortingOption = (label, key) => {
    const { selectedSort, isSortAscending } = this.state;

    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={selectedSort}
        ascending={isSortAscending}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
      />
    );
  };

  renderSortingOptionsRow = () => {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={globalStyles.sortOptionRow}>
          {this.renderSortingOption("Holding Value", "Holding Value (Rs.)")}
          {this.renderSortingOption("Holding %", "Holding Percent")}
          {this.renderSortingOption("% Change", "Change from Previous Qtr")}
        </View>
      </ScrollView>
    );
  };

  renderBarCharts = ({ item, index }) => {
    const { barChartData } = this.state;
    const showCharts = barChartData[item].length > 0;

    if (showCharts) {
      const cardLabel = item == "increase" ? "% Increase in Holding" : "% Decrease in Holding";
      // const barColor = item == "increase" ? colors.green : colors.red;

      // const chartConfig =
      //   index == 0
      //     ? this.getBarChartConfigs(barChartData.increase, colors.green)
      //     : this.getBarChartConfigs(barChartData.decrease, colors.red);

      return (
        <View style={[styles.chartCard, styles.barChartCard]}>
          <Text style={styles.barChartLabel}>{cardLabel}</Text>
          <ChartView
            style={[styles.chartContainer, styles.barChartContainer]}
            config={this.getBarChartConfigs(item, barChartData[item])}
            options={CHART_OPTIONS}
            // stock={true}
          />
        </View>
      );
    }
  };

  renderQuarterButton = (quarter) => {
    const isSelected = this.state.quarterString == quarter[1];
    const buttonStyle = isSelected ? styles.quarterButtonActive : styles.quarterButton;
    const buttonTextStyle = isSelected ? styles.quarterButtonTextActive : styles.quarterButtonText;
    const currentQuarter = quarter[0] ? quarter[0] : "";

    return (
      <TouchableOpacity onPress={() => this.setQuarterString(quarter)}>
        <View style={buttonStyle}>
          <Text style={buttonTextStyle}>{currentQuarter}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderQuarterButtonsRow = () => {
    const { dateList, quarterMonth, quarterString } = this.state;

    if (dateList.length > 0) {
      let dateData = JSON.parse(JSON.stringify(dateList));
      let date1 = dateData[0];
      let date2 = dateData[1];
      let newSelectedQuarter = [quarterMonth, quarterString];
      let date3 = quarterString == date1[1] && quarterString != date2[1] ? date2 : newSelectedQuarter;

      // console.log(date2 + " " + date3);

      return (
        <View style={styles.quarterButtonsContainer}>
          {this.renderQuarterButton(dateData[0])}
          {this.renderQuarterButton(date3)}

          <TouchableOpacity onPress={() => this.refs.QuarterList.open(quarterString)}>
            <Text style={styles.showMore}>Show more</Text>
          </TouchableOpacity>
        </View>
      );
    }
  };

  renderBarChartList = () => {
    const { barChartData, dateList } = this.state;

    let showTitle = dateList.length > 0;

    const showPageIndicator =
      Object.keys(barChartData).length > 0 && barChartData.increase.length > 0 && barChartData.decrease.length > 0;

    const showCharts =
      Object.keys(barChartData).length > 0 && (barChartData.increase.length > 0 || barChartData.decrease.length > 0);

    return (
      <View>
        {showTitle && <Text style={styles.heading}>CHANGE IN HOLDING</Text>}

        <View style={styles.quarterListContainer}>
          {this.renderQuarterButtonsRow()}
          {showPageIndicator && this.renderPageIndicator()}
        </View>

        {showCharts && (
          <FlatList
            ref={(ref) => (this.holdingListRef = ref)}
            data={Object.keys(barChartData)}
            extraData={this.state}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            contentContainerStyle={styles.barChartList}
            renderItem={this.renderBarCharts}
            keyExtractor={(item, index) => index.toString()}
            removeClippedSubviews={false}
            onViewableItemsChanged={this._onViewableItemsChanged}
            viewabilityConfig={this._viewabilityConfig}
            removeClippedSubviews
            initialNumToRender={3}
            maxToRenderPerBatch={5}
          />
        )}
      </View>
    );
  };

  renderLineChart = () => {
    const { lineChartData } = this.state;
    // const chartConfig = lineChartData.length > 0 ? this.getLineChartConfigs() : LINE_CHART_CONFIG;

    if (lineChartData.length > 1)
      return (
        <View>
          <Text style={styles.heading}>NET WORTH HISTORY (IN CR)</Text>
          <View style={[styles.chartCard, styles.linChartCard]}>
            <ChartView
              style={[styles.chartContainer, styles.lineChartContainer]}
              config={this.getLineChartConfigs(lineChartData)}
              options={CHART_OPTIONS}
              // stock={true}
            />
          </View>
        </View>
      );
  };

  showStockHoldingHistory = (stock) => {
    this.setState({ selectedStockId: stock }, () => {
      this.refs.StockHoldingHistory.openDialog();
    });
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (stock) => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  // showLogin = () => {
  //   this.refs.loginModal.open();
  // };

  renderStockListItem = ({ item, index }) => {
    const { activeWatchlist, userState } = this.props;
    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    // console.log(item.stockId);

    return (
      <SuperstarPortfolioCard
        {...this.props}
        stock={item}
        showHistory={this.showStockHoldingHistory}
        showAlerts={this.showAlerts}
        showWatchlist={this.showWatchlist}
        watchlistBtnState={isWatchlistActive}
        showPortfolio={this.showPortfolio}
        // isGuestUser={userState.isGuestUser}
        // showLogin={this.showLogin}
      />
    );
  };

  renderStockList = () => {
    const { stocks } = this.state;

    if (stocks.length > 0)
      return (
        <View>
          <Text style={styles.heading}>PUBLIC SHAREHOLDING</Text>
          <Text style={styles.sortBy}>Sort By</Text>
          {this.renderSortingOptionsRow()}

          <FlatList
            {...this.props}
            data={stocks}
            extraData={{ ...this.props, stocks }}
            showsVerticalScrollIndicator={false}
            contentContainerStyle={styles.list}
            renderItem={this.renderStockListItem}
            keyExtractor={(item, index) => index.toString()}
          />
        </View>
      );
  };

  renderSearchWarning = () => {
    if (this.state.searchWarning)
      return (
        <View style={styles.searchWarningContainer}>
          <Text style={styles.searchWarning}>
            Results truncated because the query was too generic, try choosing a longer name.
          </Text>
        </View>
      );
  };

  render() {
    const {
      stocks,
      isFirstCallComplete,
      refreshing,
      dateList,
      selectedStock,
      selectedStockId,
      superstarId,
      barChartData,
      lineChartData,
      showScrollToTop,
    } = this.state;

    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected, navigation } = this.props;

    if (isSelected) {
      let holdingId = navigation.getParam("holdingId", "");

      let showEmptyState =
        isFirstCallComplete &&
        lineChartData.length <= 1 &&
        Object.keys(barChartData).length > 0 &&
        // (barChartData.increase.length > 0 || barChartData.decrease.length > 0) &&
        stocks.length == 0;

      return (
        <View style={styles.container}>
          {/* {stocks.length > 0 && ( */}
          <ScrollView
            ref={(ref) => (this.listRef = ref)}
            onScroll={this.handleScroll}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl refreshing={isFirstCallComplete && refreshing} onRefresh={this.onRefresh} />
            }
          >
            {this.renderSearchWarning()}
            {this.renderLineChart()}
            {this.renderBarChartList()}
            {this.renderStockList()}
          </ScrollView>

          <SuperstarQuarterList ref="QuarterList" {...this.props} data={dateList} onPress={this.setQuarterString} />

          {/* stock={selectedStockId}
            superstarId={superstarId}
            check holding Id
             */}
          <StockHoldingHistoryBSheet
            ref="StockHoldingHistory"
            {...this.props}
            type={holdingId ? "stock" : "superstar"}
            stock={selectedStockId}
            superstarId={superstarId}
            holdingId={holdingId}
            // data={dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockAlertsModal
            ref="stockAlerts"
            stock={selectedStock}
            {...this.props}
            // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
            // data={dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          {showEmptyState && (
            <EmptyView
              style={{ top: hp(18) }}
              heading="No Superstars Found"
              message="Spell check the superstar you’re looking for or try different keywords for better results"
              imageSrc={require("../../assets/icons/no-match-grey.png")}
            />
          )}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          <LoginModal ref="loginModal" {...this.props} />
          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {stocks.length == 0 && !isFirstCallComplete && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(SuperstarPortfolioScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  heading: {
    marginLeft: wp(3),
    marginBottom: hp(1),
    marginTop: hp(3.5),
    fontSize: RF(2.4),
    fontWeight: "300",
    letterSpacing: 0,
    color: colors.black,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.primary_color,
  },
  sortBy: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginLeft: "3%",
    marginTop: "3%",
  },
  performanceView: {
    minHeight: hp(3),
    backgroundColor: colors.green,
    alignItems: "center",
    justifyContent: "center",
  },
  performanceDetail: {
    fontSize: RF(1.8),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
  },
  sortOptionRow: {
    flexDirection: "row",
    marginHorizontal: wp(1),
    marginVertical: wp(2),
  },
  sortOptionContainer: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    borderColor: colors.primary_color,
    borderWidth: 1,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  sortOptionContainerSelected: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
  },
  sortOption: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sortOptionName: {
    flex: 1,
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  sortOptionNameSelected: {
    flex: 1,
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.white,
  },
  sortIcon: {
    width: wp(2),
    height: wp(2),
    resizeMode: "contain",
  },
  sortOrder: {
    fontSize: RF(1.6),
    color: colors.white,
    opacity: 0.7,
  },
  hide: {
    display: "none",
  },
  listEndFooter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(1),
  },
  listEndFooterText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.pinkishGrey,
  },
  chartCard: {
    overflow: "hidden",
    // height: hp(35),
    padding: wp(3),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: 1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  chartContainer: {
    height: hp(32),
    // marginBottom: hp(-3),
  },
  linChartCard: {
    width: wp(94),
    margin: wp(3),
  },
  lineChartContainer: {
    width: wp(90),
    // height: hp(32)
    // marginBottom: hp(-3),
  },
  barChartCard: {
    // overflow: "hidden",
    width: wp(92),
    marginRight: 9,
    marginVertical: wp(3),
  },
  barChartContainer: {
    width: wp(82),
  },
  barChartList: {
    paddingHorizontal: wp(3),
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(1.8),
    height: wp(1.8),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.3),
    height: wp(1.3),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  quarterListContainer: {
    flex: 1,
    padding: wp(3),
    flexDirection: "row",
    alignItems: "center",
  },
  barChartLabel: {
    flex: 1,
    textAlign: "left",
    paddingTop: wp(1),
    marginBottom: hp(3),
    fontSize: RF(2.2),
    fontWeight: "300",
    color: colors.black,
  },
  quarterButton: {
    minWidth: wp(20),
    paddingHorizontal: wp(2),
    paddingVertical: wp(2),
    marginRight: wp(3),
    borderRadius: 5,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  quarterButtonText: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
  },
  quarterButtonActive: {
    minWidth: wp(20),
    paddingHorizontal: wp(2),
    paddingVertical: wp(2),
    marginRight: wp(3),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
  },
  quarterButtonTextActive: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.white,
  },
  quarterButtonsContainer: {
    flex: 1,
    flexDirection: "row",
    marginRight: wp(3),
    alignItems: "center",
    // justifyContent: 'center',
  },
  showMore: {
    fontSize: RF(1.9),
    fontWeight: "300",
    color: colors.primary_color,
    paddingVertical: wp(2),
    paddingRight: wp(2),
  },
  searchWarningContainer: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
    backgroundColor: "rgba(162, 9, 0, 0.1)", //colors.red
    // opacity: 0.1,
  },
  searchWarning: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.deepRed,
  },
});
