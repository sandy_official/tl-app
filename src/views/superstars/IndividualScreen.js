import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList, ScrollView, Platform } from "react-native";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { sortObjectList } from "../../utils/Utils";
import { SortOptionView } from "../../components/common/SortOptionView";
import LoadingView from "../../components/common/LoadingView";
import { APIDataFormatter } from "../../utils/APIDataFormatter";
import ScrollToTopView from "../../components/common/ScrollToTopView";

export default class IndividualScreen extends React.PureComponent {
  state = {
    superstars: [],
    selectedSort: "Networth",
    isSortAscending: false,
    refreshing: false,
    isFirstCallComplete: false,
    showScrollToTop: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  // onRefresh = () => {
  //   this.setState({ refreshing: true }, () => this.props.refresh());
  // };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  /**
   * Sorting Data
   * @param {string} label sort option label
   * @param {string} key sort option key received from an API
   */
  setSortOrder = (label, key) => {
    const { data } = this.props;
    const { selectedSort, isSortAscending } = this.state;

    const isSortActive = label == selectedSort ? !isSortAscending : false;
    this.setState({ selectedSort: label, isSortAscending: isSortActive });

    const sortedData = sortObjectList(data, key, isSortActive);
    this.setState({ superstars: sortedData });
  };

  renderSortingOption = (label, key) => {
    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={this.state.selectedSort}
        ascending={this.state.isSortAscending}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
      />
    );
  };

  renderSortingOptionsRow = () => {
    return (
      <View style={globalStyles.sortOptionRow}>
        {this.renderSortingOption("Networth", "net worth")}
        {this.renderSortingOption("Companies", "Companies Held")}
      </View>
    );
  };

  renderListItem = ({ item, index }) => {
    const { gotoSuperstar } = this.props;

    const superstarObj = {
      superstarId: item.superstarId,
      superstarName: item["Superstar name"],
      superstarString: "",
    };

    const superstarName = item["Superstar name"];
    const netWorth = APIDataFormatter("net worth", item["net worth"]);
    const companiesHeld = APIDataFormatter("Companies Held", item["Companies Held"]);

    return (
      <TouchableOpacity onPress={() => gotoSuperstar(superstarObj)}>
        <View style={styles.listItem}>
          <Text style={[styles.investorName, styles.left]} numberOfLines={2} ellipsizeMode="tail">
            {superstarName}
          </Text>

          <Text style={[styles.otherDetails, styles.center]} numberOfLines={2} ellipsizeMode="tail">
            {netWorth}
          </Text>

          <Text style={[styles.otherDetails, styles.right]} numberOfLines={2} ellipsizeMode="tail">
            {companiesHeld}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };

  renderColumns = () => {
    return (
      <View style={styles.columns}>
        <Text style={[styles.columnName, styles.left]}>Investor</Text>
        <Text style={[styles.columnName, styles.center]}>Net Worth</Text>
        <Text style={[styles.columnName, styles.right]}>Stocks</Text>
        {/* <Text style={[styles.columnName, styles.right]}>Companies</Text> */}
      </View>
    );
  };

  renderSuperstarList = () => {
    const { data } = this.props;

    const { superstars } = this.state;
    let superstarList = superstars.length > 0 ? superstars : data;

    return (
      <FlatList
        data={superstarList}
        extraData={{ data, superstars }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
        renderItem={this.renderListItem}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={() => this.renderColumns()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        removeClippedSubviews
        initialNumToRender={8}
        maxToRenderPerBatch={5}
      />
    );
  };

  render() {
    const { data, isSelected } = this.props;
    const { showScrollToTop } = this.state;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {/* {data.length > 0 && ( */}
          <ScrollView
            ref={(ref) => (this.listRef = ref)}
            onScroll={this.handleScroll}
            showsVerticalScrollIndicator={false}

            // refreshControl={
            //   <RefreshControl
            //     refreshing={this.state.refreshing}
            //     onRefresh={this.onRefresh}
            //   />
            // }
          >
            <Text style={[styles.sortBy]}>Sort By</Text>
            {this.renderSortingOptionsRow()}
            {this.renderSuperstarList()}
          </ScrollView>
          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        </View>
      );
    } else return null;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  sortBy: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginLeft: "3%",
    marginTop: "3%",
  },
  sortOptionRow: {
    flexDirection: "row",
    paddingHorizontal: wp(1),
    paddingVertical: wp(2),
  },
  sortOptionContainer: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    borderColor: colors.primary_color,
    borderWidth: 1,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
    alignItems: "center",
  },
  sortOptionContainerSelected: {
    height: Platform.OS == "ios" ? hp(6) : hp(7),
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(2),
    paddingHorizontal: wp(2),
    justifyContent: "center",
  },
  sortOption: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  sortOptionName: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color,
  },
  sortOptionNameSelected: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.white,
  },
  sortIcon: {
    width: wp(2.5),
    height: wp(2.5),
    resizeMode: "contain",
    marginLeft: 10,
  },
  sortOrder: {
    fontSize: RF(1.6),
    color: colors.white,
    opacity: 0.7,
  },
  hide: {
    display: "none",
  },
  columns: {
    flexDirection: "row",
    paddingHorizontal: wp(3),
    paddingVertical: wp(5),
    alignItems: "center",
    backgroundColor: colors.whiteTwo,
    marginTop: hp(1.5),
  },
  columnName: {
    fontSize: RF(2.2),
    fontWeight: "700",
    color: colors.greyish,
  },
  left: {
    flex: 1.5,
    textAlign: "left",
  },
  center: {
    flex: 0.8,
    textAlign: "center",
  },
  right: {
    flex: 0.4,
    textAlign: "right",
  },
  listItem: {
    paddingHorizontal: wp(3),
    paddingVertical: hp(3.5),
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: colors.white,
  },
  investorName: {
    fontSize: RF(2.1),
    fontWeight: "300",
    color: colors.primary_color,
  },
  otherDetails: {
    fontSize: RF(2.1),
    color: colors.black,
    marginLeft: wp(3),
  },
  divider: {
    flex: 1,
    height: 1,
    backgroundColor: colors.greyish,
    opacity: 0.2,
  },
});
