import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, ScrollView, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../../styles/CommonStyles";
import BulkDealCard from "../../components/superstar/BulkDealCard";
import { sortObjectList } from "../../utils/Utils";
import Toast from "react-native-easy-toast";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { SortOptionView } from "../../components/common/SortOptionView";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import LoadingView from "../../components/common/LoadingView";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import LoginModal from "../../components/common/LoginModal";
import { fetcher } from "../../controllers/Fetcher";
import { SUPERSTAR_BB_DEALS_URL } from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";
import NoMatchView from "../../components/common/NoMatchView";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class BulkDealScreen extends React.PureComponent {
  state = {
    stocks: [],
    refreshing: false,
    isFirstCallComplete: false,
    selectedStock: {},
    superstarId: -1,
    selectedSort: "Date",
    isSortAscending: false,
    sortKey: "date",
    showScrollToTop: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  // componentWillUpdate(newProps, newState) {
  //   newProps.isSelected && newState.stocks.length == 0 && this.getData();
  // }

  getData = () => {
    const { navigation, superstarId, query, userState, baseURL, appToken } = this.props;
    const { selectedSort, sortKey, isSortAscending, isFirstCallComplete } = this.state;
    const URL = baseURL + SUPERSTAR_BB_DEALS_URL;
    const superstarString = navigation.getParam("superstarString", "");
    const queryString = query.length > 0 ? query : superstarString;

    this.setState({ refreshing: true });

    let body = queryString.length > 0 ? { searchString: queryString } : { superstarId: superstarId };

    fetcher(
      URL,
      POST_METHOD,
      body,
      null,
      appToken,
      userState.token,
      (json) => {
        let data = this.mapApiData(json.body);
        let sortedData =
          !!selectedSort && !!sortKey ? sortObjectList(data.stocks, sortKey, isSortAscending) : data.stocks;

        this.setState({ stocks: sortedData });
        this.stopLoading();
      },
      (error) => {
        this.stopLoading();
      }
    );
  };

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;

    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false });
  };

  mapApiData = (json) => {
    // let barChartColumns = json.chartData.increase[0]
    let holdingIncrease = [];
    let holdingDecrease = [];

    if (json.chartData) {
      holdingIncrease = json.chartData.increase ? json.chartData.increase : [];
      holdingDecrease = json.chartData.decrease ? json.chartData.decrease : [];
    }
    let netWorthData = json.netWorthData ? json.netWorthData : [];

    holdingIncrease.shift();
    holdingDecrease.shift();
    netWorthData.shift();

    return {
      stocks: mapStocksData(json),
      barChart: { increase: holdingIncrease, decrease: holdingDecrease },
      lineChart: netWorthData,
      dateList: json.dateList,
      quarterString: json.quarterString,
      totalNetWorth: json.totalNetWorth,
    };
  };

  onRefresh = () => {
    this.setState({ refreshing: true }, () => this.getData());
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderFooter = () => {
    // return(
    //   <View style={styles.listEndFooter}>
    //     <Text style={styles.listEndFooterText}>That's all folks!</Text>
    //   </View>
    // )

    return (
      <TouchableOpacity>
        <View style={styles.footerButton}>
          <Text style={styles.footerButtonText}>Load More</Text>
        </View>
      </TouchableOpacity>
    );
  };

  setSortOrder = (label, key) => {
    const { selectedSort, isSortAscending, stocks } = this.state;

    const sortOrder = label == selectedSort ? !isSortAscending : false;
    const sortedData = sortObjectList(stocks, key, sortOrder);

    this.setState({ selectedSort: label, sortKey: key, isSortAscending: sortOrder });
    this.setState({ stocks: sortedData });
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;

    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  renderSortingOption = (label, key) => {
    const { selectedSort, isSortAscending } = this.state;

    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={selectedSort}
        ascending={isSortAscending}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
      />
    );
  };

  renderSortingOptionsRow = () => {
    return (
      <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
        <View style={globalStyles.sortOptionRow}>
          {this.renderSortingOption("Date", "date")}
          {this.renderSortingOption("Actions", "get_action_display")}
          {this.renderSortingOption("%Traded", "traded_percent")}
          {this.renderSortingOption("Quantity", "quantity")}
          {this.renderSortingOption("Price", "price")}
        </View>
      </ScrollView>
    );
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (stock) => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderListItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;
    const stockId = item.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;

    return (
      <BulkDealCard
        {...this.props}
        stock={item}
        showAlerts={this.showAlerts}
        showWatchlist={this.showWatchlist}
        watchlistBtnState={isWatchlistActive}
        showPortfolio={this.showPortfolio}
      />
    );
  };

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected } = this.props;
    const { stocks, refreshing, isFirstCallComplete, selectedStock, showScrollToTop } = this.state;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {stocks.length > 0 && (
            <ScrollView
              ref={(ref) => (this.listRef = ref)}
              onScroll={this.handleScroll}
              showsVerticalScrollIndicator={false}
              refreshControl={<RefreshControl refreshing={refreshing} onRefresh={this.onRefresh} />}
            >
              <Text style={styles.sortBy}>Sort By</Text>
              {this.renderSortingOptionsRow()}

              <FlatList
                data={stocks}
                extraData={{ ...this.props, stocks }}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={styles.list}
                renderItem={this.renderListItem}
                keyExtractor={(item, index) => index.toString()}
                // ListFooterComponent={() => this.renderFooter()}
                removeClippedSubviews
                initialNumToRender={4}
                maxToRenderPerBatch={5}
              />
            </ScrollView>
          )}

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockAlertsModal
            ref="stockAlerts"
            {...this.props}
            stock={selectedStock}
            // addedTo={name => this.refs.toast.show("Added to Primary", 1000)}
            // data={dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          <LoginModal ref="loginModal" {...this.props} />
          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {stocks.length == 0 && !isFirstCallComplete && <LoadingView />}
          {isFirstCallComplete && stocks.length == 0 && <NoMatchView message="No Results found" />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(BulkDealScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    paddingHorizontal: "3%",
  },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.primary_color,
  },
  sortBy: {
    fontSize: RF(1.5),
    color: colors.steelGrey,
    marginLeft: "3%",
    marginTop: "3%",
  },
  performanceView: {
    minHeight: hp(3),
    backgroundColor: colors.green,
    alignItems: "center",
    justifyContent: "center",
  },
  performanceDetail: {
    fontSize: RF(1.8),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
  },
  listEndFooter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(1),
  },
  listEndFooterText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.pinkishGrey,
  },
});
