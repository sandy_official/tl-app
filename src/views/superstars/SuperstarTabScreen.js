import React from "react";
import { View, StyleSheet } from "react-native";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { globalStyles } from "../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import IndividualScreen from "./IndividualScreen";
import SuperstarAlert from "../../components/superstar/SuperstarAlert";
import Toast from "react-native-easy-toast";
import { loadSuperstar } from "../../actions/SuperstarActions";
import { connect } from "react-redux";
import TabComponent from "../../components/common/TabComponent";
import BaseComponent from "../../components/common/BaseComponent";
import { fetcher } from "../../controllers/Fetcher";
import { GET_METHOD } from "../../utils/Constants";
import { GET_ACTIVE_SUPERSTAR_ALERTS_URL } from "../../utils/ApiEndPoints";

class SuperstarTabScreen extends React.PureComponent {
  /**
   * State object with initial data
   */
  state = {
    alertButtonState: { daily: false, weekly: false },
    selectedTab: 0,
    isRefreshing: false,
    baseRef: null,
  };

  /**
   * Set navigation listener on component mount
   */
  componentDidMount() {
    this.setNavigationListener();
  }

  /**
   * Remove navigation listener on component unmount
   */
  componentWillUnmount() {
    this.navListener.remove();
  }

  /**
   * Set navigation listener to call API
   * Get superstar list from an API and store that into Redux store
   * API is called from an loadSuperstar(token) action
   */
  setNavigationListener = () => {
    const { navigation, loadSuperstar, userState, baseURL, appToken } = this.props;

    this.navListener = navigation.addListener("willFocus", (payload) => {
      loadSuperstar(baseURL, appToken, userState.token);
      this.checkSuperstarAlerts();
    });
  };

  /**
   * Check for superstar alerts status from an API
   * call only for subscriber user
   */
  checkSuperstarAlerts = () => {
    const { baseURL, appToken } = this.props;
    const { token, isSubscriber } = this.props.userState;
    const { baseRef } = this.state;
    const URL = baseURL + GET_ACTIVE_SUPERSTAR_ALERTS_URL;

    if (isSubscriber) {
      fetcher(
        URL,
        GET_METHOD,
        null,
        baseRef,
        appToken,
        token,
        (json) => {
          let alertType = json.body.activeAlert;

          if (alertType) {
            if (alertType == "daily") this.setState({ alertButtonState: { daily: true, weekly: false } });
            else if (alertType == "weekly") this.setState({ alertButtonState: { daily: false, weekly: true } });
          }
        },
        (error) => {
          //Will receive error message
        }
      );
    }
  };

  /**
   * Set selected tab position from tab component
   * @param {object} tabObject //tab position
   */
  setTabSelection = (obj) => {
    this.setState({ selectedTab: obj.position });
  };

  /**
   * Goto superstar details screen
   * @param {object} superstar //superstar detail object
   */
  openSuperstar = (obj) => {
    const { navigate } = this.props.navigation;
    navigate("SuperstarInfo", obj);
  };

  /**
   * Goto superstar search screen
   */
  openSearch = () => {
    const { navigate } = this.props.navigation;
    navigate("SuperstarSearch");
  };

  /**
   * Show toast message for superstar alert action done by user
   */
  setAlerts = (isRemoved) => {
    const { baseRef } = this.state;

    if (isRemoved != null) {
      const message = isRemoved ? "Superstar Alert Removed" : "Superstar Alert Added";
      baseRef && baseRef.showToast(message);
    }
  };

  /**
   * Show superstar alert modal
   * It checks the type of user and then show superstar alerts modal
   */
  showAlertsDialog = () => {
    const { isSubscriber, isGuestUser } = this.props.userState;
    const { baseRef } = this.state;

    if (isGuestUser) baseRef && baseRef.showLogin();
    else if (isSubscriber) this.refs.alertBSheet.open();
    else baseRef && baseRef.showSubscribe();
  };

  /**
   * Render toolbar with Alert & Search action
   */
  renderToolbar = () => {
    const { alertButtonState } = this.state;
    const alertIcon =
      alertButtonState.daily || alertButtonState.weekly
        ? require("../../assets/icons/alert-added-white.png")
        : require("../../assets/icons/alert-add-white.png");

    return (
      <Toolbar title="Superstars" drawerMenu {...this.props}>
        <ImageButton source={alertIcon} onPress={this.showAlertsDialog} />
        <ImageButton source={require("../../assets/icons/search-white.png")} onPress={() => this.openSearch()} />
      </Toolbar>
    );
  };
  getAnalyticsParams = () => {
    return {
      superstarId: "",
      superstarName:""
    }
  }
  /**
   * Render Tab layout
   */
  renderTabs = () => {
    const { selectedTab } = this.state;
    const { individual, institutional, fii } = this.props.superstars;
    const tabs = ["Individual", "Institutional", "FII"];

    return (
      <TabComponent analyticsParams={this.getAnalyticsParams} tabs={tabs} initialTab={0} onTabSelect={this.setTabSelection}>
        <View>
          <IndividualScreen
            {...this.props}
            data={individual}
            gotoSuperstar={this.openSuperstar}
            isSelected={selectedTab == 0}
          />
        </View>
        <View>
          <IndividualScreen
            {...this.props}
            data={institutional}
            gotoSuperstar={this.openSuperstar}
            isSelected={selectedTab == 1}
          />
        </View>
        <View>
          <IndividualScreen
            {...this.props}
            data={fii}
            gotoSuperstar={this.openSuperstar}
            isSelected={selectedTab == 2}
          />
        </View>
      </TabComponent>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <BaseComponent ref={(ref) => this.setState({ baseRef: ref })} {...this.props}>
          {this.renderTabs()}
        </BaseComponent>

        <SuperstarAlert
          ref="alertBSheet"
          {...this.props}
          buttonState={this.state.alertButtonState}
          setButtonState={(obj) => this.setState({ alertButtonState: obj })}
          // showPlans={() => this.showPlanSheet()}
          close={this.setAlerts}
        />
      </View>
    );
  }
}

mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user, superstars: state.superstar.superstars };
};

export default connect(mapStateToProps, { loadSuperstar })(SuperstarTabScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
});
