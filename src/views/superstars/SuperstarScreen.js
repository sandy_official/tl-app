import React from "react";
import { View, StyleSheet } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { globalStyles } from "../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import InsiderTradesScreen from "./InsiderTradesScreen";
import SuperstarPortfolioScreen from "./SuperstarPortfolioScreen";
import BulkDealScreen from "./BulkDealScreen";

import SuperstarAlert from "../../components/superstar/SuperstarAlert";
import SuperstarAlertNonSub from "../../components/superstar/SuperstarAlertNonSub";
import Toast from "react-native-easy-toast";
import { getSuperstarAlerts } from "../../controllers/SuperstarAlertsController";
import { connect } from "react-redux";
import LoginModal from "../../components/common/LoginModal";
import TabComponent from "../../components/common/TabComponent";

class SuperstarScreen extends React.PureComponent {
  tabs = ["Portfolio", "Bulk & Block Deals", "Insider Trades & SAST"];

  componentDidMount() {
    this.checkSuperstarAlerts();
  }

  constructor(props) {
    super(props);
    this.state = {
      showFilters: false,
      searchQuery: "",
      alertButtonState: { daily: false, weekly: false },
      selectedTab: 0,
      sId: -1,
      superstarName: "",
      isApiCalled: [false, false, false],
    };
  }

  checkSuperstarAlerts = () => {
    const { userState, baseURL, appToken } = this.props;

    getSuperstarAlerts(baseURL, appToken, userState.token, (alertType) => {
      if (alertType != "") {
        if (alertType == "daily") this.setState({ alertButtonState: { daily: true, weekly: false } });
        else if (alertType == "weekly") this.setState({ alertButtonState: { daily: false, weekly: true } });
      }
    });
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const { alertButtonState } = this.state;
    const alertIcon =
      alertButtonState.daily || alertButtonState.weekly
        ? require("../../assets/icons/alert-added-white.png")
        : require("../../assets/icons/alert-add-white.png");

    const superstarName = navigation.getParam("superstarName");
    const superstarString = navigation.getParam("superstarString");

    const title = this.state.superstarName ? this.state.superstarName : superstarName ? superstarName : superstarString;

    return (
      <Toolbar title={title} backButton {...this.props}>
        <ImageButton source={alertIcon} onPress={this.showAlertsDialog} />
        <ImageButton source={require("../../assets/icons/search-white.png")} onPress={() => this.openSearch()} />
      </Toolbar>
    );
  };

  openSearch = () => {
    this.props.navigation.push("SuperstarSearch");
  };

  showAlertsDialog = () => {
    const { isSubscriber, isGuestUser } = this.props.userState;
    if (isGuestUser) this.refs.loginModal.open();
    else if (isSubscriber) this.refs.alertBSheet.RBSheet.open();
    else this.refs.alertNonSubBSheet.RBSheet.open();
  };

  setTabSelection = (obj) => {
    const { isApiCalled } = this.state;
    const tabPosition = obj.position;

    this.setState({
      selectedTab: tabPosition,
    });

    if (!isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          // this.refs.portfolio.getData();
          break;
        case 1:
          this.refs.bulkBlock.getData();
          break;

        case 2:
          this.refs.insiderTrades.getData();
          break;

        default:
          null;
      }

      let apiCalls = this.state.isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls });
    }
  };

  openSuperstar = (obj) => {
    if (obj.superstarId != -1)
      this.setState({ searchQuery: "", sId: obj.superstarId, superstarName: obj.superstarName });
    else this.setState({ searchQuery: obj.superstarName, sId: -1, superstarName: obj.superstarName });
  };

  setAlerts = (isRemoved) => {
    if (isRemoved != null) {
      // console.log(isRemoved);
      const message = isRemoved ? "Superstar Alert Removed" : "Superstar Alert Added";

      this.refs.toast.show(message, 1000);
    }
  };

  toggleDoSearch = () => {
    this.setState({ doSearch: false });
  };
  getAnalyticsParams = () => {
    const { navigation } = this.props;
    const superstarId = navigation.getParam("superstarId");
    const superstarName = navigation.getParam("superstarName");
    const superstarString = navigation.getParam("superstarString");
    return {
      "superstarId": superstarId,
      "superstarName": superstarName,
      "superstarString": superstarString
    }
  }
  renderTabs = () => {
    const { navigation } = this.props;
    const { selectedTab } = this.state;

    const superstarId = navigation.getParam("superstarId");
    const superstarName = navigation.getParam("superstarName");
    const superstarString = navigation.getParam("superstarString");

    const sId = this.state.sId != -1 ? this.state.sId : superstarId;
    // const query = this.state.searchQuery.length > 0 ? this.state.searchQuery : sId == -1 ? "" : superstarString;
    const query = superstarString > 0 ? superstarString : superstarName ? "" : superstarString;

    // console.log(superstarId + " "  + superstarString)
    // console.log(this.state.sId + " "  + this.state.searchQuery)
    // console.log("sId :" + sId)

    return (
      <TabComponent analyticsParams={this.getAnalyticsParams} tabs={this.tabs} initialTab={0} onTabSelect={this.setTabSelection}>
        <View>
          <SuperstarPortfolioScreen
            ref="portfolio"
            superstarId={sId}
            query={query}
            {...this.props}
            isSelected={selectedTab == 0}
          />
        </View>
        <View>
          <BulkDealScreen
            ref="bulkBlock"
            superstarId={sId}
            query={query}
            {...this.props}
            isSelected={selectedTab == 1}
          />
        </View>
        <View>
          <InsiderTradesScreen
            ref="insiderTrades"
            superstarId={sId}
            query={query}
            {...this.props}
            isSelected={selectedTab == 2}
          />
        </View>
      </TabComponent>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <View style={{ flex: 1 }}>{this.renderTabs()}</View>

        <SuperstarAlert
          ref="alertBSheet"
          {...this.props}
          buttonState={this.state.alertButtonState}
          setButtonState={(obj) =>
            this.setState({
              alertButtonState: obj,
            })
          }
          close={this.setAlerts}
        />

        <SuperstarAlertNonSub
          ref="alertNonSubBSheet"
          {...this.props}
          buttonState={this.state.alertButtonState}
          setButtonState={(obj) =>
            this.setState({
              alertButtonState: obj,
            })
          }
        />

        <LoginModal ref="loginModal" {...this.props} />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(SuperstarScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
});
