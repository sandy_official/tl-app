/**
 * Libraries
 */
import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  TouchableWithoutFeedback,
  StatusBar,
  Linking,
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";

/**
 * Custom components, methods and constants
 */
import { isValidEmail } from "../../utils/Utils";
import LoadingView from "../../components/common/LoadingView";
import { colors, globalStyles } from "../../styles/CommonStyles";
import OnboardingModal from "../../components/login/OnboardingModal";
import { doLoginApi } from "../../controllers/UserController";
import { doUserLogin, loadUserDetails } from "../../actions/UserActions";
import { FORGOT_PASSWORD_URL, navigationState } from "../../utils/Constants";
import { NavigationActions } from "react-navigation";
// import { changeToken } from "../../utils/Constants";

/**
 * @constant config Static config to generate login form fields
 */
const formData = [
  // { label: "Name", key: "name" },
  { label: "Email ID", key: "email" },
  { label: "Password", key: "password" },
];

/**
 * Login screen - For login with Email-Id and password
 */
class LoginScreen extends React.PureComponent {
  /**
   * Holds list of text input references
   */
  textInputRefs = {};

  /**
   *  Setting the navigation listener for changing the statusbar properties
   */
  componentDidMount() {
    this.onWillFocusSubscription = this.props.navigation.addListener("willFocus", (payload) => {
      StatusBar.setBarStyle("light-content");
      StatusBar.setBackgroundColor(colors.primary_color);
    });
  }

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    this.onWillFocusSubscription.remove();
  }

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = {
      // name: "",
      email: "",
      password: "",
      // email: "apabreja@gmail.com ",
      // password: "trendlyne123",
      textFocused: "",
      showPassword: false,
      refreshing: false,
    };
  }

  /**
   * Open forgot password page in a browser
   */
  openForgotPassword = () => {
    Linking.openURL(FORGOT_PASSWORD_URL);
  };

  /**
   * Login with Email-Id and password
   */
  doLogin = () => {
    const { navigation, doUserLogin, baseURL, appToken, loadUserDetails } = this.props;
    const { password, email } = this.state;

    if (this.validForm()) {
      this.setState({ refreshing: true });

      /**
       * Call login API with Email-Id, password, token
       * If token received add data into the redux and redirect to Dashboard screen
       */
      doLoginApi(baseURL, appToken, email, password, (token) => {
        this.setState({ refreshing: false });

        if (token != null) {
          doUserLogin(token);

          // if (navigationState) navigation.reset([NavigationActions.navigate({ routeName: "Profile" })], 0);

          if (navigationState) {
            loadUserDetails(baseURL, appToken, token);
            navigation.navigate(navigationState);
          } else navigation.navigate("App");
        } else {
          this.refs.toast.show("Login failed");
        }
      });
    }
    // else this.refs.toast.show("Please check your inputs");
  };

  // clearForm = () => {
  //   this.setState({
  //     email: "",
  //     password: "",
  //     textFocused: "",
  //     showPassword: false
  //   });
  // };

  /**
   * Show start without features bottom sheet modal
   */
  openModal = () => this.refs.modal.RBSheet.open();

  /**
   * Goto Email signip screen
   */
  gotoSignUp = () => {
    this.props.navigation.navigate("SignUp");
  };

  /**
   * Validate login form fields
   * @returns {boolean} if valid return true else false
   */
  validForm = () => {
    const { password, email } = this.state;
    if (email.length == 0) this.refs.toast.show("Please Enter Email");
    else if (!isValidEmail(email)) this.refs.toast.show("Please Enter valid Email");
    else if (password.length == 0) this.refs.toast.show("Please Enter Password");
    else if (password.length < 6) this.refs.toast.show("Password must be at least 6 alphanumeric");
    else return true;
    return false;
  };

  /**
   * Hide or Show password
   */
  togglePassword = () => {
    const { showPassword } = this.state;
    this.setState({ showPassword: !showPassword });
  };

  /**
   * On input field focus set that field to active field
   */
  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value });
  };

  /**
   * save text field input value in this.state
   */
  setInputText = (key, value) => {
    this.setState({ [key]: value });
  };

  /**
   * Render the text input field from static config
   *
   * @param {object} inputFieldConfig input field configuration data
   * @returns {view} return input field view
   */
  renderTextInputBox = (item) => {
    const { showPassword } = this.state;

    const inputBoxStyle =
      this.state.textFocused == item.label ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;

    const showPasswordIcon = item.label == "Password";
    const passwordIcon = showPassword
      ? require("../../assets/icons/login/open-eye.png")
      : require("../../assets/icons/login/closed-eye.png");

    const passwordIconStyle = showPassword ? styles.openEye : styles.closedEye;

    return (
      <View>
        <Text style={styles.inputLabel}>{item.label}</Text>
        <View style={inputBoxStyle}>
          <TextInput
            ref={(ref) => (this.textInputRefs[item.key] = ref)}
            // placeholder={placeholder}
            // maxLength={item.length}
            numberOfLines={1}
            style={styles.textInput}
            placeholderTextColor={colors.steelGrey}
            // maxLength={100}
            secureTextEntry={showPasswordIcon && !showPassword}
            onChangeText={(value) => this.setInputText(item.key, value)}
            onFocus={() => this.toggleTextInputFocus(item.key)}
            onBlur={() => this.toggleTextInputFocus(null)}
          />

          {showPasswordIcon && (
            <TouchableOpacity onPress={this.togglePassword}>
              <Image source={passwordIcon} style={passwordIconStyle} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  /**
   * Render login form with input fields and buttons from static config
   * @returns {view} Login form
   */
  renderForm = () => {
    return (
      <View>
        {formData.map((item) => this.renderTextInputBox(item))}
        <TouchableOpacity onPress={this.openForgotPassword}>
          <Text style={styles.forgotPassword}>Forgot Password?</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.loginBtn} onPress={this.doLogin}>
          <Text style={styles.loginText}>Log In</Text>
        </TouchableOpacity>

        <TouchableWithoutFeedback onPress={this.gotoSignUp}>
          <Text style={styles.signUp}>
            New to Trendlyne?
            <Text style={styles.blueText}> Create an Account</Text>
          </Text>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  /**
   * Render Toolbar view
   * @returns {view} toolbar
   */
  renderToolbar = () => {
    const { navigation } = this.props;
    const backIcon = require("../../assets/icons/login/back-arrow-blue.png");

    return (
      <View style={styles.toolbar}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Image style={styles.backBtn} source={backIcon} />
        </TouchableOpacity>

        <TouchableOpacity style={styles.quickStartContainer} onPress={this.openModal}>
          <Text onPress={this.openModal} style={styles.quickStart}>
            Start without the best features
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * Render greeting message for users
   * @returns {view} greeting text view
   */
  renderGreeting = () => {
    return (
      <Text style={styles.helloMessage}>
        {"Hello from Trendlyne! \n"}
        <Text style={styles.greyText}>Log in to continue</Text>
      </Text>
    );
  };

  /**
   * Render method
   */
  render() {
    const { refreshing } = this.state;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          {this.renderGreeting()}
          {this.renderForm()}
        </KeyboardAwareScrollView>

        <OnboardingModal ref="modal" {...this.props} />

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken };
};

export default connect(mapStateToProps, { doUserLogin, loadUserDetails })(LoginScreen);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(6.2),
    paddingTop: wp(6.8),
  },
  toolbar: {
    flexDirection: "row",
    // alignItems: "center",
    justifyContent: "center",
    height: heightPercentageToDP(6),
  },
  backBtn: {
    marginVertical: heightPercentageToDP(1),
    marginRight: wp(4),
  },
  quickStartContainer: {
    flex: 1,
    justifyContent: "center",
  },
  quickStart: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    textAlign: "right",
  },
  helloMessage: {
    fontSize: RF(3.3),
    fontWeight: "300",
    color: colors.primary_color,
    lineHeight: wp(9),
    // marginTop: wp(8),
    marginTop: wp(6.6),
    marginBottom: wp(10),
  },
  greyText: {
    color: colors.steelGrey,
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
  },
  openEye: {
    marginLeft: wp(2),
  },
  closedEye: {
    marginTop: wp(2),
    marginLeft: wp(2),
  },
  forgotPassword: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.primary_color,
    paddingTop: -wp(2.5),
    paddingBottom: wp(2.5),
  },
  loginBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginTop: wp(6.2),
  },
  loginText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: wp(2.6),
  },
  signUp: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    textAlign: "center",
    marginTop: wp(10),
  },
  blueText: {
    color: colors.primary_color,
    fontWeight: "500",
  },
});
