import React from "react";
/**
 * Libraries
 */
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Platform,
  TextInput,
  TouchableWithoutFeedback,
  StatusBar,
} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import Toast from "react-native-easy-toast";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import { connect } from "react-redux";

/**
 * Custom components, methods and constants
 */
import { isValidEmail } from "../../utils/Utils";
import OnboardingModal from "../../components/login/OnboardingModal";
import { colors, globalStyles } from "../../styles/CommonStyles";
import { doEmailSignUp } from "../../controllers/UserController";
import LoadingView from "../../components/common/LoadingView";

/**
 * @constant config Static config to generate login form fields
 */
const formData = [
  { label: "Email ID", key: "email" },
  { label: "Password", key: "password" },
  { label: "Re-enter Password", key: "passwordConfirm" },
];

/**
 * Signup screen - For signup with Email-Id
 */
class SignUpScreen extends React.PureComponent {
  /**
   * Holds list of text input references
   */
  textInputRefs = {};

  /**
   *  Setting the navigation listener for changing the statusbar properties
   */
  componentDidMount() {
    this.onWillFocusSubscription = this.props.navigation.addListener("willFocus", (payload) => {
      StatusBar.setBarStyle("light-content");
      StatusBar.setBackgroundColor(colors.primary_color);
    });
  }

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    this.onWillFocusSubscription.remove();
  }

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = {
      email: "",
      password: "",
      passwordConfirm: "",
      textFocused: "",
      showPassword: {},
      refreshing: false,
    };
  }

  /**
   * Show start without features bottom sheet modal
   */
  openModal = () => this.refs.modal.RBSheet.open();

  /**
   * Goto Email login screen
   */
  gotoLogin = () => {
    this.props.navigation.navigate("Login");
  };

  /**
   * Validate login form fields
   * @returns {boolean} if valid return true else false
   */
  validForm = () => {
    const { email, password, passwordConfirm } = this.state;

    if (email.length == 0) this.refs.toast.show("Please Enter Email");
    else if (!isValidEmail(email)) this.refs.toast.show("Please Enter valid Email");
    else if (password.length == 0) this.refs.toast.show("Please Enter Password");
    else if (password.length < 6 || passwordConfirm.length < 6)
      this.refs.toast.show("Password must be at least 6 alphanumeric");
    else if (passwordConfirm.length == 0) this.refs.toast.show("Please Re-enter Password");
    else if (password != passwordConfirm) this.refs.toast.show("Password not matching");
    else return true;
    return false;
  };

  /**
   * Signup with email Id
   * Call signup API
   */
  doSignUp = () => {
    const { baseURL, appToken } = this.props;
    const { password, email, refreshing } = this.state;
    if (this.validForm() && !refreshing) {
      this.setState({ refreshing: true });

      doEmailSignUp(baseURL, appToken, email.trim(), password.trim(), password.trim(), (isSuccess, message) => {
        this.setState({ refreshing: false });
        message ? this.refs.toast.show(message) : this.refs.toast.show("Please try again");
      });
    }
  };

  /**
   * Hide or Show password
   */
  togglePassword = (key) => {
    const { showPassword } = this.state;
    let obj = Object.assign({}, showPassword);

    if (showPassword[key]) obj[key] = false;
    else obj[key] = true;

    this.setState({ showPassword: obj });
  };

  /**
   * On input field focus set that field to active field
   */
  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value });
  };

  /**
   * save text field input value in this.state
   */
  setInputText = (key, value) => {
    this.setState({ [key]: value });
  };

  /**
   * Render the text input field from static config
   *
   * @param {object} inputFieldConfig input field configuration data
   * @returns {view} return input field view
   */
  renderTextInputBox = (item) => {
    const { showPassword } = this.state;

    const inputBoxStyle =
      this.state.textFocused == item.label ? [styles.inputContainer, styles.activeTextBox] : styles.inputContainer;

    const showPasswordIcon = item.key != "email";
    const passwordIcon = showPassword[item.key]
      ? require("../../assets/icons/login/open-eye.png")
      : require("../../assets/icons/login/closed-eye.png");

    const passwordIconStyle = showPassword[item.key] ? styles.openEye : styles.closedEye;

    return (
      <View>
        <Text style={styles.inputLabel}>{item.label}</Text>
        <View style={inputBoxStyle}>
          <TextInput
            ref={(ref) => (this.textInputRefs[item.key] = ref)}
            // placeholder={placeholder}
            // maxLength={item.length}
            numberOfLines={1}
            style={styles.textInput}
            placeholderTextColor={colors.steelGrey}
            maxLength={100}
            secureTextEntry={showPasswordIcon && !showPassword[item.key]}
            onChangeText={(value) => this.setInputText(item.key, value)}
            onFocus={() => this.toggleTextInputFocus(item.key)}
            onBlur={() => this.toggleTextInputFocus(null)}
          />

          {showPasswordIcon && (
            <TouchableOpacity onPress={() => this.togglePassword(item.key)}>
              <Image source={passwordIcon} style={passwordIconStyle} />
            </TouchableOpacity>
          )}
        </View>
      </View>
    );
  };

  /**
   * Render login form with input fields and buttons from static config
   * @returns {view} Login form
   */
  renderForm = () => {
    return (
      <View>
        {formData.map((item) => this.renderTextInputBox(item))}

        <TouchableOpacity style={styles.loginBtn} onPress={this.doSignUp}>
          <Text style={styles.loginText}>Create Account</Text>
        </TouchableOpacity>

        <TouchableWithoutFeedback onPress={this.gotoLogin}>
          <Text style={styles.signUp}>
            Already have an Account?
            <Text style={styles.blueText}> Login</Text>
          </Text>
        </TouchableWithoutFeedback>
      </View>
    );
  };

  /**
   * Render Toolbar view
   * @returns {view} toolbar
   */
  renderToolbar = () => {
    const { navigation } = this.props;
    const backIcon = require("../../assets/icons/login/back-arrow-blue.png");

    return (
      <View style={styles.toolbar}>
        <TouchableOpacity onPress={() => navigation.pop()}>
          <Image style={styles.backBtn} source={backIcon} />
        </TouchableOpacity>

        <TouchableOpacity style={styles.quickStartContainer} onPress={this.openModal}>
          <Text onPress={this.openModal} style={styles.quickStart}>
            Start without the best features
          </Text>
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * Render greeting message for new users
   * @returns {view} greeting text view
   */
  renderGreeting = () => {
    return (
      <Text style={styles.helloMessage}>
        {"Greetings from Trendlyne! \n"}
        <Text style={styles.greyText}>Log in to continue</Text>
      </Text>
    );
  };

  /**
   * Render method
   */
  render() {
    const { refreshing } = this.state;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <KeyboardAwareScrollView showsVerticalScrollIndicator={false}>
          {this.renderGreeting()}
          {this.renderForm()}
        </KeyboardAwareScrollView>

        <OnboardingModal ref="modal" {...this.props} />

        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken };
};

export default connect(mapStateToProps)(SignUpScreen);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(6.2),
    paddingTop: wp(6.8),
  },
  toolbar: {
    flexDirection: "row",
    // alignItems: "center",
    justifyContent: "center",
    height: heightPercentageToDP(6),
  },
  backBtn: {
    marginVertical: heightPercentageToDP(1),
    marginRight: wp(4),
  },
  quickStartContainer: {
    flex: 1,
    justifyContent: "center",
  },
  quickStart: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.primary_color,
    textAlign: "right",
  },
  helloMessage: {
    fontSize: RF(3.3),
    fontWeight: "300",
    color: colors.primary_color,
    lineHeight: wp(9),
    // marginTop: wp(8),
    marginTop: wp(6.6),
    marginBottom: wp(10),
  },
  greyText: {
    color: colors.steelGrey,
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
  },
  openEye: {
    marginLeft: wp(2),
  },
  closedEye: {
    marginTop: wp(2),
    marginLeft: wp(2),
  },
  forgotPassword: {
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.primary_color,
    marginTop: -wp(2.5),
  },
  loginBtn: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    marginTop: wp(6.2),
  },
  loginText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: wp(2.6),
  },
  signUp: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    textAlign: "center",
    marginTop: wp(10),
  },
  blueText: {
    color: colors.primary_color,
    fontWeight: "500",
  },
});
