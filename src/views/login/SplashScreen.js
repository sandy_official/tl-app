/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, Image, StatusBar, Animated } from "react-native";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";

/**
 * Custom components, methods and constants
 */
import { colors } from "../../styles/CommonStyles";
import { APP_VERSION } from "../../utils/Constants";

/**
 * Splash screen - screen with logo and text
 */
class SplashScreen extends React.PureComponent {
  componentWillMount() {
    this._bounceValue = new Animated.Value(hp(50));
  }

  componentDidMount() {
    const { userState, navigation } = this.props;

    this.startAnimation();
    this.setStatusbarColor();

    setTimeout(() => {
      const { forceUpdateVersion } = this.props;

      if (APP_VERSION > forceUpdateVersion) {
        if (userState.isLoggedIn || userState.isGuestUser) navigation.navigate("App");
        else navigation.navigate("Auth");
      }
    }, 2500); //3000
  }

  /**
   *  Setting the navigation listener for changing the statusbar properties
   */
  setStatusbarColor = () => {
    this.onWillFocusSubscription = this.props.navigation.addListener("willFocus", (payload) => {
      StatusBar.setBarStyle("dark-content");
      StatusBar.setBackgroundColor(colors.whiteTwo);
    });
  };

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    this.onWillFocusSubscription.remove();
  }

  startAnimation = () => {
    setTimeout(() => {
      Animated.spring(this._bounceValue, {
        toValue: 0,
        velocity: 1,
        tension: 2,
        friction: 8,
      }).start();
    }, 1000); //1300
  };

  /**
   * Render method
   */
  render() {
    const logoIcon = require("../../assets/icons/logo.png");

    return (
      <View style={styles.container}>
        <Image source={logoIcon} style={styles.logo} />

        <Animated.View style={{ transform: [{ translateY: this._bounceValue }] }}>
          <Text style={styles.content}>Welcome! </Text>
          <Text style={styles.content}>Trendlyne brings you </Text>
          <Text style={styles.content}>powerful investing insights</Text>
        </Animated.View>
      </View>
    );
  }
}

mapStateToProps = (state) => {
  let firebase = state.app.firebase ? state.app.firebase : {};
  let version = "version" in firebase ? firebase.version : {};
  let forceUpdateVersion = "forceUpdateVersion" in version ? version.forceUpdateVersion : 1;

  return { userState: state.user, forceUpdateVersion };
};

export default connect(mapStateToProps)(SplashScreen);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: colors.whiteTwo,
  },
  logo: {
    width: wp(33),
    height: hp(20),
    resizeMode: "contain",
    marginBottom: wp(11),
  },
  content: {
    fontSize: RF(3.3),
    fontWeight: "300",
    textAlign: "center",
    color: colors.primary_color,
    marginBottom: 5,
  },
});
