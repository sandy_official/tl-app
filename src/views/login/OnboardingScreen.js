/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image, StatusBar } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { statusCodes } from "react-native-google-signin";
import Toast from "react-native-easy-toast";
import { connect } from "react-redux";

/**
 * Custom components, methods and constants
 */
import { doGoogleSignIn, doFacebookLogin } from "../../controllers/UserController";
import { doUserLogin } from "../../actions/UserActions";
import LoadingView from "../../components/common/LoadingView";
import { colors, globalStyles } from "../../styles/CommonStyles";
import OnboardingModal from "../../components/login/OnboardingModal";
import { onboardData, navigationState } from "../../utils/Constants";

/**
 * Onboarding screen
 * shows list of features and Login methods
 */
class OnboardingScreen extends React.PureComponent {
  state = { currentPosition: 0, refreshing: false };

  componentDidMount() {
    /**
     *  Setting the navigation listener for changing the statusbar properties
     */
    this.onWillFocusSubscription = this.props.navigation.addListener("willFocus", (payload) => {
      StatusBar.setBarStyle("dark-content");
      StatusBar.setBackgroundColor(colors.whiteTwo);
    });
  }

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    this.onWillFocusSubscription.remove();
  }

  /**
   * Goto Email login screen
   */
  openLogin = () => {
    this.props.navigation.navigate("Login");
  };

  /**
   * Facebook sign in function
   */
  onFacebookLogin = async () => {
    const { navigation, doUserLogin, baseURL, appToken } = this.props;
    this.setState({ refreshing: true });

    doFacebookLogin(
      baseURL,
      appToken,
      (token) => {
        this.setState({ refreshing: false });

        if (token) {
          doUserLogin(token);
          if (navigationState) navigation.navigate(navigationState);
          else navigation.navigate("App");
        } else {
          this.refs.toast.show("Login failed");
        }
      },
      (error) => {
        this.setState({ refreshing: false });
        console.log("Please try again later.", error);
      }
    );
  };

  /**
   * Google sign in function
   */
  onGoogleLogin = async () => {
    const { navigation, doUserLogin, baseURL, googleKey, appToken } = this.props;

    this.setState({ refreshing: true });

    doGoogleSignIn(
      baseURL,
      appToken,
      googleKey,
      (token) => {
        // onSuccess
        this.setState({ refreshing: false });

        if (token) {
          doUserLogin(token);
          if (navigationState) navigation.navigate(navigationState);
          else navigation.navigate("App");
        } else {
          this.refs.toast.show("Login failed");
        }
      },
      (error) => {
        //onFailure
        this.setState({ refreshing: false });

        if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
          // play services not available or outdated
          this.refs.toast.show("Google play service not available");
        } else {
          // some other error happened
          console.log("Google sign in something went wrong", error);
        }
      }
    );
  };

  /**
   * Render feature list indicator dots from config
   * @returns {view} indicator dots list view
   */
  renderPageIndicator = () => {
    return (
      <View style={styles.pageIndicator}>
        {onboardData.map((item, i) => {
          const indicatorStyle =
            this.state.currentPosition == i ? styles.pageIndicatorActive : styles.pageIndicatorInactive;

          return <View style={indicatorStyle} key={i.toString()} />;
        })}
      </View>
    );
  };

  /**
   * Checks for feature list scroll to change page indicator dots
   */
  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      this.setState({ currentPosition: viewableItems[0].index });
    }
  };

  _viewabilityConfig = { itemVisiblePercentThreshold: 50 };

  /**
   * Show "start without features" bottom sheet for quick start
   */
  openModal = () => this.refs.modal.RBSheet.open();

  /**
   * Render app feature list item
   * @param {object} config feature list item config
   * @param {number} index feature list item position
   * @returns {view} feature list item
   */
  renderFeatureItem = ({ item, index }) => {
    return (
      <View style={styles.featureItem}>
        <Image source={item.img} style={styles.featureImg} />
      </View>
    );
  };

  /**
   * Render the app features list
   * @returns {view} feature list with images
   */
  renderFeatureList = () => {
    return (
      <View>
        <FlatList
          ref="featureList"
          data={onboardData}
          extraData={this.state}
          horizontal
          showsHorizontalScrollIndicator={false}
          // contentContainerStyle={styles.barChartList}
          renderItem={this.renderFeatureItem}
          keyExtractor={(item, index) => index.toString()}
          onViewableItemsChanged={this._onViewableItemsChanged}
          viewabilityConfig={this._viewabilityConfig}
        />

        <View style={styles.withoutFeatureRow}>
          <TouchableOpacity style={styles.withoutFeatureContainer} onPress={this.openModal}>
            <Text style={styles.withoutFeature}>Start without the best features</Text>
          </TouchableOpacity>
          {this.renderPageIndicator()}
        </View>
      </View>
    );
  };

  /**
   * render login view with login buttons and signup link
   * @returns {view} view with login buttons
   */
  renderLoginCard = () => {
    const { currentPosition } = this.state;
    const title = onboardData[currentPosition].title;
    const description = onboardData[currentPosition].description;

    return (
      <View style={styles.card}>
        <Text style={styles.title}>{title}</Text>
        <Text style={styles.description}>{description}</Text>
        <Text style={styles.logInWith}>Log in with:</Text>
        {this.renderLoginButtons()}

        <Text style={styles.description}>
          Already have an Account?{" "}
          <Text style={styles.loginText} onPress={this.openLogin}>
            Login
          </Text>
        </Text>
      </View>
    );
  };

  /**
   * Render social login buttons (Google and Facebook)
   * @returns {view} login button
   */
  renderLoginButtons = () => {
    const { refreshing } = this.state;
    const facebookLogo = require("../../assets/icons/login/facebook-logo.png");
    const googleLogo = require("../../assets/icons/login/google-logo.png");

    return (
      <View style={styles.buttonRow}>
        <TouchableOpacity style={styles.facebookBtn} onPress={this.onFacebookLogin} disabled={refreshing}>
          <Image style={styles.facebookIcon} source={facebookLogo} />
          <Text style={styles.buttonText}>Facebook</Text>
        </TouchableOpacity>

        <TouchableOpacity style={styles.googleBtn} onPress={this.onGoogleLogin} disabled={refreshing}>
          <Image style={styles.googleIcon} source={googleLogo} />
          <Text style={styles.buttonText}>Google</Text>
        </TouchableOpacity>
      </View>
    );
  };

  /**
   * Render method
   */
  render() {
    const { refreshing } = this.state;

    return (
      <View style={styles.container}>
        {this.renderFeatureList()}
        {this.renderLoginCard()}

        {refreshing && <LoadingView />}
        <OnboardingModal ref="modal" {...this.props} />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let auth = "auth" in firebase ? firebase.auth : {};
  let googleKey = "GoogleSignIn" in auth ? auth.GoogleSignIn : null;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, googleKey };
};

export default connect(mapStateToProps, { doUserLogin })(OnboardingScreen);

/**
 * Stylesheet
 */
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  pageIndicator: {
    flexDirection: "row",
    alignItems: "center",
    // marginTop: hp(3)
  },
  pageIndicatorActive: {
    width: wp(1.9),
    height: wp(1.9),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(0.95),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.2),
    height: wp(1.2),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.6),
    margin: 3,
  },
  featureItem: {
    // flex: 1,
    height: hp(50),
    width: wp(100),
    alignItems: "center",
    justifyContent: "center",
    padding: wp(6),
  },
  featureImg: {
    resizeMode: "center",
    height: hp(50),
    width: wp(60),
  },
  withoutFeatureRow: {
    flexDirection: "row",
    paddingRight: wp(5),
    paddingLeft: wp(3),
    alignItems: "center",
  },
  withoutFeatureContainer: {
    flex: 1,
    paddingBottom: wp(3),
    paddingTop: wp(1.8),
  },
  withoutFeature: {
    fontWeight: "500",
    fontSize: RF(2.4),
    color: colors.primary_color,
  },
  card: {
    flex: 1,
    backgroundColor: colors.white,
    paddingHorizontal: wp(5.2),
    paddingTop: wp(6),
  },
  title: {
    fontSize: RF(3),
    fontWeight: "500",
    color: colors.black,
  },
  description: {
    fontSize: RF(2.2),
    color: colors.greyishBrown,
    lineHeight: wp(6),
    marginTop: wp(2.4),
  },
  logInWith: {
    fontSize: RF(2.2),
    marginTop: wp(5),
    color: colors.steelGrey,
  },
  buttonRow: {
    flexDirection: "row",
    marginVertical: wp(3.2),
  },
  facebookBtn: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    paddingVertical: wp(3),
    marginRight: wp(2),
  },
  googleBtn: {
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    paddingVertical: wp(3),
    resizeMode: "contain",
    backgroundColor: colors.tomato,
    marginLeft: wp(2),
  },
  googleIcon: {
    width: wp(5),
    height: wp(5),
  },
  facebookIcon: {
    width: wp(5.7),
    height: wp(6),
    marginTop: -wp(1.4),
    resizeMode: "contain",
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    marginLeft: wp(2.2),
  },
  loginText: {
    color: colors.primary_color,
    fontWeight: "500",
  },
});
