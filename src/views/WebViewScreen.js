import React from "react";
import { View, WebView } from "react-native";
import LoadingView from "../components/common/LoadingView";

class WebViewScreen extends React.PureComponent {
  constructor(props) {
    super(props);

    const URL = this.getFormattedURL();
    this.state = { webURL: URL };
  }

  getFormattedURL = () => {
    const url = this.props.navigation.getParam("url", "");

    if (url && typeof url == "string") {
      if (url.indexOf("https://trendlyne.com") != -1) {
        return url + "/?source=tlmapp";
      }

      if (url.indexOf(".pdf") != -1) {
        return "http://docs.google.com/gview?embedded=true&url=" + url;
      }
    }
    return url;
  };

  render() {
    const { webURL } = this.state;
    // console.warn(webURL);

    // const runFirst = `
    //   window.isNativeApp = true;
    //   true; // note: this is required, or you'll sometimes get silent failures
    // `;

    return (
      <View style={{ flex: 1 }}>
        <WebView
          source={{ uri: webURL }}
          style={{ flex: 1 }}
          renderLoading={() => <LoadingView />}
          startInLoadingState={true}
          javaScriptEnabled={true}
          //   domStorageEnabled={true}
          scalesPageToFit={true}
          scrollEnabled={false}
          automaticallyAdjustContentInsets={true}
          //   onLoad={() => this.hideLoader()}
          originWhitelist={["*"]}
          onError={(e) => console.log("onError", e)}
          renderError={(e) => console.log("renderError", e)}
        />
      </View>
    );
  }
}

export default WebViewScreen;
