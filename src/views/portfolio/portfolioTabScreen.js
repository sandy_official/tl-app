import React from "react";
import { View, StyleSheet, Text,Image,TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp ,widthPercentageToDP as wp} from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { globalStyles,colors } from "../../styles/CommonStyles";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import Toast from "react-native-easy-toast";

import SummaryScreen from "./summaryScreen";
import HoldingsScreen from "./holdingsScreen";
import WatchlistScreen from "./watchlistScreen";
import TransactionsScreen from "./transactionsScreen";

import BaseComponent from "../../components/common/BaseComponent";
import TabComponent from "../../components/common/TabComponent";
import LoginModal from "../../components/common/LoginModal";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";
import AddHoldings from "../../components/portfolio/AddHoldings";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";

const addWhiteBtn = require("../../assets/icons/add-white.png");

class PortfolioTabScreen extends React.PureComponent {
  tabs = ["Summary", "Holdings", "Watchlist", "Realized P&L"];
  // componentDidMount() {
  //   const { navigation, userState } = this.props;
  //   const tabIndex = navigation.getParam("tabIndex", 0);
  //   this.setState({ selectedTab: tabIndex }, () => this.setTabSelection({ position: tabIndex }));
  // }

  constructor(props) {
    super(props);
    const tabIndex = props.navigation.getParam("tabIndex", 0);

    this.state = {
      selectedTab: tabIndex,
      isRefreshing: false,
      isApiCalled: [false, false, false, false],
      baseRef: undefined,
      actionText:'Add:'
    };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [false, false, false, false] }, () => this.setTabSelection({ position: selectedTab }));
  };

  gotoHoldingsTab = (tab) => {
    validRef = this.refs.HoldingsScreen ? true : false;
    this.tabRef && this.tabRef.setPage(1);
    console.log(this.tabRef);

    if (tab == "STOCKS") validRef && this.refs.HoldingsScreen.changeHolding("filter", 0);
    else if (tab == "MUTUAL FUNDS") validRef && this.refs.HoldingsScreen.changeHolding("filter", 1);
    else validRef && this.refs.HoldingsScreen.getHoldingsList("normal");
  };

  setTabSelection = (obj) => {
    const { baseRef, isApiCalled } = this.state;
    let validRef = false;
    const tabPosition = obj.position;
    this.setState({ selectedTab: tabPosition });

    if (!isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          validRef = this.refs.SummaryScreen ? true : false;
          // validRef ? this.refs.SummaryScreen : null;
          break;
        case 1:
          validRef = this.refs.HoldingsScreen ? true : false;
          validRef ? this.refs.HoldingsScreen.getData() : null;
          break;
        case 2:
          validRef = this.refs.WatchlistScreen ? true : false;
          validRef ? this.refs.WatchlistScreen.getData() : null;
          break;
        case 3:
          validRef = this.refs.TransactionsScreen ? true : false;
          // baseRef ? baseRef.showLoading() : null;
          validRef ? this.refs.TransactionsScreen.getData() : null;
          break;
      }

      let apiCalls = isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls }, () => {
        // console.log('All-->',isApiCalled,apiCalls,isApiCalled)
      });
    }
  };

  handleBtnClk = () => {
    this.refs.ActionPopUp.RBSheet.open()
  }

  openAddHolding = () => this.addHolding && this.addHolding.handlePopup();

  addHoldingsResponse = (res) => {
    console.log("Add Holdings response -->", res);
    if (res === "created") {
      // this.openAddHolding();
    }
  };

  handleOptions = (name) => {
    this.refs.ActionPopUp.RBSheet.close()
    if (name === 'newHoldings') {
      this.openAddHolding();
    } else {
      this.refs.newPortfolio.modal.open()
    }
  }

  renderActionChilds = () => {
    return (
      <View>
        <TouchableOpacity style={[styles.textView]} onPress={()=>this.handleOptions()}>
          <Text style={[styles.actionTitleText,{color:colors.primary_color },]}>
            {" New Portfolio"}
          </Text>
        </TouchableOpacity>
        <TouchableOpacity style={[styles.textView]} onPress={()=>this.handleOptions('newHoldings')}>
          <Text style={[styles.actionTitleText, { color: colors.primary_color },]}>
            {" New Holding"}
          </Text>
        </TouchableOpacity>
      </View>
    )
  }


  renderToolbar = () => {
    return <Toolbar title="Portfolio" drawerMenu {...this.props} >
      <ImageButton source={addWhiteBtn} onPress={() => this.handleBtnClk()} />
    </Toolbar>;
  };

  getAnalyticsParams = () => {
    return {};
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };


  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  renderTabs = () => {
    // const { navigation } = this.props;
    const { selectedTab, baseRef } = this.state;
    // const tabIndex = navigation.getParam("tabIndex", 0);

    return (
      <TabComponent
        ref={(ref) => (this.tabRef = ref)}
        tabs={this.tabs}
        initialTab={selectedTab}
        analyticsParams={this.getAnalyticsParams}
        onTabSelect={this.setTabSelection}
      >
        <View>
          <SummaryScreen
            ref="SummaryScreen"
            {...this.props}
            baseRef={baseRef}
            isSelected={selectedTab == 0}
            gotoHoldings={this.gotoHoldingsTab}
          />
        </View>
        <View>
          <HoldingsScreen ref="HoldingsScreen" {...this.props} baseRef={baseRef} isSelected={selectedTab == 1} />
        </View>
        <View>
          <WatchlistScreen ref="WatchlistScreen" {...this.props} baseRef={baseRef} isSelected={selectedTab == 2} />
        </View>
        <View>
          <TransactionsScreen
            ref="TransactionsScreen"
            {...this.props}
            baseRef={baseRef}
            isSelected={selectedTab == 3}
          />
        </View>
      </TabComponent>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <BaseComponent
          ref={(ref) => this.setState({ baseRef: ref })}
          refreshScreen={this.refreshScreen}
          {...this.props}
        >
          {this.renderTabs()}
         
        </BaseComponent>
        {/* <LoginModal ref="loginModal" {...this.props} /> */}
        <StockAddBottomSheet
          ref="ActionPopUp"
          modalMainView={{ height: hp(30) }}
          // stockName={"Watchlist"}
          actionText={this.state.actionText}
          // closeIconPress={() => this.handleCloseButton()}
          children={this.renderActionChilds()}
        />
        <AddHoldings
          ref={(refs) => (this.addHolding = refs)}
          rootURL={this.props.baseURL}
          token={this.props.userState.token}
          appToken={this.props.appToken}
          portfolio={this.props.portfolio}
          addHoldingsResponse={this.addHoldingsResponse}
          {...this.props}
        />
        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={"selectedStock"}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

      </View>
    );
  }
}

// mapStateToProps = (state) => ({
//   userState: state.user,
//   // superstars: state.superstar.superstars
  
// });

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;
  console.log("States on transactions", state);
  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps, {
  updatePortfolio,
  loadPortfolio,
},
  null,
  { forwardRef: true }
)(PortfolioTabScreen);
  // (mapStateToProps)(PortfolioTabScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
    paddingHorizontal: "3%",
  },
  textView: {
    paddingVertical: wp(2.5),
 },
  actionTitleText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
  }
});
