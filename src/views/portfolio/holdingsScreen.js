import React from "react";
import {
  View,
  TextInput,
  StyleSheet,
  Platform,
  Text,
  TouchableOpacity,
  Image,
  FlatList,
  RefreshControl,
} from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { globalStyles } from "../../styles/CommonStyles";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import { ListItem, CHIPS, alphabeticalSort } from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";
import { createNewWatchlist } from "../../controllers/WatchlistController";
import { fetcher } from "../../controllers/Fetcher";
import { PORTFOLIO_HOLDINGS_URL, PORTFOLIO_HOLDINGS_SELLFIFO_URL } from "../../utils/ApiEndPoints";
import { getOrdinalNumForDate } from "../../utils/Utils";
import { POST_METHOD, MONTHS_FULL } from "../../utils/Constants";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import HoldingsCard, { EmptyHoldings, InputFieldWithTag } from "../../components/portfolio/holdings";

import { SortingTagWithIcon, SearchBoxwithCloseButton } from "../../components/portfolio/transactions";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";

import DateTimePicker from "react-native-modal-datetime-picker";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import EmptyView from "../../components/common/EmptyView";
import NoMatchView from "../../components/common/NoMatchView";
import AddHoldings from "../../components/portfolio/AddHoldings";
import ScrollToTopView from "../../components/common/ScrollToTopView";

const whiteAddImg = "../../assets/icons/add-white.png";
const iconActive = require("../../assets/icons/tick-white-blue.png");
const iconInactive = require("../../assets/icons/add-blue.png");

class HoldingsScreen extends React.PureComponent {
  tabs = ["Stock", "Screener", "Superstar", "News", "Notifications"];

  state = {
    isLoading: false,
    isRefreshing: false,
    listItems: [],
    watchlistName: "",
    watchlistDescription: "",
    textFocused: null,
    isCreateWatchlistBtnActive: false,
    dropdownPosition: {},
    isDatePickerVisible: false,
    selectedFilter: ["all", "All Accounts"],
    stocks: [],
    actionText: "Showing list of:",
    holdingsCatagoryList: [
      ["Stocks", "All Stocks"],
      ["MF", "All MFs"],
      ["all", "All Accounts"],
    ],
    sellDate: { date: new Date(), formetted: "Select Date" },
    selectedPortFolio: ["", "Select a Portfolio"],
    inputFocused: null,
    minimumSellDate: new Date(),
    maximumSellDate: new Date(),
    availableQty: "",
    sellQty: "",
    sellPrice: "",
    searchActive: false,
    searchText: "",
    keywordNamedList: [],
    stockId: "",
    transactDateForApi: "",
    searchingHoldings: false,
    errorText: "This is error text.",
    sellFromStatus: { status: false, error: "" },
    sellDateStatus: { status: false, error: "" },
    sellQuantityStatus: { status: false, error: "" },
    sellPriceStatus: { status: false, error: "" },
    disableGotoOverview: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    const { navigation, isSelected } = this.props;
    this.setNavigationListener();
  }

  componentWillUnmount() {
    this.navListener.remove();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    const { userState, baseURL, appToken, loadPortfolio } = this.props;
    const { token, isGuestUser } = userState;

    this.getHoldingsList("normal");
    if (!isGuestUser) loadPortfolio(baseURL, appToken, token);
  };

  setNavigationListener = () => {
    const { userState, baseURL, appToken, loadPortfolio } = this.props;
    const { token, isGuestUser } = userState;

    this.navListener = this.props.navigation.addListener("willFocus", () => {
      if (this.props.isSelected) {
        this.checkForCurrentFilter();
        if (!isGuestUser) loadPortfolio(baseURL, appToken, token);
      }
    });
  };

  //for refreshing data
  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => {
      this.checkForCurrentFilter();
    });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  checkForCurrentFilter = () => {
    const { selectedFilter } = this.state;
    if (selectedFilter[0] === "Stocks") {
      this.getHoldingsList("filter", 0);
    } else if (selectedFilter[0] === "MF") {
      this.getHoldingsList("filter", 1);
    } else if (selectedFilter[0] === "all") {
      this.getHoldingsList("filter", 2);
    } else {
      this.getHoldingsList("filter", 3);
    }
  };

  handleRequestData = (type, ind) => {
    const { selectedFilter } = this.state;
    let requestObj = {};
    if (type === "filter") {
      if (ind == 0) {
        requestObj = { portfolioType: "stocks" };
      } else if (ind == 1) {
        requestObj = { portfolioType: "MF" };
      } else if (ind == 2) {
        requestObj = { portfolioType: "" };
      } else if (ind > 2) {
        requestObj = { portfolioIds: [selectedFilter[0]], portfolioType: ["stocks", "MF"] };
      }
    }
    return requestObj;
  };

  getHoldingsList = (type, ind) => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    // const { holdingsCatagoryList, selectedFilter } = this.state;

    const URL = baseURL + PORTFOLIO_HOLDINGS_URL;
    let requestData = this.handleRequestData(type, ind);
    console.log("request data -->", requestData);
    this.setState({
      isLoading: true,
    });

    fetcher(
      URL,
      POST_METHOD,
      requestData,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body, head } = json;
        console.log("Holdings Resposne --->", json);
        let dataObj = this.mapScreenerData(body).stocks;
        const keyname = dataObj[0].obj.NSEcode ? "NSEcode" : dataObj[0].obj.BSEcode ? "BSEcode" : "";
        this.setState({
          stocks: alphabeticalSort(dataObj ? dataObj : [], "A", keyname),
          allStocks: dataObj ? dataObj : [],
          isLoading: false,
          selectedStock: dataObj[0].obj,
          refreshing: false,
        });
      },
      (message) => {
        this.setState({
          isLoading: false,
          stocks: [],
          allStocks: [],
          refreshing: false,
        });
        console.log("Message -->", message);
      }
    );
  };

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;
    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false, isLoadingMore: false });
  };

  mapScreenerData = (json) => {
    let stockList = [];
    let columnList = json.tableHeaders;
    let screenData = json.screen ? json.screen : {};

    json.tableData.forEach((stockIndex) => {
      let stockObj = [];
      let stockArray = [];
      let dvmValueCrossed = false; // to get data which is after dvm in json object
      stockIndex.forEach((item, i) => {
        let column = Object.assign({}, columnList[i]);
        let columnName = column.unique_name ? column.unique_name : column.name;
        if (dvmValueCrossed) {
          column["value"] = item;
          stockArray.push(column);
        }
        if (column.unique_name == "m_color") dvmValueCrossed = true;
        stockObj[columnName] = item;
      });
      stockList.push({ obj: stockObj, array: stockArray });
    });
    return {
      isNextPage: json.isNextPage ? json.isNextPage : "",
      order: json.order ? json.order : "",
      description: json.description ? json.description : "",
      title: json.title ? json.title : "",
      sortBy: json.sortBy ? json.sortBy : "",
      pageNumber: json.thisPageNumber ? json.thisPageNumber : "",
      pageCount: json.thisPageCount ? json.thisPageCount : "",
      stocks: stockList,
      screen: screenData,
    };
  };

  changeHolding = (type, index) => {
    let data = index == 0 ? ["Stocks", "All Stocks"] : ["MF", "All MFs"];
    this.setState({ selectedFilter: data });
    this.getHoldingsList(type, index);
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  handlePopup() {
    this.watchlistActionPopUp.open();
  }

  closeDialog() {
    this.watchlistActionPopUp.close();
  }

  setInputText = (type, value) => {
    if (type == "Quantity") {
      if (value === "") {
        this.setState(
          {
            sellQty: value,
            sellQuantityStatus: {
              error: "Please enter quantity.",
              status: false,
            },
          },
          () => {
            this.setState({
              errorText: this.state.sellQuantityStatus.error,
            });
          }
        );
      } else if (value > this.state.availableQty) {
        this.setState(
          {
            sellQty: value,
            sellQuantityStatus: {
              error: "The quantity is exceeding than available.",
              status: false,
            },
          },
          () => {
            this.setState({
              errorText: "The quantity is exceeding than available.",
            });
          }
        );
      } else {
        this.setState({
          sellQty: value,
          errorText: "",
          sellQuantityStatus: {
            status: true,
            error: "",
          },
        });
      }
    } else {
      if (value === "") {
        this.setState(
          {
            sellPrice: value,
            errorText: "",
            sellPriceStatus: {
              status: false,
              error: "Please enter sell price.",
            },
          },
          () => {
            this.setState({
              errorText: this.state.sellPriceStatus.error,
            });
          }
        );
      } else {
        this.setState({
          sellPrice: value,
          errorText: "",
          sellPriceStatus: {
            status: true,
            error: "",
          },
        });
      }
    }
  };

  toggleTextInputFocus = (value) => {
    this.setState({ inputFocused: value });
  };
  createWatchlist = () => {
    const { token } = this.props.userState;
    const title = this.state.watchlistName;
    const description = this.state.watchlistDescription;

    this.setState({ isLoading: true });

    createNewWatchlist(title, description, token, (watchlistId, message) => {
      this.watchlistActionPopUp.close();
      this.getHoldingsList();
      if (watchlistId != -1) {
        this.refs.toast.show("Watchlist created");
      } else {
        if (message) this.refs.toast.show(message);
        else this.refs.toast.show("Please try again");
      }
      this.setState({ isLoading: false });
    });
  };

  checkForActivePortfolioButton = () => {
    let flag = false;
    let temp = [
      ["Stocks", "All Stocks"],
      ["all", "All Accounts"],
      ["MF", "All MFs"],
    ];
    flag = temp.findIndex((ele) => ele[0] === this.state.selectedFilter[0]);
    let clrObj = {};
    if (flag >= 0) {
      clrObj = {
        textColor: colors.primary_color,
        backgroundColor: "#fff",
        borderColor: colors.primary_color,
        selectedPlusImg: true,
      };
    } else {
      clrObj = {
        textColor: "#fff",
        backgroundColor: colors.primary_color,
        borderColor: colors.primary_color,
        selectedPlusImg: false,
      };
    }
    return clrObj;
  };

  renderCards = ({ item, index }) => {
    return (
      <HoldingsCard
        {...this.props}
        stock={item}
        disableNavigation={this.state.disableGotoOverview}
        highlightNegatives
        position={index}
        firstButtonClick={() => this.handleSellFIFO(item)}
        secondButtonClick={() => this.addStocktoWatchlist(item)}
        portfolioButtonMainView={{
          borderColor: this.checkForActivePortfolioButton().borderColor,
          backgroundColor: this.checkForActivePortfolioButton().backgroundColor,
        }}
        portfolioButtonText={{ color: this.checkForActivePortfolioButton().textColor }}
        portfolioIconSource={this.checkForActivePortfolioButton().selectedPlusImg ? iconInactive : iconActive}
        thirdButtonClick={() => this.props.navigation.navigate("HoldingsDetail", { holdingData: item })}
      />
    );
  };

  addStocktoWatchlist = (stockData) => {
    this.setState({ selectedStock: stockData.obj }, () => {
      this.refs.portfolio.modal.open();
    });
  };

  handleSellFIFO(allDetail) {
    const { get_full_name, lastprice_date, qty, stock_id } = allDetail.obj;
    const minimumSellDate = new Date(lastprice_date);
    this.setState(
      {
        selectedStock: allDetail.obj,
        actionText: `Sell FIFO of ${get_full_name ? get_full_name : ""}`,
        minimumSellDate: minimumSellDate,
        availableQty: qty,
        stockId: stock_id,
        selectedPortFolio: ["", "Select a Portfolio"],
        sellFromStatus: {
          status: false,
          error: "",
        },
        sellDate: {
          date: new Date(),
          formetted: "Select Date",
        },
        sellQty: "",
        sellPrice: "",
        sellDateStatus: {
          status: false,
          error: "",
        },
        sellQuantityStatus: {
          status: false,
          error: "",
        },
        sellPriceStatus: {
          status: false,
          error: "",
        },
        errorText: "",
      },
      () => {
        this.handleBottomSheet("sellFIFO");
      }
    );
  }

  handleCloseButton() {
    this.refs.holdingsActionsPopUp.RBSheet.close();
  }

  handleBottomSheet = (type) => {
    const { portFolioData } = this.props;
    const { holdingsCatagoryList } = this.state;
    console.log("Portfolio list -->", portFolioData);
    if (type === "holdingsCatagory") {
      this.setState(
        {
          actionText: "Showing list of:",
          holdingsCatagoryList:
            holdingsCatagoryList.length === 3
              ? [...holdingsCatagoryList, ...portFolioData.portfolio]
              : holdingsCatagoryList,
        },
        () => {
          this.refs.holdingsActionsPopUp.RBSheet.open();
        }
      );
    } else if (type === "sellFIFO") {
      this.refs.holdingsActionsPopUp.RBSheet.open();
    }
  };

  handleHoldingsListItem = (data, ind) => {
    this.setState({ selectedFilter: data ? data : "" }, () => {
      if (this.state.selectedFilter[0] === "MF") {
        this.setState({
          disableGotoOverview: true,
        });
      } else {
        this.setState({
          disableGotoOverview: false,
        });
      }
      this.getHoldingsList("filter", ind);
      this.refs.holdingsActionsPopUp.RBSheet.close();
    });
  };

  toggleDropdown = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "dropdown" ? "" : "dropdown";
    this.setState({
      showPortfolioList: !this.state.showPortfolioList,
      inputFocused: focusedInput,
    });
  };

  renderPortfolioDropdown = () => {
    const inputBoxStyle =
      this.state.inputFocused == "dropdown" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];

    const dropdownIcon = this.state.showPortfolioList
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");

    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.sellFromStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          Sell from<Text style={styles.redText}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDropdown}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.greyishBrown }]}>
            {" "}
            {this.state.selectedPortFolio[1]}
          </Text>
          <Image source={dropdownIcon} style={styles.inputIcon} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    );
  };

  renderDateField = () => {
    const inputBoxStyle =
      this.state.inputFocused == "date" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];

    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.sellDateStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          Sell Date <Text style={styles.redText}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDatePicker}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.greyishBrown }]}>
            {this.state.sellDate.formetted}
          </Text>
          <Image
            source={require("../../assets/icons/date-picker-blue.png")}
            style={styles.inputIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  selectPortfolio = (item) => {
    this.setState({ selectedPortFolio: item, errorText: "", sellFromStatus: { status: true, error: "" } });
    this.toggleDropdown();
  };

  renderPortfolioItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => this.selectPortfolio(item)}>
        <Text style={styles.listItem}>{item[1] ? item[1] : ""}</Text>
      </TouchableOpacity>
    );
  };

  renderPortfolioList = () => {
    const { portFolioData } = this.props;
    const { y, width } = this.state.dropdownPosition;
    const style = {
      position: "absolute",
      top: Platform.OS == "android" ? hp(9) : y + hp(7),
      width: width,
      maxHeight: hp(20),
      minHeight: wp(4),
      backgroundColor: "yellow",
    };
    if (this.state.showPortfolioList) {
      return (
        <View style={[style, styles.list]}>
          <FlatList data={portFolioData.portfolio} extraData={this.props} renderItem={this.renderPortfolioItem} />
        </View>
      );
    }
    return null;
  };

  updateDropdownPosition = (event) => {
    event.preventDefault();
    this.setState({ dropdownPosition: event.nativeEvent.layout });
  };

  getStyle = (tagName) => {
    let inputBox =
      this.state.inputFocused == tagName
        ? [styles.inputContainer2, styles.activeTextBox, { paddingBottom: wp(0.5), marginBottom: wp(1) }]
        : [styles.inputContainer2];
    return inputBox;
  };

  renderTextInputBox = (label) => {
    const first_tag = "Quantity";
    const second_tag = "Sell Price";
    return (
      <View>
        <InputFieldWithTag
          inputBoxStyle={[
            this.getStyle(first_tag),
            {
              borderBottomColor:
                this.state.inputFocused == first_tag
                  ? this.state.sellQuantityStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.sellQuantityStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            },
          ]}
          label={first_tag}
          onChangeText={(value) => this.setInputText(first_tag, value)}
          onFocus={() => this.toggleTextInputFocus(first_tag)}
          onBlur={() => this.toggleTextInputFocus(null)}
          placeholder={`Available : ${this.state.availableQty.toString()}`}
          value={this.state.sellQty.toString()}
        />
        <InputFieldWithTag
          inputBoxStyle={[
            this.getStyle(second_tag),
            {
              borderBottomColor:
                this.state.inputFocused == second_tag
                  ? this.state.sellPriceStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.sellPriceStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            },
          ]}
          label={second_tag}
          onChangeText={(value) => this.setInputText(second_tag, value)}
          onFocus={() => this.toggleTextInputFocus(second_tag)}
          onBlur={() => this.toggleTextInputFocus(null)}
          value={this.state.sellPrice.toString()}
        />
      </View>
    );
  };

  dividerLine = (currentItem) => {
    const { portFolioData } = this.props;
    let dividerObj = {
      color: colors.white,
      width: 0,
    };
    if (currentItem[1] === "All Accounts" && portFolioData.portfolio.length > 0) {
      dividerObj = {
        color: colors.greyish,
        width: 1,
      };
    }
    return dividerObj;
  };

  renderHoldindsActionChilds = () => {
    const { actionText } = this.state;
    if (actionText === "Showing list of:") {
      return (
        <FlatList
          data={this.state.holdingsCatagoryList}
          renderItem={({ item, index }) => (
            <TouchableOpacity
              style={{
                borderBottomColor: this.dividerLine(item).color,
                borderBottomWidth: this.dividerLine(item).width,
              }}
              onPress={() => this.handleHoldingsListItem(item, index)}
            >
              <Text
                style={[
                  styles.actionTitleText,
                  { color: item[1] === this.state.selectedFilter[1] ? colors.primary_color : colors.black },
                ]}
              >
                {" "}
                {item[1]}
              </Text>
            </TouchableOpacity>
          )}
          keyboardShouldPersistTaps="always"
        />
      );
    } else {
      const { minimumSellDate, maximumSellDate } = this.state;
      return (
        <View onLayout={this.updateDropdownPosition}>
          {this.renderPortfolioDropdown()}
          {this.renderDateField()}
          {this.renderPortfolioList()}
          <DateTimePicker
            // minimumDate={minimumSellDate}
            // maximumDate={maximumSellDate}
            isVisible={this.state.isDatePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.toggleDatePicker}
          />
          {this.renderTextInputBox()}
          {this.renderErrorText()}
          {this.renderActionButtons()}
        </View>
      );
    }
  };

  renderErrorText = () => {
    const { errorText } = this.state;
    if (errorText != "")
      return (
        <View style={{ marginVertical: wp(2) }}>
          <Text style={[styles.redText, { fontSize: RF(1.3) }]}>{`* ${errorText}`}</Text>
        </View>
      );
  };

  handleSellFIFOActions = (actiontype) => {
    if (actiontype === "sellFIFO") {
      const { baseRef, userState, baseURL, appToken } = this.props;
      const { selectedPortFolio, sellDate, sellPrice, sellQty, stockId, transactDateForApi } = this.state;

      const URL = baseURL + PORTFOLIO_HOLDINGS_SELLFIFO_URL;

      let requestData = {
        portfolioId: selectedPortFolio[0] ? selectedPortFolio[0] : "",
        stock_id: stockId ? parseInt(stockId) : "",
        qty: sellQty ? parseInt(sellQty) : "",
        sellPrice: sellPrice ? parseInt(sellPrice) : "",
        closeDate: transactDateForApi ? transactDateForApi : "",
        notes: "source:mobile app",
      };

      this.setState({ isLoading: true }, () => {
        // console.log("Req ->", URL, POST_METHOD, requestData);
        fetcher(
          URL,
          POST_METHOD,
          requestData,
          baseRef,
          appToken,
          userState.token,
          (json) => {
            console.log("SELL Holdings API response -->", json);
            this.setState(
              {
                isLoading: false,
              },
              () => {
                this.getHoldingsList();
                setTimeout(() => {
                  this.refs.toast.show(
                    `Successfully sold ${this.state.sellQty} @ ${this.state.sellPrice} using FIFO approach from this account.`
                  );
                }, 300);
                this.getHoldingsList("normal");
              }
            );
          },
          (message) => {
            this.setState({
              isLoading: false,
            });
            console.log("SELL HOLDING API Message -->", message);
            setTimeout(() => {
              this.refs.toast.show(message);
            }, 300);
          }
        );
      });
    }
    this.refs.holdingsActionsPopUp.RBSheet.close();
  };

  checkForm = (entity) => {
    let flag = false;
    if (entity.status) flag = true;
    else {
      this.setState({ errorText: entity.error });
    }
    return flag;
  };

  checkBtnStatus = (type) => {
    const { sellFromStatus, sellDateStatus, sellQuantityStatus, sellPriceStatus } = this.state;
    if (type === "sellFIFO") {
      if (
        this.checkForm(sellFromStatus) &&
        this.checkForm(sellDateStatus) &&
        this.checkForm(sellQuantityStatus) &&
        this.checkForm(sellPriceStatus)
      ) {
        return {
          color: colors.primary_color,
          status: true,
        };
      } else {
        return {
          color: colors.greyishBrown,
          status: false,
        };
      }
    }
  };

  renderActionButtons = () => {
    return (
      <View style={[styles.actionButtonRow]}>
        <CHIPS
          disableImage
          name="Cancel"
          chipsMainView={styles.button}
          onPress={() => this.handleSellFIFOActions("Cancel")}
        />
        <View style={{ flex: 0.3 }}></View>
        <CHIPS
          disableImage
          name="Sell"
          // chipsMainView={styles.button}
          chipsMainView={[
            styles.button,
            {
              backgroundColor: this.checkBtnStatus("sellFIFO").color,
              borderColor: this.checkBtnStatus("sellFIFO").color,
            },
          ]}
          selected
          onPress={() =>
            this.checkBtnStatus("sellFIFO").status
              ? this.handleSellFIFOActions("sellFIFO")
              : this.handleValidation("sellFIFO")
          }
        />
      </View>
    );
  };

  handleValidation = (type) => {
    const { selectedPortFolio, sellDate, sellPrice, availableQty, sellQty } = this.state;
    let txt;
    if (type === "sellFIFO") {
      if (selectedPortFolio[1] === "Select a Portfolio") {
        txt = "Please select a portfolio.";
        this.setState({ errorText: txt, sellFromStatus: { status: false, error: txt } });
      } else if (sellDate.formetted === "Select Date") {
        txt = "Please select sell date.";
        this.setState({ errorText: txt, sellDateStatus: { status: false, error: txt } });
      } else if (sellQty == "") {
        txt = "Please enter sell quantity.";
        this.setState({ errorText: txt, sellQuantityStatus: { status: false, error: txt } });
      } else if (parseInt(sellQty) > parseInt(availableQty)) {
        txt = "Please enter sell quantity.";
        this.setState({
          errorText: txt,
          sellQuantityStatus: { error: "exceeding than available.", status: false },
        });
      } else if (sellPrice === "") {
        txt = "Please enter sell price.";
        this.setState({ errorText: txt, sellPriceStatus: { status: false, error: txt } });
      }
    }
  };

  toggleDatePicker = () => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "date" ? "" : "date";
    this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible, inputFocused: focusedInput });
  };

  handleDatePicked = (date) => {
    this.toggleDatePicker();
    let day = date.getDate() > 10 ? date.getDate() : `0${date.getDate()}`;
    let month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    let year = date.getFullYear();
    let monthApi = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    let formattedDate = `${day}-${month}-${year}`;
    let dateForApi = year + "-" + monthApi + "-" + day;
    const dateObj = { formetted: formattedDate, dateObj: date };
    this.setState({
      sellDate: dateObj,
      transactDateForApi: dateForApi,
      sellDateStatus: { status: true, error: "" },
      errorText: "",
    });
  };

  handleSearchButton = () => {
    this.setState({ searchActive: !this.state.searchActive }, () => this.search.focus());
  };

  handleOnClear = () => {
    const { searchText } = this.state;
    // if (searchText !== "") {
    this.setState({
      searchText: "",
      keywordNamedList: [],
      searchingHoldings: false,
      searchActive: !this.state.searchActive,
      stocks: this.state.allStocks,
    });
    // } else {
    //   this.setState({ searchActive: !this.state.searchActive, stocks: this.state.allStocks });
    // }
  };

  handleSearchText = (value) => {
    const { allStocks } = this.state;
    let temparr = [];
    this.setState({ searchText: value, stocks: [], searchingHoldings: true }, () => {
      allStocks.forEach((ele, ind) => {
        if (
          ele.obj.NSEcode.toLowerCase().includes(value.toLowerCase()) ||
          ele.obj.BSEcode.toLowerCase().includes(value.toLowerCase())
        )
          temparr.push(ele);
      });
      this.setState({ stocks: temparr });
    });
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portFolioData, updatePortfolio } = this.props;
    let newPortfolioList = portFolioData.portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  renderListHeader = () => {
    if (this.state.stocks.length !== 0) {
      return (
        <View style={[styles.SortingContainer]}>
          <View style={{ flexDirection: "row" }}>
            <View style={[styles.SortingTagContainer]}>
              <Text style={{ color: colors.black }}>Showing</Text>
            </View>
            <SortingTagWithIcon
              TagText={this.state.selectedFilter[1]}
              TagTextStyle={{ color: colors.primary_color, fontSize: RF(2.2) }}
              ImageStyle={{ height: 15, width: 15 }}
              onPress={() => this.handleBottomSheet("holdingsCatagory")}
            />
          </View>
          <TouchableOpacity onPress={() => (this.props.userState.token != null ? this.handleSearchButton() : null)}>
            <Image
              source={require("../../assets/icons/search-blue.png")}
              resizeMode="contain"
              style={{ height: 20, width: 20 }}
            />
          </TouchableOpacity>
        </View>
      );
    } else {
      return null;
    }
  };

  addHoldingsResponse = (res) => {
    console.log("Add Holdings response -->", res);
    if (res === "created") {
      // this.getHoldingsList();
      this.checkForCurrentFilter();
    }
  };

  addedToPortFolio = () => {
    this.checkForCurrentFilter();
    this.refs.toast.show("Added to Portfolio", 1000);
  };

  render() {
    const {
      isLoading,
      searchActive,
      isRefreshing,
      stocks,
      selectedStock,
      searchingHoldings,
      showScrollToTop,
    } = this.state;
    const { isGuestUser } = this.props.userState;
    const { isSelected } = this.props;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {searchActive ? (
            <View style={{ marginVertical: wp(3) }}>
              <SearchBoxwithCloseButton
                onChangeText={(text) => this.handleSearchText(text)}
                onClearPress={() => this.handleOnClear()}
                inputRef={(input) => (this.search = input)}
                value={this.state.searchText}
                multiline={true}
              />
            </View>
          ) : stocks && this.props.userState.token && !stocks.length > 0 && !isGuestUser ? (
            <View style={[styles.SortingContainer]}>
              <View style={{ flexDirection: "row" }}>
                <View style={[styles.SortingTagContainer]}>
                  <Text style={{ color: colors.black }}>Showing</Text>
                </View>
                <SortingTagWithIcon
                  TagText={this.state.selectedFilter[1]}
                  TagTextStyle={{ color: colors.primary_color, fontSize: RF(2.2) }}
                  ImageStyle={{ height: 15, width: 15 }}
                  onPress={() => this.handleBottomSheet("holdingsCatagory")}
                />
              </View>
              <TouchableOpacity
                onPress={() =>
                  this.props.userState.token != null && this.state.stocks.length > 0 ? this.handleSearchButton() : null
                }
              >
                <Image
                  source={require("../../assets/icons/search-blue.png")}
                  resizeMode="contain"
                  style={{ height: 20, width: 20 }}
                />
              </TouchableOpacity>
            </View>
          ) : (
            isGuestUser && (
              <View style={{}}>
                <EmptyHoldings onPressAddHoldings={() => this.getHoldingsList()} />
              </View>
            )
          )}
          {!isLoading && this.state.stocks.length != 0 ? (
            <FlatList
              ref={(ref) => (this.listRef = ref)}
              data={this.state.stocks}
              renderItem={this.renderCards}
              extraData={{ ...this.props, stocks, isLoading } || this.state.stocks}
              showsVerticalScrollIndicator={false}
              ListHeaderComponent={!searchActive ? this.renderListHeader : null}
              refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
              // ListFooterComponent={this.renderFooter}
              keyboardShouldPersistTaps="always"
              onScroll={this.handleScroll}
              // ListFooterComponent={this.renderFooter}
            />
          ) : searchingHoldings && this.state.stocks.length === 0 && !isLoading && !isGuestUser ? (
            <View style={{ height: hp(60) }}>
              <NoMatchView message="No Results found" />
            </View>
          ) : this.state.stocks.length === 0 && !isLoading && !isGuestUser && !searchingHoldings ? (
            <View style={{}}>
              <EmptyHoldings onPressAddHoldings={() => this.addHolding.handlePopup()} />
            </View>
          ) : !isGuestUser ? (
            <View style={{ justifyContent: "center", alignItems: "center", height: hp(82) }}>
              {/* <Text>Loading Data </Text> */}
            </View>
          ) : null}

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />

          <StockAddBottomSheet
            ref="holdingsActionsPopUp"
            modalMainView={{ height: hp(60) }}
            actionText={this.state.actionText}
            closeIconPress={() => this.handleCloseButton()}
            children={this.renderHoldindsActionChilds()}
          />
          <AddHoldings
            ref={(refs) => (this.addHolding = refs)}
            // ref="addHolding"
            rootURL={this.props.baseURL}
            token={this.props.userState.token}
            appToken={this.props.appToken}
            portfolio={this.props.portFolioData.portfolio}
            addHoldingsResponse={this.addHoldingsResponse}
            {...this.props}
          />
          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={this.props.portFolioData.portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.addedToPortFolio()}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          {isLoading || isRefreshing ? <LoadingView /> : null}
        </View>
      );
    } else return null;
  }
}

let mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  console.log("Stste in holdings", state);
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    portFolioData: state.portfolio,
  };
};

export default connect(mapStateToProps, { loadPortfolio, updatePortfolio }, null, { forwardRef: true })(HoldingsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  SortingContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: wp(3),
    paddingVertical: wp(2),
  },
  SortingTagContainer: {
    alignItems: "center",
    justifyContent: "center",
  },
  addingItemContainer: {
    paddingVertical: wp(5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: wp(3),
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  listContainer: {
    backgroundColor: colors.white,
  },
  emptyViewContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  textInput: {
    flex: 1,
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    backgroundColor: "red",
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(5),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(2.5),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
  actionTitleText: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    paddingVertical: wp(2.5),
  },
  inputContainer2: {
    paddingBottom: wp(1),
    marginBottom: wp(3),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  inputLabel: {
    fontSize: RF(1.9),
    color: colors.black,
  },
  redText: { color: colors.red },
  textInput2: {
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3.7) : 0,
  },
  inputRow: {
    flexDirection: "row",
    paddingTop: wp(2),
    paddingBottom: wp(0.8),
  },
  inputIcon: {
    width: wp(5),
    height: wp(5),
  },

  listItem: {
    padding: wp(4),
    color: colors.greyishBrown,
    fontSize: RF(2.2),
  },
  list: {
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    zIndex: 99,
  },
  actionButtonRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: wp(2),
    marginBottom: wp(5),
  },
  button: {
    marginHorizontal: 0,
    flex: 2,
    justifyContent: "center",
    alignItems: "center",
    paddingVertical: wp(2.5),
  },
});
