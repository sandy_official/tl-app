import React from "react";
import {
  View,
  Text,
  Platform,
  Image,
  FlatList,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import ScreenerStockCard from "../../components/screeners/ScreenerStockCard";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors, globalStyles } from "../../styles/CommonStyles";

import Toast from "react-native-easy-toast";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import { SortOptionView } from "../../components/common/SortOptionView";
import LoadingView from "../../components/common/LoadingView";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import { connect } from "react-redux";
import {
  setActiveWatchlist,
  updateWatchlist,
  getActiveWatchlistByStockId,
  loadWatchlist,
} from "../../actions/WatchlistActions";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import NoMatchView from "../../components/common/NoMatchView";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";

import { Toolbar, ImageButton } from "../../components/Toolbar";
import { CHIPS, PopUpbutton, sortDataByKey } from "../../components/portfolio/watchlist";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";

import { fetcher } from "../../controllers/Fetcher";
import {
  WATCHLIST_STOCK_LIST_URL,
  WATCHLIST_DELETE_URL,
  PUSH_TO_WATCHLIST_URL,
  WATCHLIST_EDIT_URL,
} from "../../utils/ApiEndPoints";
import { POST_METHOD } from "../../utils/Constants";

import { searchStockByTerm } from "../../controllers/StocksController";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import EmptyView from "../../components/common/EmptyView";
import ScreenerFilters from "../../components/screeners/ScreenerFilters";
import ScrollToTopView from "../../components/common/ScrollToTopView";

// const moreIcon = require("../../assets/icons/portfolio/moreIcon.png");
// const sortIcon = require("../../assets/icons/filter-white.png");
class WatchlistDetailScreen extends React.PureComponent {
  clearStockList = false;

  state = {
    watchlistId: "",
    watchlistName: "",
    watchlistDesc: "",
    tempWatchlistName: "",
    isLoading: false,
    stockName: "",
    stockId: "",
    sortingFilters: [],
    selectedFilter: "Added on",
    reverseOrder: false,
    enableAddButton: false,
    errorAddWatchlistText: "",
    isNextPage: true,
    pageNumber: 0,
    pageCount: 10,
    stocks: [],
    refreshing: false,
    isFirstCallComplete: false,
    selectedStock: {},
    stockInWatchlist: false,
    isLoadingMore: false,
    actionText: "",
    searchDummyStocks: [],
    currentStock: {},
    showFilters: false,
    filterId: 0,
    filterName: "",
    showScrollToTop: false,
  };

  componentDidMount() {
    if (this.props.navigation.state.params) {
      const { state } = this.props.navigation;

      this.setState(
        {
          watchlistId: state.params.keyId ? state.params.keyId : null,
          watchlistName: state.params.keyName ? state.params.keyName : "",
          tempWatchlistName: state.params.keyName ? state.params.keyName : "",
          watchlistDesc: state.params.keyDesc ? state.params.keyDesc : "",
        },
        () => this.getWatchlistData()
      );
    }
  }

  getWatchlistData = () => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const { isFirstCallComplete, filterId } = this.state;

    const URL = baseURL + WATCHLIST_STOCK_LIST_URL;
    let requestBody = { watchlistId: this.state.watchlistId, filterListId: filterId };

    this.setState({ isLoading: true });

    fetcher(
      URL,
      POST_METHOD,
      requestBody,
      null,
      // baseRef,
      appToken,
      token,
      (json) => {
        const { body, head } = json;
        let dataObj = this.mapScreenerData(json.body).stocks;

        this.setState({ stocks: dataObj, isLoading: false }, () => {
          let sortOptions = this.createSortingParameters(json.body.tableHeaders);
          this.setState({ sortingFilters: sortOptions ? sortOptions : [], isLoadingMore: false });
        });

        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        // this.setState(data);
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        // baseRef && baseRef.stopLoading();
      },
      (message) => {
        this.setState({ isLoading: false, stocks: [], sortingFilters: [], isLoadingMore: false });

        if (message) this.refs.toast.show(message);
        else this.refs.toast.show("Please try again");
        console.log("Message -->", message);
        //handle error here
        // baseRef && baseRef.stopLoading();
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });

        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  createSortingParameters = (MappingData) => {
    const last = "m_color";
    let compInd = MappingData ? MappingData.findIndex((ele) => ele.unique_name === last) : -1;
    let tempArray = MappingData.filter((ele, ind) => ind > compInd);
    return tempArray;
  };

  mapScreenerData = (json) => {
    let stockList = [];
    let addedonObj = [];
    let addedonArry = [];
    let tmpObj = {};
    let columnList = json.tableHeaders;
    let screenData = json.screen ? json.screen : {};
    // stock details
    let length = json.tableData[0].length;
    json.tableData.forEach((stockIndex) => {
      let stockObj = [];

      let stockArray = [];
      let dvmValueCrossed = false; // to get data which is after dvm in json object
      stockIndex.forEach((item, i) => {
        let column = Object.assign({}, columnList[i]);
        let columnName = column.unique_name ? column.unique_name : column.name;
        // if (dvmValueCrossed && columnName != "addedDate") {
        if (dvmValueCrossed) {
          if (columnName == "addedDate") {
            tmpObj = column;
            tmpObj["value"] = item;
          } else {
            column["value"] = item;
            stockArray.push(column);
          }
        }
        if (length - 1 === i) {
          stockObj["addedDate"] = tmpObj.value;
          stockArray.push(tmpObj);
        }
        if (column.unique_name == "m_color") dvmValueCrossed = true;
        if (columnName != "addedDate") {
          stockObj[columnName] = item;
        }
      });
      stockList.push({ obj: stockObj, array: stockArray });
    });
    // console.log("-->", stockList)
    stockList = sortDataByKey(stockList, "highest_first", this.state.selectedFilter);
    return {
      isNextPage: json.isNextPage ? json.isNextPage : "",
      order: json.order ? json.order : "",
      description: json.description ? json.description : "",
      title: json.title ? json.title : "",
      sortBy: json.sortBy ? json.sortBy : "",
      pageNumber: json.thisPageNumber ? json.thisPageNumber : "",
      pageCount: json.thisPageCount ? json.thisPageCount : "",
      stocks: stockList,
      screen: screenData,
    };
  };

  // componentWillReceiveProps(nextProps) {
  //     if (nextProps.filterId !== this.props.filterId) {
  //         this.clearStockList = true;
  //         this.setState({ filterId: nextProps.filterId, selectedStock: {} });
  //         this.setState({ refreshing: true, pageNumber: 0, isFirstCallComplete: false, stocks: [] }, () => this.getData());
  //     }
  // }

  // componentWillUpdate(newProps, newState) {
  //     newProps.isSelected && newState.stocks.length == 0 && this.getData();
  // }

  stopLoading = () => {
    const { isFirstCallComplete } = this.state;
    if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false, isLoadingMore: false });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    this.setState({ isLoadingMore: true }, () => this.getWatchlistData());
  };

  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => {
      this.getData();
    });
  };

  toggleFilter = () => {
    this.setState({ showFilters: !this.state.showFilters });
  };

  setFilter = (id, name) => {
    this.setState({ filterId: id, filterName: name, showFilters: false }, () => this.getWatchlistData());
  };

  renderFooter = () => {
    const { isNextPage, isLoadingMore } = this.state;
    if (isLoadingMore) return <LoadingMoreView />;
    else if (isNextPage) {
      return (
        <TouchableOpacity onPress={this.onLoadMore}>
          <View style={styles.footerButton}>
            <Text style={styles.footerButtonText}>
              Load More
              {/* (126 remaining) */}
            </Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.listEndFooter}>
          <Text style={styles.listEndFooterText}>That's all folks!</Text>
        </View>
      );
    }
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.watchlists.showWatchlistAdded();
  };

  renderSortingOption = (label, key, index) => {
    if (this.state.selectedSort == null && index == 0) {
      this.setState({
        selectedSort: label,
        isSortAscending: true,
        sortOrder: "ASC",
        sortBy: key,
      });
    }

    return (
      <SortOptionView
        name={label}
        sortKey={key}
        selectedSort={this.state.selectedSort}
        ascending={this.state.isSortAscending}
        onSelect={(sortOption, sortKey) => this.setSortOrder(sortOption, sortKey)}
        key={key}
      />
    );
  };

  isStockEmpty = () => {
    const stocks = this.state.stocks;
    if (stocks.length > 0 && "array" in stocks[0]) return false;
    return true;
  };

  renderSortingOptionsRow = () => {
    const stock = !this.isStockEmpty() ? JSON.parse(JSON.stringify(this.state.stocks[0].array)) : [];

    return (
      <FlatList
        data={stock}
        extraData={this.state}
        horizontal
        showsHorizontalScrollIndicator={false}
        contentContainerStyle={globalStyles.sortOptionRow}
        renderItem={({ item, index }) => this.renderSortingOption(item.name, item.unique_name, index)}
        keyExtractor={(item, index) => index.toString()}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
      />
    );
  };

  showWatchlist = (stock) => {
    const { activeWatchlist, getActiveWatchlistByStockId, userState, loadWatchlist } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.watchlists.open());
    } else this.refs.loginModal.open();
  };

  showAlerts = (stock) => {
    const { isGuestUser } = this.props.userState;
    if (!isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.stockAlerts.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, baseURL, appToken, userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock.obj }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderStockItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;
    const stockId = item.obj.stock_id;
    const isWatchlistActive = stockId in activeWatchlist;
    this.setState({
      currentStock: item.obj,
    });
    return (
      // <View style={{backgroundColor:'yellow',height:30}}>
      //     <Text style={{color:'red'}}>{item.obj.stock_id}</Text>
      // </View>
      <ScreenerStockCard
        {...this.props}
        stock={item}
        position={index}
        watchlistBtnState={isWatchlistActive}
        showWatchlist={() => this.showWatchlist(item)}
        showAlerts={() => this.showAlerts(item)}
        showPortfolio={() => this.showPortfolio(item)}
        appliedPageName={`watchlist_${this.state.filterId}`}
        //Added Keys
        amountStatusWithDate={true}
      />
    );
  };

  renderStockList = () => {
    const { stocks, isFirstCallComplete } = this.state;

    if (isFirstCallComplete && stocks.length > 0)
      return (
        <FlatList
          ref={(ref) => (this.listRef = ref)}
          data={stocks}
          extraData={{ ...this.props, stocks }}
          showsVerticalScrollIndicator={false}
          contentContainerStyle={styles.list}
          renderItem={this.renderStockItem}
          keyExtractor={(item, index) => index.toString()}
          ListHeaderComponent={this.renderSortingChips()}
          // ListFooterComponent={this.renderFooter}
          removeClippedSubviews
          initialNumToRender={3}
          maxToRenderPerBatch={5}
          keyboardShouldPersistTaps="always"
          onScroll={this.handleScroll}
        />
      );
  };

  renderToolbar = () => {
    const { watchlistName } = this.state;
    const filterIcon = require("../../assets/icons/filter-white.png");
    const menuIcon = require("../../assets/icons/portfolio/moreIcon.png");

    return (
      <Toolbar title={watchlistName ? watchlistName : ""} backButton {...this.props}>
        <ImageButton source={filterIcon} onPress={() => this.toggleFilter()} />
        <ImageButton source={menuIcon} onPress={() => this.refs.watchlistActionPopUp.RBSheet.open()} />
      </Toolbar>
    );
  };

  handleSortingClick = (ele, ind) => {
    const { selectedFilter } = this.state;
    if (selectedFilter === ele.name) {
      this.setState(
        {
          reverseOrder: !this.state.reverseOrder,
          refreshing: !this.state.refreshing,
        },
        () => {
          this.handleSortingData(ele.name);
        }
      );
    } else {
      this.setState(
        {
          selectedFilter: ele.name ? ele.name : "",
          reverseOrder: false,
          refreshing: !this.state.refreshing,
        },
        () => {
          this.handleSortingData(ele.name);
        }
      );
    }
  };

  handleSortingData = (type) => {
    const { stocks, reverseOrder } = this.state;
    this.setState({
      stocks: sortDataByKey(stocks, reverseOrder ? "lowest_first" : "highest_first", type),
      refreshing: !this.state.refreshing,
    });
  };

  renderSortingChips = () => {
    return (
      <FlatList
        contentContainerStyle={[styles.sortingContainer, {}]}
        data={this.state.sortingFilters}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <CHIPS
            name={item.name ? item.name : ""}
            gradient
            onPress={() => this.handleSortingClick(item, index)}
            selected={item.name === this.state.selectedFilter}
            doubleSelection={item.name === this.state.selectedFilter && this.state.reverseOrder}
          />
        )}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        // ItemSeparatorComponent={() =>
        //     <View style={styles.divider} />}
        removeClippedSubviews
        initialNumToRender={8}
        maxToRenderPerBatch={5}
        extraData={this.state.stocks || this.state.refreshing}
      />
    );
  };

  handleWatchlist = (type) => {
    if (type === "AddStock") {
      console.log("AddStock Action");
      this.refs.watchlistActionPopUp.RBSheet.close();
      this.setState(
        {
          actionText: "Add a Stock or MF to",
        },
        () => {
          this.refs.watchlistActionPopUp.RBSheet.open();
        }
      );
      return;
    } else if (type === "RenameWatchlist") {
      this.refs.watchlistActionPopUp.RBSheet.close();
      this.setState(
        {
          actionText: "Change Title",
        },
        () => {
          this.refs.watchlistActionPopUp.RBSheet.open();
        }
      );
      return;
    } else {
      console.log("Delete WatchlistAction");
      this.setState({
        actionText: "Delete",
      });
      return;
    }
    //  this.refs.watchlistActionPopUp.RBSheet.close()
  };

  handleCloseButton() {
    if (this.state.actionText === "Delete") {
      this.setState({
        actionText: "",
      });
    } else if (this.state.actionText === "Add a Stock or MF to") {
      this.refs.watchlistActionPopUp.RBSheet.close();
      this.setState(
        {
          actionText: "",
          stockName: "",
        },
        () => this.refs.watchlistActionPopUp.RBSheet.open()
      );
    } else if (this.state.actionText === "Change Title") {
      this.refs.watchlistActionPopUp.RBSheet.close();
      this.setState(
        {
          actionText: "",
        },
        () => this.refs.watchlistActionPopUp.RBSheet.open()
      );
    } else {
      this.refs.watchlistActionPopUp.RBSheet.close();
    }
  }
  setInputText = (text, type) => {
    if (type === "renameWatchlist") {
      const { watchlistName } = this.state;
      this.setState({ tempWatchlistName: text }, () => {
        if (text.length < 3 || text.length > 30) {
          this.setState({
            enableAddButton: false,
            errorAddWatchlistText: `*Enter watchlist name upto 30 characters.`,
          });
        } else if (text.toLowerCase() === watchlistName.toLowerCase()) {
          this.setState({
            enableAddButton: false,
            errorAddWatchlistText: "*Enter different name.",
          });
        } else {
          this.setState({
            enableAddButton: true,
            errorAddWatchlistText: "",
          });
        }
      });
    } else {
      this.setState(
        {
          stockName: text,
        },
        () => {
          const { baseURL, appToken, userState } = this.props;
          if (text.length > 1) {
            searchStockByTerm(baseURL, appToken, userState.token, text, "stock", (StockList) =>
              this.setState({ searchDummyStocks: StockList })
            );
          } else this.setState({ searchDummyStocks: [] });
        }
      );
    }
  };

  renameWatchlist = () => {
    const { stockId, watchlistId, watchlistDesc, isLoading, tempWatchlistName } = this.state;
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;

    const URL = baseURL + WATCHLIST_EDIT_URL;

    this.setState({ isLoading: true }, () => {
      let requestBody = {
        watchlistTitle: tempWatchlistName ? tempWatchlistName : "",
        watchlistId: watchlistId === null ? "" : watchlistId,
        watchlistDesc: watchlistDesc ? watchlistDesc : "",
      };

      fetcher(
        URL,
        POST_METHOD,
        requestBody,
        null,
        // baseRef,
        appToken,
        token,
        (json) => {
          const { body, head } = json;
          const { navigate } = this.props.navigation;
          console.log("===>", json);
          this.refs.watchlistActionPopUp.RBSheet.close();
          this.setState(
            {
              isLoading: false,
            },
            () => {
              this.refs.toast.show("Watchlist updated.");
              navigate("Portfolio");
            }
          );
        },
        (message) => {
          this.setState(
            {
              isLoading: false,
            },
            () => {
              console.log("Message -->", message);
              this.refs.toast.show(message);
            }
          );
          //handle error here
          // baseRef && baseRef.stopLoading();
          // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        }
      );
    });
  };

  renderStocksList = (item, index) => {
    return (
      <TouchableOpacity onPress={() => this.onStockPress(item)}>
        <Text style={styles.stockListItem}>{item.get_full_name ? item.get_full_name : ""}</Text>
      </TouchableOpacity>
    );
  };

  onStockPress = (stock) => {
    this.setState({ selectedStock: stock, searchDummyStocks: [] });
    this.setState(
      {
        stockName: stock.get_full_name ? stock.get_full_name : "",
        stockId: stock.stock_id ? stock.stock_id : "",
      },
      () => {
        this.addStockToCurrentWatchlist();
      }
    );
  };

  addStockToCurrentWatchlist = () => {
    const { stockId, watchlistId, isLoading, stocks } = this.state;
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;

    const URL = baseURL + PUSH_TO_WATCHLIST_URL;

    this.setState({ isLoading: true });

    let requestBody = {
      stock_id: stockId ? stockId : "",
      watchlistId: watchlistId === null ? "" : watchlistId,
    };

    fetcher(
      URL,
      POST_METHOD,
      requestBody,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body, head } = json;

        this.refs.watchlistActionPopUp.RBSheet.close();
        this.refs.watchlists.open();
        // this.handleCloseButton();

        this.setState({ isLoading: false, stockName: "", stockId: "", actionText: "" }, () => {
          this.getWatchlistData();
          console.log("Watchlist updated.-->");
        });
      },
      (message, status) => {
        if (Array.isArray(stocks) && stocks.length == 0) this.setState({ stocks: [] });
        this.setState({ isLoading: false, stockName: "", stockId: "" });
        console.log("Message -->", message);
        if (status == 4) this.refs.toast.show(message, 1000);
        this.handleCloseButton();
        //handle error here
        // baseRef && baseRef.stopLoading();
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  handleDelete = (command) => {
    this.setState({
      isLoading: true,
    });
    let requestBody = {
      watchlistId: this.state.watchlistId,
    };
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const URL = baseURL + WATCHLIST_DELETE_URL;

    if (command === "yes") {
      fetcher(
        URL,
        POST_METHOD,
        requestBody,
        baseRef,
        appToken,
        token,
        (json) => {
          const { body, head } = json;
          // console.log("********", json);
          // this.setState(
          //   {
          //     isLoading: false,
          //     actionText: "",
          //     stockName: ""
          //   },
          // () => {
          setTimeout(() => this.refs.toast.show("Watchlist deleted"), 200);

          // this.refs.watchlistActionPopUp.RBSheet.close();

          setTimeout(() => this.props.navigation.pop(), 1000);
          // }
          // );
          // this.props.navigation.navigate('Portfolio')

          // this.setState({
          //     isLoading: false,
          // }, () => {
          //     this.refs.watchlistActionPopUp.RBSheet.close();
          //     this.setState({
          //         actionText: '',
          //         stockName:""
          //     }, () =>  this.props.navigation.navigate('Watchlists'))
          // })
        },
        (message) => {
          this.setState({ isLoading: false });
          console.log("Message -->", message);

          if (message) this.refs.toast.show(message);
          else this.refs.toast.show("Please try again");
        }
      );
    } else {
      this.setState(
        {
          actionText: "",
          stockName: "",
          isLoading: false,
        },
        () => this.refs.watchlistActionPopUp.RBSheet.open()
      );
    }
  };

  handlePopup = () => {
    this.setState({ actionText: "Add a Stock or MF to" }, () => {
      this.refs.watchlistActionPopUp.RBSheet.open();
    });
  };

  toggleTextInputFocus = (currentState) => {
    this.setState({
      onFocusInput: currentState,
    });
  };

  renderWatchlistActionChilds = () => {
    if (this.state.actionText === "Delete") {
      const deleteText = `Are you sure about deleting this Watchlist ? This action can not be reversed.`;
      return (
        <View style={{ justifyContent: "space-between" }}>
          <View>
            <Text style={[{ fontSize: RF(2.2), color: colors.black }]}>{deleteText}</Text>
          </View>
          <View style={{ marginVertical: wp(6) }}></View>
          <View style={[styles.deleteWatchlistButtonContainer]}>
            <PopUpbutton
              buttonName={"Yes, delete"}
              buttonNameText={{ color: colors.white }}
              onPress={() => this.handleDelete("yes")}
            />
            <PopUpbutton
              buttonMainView={{ backgroundColor: colors.white, borderColor: colors.primary_color }}
              buttonName={"No, keep it"}
              onPress={() => this.handleDelete("no")}
            />
          </View>
        </View>
      );
    } else if (this.state.actionText === "Add a Stock or MF to") {
      return (
        <View>
          <Text style={{ color: colors.greyishBrown }}>
            {"Enter Stock Name"}
            <Text style={{ color: "red" }}>*</Text>
          </Text>
          <View style={[styles.inputContainer]}>
            <TextInput
              placeholder={"Stock or MF name"}
              // maxLength={15}
              numberOfLines={1}
              style={[styles.textInput, { color: this.state.onFocusInput ? colors.primary_color : colors.black }]}
              placeholderTextColor={colors.steelGrey}
              onChangeText={(value) => this.setInputText(value, "Add Stock")}
              value={this.state.stockName}
              onFocus={() => this.toggleTextInputFocus(true)}
              onBlur={() => this.toggleTextInputFocus(false)}
            />
          </View>
          {/* {this.state.searchDummyStocks.length > 0 ? ( */}
          <FlatList
            data={this.state.searchDummyStocks}
            style={{ height: hp(65) }}
            contentContainerStyle={[styles.StocksContainer]}
            // initialNumToRender={this.state.searchDummyStocks.length}
            extraData={this.state.searchDummyStocks}
            renderItem={({ item, index }) => this.renderStocksList(item, index)}
            keyboardShouldPersistTaps="always"
          />
          {/* ) : this.state.stockName ? (
            <TouchableOpacity
              style={{
                borderRadius: 5,
                marginTop: wp(5),
                backgroundColor: colors.primary_color
              }}
              onPress={() => this.addStockToCurrentWatchlist()}
              //   /  disabled={isWatchlistBtnDisabled}
            >
              <Text
                style={{
                  padding: wp(2.5),
                  textAlign: "center",
                  fontSize: RF(2.2),
                  color: colors.white
                }}
              >
                Add Stock to Watchlist
              </Text>
            </TouchableOpacity>
          ) : null} */}
        </View>
      );
    } else if (this.state.actionText === "Change Title") {
      const { errorAddWatchlistText, watchlistName, tempWatchlistName } = this.state;
      return (
        <View>
          <Text style={{ color: colors.black }}>
            {"Enter Watchlist Title"}
            <Text style={{ color: "red" }}>*</Text>
          </Text>
          <View style={[styles.inputContainer]}>
            <TextInput
              // placeholder={watchlistName}
              numberOfLines={1}
              style={styles.textInput}
              placeholderTextColor={colors.steelGrey}
              maxLength={60}
              onChangeText={(value) => this.setInputText(value, "renameWatchlist")}
              value={tempWatchlistName}
            />
          </View>
          <View
            style={{
              // paddingVertical: wp(0.5),
              justifyContent: "flex-start",
              alignContent: "flex-start",
            }}
          >
            <Text style={[{ color: colors.red }]}>{errorAddWatchlistText}</Text>
          </View>
          <TouchableOpacity
            style={{
              borderRadius: 5,
              backgroundColor: this.state.enableAddButton ? colors.primary_color : colors.greyish,
            }}
            onPress={() => (this.state.enableAddButton ? this.renameWatchlist() : null)}
          >
            <Text style={{ padding: wp(2.5), textAlign: "center", fontSize: RF(2.2), color: colors.white }}>
              Save Changes
            </Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View>
          <TouchableOpacity style={styles.actionItem} onPress={() => this.handleWatchlist("AddStock")}>
            <Image style={styles.actionIcon} source={require("../../assets/icons/add-blue.png")} />
            <Text style={styles.action}>Add a Stock</Text>
          </TouchableOpacity>
          {this.state.watchlistName === "Default" ? null : (
            <TouchableOpacity style={styles.actionItem} onPress={() => this.handleWatchlist("RenameWatchlist")}>
              <Image style={styles.actionIcon} source={require("../../assets/icons/edit-blue.png")} />
              <Text style={styles.action}>Change Title</Text>
            </TouchableOpacity>
          )}
          {this.state.watchlistName === "Default" ? null : (
            <TouchableOpacity style={styles.actionItem} onPress={() => this.handleWatchlist("DeleteWatchlist")}>
              <Image style={styles.actionIcon} source={require("../../assets/icons/delete.png")} />
              <Text style={[styles.action, { color: colors.red }]}>Delete Watchlist</Text>
            </TouchableOpacity>
          )}
        </View>
      );
    }
  };

  handleModalHeight = () => {
    const { actionText } = this.state;
    if (actionText === "Add a Stock or MF to") {
      return hp(90);
    } else if (actionText === "Change Title") {
      return hp(35);
    } else {
      return hp(38);
    }
  };

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;
    const { isSubscriber } = this.props.userState;
    // console.log("Portfolio List --->",portfolio)
    const {
      isLoading,
      stocks,
      dummyStocks,
      screen,
      selectedStock,
      refreshing,
      isFirstCallComplete,
      filterName,
      filterId,
      showFilters,
      showScrollToTop,
    } = this.state;
    let showEmpty = isFirstCallComplete && stocks.length == 0;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <View style={styles.container}>
          {this.renderStockList()}

          <WatchlistModal
            ref="watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => {
              setActiveWatchlist(obj);
              this.getWatchlistData();
            }}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />
          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.watchlists.open()}
          />

          <ScreenerFilters
            currentFilter={filterName}
            currentFilterId={filterId}
            showFilter={showFilters}
            toggle={() => this.toggleFilter()}
            setFilter={(id, name) => this.setFilter(id, name)}
            isSubscriber={isSubscriber}
          />

          <Toast
            ref="toast"
            position="center"
            style={[globalStyles.toast, { bottom: hp(10) }]}
            textStyle={globalStyles.toastText}
          />
          <LoginModal ref="loginModal" {...this.props} />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <StockAlertsModal
            ref="stockAlerts"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            addedTo={(name) => this.refs.toast.show("Added to Primary", 1000)}
            // data={this.state.dateList}
            // onPress={quarter => this.setQuarterString(quarter)}
          />

          <StockAddBottomSheet
            ref="watchlistActionPopUp"
            modalMainView={{ height: this.handleModalHeight() }}
            stockName={"Watchlist"}
            actionText={this.state.actionText}
            closeIconPress={() => this.handleCloseButton()}
            children={this.renderWatchlistActionChilds()}
          />

          {showEmpty && (
            <EmptyView
              heading={"Looks like an Empty list"}
              message={"Add stocks to your watchlist and get constant useful insights about them"}
              imageSrc={require("../../assets/icons/empty-watchlist.png")}
              button={"Add a Stock"}
              onButtonPress={() => this.handlePopup()}
            />
          )}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          {(isLoading || refreshing) && <LoadingView />}
        </View>
      </View>
    );
  }
}
let mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;
  // console.log("State--==-->", state, "Portfolio -->", state.portfolio.portfolio)
  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  { setActiveWatchlist, updateWatchlist, updatePortfolio, getActiveWatchlistByStockId, loadPortfolio, loadWatchlist },
  null,
  { forwardRef: true }
)(WatchlistDetailScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
    // backgroundColor:'red'
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    // paddingHorizontal: "3%"
  },
  StocksContainer: {
    minHeight: 0,
    backgroundColor: colors.whiteTwo,
    // borderRadius: 5,
    // shadowColor: "rgba(0, 0, 0, 0.3)",
    // shadowOffset: {
    //   width: 0,
    //   height: 2
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1,
    // elevation: 2,
    // zIndex: 1
  },
  // StocksListItemView: {
  //   paddingVertical: wp(3),
  //   paddingHorizontal: wp(1)
  // },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
    marginHorizontal: "3%",
  },
  footerButtonText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: wp(3),
    color: colors.primary_color,
  },

  performanceView: {
    flexDirection: "row",
    minHeight: hp(3.5),
    alignItems: "center",
    justifyContent: "center",
  },
  performanceDetail: {
    flex: 1,
    fontSize: RF(1.9),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
  },
  performanceIndex: {
    fontSize: RF(1.8),
    fontWeight: "500",
    color: colors.white,
    padding: 7,
    textAlign: "right",
  },
  hide: {
    display: "none",
  },
  listEndFooter: {
    flex: 1,
    alignItems: "center",
    justifyContent: "center",
    marginTop: hp(1),
  },
  listEndFooterText: {
    fontSize: RF(2.2),
    fontWeight: "500",
    padding: "3%",
    color: colors.pinkishGrey,
  },
  loaderContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
  },
  greenBackground: {
    backgroundColor: colors.green,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
  sortingContainer: {
    height: wp(20),
    alignItems: "center",
  },
  actionItem: {
    flexDirection: "row",
    alignItems: "center",
    paddingVertical: wp(2.6),
  },
  actionIcon: {
    width: wp(4.5),
    height: wp(5),
    resizeMode: "contain",
    // marginTop: 1,
  },
  action: {
    fontSize: RF(2.2),
    color: colors.primary_color,
    marginLeft: wp(4),
  },
  deleteWatchlistButtonContainer: {
    flexDirection: "row",
    justifyContent: "space-between",
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(4),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
  },
  emptyViewContainer: {
    justifyContent: "flex-start",
    paddingTop: wp(50),
    alignItems: "center",
    flex: 1,
  },
  stockListItem: {
    paddingHorizontal: wp(3),
    paddingVertical: wp(3),
    color: colors.greyishBrown,
    fontSize: RF(2.2),
  },
});
