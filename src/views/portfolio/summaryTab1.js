import React from "react";
import { View, Image, StyleSheet, Text, FlatList, TouchableOpacity } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { TagNameWithIcon } from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";
import { fetcher } from "../../controllers/Fetcher";
import { PORTFOLIO_ANALYSER_URL } from "../../utils/ApiEndPoints";
import { formatNumberToInr, roundNumber } from "../../utils/Utils";
import { POST_METHOD, MONTHS_SHORT } from "../../utils/Constants";
import Toast from "react-native-easy-toast";
import { GradientCardForDetail, SortingTagWithIcon, CardDetailText } from "../../components/portfolio/transactions";
import { Divider, ColoredBar, CompareFactor } from "../../components/portfolio/summary";
import LinearGradient from "react-native-linear-gradient";
import ChartView from "react-native-highcharts";
import { LINE_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";
import LoadingView from "../../components/common/LoadingView";
import PortfolioEmptyView from "../../components/portfolio/PortfolioEmptyView";
import StocksEmptyView from "../../components/portfolio/StocksEmptyView";
import MFEmptyView from "../../components/portfolio/MFEmptyView";
import { showPortfolioAddStocksSuggestion, showPortfolioAddMFSuggestion } from "../../actions/AppActions";

const CHART_TIMELINE = ["1M", "3M", "6M", "1Y", "5Y"];

class SummaryTab1 extends React.PureComponent {
  navRef = null;
  navConfig = JSON.parse(JSON.stringify(LINE_CHART_CONFIG));
  isNoMutualFunds = false;
  isNoStocks = false;

  state = {
    isLoading: false,
    isRefreshing: false,
    tabs: [{ name: "Summary" }, { name: "Stock Analysis" }, { name: "MF Analysis" }],
    selectedFilter: "Summary",
    allSummaryComponents: [
      { name: "CONSOLIDATED", img: require("../../assets/icons/portfolio/consolidatedIcon.png") },
      { name: "STOCKS", img: require("../../assets/icons/portfolio/stocksIcon.png") },
      { name: "MUTUAL FUNDS", img: require("../../assets/icons/portfolio/mutualFundsIcon.png") },
    ],
    consolidatedData: {},
    stocksData: {},
    mutualFundsData: {},
    navData: {},
    indicatorPosition: 0,
    selectedTimeline: "1Y",
    selectedOption: 3,
  };

  componentDidMount() {
    const { currentTab } = this.props;

    if (currentTab) {
      this.getSummaryData();
    }
  }

  getSummaryData = () => {
    let REQUEST_DATA;
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;

    this.setState({ isLoading: true });

    const URL = baseURL + PORTFOLIO_ANALYSER_URL;

    REQUEST_DATA = { summaryType: "consolidated" };

    fetcher(
      URL,
      POST_METHOD,
      REQUEST_DATA,
      baseRef,
      appToken,
      token,
      (json) => {
        const { navData, stockAnalysis, mfAnalysis, consolidated } = json.body;
        console.log("Response Summary Data -->", json);

        this.setState(
          {
            mutualFundsData: mfAnalysis ? mfAnalysis.summaryData : {},
            stocksData: stockAnalysis ? stockAnalysis.summaryData : {},
            consolidatedData: consolidated ? consolidated.summaryData : {},
            navData: navData ? navData : {},
          },
          () => this.checkIfPortfolioEmpty()
        );

        // this.setState({ isLoading: false });
        // this.setState(data);
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        // baseRef && baseRef.stopLoading();
      },
      (message, statusCode) => {
        // this.setState({ isLoading: false });
        console.log("Message -->", message);
        //handle error here
        // baseRef && baseRef.stopLoading();
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  checkIfPortfolioEmpty() {
    // const { isSubscriber } = this.props.userState;
    const { mutualFundsData, stocksData } = this.state;
    const { navStatus } = this.state.navData;

    this.isNoMutualFunds =
      Object.keys(mutualFundsData).length == 0 ||
      ("totalInvestment" in mutualFundsData && mutualFundsData.totalInvestment == 0);

    this.isNoStocks =
      Object.keys(stocksData).length == 0 || ("totalInvestment" in stocksData && stocksData.totalInvestment == 0);

    this.props.setEmpty(!this.isNoStocks, !this.isNoMutualFunds);
    if (navStatus == 0) this.navConfig = this.getLineChartConfigs(!this.isNoStocks && !this.isNoMutualFunds);

    this.setState({ isLoading: false });
  }

  setComponents = () => {
    const { allSummaryComponents, allStAnalysisComponents, allMFAnalysisComponents, selectedFilter } = this.state;
    if (selectedFilter) {
      if (selectedFilter === "Summary") {
        return allSummaryComponents;
      } else if (selectedFilter === "Stock Analysis") {
        return allStAnalysisComponents;
      } else {
        return allMFAnalysisComponents;
      }
    } else {
      return [];
    }
  };

  getLineChartConfigs = (isDarkTheme) => {
    const { chart_data } = this.state.navData;

    let data = chart_data && Array.isArray(chart_data) ? chart_data : [];
    let textColor = isDarkTheme ? colors.white : colors.black;
    // let key = "line";
    // if (!this.chartConfigs[key]) {
    let series1 = [];
    let series2 = [];
    let labels = [];
    let config = JSON.parse(JSON.stringify(LINE_CHART_CONFIG));

    data.slice(1).forEach((item) => {
      let date = Date.parse(item[0]);
      // let formattedDate = `${MONTHS_SHORT[date.getMonth()]} ${date.getDate()}`;

      // console.log("NAV date", item[0], date);

      // labels.push(date.toISOString());
      // labels.push(date);
      series1.push([date, item[1], item[2]]);
      series2.push([date, item[3], item[4]]);
    });

    config.rangeSelector = {
      buttons: [
        { type: "month", count: 1, text: "1m" },
        { type: "month", count: 3, text: "3m" },
        { type: "month", count: 6, text: "6m" },
        { type: "year", count: 1, text: "1y" },
        { type: "year", count: 5, text: "5y" },
      ],
      inputEnabled: false,
      buttonTheme: { visibility: "hidden" },
      labelStyle: { visibility: "hidden" },
      selected: 4,
    };

    config.xAxis.visible = true;
    config.xAxis.categories = labels;
    config.series[0].data = series1;
    config.series[1].data = series2;
    config.plotOptions.series.dataLabels.enabled = false;
    config.series[0].color = isDarkTheme ? colors.white : colors.primary_color;
    config.series[1].color = colors.barbiePink;

    if (isDarkTheme) {
      config.chart.backgroundColor = {
        // config.chart.plotBackgroundColor = {
        linearGradient: { x1: 0, x2: 0, y1: 0, y2: 1 },
        stops: [
          [0, colors.gradientCardStartColor],
          [1, colors.gradientCardEndColor],
        ],
      };
    }
    config.yAxis.opposite = false;
    config.yAxis.tickPixelInterval = wp(10);
    config.xAxis.labels = {
      format: "{value:%b %e}",
      style: { color: textColor },
    };

    config.yAxis.labels = { style: { color: textColor } };
    config.xAxis.tickPixelInterval = wp(16);

    config.tooltip = {
      positioner: function () {
        return { x: 25, y: chart.plotTop };
      },
      formatter: function () {
        var portfolio = this.points[0].y.toFixed(1);
        var nifty = this.points[1].y.toFixed(1);

        return (
          "<spa><span>" +
          // Highcharts.dateFormat("%I:%M %p", new Date(this.x)) +
          // "&nbsp;" +
          Highcharts.dateFormat("%e %b %Y", new Date(this.x)) +
          "</span>&nbsp;&nbsp;&nbsp;<span>Portfolio: " +
          portfolio +
          "</span>&nbsp;&nbsp;&nbsp;<span>Nifty50: " +
          nifty +
          "</span></spa>"
        );
      },
      useHTML: true,
      borderWidth: 0,
      borderRadius: 0,
      shadow: false,
      backgroundColor: "transparent",
      style: { fontSize: "14px", color: textColor },
      shared: true,
    };

    config.xAxis.lineColor = "rgba(255,255,255.0.05)";
    config.scrollbar = { enabled: false };
    config.navigator = { enabled: false };

    return config;
    // } else return this.chartConfigs[key];
  };

  gotoSubscription = () => {
    this.props.navigation.navigate("Subscription");
  };

  renderPageIndicator = () => {
    const { indicatorPosition, navData } = this.state;

    if (navData && Object.keys(navData).length > 1)
      return (
        <View style={styles.pageIndicator}>
          {[{}, {}].map((item, index) => {
            const indicatorStyle =
              indicatorPosition == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
            return <View style={indicatorStyle} key={index} />;
          })}
        </View>
      );
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      let currentIndex = viewableItems[0].index;
      this.setState({ indicatorPosition: currentIndex }, () => () =>
        this.navRef && this.navRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  setChartTimeline = (option, index) => {
    const { selectedOption } = this.state;
    //this.getStockChartConfig().rangeSelector.selected = 0;

    if (typeof this.navConfig.rangeSelector != undefined) {
      if (this.navConfig.rangeSelector) {
        this.navConfig.rangeSelector.selected = index;
      }

      this.setState({ selectedTimeline: option, selectedOption: index });
    }
  };

  getValue = (type, term) => {
    const { consolidatedData, stocksData, mutualFundsData } = this.state;
    let data = mutualFundsData;

    if (type === "CONSOLIDATED") {
      data = consolidatedData;
    } else if (type === "STOCKS") {
      data = stocksData;
    }
    const {
      currentValue,
      dayChangeValue,
      dayChangePercent,
      unrealizedGainValue,
      unrealizedGainPercent,
      losers,
      gainers,
      totalInvestment,
      totalNum,
    } = data;

    switch (term) {
      case "currentVal":
        return currentValue ? currentValue : "";
      case "totalInvestment":
        return totalInvestment ? totalInvestment : "";
      case "dayChangeValue":
        return dayChangeValue ? dayChangeValue : "";
      case "dayChangePercent":
        return dayChangePercent ? dayChangePercent : "";
      case "unrealizedGainValue":
        return unrealizedGainValue ? unrealizedGainValue : "";
      case "unrealizedGainPercent":
        return unrealizedGainPercent ? unrealizedGainPercent : "";
      case "gainers":
        return gainers ? gainers : 0;
      case "losers":
        return losers ? losers : 0;
      default:
        return "_";
    }
  };

  renderEmptyStocks() {
    return <View></View>;
  }

  // amountFormatter = (num) => {
  //   // let roundedNumber = num ? roundNumber(num).toFixed(0) : num;
  //   let x = num ? formatNumberToInr(num, true) : num;
  //   if (typeof x == "string") {
  //     let arr = x.split(" ");
  //     let label = arr.length > 2 ? arr[2] : arr.length == 2 ? arr[1] : "";
  //     return x.split(".")[0] + " " + label;
  //   } else {
  //     return x;
  //   }
  // };

  getConcatenatedValue = (rootName, subRootName) => {
    if (rootName) {
      let value, percent;

      if (subRootName === "unreleasedGain") {
        let unrealizedGain = this.getValue(rootName, "unrealizedGainPercent");
        value = formatNumberToInr(this.getValue(rootName, "unrealizedGainValue"), true);
        percent = !isNaN(unrealizedGain) && parseInt(unrealizedGain) > 0 ? `+${unrealizedGain}` : unrealizedGain;

        return `₹${value} (${percent}%)`;
      } else if (subRootName === "dayChange") {
        let dayChangeP = this.getValue(rootName, "dayChangePercent");
        value = this.getValue(rootName, "dayChangeValue");
        percent = !isNaN(dayChangeP) && parseInt(dayChangeP) > 0 ? `+${dayChangeP}` : dayChangeP;

        return `₹${formatNumberToInr(value, true)} (${percent}%)`;
      }
    }
  };

  renderStockMutualFunds = (parentData) => {
    let isStock = parentData.name === "STOCKS";
    let currentValue = formatNumberToInr(this.getValue(parentData.name, "currentVal"), true);
    let currentValueFormatted = currentValue ? `₹${currentValue}` : `₹ -`;

    let totalInvestment = formatNumberToInr(this.getValue(parentData.name, "totalInvestment"), true);
    let totalInvestmentFormatted = totalInvestment ? `₹${totalInvestment}` : `₹ -`;

    // let unrealizedGain = this.getConcatenatedValue(parentData.name, "unreleasedGain");

    let unrealizedGainValue = formatNumberToInr(this.getValue(parentData.name, "unrealizedGainValue"), true);
    let unrealizedGainP = this.getValue(parentData.name, "unrealizedGainPercent");
    unrealizedGainP =
      !isNaN(unrealizedGainP) && parseInt(unrealizedGainP) > 0 ? `(+${unrealizedGainP}%)` : `(${unrealizedGainP}%)`;

    let numberOfGainers = this.getValue(parentData.name, "gainers") ? this.getValue(parentData.name, "gainers") : 0;
    let numberOfLosers = this.getValue(parentData.name, "losers") ? this.getValue(parentData.name, "losers") : 0;

    let currentValueStyle = { fontWeight: "500", fontSize: RF(3.6), color: colors.black };

    let dayChangeValue = this.getValue(parentData.name, "dayChangeValue");
    dayChangeValue = `₹${formatNumberToInr(dayChangeValue, true)}`;
    // let dayChange = this.getConcatenatedValue(parentData.name, "dayChange");

    let dayChangeP = this.getValue(parentData.name, "dayChangePercent");
    dayChangeP = !isNaN(dayChangeP) && parseInt(dayChangeP) > 0 ? `(+${dayChangeP}%)` : `(${dayChangeP}%)`;
    return (
      <View style={[styles.card, { width: wp(94) }]}>
        <LinearGradient colors={[colors.azul, colors.primary_color]} style={styles.linearGradient} />

        <View style={[{ alignItems: "center", paddingTop: wp(2.5) }, {}]}>
          <CardDetailText Name="Current Value" CardText={[styles.CardHeadingText, { color: colors.black }]} />
          <CardDetailText
            Name={currentValueFormatted}
            CardTextView={{ marginTop: wp(1), marginBottom: wp(2) }}
            CardText={currentValueStyle}
            size="large"
            color={colors.black}
            // enableLoader={this.state.isLoading}
            // isLoading={true}
          />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal", color: colors.black }} Name="Total Investment" />
          <CardDetailText
            Name={totalInvestmentFormatted}
            //  enableLoader={this.state.isLoading}
            //   isLoading={true}
            CardText={[styles.CardTextStyle]}
          />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal", color: colors.black }} Name="Day Change" />
          <CardDetailText
            Name={dayChangeValue}
            biPartValues
            secondName={dayChangeP}
            // enableLoader={this.state.isLoading}
            //   isLoading={true}
            CardText={[styles.CardTextStyle]}
            CardText2={[styles.CardTextStyle]}
          />
        </View>

        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal", color: colors.black }} Name="Unrealized Gain" />
          <CardDetailText
            Name={unrealizedGainValue}
            color={colors.black}
            biPartValues
            secondName={unrealizedGainP}
            CardText={[styles.CardTextStyle]}
            CardText2={[styles.CardTextStyle]}
            // enableLoader={this.state.isLoading}
            //   isLoading={true}
          />
        </View>

        <Divider DividerMainView={{ marginVertical: wp(3), borderBottomColor: "rgba(77,77,77,0.3)" }} />

        <View style={[{ paddingVertical: wp(2) }]}>
          <ColoredBar Gainers={numberOfGainers} Losers={numberOfLosers} BarTextStyle={{ color: colors.black }} />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <SortingTagWithIcon
            TagText={isStock ? "All Stock Holdings" : "All MF Holdings"}
            ImageStyle={{ transform: [{ rotate: "270deg" }] }}
            TagTextStyle={{ color: colors.cobalt }}
            onPress={() => this.props.gotoHoldings(parentData.name)}
          />
          <SortingTagWithIcon
            TagText={isStock ? "View Stock Analysis" : "View MF Analysis"}
            ImageStyle={{ transform: [{ rotate: "270deg" }] }}
            TagTextStyle={{ color: colors.cobalt }}
            onPress={() => this.props.changeTab(parentData.name)}
          />
        </View>
      </View>
    );
  };

  renderOtherCards = (parentData) => {
    let currentValue = this.getValue(parentData.name, "currentVal");
    let currentValueFormatted = currentValue ? `₹${formatNumberToInr(currentValue, true)}` : "₹ -";

    let totalInvestment = this.getValue(parentData.name, "totalInvestment");
    let totalInvestmentFormatted = totalInvestment ? `₹${formatNumberToInr(totalInvestment, true)}` : "₹ -";

    // let dayChange = this.getConcatenatedValue(parentData.name, "dayChange");
    // let unrealizedGain = this.getConcatenatedValue(parentData.name, "unreleasedGain");

    let dayChangeValue = this.getValue(parentData.name, "dayChangeValue");
    dayChangeValue = `₹${formatNumberToInr(dayChangeValue, true)}`;
    // let dayChange = this.getConcatenatedValue(parentData.name, "dayChange");

    let dayChangeP = this.getValue(parentData.name, "dayChangePercent");
    dayChangeP = !isNaN(dayChangeP) && parseInt(dayChangeP) > 0 ? `(+${dayChangeP}%)` : `(${dayChangeP}%)`;

    let unrealizedGainValue = formatNumberToInr(this.getValue(parentData.name, "unrealizedGainValue"), true);
    let unrealizedGainP = this.getValue(parentData.name, "unrealizedGainPercent");
    unrealizedGainP =
      !isNaN(unrealizedGainP) && parseInt(unrealizedGainP) > 0 ? `(+${unrealizedGainP}%)` : `(${unrealizedGainP}%)`;

    let numberOfGainers = this.getValue(parentData.name, "gainers") ? this.getValue(parentData.name, "gainers") : 0;
    let numberOfLosers = this.getValue(parentData.name, "losers") ? this.getValue(parentData.name, "losers") : 0;

    let arrowIcon = require("../../assets/icons/portfolio/arrowWhite.png");

    return (
      <View>
        <View style={[{ alignItems: "center" }, {}]}>
          <CardDetailText Name="Current Value" CardText={[styles.CardHeadingText]} />

          <CardDetailText
            Name={currentValueFormatted}
            CardTextView={{ marginTop: wp(1), marginBottom: wp(2) }}
            CardText={{ fontWeight: "500", fontSize: RF(3.6) }}
            size="large"
            color={colors.white}
            // enableLoader={this.state.isLoading}
            // isLoading={true}
          />
        </View>

        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Total Investment" />
          <CardDetailText
            Name={totalInvestmentFormatted}
            // enableLoader={this.state.isLoading}
            // isLoading={true}
          />
        </View>

        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Day Change" />
          <CardDetailText
            Name={dayChangeValue}
            biPartValues
            secondName={dayChangeP}
            // enableLoader={this.state.isLoading}
            // isLoading={true}
          />
        </View>

        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Unrealized Gain" />
          <CardDetailText
            Name={unrealizedGainValue}
            biPartValues
            secondName={unrealizedGainP}
            //  enableLoader={this.state.isLoading}
            // isLoading={true}
          />
        </View>

        <Divider DividerMainView={{ marginVertical: wp(3), borderBottomColor: "rgba(255,255,255,0.3)" }} />

        <View style={[{ paddingVertical: wp(2) }]}>
          <ColoredBar Gainers={numberOfGainers} Losers={numberOfLosers} />
        </View>

        <View style={[styles.CardRowView, {}]}>
          <SortingTagWithIcon
            icon={arrowIcon}
            TagText={"All Stock Holdings"}
            TagTextStyle={{ color: colors.white }}
            onPress={() => this.props.gotoHoldings("STOCKS")}
          />

          <SortingTagWithIcon
            TagText={"All MF Holdings"}
            icon={arrowIcon}
            TagTextStyle={{ color: colors.white }}
            onPress={() => this.props.gotoHoldings("MUTUAL FUNDS")}
          />
        </View>
      </View>
    );
  };

  renderChilds = (parentData) => {
    if (parentData.name === "STOCKS" || parentData.name === "MUTUAL FUNDS") {
      return this.renderStockMutualFunds(parentData);
    } else {
      return this.renderOtherCards(parentData);
    }
  };

  renderChartTimeline = (isDarkTheme) => {
    const { selectedTimeline } = this.state;

    let itemTextStyle = isDarkTheme ? styles.timelineText : [styles.timelineText, { color: colors.greyishBrown }];
    let selectedItemTextStyle = isDarkTheme ? styles.blueText : { color: colors.white };
    let selectedItemStyle = isDarkTheme
      ? styles.activeTimeline
      : [styles.activeTimeline, { backgroundColor: colors.primary_color }];

    return (
      <View style={styles.timelineRow}>
        {CHART_TIMELINE.map((item, index) => {
          const containerStyle = selectedTimeline == item ? selectedItemStyle : {};
          const itemStyle = selectedTimeline == item ? [styles.timelineText, selectedItemTextStyle] : itemTextStyle;

          return (
            <View style={{ flex: 1 }}>
              <TouchableOpacity style={containerStyle} onPress={() => this.setChartTimeline(item, index)}>
                <Text style={itemStyle}>{item}</Text>
              </TouchableOpacity>
            </View>
          );
        })}
      </View>
    );
  };

  renderEmptyNav = (isDarkTheme) => {
    let cardStyle = {
      position: "absolute",
      top: 0,
      bottom: 0,
      left: 0,
      right: 0,
      borderRadius: 5,
      width: wp(95),
      overflow: "hidden",
    };
    let image = require("../../assets/icons/portfolio/nav-blue.png");

    let message = "Unlock premium features to get access to refined investing techniques and insights";

    let headingStyle = isDarkTheme ? styles.heading : [styles.heading, { color: colors.black }];
    let messageStyle = isDarkTheme ? styles.message : [styles.message, { color: colors.greyishBrown }];
    let buttonStyle = isDarkTheme ? styles.button : [styles.button, { backgroundColor: colors.cobalt }];
    let buttonTextStyle = isDarkTheme ? styles.buttonText : [styles.buttonText, { color: colors.white }];

    return (
      <View style={cardStyle}>
        <Image source={image} resizeMode="stretch" style={cardStyle} />

        <View style={{ flex: 1, justifyContent: "center", alignItems: "center", paddingHorizontal: wp(5) }}>
          <Text style={headingStyle}>Get Access to NAV chart </Text>
          <Text style={messageStyle}>{message}</Text>

          <TouchableOpacity style={buttonStyle} onPress={this.gotoSubscription}>
            <Text style={buttonTextStyle}>View Subscription Plans</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  };

  renderNav = (isDarkTheme) => {
    // const { isSubscriber } = this.props.userState;
    const { navStatus } = this.state.navData;
    let textStyle = isDarkTheme ? { color: colors.white } : { color: colors.black };
    let portfolioColor = isDarkTheme ? colors.white : colors.primary_color;
    let niftyColor = isDarkTheme ? colors.white : colors.barbiePink;

    if (navStatus == 4) return this.renderEmptyNav(isDarkTheme);
    else {
      let containerStyle = {
        paddingHorizontal: wp(1.5),
        paddingBottom: wp(5),
        // paddingTop: wp(3),
        flexDirection: "row",
        alignItems: "center",
      };

      return (
        <View>
          <View style={containerStyle}>
            <Text style={[{ fontWeight: "500", fontSize: RF(2.2), flex: 1 }, textStyle]}>NAV Analysis</Text>

            <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
              <CompareFactor
                name={"Portfolio"}
                color={portfolioColor}
                textStyle={{ color: portfolioColor }}
                containerStyle={{ paddingRight: wp(3) }}
              />
              <CompareFactor name={"Nifty50"} color={colors.barbiePink} textStyle={{ color: niftyColor }} />
            </View>
          </View>

          <ChartView
            style={{ width: wp(90), minHeight: wp(55), overflow: "hidden" }}
            config={this.navConfig}
            options={CHART_OPTIONS}
            stock
          />
          {this.renderChartTimeline(isDarkTheme)}
        </View>
      );
    }
  };

  renderCardListItem = ({ item, index }) => {
    const { isSubscriber } = this.props.userState;

    if (index == 0) {
      if (item.name === "CONSOLIDATED") {
        return (
          <GradientCardForDetail
            GradientCardMainView={{ marginHorizontal: wp(1.5), width: wp(94) }}
            child={this.renderChilds(item)}
          />
        );
      } else if (item.name === "STOCKS" || item.name === "MUTUAL FUNDS") {
        return this.renderChilds(item);
      }
    } else {
      // if (!this.isNoMutualFunds && !this.isNoStocks) {
      return (
        <GradientCardForDetail
          GradientCardMainView={{ width: wp(95), paddingHorizontal: wp(2.5), paddingBottom: 0 }}
          child={this.renderNav(true)}
        />
      );
      // } else {
      //   let cardStyle = { width: wp(94), paddingHorizontal: wp(2.5), marginHorizontal: 0.5, paddingBottom: 0 };

      //   return (
      //     <View style={[styles.card, cardStyle]}>
      //       <LinearGradient colors={[colors.azul, colors.primary_color]} style={styles.linearGradient} />
      //       {this.renderNav(false)}
      //     </View>
      //   );
      // }
    }
  };

  renderCardList = ({ item, index }) => {
    const { navData } = this.state;
    let showItem = true;
    let data = [item];

    if (item.name === "CONSOLIDATED") {
      data.push(navData);
    } else if (item.name === "STOCKS") {
      if (this.isNoStocks) showItem = false;
    } else if (item.name === "MUTUAL FUNDS") {
      if (this.isNoMutualFunds) showItem = false;
    }

    if (showItem) {
      return (
        <View>
          <View style={[styles.addingItemContainer, { flexDirection: "row", alignItems: "center" }]}>
            <TagNameWithIcon
              TagName={item.name ? item.name : ""}
              Icon={item.img ? item.img : null}
              headingContainer={{ flex: 1 }}
              headingText={{ flex: 1 }}
            />
            {data.length > 1 && this.renderPageIndicator()}
          </View>
          <FlatList
            ref={(ref) => data.length == 2 && (this.navRef = ref)}
            contentContainerStyle={{ paddingHorizontal: wp(1.5) }}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={data}
            //   extraData={this.state.topGainersLosers || this.state.refreshing}
            renderItem={this.renderCardListItem}
            keyExtractor={(item, index) => index.toString()}
            onViewableItemsChanged={this._onViewableItemsChanged}
            viewabilityConfig={this._viewabilityConfig}
          />
        </View>
      );
    }
  };

  renderFooter = () => {
    return <View style={{ marginVertical: wp(2) }}></View>;
  };

  render() {
    const {
      addHolding,
      showAddMF,
      showAddStocks,
      showPortfolioAddStocksSuggestion,
      showPortfolioAddMFSuggestion,
    } = this.props;
    const { isLoading, isRefreshing, isFirstCallComplete, consolidatedData, allSummaryComponents } = this.state;

    let showList =
      (Object.keys(consolidatedData).length > 0 || !this.isNoStocks || !this.isNoMutualFunds) && !isLoading;

    return (
      <View style={styles.container}>
        {showList && (
          <FlatList
            contentContainerStyle={[styles.SectionTwoListView]}
            data={allSummaryComponents}
            showsVerticalScrollIndicator={false}
            renderItem={this.renderCardList}
            keyExtractor={(item, index) => index.toString()}
            ListFooterComponent={this.renderFooter}
            removeClippedSubviews={false}
            initialNumToRender={2}
            // maxToRenderPerBatch={20}
            // extraData={this.state.stocks || this.state.refreshing}
          />
        )}

        {showAddStocks && this.isNoStocks && (
          <StocksEmptyView onButtonPress={addHolding} onHide={() => showPortfolioAddStocksSuggestion(false)} />
        )}
        {showAddMF && this.isNoMutualFunds && (
          <MFEmptyView onButtonPress={addHolding} onHide={() => showPortfolioAddMFSuggestion(false)} />
        )}
        {isLoading && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  let preferences = state.app.preferences ? state.app.preferences : {};
  const { showPortfolioAddMFSuggestion, showPortfolioAddStocksSuggestion } = preferences;

  let showAddMF = showPortfolioAddMFSuggestion != undefined ? showPortfolioAddMFSuggestion : true;
  let showAddStocks = showPortfolioAddStocksSuggestion != undefined ? showPortfolioAddStocksSuggestion : true;

  return { baseURL, appToken, userState: state.user, showAddMF, showAddStocks };
};

export default connect(mapStateToProps, { showPortfolioAddMFSuggestion, showPortfolioAddStocksSuggestion })(
  SummaryTab1
);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: hp(70),
    backgroundColor: colors.whiteTwo,
    // backgroundColor:'cyan',
    alignItems: "flex-start",
    flexDirection: "column",
    // paddingBottom:wp(5)
  },
  SectionOne: {
    flex: 2,
    // backgroundColor: 'red'
  },
  SectionTwo: {
    flex: 8,
    // backgroundColor: 'green'
  },
  SectionTwoListView: {},
  SectionOneListView: {
    // backgroundColor:'yellow',
    height: wp(20),
    alignItems: "center",
  },
  addingItemContainer: {
    paddingTop: wp(5),
    marginTop: wp(3),
    paddingVertical: wp(2.5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "flex-start",
    // justifyContent: 'space-between',
    // flexDirection: 'row',
    paddingHorizontal: wp(3),
  },
  CardHeadingText: {
    fontSize: RF(2.2),
    fontWeight: "normal",
  },
  CardRowView: {
    justifyContent: "space-between",
    flexDirection: "row",
    paddingVertical: wp(2),
    // backgroundColor:'green',
  },
  card: {
    marginTop: wp(1.5),
    marginBottom: wp(1),
    marginHorizontal: wp(1.5),
    paddingVertical: wp(2.5),
    paddingHorizontal: wp(5),
    backgroundColor: colors.white,
    borderRadius: 10,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    overflow: "hidden",
  },
  CardTextStyle: { color: colors.black },
  linearGradient: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    height: 5.2,
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
  timelineRow: {
    flexDirection: "row",
    marginTop: wp(3),
    // paddingVertical: 7.6,
    paddingHorizontal: wp(1),
    // backgroundColor: "rgba(36,83,161,0.03)",
    alignItems: "center",
    justifyContent: "center",
  },
  timelineText: {
    fontSize: RF(1.9),
    color: "rgba(255,255,255,0.5)",
    textAlign: "center",
    letterSpacing: 1,
    paddingVertical: 12.8,
  },
  activeTimeline: {
    // paddingVertical: 12.8,
    marginVertical: -wp(3),
    marginHorizontal: wp(2),
    borderTopLeftRadius: 5,
    borderTopRightRadius: 5,
    backgroundColor: colors.white,
  },
  blueText: {
    color: colors.blueBlue,
    fontWeight: "500",
  },
  message: {
    fontSize: RF(1.9),
    color: colors.white,
    marginTop: wp(2),
    lineHeight: wp(5.5),
    textAlign: "center",
    fontWeight: "500",
  },
  heading: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.white,
  },
  button: {
    flexDirection: "row",
    borderRadius: 5,
    backgroundColor: colors.white,
    marginHorizontal: wp(8),
    marginTop: hp(4),
    alignItems: "center",
    justifyContent: "center",
  },
  buttonText: {
    fontSize: RF(2.1),
    color: colors.cobalt,
    paddingVertical: wp(2.4),
    paddingHorizontal: wp(6),
    textAlign: "center",
    fontWeight: "500",
  },
  // chartContainer: {
  //   // marginVertical: wp(6),
  //   marginTop: wp(6),
  //   justifyContent: "center"
  // }
});
