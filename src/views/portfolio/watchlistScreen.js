import React from "react";
import { View, TextInput, StyleSheet, Platform, Text, TouchableOpacity, Image, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { globalStyles } from "../../styles/CommonStyles";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import {
  ListItem,
  TagNameWithIcon,
  ActionButtonWithIcon,
  CreateNewWatchlist,
} from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";
import { createNewWatchlist } from "../../controllers/WatchlistController";
import { fetcher } from "../../controllers/Fetcher";
import { GET_ALL_WATCHLIST_WITH_COUNT_URL } from "../../utils/ApiEndPoints";
import { GET_METHOD } from "../../utils/Constants";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import EmptyView from "../../components/common/EmptyView";
import { getActiveWatchlist } from "../../actions/WatchlistActions";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class WatchlistScreen extends React.PureComponent {
  tabs = ["Stock", "Screener", "Superstar", "News", "Notifications"];

  state = {
    isLoading: false,
    watchlistList: [],
    watchlistName: "",
    watchlistDescription: "",
    textFocused: null,
    isCreateWatchlistBtnActive: false,
    isFirstCallComplete: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    this.setNavigationListener();
    // if (this.props.isSelected) {
    //   this.getData();
    // }
  }

  componentWillUnmount() {
    this.navListener.remove();
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  setNavigationListener = () => {
    const { userState, baseURL, appToken, getActiveWatchlist } = this.props;
    const { token, isGuestUser } = userState;

    this.navListener = this.props.navigation.addListener("willFocus", () => {
      if (this.props.isSelected) {
        this.getData();
        getActiveWatchlist(baseURL, appToken, token);
      }
    });
  };

  getData = () => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const { isFirstCallComplete } = this.state;

    const URL = baseURL + GET_ALL_WATCHLIST_WITH_COUNT_URL;
    this.setState({ isLoading: true });

    fetcher(
      URL,
      GET_METHOD,
      null,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body } = json;
        let data = body.watchlists ? body.watchlists : [];
        this.setState({ watchlistList: data, isLoading: false });

        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      },
      (message) => {
        this.setState({ isLoading: false });
        console.log("Message -->", message);
      }
    );
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  renderWatchlistItem = ({ item, index }) => {
    const { navigate } = this.props.navigation;
    let navigationData = {
      keyName: item[1] ? item[1] : "",
      keyId: item[0] ? item[0] : "",
      keyDesc: item[2] ? `${item[2]} Stocks` : "No Stocks",
    };

    return (
      <ListItem
        key={item[0]}
        name={item[1]}
        description={item[2]}
        onPress={() => navigate("WatchlistDetails", navigationData)}
      />
    );
  };

  renderWatchlist = () => {
    if (this.state.watchlistList.length > 0)
      return (
        <FlatList
          ref={(ref) => (this.listRef = ref)}
          contentContainerStyle={styles.listContainer}
          data={this.state.watchlistList}
          showsVerticalScrollIndicator={false}
          renderItem={this.renderWatchlistItem}
          ItemSeparatorComponent={() => <View style={styles.divider} />}
          extraData={this.state.watchlistList}
          onScroll={this.handleScroll}
        />
      );
  };

  handlePopup() {
    const { baseRef, userState } = this.props;
    const { watchlistList } = this.state;

    if (userState.isSubscriber || watchlistList.length < 3) this.watchlistActionPopUp.open();
    else baseRef && baseRef.showSubscribe();
  }

  closeDialog() {
    this.watchlistActionPopUp.close();
  }

  setInputText = (type, value) => {
    if (type == "name") {
      if (value.length <= 60) {
        this.setState({ watchlistName: value, isCreateWatchlistBtnActive: value.length > 0 });
      }
    } else {
      this.setState({ watchlistDescription: value });
    }
  };

  toggleTextInputFocus = (value) => {
    this.setState({ textFocused: value });
  };

  createWatchlist = () => {
    const { baseURL, appToken, userState } = this.props;
    const { token } = userState;

    const title = this.state.watchlistName;
    const description = this.state.watchlistDescription;

    this.setState({ isLoading: true });

    createNewWatchlist(baseURL, appToken, token, title, description, (watchlistId, message) => {
      console.log("--->Response", watchlistId, message);
      this.watchlistActionPopUp.close();
      if (watchlistId != -1) {
        this.refs.toast.show("Watchlist created");
        this.getData();
      } else {
        this.setState({ isLoading: false });
        if (message) this.refs.toast.show(message);
        else this.refs.toast.show("Please try again");
      }
      this.setState({ isLoading: false });
    });
  };

  renderChilds() {
    const buttonStyle = this.state.isCreateWatchlistBtnActive
      ? styles.createWatchlistBtn
      : [styles.createWatchlistBtn, styles.greyBackground];

    const buttonText = this.state.isCreateWatchlistBtnActive
      ? styles.createWatchlistBtnText
      : [styles.createWatchlistBtnText, styles.whiteText];
    const isWatchlistBtnDisabled = this.state.watchlistName.length == 0;
    const { textFocused, watchlistName, watchlistDescription } = this.state;

    return (
      <View>
        <Text style={styles.inputLabel}>
          Enter Watchlist Title
          <Text style={{ color: colors.red }}>* </Text>
        </Text>
        <View style={[styles.inputContainer, textFocused === "name" ? styles.activeTextBox : {}]}>
          <TextInput
            maxLength={60}
            style={styles.textInput}
            placeholderTextColor={colors.steelGrey}
            onChangeText={(value) => this.setInputText("name", value)}
            onFocus={() => this.toggleTextInputFocus("name")}
            onBlur={() => this.toggleTextInputFocus(null)}
          />
          <Text style={styles.inputLength}>{60 - watchlistName.length}</Text>
        </View>
        <View>
          <Text style={styles.inputLabel}>Enter Watchlist description (optional)</Text>
          <View style={[styles.inputContainer, textFocused === "description" ? styles.activeTextBox : {}]}>
            <TextInput
              maxLength={260}
              style={styles.textInput}
              placeholderTextColor={colors.steelGrey}
              onChangeText={(value) => this.setInputText("description", value)}
              onFocus={() => this.toggleTextInputFocus("description")}
              onBlur={() => this.toggleTextInputFocus(null)}
            />
            <Text style={styles.inputLength}>{260 - watchlistDescription.length}</Text>
          </View>
        </View>
        <TouchableOpacity style={buttonStyle} onPress={() => this.createWatchlist()} disabled={isWatchlistBtnDisabled}>
          <Text style={buttonText}>Create Watchlist</Text>
        </TouchableOpacity>

        {this.state.isLoading && <LoadingView />}
      </View>
    );
  }

  render() {
    const { isLoading, watchlistList, isFirstCallComplete, showScrollToTop } = this.state;
    const { isSelected } = this.props;
    const { isGuestUser } = this.props.userState;

    let showList = isFirstCallComplete && watchlistList.length > 0;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {showList && (
            <View style={[styles.addingItemContainer]}>
              <TagNameWithIcon />
              <ActionButtonWithIcon onPress={() => this.handlePopup()} />
            </View>
          )}

          <Toast ref="toast" position="top" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {this.renderWatchlist()}

          <CreateNewWatchlist
            PopupRef={(modal) => (this.watchlistActionPopUp = modal)}
            closeDialog={() => this.closeDialog()}
            child={this.renderChilds()}
          />

          {isGuestUser && (
            <EmptyView
              heading={"Keep an Eye Open!"}
              message={"Create and add stocks to your watchlists and stay constantly updated about them"}
              imageSrc={require("../../assets/icons/closed-eye-grey.png")}
              button={isGuestUser ? null : "Create a Watchlist"}
              onButtonPress={() => this.handlePopup()}
            />
          )}

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
          {isLoading && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};
export default connect(mapStateToProps, { getActiveWatchlist }, null, { forwardRef: true })(WatchlistScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  addingItemContainer: {
    paddingVertical: wp(5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: wp(3),
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    backgroundColor: colors.greyish,
  },
  listContainer: {
    backgroundColor: colors.white,
  },
  emptyViewContainer: {
    justifyContent: "center",
    alignItems: "center",
    flex: 1,
  },
  inputContainer: {
    flexDirection: "row",
    paddingVertical: Platform.OS == "ios" ? 6 : 0,
    marginBottom: wp(6),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: {
    borderBottomWidth: 2,
    borderBottomColor: colors.primary_color,
  },
  textInput: {
    flex: 1,
    marginRight: wp(4),
    fontSize: RF(2.2),
    color: colors.black,
    marginBottom: Platform.OS == "android" ? -wp(3) : 0,
    marginTop: Platform.OS == "android" ? -wp(1) : wp(2.6),
    marginLeft: Platform.OS == "android" ? -wp(1) : 0,
  },
  createWatchlistBtn: {
    borderRadius: 5,
    marginTop: wp(5),
    backgroundColor: colors.primary_color,
  },
  createWatchlistBtnText: {
    padding: wp(3),
    textAlign: "center",
    fontSize: RF(2.2),
    color: colors.white,
  },
  greyBackground: {
    backgroundColor: colors.steelGrey,
  },
  whiteText: {
    color: colors.white,
  },
});
