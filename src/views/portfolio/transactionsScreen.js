import React from "react";
import {
  View,
  Text,
  Platform,
  Image,
  FlatList,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
  RefreshControl,
  ActivityIndicator,
} from "react-native";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import Toast from "react-native-easy-toast";
import DateTimePicker from "react-native-modal-datetime-picker";

import { connect } from "react-redux";
import { loadPortfolio } from "../../actions/PortfolioActions";
import { fetcher } from "../../controllers/Fetcher";
import { PORTFOLIO_TRANSACTIONS_URL, DELETE_TRANSACTIONS_URL, EDIT_TRANSACTION_URL } from "../../utils/ApiEndPoints";
import { GET_METHOD, POST_METHOD, MONTHS_FULL } from "../../utils/Constants";

import { colors, globalStyles } from "../../styles/CommonStyles";
import { formatNumberToInr, getOrdinalNumForDate, dateStringFromObj } from "../../utils/Utils";

import ScreenerStockCard from "../../components/screeners/ScreenerStockCard";
import { getScreenerData } from "../../controllers/ScreenerController";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import StockAlertsModal from "../../components/common/StockAlertsModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";

import NoMatchView from "../../components/common/NoMatchView";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import LoginModal from "../../components/common/LoginModal";
import { SubmitButton, InputField } from "../../components/portfolio/AddHoldings";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";
import {
  SearchBoxwithCloseButton,
  GradientCardForDetail,
  CardDetailText,
  SortingTagWithIcon,
  getDateRange,
  EmptyTransaction,
} from "../../components/portfolio/transactions";
import { CHIPS, TagNameWithIcon, sortDataByKey, PopUpbutton } from "../../components/portfolio/watchlist";
import SearchStockResultsComponent from "../../components/portfolio/searchcomponent";
import { searchStockByTerm } from "../../controllers/StocksController";
import { CardButton } from "../../components/portfolio/holdings";
import ScrollToTopView from "../../components/common/ScrollToTopView";

const closedTransactionIcon = require("../../assets/icons/portfolio/closedTransactionIcon.png");
const searchIcon = require("../../assets/icons/portfolio/searchIcon.png");
const moreIcon = require("../../assets/icons/portfolio/moreIcon.png");
const sortIcon = require("../../assets/icons/filter-white.png");

function getStaticDateRange(dt) {
  const endYear = 2012;
  const month = dt.getMonth() + 1;
  const year = dt.getFullYear();

  let returnList = [],
    startDate,
    endDate;
  returnList.push({
    endDate: "",
    range: "Most Recent",
    startDate: "",
  });
  if (month > 3) {
    startDate = new Date(year, 3, 1);
    endDate = new Date(year + 1, 2, 31);
  } else {
    startDate = new Date(year - 1, 3, 1);
    endDate = new Date(year, 2, 31);
  }
  returnList.push({
    endDate: dateStringFromObj(endDate),
    range: "Year till date",
    startDate: dateStringFromObj(startDate),
  });
  for (let i = year; i >= endYear; i--) {
    let thisYearStr = "" + i;
    let prevYearStr = "" + (i - 1);
    returnList.push({
      endDate: dateStringFromObj(new Date(i, 2, 31)), //"2019-03-31",
      range: "Apr'" + prevYearStr.substring(2, 4) + " - Mar'" + thisYearStr.substring(2, 4),
      startDate: dateStringFromObj(new Date(i - 1, 3, 1)), //"2018-04-1",
    });
  }
  return returnList;
}
class TransactionsScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isFirstCallComplete: false, // enables on first API call.
      isLoading: false, //active while API request under process.
      stocks: [], //transactions entries.
      freshUser: false, //checking for user which have no transactions.
      selectedCatagory: ["", "All Portfolios"], //default Category filter object.
      selectedTimePeriod: getStaticDateRange(new Date())[0], //default selected time period filter object.
      selectedPortfolioId: ["", ""], //default selected portfolio data from catagory filter.
      stockId: "", //stock id of stock after user search and select.
      pageNumber: 0, // default Page Number.
      allClosedTransactions: [], //all closed transactions.
      searchByStockAll: [], //all search by stock transactions.
      allFilterizedTransactions: [], //all filterized transactions.
      sortingFilters: [], //all sorting parameters.
      sortingFilterParams: ["qty", "close_date", "open_date", "openprice", "closeprice"], //list of valid sorting params.
      reverseOrder: false, //flag for sorting orders.
      refreshing: false, //refreshing for sorting filters.
      enableSearchBox: false, //flag for enabling searchbox.
      actionText: "", //heading of popup text
      transactionsCatagoryList: [
        ["", "All Portfolios", "type"],
        ["stocks", "All Stocks", "type"],
        ["mf", "All MFs", "type"],
        // ["portfolio", "Portfolio", "type"],
      ], //default category options.
      isNextPage: false, // next page availability.
      keywordNamedList: [], //matching stock search results.
      searchResultsReady: false, //when search results ready after typing.
      searchText: "", //typing search stock text
      selectedFilter: "Sell Date", //default selected sorting filter.
      transactionId: "", //transaction id of perticular transaction.
      errorText: "", //error text to show
      //form variables
      buyDate: { date: new Date(), formetted: "Select Date" },
      buyDateStatus: { status: false, error: "" },
      buyPrice: "",
      buyPriceStatus: { status: false, error: "" },
      sellPrice: "",
      sellPriceStatus: { status: false, error: "" },
      sellDate: { date: new Date(), formetted: "Select Date" },
      sellDateStatus: { status: false, error: "" },
      totalShare: "",
      totalShareStatus: { status: false, error: "" },
      sellShare: "",
      sellShareStatus: { status: false, error: "" },
      dateType: "",
      activeSubmitBtn: false,
      portfolioData: [],
      transactionData: {},
      inputFocused: "", //focused input
      dateRangeStatic: getStaticDateRange(new Date()),
      showScrollToTop: false,
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  //function for calling API on transaction tab open.
  getData = () => {
    const { userState, baseURL, appToken, loadPortfolio } = this.props;
    const { token, isGuestUser } = userState;
    this.getAllTransactionsData("", true, {});
    if (!isGuestUser) loadPortfolio(baseURL, appToken, token);
  };

  //function for creating date object.
  convertDateObject = (into, date) => {
    console.log("Type", into, "Date -->", date);
    let res_Date, day, year, monthApi, month, formattedDate;
    if (into === "API_Formet") {
      res_Date = new Date(date.dateObj ? date.dateObj : date);
      day = res_Date.getDate();
      year = res_Date.getFullYear();
      monthApi = res_Date.getMonth() + 1 < 10 ? "0" + (res_Date.getMonth() + 1) : res_Date.getMonth() + 1;
      dateForApi = year + "-" + monthApi + "-" + day;
      return dateForApi;
    } else {
      res_Date = new Date(date);
      day = res_Date.getDate() > 10 ? res_Date.getDate() : `0${res_Date.getDate()}`;
      // month = MONTHS_FULL[res_Date.getMonth()];
      month = res_Date.getMonth() + 1 > 10 ? res_Date.getMonth() + 1 : `0${res_Date.getMonth() + 1}`;
      year = res_Date.getFullYear();
      formattedDate = `${day}-${month}-${year}`;
      return { formetted: formattedDate, dateObj: res_Date };
    }
  };

  //function for creating request data for main API.
  createRequestData = (action, toRefresh, payload) => {
    const {
      selectedCatagory,
      selectedTimePeriod,
      selectedPortfolioId,
      stockId,
      pageNumber,
      isNextPage,
      isFirstCallComplete,
    } = this.state;
    let requestBody = {};
    console.log(action, toRefresh, payload, getDateRange(8));
    console.log("selectedTimePeriod", selectedTimePeriod);
    requestBody = {
      startDate: selectedTimePeriod.startDate,
      endDate: selectedTimePeriod.endDate,
    };
    if (action === "TransactionsType" || action === "TimePeriod") {
      requestBody.portfolioType = selectedCatagory[0] ? selectedCatagory[0] : "";

      // alert(selectedTimePeriod.endDate)
      if (selectedPortfolioId[0] != "" && payload[2] !== "type") {
        requestBody.portfolioIds = selectedPortfolioId[0] ? [selectedPortfolioId[0]] : null;
        requestBody.portfolioType = ["stocks", "MF"];
      }
      if (isFirstCallComplete) {
        requestBody.pageNumber = toRefresh ? 0 : isNextPage ? ++this.state.pageNumber : 0;
      }
      if (payload.key && payload.key === "portfolio") {
        requestBody = null;
      }
      this.refs.watchlistActionPopUp.RBSheet.close();
    } else if (action && action == "searchByStock") {
      this.setState({ pageNumber: 0 });
      requestBody = { stock_id: stockId, pageNumber: 0 };
      if (isFirstCallComplete) {
        requestBody = { pageNumber: toRefresh ? 0 : isNextPage ? ++this.state.pageNumber : 0 };
      }
    } else {
      if (isFirstCallComplete) {
        requestBody = { pageNumber: toRefresh ? 0 : isNextPage ? ++this.state.pageNumber : 0 };
      }
    }
    return requestBody;
  };

  //function for checking unchangedFilter
  // checkingAltFilters=() =>{
  //   let flag = false;
  //   this.setState({

  //   })
  //   if () {

  //   }
  // }

  //function for setting stocks from API
  setDataToCards = (type, refreshData, payload) => {
    let tmpStocks = [];
    const { allClosedTransactions, searchByStockAll, allFilterizedTransactions } = this.state;
    if (type === "searchByStock") {
      this.setState({ searchByStockAll: [...this.state.searchByStockAll, ...payload] }, () => {
        this.setState(
          {
            stocks: payload ? (refreshData ? payload : [...this.state.searchByStockAll]) : [],
          },
          () => {
            if (this.state.stocks.length == 0) {
              this.setState({ noTransactions: true });
            } else {
              this.setState({ noTransactions: false });
            }
          }
        );
      });
    } else if (type === "TransactionsType" || type === "TimePeriod") {
      this.setState(
        {
          allFilterizedTransactions: refreshData ? payload : [...this.state.allFilterizedTransactions, ...payload],
          // stocks: payload ? payload : [],
        },
        () => {
          console.log("allFilterizedTransactions", this.state.allFilterizedTransactions);
          this.setState(
            {
              // stocks: payload ? refreshData ? payload : [...this.state.allFilterizedTransactions] : [],
              stocks: [...this.state.allFilterizedTransactions],
            },
            () => {
              if (this.state.stocks.length == 0) {
                this.setState({ noTransactions: true });
              } else {
                this.setState({ noTransactions: false });
              }
            }
          );
        }
      );
    } else {
      this.setState(
        {
          allClosedTransactions: [...this.state.allClosedTransactions, ...payload],
        },
        () => {
          this.setState(
            {
              stocks: payload ? (refreshData ? payload : [...this.state.allClosedTransactions]) : [],
            },
            () => {
              if (this.state.stocks.length == 0) {
                this.setState({ noTransactions: true });
              } else {
                this.setState({ noTransactions: false });
              }
            }
          );
        }
      );
    }
    return tmpStocks;
  };

  //function to get all transactions data
  getAllTransactionsData = (type, refreshData, requests) => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const { isFirstCallComplete, pageNumber, isNextPage, stocks, isLoadingMore, allClosedTransactions } = this.state;
    const URL = baseURL + PORTFOLIO_TRANSACTIONS_URL;
    let requestObj = {};
    this.setState(
      {
        isLoading: isLoadingMore ? false : true,
      },
      () => {
        requestObj = this.createRequestData(type, refreshData, requests);
        console.log("Request Data -->", requestObj);
        fetcher(
          URL,
          POST_METHOD,
          requestObj,
          baseRef,
          appToken,
          token,
          (json) => {
            const { body, head } = json;
            console.log("transactions Response-->", json);
            if (body === "No Transactions available") {
              console.log("No transactions Case -->", body);
              this.refs.toast.show(body, 500);
              if (!isFirstCallComplete) {
                this.setState({ freshUser: true, noTransactions: false });
              } else {
                this.setState({ freshUser: false, noTransactions: true });
              }
            }
            let dataObj = this.mapScreenerData(body, head);
            this.setDataToCards(type, refreshData, dataObj.stocks),
              this.setState({
                // stocks: this.setDataToCards(type, refreshData, dataObj.stocks),
                // stocks: dataObj.stocks ? refreshData ? dataObj.stocks : [...this.state.allClosedTransactions] : [],
                isLoading: false,
                refreshing: false,
                netProfitAbs: body.netProfitAbs ? body.netProfitAbs : "__",
                totalTradedAmount: body.totalTraded ? body.totalTraded : "__",
                netProfit: body.netProfitPercent ? body.netProfitPercent : "__",
                sortingFilters: this.createSortingParameters(json.body.tableHeaders),
                // pageNumber: refreshData ? 0 : ++this.state.pageNumber,
                isNextPage: dataObj.isNextPage,
                isLoadingMore: false,
              });
            console.log("Stock length -->", this.state.stocks.length);
            if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
            baseRef && baseRef.stopLoading();
          },
          (message) => {
            this.setState({
              isLoading: false,
              stocks: [],
              sortingFilters: [],
              isLoadingMore: false,
              refreshing: false,
            });
            console.log("Transaction API RESPONSE -->", message);
            // alert(message)
            if (message == "failed" || "No Transactions available") {
              this.setState(
                {
                  noTransactions: true,
                },
                () => {
                  this.refs.toast.show("No transactions available", 500);
                }
              );
            }
            // if (message !== "failed") {
            //   // this.refs.toast.show(message, 500);
            // } else {
            //   this.refs.toast.show("No transactions available", 500);
            //   this.setState({
            //     noTransactions: true,
            //   });
            // }

            //handle error here
            baseRef && baseRef.stopLoading();
            if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
          }
        );
      }
    );
  };

  //function for Mapping Data
  mapScreenerData = (json, pageData) => {
    let stockList = [];
    let columnList = json.tableHeaders;
    let screenData = json.screen ? json.screen : {};
    json.tableData.forEach((stockIndex) => {
      let stockObj = [];
      let stockArray = [];
      let dvmValueCrossed = false; // to get data which is after dvm in json object
      stockIndex.forEach((item, i) => {
        let column = Object.assign({}, columnList[i]);
        let columnName = column.unique_name ? column.unique_name : column.name;
        if (dvmValueCrossed) {
          column["value"] = item;
          stockArray.push(column);
        }
        if (column.unique_name == "m_color") dvmValueCrossed = true;
        stockObj[columnName] = item;
      });
      stockList.push({ obj: stockObj, array: stockArray });
    });

    return {
      isNextPage: pageData.isNextPage,
      order: json.order ? json.order : "",
      description: json.description ? json.description : "",
      title: json.title ? json.title : "",
      sortBy: json.sortBy ? json.sortBy : "",
      pageNumber: pageData.pageNumber,
      pageCount: pageData.perPageCount ? pageData.perPageCount : "",
      stocks: stockList,
      screen: screenData,
    };
  };

  //no transactions view
  renderNoTransactionsYetView = () => {
    return (
      <View style={{ height: hp(75), justifyContent: "center" }}>
        <EmptyTransaction mainView={{ opacity: this.state.isLoading ? 0.2 : 1 }} />
      </View>
    );
  };

  //for refreshing data
  onRefresh = () => {
    this.setState({ refreshing: true, pageNumber: 0, isNextPage: true }, () => {
      if (this.state.selectedCatagory[1] === "All Portfolios") {
        this.getAllTransactionsData("", true, {});
      } else {
        this.getAllTransactionsData("TransactionsType", true, this.state.selectedCatagory);
      }
    });
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  //function to enable/disable navigation to stock overview screen.
  enableStockOverviewScreen = () => {
    const { selectedCatagory } = this.state;
    let flag = false;
    if (selectedCatagory[1] === "All MFs") {
      flag = true;
    }
    return flag;
  };

  //list Item Box
  renderStockItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;
    const stockId = item.obj.stock_id;
    const isWatchlistActive = stockId in activeWatchlist && activeWatchlist[stockId].length > 0;
    return (
      <ScreenerStockCard
        {...this.props}
        stock={item}
        preventOverView={this.enableStockOverviewScreen()}
        appliedPageName="transactions"
        relatedPortfolio
        position={index}
        watchlistBtnState={isWatchlistActive}
        showWatchlist={() => this.showWatchlist(item)}
        showAlerts={() => this.showAlerts(item)}
        showPortfolio={() => this.showPortfolio(item)}
        children={this.renderChildButtons(item)}
      />
    );
  };

  //function for handling card button click
  handleBtnClick = (action, transaction_Data) => {
    const {
      id,
      stock_id,
      open_date,
      portfolio_id,
      openprice,
      portfolio,
      close_date,
      closeprice,
      qty,
    } = transaction_Data.obj;
    if (action === "deleteTransaction") {
      this.setState({ transactionId: id, actionText: "Delete Transaction:" }, () =>
        this.refs.watchlistActionPopUp.RBSheet.open()
      );
    } else {
      let obj = { status: true, error: "" };
      let tempObj = { transactionId: id, stockId: stock_id, portfolioId: portfolio_id };
      this.setState(
        {
          transactionId: id,
          actionText: "Edit Transaction:",
          buyDate: this.convertDateObject("show_Formet", open_date),
          buyDateStatus: obj,
          buyPrice: openprice.toString(),
          buyPriceStatus: obj,
          sellDate: this.convertDateObject("show_Formet", close_date),
          sellDateStatus: obj,
          sellPrice: closeprice.toString(),
          sellPriceStatus: obj,
          sellShare: qty.toString(),
          sellShareStatus: obj,
          activeSubmitBtn: false,
          transactionData: tempObj,
          portfolioData: [portfolio, this.getPortfolioID(portfolio)],
        },
        () => this.refs.watchlistActionPopUp.RBSheet.open()
      );
    }
  };

  //function for getting id of selected portfolio.
  getPortfolioID = (name) => {
    let id;
    const { portfolio } = this.props;
    portfolio.forEach((ele) => {
      if (name === ele[1]) id = ele[0];
    });
    return id;
  };

  //child of list Item Box
  renderChildButtons = (data) => {
    const { alertBtnActive, portfolioActive } = this.state;
    return (
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingTop: wp(3),
        }}
      >
        <CardButton
          ButtonName="Undo Sell"
          disableImg
          onPress={() => this.handleBtnClick("deleteTransaction", data)}
          {...this.props}
          buttonMainView={{ width: wp(40) }}
        />
        <CardButton
          ButtonName="Edit"
          disableImg
          onPress={() => this.handleBtnClick("editTransaction", data)}
          {...this.props}
          buttonMainView={{ width: wp(40) }}
        />
      </View>
    );
  };

  // function for Validating Params for sorting
  checkParams = (entityName) => {
    const { sortingFilterParams } = this.state;
    const status = sortingFilterParams.findIndex((ele) => ele === entityName);
    return status != -1 ? true : false;
  };

  //function for creating sorting parameters list.
  createSortingParameters = (MappingData) => {
    const last = "m_color";
    let compInd = MappingData ? MappingData.findIndex((ele) => ele.unique_name === last) : -1;
    let tempArray = MappingData.filter((ele, ind) => ind > compInd && this.checkParams(ele.unique_name));
    return tempArray;
  };

  //Sorting Parameters View
  renderSortingChips = () => {
    return (
      <FlatList
        style={{ marginHorizontal: wp(1) }}
        contentContainerStyle={[styles.sortingContainer]}
        data={this.state.sortingFilters}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <CHIPS
            name={item.name ? item.name : ""}
            gradient
            onPress={() => this.handleSortingClick(item, index)}
            selected={item.name === this.state.selectedFilter}
            doubleSelection={item.name === this.state.selectedFilter && this.state.reverseOrder}
          />
        )}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        removeClippedSubviews
        initialNumToRender={8}
        maxToRenderPerBatch={5}
        extraData={this.state}
        keyboardShouldPersistTaps="always"
      />
    );
  };

  //function to handle on click of sorting parameteres.
  handleSortingClick = (ele, ind) => {
    const { selectedFilter } = this.state;
    if (selectedFilter === ele.name) {
      this.setState({ reverseOrder: !this.state.reverseOrder, refreshing: !this.state.refreshing }, () => {
        this.handleSortingData(ele.name);
      });
    } else {
      this.setState(
        {
          selectedFilter: ele.name ? ele.name : "",
          reverseOrder: false,
          refreshing: !this.state.refreshing,
        },
        () => {
          this.handleSortingData(ele.name);
        }
      );
    }
  };
  handleSortingData = (type) => {
    const { stocks, reverseOrder } = this.state;
    this.setState(
      {
        stocks: sortDataByKey(stocks, reverseOrder ? "lowest_first" : "highest_first", type),
        refreshing: !this.state.refreshing,
      },
      () => {
        console.log("updated Sorted stocks--->", stocks);
      }
    );
  };

  //Gradient Card Child
  renderGradientCardChilds = () => {
    const { totalTradedAmount, netProfit, netProfitAbs } = this.state;
    let TransactionsData = `₹${formatNumberToInr(netProfitAbs, true)}(${formatNumberToInr(netProfit, true)}%)`;

    return (
      <View style={[]}>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Total Traded" />
          <CardDetailText Name={`₹${formatNumberToInr(totalTradedAmount, true)}`} />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Net Profit" />
          <CardDetailText Name={TransactionsData} />
        </View>
      </View>
    );
  };

  //function for enabling popup
  handleBottomSheet = (type) => {
    if (type === "catagoryList") {
      let tempPortfoliolist = this.state.transactionsCatagoryList.length === 3 ? this.props.portfolio : [];
      let combineList = [...this.state.transactionsCatagoryList, ...tempPortfoliolist];
      this.setState(
        { transactionsCatagoryList: combineList, actionText: "Show Transactions from:" },
        () => {
          this.refs.watchlistActionPopUp.RBSheet.open();
        },
        () => {
          console.log(
            "transactionsCatagoryList",
            this.state.transactionsCatagoryList,
            "Combine list ->",
            combineList,
            "portfolio",
            tempPortfoliolist
          );
        }
      );
    } else {
      this.refs.watchlistActionPopUp.RBSheet.close();
      this.setState({ actionText: "Choose a financial period" }, () => {
        this.refs.watchlistActionPopUp.RBSheet.open();
      });
    }
  };

  //handle close button  click.
  handleCloseButton() {
    this.refs.watchlistActionPopUp.RBSheet.close();
  }

  //handleSearch text
  handleSearchText = (value) => {
    const { userState, baseURL, appToken } = this.props;
    this.setState({ searchText: value, noTransactions: false, searchResultsReady: false }, () => {
      if (value.length > 2) {
        searchStockByTerm(baseURL, appToken, userState.token, value, (StockList) =>
          this.setState({ keywordNamedList: StockList, searchResultsReady: true })
        );
      } else {
        this.setState({ keywordNamedList: [], searchResultsReady: false });
      }
    });
  };

  //handle Search box clear click
  handleOnClear = () => {
    const { searchText } = this.state;
    if (searchText !== "") {
      this.setState({ searchText: "", keywordNamedList: [] }, () => {
        this.setState({ enableSearchBox: !this.state.enableSearchBox, searchResultsReady: false }, () => {
          this.onRefresh();
        });
      });
    } else {
      this.setState({ enableSearchBox: !this.state.enableSearchBox, searchResultsReady: false }, () => {
        this.onRefresh();
      });
    }
  };

  //Header of list  View
  renderListHeader = () => {
    const { enableSearchBox, stocks, searchText } = this.state;
    if (enableSearchBox) {
      return null;
    } else {
      return (
        <View>
          <View style={styles.filterRowMainView}>
            <SortingTagWithIcon
              TagText={this.state.selectedCatagory[1]}
              onPress={() => this.handleBottomSheet("catagoryList")}
            />
            <SortingTagWithIcon
              TagText={this.state.selectedTimePeriod.range}
              onPress={() => this.handleBottomSheet("timeQuater")}
            />
          </View>
          {!enableSearchBox && stocks.length > 0 ? (
            <View style={{ backgroundColor: colors.whiteTwo, paddingHorizontal: wp(3), paddingVertical: wp(3) }}>
              <GradientCardForDetail child={this.renderGradientCardChilds()} />
            </View>
          ) : null}
          {this.renderSortingChips()}
        </View>
      );
    }
  };

  //lfunctions for oading more data
  onLoadMore = () => {
    const { selectedCatagory, selectedTimePeriod } = this.state;
    // console.log("^^^^^^^^^^^^^-->", selectedCatagory, selectedTimePeriod)
    this.setState({ isLoadingMore: true }, () => {
      if (selectedCatagory[1] !== "All Portfolios") {
        this.getAllTransactionsData("TransactionsType", false, {});
      } else if (selectedTimePeriod.range != "Year till date") {
        this.getAllTransactionsData("TimePeriod", false, {});
      } else {
        this.getAllTransactionsData("", false, {});
      }
    });
  };

  //render list footer
  renderFooter = () => {
    const { isNextPage, isLoadingMore } = this.state;
    if (isLoadingMore) return <LoadingMoreView />;
    else if (isNextPage) {
      return (
        <TouchableOpacity onPress={this.onLoadMore} style={{ marginBottom: wp(5) }}>
          <View style={styles.footerButton}>
            <Text style={styles.footerButtonText}>Load More</Text>
          </View>
        </TouchableOpacity>
      );
    } else {
      return (
        <View style={styles.listEndFooter}>
          <Text style={styles.listEndFooterText}>That's all folks!</Text>
        </View>
      );
    }
  };

  //list View for transactions.
  renderStockList = () => {
    const { stocks, isLoading } = this.state;

    return (
      <FlatList
        ref={(ref) => (this.listRef = ref)}
        data={stocks}
        extraData={{ ...this.props, stocks, isLoading }}
        showsVerticalScrollIndicator={false}
        contentContainerStyle={styles.list}
        renderItem={this.renderStockItem}
        keyExtractor={(item, index) => index.toString()}
        ListHeaderComponent={this.renderListHeader}
        ListFooterComponent={this.renderFooter}
        refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
        onEndReached={this.onEndReached}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={3}
        maxToRenderPerBatch={5}
        onScroll={this.handleScroll}
        keyboardShouldPersistTaps="always"
      />
    );
  };

  //function on search result
  clickResult = (result) => {
    this.setState({ stockId: result.stock_id, searchResultsReady: false, searchText: "" }, () => {
      this.getAllTransactionsData("searchByStock", false);
    });
  };

  renderLayouts = () => {
    const { userState, watchlist, activeWatchlist, setActiveWatchlist, portfolio } = this.props;
    const { isGuestUser } = userState;
    const {
      isFirstCallComplete,
      enableSearchBox,
      noTransactions,
      stocks,
      keywordNamedList,
      searchResultsReady,
      isLoading,
      freshUser,
    } = this.state;
    // console.log(
    //   "isFirstCallComplete",
    //   isFirstCallComplete,
    //   "No transactions state-->",
    //   noTransactions,
    //   "searchResultsReady",
    //   searchResultsReady,
    //   "stocks.length",
    //   stocks.length
    // );

    if (isGuestUser || (isFirstCallComplete && freshUser && stocks.length === 0)) {
      // console.log("Case 1. Not yet View-->")
      return this.renderNoTransactionsYetView();
    } else if (searchResultsReady && !noTransactions) {
      // console.log("Case 2 --> Search stock results -->")
      return (
        <SearchStockResultsComponent
          stocks={keywordNamedList}
          searchedResultsReady={searchResultsReady}
          clickResult={this.clickResult}
        />
      );
    } else if ((isFirstCallComplete && stocks.length == 0 && searchResultsReady) || noTransactions) {
      console.log("Case 3 --> No results found. -->");
      return <NoMatchView message="No Results found" />;
      // return <View><Text>No Result VIwew</Text></View>;
    } else if (isLoading) {
      // console.log("Case 4 --> loading View. -->")
      return (
        <View style={[styles.emptyViewContainer]}>
          <ActivityIndicator color={colors.primary_color} size={"large"} />
        </View>
      );
    } else {
      // console.log("Case 5 --> Stock    View. -->")
      return this.renderStockList();
    }
  };

  //on click search icon function
  onPressSearch = () => {
    this.setState({ enableSearchBox: !this.state.enableSearchBox });
  };

  //tab Title View
  renderTabTitle = () => {
    const { token, isGuestUser } = this.props.userState;
    const { freshUser } = this.state;
    if (!isGuestUser && !freshUser) {
      return (
        <View style={[styles.addingItemContainer, {}]}>
          <TagNameWithIcon disableIcon TagName="Search stocks" />
          <TouchableOpacity onPress={() => (token ? this.onPressSearch() : null)}>
            <Image source={searchIcon} resizeMode="contain" />
          </TouchableOpacity>
        </View>
      );
    }
  };

  //render filters in no stock case
  renderNoStockCaseFilters = () => {
    return (
      <View style={[styles.NoStockCaseFiltersView]}>
        <SortingTagWithIcon
          TagText={this.state.selectedCatagory[1]}
          onPress={() => this.handleBottomSheet("catagoryList")}
        />
        <SortingTagWithIcon
          TagText={this.state.selectedTimePeriod.range}
          onPress={() => this.handleBottomSheet("timeQuater")}
        />
      </View>
    );
  };

  //search box
  enableSearchBox = () => {
    return (
      <View style={{ marginVertical: wp(4) }}>
        <SearchBoxwithCloseButton
          onChangeText={(text) => this.handleSearchText(text)}
          onClearPress={() => this.handleOnClear()}
          inputRef={(input) => (this.search = input)}
          value={this.state.searchText}
        />
      </View>
    );
  };

  //function for adjusting height of popup
  getPopupHeight = () => {
    let height = hp(37);
    const { actionText, transactionsCatagoryList } = this.state;
    if (actionText === "Choose a financial period") {
      height = hp(70);
    } else if (actionText === "Show Transactions from:") {
      height = transactionsCatagoryList.length * hp(7) < hp(70) ? transactionsCatagoryList.length * hp(7) : hp(70);
    } else if (actionText === "Edit Transaction:") {
      height = hp(57);
    }
    return height;
  };

  //handlecatory filter click
  handleItemPress = (item, ind) => {
    let tempArr = [];
    tempArr = item;
    if (item[2] && item[2] === "type") {
      this.setState({ selectedCatagory: tempArr }, () =>
        this.getAllTransactionsData("TransactionsType", true, tempArr)
      );
    } else {
      this.setState({ selectedPortfolioId: tempArr, selectedCatagory: tempArr }, () =>
        this.getAllTransactionsData("TransactionsType", true, tempArr)
      );
    }
  };

  //Adding Divider to the list
  dividerLine = (currentItem) => {
    const { portfolio } = this.props;
    let dividerObj = { color: colors.white, width: 0 };
    if (currentItem[1] === "All MFs" && portfolio.length > 0) {
      dividerObj = { color: colors.greyish, width: 1 };
    }
    return dividerObj;
  };

  //functions to load list of category filters
  rendercatagoryList = (item, index) => {
    return (
      <TouchableOpacity
        style={{
          flex: 1,
          paddingVertical: wp(2.5),
          borderBottomColor: this.dividerLine(item).color,
          borderBottomWidth: this.dividerLine(item).width,
        }}
        onPress={() => this.handleItemPress(item)}
      >
        <Text style={styles.action}>{item[1]}</Text>
      </TouchableOpacity>
    );
  };

  // undo sell/delete trasaction  function
  handleDelete = (command) => {
    this.setState({
      isLoading: true,
    });
    let requestBody = {
      transactionId: this.state.transactionId,
    };
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const URL = baseURL + DELETE_TRANSACTIONS_URL;
    // console.log("Requesting action --->", URL, requestBody)
    if (command === "yes") {
      fetcher(
        URL,
        POST_METHOD,
        requestBody,
        baseRef,
        appToken,
        token,
        (json) => {
          const { body, head } = json;
          // console.log("Response -->", json)
          setTimeout(() => this.refs.toast.show("transaction has deleted."), 200);
          this.updatingItemFromList(this.state.transactionId, "remove");
          this.refs.watchlistActionPopUp.RBSheet.close();
        },
        (message) => {
          this.setState({ isLoading: false });
          console.log("Message -->", message);
          if (message) this.refs.toast.show(message);
          else this.refs.toast.show("Please try again");
        }
      );
    } else {
      this.setState({ actionText: "Canceled..", stockName: "", isLoading: false }, () =>
        this.refs.watchlistActionPopUp.RBSheet.close()
      );
    }
  };

  //updating transactions after cruds
  updatingItemFromList = (transaction_id, type) => {
    const { stocks, transactionData } = this.state;
    let compInd;
    let itemToUpdate = {};
    let tmpArray = [];
    tmpArray = stocks;
    if (type === "remove") {
      compInd = tmpArray.findIndex((ele) => ele.obj["id"] === transaction_id);
      this.setState({ isLoading: true }, () => {
        tmpArray.splice(compInd, 1);
        // console.log("UPdated length -->", tmpArray.length)
        this.setState({ stocks: tmpArray, isLoading: false }, () => {
          this.getAllTransactionsData("", true, {});
        });
      });
    } else if (type === "update") {
      let temp = {};
      this.setState({ isLoading: true }, () => {
        compInd = tmpArray.findIndex((ele) => ele.obj["id"] === transactionData.transactionId);
        itemToUpdate = tmpArray[compInd];
        // console.log("transactionData  -->", transaction_id, ",to update from Data in state  of length ", tmpArray.length, 'Item -->', itemToUpdate);
        // itemToUpdate = this.setDataItem(itemToUpdate,transaction_id
        itemToUpdate.obj.open_date = transaction_id.transData.buyDate;
        temp = itemToUpdate.array[itemToUpdate.array.findIndex((ele) => ele.unique_name === "open_date")];
        temp.value = transaction_id.transData.buyDate;

        itemToUpdate.obj.close_date = transaction_id.transData.sellDate;
        temp = itemToUpdate.array[itemToUpdate.array.findIndex((ele) => ele.unique_name === "close_date")];
        temp.value = transaction_id.transData.sellDate;

        itemToUpdate.obj.openprice = transaction_id.transData.buyPrice;
        temp = itemToUpdate.array[itemToUpdate.array.findIndex((ele) => ele.unique_name === "openprice")];
        temp.value = transaction_id.transData.buyPrice;

        itemToUpdate.obj.closeprice = transaction_id.transData.sellPrice;
        temp = itemToUpdate.array[itemToUpdate.array.findIndex((ele) => ele.unique_name === "closeprice")];
        temp.value = transaction_id.transData.sellPrice;

        itemToUpdate.obj.qty = transaction_id.transData.qty;
        temp = itemToUpdate.array[itemToUpdate.array.findIndex((ele) => ele.unique_name === "qty")];
        temp.value = transaction_id.transData.qty;

        // console.log("Item to update -->", itemToUpdate);
        tmpArray[compInd] = itemToUpdate;
        this.setState({ stocks: tmpArray, isLoading: false }, () => {
          console.log("Updated stocks -->", stocks);
        });
      });
    }
  };

  //function for rendering date field.
  renderDateField = (fieldName) => {
    const { inputFocused, buyDate, buyDateStatus, sellDate, sellDateStatus } = this.state;
    const inputBoxStyle =
      fieldName === "Buy Date"
        ? inputFocused == "buyDate"
          ? [styles.inputContainer2, styles.activeTextBox]
          : [styles.inputContainer2]
        : inputFocused == "sellDate"
        ? [styles.inputContainer2, styles.activeTextBox]
        : [styles.inputContainer2];
    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              fieldName === "Buy Date"
                ? inputFocused == "buyDate"
                  ? colors.primary_color
                  : buyDateStatus.error === ""
                  ? colors.greyishBrown
                  : colors.red
                : inputFocused == "sellDate"
                ? colors.primary_color
                : sellDateStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          {fieldName}
          <Text style={{ color: "red" }}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={() => this.showDatePicker(fieldName)}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.greyishBrown }]}>
            {fieldName === "Buy Date" ? buyDate.formetted : sellDate.formetted}
          </Text>
          <Image
            source={require("../../assets/icons/date-picker-blue.png")}
            style={styles.inputIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  //function for showing datepicker
  showDatePicker = (type) => {
    this.setState({ dateType: type }, () => {
      this.toggleDatePicker();
    });
  };

  //render error message function
  renderErrorText = () => {
    const { errorText } = this.state;
    if (errorText != "")
      return (
        <View style={{ marginVertical: wp(2) }}>
          <Text style={[{ color: colors.red, fontSize: RF(1.3) }]}>{`* ${errorText}`}</Text>
        </View>
      );
  };

  //function for validation
  checkBtnStatus = () => {
    const {
      buyDateStatus,
      buyPriceStatus,
      sellPriceStatus,
      sellDateStatus,
      sellShareStatus,
      activeSubmitBtn,
    } = this.state;
    if (
      this.checkForm(buyDateStatus) &&
      this.checkForm(buyPriceStatus) &&
      this.checkForm(sellDateStatus) &&
      this.checkForm(sellShareStatus) &&
      this.checkForm(sellPriceStatus) &&
      activeSubmitBtn
    ) {
      return { color: colors.primary_color, status: true };
    } else {
      return { color: colors.greyishBrown, status: false };
    }
  };

  //form validation
  checkForm = (entity) => {
    let flag = false;
    if (entity.status) flag = true;
    else {
      this.setState({ errorText: entity.error });
    }
    return flag;
  };

  //handle Edit form submit
  handleSubmit = () => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const { buyDate, transactionData, buyPrice, sellShare, sellPrice, sellDate, portfolioData } = this.state;
    const URL = baseURL + EDIT_TRANSACTION_URL;

    this.setState({ isLoading: true });
    let requestBody = {
      portfolioId: portfolioData[1] ? portfolioData[1] : transactionData.portfolioId,
      stock_id: transactionData.stockId,
      openDate: this.convertDateObject("API_Formet", buyDate),
      buyPrice: buyPrice,
      sellPrice: sellPrice,
      closeDate: this.convertDateObject("API_Formet", sellDate),
      qty: sellShare,
      transactionId: transactionData.transactionId,
      notes: "source : mobile App",
    };
    console.log("requestBody -->", requestBody);
    fetcher(
      URL,
      POST_METHOD,
      requestBody,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body, head } = json;
        this.refs.toast.show(body.msg, 300);
        console.log("Edit Response Transactions API ---->", json);
        this.updatingItemFromList(body, "update");
        this.refs.watchlistActionPopUp.RBSheet.close();
      },
      (message) => {
        this.setState(
          {
            isLoading: false,
          },
          () =>
            setTimeout(() => {
              this.refs.toast.show(message, 200);
            }, 200)
        );
        console.log("Edit Transaction API RESPONSE -->", message);
      }
    );
    {
      /* }); */
    }
  };

  //handle edit form validations
  handleValidation = () => {
    let txt;
    const { buyDate, buyPrice, sellShare, sellDate, sellPrice } = this.state;
    if (buyDate.formetted === "Select Date") {
      txt = "Please select buy date.";
      this.setState({ errorText: txt, buyDateStatus: { status: false, error: txt } });
    } else if (buyPrice == "") {
      txt = "Please enter buy price.";
      this.setState({ errorText: txt, buyPriceStatus: { status: false, error: txt } });
    } else if (sellDate.formetted === "Select Date") {
      txt = "Please select sell date.";
      this.setState({ errorText: txt, sellDateStatus: { status: false, error: txt } });
    } else if (sellShare == "") {
      txt = "Please enter sell share.";
      this.setState({ errorText: txt, sellShareStatus: { status: false, error: txt } });
    } else if (sellPrice === "") {
      txt = "Please enter sell price.";
      this.setState({ errorText: txt, sellPriceStatus: { status: false, error: txt } }, () => {
        this.setState({ errorText: txt });
      });
    }
  };

  //onchangefield function of edit form
  onChangeTextField = (text, type) => {
    let txt;
    if (type === "buyPrice") {
      txt = "Plaese enter buy price.";
      if (text === "") {
        this.setState({ buyPrice: text, buyPriceStatus: { status: false, error: txt }, errorText: txt });
      } else {
        this.setState({
          buyPrice: text,
          buyPriceStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        });
      }
    } else if (type === "totalShare") {
      txt = "Plaese enter total shares.";
      if (text === "") {
        this.setState({ totalShare: text, totalShareStatus: { status: false, error: txt }, errorText: txt });
      } else {
        this.setState({
          totalShare: text,
          totalShareStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        });
      }
    } else if (type === "sellShare") {
      txt = "Plaese enter sell shares.";
      if (text === "") {
        this.setState({ sellShare: text, sellShareStatus: { status: false, error: txt }, errorText: txt });
      } else {
        this.setState({
          sellShare: text,
          sellShareStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        });
      }
    } else {
      txt = "Plaese enter sell price.";
      if (text === "") {
        this.setState({ sellPrice: text, sellPriceStatus: { status: false, error: txt }, errorText: txt });
      } else {
        this.setState({
          sellPrice: text,
          sellPriceStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        });
      }
    }
  };

  //toggle datepicker
  toggleDatePicker = () => {
    const { inputFocused, dateType } = this.state;
    const focusedInput = inputFocused.toLowerCase().includes("date")
      ? ""
      : dateType === "Buy Date"
      ? "buyDate"
      : "sellDate";
    this.setState({ isDatePickerVisible: !this.state.isDatePickerVisible, inputFocused: focusedInput });
  };

  //handle date picked
  handleDatePicked = (date) => {
    this.toggleDatePicker();
    // let day = date.getDate();
    let day = date.getDate() > 10 ? date.getDate() : `0${date.getDate()}`;
    let month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    // let month = MONTHS_FULL[date.getMonth()];
    let year = date.getFullYear();
    // let formattedDate = getOrdinalNumForDate(day) + " " + month + ", " + year;
    let formattedDate = `${day}-${month}-${year}`;
    if (this.state.dateType === "Buy Date") {
      this.setState(
        {
          buyDate: { formetted: formattedDate, dateObj: date },
          buyDateStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        },
        () => {
          this.setState({ errorText: "" });
        }
      );
    } else {
      this.setState(
        {
          sellDate: { formetted: formattedDate, dateObj: date },
          sellDateStatus: { status: true, error: "" },
          errorText: "",
          activeSubmitBtn: true,
        },
        () => {
          this.setState({ errorText: "" });
        }
      );
    }
  };

  //function to render content inside popup
  renderActionChilds = () => {
    const { actionText, transactionsCatagoryList } = this.state;
    if (actionText === "Show Transactions from:") {
      return (
        <FlatList
          data={transactionsCatagoryList}
          renderItem={({ item, index }) => this.rendercatagoryList(item, index)}
          keyboardShouldPersistTaps="always"
        />
      );
    } else if (actionText === "Delete Transaction:") {
      const deleteText = `Are you sure about deleting this transaction ? This action can not be reversed.`;
      return (
        <View style={{ justifyContent: "space-between" }}>
          <View>
            <Text style={[{ fontSize: RF(2.2), color: colors.black }]}>{deleteText}</Text>
          </View>
          <View style={{ marginVertical: wp(6) }}></View>
          <View style={[styles.deleteWatchlistButtonContainer]}>
            <PopUpbutton
              buttonName={"Yes, delete"}
              buttonNameText={{ color: colors.white }}
              onPress={() => this.handleDelete("yes")}
            />
            <PopUpbutton
              buttonMainView={{ backgroundColor: colors.white, borderColor: colors.primary_color }}
              buttonName={"No, keep it"}
              onPress={() => this.handleDelete("no")}
            />
          </View>
        </View>
      );
    } else if (actionText === "Edit Transaction:") {
      const { inputFocused, buyPrice, sellShare, sellPrice } = this.state;
      return (
        <View style={{ flex: 1, paddingBottom: wp(2) }}>
          <View style={{ flex: 2, flexDirection: "row", marginVertical: wp(0.5) }}>
            <View style={{ flex: 2 }}>{this.renderDateField("Buy Date")}</View>
            <View style={{ marginHorizontal: wp(0.5) }}></View>
            <View style={{ flex: 2 }}>
              <InputField
                tagName={"Buy Price"}
                numberOfLines={1}
                inputContainer={{
                  borderBottomColor:
                    inputFocused === "Buy Price"
                      ? this.state.buyPriceStatus.error
                        ? colors.red
                        : colors.primary_color
                      : this.state.buyPriceStatus.error != ""
                      ? colors.red
                      : colors.greyishBrown,
                }}
                style={[{ color: this.state.onFocusInput ? colors.primary_color : colors.black }]}
                placeholderTextColor={colors.steelGrey}
                onChangeText={(value) => this.onChangeTextField(value, "buyPrice")}
                value={buyPrice}
              />
            </View>
          </View>
          <View style={{ marginHorizontal: wp(1) }}></View>
          <View style={{ flex: 2, flexDirection: "row", marginVertical: wp(0.5) }}>
            <View style={{ flex: 2 }}>{this.renderDateField("Sell Date")}</View>
            <View style={{ marginHorizontal: wp(0.5) }}></View>
            <View style={{ flex: 2 }}>
              <InputField
                tagName={"Sell Price"}
                numberOfLines={1}
                inputContainer={{
                  borderBottomColor:
                    inputFocused === "Sell Price"
                      ? this.state.sellPriceStatus.error
                        ? colors.red
                        : colors.primary_color
                      : this.state.sellPriceStatus.error != ""
                      ? colors.red
                      : colors.greyishBrown,
                }}
                style={[{ color: this.state.onFocusInput ? colors.primary_color : colors.black }]}
                placeholderTextColor={colors.steelGrey}
                onChangeText={(value) => this.onChangeTextField(value, "sellPrice")}
                value={sellPrice}
              />
            </View>
          </View>
          <View style={{ flex: 2, flexDirection: "row", marginVertical: wp(0.5) }}>
            <View style={{ flex: 2 }}>
              <InputField
                tagName={"Sell Shares"}
                numberOfLines={1}
                inputContainer={{
                  borderBottomColor:
                    inputFocused === "Sell Shares"
                      ? this.state.sellShareStatus.error
                        ? colors.red
                        : colors.primary_color
                      : this.state.sellShareStatus.error != ""
                      ? colors.red
                      : colors.greyishBrown,
                }}
                style={[{ color: this.state.onFocusInput ? colors.primary_color : colors.black }]}
                placeholderTextColor={colors.steelGrey}
                onChangeText={(value) => this.onChangeTextField(value, "sellShare")}
                value={sellShare}
              />
            </View>
            <View style={{ marginHorizontal: wp(1) }}></View>
            <View style={{ flex: 2 }}></View>
          </View>
          <DateTimePicker
            // minimumDate={minimumSellDate}
            // maximumDate={maximumSellDate}
            isVisible={this.state.isDatePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.toggleDatePicker}
          />
          {this.renderErrorText()}
          <SubmitButton
            buttonName="Save Changes"
            // onPress={() => (this.checkBtnStatus().status ? this.handleSubmit() : this.handleValidation())}
            // btnBackground={{ backgroundColor: this.checkBtnStatus().color }}
            onPress={() => this.handleSubmit()}
            btnBackground={{ backgroundColor: colors.primary_color }}
          />
        </View>
      );
    } else if (actionText === "Canceled..") {
      return <View></View>;
    } else {
      const { selectedTimePeriod } = this.state;
      return (
        <View>
          <FlatList
            // data={getDateRange(8)}
            data={this.state.dateRangeStatic}
            renderItem={({ item, index }) => (
              <TouchableOpacity
                onPress={() =>
                  this.setState({ selectedTimePeriod: item }, () => {
                    console.log("Item -->", item);
                    this.getAllTransactionsData("TimePeriod", true, item);
                  })
                }
                style={[styles.TimePeriodMainView]}
              >
                <View style={[styles.RadioMainView]}>
                  <View
                    style={[
                      styles.radioButton,
                      { borderColor: selectedTimePeriod.range === item.range ? colors.primary_color : colors.black },
                    ]}
                  >
                    <View style={[selectedTimePeriod.range === item.range ? styles.radioButtonCircle : styles.hide]} />
                  </View>
                </View>
                <View style={[styles.RadioTextMainView]}>
                  <Text
                    style={[
                      styles.action,
                      {
                        color: selectedTimePeriod.range === item.range ? colors.primary_color : colors.black,
                        fontWeight: selectedTimePeriod.range === item.range ? "500" : "normal",
                      },
                    ]}
                  >
                    {item.range ? item.range : ""}
                  </Text>
                </View>
              </TouchableOpacity>
            )}
            keyboardShouldPersistTaps="always"
          />
        </View>
      );
    }
  };

  render() {
    const { userState, watchlist, activeWatchlist, setActiveWatchlist, portfolio, isSelected } = this.props;
    const { isGuestUser } = userState;
    const { enableSearchBox, stocks, showScrollToTop } = this.state;
    if (isSelected) {
      return (
        <View style={styles.container}>
          {enableSearchBox ? this.enableSearchBox() : this.renderTabTitle()}
          {this.props.userState.token && !stocks.length && !enableSearchBox ? this.renderNoStockCaseFilters() : null}
          {this.renderLayouts()}

          <Toast
            ref="toast"
            position="bottom"
            style={[globalStyles.toast, { bottom: hp(10) }]}
            textStyle={globalStyles.toastText}
          />

          <StockAddBottomSheet
            ref="watchlistActionPopUp"
            modalMainView={{ height: this.getPopupHeight() }}
            // stockName={""}
            actionText={this.state.actionText}
            closeIconPress={() => this.handleCloseButton()}
            children={this.renderActionChilds()}
          />

          <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        </View>
      );
    } else {
      return null;
    }
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.whiteTwo },
  list: { paddingBottom: hp(1) },
  filterRowMainView: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: wp(3),
    paddingVertical: wp(1.5),
  },
  sortingContainer: { alignItems: "center" },
  CardRowView: { justifyContent: "space-between", flexDirection: "row", paddingVertical: wp(1) },
  addingItemContainer: {
    paddingTop: wp(2.5),
    paddingVertical: wp(2.5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "center",
    justifyContent: "space-between",
    flexDirection: "row",
    paddingHorizontal: wp(3),
  },
  deleteWatchlistButtonContainer: { flexDirection: "row", justifyContent: "space-between" },
  action: { fontSize: RF(2.2), color: colors.primary_color },
  radioButtonCircle: { width: wp(2.4), height: wp(2.4), borderRadius: wp(1.2), backgroundColor: colors.primary_color },
  TimePeriodMainView: { flexDirection: "row", justifyContent: "flex-start", paddingVertical: wp(2) },
  radioButton: {
    width: wp(4),
    height: wp(4),
    borderWidth: 0.8,
    borderColor: colors.black,
    borderRadius: 4,
    alignItems: "center",
    justifyContent: "center",
  },
  radioButtonActive: { borderColor: colors.primary_color },
  RadioMainView: { flex: 1, alignItems: "center", justifyContent: "center" },
  RadioTextMainView: { flex: 10, alignItems: "flex-start", justifyContent: "center" },
  hide: { display: "none" },
  footerButton: {
    flex: 1,
    borderWidth: 1,
    borderColor: colors.primary_color,
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    marginTop: hp(1),
    marginHorizontal: "3%",
  },
  footerButtonText: { fontSize: RF(2.2), fontWeight: "500", padding: wp(3), color: colors.primary_color },
  listEndFooter: { flex: 1, alignItems: "center", justifyContent: "center", marginTop: hp(1) },
  listEndFooterText: { fontSize: RF(2.2), fontWeight: "500", padding: "3%", color: colors.pinkishGrey },
  emptyViewContainer: { paddingVertical: wp(45), alignItems: "center" },
  inputContainer2: { paddingBottom: wp(1.55), borderBottomWidth: 1, borderBottomColor: colors.steelGrey },
  activeTextBox: { borderBottomWidth: 2, borderBottomColor: colors.primary_color },
  inputLabel: { fontSize: RF(1.9), color: colors.black },
  inputRow: { flexDirection: "row", paddingTop: wp(5), paddingBottom: wp(0.8) },
  NoStockCaseFiltersView: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingHorizontal: wp(3),
    paddingVertical: wp(1.5),
  },
});

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;
  console.log("States on transactions", state);
  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
  };
};

export default connect(
  mapStateToProps,
  {
    loadPortfolio,
  },
  null,
  { forwardRef: true }
)(TransactionsScreen);
