import React from "react";
import { View, StyleSheet, Text, FlatList } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { ListItem, TagNameWithIcon, CHIPS } from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";
import { fetcher } from "../../controllers/Fetcher";
import { PORTFOLIO_ANALYSER_URL } from "../../utils/ApiEndPoints";
import { mapHeadersData, formatNumberToInr, roundNumber } from "../../utils/Utils";
import { GET_METHOD, POST_METHOD } from "../../utils/Constants";
import LoadingView from "../../components/common/LoadingView";
import Toast from "react-native-easy-toast";
import { GradientCardForDetail, SortingTagWithIcon, CardDetailText } from "../../components/portfolio/transactions";
import {
  Divider,
  ColoredBar,
  BarChartWithIndicator,
  InsightComponent,
  CompareFactor,
} from "../../components/portfolio/summary";

import ChartView from "react-native-highcharts";
import { BAR_CHART_CONFIG, LINE_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";

const negativeImg = require("../../assets/icons/arrow-red.png");
const positiveImg = require("../../assets/icons/arrow-green.png");
const neutralImg = require("../../assets/icons/arrow-yellow.png");
const stockStatisticsOrder = [
  "profitGrowth",
  "priceToEarnings",
  "priceToBook",
  "dividendYield",
  "priceToEarningsGrowth",
];

class SummaryTab2 extends React.PureComponent {
  gainersListRef = null;
  earningsListRef = null;
  statisticsListRef = null;
  barChartConfig = {};

  state = {
    isLoading: false,
    isRefreshing: false,
    tabs: [{ name: "Summary" }, { name: "Stock Analysis" }, { name: "MF Analysis" }],
    selectedFilter: "Summary",
    allStAnalysisComponents: [
      { name: "STOCK ALLOCATION (IN ₹)", img: require("../../assets/icons/portfolio/stocksIcon.png") },
      { name: "TOP GAINERS AND LOSERS", img: require("../../assets/icons/portfolio/gainerLosersIcon.png") },
      { name: "TOP EARNINGS AND VALUATIONS", img: require("../../assets/icons/portfolio/earningsValuationIcon.png") },
      { name: "STOCK STATISTICS", img: require("../../assets/icons/portfolio/stockStatsIcon.png") },
    ],
    myData: {},
    stocksData: [],
    gainerLoserData: [],
    growth: [],
    valuations: [],
    statistics: [],
    yValueSuffix: "₹",
    indicatorPosition: 0,
    selectedIndicator: {},
  };

  // componentWillUnmount() {
  //     this.navListener.remove();
  // }

  // setNavigationListener = () => {
  //     this.navListener = this.props.navigation.addListener("willFocus", () => {
  //         // this.getWatchlists()
  //     });
  // };

  // getWatchlists = () => {
  //   const { baseRef, userState, baseURL } = this.props;
  //   const { token } = userState;
  //   const URL = baseURL + GET_ALL_WATCHLIST_URL;
  //   this.setState({ isLoading: true });
  //   // console.log("Getting Watchlist Service Initiated 3 2 1....GO", "URL ==>", token);

  //   fetcher(
  //     URL,
  //     GET_METHOD,
  //     null,
  //     baseRef,
  //     token,
  //     json => {
  //       const { body } = json;
  //       this.setState({ listItems: body.watchlists ? body.watchlists : [], isLoading: false });
  //       // this.setState(data);
  //       // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
  //       // baseRef && baseRef.stopLoading();
  //     },
  //     message => {
  //       this.setState({ isLoading: false, listItems: [] });
  //       console.log("Message -->", message);
  //       //handle error here
  //       // baseRef && baseRef.stopLoading();
  //       // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
  //     }
  //   );
  // };

  componentDidMount() {
    // this.setNavigationListener();
    // const { navigation } = this.props;
    // const tabIndex = navigation.getParam("tabIndex", 0);
    // console.log("tabIndex", tabIndex);
    // console.log("Logged User Data ===>", this.props.userState, "USRL")
    // this.getWatchlists()
    console.log("props on Sub Tabs -->,", this.props);
    // this.refs.ActionPopUp.RBSheet.open();
    if (this.props.currentTab) {
      // console.log('true Case Tab 2-->')
      this.getAnalysisData();
    } else {
      // console.log('False Case Tab 2-->')
    }
  }

  getAnalysisData = (type) => {
    let REQUEST_DATA;
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const URL = baseURL + PORTFOLIO_ANALYSER_URL;

    this.setState({ isLoading: true });

    REQUEST_DATA = { portfolioType: "stocks" };
    fetcher(
      URL,
      POST_METHOD,
      REQUEST_DATA,
      baseRef,
      appToken,
      token,
      (json) => {
        const { body } = json;
        let tempArray = [];
        let earniingValuationArr = [];
        // console.log("Response Summary Data -->", json);
        const { maxValueStocks, gainersLosers, growth, valuation } = json.body.stockAnalysis;
        tempArray.push({ data: gainersLosers.day, periodName: "day" });
        tempArray.push({ data: gainersLosers.week, periodName: "week" });
        tempArray.push({ data: gainersLosers.month, periodName: "month" });

        earniingValuationArr.push({ data: valuation, periodName: "valuation" });
        earniingValuationArr.push({ data: growth, periodName: "growth" });

        this.setState({
          stocksData: maxValueStocks ? maxValueStocks : [],
          gainerLoserData: tempArray,
          growth: growth,
          valuation: valuation,
          earningsValuations: earniingValuationArr,
          // myData: body.stockAnalysis,
          // statistics: statistics ? statistics : {},
          isLoading: false,
        });

        let statistics = "statistics" in json.body.stockAnalysis ? json.body.stockAnalysis.statistics : {};
        let orderedStatistics = [];

        stockStatisticsOrder.map((key) => {
          if (key in statistics) {
            orderedStatistics.push(statistics[key]);
          }
        });

        if (Object.keys(statistics).length > stockStatisticsOrder.length) {
          Object.keys(statistics).map((key) => {
            if (stockStatisticsOrder.indexOf(key) == -1) orderedStatistics.push(statistics[key]);
          });
        }

        this.setState({ statistics: orderedStatistics });
        // this.setState(data);
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        // baseRef && baseRef.stopLoading();
      },
      (message) => {
        // this.setState({ isLoading: false });
        console.log("Message -->", message);
        // if(message === 'Login required'){
        //     baseRef.login
        // }
        //handle error here
        // baseRef && baseRef.stopLoading();
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  renderPageIndicator = (type, data) => {
    let selected = this.state.selectedIndicator[type] ? this.state.selectedIndicator[type] : 0;

    return (
      <View style={styles.pageIndicator}>
        {data.map((item, index) => {
          const indicatorStyle = selected == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
          return <View style={indicatorStyle} key={index} />;
        })}
      </View>
    );
  };

  // case "TOP GAINERS AND LOSERS":
  // case "TOP EARNINGS AND VALUATIONS":
  // case "STOCK STATISTICS":
  _onViewableChangeGainerLosers = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      let prevSelected = this.state.selectedIndicator["TOP GAINERS AND LOSERS"];
      let currentIndex = viewableItems[0].index;
      let oldState = { ...this.state.selectedIndicator };
      oldState["TOP GAINERS AND LOSERS"] = currentIndex;

      if (currentIndex != prevSelected)
        this.setState(
          { selectedIndicator: oldState },
          () =>
            this.gainersListRef &&
            this.gainersListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
        );
    }
  };

  _onViewableChangeEarningValuation = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      let prevSelected = this.state.selectedIndicator["TOP EARNINGS AND VALUATIONS"];
      let currentIndex = viewableItems[0].index;
      let oldState = { ...this.state.selectedIndicator };
      oldState["TOP EARNINGS AND VALUATIONS"] = currentIndex;

      if (currentIndex != prevSelected)
        this.setState(
          { selectedIndicator: oldState },
          () =>
            this.earningsListRef &&
            this.earningsListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
        );
    }
  };

  _onViewableChangeStockStats = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      let prevSelected = this.state.selectedIndicator["STOCK STATISTICS"];
      let currentIndex = viewableItems[0].index;
      let oldState = { ...this.state.selectedIndicator };
      oldState["STOCK STATISTICS"] = currentIndex;

      if (currentIndex != prevSelected)
        this.setState(
          { selectedIndicator: oldState },
          () =>
            this.statisticsListRef &&
            this.statisticsListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
        );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  handleSubTabs = () => {
    const { selectedFilter } = this.state;
    if (selectedFilter === "Summary") {
      return (
        <FlatList
          contentContainerStyle={[styles.SectionTwoListView]}
          data={this.setComponents()}
          showsHorizontalScrollIndicator={false}
          renderItem={({ item, index }) => this.renderSubComponents(item, index)}
          keyExtractor={(item, index) => index.toString()}
          // extraData={this.state.stocks || this.state.refreshing}
        />
      );
    } else if (selectedFilter === "Stock Analysis") {
      return (
        <View>
          <Text>csa</Text>
        </View>
      );
    } else {
      return (
        <View>
          <Text>cs23a</Text>
        </View>
      );
    }
  };

  setComponents = () => {
    const { allStAnalysisComponents } = this.state;
    return allStAnalysisComponents;
  };

  renderChilds = (parentData) => {
    return (
      <View>
        <View style={[{ alignItems: "center" }, {}]}>
          <CardDetailText Name="Current Value" CardText={{ fontWeight: "normal", fontSize: RF(2.2) }} />
          <CardDetailText
            Name={`₹${"Amount"}`}
            CardTextView={{ marginVertical: wp(2) }}
            CardText={{ fontWeight: "500", fontSize: RF(4.4) }}
            size="large"
            color={colors.white}
            // enableLoader={true}
            isLoading={true}
          />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Real XIRR%" />
          <CardDetailText
            Name={`₹${"Amount"}`}
            //   enableLoader={true}
            isLoading={true}
          />
        </View>
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Day Change" />
          <CardDetailText
            Name={`₹${"Amount"}`}
            //   enableLoader={true}
            isLoading={true}
          />
        </View>
        <Divider DividerMainView={{ marginVertical: wp(3) }} />
        <View style={[styles.CardRowView, {}]}>
          <CardDetailText CardText={{ fontWeight: "normal" }} Name="Unreleased Gain" />
          <CardDetailText Name={"TransactionsData"} />
        </View>
      </View>
    );
  };

  findSpecificElement = (anyArray) => {
    // let len = anyArray.length;
    let NIFTYObj = {};
    anyArray.forEach((ele, ind) => {
      if (ele.NSEcode === "NIFTY50") {
        NIFTYObj = { NIFTYEle: ele, NIFTYInd: ind };
      }
    });
    return NIFTYObj;
  };

  getBarChartConfigs = (data, type, index) => {
    let backgroundSeries = [];
    let seriesData = [];
    let labels = [];
    let dataLabels = [];

    if (!(type in this.barChartConfig)) this.barChartConfig[type] = {};

    if (index in this.barChartConfig[type]) return this.barChartConfig[type][index];
    else {
      let config = JSON.parse(JSON.stringify(BAR_CHART_CONFIG));

      if (type === "stocks_Allocation") {
        data.forEach((item, index) => {
          let category = "NSEcode" in item ? item.NSEcode : item.BSEcode;
          console.log("stocks_Allocation Stocks Data", item);
          let value = !isNaN(item.currentValue) ? formatNumberToInr(item.currentValue, true) : item.currentValue;

          labels.push(category);
          seriesData.push(item.currentValue);
          dataLabels.push(value);
        });

        config.xAxis[0].categories = labels;
        config.series[0].data = seriesData;
        config.series[0].color = colors.primary_color;
        config.xAxis[1].categories = dataLabels;
        config.xAxis[0].labels = {
          formatter: function () {
            return `<span style="color:#000;font-weight:500">${this.value}</span>`;
          },
        };
        config.xAxis[1].labels = {
          formatter: function () {
            return `<span style="color:#174078;font-size:12px;font-weight:500">${this.value}</span>`;
          },
        };
        config.yAxis.labels = {
          formatter: function () {
            // var value = this.value;
            var formattedNumber = this.value;
            var number = formattedNumber < 0 ? formattedNumber * -1 : formattedNumber;
            var label = "";

            if (number >= 1000 && number < 100000) {
              formattedNumber = number / 1000;
              label = "K";
            }
            if (number >= 100000 && number < 10000000) {
              formattedNumber = number / 100000;
              label = "L";
            } else if (number >= 10000000) {
              formattedNumber = number / 10000000;
              label = "Cr";
            }
            formattedNumber += ` ${label}`;
            return `<span style="color:#4d4d4d;font-size:12px;font-weight:500">${formattedNumber}</span>`;
          },
        };
      } else if (type === "gainerloser") {
        data.slice(0).forEach((item, index) => {
          let category = "NSEcode" in item ? item.NSEcode : item.BSEcode;
          let color = category == "NIFTY50" ? colors.lightNavy : item.changeP >= 0 ? colors.mediumGreen : colors.red;
          labels.push(category);
          seriesData.push({ y: item.changeP, color: color });
          dataLabels.push({ x: item.changeP, color: color });
        });
        config.xAxis[0].categories = labels;
        config.series[0].data = seriesData;
        config.series[0].color = colors.primary_color;
        config.xAxis[1].categories = dataLabels;
        config.xAxis[1].labels = {
          formatter: function () {
            var value = !isNaN(this.value.x) ? Math.round(this.value.x * 10) / 10 : this.value.x;
            return `<span style="color:${this.value.color};font-size:12px;font-weight:500">${value}%</span>`;
          },
        };
        config.tooltip.formatter = function () {
          return this.x + "<br/>" + this.y + "%";
        };
      } else {
        labels = ["Portfolio", "Nifty50"];
        seriesData = [
          { y: data.portfolio, color: colors.barbiePink },
          { y: data.nifty50, color: colors.lightNavy },
        ];
        dataLabels = [
          { x: data.portfolio, color: colors.barbiePink },
          { x: data.nifty50, color: colors.lightNavy },
        ];

        config.xAxis[0].categories = labels;
        config.series[0].data = seriesData;
        config.series[0].color = colors.primary_color;
        config.xAxis[1].categories = dataLabels;
        config.xAxis[1].labels = {
          formatter: function () {
            var value = !isNaN(this.value.x) ? Math.round(this.value.x * 10) / 10 : this.value.x;
            return `<span style="color:${this.value.color};font-size:12px;font-weight:500">${value}</span>`;
          },
        };
      }
      this.barChartConfig[type][index] = config;
      return config;
    }
  };

  renderBarCharts = (type) => {
    const { isLoading, stocksData } = this.state;
    if (type === "stocks_Allocation") {
      return (
        <BarChartWithIndicator
          // EnableLoader={true}
          LoadingProgress={isLoading}
          ChartData={stocksData}
          config={this.getBarChartConfigs(stocksData, type)}
          LoaderColor={colors.primary_color}
          ChartLoadingMainView={{ height: hp(40) }}
        />
      );
    }
    // else if(type === 'gainers_Losers'){
    //     return (
    //         <FlatList
    //             contentContainerStyle={[styles.SectionTwoListView]}
    //             data={[stocksData,stocksData,stocksData]}
    //             horizontal={true}

    //             // showsHorizontalScrollIndicator={false}
    //             renderItem={({ item, index }) => this.renderAllPeriodsData(item,index,'gainer_Loser')}
    //             keyExtractor={(item, index) => index.toString()}
    //            // extraData={this.state.stocks || this.state.refreshing}
    //         />
    //     );
    // }
  };

  renderStatistics = (chartData, index) => {
    const { isLoading } = this.state;
    // console.log("chartData", chartData);
    // const portfolioImg = require("../../assets/icons/portfolio/portfolioIcon.png");
    // const indexIcon = require("../../assets/icons/portfolio/indexIcon.png");
    let cardTitle = "title" in chartData ? chartData.title : "";
    let showInsights = "insights" in chartData ? Array.isArray(chartData.insights) : false;

    return (
      <View style={[styles.CardStyle, {}]}>
        <View style={{ paddingVertical: wp(1), flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontWeight: "500", color: colors.black, fontSize: RF(2.2), flex: 1 }}>{cardTitle}</Text>

          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <CompareFactor color={colors.barbiePink} name={"Portfolio"} containerStyle={{ paddingRight: wp(3) }} />
            <CompareFactor color={colors.lightNavy} name={"Nifty50"} />
          </View>
        </View>

        <View style={{ paddingVertical: wp(2) }}>
          <BarChartWithIndicator
            //   EnableLoader={true}
            LoadingProgress={isLoading}
            // CompareFactor={[]}
            ChartData={[{}]}
            config={this.getBarChartConfigs(chartData, "statistics", index)}
            LoaderColor={colors.primary_color}
            ChartLoadingMainView={{ height: hp(40) }}
          />
        </View>

        {showInsights && (
          <View>
            <View style={{ paddingTop: wp(1.5) }}>
              <Text style={{ fontWeight: "500", color: colors.black, fontSize: RF(2.2) }}>Insights</Text>
            </View>
            <FlatList
              contentContainerStyle={{ paddingBottom: wp(2) }}
              contentContainerStyle={[styles.SectionTwoListView]}
              data={chartData.insights}
              showsVerticalScrollIndicator={false}
              renderItem={({ item, index }) =>
                this.renderInsightData("gainer_Loser", chartData.insights.length, item, index)
              }
              keyExtractor={(item, index) => index.toString()}
              ItemSeparatorComponent={() => <View style={styles.divider} />}

              // extraData={this.state.stocks || this.state.refreshing}
            />
          </View>
        )}
      </View>
    );
  };

  renderAllPeriodsData = (ele, ind, type) => {
    const { isLoading } = this.state;

    return (
      <View style={[styles.CardStyle, {}]}>
        <View>
          <BarChartWithIndicator
            ChartLabel={ele.periodName}
            // EnableLoader={true}
            firstParameter={"Portfolio"}
            secondParameter={"Nifty"}
            LoadingProgress={isLoading}
            ChartData={ele.data.chartData}
            config={this.getBarChartConfigs(ele.data.chartData, "gainerloser", ind)}
            LoaderColor={colors.primary_color}
            ChartLoadingMainView={{ height: hp(40) }}
          />
        </View>
        <SortingTagWithIcon
          TagText={"All Stock Gainers and Losers"}
          // SortingTagMainView={{paddingHorizontal:wp(2)}}
          TagTextStyle={{ color: colors.cobalt, fontSize: RF(1.9) }}
          ImageStyle={{ height: 15, width: 15, transform: [{ rotate: "270deg" }] }}
          onPress={() => this.props.gotoHoldings("STOCKS")}
        />
        <View style={[{ paddingVertical: wp(4) }]}>
          <ColoredBar
            BarTextStyle={{ color: colors.black }}
            // BarTextStyle={{ color: colors.white }}
            Gainers={ele.data.frequencyDist.gainers ? ele.data.frequencyDist.gainers : 1}
            Losers={ele.data.frequencyDist.losers ? ele.data.frequencyDist.losers : 1}
          />
        </View>

        <View style={{ paddingTop: wp(1.5) }}>
          <Text style={{ fontWeight: "500", color: colors.black, fontSize: RF(2.2) }}>Insights</Text>
        </View>

        <FlatList
          contentContainerStyle={{ paddingBottom: wp(2) }}
          contentContainerStyle={[styles.SectionTwoListView]}
          data={ele.data.insights}
          showsVerticalScrollIndicator={false}
          renderItem={({ item, index }) =>
            this.renderInsightData("gainer_Loser", ele.data.insights.length, item, index)
          }
          keyExtractor={(item, index) => index.toString()}
          ItemSeparatorComponent={() => <View style={styles.divider} />}

          // extraData={this.state.stocks || this.state.refreshing}
        />
      </View>
    );
  };

  renderInsightData = (type, len, ele, ind) => {
    if (type === "gainer_Loser") {
      return (
        <View style={{ flexDirection: "column", marginVertical: wp(2) }}>
          <InsightComponent
            imgSrc={this.getImg(ele.color ? ele.color : "")}
            insightElementText={ele.lt ? ele.lt : ""}
            dividingActive={true}
            insightText={{ fontSize: RF(2.1), lineHeight: wp(5.6) }}
          />
        </View>
      );
    }
  };

  getImg = (insightType) => {
    insightType = insightType.toLowerCase();
    if (insightType === "positive") {
      return positiveImg;
    } else if (insightType === "negative") {
      return negativeImg;
    } else {
      return neutralImg;
    }
  };

  getValue = (type) => {
    if (type === "gainers_Losers") {
      return 3;
    } else {
      return 87;
    }
    // return 5
  };

  renderChart = (type) => {
    if (type) {
      return <View>{this.renderBarCharts(type)}</View>;
    } else {
      return (
        <View style={{ justifyContent: "center", alignItems: "center" }}>
          <Text>No data available.</Text>
        </View>
      );
    }
  };

  renderChartComponents = (extraData) => {
    const { addHolding } = this.props;

    if (extraData.name === "STOCK ALLOCATION (IN ₹)")
      return (
        <View style={[styles.mfCard, {}]}>
          <View>{this.renderChart("stocks_Allocation")}</View>
          <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: wp(2) }}>
            <CHIPS
              name={`+ New Holding`}
              onPress={addHolding}
              selected={true}
              chipsMainView={{ paddingHorizontal: wp(5), paddingVertical: wp(1.5) }}
              disableImage={true}
            />
            <SortingTagWithIcon
              TagText={"All Stock Holdings"}
              // SortingTagMainView={{paddingHorizontal:wp(2)}}
              TagTextStyle={{ color: colors.cobalt, fontSize: RF(1.9) }}
              ImageStyle={{ height: 15, width: 15, transform: [{ rotate: "270deg" }] }}
              onPress={() => this.props.gotoHoldings("STOCKS")}
            />
          </View>
        </View>
      );
    else if (extraData.name === "TOP GAINERS AND LOSERS")
      return (
        <FlatList
          ref={(ref) => (this.gainersListRef = ref)}
          contentContainerStyle={[styles.list]}
          data={this.state.gainerLoserData}
          extraData={this.state}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          renderItem={({ item, index }) => this.renderAllPeriodsData(item, index, "gainer_Loser")}
          keyExtractor={(item, index) => "gainer_Loser" + index}
          onViewableItemsChanged={this._onViewableChangeGainerLosers}
          viewabilityConfig={this._viewabilityConfig}
          removeClippedSubviews={false}
          // initialNumToRender={2}
          // maxToRenderPerBatch={20}
        />
      );
    else if (extraData.name === "TOP EARNINGS AND VALUATIONS")
      return (
        <FlatList
          ref={(ref) => (this.earningsListRef = ref)}
          contentContainerStyle={[styles.list]}
          data={this.state.earningsValuations}
          extraData={this.state}
          showsHorizontalScrollIndicator={false}
          horizontal={true}
          renderItem={({ item, index }) => this.renderAllPeriodsData(item, index, "gainer_Loser")}
          keyExtractor={(item, index) => "earning_valuation" + index}
          onViewableItemsChanged={this._onViewableChangeEarningValuation}
          viewabilityConfig={this._viewabilityConfig}
          removeClippedSubviews={false}
          // initialNumToRender={2}
          // maxToRenderPerBatch={20}
        />
      );
    else {
      const { statistics } = this.state;
      return (
        <View>
          <FlatList
            ref={(ref) => (this.statisticsListRef = ref)}
            contentContainerStyle={[styles.list]}
            data={statistics}
            extraData={this.state}
            showsHorizontalScrollIndicator={false}
            horizontal={true}
            renderItem={({ item, index }) => this.renderStatistics(item, index)}
            keyExtractor={(item, index) => "statistics" + index.toString()}
            onViewableItemsChanged={this._onViewableChangeStockStats}
            viewabilityConfig={this._viewabilityConfig}
            removeClippedSubviews={false}
            // initialNumToRender={2}
            // maxToRenderPerBatch={20}
          />
        </View>
      );
    }
  };

  getData = (type) => {
    switch (type) {
      case "TOP GAINERS AND LOSERS":
        return this.state.gainerLoserData;
      case "TOP EARNINGS AND VALUATIONS":
        return this.state.earningsValuations;
      case "STOCK STATISTICS":
        return this.state.statistics;
      default:
        return [];
    }
  };

  renderSubComponents = (parentData) => {
    let data = this.getData(parentData.name);
    let showIndicator = data && Array.isArray(data) && data.length > 1;

    return (
      <View>
        <View style={[styles.addingItemContainer, { flexDirection: "row", alignItems: "center" }]}>
          <TagNameWithIcon
            TagName={parentData.name ? parentData.name : ""}
            Icon={parentData.img ? parentData.img : null}
            headingContainer={{ flex: 1 }}
            headingText={{ flex: 1 }}
          />
          {showIndicator && this.renderPageIndicator(parentData.name, data)}
        </View>
        {this.renderChartComponents(parentData)}
      </View>
    );
  };

  render() {
    const { allStAnalysisComponents, isLoading, stocksData } = this.state;
    let showList = Array.isArray(stocksData) && stocksData.length > 0;

    return (
      <View style={styles.container}>
        {showList && (
          <FlatList
            contentContainerStyle={[styles.SectionTwoListView]}
            data={allStAnalysisComponents}
            extraData={{ ...this.state, ...this.props }}
            showsVerticalScrollIndicator={false}
            renderItem={({ item, index }) => this.renderSubComponents(item, index)}
            keyExtractor={(item, index) => item.name}
            removeClippedSubviews={false}
            // initialNumToRender={2}
            // maxToRenderPerBatch={20}
            // extraData={this.state.stocks || this.state.refreshing}
          />
        )}

        {isLoading && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(SummaryTab2);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: hp(70),
    backgroundColor: colors.whiteTwo,
    // backgroundColor:'cyan',
    alignItems: "flex-start",
    flexDirection: "column",
    // paddingHorizontal:wp(3)
  },
  SectionOne: {
    flex: 2,
    // backgroundColor: 'red'
  },
  SectionTwo: {
    flex: 8,
    // backgroundColor: 'green'
  },
  SectionTwoListView: {},
  SectionOneListView: {
    // backgroundColor:'yellow',
    height: wp(20),
    alignItems: "center",
  },
  addingItemContainer: {
    paddingTop: wp(5),
    paddingVertical: wp(2.5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "flex-start",
    // justifyContent: 'space-between',
    // flexDirection: 'row',
    paddingHorizontal: wp(3),
  },
  CardRowView: {
    justifyContent: "space-between",
    flexDirection: "row",
    paddingVertical: wp(2),
    // backgroundColor:'green',
  },
  CardStyle: {
    width: wp(93),
    marginRight: wp(2),
    marginTop: wp(2),
    marginBottom: wp(3),
    padding: wp(3),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  mfCard: {
    width: wp(100),
    marginTop: wp(2),
    marginBottom: wp(3),
    padding: wp(3),
    backgroundColor: colors.white,
    paddingBottom: wp(2),
  },
  chartCard: {},
  barChartLabel: {},
  list: {
    paddingHorizontal: wp(3),
  },
  divider: {
    height: 1,
    width: wp(93),
    marginHorizontal: -wp(3),
    backgroundColor: "rgba(179,179,179,0.2)",
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
});
