import React from "react";
import { View, StyleSheet, Text, FlatList, ScrollView } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";

import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { TagNameWithIcon, CHIPS } from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";

import { PORTFOLIO_ANALYSER_URL } from "../../utils/ApiEndPoints";

import { fetcher } from "../../controllers/Fetcher";
import { POST_METHOD } from "../../utils/Constants";

import Toast from "react-native-easy-toast";
import { SortingTagWithIcon } from "../../components/portfolio/transactions";
import {
  Divider,
  ColoredBar,
  BarChartWithIndicator,
  InsightComponent,
  CompareFactor,
} from "../../components/portfolio/summary";

import { BAR_CHART_CONFIG, LINE_CHART_CONFIG, CHART_OPTIONS } from "../../utils/ChartConfigs";
import { formatNumberToInr, roundNumber } from "../../utils/Utils";
import LoadingView from "../../components/common/LoadingView";

const negativeImg = require("../../assets/icons/arrow-red.png");
const positiveImg = require("../../assets/icons/arrow-green.png");
const neutralImg = require("../../assets/icons/arrow-yellow.png");

class SummaryTab3 extends React.PureComponent {
  gainersListRef = null;

  state = {
    isLoading: false,
    isRefreshing: false,
    tabs: [{ name: "Summary" }, { name: "Stock Analysis" }, { name: "MF Analysis" }],
    selectedFilter: "Summary",
    allSummaryComponents: [
      { name: "CONSOLIDATED", img: require("../../assets/icons/portfolio/consolidatedIcon.png") },
      { name: "STOCKS", img: require("../../assets/icons/portfolio/stocksIcon.png") },
      { name: "MUTUAL FUNDS", img: require("../../assets/icons/portfolio/mutualFundsIcon.png") },
    ],
    allStAnalysisComponents: [
      { name: "STOCK ALLOCATIONS" },
      { name: "TOP GAINERS AND LOSERS" },
      { name: "TOP EARNINGS AND VALUATIONS" },
      { name: "STOCK STATISTICS" },
    ],
    allMFAnalysisComponents: [
      { name: "MF ALLOCATIONS", img: require("../../assets/icons/portfolio/mutualFundsIcon.png") },
      { name: "TOP GAINERS AND LOSERS", img: require("../../assets/icons/portfolio/gainerLosersIcon.png") },
    ],
    mfAllocations: [],
    topGainersLosers: [],
    indicatorPosition: 0,
  };

  componentDidMount() {
    if (this.props.currentTab) this.getAnalysisData();
  }

  getAnalysisData = (type) => {
    const { baseRef, userState, baseURL, appToken } = this.props;
    const URL = baseURL + PORTFOLIO_ANALYSER_URL;
    const { token } = userState;

    this.setState({ isLoading: true });

    let REQUEST_DATA = { portfolioType: "mf" };

    fetcher(
      URL,
      POST_METHOD,
      REQUEST_DATA,
      baseRef,
      appToken,
      token,
      (json) => {
        let tempArray = [];
        const { gainersLosers, ratingSpread } = json.body.mfAnalysis;
        console.log("JSON==>", json);
        let objArr = Object.entries(ratingSpread);
        let tempAllocations = [];
        let unrated = {};

        objArr.forEach(([key, value]) => {
          key === "unrated"
            ? (unrated = { keyName: "unrated", keyValue: value, rating: key })
            : tempAllocations.push({ keyName: "rated", keyValue: value, rating: key });
        });

        if (tempAllocations[0].rating == 1) tempAllocations = [...tempAllocations.reverse(), unrated];
        else tempAllocations = [...tempAllocations, unrated];

        tempArray.push({ data: gainersLosers.day, periodName: "day" });
        tempArray.push({ data: gainersLosers.week, periodName: "week" });
        tempArray.push({ data: gainersLosers.month, periodName: "month" });

        console.log(tempArray, tempAllocations);
        this.setState({ topGainersLosers: tempArray, mfAllocations: tempAllocations });
        this.setState({ isLoading: false });
        // this.setState(data);
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
        // baseRef && baseRef.stopLoading();
      },
      (message) => {
        this.setState({ isLoading: false });
        console.log("Message -->", message);
        // if(message === 'Login required'){
        //     baseRef.login
        // }
        //handle error here
        // baseRef && baseRef.stopLoading();
        // if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      }
    );
  };

  renderPageIndicator = () => {
    const { indicatorPosition, topGainersLosers } = this.state;

    if (topGainersLosers.length > 1)
      return (
        <View style={styles.pageIndicator}>
          {topGainersLosers.map((item, index) => {
            const indicatorStyle =
              indicatorPosition == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
            return <View style={indicatorStyle} key={index} />;
          })}
        </View>
      );
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    if (viewableItems[0]) {
      let currentIndex = viewableItems[0].index;
      this.setState(
        { indicatorPosition: currentIndex },
        () =>
          this.gainersListRef && this.gainersListRef.scrollToIndex({ index: viewableItems[0].index, viewPosition: 0.5 })
      );
    }
  };

  _viewabilityConfig = {
    waitForInteraction: true,
    itemVisiblePercentThreshold: 75,
  };

  getBarChartConfigs = (data, type) => {
    // let backgroundSeries = [];
    let seriesData = [];
    let labels = [];
    let dataLabels = [];
    let config = JSON.parse(JSON.stringify(BAR_CHART_CONFIG));
    if (type === "mf_allocations") {
      // console.log("MF Data ->", data);

      data.slice(0).forEach((item, index) => {
        let value = !isNaN(item.keyValue.currentValue)
          ? formatNumberToInr(item.keyValue.currentValue, true)
          : item.keyValue.currentValue;

        labels.push(item.rating);
        seriesData.push({ y: item.keyValue.currentValue, mf: item.keyValue.numMF });
        dataLabels.push(value);
      });

      config.xAxis[0].categories = labels;
      config.series[0].data = seriesData;
      config.series[0].color = colors.primary_color;
      config.xAxis[0].labels.formatter = function () {
        return `<span style="color:#000;font-size:12px;font-weight:500">${
          this.value
        }<span style="color:#f7941e ;font-size:14px;font-weight:500">${
          this.value != "unrated" ? "★" : ""
        }</span></span>`;
      };
      config.plotOptions.series.dataLabels = { enabled: true, inside: true, overflow: "none", crop: false };
      // config.plotOptions.series[0] = {
      //   borderWidth: 2,
      //   borderColor: 'black'
      // }
      // config.plotOptions: {
      //   series: {
      //     borderWidth: 2,
      //       borderColor: 'black'
      //   }
      // },
      config.series[0].dataLabels = {
        align: "top",
        formatter: function () {
          return this.point.mf <= 0
            ? ""
            : `<span style="color:'black';font-size:12px;font-weight:500">${this.point.mf} MFs</span>`;
        },
        rotation: -90,
        // verticalAlign: "top",
        // style: { textOutline: "2px" }
      };

      config.xAxis[1].categories = dataLabels;
      config.xAxis[1].labels = {
        formatter: function () {
          return `<span style="color:rgb(36,83,161);font-size:12px;font-weight:500">${this.value}</span>`;
        },
      };
      config.yAxis.labels = {
        formatter: function () {
          // var value = this.value;
          var formattedNumber = this.value;
          var number = formattedNumber < 0 ? formattedNumber * -1 : formattedNumber;
          var label = "";

          if (number >= 1000 && number < 100000) {
            formattedNumber = number / 1000;
            label = "K";
          } else if (number >= 100000 && number < 10000000) {
            formattedNumber = number / 100000;
            label = "L";
          } else if (number >= 10000000) {
            formattedNumber = number / 10000000;
            label = "Cr";
          }
          formattedNumber += ` ${label}`;
          return `<span style="color:#4d4d4d;font-size:12px;font-weight:500">${formattedNumber}</span>`;
        },
      };
      config.tooltip.formatter = function () {
        return this.y;
      };
    } else {
      data.forEach((item, index) => {
        let category = "NSEcode" in item ? item.NSEcode : item.BSEcode;
        let color = category == "NIFTY50" ? colors.lightNavy : item.changeP >= 0 ? colors.mediumGreen : colors.red;

        labels.push(category);
        seriesData.push({ y: item.changeP, color: color });
        dataLabels.push({ x: item.changeP, color: color });
      });

      config.xAxis[0].categories = labels;
      config.series[0].data = seriesData;
      config.series[0].color = colors.primary_color;
      config.xAxis[1].categories = dataLabels;
      config.xAxis[1].labels = {
        formatter: function () {
          var value = !isNaN(this.value.x) ? Math.round(this.value.x * 10) / 10 : this.value.x;
          return `<span style="color:${this.value.color};font-size:12px;font-weight:500">${value}%</span>`;
        },
      };
      config.tooltip.formatter = function () {
        return this.x + "<br/>" + this.y + "%";
      };
    }
    return config;
  };
  renderBarCharts = (type, chrtData) => {
    const { isLoading, mfAllocations } = this.state;
    if (type === "mf_allocations") {
      return (
        <BarChartWithIndicator
          //   EnableLoader={true}
          LoadingProgress={isLoading}
          ChartData={mfAllocations}
          config={this.getBarChartConfigs(mfAllocations, type)}
          LoaderColor={colors.primary_color}
          ChartLoadingMainView={{ height: hp(40) }}
        />
      );
    } else {
      return (
        <BarChartWithIndicator
          EnableLoader={true}
          LoadingProgress={isLoading}
          ChartData={chrtData}
          config={this.getBarChartConfigs(chrtData, type)}
          LoaderColor={colors.primary_color}
          ChartLoadingMainView={{ height: hp(40) }}
        />
      );
    }
  };

  renderHeaderText = (CardId) => {
    if (CardId && CardId === "month") return `Month's Analysis (in %)`;
    else if (CardId && CardId === "day") return `Day's Analysis (in %)`;
    else return `Week's Analysis (in %)`;
  };

  renderMultipleCharts = ({ item, index }) => {
    const { chartData, frequencyDist, insights } = item.data;
    const portfolioImg = require("../../assets/icons/portfolio/portfolioIcon.png");
    const indexIcon = require("../../assets/icons/portfolio/indexIcon.png");

    return (
      <View style={styles.CardStyle}>
        <View style={{ paddingVertical: wp(1), flexDirection: "row", alignItems: "center" }}>
          <Text style={{ fontWeight: "500", color: colors.black, fontSize: RF(2.2), flex: 1 }}>
            {this.renderHeaderText(item.periodName)}
          </Text>

          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <CompareFactor img={portfolioImg} name={"Portfolio"} containerStyle={{ paddingRight: wp(3) }} />
            <CompareFactor img={indexIcon} name={"Nifty50"} />
          </View>
        </View>

        <View style={{ paddingVertical: wp(2) }}>{this.renderChart("top_gainersosers", chartData)}</View>

        <SortingTagWithIcon
          TagText={"View all MF Gainers and Losers"}
          TagTextStyle={{ color: colors.cobalt, fontSize: RF(1.9) }}
          ImageStyle={{ height: wp(3), width: wp(3), transform: [{ rotate: "270deg" }], marginTop: 2 }}
          onPress={() => this.props.gotoHoldings("MUTUAL FUNDS")}
          // onPress={() => this.handleBottomSheet('holdingsCatagory')}
        />

        <View style={[{ paddingVertical: wp(4) }]}>
          <ColoredBar
            BarTextStyle={{ color: colors.black }}
            Gainers={parseInt(frequencyDist.gainers)}
            Losers={parseInt(frequencyDist.losers)}
          />
        </View>

        <View>
          <View style={{ paddingVertical: wp(1.5) }}>
            <Text style={{ fontWeight: "500", color: colors.black, fontSize: RF(2.2) }}>Insights</Text>
          </View>

          {this.insightDataComponent(insights)}
        </View>
      </View>
    );
  };

  renderChart = (type, chartData) => {
    return <View>{this.renderBarCharts(type, chartData ? chartData : [])}</View>;
  };

  renderMFAllocation = () => {
    const { addHolding } = this.props;

    return (
      <View style={[styles.mfCard]}>
        <View>{this.renderChart("mf_allocations")}</View>
        <View style={{ flexDirection: "row", justifyContent: "space-between", marginBottom: wp(2) }}>
          <CHIPS
            name={`+ New Holding`}
            onPress={addHolding}
            selected={true}
            chipsMainView={{ paddingHorizontal: wp(5), paddingVertical: wp(1.5) }}
            disableImage={true}
          />
          <SortingTagWithIcon
            TagText={"All MF Holdings"}
            SortingTagMainView={{ justifyContent: "center" }}
            TagTextStyle={{ color: colors.cobalt, fontSize: RF(1.9) }}
            ImageStyle={{ height: 15, width: 15, transform: [{ rotate: "270deg" }] }}
            onPress={() => this.props.gotoHoldings("MUTUAL FUNDS")}
          />
        </View>
      </View>
    );
  };

  renderChartComponents = (extraData) => {
    console.log("this.state.topGainersLosers", this.state.topGainersLosers);
    if (extraData.name === "MF ALLOCATIONS") return this.renderMFAllocation();
    else
      return (
        <View>
          <FlatList
            ref={(ref) => {
              this.gainersListRef = ref;
            }}
            contentContainerStyle={styles.list}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            data={this.state.topGainersLosers}
            //   extraData={this.state.topGainersLosers || this.state.refreshing}
            renderItem={this.renderMultipleCharts}
            keyExtractor={(item, index) => index.toString()}
            onViewableItemsChanged={this._onViewableItemsChanged}
            viewabilityConfig={this._viewabilityConfig}
            removeClippedSubviews={false}
            // initialNumToRender={2}
            // maxToRenderPerBatch={20}
          />
        </View>
      );
  };

  insightDataComponent = (insightData, id) => {
    return (
      <FlatList
        data={insightData}
        showsHorizontalScrollIndicator={false}
        renderItem={(dat, indx) => this.renderInsightDataComponent(dat, indx, insightData.length)}
        keyExtractor={(dat, indx) => indx.toString()}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        // extraData={this.state.stocks || this.state.refreshing}
      />
    );
  };

  renderInsightDataComponent = (insightItem, ind, totalnum) => {
    ind = insightItem.index;
    const { color, lt } = insightItem.item;
    return (
      <View style={{ flexDirection: "column", marginVertical: wp(2) }}>
        <View style={{ flex: 2 }}>
          <InsightComponent
            imgSrc={this.getImg(color)}
            insightElementText={lt}
            dividingActive={false}
            insightText={{ fontSize: RF(2.1), lineHeight: wp(5.6) }}
          />
        </View>
      </View>
    );
  };
  getImg = (insightType) => {
    if (insightType.toLowerCase() === "positive") {
      return positiveImg;
    } else if (insightType.toLowerCase() === "negative") {
      return negativeImg;
    } else {
      return neutralImg;
    }
  };

  renderSubComponents = (parentData) => {
    console.log(parentData.name);
    let isMutualFund = typeof parentData.name == "string" && parentData.name.indexOf("MF") != -1;
    let tagName = isMutualFund ? "MF ALLOCATION (IN ₹)" : parentData.name;

    return (
      <View>
        <View style={[styles.addingItemContainer, { flexDirection: "row", alignItems: "center" }]}>
          <TagNameWithIcon
            TagName={tagName}
            Icon={parentData.img ? parentData.img : null}
            headingContainer={{ flex: 1 }}
            headingText={{ flex: 1 }}
          />
          {!isMutualFund && this.renderPageIndicator()}
        </View>
        {this.renderChartComponents(parentData)}
      </View>
    );
  };

  render() {
    const { allMFAnalysisComponents, isLoading, mfAllocations } = this.state;
    let showList = Array.isArray(mfAllocations) && mfAllocations.length > 0;

    return (
      <View style={styles.container}>
        {showList &&
          allMFAnalysisComponents.map((item, index) => {
            return this.renderSubComponents(item, index);
          })}

        {isLoading && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps)(SummaryTab3);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    minHeight: hp(70),
    backgroundColor: colors.whiteTwo,
    // alignItems: "flex-start"
    // flexDirection: "column"
  },
  addingItemContainer: {
    paddingTop: wp(5),
    paddingVertical: wp(2.5),
    width: wp("100%"),
    backgroundColor: colors.whiteTwo,
    alignItems: "flex-start",
    paddingHorizontal: wp(3),
  },
  CardRowView: {
    justifyContent: "space-between",
    flexDirection: "row",
    paddingVertical: wp(2),
  },
  CardStyle: {
    width: wp(93),
    marginRight: wp(2),
    marginTop: wp(2),
    marginBottom: wp(3),
    padding: wp(3),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: { width: 0, height: 1 },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  mfCard: {
    width: wp(100),
    marginTop: wp(2),
    marginBottom: wp(3),
    padding: wp(3),
    backgroundColor: colors.white,
    paddingBottom: wp(2),
  },
  list: {
    paddingHorizontal: wp(3),
  },
  divider: {
    height: 1,
    width: wp(93),
    marginHorizontal: -wp(3),
    backgroundColor: "rgba(179,179,179,0.2)",
  },
  pageIndicator: {
    flexDirection: "row",
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2,
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3,
  },
});
