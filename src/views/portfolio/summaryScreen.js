import React from "react";
import { View, StyleSheet, ScrollView } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { connect } from "react-redux";
import RF from "react-native-responsive-fontsize";
import { CHIPS } from "../../components/portfolio/watchlist/index";
import { colors } from "../../styles/CommonStyles";
import LoadingView from "../../components/common/LoadingView";

import SummaryTab1 from "./summaryTab1";
import SummaryTab2 from "./summaryTab2";
import SummaryTab3 from "./summaryTab3";
import PortfolioEmptyView from "../../components/portfolio/PortfolioEmptyView";

import AddHoldings from "../../components/portfolio/AddHoldings";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";

class SummaryScreen extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      isLoading: false,
      isRefreshing: false,
      tabs: [{ name: "Summary" }, { name: "Stock Analysis" }, { name: "MF Analysis" }],
      selectedFilter: "Summary",
      showStockAnalyses: true,
      showMFAnalyses: true,
    };
  }

  // componentDidMount() {
  //   this.setNavigationListener();
  // const { navigation } = this.props;
  // const tabIndex = navigation.getParam("tabIndex", 0);
  // console.log("SummarytabIndex", tabIndex);
  // console.log("Logged User Data ===>", this.props.userState, "USRL")
  // this.getWatchlists()
  // }

  // componentWillUnmount() {
  //   this.navListener.remove();
  // }

  // setNavigationListener = () => {
  //   this.navListener = this.props.navigation.addListener("willFocus", () => {
  //     // this.getWatchlists()
  //   });
  // };

  addPortfolio = () => {
    const { baseRef, userState } = this.props;

    if (userState.isGuestUser) baseRef && baseRef.showLogin();
    else this.refs.newPortfolio.open();
  };

  getWatchlists() {
    console.log("tab 0 API called.");
  }

  openAddHolding = () => this.addHolding && this.addHolding.handlePopup();

  setPortfolioState = (stockState, mfState) => {
    this.setState({ showStockAnalyses: stockState, showMFAnalyses: mfState });
  };

  gotoAnalysisTab = (tab) => {
    if (tab == "STOCKS") {
      this.setState({ selectedFilter: "Stock Analysis" });
    } else if (tab == "MUTUAL FUNDS") {
      this.setState({ selectedFilter: "MF Analysis" });
    } else {
      this.setState({ selectedFilter: "Summary" });
    }
  };

  renderTabView = () => {
    const { isSelected } = this.props;
    const { selectedFilter } = this.state;

    if (selectedFilter === "Summary") {
      return (
        <SummaryTab1
          {...this.props}
          currentTab={isSelected}
          setEmpty={this.setPortfolioState}
          changeTab={this.gotoAnalysisTab}
          addHolding={this.openAddHolding}
        />
      );
    } else if (selectedFilter === "Stock Analysis") {
      return (
        <SummaryTab2
          {...this.props}
          currentTab={isSelected}
          changeTab={this.gotoAnalysisTab}
          addHolding={this.openAddHolding}
        />
      );
    } else {
      return (
        <SummaryTab3
          {...this.props}
          currentTab={isSelected}
          changeTab={this.gotoAnalysisTab}
          addHolding={this.openAddHolding}
        />
      );
    }
  };

  handleSubTabsClick = (SubsTabsName) => {
    // console.log("Summary Current Tab: ", SubsTabsName.name);
    this.setState({ selectedFilter: SubsTabsName.name });
  };

  renderTabs = () => {
    const { tabs, showStockAnalyses, showMFAnalyses } = this.state;
    return tabs.map((item, index) => {
      let showTab =
        item.name == "Stock Analysis" ? showStockAnalyses : item.name == "MF Analysis" ? showMFAnalyses : true;

      if (showTab)
        return (
          <CHIPS
            name={item.name ? item.name : ""}
            key={index}
            onPress={() => this.handleSubTabsClick(item, index)}
            selected={item.name === this.state.selectedFilter}
            disableImage={true}
          />
        );
      else return null;
    });
  };

  render() {
    const { isLoading, isRefreshing, showStockAnalyses, showMFAnalyses } = this.state;
    const { isGuestUser } = this.props.userState;
    const { gotoHoldings, isSelected } = this.props;

    if (isSelected) {
      let isPortfolioEmpty = !showStockAnalyses && !showMFAnalyses;
      return (
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <View style={styles.tabContainer}>{this.renderTabs()}</View>
            {this.renderTabView()}
          </ScrollView>

          <AddHoldings
            ref={(refs) => (this.addHolding = refs)}
            rootURL={this.props.baseURL}
            token={this.props.userState.token}
            appToken={this.props.appToken}
            portfolio={this.props.portfolioData.portfolio}
            {...this.props}
          />

          {/* <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={selectedStock}
          portfolio={portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        /> */}

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            // stock={null}
            showPortfolioModal={gotoHoldings}
            onUpdate={() => null}
          />

          {(isGuestUser || isPortfolioEmpty) && <PortfolioEmptyView onPress={this.addPortfolio} />}
          {isLoading || isRefreshing ? <LoadingView /> : null}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user, portfolioData: state.portfolio };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(SummaryScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  tabContainer: {
    flexDirection: "row",
    marginTop: wp(5),
    paddingLeft: wp(1.5),
  },
});
