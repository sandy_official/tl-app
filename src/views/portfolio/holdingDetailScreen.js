import React from "react";
import { View, StyleSheet, Platform, Text, TouchableOpacity, Image, FlatList } from "react-native";

import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { globalStyles } from "../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";

import { Toolbar } from "../../components/Toolbar";

import { connect } from "react-redux";

import { TagNameWithIcon, CHIPS, sortDataByKey, PopUpbutton } from "../../components/portfolio/watchlist/index";
import HoldingsCard, { CardButton, DetailChip, InputFieldWithTag } from "../../components/portfolio/holdings";
import StockAddBottomSheet from "../../components/stockDetail/StockAddBottomSheet";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { updatePortfolio } from "../../actions/PortfolioActions";

import { fetcher } from "../../controllers/Fetcher";
import { POST_METHOD, MONTHS_FULL } from "../../utils/Constants";

import {
  PORTFOLIO_HOLDINGS_LIST_URL,
  PORTFOLIO_HOLDINGS_SELL_URL,
  PORTFOLIO_HOLDINGS_SELLFIFO_URL,
  PORTFOLIO_HOLDINGS_DELETE_URL,
  PORTFOLIO_HOLDINGS_EDIT_URL,
} from "../../utils/ApiEndPoints";
import { getDVMBackgroundColor, formatNumberToInr, getOrdinalNumForDate, dateStringFromObj } from "../../utils/Utils";

import LoadingView from "../../components/common/LoadingView";

import Toast from "react-native-easy-toast";
import DateTimePicker from "react-native-modal-datetime-picker";
import ScrollToTopView from "../../components/common/ScrollToTopView";

const HOLDING_ICON = require("../../assets/icons/portfolio/holdingsIcon.png");
const priceArrowGreen = require("../../assets/icons/arrow-green.png");
const priceArrowRed = require("../../assets/icons/arrow-red.png");

class HoldingsDetailScreen extends React.PureComponent {
  state = {
    isLoading: false,
    isRefreshing: false,
    holdingData: {},
    holdingObj: {},
    holdingStockName: "",
    stockDayGain: "-",
    stockDayChangePer: "-",
    sortingFilters: [{ name: "Open Date" }, { name: "Qty" }, { name: "Total Invested" }, { name: "Unrealized Gain" }],
    selectedFilter: "Open Date",
    reverseOrder: false,
    holdingsListByStocks: [],
    minimumSellDate: new Date(),
    maximumSellDate: new Date(),
    availableQty: "",
    stockId: "",
    sellDate: { date: new Date(), formetted: "Select Date" },
    buyDate: { date: new Date(), formetted: "Select Date" },
    selectingAddtionalDate: false,
    dropdownPosition: {},
    selectedPortFolio: ["", "Select a Portfolio"],
    showPortfolioList: false,
    actionText: "",
    holdingId: "",
    sellQty: "",
    sellPrice: "",
    buyPrice: "",
    errorText: "",
    sellFromStatus: { status: false, error: "" },
    sellDateStatus: { status: false, error: "" },
    quantityStatus: { status: false, error: "" },
    sellPriceStatus: { status: false, error: "" },
    buyPriceStatus: { status: false, error: "" },
    buyDateStatus: { status: false, error: "" },
    activeSubmitBtn: false,
    showScrollToTop: false,
  };

  componentDidMount() {
    if (this.props.navigation.state.params) {
      const { params } = this.props.navigation.state;
      this.setState(
        {
          holdingObj: params.holdingData ? params.holdingData : {},
          holdingData: params.holdingData.obj ? params.holdingData.obj : {},
          availableQty: params.holdingData.obj.qty ? params.holdingData.obj.qty : "",
          holdingStockName: params.holdingData.obj["get_full_name"] ? params.holdingData.obj["get_full_name"] : "",
        },
        () => {
          this.getHoldingDetails();
        }
      );
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  renderToolbar = () => {
    const { holdingStockName } = this.state;
    return <Toolbar title={holdingStockName ? holdingStockName : ""} backButton {...this.props} />;
  };

  getHoldingDetails = () => {
    const { userState, baseURL, appToken } = this.props;
    const { token } = userState;

    const URL = baseURL + PORTFOLIO_HOLDINGS_LIST_URL;
    let requestData = { stock_id: this.state.holdingData["stock_id"] };
    console.log("Requset Data getHoldingDetails--->", URL, requestData);

    this.setState({ isLoading: true }, () => {
      fetcher(
        URL,
        POST_METHOD,
        requestData,
        null,
        // baseRef,
        appToken,
        token,
        (json) => {
          const { body } = json;
          console.log("holdings API response -->", json);
          let dataObj = this.mapListData(json.body).stocks;
          this.setState({
            holdingsListByStocks: dataObj ? dataObj : [],
            stockDayGain: body.dayGain ? body.dayGain : "-",
            stockDayChangePer: body.dayChangeP ? body.dayChangeP : "-",
            stockCurrPrice: body.currentPrice ? body.currentPrice : null,
            isLoading: false,
          });
        },
        (message) => {
          this.setState({
            isLoading: false,
            holdingsListByStocks: [],
          });
          console.log("holdings API response -->1", message);
        }
      );
    });
  };

  mapListData = (json) => {
    let stockList = [];
    let columnList = json.tableHeaders;
    let screenData = json.screen ? json.screen : {};
    // stock details
    let tableData = json.tabledata;
    tableData.forEach((stockIndex) => {
      let stockObj = [];
      let stockArray = [];
      let dvmValueCrossed = false; // to get data which is after dvm in json object
      stockIndex.forEach((item, i) => {
        let column = Object.assign({}, columnList[i]);
        let columnName = column.unique_name ? column.unique_name : column.name;

        if (dvmValueCrossed) {
          column["value"] = item;
          stockArray.push(column);
        }

        if (column.unique_name == "m_color") dvmValueCrossed = true;
        stockObj[columnName] = item;
      });
      stockList.push({ obj: stockObj, array: stockArray });
    });
    return {
      isNextPage: json.isNextPage ? json.isNextPage : "",
      order: json.order ? json.order : "",
      description: json.description ? json.description : "",
      title: json.title ? json.title : "",
      sortBy: json.sortBy ? json.sortBy : "",
      pageNumber: json.thisPageNumber ? json.thisPageNumber : "",
      pageCount: json.thisPageCount ? json.thisPageCount : "",
      stocks: stockList,
      screen: screenData,
    };
  };

  createSortingParameters = (MappingData) => {
    const last = "m_color";
    let compInd = MappingData ? MappingData.findIndex((ele) => ele.unique_name === last) : -1;
    let tempArray = MappingData.filter((ele, ind) => ind > compInd);
    return tempArray;
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  showPortfolioModal = () => {
    this.refs.portfolio.modal.open();
    this.refs.portfolio.showPortfolioAdded();
  };

  selectPortfolio = (item) => {
    console.log("Selected Portfolio", item);
    this.setState({
      selectedPortFolio: item,
      errorText: "",
      sellFromStatus: {
        status: true,
        error: "",
      },
    });
    this.toggleDropdown();
  };

  handleSortingClick = (ele, ind) => {
    const { selectedFilter, reverseOrder } = this.state;
    // console.log("Called", selectedFilter, reverseOrder, ele.name, selectedFilter === ele.name)
    if (selectedFilter === ele.name) {
      this.setState(
        {
          reverseOrder: !reverseOrder,
          refreshing: !this.state.refreshing,
        },
        () => {
          // console.log("Called 2", selectedFilter, reverseOrder, ele.name, selectedFilter === ele.name);
          this.handleSortingData(ele.name);
        }
      );
    } else {
      this.setState(
        {
          selectedFilter: ele.name ? ele.name : "",
          reverseOrder: false,
          refreshing: !this.state.refreshing,
        },
        () => {
          this.handleSortingData(ele.name);
        }
      );
    }
  };
  handleSortingData = (type) => {
    const { holdingsListByStocks, reverseOrder } = this.state;
    this.setState({
      holdingsListByStocks: sortDataByKey(holdingsListByStocks, reverseOrder ? "lowest_first" : "highest_first", type),
      refreshing: !this.state.refreshing,
    });
  };

  renderSortingChips = (sortingParameter) => {
    return (
      <FlatList
        data={this.state.sortingFilters}
        contentContainerStyle={{ paddingHorizontal: wp(2) }}
        showsHorizontalScrollIndicator={false}
        renderItem={({ item, index }) => (
          <CHIPS
            chipsMainView={{ marginVertical: wp(1) }}
            name={item.name ? item.name : ""}
            gradient
            onPress={() => this.handleSortingClick(item, index)}
            selected={item.name === this.state.selectedFilter}
            doubleSelection={item.name === this.state.selectedFilter && this.state.reverseOrder}
          />
        )}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        extraData={this.state || this.state.selectedFilter}
      />
    );
  };

  handleCloseButton() {
    this.refs.holdingsActionsPopUp.RBSheet.close();
  }
  renderButtonRow = (obj) => {
    return (
      <View style={[styles.CardButtonRow, {}]}>
        <CardButton
          buttonMainView={[styles.CardButtonMainView, { borderColor: colors.red }]}
          buttonText={{ color: colors.red }}
          ButtonName="Delete"
          disableImg
          onPress={() => this.handleCardButton("delete", obj)}
        />
        <CardButton
          buttonMainView={[styles.CardButtonMainView]}
          ButtonName="Edit"
          disableImg
          onPress={() => this.handleCardButton("edit", obj)}
        />
        <CardButton
          buttonMainView={[styles.CardButtonMainView]}
          ButtonName="Sell"
          disableImg
          onPress={() => this.handleCardButton("sell", obj)}
        />
      </View>
    );
  };

  createDateObject = (givenDate, type) => {
    let dateObj = {};
    let date = new Date(givenDate);
    // let day = date.getDate();
    // let month = MONTHS_FULL[date.getMonth()];
    let day = date.getDate() > 10 ? date.getDate() : `0${date.getDate()}`;
    let month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    let year = date.getFullYear();
    // 2015-08-01
    let formattedDate = `${day}-${month}-${year}`;
    // let formattedDate = getOrdinalNumForDate(day) + " " + month + ", " + year;
    dateObj = { formetted: formattedDate, dateObj: date };
    return dateObj;
  };

  handleCardButton = (CRUD, holdingObj) => {
    const { holding_id, portfolio_name, portfolio_id, open_date, qty, openprice, stock_id } = holdingObj.obj;
    this.setState(
      {
        holdingId: holding_id,
      },
      () => {
        if (CRUD === "delete") {
          this.setState(
            {
              selectingAddtionalDate: false,
              actionText: "Delete holdings",
            },
            () => {
              this.refs.holdingsActionsPopUp.RBSheet.open();
            }
          );
        } else if (CRUD === "edit") {
          const tempdata = [portfolio_id, portfolio_name];
          const tmpObj = {
            status: true,
            error: "",
          };
          this.setState(
            {
              selectingAddtionalDate: false,
              actionText: "Edit holdings",
              stockId: stock_id,
              selectedPortFolio: tempdata,
              sellDate: this.createDateObject(open_date, "edit"),
              sellQty: qty,
              sellPrice: openprice,
              errorText: "",
              sellFromStatus: tmpObj,
              sellDateStatus: tmpObj,
              quantityStatus: tmpObj,
              sellPriceStatus: tmpObj,
            },
            () => {
              this.refs.holdingsActionsPopUp.RBSheet.open();
            }
          );
        } else {
          const tempdata = [portfolio_id, portfolio_name];
          const tmpObj = {
            status: true,
            error: "",
          };
          this.setState(
            {
              actionText: "Sell holdings",
              stockId: stock_id,
              selectedPortFolio: tempdata,
              buyDate: this.createDateObject(open_date, "sell"),
              sellDate: {
                date: new Date(),
                formetted: "Select Date",
              },
              sellQty: "",
              sellPrice: "",
              buyPrice: openprice,
              errorText: "",
              sellFromStatus: tmpObj,
              buyPriceStatus: tmpObj,
            },
            () => {
              this.refs.holdingsActionsPopUp.RBSheet.open();
            }
          );
        }
      }
    );
  };

  renderMoreDetailsCard = (item, ind) => {
    return (
      <HoldingsCard
        disableDVM={true}
        {...this.props}
        stock={item}
        type={"holdingDetail"}
        position={ind}
        children={this.renderButtonRow(item)}
      />
    );
  };

  formatTime = (str) => {
    if (str) {
      try {
        const d = new Date(str);
        var hours = d.getHours();
        var minutes = d.getMinutes();

        return hours + ":" + minutes;
      } catch (e) {
        console.log("holdings date parse error ", e);
        return str;
      }
    }
  };

  renderListHeader = () => {
    const { holdingData, holdingStockName, stockDayChangePer, stockDayGain, stockCurrPrice } = this.state;
    const holdingDate = new Date(holdingData["lastprice_date"]);
    const marketType = holdingData["NSEcode"] ? "NSE" : "BSE";
    return (
      <View>
        <View style={[styles.SectionOne, {}]}>
          <View style={[styles.MarketTypeView, {}]}>
            <Text style={[styles.MarketTypeText, {}]}>{`${marketType} | ${this.formatTime(
              holdingDate
            )} | ${dateStringFromObj(holdingDate)}`}</Text>
          </View>
          <View style={[styles.StockWithDVMView, {}]}>
            <View style={[styles.StockNameView, {}]}>
              <Text style={[styles.StockNameText]}>{holdingStockName}</Text>
            </View>
            <View style={[styles.DVMView, {}]}>
              <View style={getDVMBackgroundColor(holdingData["d_color"])} />
              <View style={getDVMBackgroundColor(holdingData["v_color"])} />
              <View style={getDVMBackgroundColor(holdingData["m_color"])} />
            </View>
          </View>
          <View style={[styles.DaysGainRowView]}>
            <View style={{}}>
              <Text style={[styles.DaysGainText]}>{`Day's Gain:`}</Text>
            </View>
            <View style={[styles.priceChangeIcon]}>
              {stockCurrPrice && <Image source={parseInt(stockDayGain) < 0 ? priceArrowRed : priceArrowGreen} />}
              <Text style={[styles.currPriceText]}>{stockCurrPrice}</Text>
            </View>
            <DetailChip
              ChipMainContainer={{
                backgroundColor:
                  parseInt(stockDayGain) < 0
                    ? colors.red
                    : parseInt(stockDayGain) == 0 || !stockDayGain
                    ? colors.brightBlue
                    : colors.green,
              }}
              firstDetail={`${formatNumberToInr(stockDayGain, true)}`}
              secondDetail={`${stockDayChangePer}%`}
            />
          </View>
          <View style={[styles.ButtonView, {}]}>
            <CardButton disableImg ButtonName="- Sell FIFO" onPress={() => this.handleSell()} />
            <View style={{ marginHorizontal: wp(2) }}></View>
            <CardButton ButtonName="Portfolio" onPress={() => this.refs.portfolio.modal.open()} />
          </View>
        </View>
        <View style={[styles.SectionTwo, {}]}>
          <TagNameWithIcon headingContainer={[styles.HeadingTag]} TagName={"ALL HOLDINGS"} Icon={HOLDING_ICON} />
        </View>
        <View style={[styles.ChipsView, {}]}>{this.renderSortingChips()}</View>
      </View>
    );
  };

  updateDropdownPosition = (event) => {
    event.preventDefault();
    this.setState({ dropdownPosition: event.nativeEvent.layout });
  };

  handleDelete = (command) => {
    this.setState({
      isLoading: true,
    });
    let requestBody = {
      holdingId: this.state.holdingId,
    };
    const { baseRef, userState, baseURL, appToken } = this.props;
    const { token } = userState;
    const URL = baseURL + PORTFOLIO_HOLDINGS_DELETE_URL;

    if (command === "yes") {
      fetcher(
        URL,
        POST_METHOD,
        requestBody,
        baseRef,
        appToken,
        token,
        (json) => {
          const { head } = json;
          this.setState({ isLoading: false }, () => {
            this.refs.holdingsActionsPopUp.RBSheet.close();
            this.getHoldingDetails();
            console.log("===>", this.state.holdingsListByStocks.length);
            setTimeout(() => this.refs.toast.show(head.statusDescription), 200);
            if (this.state.holdingsListByStocks.length - 1 === 0) {
              setTimeout(() => this.props.navigation.pop(), 250);
            }
            // setTimeout(() => this.props.navigation.pop(), 300);
          });
        },
        (message) => {
          this.setState({ isLoading: false });
          console.log("Message -->", message);

          if (message) this.refs.toast.show(message);
          else this.refs.toast.show("Please try again");
        }
      );
    } else {
      this.setState(
        {
          actionText: "",
          stockName: "",
          isLoading: false,
        },
        () => this.refs.holdingsActionsPopUp.RBSheet.close()
      );
    }
  };

  renderHoldindsActionChilds = () => {
    const { minimumSellDate, maximumSellDate, actionText, isDatePickerVisible, selectingAddtionalDate } = this.state;
    if (actionText != "" && actionText.toLowerCase().includes("delete")) {
      console.log("Delete Case");
      const deleteText = `Are you sure about deleting this holding transaction ? This action can not be reversed.`;
      return (
        <View style={{ justifyContent: "space-between" }}>
          <View>
            <Text style={[{ fontSize: RF(2.2), color: colors.black }]}>{deleteText}</Text>
          </View>
          <View style={{ marginVertical: wp(6) }}></View>
          <View style={[styles.deleteWatchlistButtonContainer]}>
            <PopUpbutton
              buttonName={"Yes, delete"}
              buttonNameText={{ color: colors.white }}
              onPress={() => this.handleDelete("yes")}
            />
            <View style={{ marginHorizontal: wp(1) }}></View>
            <PopUpbutton
              buttonMainView={{ backgroundColor: colors.white, borderColor: colors.primary_color }}
              buttonName={"No, keep it"}
              onPress={() => this.handleDelete("no")}
            />
          </View>
        </View>
      );
    } else if (actionText != "" && actionText.toLowerCase().includes("edit")) {
      return (
        <View onLayout={this.updateDropdownPosition}>
          {this.renderPortfolioDropdown("edit")}
          {this.renderDateField("edit")}
          {this.renderPortfolioList("edit")}
          <DateTimePicker
            // minimumDate={minimumSellDate}
            // maximumDate={maximumSellDate}
            isVisible={isDatePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.toggleDatePicker}
          />
          {this.renderTextInputBox("edit")}
          {this.renderErrorText("edit")}
          {this.renderActionButtons("edit")}
        </View>
      );
    } else if (actionText != "" && actionText.toLowerCase().includes("sell fifo")) {
      const { minimumSellDate, maximumSellDate, isDatePickerVisible } = this.state;
      return (
        <View onLayout={this.updateDropdownPosition}>
          {this.renderPortfolioDropdown("sellFIFO")}
          {this.renderDateField("sellFIFO")}
          {this.renderPortfolioList("sellFIFO")}
          <DateTimePicker
            // minimumDate={minimumSellDate}
            // maximumDate={maximumSellDate}
            isVisible={isDatePickerVisible}
            onConfirm={this.handleDatePicked}
            onCancel={this.toggleDatePicker}
          />
          {this.renderTextInputBox("sellFIFO")}
          {this.renderErrorText("sellFIFO")}
          {this.renderActionButtons("sellFIFO")}
        </View>
      );
    } else {
      return (
        <View onLayout={this.updateDropdownPosition}>
          {this.renderPortfolioDropdown("sell")}
          {this.renderDateField("sell")}
          {this.renderDateField("buy")}
          {this.renderPortfolioList("sell")}
          <DateTimePicker
            // minimumDate={minimumSellDate}
            // maximumDate={this.state.selectingAddtionalDate ? null:maximumSellDate}
            isVisible={isDatePickerVisible}
            onConfirm={selectingAddtionalDate ? this.handleDatePicked2 : this.handleDatePicked}
            onCancel={this.toggleDatePicker}
          />
          {this.renderTextInputBox("sell")}
          {this.renderErrorText("sell")}
          {this.renderActionButtons("sell")}
        </View>
      );
    }
  };

  renderErrorText = () => {
    const { errorText } = this.state;
    if (errorText != "")
      return (
        <View style={{ marginVertical: wp(2) }}>
          <Text style={[styles.redText, { fontSize: RF(1.3) }]}>{`* ${errorText}`}</Text>
        </View>
      );
  };

  popUpContentComponent = (type) => {
    const { minimumSellDate, maximumSellDate, isDatePickerVisible } = this.state;
    return (
      <View onLayout={this.updateDropdownPosition}>
        {this.renderPortfolioDropdown(type)}
        {this.renderDateField(type)}
        {this.renderPortfolioList(type)}
        <DateTimePicker
          minimumDate={minimumSellDate}
          maximumDate={maximumSellDate}
          isVisible={isDatePickerVisible}
          onConfirm={this.handleDatePicked}
          onCancel={this.toggleDatePicker}
        />
        {this.renderTextInputBox("Quantity", type)}
        {this.renderTextInputBox("Sell Price", type)}
        {this.renderActionButtons(type)}
      </View>
    );
  };
  setInputText = (type, value, popUpName) => {
    if (type == "Quantity") {
      if (value === "") {
        this.setState(
          {
            sellQty: value,
            quantityStatus: {
              error: "Please enter quantity.",
              status: false,
            },
          },
          () => {
            this.setState({
              errorText: this.state.quantityStatus.error,
            });
          }
        );
      } else if (value > this.state.availableQty) {
        this.setState(
          {
            sellQty: value,
            quantityStatus: {
              error: "The quantity is exceeding than available.",
              status: false,
            },
          },
          () => {
            this.setState({
              errorText: "The quantity is exceeding than available.",
            });
          }
        );
      } else {
        this.setState({
          sellQty: value,
          activeSubmitBtn: popUpName === "edit" ? true : false,
          quantityStatus: {
            error: "",
            status: true,
          },
          errorText: "",
        });
      }
    } else if (type === "Buy Price") {
      if (value === "") {
        this.setState(
          {
            buyPrice: value,
            buyPriceStatus: {
              error: "Please enter buy price.",
              status: false,
            },
          },
          () => {
            this.setState(
              {
                errorText: this.state.buyPriceStatus.error,
              },
              () => {
                console.log("--error-->", this.state.buyPriceStatus, this.state.errorText);
              }
            );
          }
        );
      } else {
        this.setState({
          buyPrice: value,
          buyPriceStatus: {
            error: "",
            status: true,
          },
          errorText: "",
        });
      }
    } else {
      if (value === "") {
        this.setState(
          {
            sellPrice: value,
            sellPriceStatus: {
              error: "Please enter sell price.",
              status: false,
            },
          },
          () => {
            this.setState({
              errorText: this.state.sellPriceStatus.error,
            });
          }
        );
      } else {
        this.setState({
          sellPrice: value,
          sellPriceStatus: {
            error: "",
            status: true,
          },
          activeSubmitBtn: popUpName === "edit" ? true : false,
          errorText: "",
        });
      }
    }
  };

  getStyle = (tagName) => {
    let inputBox =
      this.state.inputFocused == tagName
        ? [styles.inputContainer2, styles.activeTextBox, { paddingBottom: wp(0.5), marginBottom: wp(1) }]
        : [styles.inputContainer2];
    return inputBox;
  };
  renderTextInputBox = (type) => {
    const first_tag = "Quantity";
    const second_tag = "Sell Price";
    const third_tag = "Buy Price";
    return (
      <View>
        <InputFieldWithTag
          inputBoxStyle={[
            this.getStyle(first_tag),
            {
              borderBottomColor:
                this.state.inputFocused == first_tag
                  ? this.state.quantityStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.quantityStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            },
          ]}
          label={first_tag}
          onChangeText={(value) => this.setInputText(first_tag, value, type)}
          onFocus={() => this.toggleTextInputFocus(first_tag)}
          onBlur={() => this.toggleTextInputFocus(null)}
          placeholder={`Available : ${this.state.availableQty.toString()}`}
          value={this.state.sellQty.toString()}
        />

        <InputFieldWithTag
          inputBoxStyle={[
            this.getStyle(second_tag),
            {
              borderBottomColor:
                this.state.inputFocused == second_tag
                  ? this.state.sellPriceStatus.error
                    ? colors.red
                    : colors.primary_color
                  : this.state.sellPriceStatus.error != ""
                  ? colors.red
                  : colors.greyishBrown,
            },
          ]}
          label={type === "edit" ? third_tag : second_tag}
          onChangeText={(value) => this.setInputText(second_tag, value, type)}
          onFocus={() => this.toggleTextInputFocus(second_tag)}
          onBlur={() => this.toggleTextInputFocus(null)}
          // placeholder={this.state.sellPrice.toString()}
          value={this.state.sellPrice.toString()}
        />
        {type === "sell" ? (
          <InputFieldWithTag
            inputBoxStyle={[
              this.getStyle(third_tag),
              {
                borderBottomColor:
                  this.state.inputFocused == third_tag
                    ? this.state.buyPriceStatus.error
                      ? colors.red
                      : colors.primary_color
                    : this.state.buyPriceStatus.error != ""
                    ? colors.red
                    : colors.greyishBrown,
              },
            ]}
            label={third_tag}
            onChangeText={(value) => this.setInputText(third_tag, value, type)}
            onFocus={() => this.toggleTextInputFocus(third_tag)}
            onBlur={() => this.toggleTextInputFocus(null)}
            // placeholder={this.state.sellPrice.toString()}
            value={this.state.buyPrice.toString()}
          />
        ) : null}
      </View>
    );
  };

  toggleTextInputFocus = (value) => {
    this.setState({ inputFocused: value });
  };

  handleDatePicked = (date) => {
    this.toggleDatePicker();
    let day = date.getDate() > 10 ? date.getDate() : `0${date.getDate()}`;
    let month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    let year = date.getFullYear();
    let formattedDate = `${day}-${month}-${year}`;
    const dateObj = { formetted: formattedDate, dateObj: date };
    this.setState({ sellDate: dateObj, sellDateStatus: { status: true, error: "" }, errorText: "" });
  };

  handleDatePicked2 = (date) => {
    this.toggleDatePicker();
    let day = date.getDate() > 10 ? date.getDate() : `0${date.getDate()}`;
    let month = date.getMonth() + 1 > 10 ? date.getMonth() + 1 : `0${date.getMonth() + 1}`;
    let year = date.getFullYear();
    let formattedDate = `${day}-${month}-${year}`;
    const dateObj = { formetted: formattedDate, dateObj: date };
    this.setState({ buyDate: dateObj, buyDateStatus: { status: true, error: "" }, errorText: "" }, () => {
      this.setState({ selectingAddtionalDate: false });
    });
  };

  renderPortfolioList = (type) => {
    const { portFolioData } = this.props;
    const { y, width } = this.state.dropdownPosition;
    const style = {
      position: "absolute",
      top: Platform.OS == "android" ? hp(9) : y + hp(7),
      width: width,
      maxHeight: hp(20),
      minHeight: wp(4),
      backgroundColor: colors.white,
    };
    if (this.state.showPortfolioList) {
      return (
        <View style={[style, styles.list]}>
          <FlatList data={portFolioData.portfolio} extraData={this.props} renderItem={this.renderPortfolioItem} />
        </View>
      );
    }
    return null;
  };

  renderPortfolioItem = ({ item, index }) => {
    return (
      <TouchableOpacity onPress={() => this.selectPortfolio(item)}>
        <Text style={styles.listItem}>{item[1] ? item[1] : ""}</Text>
      </TouchableOpacity>
    );
  };

  toggleDatePicker = (type) => {
    const { inputFocused } = this.state;
    const focusedInput = inputFocused == "date" ? "" : "date";
    this.setState({
      selectingAddtionalDate:
        type === "buy" && this.state.actionText.toLowerCase().includes("sell holdings") ? true : false,
      isDatePickerVisible: !this.state.isDatePickerVisible,
      inputFocused: focusedInput,
    });
  };

  renderDateField = (fieldName) => {
    const inputBoxStyle =
      this.state.inputFocused == "date" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];

    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.sellDateStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          {fieldName === "buy" || fieldName === "edit" ? "Buy Date" : "Sell Date "}
          <Text style={styles.redText}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={() => this.toggleDatePicker(fieldName)}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.greyishBrown }]}>
            {fieldName === "buy" ? this.state.buyDate.formetted : this.state.sellDate.formetted}
          </Text>
          <Image
            source={require("../../assets/icons/date-picker-blue.png")}
            style={styles.inputIcon}
            resizeMode="contain"
          />
        </TouchableOpacity>
      </View>
    );
  };

  renderPortfolioDropdown = (type) => {
    const inputBoxStyle =
      this.state.inputFocused == "dropdown" ? [styles.inputContainer2, styles.activeTextBox] : [styles.inputContainer2];

    const dropdownIcon = this.state.showPortfolioList
      ? require("../../assets/icons/arrow-up-blue.png")
      : require("../../assets/icons/arrow-down-blue.png");

    return (
      <View
        style={[
          inputBoxStyle,
          {
            borderBottomColor:
              this.state.inputFocused == "dropdown"
                ? colors.primary_color
                : this.state.sellFromStatus.error === ""
                ? colors.greyishBrown
                : colors.red,
          },
        ]}
      >
        <Text style={[styles.inputLabel]}>
          {type === "edit" ? "Portfolio" : "Sell from"}
          <Text style={styles.redText}> *</Text>
        </Text>
        <TouchableOpacity style={styles.inputRow} onPress={this.toggleDropdown}>
          <Text style={[styles.textInput2, { flex: 1, color: colors.greyishBrown }]}>
            {this.state.selectedPortFolio[1]}
          </Text>
          <Image source={dropdownIcon} style={styles.inputIcon} resizeMode="contain" />
        </TouchableOpacity>
      </View>
    );
  };

  toggleDropdown = () => {
    const { inputFocused } = this.state;
    const focusedInput = this.state.inputFocused == "dropdown" ? "" : "dropdown";
    this.setState({ showPortfolioList: !this.state.showPortfolioList, inputFocused: focusedInput });
  };

  checkForm = (entity) => {
    let flag = false;
    if (entity.status) flag = true;
    else {
      this.setState({ errorText: entity.error });
    }
    return flag;
  };

  checkBtnStatus = (type) => {
    const { sellFromStatus, sellDateStatus, quantityStatus, sellPriceStatus, buyPriceStatus } = this.state;
    if (type === "sell") {
      if (
        this.checkForm(sellDateStatus) &&
        this.checkForm(quantityStatus) &&
        this.checkForm(sellPriceStatus) &&
        this.checkForm(buyPriceStatus)
      ) {
        return { color: colors.primary_color, status: true };
      } else {
        return { color: colors.greyishBrown, status: false };
      }
    } else {
      if (
        this.checkForm(sellFromStatus) &&
        this.checkForm(sellDateStatus) &&
        this.checkForm(quantityStatus) &&
        this.checkForm(sellPriceStatus)
      ) {
        return { color: colors.primary_color, status: true };
      } else {
        return { color: colors.greyishBrown, status: false };
      }
    }
  };

  handleValidation = (type) => {
    console.log("handle validation called for ", type, "type.");
    const { selectedPortFolio, sellDate, sellPrice, buyPrice, availableQty, sellQty } = this.state;
    let txt;
    if (type === "sellFIFO") {
      if (selectedPortFolio[1] === "Select a Portfolio") {
        txt = "Please select a portfolio.";
        this.setState({
          errorText: txt,
          sellFromStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (sellDate.formetted === "Select Date") {
        txt = "Please select sell date.";
        this.setState({
          errorText: txt,
          sellDateStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (sellQty == "") {
        txt = "Please enter sell quantity.";
        this.setState({
          errorText: txt,
          quantityStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (parseInt(sellQty) > parseInt(availableQty)) {
        txt = "Please enter sell quantity.";
        this.setState({
          errorText: txt,
          quantityStatus: {
            error: "exceeding than available.",
            status: false,
          },
        });
      } else if (sellPrice === "") {
        txt = "Please enter sell price.";
        this.setState({
          errorText: txt,
          sellPriceStatus: {
            status: false,
            error: txt,
          },
        });
      }
    } else if (type === "edit") {
      // console.log("Checking Status for Current PortFolio",this.checkUnchagedStatus(this.state.selectedPortFolio))
    } else if (type === "sell") {
      if (selectedPortFolio[1] === "Select a Portfolio") {
        txt = "Please select a portfolio.";
        this.setState({
          errorText: txt,
          sellFromStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (sellDate.formetted === "Select Date") {
        txt = "Please select sell date.";
        this.setState({
          errorText: txt,
          sellDateStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (sellQty == "") {
        txt = "Please enter sell quantity.";
        this.setState({
          errorText: txt,
          quantityStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (parseInt(sellQty) > parseInt(availableQty)) {
        txt = "Please enter sell quantity.";
        this.setState({
          errorText: txt,
          quantityStatus: {
            error: "exceeding than available.",
            status: false,
          },
        });
      } else if (sellPrice === "") {
        txt = "Please enter sell price.";
        this.setState({
          errorText: txt,
          sellPriceStatus: {
            status: false,
            error: txt,
          },
        });
      } else if (buyPrice === "") {
        txt = "Please enter buy price.";
        this.setState({
          errorText: txt,
          buyPriceStatus: {
            status: false,
            error: txt,
          },
        });
      }
    }
  };

  renderActionButtons = (type) => {
    if (type === "edit") {
      return (
        <View style={[styles.actionButtonRow]}>
          <CHIPS
            disableImage
            name="Cancel"
            chipsMainView={styles.button}
            onPress={() => this.handleSellFIFOActions("Cancel")}
          />
          <View style={{ flex: 0.3 }}></View>
          <CHIPS
            disableImage
            name="Submit"
            // chipsMainView={[
            //   styles.button,
            //   { backgroundColor: this.checkBtnStatus(type).color, borderColor: this.checkBtnStatus(type).color },
            // ]}
            chipsMainView={[styles.button, { backgroundColor: colors.primary_color }]}
            selected
            // onPress={() => this.checkBtnStatus(type).status ? this.handleSellFIFOActions("edit") : this.handleValidation(type)}
            onPress={() => this.handleSellFIFOActions("edit")}
          />
        </View>
      );
    } else {
      return (
        <View style={[styles.actionButtonRow]}>
          <CHIPS
            disableImage
            name="Cancel"
            chipsMainView={styles.button}
            onPress={() => this.handleSellFIFOActions("Cancel")}
          />
          <View style={{ flex: 0.3 }}></View>
          <CHIPS
            disableImage
            name="Sell"
            chipsMainView={[
              styles.button,
              { backgroundColor: this.checkBtnStatus(type).color, borderColor: this.checkBtnStatus(type).color },
            ]}
            selected
            onPress={() =>
              this.checkBtnStatus(type).status
                ? type === "sell"
                  ? this.handleSellFIFOActions("sell")
                  : this.handleSellFIFOActions("SellFIFO")
                : this.handleValidation(type)
            }
          />
        </View>
      );
    }
  };

  convertRequestDateObj = (normalDate) => {
    let date = new Date(normalDate.dateObj);
    let day = date.getDate();
    let year = date.getFullYear();
    let monthApi = date.getMonth() + 1 < 10 ? "0" + (date.getMonth() + 1) : date.getMonth() + 1;
    // 2015-08-01
    let dateForApi = year + "-" + monthApi + "-" + day;
    return dateForApi;
  };

  handleSellFIFOActions = (actiontype) => {
    if (actiontype === "SellFIFO") {
      const { baseRef, userState, baseURL, appToken } = this.props;
      const { selectedPortFolio, sellDate, sellPrice, sellQty, stockId } = this.state;

      const URL = baseURL + PORTFOLIO_HOLDINGS_SELLFIFO_URL;

      let requestData = {
        portfolioId: selectedPortFolio[0] ? selectedPortFolio[0] : "",
        stock_id: stockId ? parseInt(stockId) : "",
        qty: sellQty ? parseInt(sellQty) : "",
        sellPrice: sellPrice ? parseInt(sellPrice) : "",
        closeDate: this.convertRequestDateObj(sellDate) ? this.convertRequestDateObj(sellDate) : "",
        notes: "source:mobile app",
      };

      this.setState(
        { isLoading: true },
        () => {
          console.log("Requset Data for selling FIFO-->", URL, POST_METHOD, requestData, userState.token);
          fetcher(
            URL,
            POST_METHOD,
            requestData,
            baseRef,
            appToken,
            userState.token,
            (json) => {
              console.log("SELL Holdings API response -->", json);
              this.setState(
                {
                  isLoading: false,
                },
                () => {
                  setTimeout(() => {
                    this.refs.toast.show(
                      `Successfully sold ${this.state.sellQty} @ ${this.state.sellPrice} using FIFO approach from this account.`
                    );
                  }, 300);
                  this.getHoldingDetails();
                }
              );
            },
            (message) => {
              this.setState({
                isLoading: false,
              });
              console.log("SELL HOLDING API Message -->", message);
              setTimeout(() => {
                this.refs.toast.show(message);
              }, 300);
            }
          );
          // this.getHoldingDetails();
        },
        (message) => {
          this.setState({
            isLoading: false,
          });
          console.log("SELL HOLDING API Message -->", message);
          setTimeout(() => {
            this.refs.toast.show(message);
          }, 300);
        }
      );
    } else if (actiontype === "edit") {
      const { baseRef, userState, baseURL, appToken } = this.props;
      const { selectedPortFolio, sellPrice, sellDate, sellQty, stockId } = this.state;
      const URL = baseURL + PORTFOLIO_HOLDINGS_EDIT_URL;
      let requestData = {
        portfolioId: selectedPortFolio[0] ? selectedPortFolio[0] : "",
        stock_id: stockId ? parseInt(stockId) : "",
        qty: sellQty ? parseInt(sellQty) : "",
        price: sellPrice ? parseInt(sellPrice) : "",
        holdingId: this.state.holdingId,
        transactionDate: this.convertRequestDateObj(sellDate) ? this.convertRequestDateObj(sellDate) : "",
        notes: "source:mobile app",
      };

      this.setState(
        {
          isLoading: true,
        },
        () => {
          console.log("Requset Data for editing holdings-->", URL, POST_METHOD, requestData, userState.token);
          fetcher(
            URL,
            POST_METHOD,
            requestData,
            baseRef,
            appToken,
            userState.token,
            (json) => {
              console.log("Edit Holdings API response -->", json);
              this.setState(
                {
                  isLoading: false,
                },
                () => {
                  this.getHoldingDetails();
                  setTimeout(() => {
                    this.refs.toast.show(json.head.statusDescription);
                  }, 300);
                }
              );
            },
            (message) => {
              this.setState({
                isLoading: false,
              });
              console.log("SELL HOLDING API Message -->", message);
              setTimeout(() => {
                this.refs.toast.show(message);
              }, 300);
              // this.getHoldingDetails();
            }
          );
        },
        (message) => {
          this.setState({
            isLoading: false,
          });
          console.log("SELL HOLDING API Message -->", message);
          setTimeout(() => {
            this.refs.toast.show(message);
          }, 300);
        }
      );
    } else if (actiontype === "sell") {
      const { baseRef, userState, baseURL, appToken } = this.props;
      const URL = baseURL + PORTFOLIO_HOLDINGS_SELL_URL;
      const { selectedPortFolio, buyDate, sellDate, sellPrice, holdingId, sellQty, stockId, buyPrice } = this.state;

      let requestData = {
        portfolioId: selectedPortFolio[0] ? selectedPortFolio[0] : "",
        stock_id: stockId ? parseInt(stockId) : "",
        qty: sellQty ? parseInt(sellQty) : "",
        buyPrice: buyPrice ? parseInt(buyPrice) : "",
        sellPrice: sellPrice ? parseInt(sellPrice) : "",
        holdingId: holdingId,
        openDate: this.convertRequestDateObj(buyDate) ? this.convertRequestDateObj(buyDate) : "",
        closeDate: this.convertRequestDateObj(sellDate) ? this.convertRequestDateObj(sellDate) : "",
        notes: "source:mobile app",
      };
      // console.log("Request Data -->", requestData);

      this.setState({ isLoading: true }, () => {
        fetcher(
          URL,
          POST_METHOD,
          requestData,
          baseRef,
          appToken,
          userState.token,
          (json) => {
            console.log("sell Holdings API response -->", json);
            this.setState({ isLoading: false }, () => {
              let initializingObj = {
                status: false,
                error: "",
              };
              this.setState({
                sellDateStatus: initializingObj,
                quantityStatus: initializingObj,
                sellPriceStatus: initializingObj,
                buyPriceStatus: initializingObj,
              });

              setTimeout(() => {
                this.refs.toast.show(json.head.statusDescription);
              }, 300);
              // this.getHoldingDetails()
            });
          },
          (message) => {
            this.setState({
              isLoading: false,
            });
            console.log("SELL HOLDING API Message -->", message);
            setTimeout(() => {
              this.refs.toast.show(message);
            }, 150);
          }
        );
      });
    }
    this.refs.holdingsActionsPopUp.RBSheet.close();
  };

  handleBottomSheet = (type) => {
    this.refs.holdingsActionsPopUp.RBSheet.open();
    console.log("Done");
  };

  handleSell() {
    const { get_full_name, lastprice_date, qty, stock_id } = this.state.holdingData;
    const minimumSellDate = new Date(lastprice_date);
    this.setState(
      {
        selectingAddtionalDate: false,
        actionText: `Sell FIFO of ${get_full_name ? get_full_name : ""}`,
        minimumSellDate: minimumSellDate,
        availableQty: qty,
        stockId: stock_id,
        selectedPortFolio: ["", "Select a Portfolio"],
        sellDate: {
          date: new Date(),
          formetted: "Select Date",
        },
        sellQty: "",
        quantityStatus: {
          status: false,
          error: "",
        },
        sellPrice: "",
        sellPriceStatus: {
          status: false,
          error: "",
        },
        errorText: "",
        inputFocused: "",
      },
      () => {
        this.refs.holdingsActionsPopUp.RBSheet.open();
      }
    );
  }

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portFolioData, updatePortfolio } = this.props;
    let newPortfolioList = portFolioData.portfolio;
    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  handlePopUpHeight = () => {
    let height = hp(60);
    const { actionText } = this.state;
    if (actionText.toLowerCase().includes("delete holdings")) {
      height = hp(35);
    } else if (actionText.toLowerCase().includes("sell holdings")) {
      height = hp(80);
    }
    return height;
  };

  render() {
    const { isLoading, holdingsListByStocks, holdingObj, showScrollToTop } = this.state;
    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <View style={[styles.SectionThird]}>
          {isLoading ? (
            <View style={[styles.LoadingView, {}]}>
              <Text style={{ fontSize: RF(1.9) }}>{"Loading details..."}</Text>
            </View>
          ) : (
            <FlatList
              ref={(ref) => (this.listRef = ref)}
              showsVerticalScrollIndicator={false}
              data={holdingsListByStocks}
              extraData={{ ...this.props, holdingsListByStocks, isLoading }}
              renderItem={({ item, index }) => this.renderMoreDetailsCard(item, index)}
              keyExtractor={(item, index) => index.toString()}
              ListHeaderComponent={this.renderListHeader}
              onScroll={this.handleScroll}
            />
          )}
        </View>
        <StockAddBottomSheet
          ref="holdingsActionsPopUp"
          modalMainView={{ height: this.handlePopUpHeight() }}
          actionText={this.state.actionText}
          closeIconPress={() => this.handleCloseButton()}
          children={this.renderHoldindsActionChilds()}
        />
        <StockPortfolioModal
          ref="portfolio"
          {...this.props}
          stock={holdingObj.obj}
          portfolio={this.props.portFolioData.portfolio}
          createNew={() => this.refs.newPortfolio.modal.open()}
          addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
        />
        <CreatePortfolioModal
          ref="newPortfolio"
          {...this.props}
          stock={holdingObj.obj}
          showPortfolioModal={this.showPortfolioModal}
          onUpdate={this.onPortfolioCreated}
        />

        <ScrollToTopView listRef={this.listRef} isVisible={showScrollToTop} />
        <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;
  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user, portFolioData: state.portfolio };
};

export default connect(mapStateToProps, { updatePortfolio })(HoldingsDetailScreen);

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: colors.whiteTwo },
  SectionOne: { flex: 3, paddingHorizontal: wp(3), paddingVertical: wp(2) },
  MarketTypeView: { paddingVertical: wp(1) },
  MarketTypeText: { fontSize: RF(1.9), color: colors.steelGrey },
  StockWithDVMView: { flexDirection: "row", justifyContent: "space-between", paddingVertical: wp(2) },
  StockNameView: { flex: 8 },
  StockNameText: { fontSize: RF(2.4), fontWeight: "500", color: colors.primary_color },
  DVMView: { flex: 2, justifyContent: "center", alignItems: "center", flexDirection: "row" },
  DaysGainRowView: { flexDirection: "row", justifyContent: "flex-start", paddingVertical: wp(2), alignItems: "center" },
  ButtonView: { paddingVertical: wp(3), flexDirection: "row" },
  DaysGainText: { fontSize: RF(2.2), color: colors.black },
  currPriceText: { fontSize: RF(2.8), color: colors.black, marginLeft: wp(2), fontWeight: "500" },
  SectionTwo: { flex: 1.5, marginTop: wp(4), marginVertical: wp(2) },
  HeadingTag: { flex: 2, justifyContent: "flex-start", paddingHorizontal: wp(3) },
  ChipsView: { flex: 2, marginVertical: wp(2.5) },
  SectionThird: { flex: 6 },
  LoadingView: { justifyContent: "center", flex: 1, alignItems: "center" },
  CardButtonRow: { flexDirection: "row", justifyContent: "space-between", paddingTop: wp(3) },
  CardButtonMainView: { paddingLeft: wp(6), paddingRight: wp(8) },
  inputContainer2: {
    paddingBottom: wp(1),
    marginBottom: wp(3),
    borderBottomWidth: 1,
    borderBottomColor: colors.steelGrey,
  },
  activeTextBox: { borderBottomWidth: 2, borderBottomColor: colors.primary_color },
  inputLabel: { fontSize: RF(1.9), color: colors.black },
  redText: { color: colors.red },
  inputIcon: { width: wp(5), height: wp(5) },
  inputRow: { flexDirection: "row", paddingTop: wp(2), paddingBottom: wp(0.8) },
  actionButtonRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: wp(2),
    marginBottom: wp(5),
  },
  button: { marginHorizontal: 0, flex: 2, justifyContent: "center", alignItems: "center", paddingVertical: wp(2.5) },
  list: {
    backgroundColor: colors.white,
    borderRadius: 5,
    shadowColor: "rgba(0, 0, 0, 0.3)",
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
    zIndex: 99,
  },
  listItem: { padding: wp(4), color: colors.greyishBrown, fontSize: RF(2.2) },
  deleteWatchlistButtonContainer: { flexDirection: "row", justifyContent: "space-between" },
  priceChangeIcon: { justifyContent: "center", alignItems: "center", marginLeft: wp(2), flexDirection: "row" },
});
