import React from "react";
import { View, Text, FlatList, StyleSheet, TouchableOpacity, RefreshControl } from "react-native";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import RF from "react-native-responsive-fontsize";
import { colors } from "../../styles/CommonStyles";
import LoadingView from "../../components/common/LoadingView";
import StockNewsItem from "../../components/stockDetail/news/StockNewsItem";
import { connect } from "react-redux";
import LoadingMoreView from "../../components/common/LoadingMoreView";
import { getTrendingNews } from "../../controllers/MarketController";
import ScrollToTopView from "../../components/common/ScrollToTopView";

class TrendingNewsScreen extends React.PureComponent {
  state = {
    news: [],
    nextPage: 1,
    nextLevel: 1,
    isNextPage: true,
    refreshing: false,
    isFirstCallComplete: false,
    qsTime: "",
    isLoadingMore: false,
    showScrollToTop: false,
  };

  componentWillReceiveProps(nextProps) {
    if (nextProps.isSelected != this.props.isSelected) {
      this.showScrollToTop = false;
      this.setState({ showScrollToTop: false });
    }
  }

  getData = () => {
    const {
      news,
      nextPage,
      nextLevel,
      isNextPage,
      refreshing,
      qsTime,
      isLoadingMore,
      isFirstCallComplete,
    } = this.state;
    const { baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    if (isNextPage && (refreshing || isLoadingMore || !isFirstCallComplete)) {
      getTrendingNews(baseURL, appToken, token, nextPage, nextLevel, qsTime, (data) => {
        this.setState({ news: refreshing ? data.news : news.concat(data.news) });

        this.setState({
          qsTime: data.qsTime,
          nextPage: data.nextPage,
          nextLevel: data.nextLevel,
          isNextPage: data.isNextPage,
          // refreshing: false,
          // isLoadingMore: false
        });

        if (data.news.length == 0 && isNextPage) {
          this.getData();
        } else this.setState({ refreshing: false, isLoadingMore: false });

        if (!isFirstCallComplete) this.setState({ isFirstCallComplete: true });
      });
    } else {
      this.setState({ refreshing: false, isLoadingMore: false });
    }
  };

  refreshData = () => {
    this.setState({ nextPage: 1, nextLevel: 1, qsTime: "", refreshing: true }, () => this.getData());
  };

  handleScroll = (event) => {
    if (event) {
      if (!this.showScrollToTop && event.nativeEvent.contentOffset.y > hp(85)) {
        this.showScrollToTop = true;
        this.setState({ showScrollToTop: true });
      } else if (this.showScrollToTop && event.nativeEvent.contentOffset.y < hp(85)) {
        this.showScrollToTop = false;
        this.setState({ showScrollToTop: false });
      }
    }
  };

  onLoadMore = () => {
    const { isLoadingMore, isNextPage } = this.state;
    if (!isLoadingMore && isNextPage) this.setState({ isLoadingMore: true }, () => this.getData());
  };

  renderRefreshControl = () => {
    const { refreshing } = this.state;
    return <RefreshControl refreshing={refreshing} onRefresh={() => this.refreshData()} />;
  };

  renderNewsList = () => {
    const { news, isLoadingMore } = this.state;
    // console.log("news", news);

    return (
      <FlatList
        ref={(ref) => (this.newsListRef = ref)}
        data={news}
        contentContainerStyle={styles.list}
        showsVerticalScrollIndicator={false}
        renderItem={({ item, index }) => <StockNewsItem {...this.props} data={item} showStock />}
        ItemSeparatorComponent={() => <View style={styles.divider} />}
        refreshControl={this.renderRefreshControl()}
        onEndReached={this.onLoadMore}
        onEndReachedThreshold={0.7}
        removeClippedSubviews
        initialNumToRender={4}
        maxToRenderPerBatch={4}
        ListFooterComponent={() => isLoadingMore && <LoadingMoreView />}
        onScroll={this.handleScroll}
      />
    );
  };

  render() {
    const { isSelected } = this.props;
    const { isFirstCallComplete, newsListRef, showScrollToTop } = this.state;

    if (isSelected) {
      return (
        <View style={styles.container}>
          {this.renderNewsList()}

          <ScrollToTopView listRef={this.newsListRef} isVisible={showScrollToTop} />
          {!isFirstCallComplete && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return { baseURL, appToken, userState: state.user };
};

export default connect(mapStateToProps, null, null, { forwardRef: true })(TrendingNewsScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(4) : hp(1),
    backgroundColor: colors.white,
  },
  divider: {
    flex: 1,
    height: 1,
    opacity: 0.2,
    marginHorizontal: wp(3),
    backgroundColor: colors.greyish,
  },
});
