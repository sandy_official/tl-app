import React from "react";
import { View, StyleSheet } from "react-native";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { colors, globalStyles } from "../../styles/CommonStyles";
import MarketStockList from "../../components/markets/marketListing/MarketStockList";
import BaseComponent from "../../components/common/BaseComponent";
import TabComponent from "../../components/common/TabComponent";

export default class MarketListingScreen extends React.PureComponent {
  tabs = ["NIFTY 500", "BSE 200", "NIFTY 50", "Watchlist", "Portfolio"];
  viewpagerRef = null;

  constructor(props) {
    super(props);
    let tabIndex = props.navigation.getParam("tabIndex", 0);

    this.state = {
      viewpagerRef: null,
      selectedTab: tabIndex,
      baseRef: null,
      isApiCalled: [false, false, false, false, false],
    };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [false, false, false, false, false] }, () =>
      this.setTabSelection({ position: selectedTab })
    );
  };

  setTabSelection = (obj) => {
    const { isApiCalled } = this.state;

    const tabPosition = obj.position;
    this.setState({ selectedTab: tabPosition });

    // console.log("tabs", tabPosition);

    if (!isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          this.refs.nifty500.getData();
          break;

        case 1:
          this.refs.bse200.getData();
          break;

        case 2:
          this.refs.nifty50.getData();
          break;

        case 3:
          this.refs.watchlist.getData();
          break;

        case 4:
          this.refs.portfolio.getData();
          break;

        default:
          null;
      }

      let apiCalls = this.state.isApiCalled;
      apiCalls[tabPosition] = true;
      this.setState({ isApiCalled: apiCalls });
    }
  };

  renderToolbar = () => {
    const { navigation } = this.props;
    const screener = navigation.getParam("screener", "");

    return (
      <Toolbar title={screener} backButton {...this.props}>
        <ImageButton
          source={require("../../assets/icons/search-white.png")}
          onPress={() => navigation.push("StockSearch")}
        />
      </Toolbar>
    );
  };
  getAnalyticsParams = () => {
    return {};
  };
  renderTabScreen = () => {
    const { selectedTab, baseRef } = this.state;
    const { navigation } = this.props;

    const pk = navigation.getParam("pk", "");

    return (
      <TabComponent
        analyticsParams={this.getAnalyticsParams}
        tabs={this.tabs}
        initialTab={selectedTab}
        onTabSelect={this.setTabSelection}
      >
        <View>
          <MarketStockList
            ref="nifty500"
            {...this.props}
            baseRef={baseRef}
            pk={pk}
            groupName={"NIFTY500"}
            groupType={"index"}
            label="NIFTY 500"
            isSelected={selectedTab == 0}
          />
        </View>
        <View>
          <MarketStockList
            ref="bse200"
            {...this.props}
            baseRef={baseRef}
            pk={pk}
            groupName={"BSE200"}
            groupType={"index"}
            label="BSE 200"
            isSelected={selectedTab == 1}
          />
        </View>
        <View>
          <MarketStockList
            ref="nifty50"
            {...this.props}
            baseRef={baseRef}
            pk={pk}
            groupName={"NIFTY50"}
            groupType={"index"}
            label="NIFTY 50"
            isSelected={selectedTab == 2}
          />
        </View>
        <View>
          <MarketStockList
            ref="watchlist"
            {...this.props}
            baseRef={baseRef}
            pk={pk}
            groupName={"following"}
            groupType={"user"}
            label="Watchlist"
            isSelected={selectedTab == 3}
          />
        </View>
        <View>
          <MarketStockList
            ref="portfolio"
            {...this.props}
            baseRef={baseRef}
            pk={pk}
            groupName={"portfolio"}
            groupType={"user"}
            label="Portfolio"
            isSelected={selectedTab == 4}
          />
        </View>
      </TabComponent>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderToolbar()}

        <BaseComponent
          ref={(ref) => this.setState({ baseRef: ref })}
          refreshScreen={this.refreshScreen}
          {...this.props}
        >
          {this.renderTabScreen()}
        </BaseComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.iceBlue,
  },
});
