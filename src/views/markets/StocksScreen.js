import React from "react";
import { View, StyleSheet, FlatList, ScrollView, RefreshControl } from "react-native";
import StockIndex from "../../components/markets/stocks/StockIndex";
import { colors, globalStyles } from "../../styles/CommonStyles";
import StockList from "../../components/markets/stocks/StockList";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { ifIphoneX } from "react-native-iphone-x-helper";
import { getStocksCardData } from "../../controllers/MarketController";
import Toast from "react-native-easy-toast";
import LoadingView from "../../components/common/LoadingView";

import { connect } from "react-redux";
import { setActiveWatchlist, updateWatchlist } from "../../actions/WatchlistActions";
import CreateWatchlistModal from "../../components/common/CreateWatchlistModal";
import WatchlistModal from "../../components/common/WatchlistModal";
import StockPortfolioModal from "../../components/common/StockPortfolioModal";
import CreatePortfolioModal from "../../components/common/CreatePortfolioModal";
import { updatePortfolio, loadPortfolio } from "../../actions/PortfolioActions";
import LoginModal from "../../components/common/LoginModal";
import { loadIndexData, loadIndicesCards } from "../../actions/MarketActions";

class StocksScreen extends React.PureComponent {
  componentDidMount() {
    const { loadIndexData, loadIndicesCards, market, baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    loadIndexData(baseURL, appToken, token);
    getStocksCardData(baseURL, appToken, token, "indicesdata", this.setIndicesData);

    // this.setIndicesData(market.indices);
    // loadIndicesCards(baseURL, token, "indicesdata", this.setIndicesData);
  }

  constructor(props) {
    super(props);
    this.state = {
      data: [],
      pkList: [],
      niftySensex: [],
      groupNames: [],
      groupTypes: [],
      refreshing: false,
      isFirstCallComplete: false,
      selectedStock: {},
      stockInWatchlist: false,
    };
  }

  onRefresh = () => {
    const { loadIndicesCards, baseURL, appToken } = this.props;
    const { token } = this.props.userState;

    this.setState({ refreshing: true });
    loadIndicesCards(baseURL, appToken, token, "indicesdata", this.setIndicesData);
  };

  // getStockIndexData = () => {
  //   const { baseURL,appToken } = this.props;
  //   const { token } = this.props.userState;

  //   // this.setState({ refreshing: true });

  //   // getNiftySensexPoints(baseURL,appToken,token, data => {
  //   //   this.setState({ niftySensex: data.reverse() });
  //   // });

  //   loadIndexData(baseURL, token);
  // };

  setIndicesData = (data) => {
    const { baseURL, appToken } = this.props;
    const { token, isGuestUser } = this.props.userState;

    if ("stockList" in data && data.stockList.length > 0) {
      let groupNames = [];
      let groupTypes = [];

      data.stockList[0].forEach((item) => {
        groupTypes.push(item.groupType);
        groupNames.push(item.groupName);
      });

      this.setState({ data: data.stockList });
      this.setState({ pkList: data.pkList, groupNames: groupNames, groupTypes: groupTypes, refreshing: false });
    }

    !isGuestUser && getStocksCardData(baseURL, appToken, token, "userdata", this.setUserData);

    if (!this.state.isFirstCallComplete) this.setState({ isFirstCallComplete: true });
    this.setState({ refreshing: false });
  };

  setUserData = (data) => {
    if (data.stockList && data.stockList.length > 0) {
      let groupNames = [];
      let groupTypes = [];
      let newData = [];

      if (this.state.data.length > 0) {
        this.state.data.forEach((oldList, i) => {
          data.stockList[i].forEach((item) => oldList.push(item));
          newData.push(oldList);
        });
      } else newData = data.stockList;

      newData[0].forEach((item) => {
        groupTypes.push(item.groupType);
        groupNames.push(item.groupName);
      });

      setTimeout(() => {
        this.setState({ data: newData, groupNames: groupNames, groupTypes: groupTypes });
      }, 500);
    }
  };

  showLogin = () => {
    this.refs.loginModal.open();
  };

  onPortfolioCreated = ({ portfolioName, portfolioId }) => {
    const { portfolio, updatePortfolio } = this.props;
    let newPortfolioList = portfolio;

    newPortfolioList.push([portfolioId, portfolioName]);
    updatePortfolio(newPortfolioList);
  };

  onWatchlistCreated = ({ newWatchlist, newActiveWatchlist }) => {
    const { updateWatchlist, setActiveWatchlist } = this.props;

    updateWatchlist(newWatchlist);
    setActiveWatchlist(newActiveWatchlist);
    this.refs.Watchlists.showWatchlistAdded();
  };

  showWatchlist = (stock) => {
    const { userState } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.Watchlists.open());
    } else this.refs.loginModal.open();
  };

  showPortfolioModal = () => {
    const { isGuestUser } = this.props.userState;

    if (!isGuestUser) {
      this.refs.portfolio.modal.open();
      this.refs.portfolio.showPortfolioAdded();
    } else this.refs.loginModal.open();
  };

  showPortfolio = (stock) => {
    const { loadPortfolio, userState, baseURL, appToken } = this.props;

    if (!userState.isGuestUser) {
      this.setState({ selectedStock: stock }, () => this.refs.portfolio.modal.open());
      loadPortfolio(baseURL, appToken, userState.token);
    } else this.refs.loginModal.open();
  };

  renderListItem = ({ item, index }) => {
    const { activeWatchlist } = this.props;
    let data = item;

    if (item.length == 3) {
      let watchlist = { ...item[0], cardName: "watchlist" };
      let portfolio = { ...item[1], cardName: "portfolio" };

      data = [...item, watchlist, portfolio];
    }

    return (
      <StockList
        {...this.props}
        parentIndex={index}
        data={data}
        groupNames={this.state.groupNames}
        groupTypes={this.state.groupTypes}
        // update={(parentIndex, childIndex) => this.updateCardData(parentIndex, childIndex)}
        showWatchlist={this.showWatchlist}
        showPortfolio={this.showPortfolioModal}
        watchlist={activeWatchlist}
        showLogin={this.showLogin}
      />
    );
  };

  extractItemKey = (item, index) => index.toString();

  render() {
    const { watchlist, activeWatchlist, setActiveWatchlist, isSelected, market } = this.props;
    const { selectedStock, portfolio, niftySensex, data, isFirstCallComplete } = this.state;

    const showIndex = market.niftySensex.length > 0;
    const showLoader = data.length == 0 && !isFirstCallComplete;
    // console.log("showLoader", data.length, !isFirstCallComplete, showLoader);

    if (isSelected) {
      return (
        <View style={styles.container}>
          <ScrollView
            showsVerticalScrollIndicator={false}
            refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this.onRefresh} />}
            removeClippedSubviews
          >
            {showIndex && <StockIndex {...this.props} data={market.niftySensex} />}
            {this.state.data.length > 0 && (
              <FlatList
                horizontal={false}
                contentContainerStyle={styles.list}
                showsVerticalScrollIndicator={false}
                data={this.state.data}
                extraData={this.props}
                renderItem={this.renderListItem}
                keyExtractor={this.extractItemKey}
                removeClippedSubviews
                initialNumToRender={2}
                maxToRenderPerBatch={2}
              />
            )}
          </ScrollView>

          <WatchlistModal
            ref="Watchlists"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={(obj) => setActiveWatchlist(obj)}
            createWatchList={() => this.refs.createWatchlist.modal.open()}
          />

          <CreateWatchlistModal
            ref="createWatchlist"
            {...this.props}
            stock={selectedStock}
            watchlist={watchlist}
            activeWatchlist={activeWatchlist}
            onUpdate={this.onWatchlistCreated}
            openWatchlist={() => this.refs.Watchlists.open()}
          />

          <StockPortfolioModal
            ref="portfolio"
            {...this.props}
            stock={selectedStock}
            portfolio={portfolio}
            createNew={() => this.refs.newPortfolio.modal.open()}
            addedTo={() => this.refs.toast.show("Added to Portfolio", 1000)}
          />

          <CreatePortfolioModal
            ref="newPortfolio"
            {...this.props}
            stock={selectedStock}
            showPortfolioModal={this.showPortfolioModal}
            onUpdate={this.onPortfolioCreated}
          />

          <LoginModal ref="loginModal" {...this.props} />

          <Toast ref="toast" position="bottom" style={globalStyles.toast} textStyle={globalStyles.toastText} />
          {showLoader && <LoadingView />}
        </View>
      );
    } else return null;
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    watchlist: state.watchlist.watchlist,
    activeWatchlist: state.watchlist.activeWatchlist,
    portfolio: state.portfolio.portfolio,
    market: state.market,
  };
};

export default connect(
  mapStateToProps,
  {
    setActiveWatchlist,
    updateWatchlist,
    updatePortfolio,
    loadPortfolio,
    loadIndexData,
    loadIndicesCards,
  },
  null,
  { forwardRef: true }
)(StocksScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  list: {
    paddingBottom: ifIphoneX ? hp(3) : hp(1),
  },
});
