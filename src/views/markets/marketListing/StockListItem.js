import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { PRIMARY_COLOR, colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp
} from "react-native-responsive-screen";
import { getDVMBackgroundColor, numberWithCommas } from "../../../utils/Utils";
import { ICON_CDN } from "../../../utils/Constants";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockListItem extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { isAdded: props.WatchlistBtnState };
  }

  gotoStockDetails = () => {
    const { navigation, data } = this.props;
    console.log("market stock", data);
    const stockCode = data.NSEcode ? data.NSEcode : data.BSEcode;

    navigation.push("Stocks", {
      stockId: data.stock_id,
      stockName: data.full_name,
      pageType: data.page_type,
      stockCode: stockCode,
    });
  };

  renderStockNameCode = () => {
    const { data } = this.props;

    return (
      <TouchableOpacity
        style={styles.nameCodeContainer}
        onPress={this.gotoStockDetails}
      >
        <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
          {data.NSEcode ? data.NSEcode : data.BSECode}
        </Text>
        <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
          {data.full_name}
        </Text>
      </TouchableOpacity>
    );
  };

  renderDvmPrice = () => {
    const { data } = this.props;
    const currentPrice = data.currentPrice
      ? numberWithCommas(data.currentPrice, true)
      : data.currentPrice;

    return (
      <View style={styles.priceContainer}>
        <Text style={styles.price}>{currentPrice}</Text>
        <View style={styles.row}>
          <View style={getDVMBackgroundColor(data.d_color)} />
          <View style={getDVMBackgroundColor(data.v_color)} />
          <View style={getDVMBackgroundColor(data.m_color)} />
        </View>
      </View>
    );
  };

  showWatchList = () => {
    const { data, showWatchlist } = this.props;
    showWatchlist(data);
    // this.setState({ isAdded: !this.state.isAdded });
  };

  renderAddToWatchlist = () => {
    const { WatchlistBtnState } = this.props;
    const icon = WatchlistBtnState
      ? require("../../../assets/icons/check-mark-blue.png")
      : require("../../../assets/icons/add-black.png");

    return (
      <TouchableOpacity
        style={styles.addIconContainer}
        onPress={() => this.showWatchList()}
      >
        <Image source={icon} style={styles.addIcon} />
      </TouchableOpacity>
    );
  };

  render() {
    const { data } = this.props;

    const day_changeP = APIDataFormatter("day_changeP", data.day_changeP);
    const dayChangePrice = APIDataFormatter("day_change", data.day_change);
    const dayVolume = APIDataFormatter(
      "vol_day_times_vol_week_str",
      data.vol_day_times_vol_week_str
    );

    const changeBoxStyle =
      data.day_changeP > 0
        ? styles.changeBox
        : [styles.changeBox, styles.redBackground];

    return (
      <View style={styles.container}>
        {this.renderStockNameCode()}

        {this.renderDvmPrice()}

        <View style={styles.stockChangeContainer}>
          <View style={changeBoxStyle}>
            <Text style={styles.changeInPercent}>{day_changeP}</Text>
            {!isNaN(data.day_change) ? (
              <Text style={styles.changeInPrice}>{dayChangePrice}</Text>
            ) : (
              <Text style={styles.changeInPrice}>{dayVolume}</Text>
            )}
          </View>
        </View>

        {this.renderAddToWatchlist()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(1.5),
    paddingHorizontal: wp(3)
  },
  nameCodeContainer: {
    flex: 2,
    paddingRight: wp(3)
  },
  priceContainer: {
    flex: 1.3,
    alignItems: "flex-end",
    marginRight: wp(7)
  },
  stockChangeContainer: {
    flex: 1.6,
    alignItems: "center"
  },
  addIconContainer: {
    flex: 0.5,
    alignItems: "flex-end",
    justifyContent: "center"
  },
  code: {
    color: colors.lightNavy,
    fontSize: RF(2.2),
    fontWeight: "400",
    marginTop: 2
  },
  name: {
    fontSize: RF(1.9),
    marginTop: hp(0.5),
    color: colors.greyishBrown,
    opacity: 0.85
  },
  price: {
    fontSize: RF(2.2),
    fontWeight: "500"
    // marginBottom: 4,
    // marginLeft: 4
    // marginRight: wp(8),
    // textAlign: "right"
  },
  row: {
    flexDirection: "row",
    marginTop: 8
  },
  colorBlock: {
    width: wp(2),
    height: wp(2),
    backgroundColor: "green",
    margin: 3,
    borderRadius: 2
  },
  changeInPercent: {
    color: "#fff",
    fontSize: RF(2.2),
    fontWeight: "500"
  },
  changeInPrice: {
    color: "#fff",
    fontSize: RF(2),
    marginTop: 2.3,
    opacity: 0.95
  },
  changeBox: {
    minWidth: wp(22),
    padding: wp(2),
    paddingVertical: 6,
    paddingRight: 6,
    paddingLeft: 10,
    backgroundColor: colors.green,
    justifyContent: "center",
    borderRadius: 5
  },
  // changeBox: {
  //   width: wp(23),
  //   paddingVertical: 6,
  //   paddingRight: 6,
  //   paddingLeft: 10,
  //   backgroundColor: colors.green,
  //   justifyContent: "center",
  //   borderRadius: 5
  // },
  stockChangeSinceAdded: {
    fontSize: RF(1.6),
    marginTop: 8
  },
  greenText: {
    color: colors.green
  },
  redText: {
    color: colors.red
  },
  greyText: {
    color: colors.greyishBrown
  },
  addIcon: {
    width: wp(5),
    height: wp(5),
    padding: 8
  },
  redBackground: {
    backgroundColor: colors.red
  }
});
