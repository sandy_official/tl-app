import React from "react";
import { View, Text, StyleSheet, FlatList, TouchableOpacity, Platform } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import StockCardItem from "./StockCardItem";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";

export default class StockListCard extends React.PureComponent {
  gotoMarketListing = () => {
    const { groupNames, pk, screener, tabIndex, navigation } = this.props;
    navigation.navigate("MarketListing", {
      groupNames: groupNames,
      pk: pk,
      tabIndex: tabIndex,
      screener: screener
    });
  };

  renderListItem = ({ item, index }) => {
    // if(index == 0)console.log(item)
    if (index < 3) return <StockCardItem {...this.props} stock={item} />;
  };

  extractItemKey = (item, index) => index.toString();

  render() {
    const { cardTitle, stockList } = this.props;

    return (
      <View style={styles.container}>
        <View style={styles.row}>
          <Text style={styles.label}>{cardTitle ? cardTitle : ""}</Text>

          <TouchableOpacity onPress={this.gotoMarketListing}>
            <Text style={styles.viewAll}>View all</Text>
          </TouchableOpacity>
        </View>

        {stockList && (
          <FlatList
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            removeClippedSubviews={false}
            data={stockList}
            extraData={stockList}
            renderItem={this.renderListItem}
            keyExtractor={this.extractItemKey}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
    padding: wp(3),
    borderRadius: 5,
    ...Platform.select({
      ios: {
        shadowColor: "rgba(0, 0, 0, 0.1)",
        shadowOffset: {
          width: 0,
          height: -1
        },
        shadowRadius: 5,
        shadowOpacity: 1
      },
      android: {
        elevation: 2
      }
    }),
    backgroundColor: colors.white
    // shadowColor: "rgba(0, 0, 0, 0.1)",
    // shadowOffset: {
    //   width: 0,
    //   height: -1
    // },
    // shadowRadius: 5,
    // shadowOpacity: 1,
    // elevation: 2
    // borderColor: '#000',
    // borderWidth: 1,
  },
  label: {
    flex: 1,
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black
  },
  row: {
    flexDirection: "row",
    alignItems: "center"
  },
  viewAll: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color
  }
});
