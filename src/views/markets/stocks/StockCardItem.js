import React from "react";
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import { PRIMARY_COLOR, colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { getDVMBackgroundColor, numberWithCommas, roundNumber } from "../../../utils/Utils";
import { APIDataFormatter } from "../../../utils/APIDataFormatter";

export default class StockCardItem extends React.PureComponent {
  gotoStockDetails = () => {
    const { navigation, stock } = this.props;
    console.log("stock", stock);
    const stockCode = stock.NSEcode ? stock.NSEcode : stock.BSEcode;
    
    navigation.push("Stocks", {
      stockId: stock.stock_id,
      stockName: stock.full_name,
      pageType: stock.page_type,
      stockCode: stockCode,
    });
  };

  renderPriceDvm = () => {
    const { stock } = this.props;
    const currentPrice = stock.currentPrice ? numberWithCommas(stock.currentPrice, true) : stock.currentPrice;
    currentPrice = roundNumber(stock.currentPrice);
    return (
      <View style={styles.priceContainer}>
        <Text style={styles.price} numberOfLines={1}>
          {currentPrice}
        </Text>
        <View style={styles.row}>
          <View style={getDVMBackgroundColor(stock.d_color)} />
          <View style={getDVMBackgroundColor(stock.v_color)} />
          <View style={getDVMBackgroundColor(stock.m_color)} />
        </View>
      </View>
    );
  };

  renderStockCodeName = () => {
    const { stock } = this.props;

    return (
      <TouchableOpacity style={styles.nameCodeContainer} onPress={this.gotoStockDetails}>
        <Text style={styles.code} numberOfLines={1} ellipsizeMode="tail">
          {stock.NSEcode ? stock.NSEcode : stock.BSEcode}
        </Text>
        <Text style={styles.name} numberOfLines={1} ellipsizeMode="tail">
          {stock.full_name}
        </Text>
        {/* <Text
            style={[styles.stockChangeSinceAdded,styles.greenText]}
            numberOfLines={1}
            ellipsizeMode="tail"
          >
            +10.5% <Text style={styles.greyText}>since added</Text>
          </Text> */}
      </TouchableOpacity>
    );
  };

  renderPriceChange = () => {
    const { stock } = this.props;

    const dayChange = APIDataFormatter("day_change", stock.day_change);
    const dayChangePercent = APIDataFormatter("day_changeP", stock.day_changeP);
    const volume = APIDataFormatter("vol_day_times_vol_week_str", stock.vol_day_times_vol_week_str);

    const showVolume = "vol_day_times_vol_week_str" in stock;

    return (
      <View style={styles.stockChangeContainer}>
        <View style={stock.day_changeP >= 0 ? styles.changeBox : [styles.changeBox, styles.redBackground]}>
          <Text style={styles.changeInPercent} numberOfLines={1}>
            {dayChangePercent}
          </Text>

          {showVolume ? (
            <Text style={styles.changeInPrice} numberOfLines={1}>
              {volume}
            </Text>
          ) : (
            <Text style={styles.changeInPrice} numberOfLines={1}>
              {dayChange}
            </Text>
          )}
        </View>
      </View>
    );
  };

  render() {
    return (
      <View style={styles.container}>
        {this.renderStockCodeName()}
        {this.renderPriceDvm()}
        {this.renderPriceChange()}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "row",
    overflow: "hidden",
    justifyContent: "center",
    alignItems: "center",
    marginTop: hp(1.5),
  },
  nameCodeContainer: {
    flex: 2,
    paddingRight: 18,
    justifyContent: "center",
  },
  priceContainer: {
    flex: 1.2,
    alignItems: "flex-end",
    marginRight: wp(10),
  },
  stockChangeContainer: {
    flex: 1.4,
    alignItems: "flex-end",
  },
  code: {
    color: colors.lightNavy,
    fontSize: RF(2.2),
    fontWeight: "500",
  },
  name: {
    fontSize: RF(1.9),
    marginTop: 6,
    color: colors.greyishBrown,
    opacity: 0.85,
  },
  price: {
    fontSize: RF(2.2),
    fontWeight: "500",
    // marginBottom: 4,
    color: colors.black,
  },
  row: {
    flexDirection: "row",
    marginTop: 8,
    marginBottom: 2,
  },
  // colorBlock: {
  //   width: wp(2),
  //   height: wp(2),
  //   backgroundColor: "green",
  //   margin: 3,
  //   borderRadius: 2
  // },
  changeInPercent: {
    color: "#fff",
    fontSize: RF(2.2),
    fontWeight: "500",
  },
  changeInPrice: {
    color: "#fff",
    fontSize: RF(2),
    marginTop: 5,
    opacity: 0.95,
  },
  changeBox: {
    width: wp(23),
    paddingVertical: 6,
    paddingRight: 6,
    paddingLeft: 10,
    backgroundColor: colors.green,
    justifyContent: "center",
    borderRadius: 5,
  },
  stockChangeSinceAdded: {
    fontSize: RF(1.6),
    marginTop: 8,
  },
  greenText: {
    color: colors.green,
  },
  redText: {
    color: colors.red,
  },
  greyText: {
    color: colors.greyishBrown,
  },
  redBackground: {
    backgroundColor: colors.red,
  },
});
