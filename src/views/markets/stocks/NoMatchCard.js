import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image } from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";

export default class NoMatchCard extends React.PureComponent {
  render() {
    const { item, groupNames, tabIndex } = this.props;
    const { navigate } = this.props.navigation;

    return (
      <View style={styles.container}>
        <Text style={styles.label}>{item.cardName}</Text>

        <View style={styles.content}>
          <Image source={require("../../../assets/icons/search-error-black.png")} style={styles.searchErrorIcon} />
          <Text style={styles.noMatch}>No matches in {item.screen.title}</Text>

          <TouchableOpacity
            onPress={() =>
              navigate("MarketListing", {
                groupNames: groupNames,
                pk: item.screen.pk,
                tabIndex: tabIndex,
                screener: item.screen.title,
              })
            }
          >
            {/* <Text style={styles.viewAll}>View all {item.screen.title}</Text> */}
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    minHeight: hp(30),
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
    padding: wp(3),
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: -1,
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2,
  },
  label: {
    fontSize: RF(2.2),
    fontWeight: "500",
    color: colors.black,
  },
  content: {
    flex: 1,
    marginTop: hp(4),
    // justifyContent: "center",
    alignItems: "center",
  },
  searchErrorIcon: {
    width: wp(12),
    height: hp(10),
    marginTop: hp(1),
    resizeMode: "contain",
  },
  noMatch: {
    fontSize: RF(2.4),
    fontWeight: "500",
    color: colors.greyishBrown,
    marginTop: hp(1),
    textAlign: "center",
  },
  viewAll: {
    fontSize: RF(2),
    fontWeight: "500",
    color: colors.primary_color,
    marginTop: hp(3),
  },
});
