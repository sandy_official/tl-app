import React from "react";
import { View, Text, StyleSheet, FlatList, ScrollView } from "react-native";
import StockListCard from "./StockListCard";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from "react-native-responsive-screen";
import NoMatchCard from "./NoMatchCard";
import AddStockList from "./AddStockList";
import StockLoginCard from "./StockLoginCard";

export default class StockList extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = { currentPosition: 0 };
  }

  renderPageIndicator = () => {
    const { data } = this.props;
    const { currentPosition } = this.state;

    return (
      <View style={styles.pageIndicator}>
        {data.map((item, index) => {
          const indicatorStyle = currentPosition == index ? styles.pageIndicatorActive : styles.pageIndicatorInactive;
          return <View style={indicatorStyle} key={index} />;
        })}
      </View>
    );
  };

  _onViewableItemsChanged = ({ viewableItems, changed }) => {
    // const { update } = this.props;
    const { currentPosition } = this.state;

    try {
      let currentIndex = viewableItems[0].index;

      // if (currentPosition != currentIndex) update(this.props.parentIndex, currentIndex);
      this.setState({ currentPosition: currentIndex });
    } catch {
      e => console.log("stocks page indicator ", e);
    }
  };

  _viewabilityConfig = { itemVisiblePercentThreshold: 50 };

  renderListItem = ({ item, index }) => {
    const { data, groupNames, userState } = this.props;
    const isPortfolio = index == data.length - 1;

    if (item.stocks.length == 0) {
      return <NoMatchCard {...this.props} item={item} groupNames={groupNames} tabIndex={index} />;
    } else if (index < data.length - 2) {
      return (
        <StockListCard
          {...this.props}
          cardTitle={item.cardName}
          stockList={item.stocks}
          groupNames={groupNames}
          pk={item.screen.pk}
          screener={item.screen.title}
          tabIndex={index}
        />
      );
    } else if (userState.isGuestUser) {
      return <StockLoginCard {...this.props} label={item.cardName} />;
    } else {
      return (
        <AddStockList
          {...this.props}
          cardTitle={item.cardName}
          stockList={item.stocks}
          tabIndex={index}
          isPortfolio={isPortfolio}
        />
      );
    }
  };

  extractItemKey = (item, index) => index.toString();

  render() {
    const { data } = this.props;
    const listTitle = data[0].screen.title.toUpperCase();

    return (
      <View style={styles.container}>
        {data && (
          <ScrollView>
            <View style={styles.labelContainer}>
              <Text style={styles.listLabel}>{listTitle}</Text>
              {data.length > 1 && this.renderPageIndicator()}
            </View>

            <FlatList
              ref="cardList"
              horizontal={true}
              contentContainerStyle={styles.list}
              showsHorizontalScrollIndicator={false}
              data={data}
              extraData={this.props}
              renderItem={this.renderListItem}
              keyExtractor={this.extractItemKey}
              onViewableItemsChanged={this._onViewableItemsChanged}
              viewabilityConfig={this._viewabilityConfig}
              removeClippedSubviews
              initialNumToRender={2}
            />
          </ScrollView>
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: hp(1)
  },
  labelContainer: {
    margin: "3%",
    flexDirection: "row",
    alignItems: "center"
  },
  listLabel: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black
  },
  list: {
    paddingLeft: wp(1)
    // paddingVertical: 4
  },
  pageIndicator: {
    flexDirection: "row"
  },
  pageIndicatorActive: {
    width: wp(2),
    height: wp(2),
    backgroundColor: colors.lightNavy,
    borderRadius: wp(1),
    margin: 2
  },
  pageIndicatorInactive: {
    width: wp(1.5),
    height: wp(1.5),
    backgroundColor: colors.pinkishGrey,
    borderRadius: wp(0.75),
    margin: 3
  }
});
