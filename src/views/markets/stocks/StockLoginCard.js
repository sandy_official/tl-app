import React from "react";
import { View, Text, StyleSheet, TouchableOpacity,Image} from "react-native";
import { widthPercentageToDP as wp, heightPercentageToDP as hp } from "react-native-responsive-screen";
import { colors } from "../../../styles/CommonStyles";
import RF from "react-native-responsive-fontsize";


export default class StockLoginCard extends React.PureComponent {
  render() {
    const { label, showLogin } = this.props;
    const message = `Sign up to add stocks to your ${label}`;

    return (
      <View style={styles.container}>
        <Image source={require("../../../assets/icons/nounSignUpUser.png")} style={styles.searchErrorIcon} />
        <Text style={styles.message}>{message}</Text>
        <TouchableOpacity style={styles.btn} onPress={() => showLogin()}>
          <Text style={styles.btnText}>Sign up</Text>
        </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    width: wp(90),
    minHeight: hp(30),
    marginHorizontal: wp(1.5),
    marginVertical: wp(1),
    paddingVertical: wp(3),
    alignItems: "center",
    justifyContent: "center",
    borderRadius: 5,
    backgroundColor: colors.white,
    shadowColor: "rgba(0, 0, 0, 0.1)",
    shadowOffset: {
      width: 0,
      height: -1
    },
    shadowRadius: 5,
    shadowOpacity: 1,
    elevation: 2
    // borderColor: '#000',
    // borderWidth: 1,
  },
  message: {
    fontSize: RF(2.2),
    color: colors.black,
    paddingLeft: wp(3),
    marginBottom: hp(3.8)
  },
  btn: {
    width: wp(50),
    padding: wp(2),
    backgroundColor: colors.primary_color,
    borderRadius: 5
  },
  btnText: {
    fontSize: RF(2.4),
    color: colors.white,
    textAlign: "center"
  },
  searchErrorIcon: {
    width: wp(12),
    height: hp(10),
    marginTop: hp(1),
    resizeMode: "contain"
  }
});
