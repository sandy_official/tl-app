import React from "react";
import { View, StyleSheet, Platform } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { TAB_BAR_HEIGHT, colors, globalStyles } from "../../styles/CommonStyles";
import StocksScreen from "./StocksScreen";
import MarketBulkDealsScreen from "./MarketBulkDealsScreen";
import MarketInsiderTradesScreen from "./MarketInsiderTradesScreen";
import TrendingNewsScreen from "./TrendingNewsScreen";
import TabComponent from "../../components/common/TabComponent";

export default class MarketTabScreen extends React.PureComponent {
  tabs = ["Stocks", "Insider Trades & SAST", "Bulk & Block Deals", "Trending News"];

  componentDidMount() {
    const { navigation } = this.props;
    const tabIndex = navigation.getParam("tabIndex", 0);
    this.setState({ selectedTab: tabIndex }, () => this.setTabSelection({ position: tabIndex }));
  }

  constructor(props) {
    super(props);
    let tabIndex = props.navigation.getParam("tabIndex", 0);

    this.state = { selectedTab: tabIndex, isApiCalled: [false, false, false, false] };
  }

  refreshScreen = () => {
    const { selectedTab } = this.state;
    this.setState({ isApiCalled: [false, false, false, false] }, () => this.setTabSelection({ position: selectedTab }));
  };

  setTabSelection = (obj) => {
    const tabPosition = obj.position;
    let validRef = false;

    this.setState({ selectedTab: tabPosition });

    if (!this.state.isApiCalled[tabPosition]) {
      switch (tabPosition) {
        case 0:
          validRef = this.refs.stocks ? true : false;
          // validRef ? this.refs.stocks.getData() : null;
          break;
        case 1:
          validRef = this.refs.insiderTrades ? true : false;
          validRef ? this.refs.insiderTrades.getData() : null;
          break;
        case 2:
          validRef = this.refs.bbDeals ? true : false;
          validRef ? this.refs.bbDeals.getData() : null;
          break;
        case 3:
          validRef = this.refs.news ? true : false;
          validRef ? this.refs.news.getData() : null;
          break;
      }

      if (validRef) {
        let apiCalls = this.state.isApiCalled;
        apiCalls[tabPosition] = true;
        this.setState({ isApiCalled: apiCalls });
      } else {
        // console.log("calling again");
        setTimeout(() => this.setTabSelection({ position: tabPosition }), 200);
      }
    }
  };

  renderToolbar = () => {
    const { navigation } = this.props;

    return (
      <Toolbar title="Markets" drawerMenu {...this.props}>
        <ImageButton
          source={require("../../assets/icons/search-white.png")}
          onPress={() => navigation.push("StockSearch")}
        />
      </Toolbar>
    );
  };

  getAnalyticsParams = () => {
    return {};
  };

  render() {
    const { navigation } = this.props;
    const { selectedTab } = this.state;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <TabComponent
          analyticsParams={this.getAnalyticsParams}
          ref={(ref) => (this.tabComponent = ref)}
          tabs={this.tabs}
          initialTab={selectedTab}
          onTabSelect={this.setTabSelection}
        >
          <View>
            <StocksScreen
              ref="stocks"
              {...this.props}
              isSelected={selectedTab == 0}
              refreshScreen={this.refreshScreen}
            />
          </View>
          <View>
            <MarketInsiderTradesScreen ref="insiderTrades" {...this.props} isSelected={selectedTab == 1} />
          </View>
          <View>
            <MarketBulkDealsScreen ref="bbDeals" {...this.props} isSelected={selectedTab == 2} />
          </View>
          <View>
            <TrendingNewsScreen ref="news" {...this.props} isSelected={selectedTab == 3} />
          </View>
        </TabComponent>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.iceBlue,
  },
  pagerStyle: {
    marginTop: Platform.OS == "ios" ? TAB_BAR_HEIGHT : TAB_BAR_HEIGHT + wp(2),
  },
});
