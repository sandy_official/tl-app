/**
 * Libraries
 */
import React from "react";
import { View, Text, StyleSheet, TouchableOpacity, Image, ScrollView, StatusBar } from "react-native";
import { widthPercentageToDP as wp } from "react-native-responsive-screen";
import RF from "react-native-responsive-fontsize";
import { connect } from "react-redux";

/**
 * Custom components, methods and constants
 */
import { Toolbar, ImageButton } from "../../components/Toolbar";
import { colors } from "../../styles/CommonStyles";
import SubscriptionFeatures from "../../components/subscription/SubscriptionFeatures";
import LoadingView from "../../components/common/LoadingView";
import DashboardNewsList from "../../components/dashboard/DashboardNewsList";
import DashboardMenu from "../../components/dashboard/DashboardMenu";

// Redux actions
import { loadUserDetails, loadUserDashboard } from "../../actions/UserActions";

import { loadWatchlist, getActiveWatchlist } from "../../actions/WatchlistActions";
import { loadPortfolio } from "../../actions/PortfolioActions";
import NoInternetView from "../../components/common/NoInternetView";
import { loadIndicesCards } from "../../actions/MarketActions";
import { setNavigationState } from "../../utils/Constants";
// import firebase from "react-native-firebase";

/**
 * Dashboard screen - this is user dashboard screen
 */
class DashboardScreen extends React.PureComponent {
  componentDidMount = () => {
    this.setNavigationListener();
    this.loadData();
    setNavigationState(null);
    // firebase.crashlytics().crash();
  };

  /**
   * Removing the navigation listener
   */
  componentWillUnmount() {
    !!this.navListener && this.navListener.remove();
  }

  /**
   * @constructor
   */
  constructor(props) {
    super(props);
    this.state = { refreshing: false };
  }

  /**
   *  Setting the navigation listener for changing the statusbar properties
   *  Calling api
   */
  setNavigationListener = () => {
    const {
      loadUserDetails,
      loadUserDashboard,
      userState,
      navigation,
      loadIndicesCards,
      market,
      baseURL,
      appToken,
    } = this.props;
    const { token, isGuestUser } = userState;
    const { indices } = market;

    this.navListener = navigation.addListener("willFocus", (payload) => {
      StatusBar.setBarStyle("light-content");
      StatusBar.setBackgroundColor(colors.primary_color);
      /**
       *  Load data into redux
       */
      loadUserDashboard(baseURL, appToken, token);
      if (!isGuestUser) loadUserDetails(baseURL, appToken, token);
    });

    // if (Object.keys(indices).length > 0) loadIndicesCards(baseURL, token, "indicesdata", () => null);
  };

  loadData = () => {
    const { userState, baseURL, appToken, loadWatchlist, loadPortfolio, getActiveWatchlist } = this.props;
    const { token, isGuestUser } = userState;

    if (!isGuestUser) {
      loadPortfolio(baseURL, appToken, token);
      loadWatchlist(baseURL, appToken, token, "");
      getActiveWatchlist(baseURL, appToken, token);
    }
  };

  gotoNotifications = () => {
    this.props.navigation.navigate({ routeName: "Alerts", params: { tabIndex: 4 } });
  };

  gotoSubscription = () => {
    this.props.navigation.navigate("Subscription");
  };

  renderOffer = () => {
    const { bestOfferLongStr } = this.props.dashboard;
    if (bestOfferLongStr)
      return (
        <TouchableOpacity onPress={this.gotoSubscription} style={{ backgroundColor: "rgba(49,167,69,0.2)" }}>
          <Text style={styles.offer}>{bestOfferLongStr}</Text>
        </TouchableOpacity>
      );
  };

  renderShowPlans = () => {
    const { navigate } = this.props.navigation;

    return (
      <TouchableOpacity style={styles.button} onPress={() => navigate("Subscription")}>
        <Text style={styles.buttonText}>Show Plans</Text>
      </TouchableOpacity>
    );
  };

  renderToolbar = () => {
    const { navigate } = this.props.navigation;
    const logoIcon = require("../../assets/icons/trendlyneLogo.png");
    const searchIcon = require("../../assets/icons/search-white.png");

    const logoStyle = { alignItems: "center", justifyContent: "center", marginRight: wp(3) };

    return (
      <Toolbar
        {...this.props}
        drawerMenu
        customTitle={
          <View style={logoStyle}>
            <Image source={logoIcon} style={styles.logo} />
          </View>
        }
      >
        <ImageButton source={searchIcon} onPress={() => navigate("StockSearch")} />
      </Toolbar>
    );
  };

  renderNews = () => {
    const { insight } = this.props.dashboard;
    if (insight && insight.notification)
      return (
        <TouchableOpacity style={styles.newsRow} onPress={this.gotoNotifications}>
          <Image source={require("../../assets/icons/dashboard/bolt-blue.png")} />
          <Text style={styles.news} numberOfLines={1} ellipsizeMode="tail">
            {insight.notification}
          </Text>
          {/* <Text style={styles.newsCount}>+5</Text> */}
          <Image source={require("../../assets/icons/arrow-right-blue.png")} style={{ marginLeft: wp(3) }} />
        </TouchableOpacity>
      );
  };

  render() {
    const newsList = this.props.dashboard.newsList ? this.props.dashboard.newsList : [];
    // const { newsList } = this.props.dashboard;
    const { refreshing } = this.state;
    let showNews = newsList.length > 0;

    return (
      <View style={styles.container}>
        {this.renderToolbar()}
        <ScrollView showsVerticalScrollIndicator={false}>
          {showNews && <DashboardNewsList {...this.props} stocks={newsList} />}

          {this.renderNews()}
          <DashboardMenu {...this.props} />

          {this.renderOffer()}
          <SubscriptionFeatures />
          {this.renderShowPlans()}
        </ScrollView>

        {refreshing && <LoadingView />}
      </View>
    );
  }
}

const mapStateToProps = (state) => {
  let firebase = state.app.firebase;

  let version = "version" in firebase ? firebase.version : {};
  let baseURL = "baseUrl" in version ? version.baseUrl : null;
  let appToken = "appToken" in state.app ? state.app.appToken : null;

  return {
    baseURL,
    appToken,
    userState: state.user,
    dashboard: state.user.dashboard ? state.user.dashboard : {},
    market: state.market,
  };
};

export default connect(mapStateToProps, {
  loadUserDetails,
  loadUserDashboard,
  loadWatchlist,
  getActiveWatchlist,
  loadPortfolio,
  // loadIndicesCards
})(DashboardScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: colors.whiteTwo,
  },
  headingRow: {
    flexDirection: "row",
    marginHorizontal: wp(3),
    marginTop: wp(6),
    alignItems: "center",
  },
  heading: {
    flex: 1,
    fontSize: RF(2.4),
    fontWeight: "300",
    color: colors.black,
  },
  row: {
    flexDirection: "row",
  },
  menuContainer: {
    backgroundColor: colors.white,
    paddingVertical: wp(5),
    paddingHorizontal: wp(3),
    marginBottom: wp(10),
  },
  menuItem: {
    flex: 1,
    height: wp(15),
    alignItems: "center",
  },
  menuRowSpace: {
    height: wp(8.5),
  },
  menuName: {
    fontSize: RF(1.9),
    color: colors.black,
    fontWeight: "500",
    marginTop: 5,
  },
  offer: {
    fontSize: 12,
    // fontWeight: "500",
    lineHeight: 19,
    letterSpacing: 0,
    color: colors.emeraldGreen,
    padding: wp(3),
  },
  button: {
    borderRadius: 5,
    backgroundColor: colors.primary_color,
    marginHorizontal: wp(3),
    marginTop: wp(3),
    marginBottom: wp(8),
  },
  buttonText: {
    fontSize: RF(2.2),
    color: colors.white,
    padding: 12,
    textAlign: "center",
  },
  logo: {
    height: wp(6),
    resizeMode: "contain",
  },
  dividerVertical: {
    position: "absolute",
    right: -wp(0.5),
    top: -wp(4),
    bottom: -wp(5),
    width: 1,
    // backgroundColor: colors.primary_color,
    backgroundColor: colors.whiteTwo,
    marginVertical: wp(3),
  },
  dividerHorizontal: {
    position: "absolute",
    right: 0,
    left: 3,
    bottom: -wp(5.5),
    height: 1,
    backgroundColor: colors.whiteTwo,
    // backgroundColor: colors.primary_color,
    marginHorizontal: wp(3),
  },
  newsRow: {
    flexDirection: "row",
    backgroundColor: colors.whiteThree,
    paddingHorizontal: wp(3),
    paddingVertical: wp(2.5),
    marginTop: wp(6.5),
    alignItems: "center",
  },
  news: {
    flex: 1,
    fontSize: RF(1.9),
    color: colors.black,
    marginLeft: wp(3),
    // marginRight: 3
  },
  newsCount: {
    fontSize: RF(1.9),
    marginLeft: wp(2.2),
    color: colors.primary_color,
    marginRight: wp(3),
  },
});
