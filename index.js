/**
 * @format
 */

if (__DEV__) {
  import("./src/ReactotronConfig").then(() => console.log("Reactotron Configured"));
}

import { AppRegistry } from "react-native";
import App from "./App";
import { name as appName } from "./app.json";
import fcmBgMessaging from "./src/fcmBgMessaging";

AppRegistry.registerComponent(appName, () => App);

// New task registration
// The headless task must be registered as RNFirebaseBackgroundMessage.
AppRegistry.registerHeadlessTask("RNFirebaseBackgroundMessage", () => fcmBgMessaging);
