/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from "react";
import { Platform, StyleSheet, StatusBar, View } from "react-native";
import { getStatusBarHeight } from "react-native-status-bar-height";

import { colors } from "./src/styles/CommonStyles";
import AppNavigator from "./src/navigation/AppNavigator";
import OfflineNotice from "./src/components/OfflineNotice";

import { Provider } from "react-redux";
import { store, persistor } from "./src/store";
import { PersistGate } from "redux-persist/integration/react";
import StockFundamentalScreen from "./src/views/stockDetail/StockFundamentalScreen";
import AppContainer from "./src/components/common/AppContainer";
import firebase from "react-native-firebase";
import ForceUpdateScreen from "./src/views/ForceUpdateScreen";

type Props = {};
export default class App extends Component<Props> {
  componentDidMount() {
    if (__DEV__) {
      // firebase.crashlytics().enableCrashlyticsCollection();
      firebase.config().enableDeveloperMode();
    } else {
      firebase.crashlytics().enableCrashlyticsCollection();
      // Disable experimental features of remote config for release
      firebase.config().setDefaults({ hasExperimentalFeature: false });
    }
    firebase.analytics().setAnalyticsCollectionEnabled(true);
  }

  state = { navigation: null };

  setNavRef = (nav) => {
    if (!this.navReceived && nav) {
      this.navReceived = true;
      setTimeout(() => this.setState({ navigation: nav._navigation }), 3000);
    }
  };

  renderStatusBar = () => {
    return (
      <View style={styles.statusbar}>
        <StatusBar backgroundColor={colors.primary_color} barStyle="light-content" translucent />
      </View>
    );
  };

  getActiveRouteName(navigationState) {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // Dive into nested navigators
    if (route.routes) {
      return this.getActiveRouteName(route);
    }
    return route.routeName;
  }

  getNavigationParams(navigationState) {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];
    // Dive into nested navigators
    if (route.routes) {
      return this.getNavigationParams(route);
    }
    return route.params;
  }

  renderApp = () => {
    const { navigation } = this.state;

    return (
      <AppContainer navigation={navigation}>
        <AppNavigator
          ref={(nav) => this.setNavRef(nav)}
          onNavigationStateChange={(prevState, currentState) => {
            const currentScreen = this.getActiveRouteName(currentState);
            const prevScreen = this.getActiveRouteName(prevState);
            const currentPara = this.getNavigationParams(currentState);
            if (prevScreen !== currentScreen) {
              firebase.analytics().setCurrentScreen(currentScreen);
              firebase.analytics().logEvent(currentScreen, currentPara);
            }
          }}
        />
        <ForceUpdateScreen />
        {/* <ForceUpdateScreen /> */}
        <OfflineNotice />
      </AppContainer>
    );
  };

  render() {
    console.disableYellowBox = true;

    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={persistor}>
          {this.renderStatusBar()}
          {this.renderApp()}
        </PersistGate>
      </Provider>
    );
  }
}

const styles = StyleSheet.create({
  // container: {
  //   flex: 1,
  //   backgroundColor: colors.ice_blue
  // },
  statusbar: {
    height: getStatusBarHeight(),
    width: "100%",
    backgroundColor: colors.primary_color,
  },
});
