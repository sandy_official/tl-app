/* AUTO-GENERATED FILE.  DO NOT MODIFY.
 *
 * This class was automatically generated by the
 * gradle plugin from the resource data it found. It
 * should not be modified by hand.
 */
package com.google.android.gms.tagmanager.impl;

public final class R {
    private R() {}

    public static final class string {
        private string() {}

        public static final int tagmanager_preview_dialog_button = 0x7f0d0076;
        public static final int tagmanager_preview_dialog_message = 0x7f0d0077;
        public static final int tagmanager_preview_dialog_title = 0x7f0d0078;
    }
}
